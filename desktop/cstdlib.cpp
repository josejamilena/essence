#define _POSIX_SOURCE
#define _GNU_SOURCE

#define ES_API
#define ES_FORWARD(x) x
#define ES_EXTERN_FORWARD extern "C"
#define ES_DIRECT_API
#include <essence.h>

#define STB_DS_DEFINITIONS_ONLY
#include <shared/stb_ds.h>

extern "C" void *ProcessorTLSRead(uintptr_t offset);
extern "C" void ProcessorTLSWrite(uintptr_t offset, void *value);
extern ptrdiff_t tlsStorageOffset;
extern EsProcessStartupInformation *startupInformation;

#define __NEED_struct_iovec
#define __NEED_sigset_t
#define __NEED_struct_timespec
#define __NEED_time_t
#include <limits.h>
#include <bits/syscall.h>
#include <bits/alltypes.h>
#include <signal.h>
#include <sys/sysinfo.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <bits/ioctl.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <poll.h>
#include <sys/utsname.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sched.h>
#include <elf.h>

extern uint64_t esSystemConstants[];
char *workingDirectory;
int lastVforkPid;

#ifdef DEBUG_BUILD
double syscallTimeSpent[1024];
uint64_t syscallCallCount[1024];
#endif

const char *syscallNames[] = {
	"read", "write", "open", "close", "stat", "fstat", "lstat", "poll",
	"lseek", "mmap", "mprotect", "munmap", "brk", "rt_sigaction", "rt_sigprocmask", "rt_sigreturn",
	"ioctl", "pread64", "pwrite64", "readv", "writev", "access", "pipe", "select",
	"sched_yield", "mremap", "msync", "mincore", "madvise", "shmget", "shmat", "shmctl",
	"dup", "dup2", "pause", "nanosleep", "getitimer", "alarm", "setitimer", "getpid",
	"sendfile", "socket", "connect", "accept", "sendto", "recvfrom", "sendmsg", "recvmsg",
	"shutdown", "bind", "listen", "getsockname", "getpeername", "socketpair", "setsockopt", "getsockopt",
	"clone", "fork", "vfork", "execve", "exit", "wait4", "kill", "uname",
	"semget", "semop", "semctl", "shmdt", "msgget", "msgsnd", "msgrcv", "msgctl",
	"fcntl", "flock", "fsync", "fdatasync", "truncate", "ftruncate", "getdents", "getcwd",
	"chdir", "fchdir", "rename", "mkdir", "rmdir", "creat", "link", "unlink",
	"symlink", "readlink", "chmod", "fchmod", "chown", "fchown", "lchown", "umask",
	"gettimeofday", "getrlimit", "getrusage", "sysinfo", "times", "ptrace", "getuid", "syslog",
	"getgid", "setuid", "setgid", "geteuid", "getegid", "setpgid", "getppid", "getpgrp",
	"setsid", "setreuid", "setregid", "getgroups", "setgroups", "setresuid", "getresuid", "setresgid",
	"getresgid", "getpgid", "setfsuid", "setfsgid", "getsid", "capget", "capset", "rt_sigpending",
	"rt_sigtimedwait", "rt_sigqueueinfo", "rt_sigsuspend", "sigaltstack", "utime", "mknod", "uselib", "personality",
	"ustat", "statfs", "fstatfs", "sysfs", "getpriority", "setpriority", "sched_setparam", "sched_getparam",
	"sched_setscheduler", "sched_getscheduler", "sched_get_priority_max", "sched_get_priority_min", "sched_rr_get_interval", "mlock", "munlock", "mlockall",
	"munlockall", "vhangup", "modify_ldt", "pivot_root", "_sysctl", "prctl", "arch_prctl", "adjtimex",
	"setrlimit", "chroot", "sync", "acct", "settimeofday", "mount", "umount2", "swapon",
	"swapoff", "reboot", "sethostname", "setdomainname", "iopl", "ioperm", "create_module", "init_module",
	"delete_module", "get_kernel_syms", "query_module", "quotactl", "nfsservctl", "getpmsg", "putpmsg", "afs_syscall",
	"tuxcall", "security", "gettid", "readahead", "setxattr", "lsetxattr", "fsetxattr", "getxattr",
	"lgetxattr", "fgetxattr", "listxattr", "llistxattr", "flistxattr", "removexattr", "lremovexattr", "fremovexattr",
	"tkill", "time", "futex", "sched_setaffinity", "sched_getaffinity", "set_thread_area", "io_setup", "io_destroy",
	"io_getevents", "io_submit", "io_cancel", "get_thread_area", "lookup_dcookie", "epoll_create", "epoll_ctl_old", "epoll_wait_old",
	"remap_file_pages", "getdents64", "set_tid_address", "restart_syscall", "semtimedop", "fadvise64", "timer_create", "timer_settime",
	"timer_gettime", "timer_getoverrun", "timer_delete", "clock_settime", "clock_gettime", "clock_getres", "clock_nanosleep", "exit_group",
	"epoll_wait", "epoll_ctl", "tgkill", "utimes", "vserver", "mbind", "set_mempolicy", "get_mempolicy",
	"mq_open", "mq_unlink", "mq_timedsend", "mq_timedreceive", "mq_notify", "mq_getsetattr", "kexec_load", "waitid",
	"add_key", "request_key", "keyctl", "ioprio_set", "ioprio_get", "inotify_init", "inotify_add_watch", "inotify_rm_watch",
	"migrate_pages", "openat", "mkdirat", "mknodat", "fchownat", "futimesat", "newfstatat", "unlinkat",
	"renameat", "linkat", "symlinkat", "readlinkat", "fchmodat", "faccessat", "pselect6", "ppoll",
	"unshare", "set_robust_list", "get_robust_list", "splice", "tee", "sync_file_range", "vmsplice", "move_pages",
	"utimensat", "epoll_pwait", "signalfd", "timerfd_create", "eventfd", "fallocate", "timerfd_settime", "timerfd_gettime",
	"accept4", "signalfd4", "eventfd2", "epoll_create1", "dup3", "pipe2", "inotify_init1", "preadv",
	"pwritev", "rt_tgsigqueueinfo", "perf_event_open", "recvmmsg", "fanotify_init", "fanotify_mark", "prlimit64", "name_to_handle_at",
	"open_by_handle_at", "clock_adjtime", "syncfs", "sendmmsg", "setns", "getcpu", "process_vm_readv", "process_vm_writev",
	"kcmp", "finit_module", "sched_setattr", "sched_getattr", "renameat2", "seccomp", "getrandom", "memfd_create",
	"kexec_file_load", "bpf", "execveat", "userfaultfd", "membarrier", "mlock2", "copy_file_range", "preadv2",
	"pwritev2", "pkey_mprotect", "pkey_alloc", "pkey_free", "statx",
};

extern "C" void CheckStackAlignment();

long EsMakeLinuxSystemCall2(long n, long a1, long a2, long a3, long a4, long a5, long a6) {
#ifdef DEBUG_BUILD
	CheckStackAlignment();
#endif

	long returnValue = 0;
	_EsPOSIXSyscall syscall = { n, a1, a2, a3, a4, a5, a6 };

#ifdef DEBUG_BUILD
	double startTime = EsTimeStampMs();
	static double processStartTime = 0;

	if (!processStartTime) {
		processStartTime = startTime;
	}

	if (n == SYS_exit_group) {
		double processExecutionTime = startTime - processStartTime;

		EsPrint("=== System call performance ===\n");

		int array[sizeof(syscallNames) / sizeof(syscallNames[0])];

		for (uintptr_t i = 0; i < sizeof(array) / sizeof(array[0]); i++) {
			array[i] = i;
		}

		EsCRTqsort(array, sizeof(array) / sizeof(array[0]), sizeof(array[0]), [] (const void *_left, const void *_right) {
			int left = *(int *) _left, right = *(int *) _right;
			if (syscallTimeSpent[left] > syscallTimeSpent[right]) return -1;
			if (syscallTimeSpent[left] < syscallTimeSpent[right]) return 1;
			return 0;
		});

		double total = 0;

		for (uintptr_t i = 0; i < sizeof(array) / sizeof(array[0]); i++) {
			if (!syscallTimeSpent[array[i]]) break;
			EsPrint("%z - %Fms - %d calls\n", syscallNames[array[i]], syscallTimeSpent[array[i]], syscallCallCount[array[i]]);
			total += syscallTimeSpent[array[i]];
		}

		EsPrint("Total time in system calls: %Fms\n", total);
		EsPrint("Total run time of process: %Fms\n", processExecutionTime);
	}
#endif

	switch (n) {
		case SYS_open: {
			char *path = (char *) a1;
			bool freePath = false;

			if (*path != '/') {
				char *buffer = (char *) EsHeapAllocate(EsCStringLength(workingDirectory) + EsCStringLength(path) + 1, true);
				EsStringFormat(buffer, ES_STRING_FORMAT_ENOUGH_SPACE, "%z%z", workingDirectory, path);
				freePath = true;
				path = buffer;
			}

			// EsPrint(":::: open %z\n", path);

			syscall.arguments[0] = (long) path;
			syscall.arguments[6] = EsCStringLength(path);
			returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
			if (freePath) EsHeapFree(path);
		} break;

		case SYS_vfork: {
			lastVforkPid = returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
		} break;

		case SYS_pipe: {
			syscall.index = SYS_pipe2;
			syscall.arguments[1] = 0;
			returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
		} break;

		case SYS_pipe2:
		case SYS_writev:
		case SYS_fcntl:
		case SYS_dup2:
		case SYS_write:
		case SYS_readv:
		case SYS_lseek:
		case SYS_read:
		case SYS_fstat:
		case SYS_sysinfo:
		case SYS_close:
		case SYS_getdents64:
		case SYS_exit_group:
		case SYS_ioctl: {
			returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
			// EsPrint("\treturning %d\n", returnValue);
		} break;

		case SYS_chdir: {
			const char *path = (const char *) a1;
			size_t length = EsCStringLength(path);
			EsHeapFree(workingDirectory);
			workingDirectory = (char *) EsHeapAllocate(length + 2, true);
			if (path[length - 1] != '/') workingDirectory[length] = '/';
			EsMemoryCopy(workingDirectory, path, length);
			return 0;
		} break;

		case SYS_getpid: {
			return EsProcessGetID(ES_CURRENT_PROCESS);
		} break;

		case SYS_gettid: {
			return EsThreadGetID(ES_CURRENT_THREAD);
		} break;

		case SYS_getcwd: {
			if (EsCStringLength(workingDirectory) + 1 > (size_t) a2) return -ERANGE;
			EsMemoryCopy((void *) a1, workingDirectory, EsCStringLength(workingDirectory) + 1);
			return a1;
		} break;

		case SYS_getppid:
		case SYS_getuid:
		case SYS_getgid:
		case SYS_getegid:
		case SYS_geteuid: {
			// TODO.
			return 0;
		} break;

		case SYS_getrusage: {
			// TODO.
			struct rusage *buffer = (struct rusage *) a2;
			EsMemoryZero(buffer, sizeof(struct rusage));
			return 0;
		} break;

		case SYS_unlink: {
			_EsNodeInformation node;
			bool freePath = false;
			char *path = (char *) a1;

			if (*path != '/') {
				char *buffer = (char *) EsHeapAllocate(EsCStringLength(workingDirectory) + EsCStringLength(path) + 1, false);
				buffer[EsStringFormat(buffer, ES_STRING_FORMAT_ENOUGH_SPACE, "%z%z", workingDirectory, path)] = 0;
				freePath = true;
				// EsPrint("Relative path, resolve '%z' to '%z'\n", path, buffer);
				path = buffer;
			}

			EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, EsCStringLength(path), 
					ES_NODE_FAIL_IF_NOT_FOUND | ES_NODE_POSIX_NAMESPACE, (uintptr_t) &node);
			// EsPrint("DLTE '%z'\n", path);
			if (freePath) EsHeapFree(path);
			if (error == ES_ERROR_FILE_DOES_NOT_EXIST || error == ES_ERROR_PATH_NOT_TRAVERSABLE) return -ENOENT;
			else if (error == ES_ERROR_FILE_IN_EXCLUSIVE_USE) return -EBUSY;
			else if (error == ES_ERROR_DRIVE_CONTROLLER_REPORTED || error == ES_ERROR_CORRUPT_DATA) return -EIO;
			else if (error != ES_SUCCESS) return -EACCES;
			error = EsSyscall(ES_SYSCALL_DELETE_NODE, node.handle, 0, 0, 0);
			EsHandleClose(node.handle);
			if (error == ES_ERROR_DRIVE_CONTROLLER_REPORTED || error == ES_ERROR_CORRUPT_DATA) return -EIO;
			else if (error != ES_SUCCESS) return -EACCES;
			return 0;
		} break;

		case SYS_execve: {
			char *filename = (char *) a1;
			char **argv = (char **) a2;
			char **envp = (char **) a3;
		
			size_t environmentSize = 2;

			for (uintptr_t i = 0; argv[i]; i++) environmentSize += EsCStringLength(argv[i]) + 1;
			for (uintptr_t i = 0; envp[i]; i++) environmentSize += EsCStringLength(envp[i]) + 1;

			char newEnvironment[environmentSize]; // We can't use EsHeapAllocate since the system call never returns.
			char *position = newEnvironment;
			EsMemoryZero(newEnvironment, environmentSize);

			for (uintptr_t i = 0; argv[i]; i++) {
				size_t length = EsCStringLength(argv[i]) + 1;
				EsMemoryCopy(position, argv[i], length);
				position += length;
			}

			position++;

			for (uintptr_t i = 0; envp[i]; i++) {
				size_t length = EsCStringLength(envp[i]) + 1;
				EsMemoryCopy(position, envp[i], length);
				position += length;
			}

			syscall.arguments[0] = (long) filename;
			syscall.arguments[1] = (long) EsCStringLength(filename);
			syscall.arguments[2] = (long) newEnvironment;
			syscall.arguments[3] = (long) environmentSize;

			// EsPrint("Execve %d -> %z\n", EsProcessGetID(ES_CURRENT_PROCESS), filename);

			returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
		} break;

		case SYS_access: {
			// We don't support file permissions yet, so just check the file exists.
			int fd = EsMakeLinuxSystemCall2(SYS_open, a1, O_PATH, 0, 0, 0, 0);
			if (fd < 0) returnValue = fd;
			else {
				returnValue = 0;
				EsMakeLinuxSystemCall2(SYS_close, fd, 0, 0, 0, 0, 0);
			}
		} break;

		case SYS_lstat:
		case SYS_stat: {
			int fd = EsMakeLinuxSystemCall2(SYS_open, a1, O_PATH, 0, 0, 0, 0);
			if (fd < 0) returnValue = fd;
			else {
				returnValue = EsMakeLinuxSystemCall2(SYS_fstat, fd, a2, 0, 0, 0, 0);
				EsMakeLinuxSystemCall2(SYS_close, fd, 0, 0, 0, 0, 0);
			}
		} break;

		case SYS_readlink: {
			if (0 == EsMemoryCompare((void *) a1, EsLiteral("/proc/self/fd/"))) {
				// The process is trying to get the path of a file descriptor.
				syscall.index = ES_POSIX_SYSCALL_GET_POSIX_FD_PATH;
				syscall.arguments[0] = EsCRTatoi((char *) a1 + EsCStringLength("/proc/self/fd/"));
				returnValue = EsSyscall(ES_SYSCALL_POSIX, (uintptr_t) &syscall, 0, 0, 0);
			} else {
				// We don't support symbolic links, so the output is the same as the input.
				int length = EsCStringLength((char *) a1);
				EsMemoryZero((void *) a2, a3);
				EsMemoryCopy((void *) a2, (void *) a1, length > a3 ? a3 : length);
				returnValue = length > a3 ? a3 : length;
			}
		} break;

		case SYS_set_tid_address: {
			// TODO Support set_child_tid and clear_child_tid addresses.
			returnValue = EsThreadGetID(ES_CURRENT_THREAD);
		} break;

		case SYS_brk: {
			returnValue = -1;
		} break;

		case SYS_mremap: {
			returnValue = -ENOMEM;
		} break;

		case SYS_mmap: {
			bool read = a3 & PROT_READ, write = a3 & PROT_WRITE, none = a3 == PROT_NONE;

			if (a4 & MAP_FIXED) {
				returnValue = -ENOMEM;
			} else if ((a4 == (MAP_ANON | MAP_PRIVATE)) && (a5 == -1) && (a6 == 0) && ((read && write) || none)) {
				returnValue = (long) EsMemoryReserve(a2, ES_MEMORY_PROTECTION_READ_WRITE, none ? 0 : ES_MEMORY_RESERVE_COMMIT_ALL);
			} else {
				EsPrint("Unsupported mmap [%x, %x, %x, %x, %x, %x]\n", a1, a2, a3, a4, a5, a6);
				EsProcessCrash(ES_FATAL_ERROR_UNKNOWN_SYSCALL, nullptr, 0);
			}

		} break;

		case SYS_munmap: {
			void *address = (void *) a1;
			size_t length = (size_t) a2;

			if (length == 0 || ((uintptr_t) address & (ES_PAGE_SIZE - 1))) {
				returnValue = -EINVAL;
			} else {
				EsMemoryUnreserve(address, length); 
			}
		} break;

		case SYS_mprotect: {
			void *address = (void *) a1;
			size_t length = (size_t) a2;
			int protection = (int) a3;

			if (protection == (PROT_READ | PROT_WRITE)) {
				returnValue = EsMemoryCommit(address, length) ? 0 : -ENOMEM;
			} else if (protection == 0) {
				returnValue = EsMemoryDecommit(address, length) ? 0 : -ENOMEM; 
			} else {
				EsPrint("Unsupported mprotect [%x, %x, %x, %x, %x, %x]\n", a1, a2, a3, a4, a5, a6);
				EsProcessCrash(ES_FATAL_ERROR_UNKNOWN_SYSCALL, nullptr, 0);
			}
		} break;

		case SYS_prlimit64: {
			// You can't access other process's resources.
			if (a1 && a1 != (long) EsProcessGetID(ES_CURRENT_PROCESS)) {
				returnValue = -EPERM;
				break;
			}

			// You can't change resource limits.
			if (a3) {
				returnValue = -EPERM;
				break;
			}

			// Get the limit.
			struct rlimit *limit = (struct rlimit *) a4;
			if (a2 == RLIMIT_STACK) {
				limit->rlim_cur = limit->rlim_max = 0x100000; // 1MB stack. See Scheduler::SpawnThread.
			} else if (a2 == RLIMIT_AS) {
				limit->rlim_cur = limit->rlim_max = RLIM_INFINITY;
			} else if (a2 == RLIMIT_RSS) {
				limit->rlim_cur = limit->rlim_max = 0x10000000; // 256MB. This value is fake. TODO
			} else if (a2 == RLIMIT_NOFILE) {
				limit->rlim_cur = limit->rlim_max = 1048576;
			} else {
				EsPrint("Unsupported prlimit64 [%x]\n", a2);
				EsProcessCrash(ES_FATAL_ERROR_UNKNOWN_SYSCALL, nullptr, 0);
			}

			return 0;
		} break;

		case SYS_setitimer:
		case SYS_madvise:
		case SYS_umask:
		case SYS_chmod:
		case SYS_rt_sigaction:
		case SYS_rt_sigprocmask: {
			// TODO Support signals.
			// Ignore.
		} break;

		case SYS_clock_gettime: {
			// We'll ignore the clockid_t in a1, since we don't have proper timekeeping yet.
			struct timespec *tp = (struct timespec *) a2;
			uint64_t timeStamp = EsTimeStamp();
			uint64_t unitsPerMicrosecond = esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
			uint64_t microseconds = timeStamp / unitsPerMicrosecond;
			tp->tv_sec = microseconds / 1000000;
			tp->tv_nsec = microseconds * 1000;
			return 0;
		} break;

		case SYS_wait4: {
			// We don't support the options or rusage parameters yet.
			if (a3 || a4) {
				EsPrint("Unsupported wait4 [%x/%x/%x/%x]\n", a1, a2, a3, a4);
				EsProcessCrash(ES_FATAL_ERROR_UNKNOWN_SYSCALL, nullptr, 0);
			}

			// EsPrint("Wait %d -> %d\n", EsProcessGetID(ES_CURRENT_PROCESS), a1);

			int *wstatus = (int *) a2;

			// TODO Properly waiting on 'child' processes.
			if (a1 == -1) a1 = lastVforkPid;

			EsHandle process = EsProcessOpen(a1);

			if (process != ES_INVALID_HANDLE) {
				EsWait(&process, 1, ES_WAIT_NO_TIMEOUT);
				int status = EsProcessGetExitStatus(process) & 0xFF;
				*wstatus = status << 8;
				EsHandleClose(process);
			}

			return a1;
		} break;

		case SYS_sched_getaffinity: {
			// TODO Getting the correct number of CPUs.
			// TODO Getting the affinity for other processes.
			cpu_set_t *set = (cpu_set_t *) a3;
			EsCRTmemset(set, 0, a2);
			CPU_SET(0, set); 
			return 0;
		} break;

		case -1000: {
			// Update thread local storage:
			void *apiTLS = ProcessorTLSRead(tlsStorageOffset);
			EsSyscall(ES_SYSCALL_SET_TLS, a1, 0, 0, 0);
			tlsStorageOffset = -a2;
			ProcessorTLSWrite(tlsStorageOffset, apiTLS);
			return 0;
		} break;

		default: {
			EsPrint("Unknown linux syscall %d = %z.\n", n, syscallNames[n]);
			EsPrint("Arguments: %x, %x, %x, %x, %x, %x\n", a1, a2, a3, a4, a5, a6);
			EsProcessCrash(ES_FATAL_ERROR_UNKNOWN_SYSCALL, nullptr, 0);
		} break;
	}

#ifdef DEBUG_BUILD
	double endTime = EsTimeStampMs();
	syscallTimeSpent[n] += endTime - startTime;
	syscallCallCount[n]++;
#endif

	// EsPrint(":: %z %x %x %x -> %x; %Fms\n", syscallNames[n], a1, a2, a3, returnValue, endTime - startTime);

	return returnValue;
}

void EsInitialiseCStandardLibrary(int *argc, char ***argv) {
	// Get the arguments and environment.

	void *creationArgument = EsGetCreationArgument(ES_CURRENT_PROCESS).p;
	char *environmentBuffer = (char *) "./application\0\0LANG=en_US.UTF-8\0PWD=/\0HOME=/\0PATH=/Applications/POSIX/usr/bin\0\0";

	if (creationArgument) {
		// EsPrint("have creation argument, %x\n", creationArgument);
		environmentBuffer = (char *) EsHeapAllocate(ARG_MAX, false);
		EsConstantBufferRead((EsHandle) creationArgument, environmentBuffer);
		EsHandleClose((EsHandle) creationArgument);
	}

	// Extract the arguments and environment variables.

	uintptr_t position = 0;
	char *start = environmentBuffer;
	DS_ARRAY(void *) _argv = nullptr;
	*argc = 0;

	for (int i = 0; i < 2; i++) {
		while (position < ARG_MAX) {
			if (!environmentBuffer[position]) {
				arrput(_argv, start);
				start = environmentBuffer + position + 1;

				if (i == 0) {
					*argc = *argc + 1;
				}

				if (!environmentBuffer[position + 1]) {
					start = environmentBuffer + position + 2;
					arrput(_argv, nullptr);
					break;
				}
			}

			position++;
		}

		position += 2;
	}

	// Copy the working directory string.

	for (int i = *argc + 1; i < arrlen(_argv); i++) {
		if (_argv[i] && 0 == EsMemoryCompare("PWD=", _argv[i], 4)) {
			size_t length = EsCStringLength((char *) _argv[i]) - 4;
			workingDirectory = (char *) EsHeapAllocate(length + 2, false);
			workingDirectory[length] = 0, workingDirectory[length + 1] = 0;
			EsMemoryCopy(workingDirectory, (char *) _argv[i] + 4, length);
			if (workingDirectory[length - 1] != '/') workingDirectory[length] = '/';
		}
	}

	// Add the auxillary vectors.

#ifdef ARCH_X86_64
	Elf64_Phdr *tlsHeader = (Elf64_Phdr *) EsHeapAllocate(sizeof(Elf64_Phdr), true);
	tlsHeader->p_type = PT_TLS;
	tlsHeader->p_flags = 4 /* read */;
	tlsHeader->p_vaddr = startupInformation->tlsImageStart;
	tlsHeader->p_filesz = startupInformation->tlsImageBytes;
	tlsHeader->p_memsz = startupInformation->tlsBytes;
	tlsHeader->p_align = 8;

	arrput(_argv, (void *) AT_PHNUM);
	arrput(_argv, (void *) 1);
	arrput(_argv, (void *) AT_PHENT);
	arrput(_argv, (void *) sizeof(Elf64_Phdr));
	arrput(_argv, (void *) AT_PHDR);
	arrput(_argv, (void *) tlsHeader);
#else
#error "no architecture TLS support"
#endif

	arrput(_argv, (void *) AT_PAGESZ);
	arrput(_argv, (void *) ES_PAGE_SIZE);

	arrput(_argv, nullptr);

	// Return argv.
	
	*argv = (char **) _argv;
}
