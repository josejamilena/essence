// TODO Warn on Read/WriteBAR if port IO/memory space access is disabled in the command register.

#include <module.h>

#define PCI_CONFIG	(0xCF8)
#define PCI_DATA	(0xCFC)

const char *classCodeStrings[] = {
	"Unknown",
	"Mass storage controller",
	"Network controller",
	"Display controller",
	"Multimedia controller",
	"Memory controller",
	"Bridge controller",
	"Simple communication controller",
	"Base system peripheral",
	"Input device controller",
	"Docking station",
	"Processor",
	"Serial bus controller",
	"Wireless controller",
	"Intelligent controller",
	"Satellite communication controller",
	"Encryption controller",
	"Signal processing controller",
};

const char *subclassCodeStrings1[] = {
	"SCSI bus controller",
	"IDE controller",
	"Floppy disk controller",
	"IPI bus controller",
	"RAID controller",
	"ATA controller",
	"Serial ATA",
	"Serial attached SCSI",
	"Non-volatile memory controller",
};

const char *subclassCodeStrings12[] = {
	"FireWire (IEEE 1394) controller",
	"ACCESS bus",
	"SSA",
	"USB controller",
	"Fibre channel",
	"SMBus",
	"InfiniBand",
	"IPMI interface",
	"SERCOS interface (IEC 61491)",
	"CANbus",
};

const char *progIFStrings12_3[] = {
	"UHCI",
	"OHCI",
	"EHCI",
	"XHCI",
};

uint8_t KPCIDevice::ReadBAR8(uintptr_t index, uintptr_t offset) {
	uint32_t baseAddress = baseAddresses[index];
	uint8_t result;

	if (baseAddress & 1) {
		result = ProcessorIn8((baseAddress & ~3) + offset);
	} else {
		result = *(volatile uint8_t *) (baseAddressesVirtual[index] + offset);
	}

	KernelLog(LOG_VERBOSE, "PCI", "read BAR", "ReadBAR8 - %x, %x, %x, %x\n", this, index, offset, result);
	return result;
}

void KPCIDevice::WriteBAR8(uintptr_t index, uintptr_t offset, uint8_t value) {
	uint32_t baseAddress = baseAddresses[index];
	KernelLog(LOG_VERBOSE, "PCI", "write BAR", "WriteBAR8 - %x, %x, %x, %x\n", this, index, offset, value);

	if (baseAddress & 1) {
		ProcessorOut8((baseAddress & ~3) + offset, value);
	} else {
		*(volatile uint8_t *) (baseAddressesVirtual[index] + offset) = value;
	}
}

uint16_t KPCIDevice::ReadBAR16(uintptr_t index, uintptr_t offset) {
	uint32_t baseAddress = baseAddresses[index];
	uint16_t result;

	if (baseAddress & 1) {
		result = ProcessorIn16((baseAddress & ~3) + offset);
	} else {
		result = *(volatile uint16_t *) (baseAddressesVirtual[index] + offset);
	}

	KernelLog(LOG_VERBOSE, "PCI", "read BAR", "ReadBAR16 - %x, %x, %x, %x\n", this, index, offset, result);
	return result;
}

void KPCIDevice::WriteBAR16(uintptr_t index, uintptr_t offset, uint16_t value) {
	uint32_t baseAddress = baseAddresses[index];
	KernelLog(LOG_VERBOSE, "PCI", "write BAR", "WriteBAR16 - %x, %x, %x, %x\n", this, index, offset, value);

	if (baseAddress & 1) {
		ProcessorOut16((baseAddress & ~3) + offset, value);
	} else {
		*(volatile uint16_t *) (baseAddressesVirtual[index] + offset) = value;
	}
}

uint32_t KPCIDevice::ReadBAR32(uintptr_t index, uintptr_t offset) {
	uint32_t baseAddress = baseAddresses[index];
	uint32_t result;

	if (baseAddress & 1) {
		result = ProcessorIn32((baseAddress & ~3) + offset);
	} else {
		result = *(volatile uint32_t *) (baseAddressesVirtual[index] + offset);
	}

	KernelLog(LOG_VERBOSE, "PCI", "read BAR", "ReadBAR32 - %x, %x, %x, %x\n", this, index, offset, result);
	return result;
}

void KPCIDevice::WriteBAR32(uintptr_t index, uintptr_t offset, uint32_t value) {
	uint32_t baseAddress = baseAddresses[index];
	KernelLog(LOG_VERBOSE, "PCI", "write BAR", "WriteBAR32 - %x, %x, %x, %x\n", this, index, offset, value);

	if (baseAddress & 1) {
		ProcessorOut32((baseAddress & ~3) + offset, value);
	} else {
		*(volatile uint32_t *) (baseAddressesVirtual[index] + offset) = value;
	}
}

uint64_t KPCIDevice::ReadBAR64(uintptr_t index, uintptr_t offset) {
	uint32_t baseAddress = baseAddresses[index];
	uint64_t result;

	if (baseAddress & 1) {
		result = (uint64_t) ReadBAR32(index, offset) | ((uint64_t) ReadBAR32(index, offset + 4) << 32);
	} else {
		result = *(volatile uint64_t *) (baseAddressesVirtual[index] + offset);
	}

	KernelLog(LOG_VERBOSE, "PCI", "read BAR", "ReadBAR64 - %x, %x, %x, %x\n", this, index, offset, result);
	return result;
}

void KPCIDevice::WriteBAR64(uintptr_t index, uintptr_t offset, uint64_t value) {
	uint32_t baseAddress = baseAddresses[index];
	KernelLog(LOG_VERBOSE, "PCI", "write BAR", "WriteBAR64 - %x, %x, %x, %x\n", this, index, offset, value);

	if (baseAddress & 1) {
		WriteBAR32(index, offset, value & 0xFFFFFFFF);
		WriteBAR32(index, offset + 4, (value >> 32) & 0xFFFFFFFF);
	} else {
		*(volatile uint64_t *) (baseAddressesVirtual[index] + offset) = value;
	}
}

struct PCIController : KDevice {
	inline uint32_t ReadConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, int size = 32);
	inline void WriteConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, uint32_t value, int size = 32);

#define PCI_BUS_DO_NOT_SCAN 0
#define PCI_BUS_SCAN_NEXT 1
#define PCI_BUS_SCANNED 2
	uint8_t busScanStates[256];

	void Enumerate();
	void EnumerateFunction(int bus, int device, int function, int *busesToScan);
};

// Spinlock since some drivers need to access it in IRQs (e.g. ACPICA).
// Also can't be part of PCIController since PCI is initialised after ACPICA.
static KSpinlock configSpaceSpinlock; 

static PCIController *pci;

uint32_t KPCIReadConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, int size) {
	return pci->ReadConfig(bus, device, function, offset, size);
}

void KPCIWriteConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, uint32_t value, int size) {
	pci->WriteConfig(bus, device, function, offset, value, size);
}

uint32_t PCIController::ReadConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, int size) {
	configSpaceSpinlock.Acquire();
	EsDefer(configSpaceSpinlock.Release());
	if (offset & 3) KernelPanic("PCIController::ReadConfig - offset is not 4-byte aligned.");
	ProcessorOut32(PCI_CONFIG, (uint32_t) (0x80000000 | (bus << 16) | (device << 11) | (function << 8) | offset));
	if (size == 8) return ProcessorIn8(PCI_DATA);
	if (size == 16) return ProcessorIn16(PCI_DATA);
	if (size == 32) return ProcessorIn32(PCI_DATA);
	KernelPanic("PCIController::ReadConfig - Invalid size %d.\n", size);
	return 0;
}

void PCIController::WriteConfig(uint8_t bus, uint8_t device, uint8_t function, uint8_t offset, uint32_t value, int size) {
	configSpaceSpinlock.Acquire();
	EsDefer(configSpaceSpinlock.Release());
	if (offset & 3) KernelPanic("PCIController::WriteConfig - offset is not 4-byte aligned.");
	ProcessorOut32(PCI_CONFIG, (uint32_t) (0x80000000 | (bus << 16) | (device << 11) | (function << 8) | offset));
	if (size == 8) ProcessorOut8(PCI_DATA, value);
	else if (size == 16) ProcessorOut16(PCI_DATA, value);
	else if (size == 32) ProcessorOut32(PCI_DATA, value);
	else KernelPanic("PCIController::WriteConfig - Invalid size %d.\n", size);
}

void KPCIDevice::WriteConfig8(uintptr_t offset, uint8_t value) {
	pci->WriteConfig(bus, slot, function, offset, value, 8);
}

uint8_t KPCIDevice::ReadConfig8(uintptr_t offset) {
	return pci->ReadConfig(bus, slot, function, offset, 8);
}

void KPCIDevice::WriteConfig16(uintptr_t offset, uint16_t value) {
	pci->WriteConfig(bus, slot, function, offset, value, 16);
}

uint16_t KPCIDevice::ReadConfig16(uintptr_t offset) {
	return pci->ReadConfig(bus, slot, function, offset, 16);
}

void KPCIDevice::WriteConfig32(uintptr_t offset, uint32_t value) {
	pci->WriteConfig(bus, slot, function, offset, value, 32);
}

uint32_t KPCIDevice::ReadConfig32(uintptr_t offset) {
	return pci->ReadConfig(bus, slot, function, offset, 32);
}

bool KPCIDevice::EnableSingleInterrupt(KIRQHandler irqHandler, void *context, const char *cOwnerName) {
	if (EnableMSI(irqHandler, context, cOwnerName)) {
		return true;
	}

	EnableFeatures(K_PCI_FEATURE_INTERRUPTS);

	if (KRegisterIRQ(interruptLine, irqHandler, context, cOwnerName)) {
		return true;
	}

	return false;
}

bool KPCIDevice::EnableMSI(KIRQHandler irqHandler, void *context, const char *cOwnerName) {
	// Find the MSI capability.

	uint16_t status = ReadConfig32(0x04) >> 16;

	if (~status & (1 << 4)) {
		KernelLog(LOG_ERROR, "PCI", "no MSI support", "Device does not support MSI.\n");
		return false;
	}

	uint8_t pointer = ReadConfig8(0x34);
	uintptr_t index = 0;

	while (pointer && index++ < 0xFF) {
		uint32_t dw = ReadConfig32(pointer);
		uint8_t nextPointer = (dw >> 8) & 0xFF;
		uint8_t id = dw & 0xFF;

		if (id != 5) {
			pointer = nextPointer;
			continue;
		}

		KMSIInformation msi = KRegisterMSI(irqHandler, context, cOwnerName);

		if (!msi.address) {
			KernelLog(LOG_ERROR, "PCI", "register MSI failure", "Could not register MSI.\n");
			return false;
		}

		uint16_t control = (dw >> 16) & 0xFFFF;

		if (msi.data & ~0xFFFF) {
			KUnregisterMSI(msi.tag);
			KernelLog(LOG_ERROR, "PCI", "unsupported MSI data", "PCI only supports 16 bits of MSI data. Requested: %x.\n", msi.data);
			return false;
		}

		if (msi.address & 3) {
			KUnregisterMSI(msi.tag);
			KernelLog(LOG_ERROR, "PCI", "unsupported MSI address", "PCI requires DWORD alignment of MSI address. Requested: %x.\n", msi.address);
			return false;
		}

#ifdef ARCH_64
		if ((msi.address & 0xFFFFFFFF00000000) && (~control & (1 << 7))) {
			KUnregisterMSI(msi.tag);
			KernelLog(LOG_ERROR, "PCI", "unsupported MSI address", "MSI does not support 64-bit addresses. Requested: %x.\n", msi.address);
			return false;
		}
#endif

		control = (control & ~(7 << 4) /* don't allow modifying data */) 
			| (1 << 0 /* enable MSI */);
		dw = (dw & 0x0000FFFF) | (control << 16);

		WriteConfig32(pointer + 0, dw);
		WriteConfig32(pointer + 4, msi.address & 0xFFFFFFFF);

		if (control & (1 << 7)) {
			WriteConfig32(pointer + 8, msi.address >> 32);
			WriteConfig16(pointer + 12, (ReadConfig16(pointer + 12) & 0x3800) | msi.data);
			if (control & (1 << 8)) WriteConfig32(pointer + 16, 0);
		} else {
			WriteConfig16(pointer + 8, msi.data);
			if (control & (1 << 8)) WriteConfig32(pointer + 12, 0);
		}

		return true;
	}

	KernelLog(LOG_ERROR, "PCI", "no MSI support", "Device does not support MSI (2).\n");
	return false;
}

bool KPCIDevice::EnableFeatures(uint64_t features) {
	uint32_t config = ReadConfig32(4);
	if (features & K_PCI_FEATURE_INTERRUPTS) 		config &= ~(1 << 10);
	if (features & K_PCI_FEATURE_BUSMASTERING_DMA) 		config |= 1 << 2;
	if (features & K_PCI_FEATURE_MEMORY_SPACE_ACCESS) 	config |= 1 << 1;
	if (features & K_PCI_FEATURE_IO_PORT_ACCESS)		config |= 1 << 0;
	WriteConfig32(4, config);

	// Return true if the configuration was successfully updated.
	return ReadConfig32(4) == config;
}

bool EnumeratePCIDrivers(KInstalledDriver *driver, KDevice *_device) {
	KPCIDevice *device = (KPCIDevice *) _device;

	int classCode = -1, subclassCode = -1, progIF = -1;
	bool foundAnyDeviceIDs = false, foundMatchingDeviceID = false;

	EsINIState s = {};
	s.buffer = driver->config, s.bytes = driver->configBytes;

	while (EsINIParse(&s)) {
		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("classCode"))) classCode = EsIntegerParse(s.value, s.valueBytes);
		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("subclassCode"))) subclassCode = EsIntegerParse(s.value, s.valueBytes);
		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("progIF"))) progIF = EsIntegerParse(s.value, s.valueBytes);

		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("deviceID"))) {
			foundAnyDeviceIDs = true;
			
			if (device->deviceID == EsIntegerParse(s.value, s.valueBytes)) {
				foundMatchingDeviceID = true;
			}
		}
	}

	if (classCode    != -1 && device->classCode      != classCode)    return false;
	if (subclassCode != -1 && device->subclassCode   != subclassCode) return false;
	if (progIF       != -1 && device->progIF         != progIF)       return false;

	return !foundAnyDeviceIDs || foundMatchingDeviceID;
}

void PCIController::EnumerateFunction(int bus, int device, int function, int *busesToScan) {
	uint32_t deviceID = ReadConfig(bus, device, function, 0x00);
	if ((deviceID & 0xFFFF) == 0xFFFF) return;

	uint32_t deviceClass = ReadConfig(bus, device, function, 0x08);
	uint32_t interruptInformation = ReadConfig(bus, device, function, 0x3C);

	KPCIDevice *pciDevice = (KPCIDevice *) driver->NewDevice(this, sizeof(KPCIDevice));
	if (!pciDevice) return;

	pciDevice->driverData.i = 1; // Indicates this is a KPCIDevice.

	pciDevice->classCode = (deviceClass >> 24) & 0xFF;
	pciDevice->subclassCode = (deviceClass >> 16) & 0xFF;
	pciDevice->progIF = (deviceClass >> 8) & 0xFF;

	pciDevice->bus = bus;
	pciDevice->slot = device;
	pciDevice->function = function;
	
	pciDevice->interruptPin = (interruptInformation >> 8) & 0xFF;
	pciDevice->interruptLine = (interruptInformation >> 0) & 0xFF;

	pciDevice->deviceID = ReadConfig(bus, device, function, 0);
	pciDevice->subsystemID = ReadConfig(bus, device, function, 0x2C);

	for (int i = 0; i < 6; i++) {
		pciDevice->baseAddresses[i] = pciDevice->ReadConfig32(0x10 + 4 * i);
	}

	const char *classCodeString = pciDevice->classCode < sizeof(classCodeStrings) / sizeof(classCodeStrings[0]) 
		? classCodeStrings[pciDevice->classCode] : "Unknown";
	const char *subclassCodeString = 
		  pciDevice->classCode == 1  && pciDevice->subclassCode < sizeof(subclassCodeStrings1)  / sizeof(const char *) 
			? subclassCodeStrings1 [pciDevice->subclassCode] 
		: pciDevice->classCode == 12 && pciDevice->subclassCode < sizeof(subclassCodeStrings12) / sizeof(const char *)
			? subclassCodeStrings12[pciDevice->subclassCode] : "";
	const char *progIFString = 
		  pciDevice->classCode == 12 && pciDevice->subclassCode == 3 && pciDevice->progIF / 0x10 < sizeof(progIFStrings12_3) / sizeof(const char *)
			? progIFStrings12_3[pciDevice->progIF / 0x10] : "";

	KernelLog(LOG_INFO, "PCI", "enumerate device", 
			"Found PCI device at %d/%d/%d with ID %x. Class code: %X '%z'. Subclass code: %X '%z'. Prog IF: %X '%z'. Interrupt pin: %d. Interrupt line: %d.\n", 
			bus, device, function, deviceID, 
			pciDevice->classCode, classCodeString, pciDevice->subclassCode, subclassCodeString, pciDevice->progIF, progIFString,
			pciDevice->interruptPin, pciDevice->interruptLine);

	if (pciDevice->classCode == 0x06 && pciDevice->subclassCode == 0x04 /* PCI bridge */) {
		uint8_t secondaryBus = (ReadConfig(bus, device, function, 0x18) >> 8) & 0xFF; 

		if (busScanStates[secondaryBus] == PCI_BUS_DO_NOT_SCAN) {
			KernelLog(LOG_INFO, "PCI", "PCI bridge", "PCI bridge to bus %d.\n", secondaryBus);
			*busesToScan = *busesToScan + 1;
			busScanStates[secondaryBus] = PCI_BUS_SCAN_NEXT;
		}
	} else {
		// TODO Only map BARs requested by the driver.
		
		for (int i = 0; i < 6; i++) {
			if (pciDevice->baseAddresses[i] & 1) continue;

			// Memory space BAR.

			bool size64 = pciDevice->baseAddresses[i] & 4;

			if (!(pciDevice->baseAddresses[i] & 8)) {
				// TODO Not prefetchable.
			}

			uint64_t address, size;

			if (size64) {
				pciDevice->WriteConfig32(0x10 + 4 * i, 0xFFFFFFFF);
				pciDevice->WriteConfig32(0x10 + 4 * (i + 1), 0xFFFFFFFF);
				size = pciDevice->ReadConfig32(0x10 + 4 * i);
				size |= (uint64_t) pciDevice->ReadConfig32(0x10 + 4 * (i + 1)) << 32;
				pciDevice->WriteConfig32(0x10 + 4 * i, pciDevice->baseAddresses[i]);
				pciDevice->WriteConfig32(0x10 + 4 * (i + 1), pciDevice->baseAddresses[i + 1]);
				address = pciDevice->baseAddresses[i];
				address |= (uint64_t) pciDevice->baseAddresses[i + 1] << 32;
			} else {
				pciDevice->WriteConfig32(0x10 + 4 * i, 0xFFFFFFFF);
				size = pciDevice->ReadConfig32(0x10 + 4 * i);
				size |= (uint64_t) 0xFFFFFFFF << 32;
				pciDevice->WriteConfig32(0x10 + 4 * i, pciDevice->baseAddresses[i]);
				address = pciDevice->baseAddresses[i];
			}

			if (!size || !address) continue;
			size &= ~15;
			size = ~size + 1;
			address &= ~15;

			// TODO Do we sometimes have to allocate the physical address ourselves..?

			// If the driver wants to allow WC caching later, it can, with MMAllowWriteCombiningCaching.
			pciDevice->baseAddressesVirtual[i] = (uint8_t *) MMMapPhysical(MMGetKernelSpace(), address, size, MM_REGION_NOT_CACHEABLE);
			pciDevice->baseAddressesPhysical[i] = address;
			pciDevice->baseAddressesSizes[i] = size;

			MMCheckUnusable(address, size);

			KernelLog(LOG_INFO, "PCI", "enumerate device BAR", 
					"BAR %d has address %x and size %x, mapped to %x.\n",
					i, address, size, pciDevice->baseAddressesVirtual[i]);

			if (size64) i++;
		}
	}

	pciDevice->implementingDriver = driver->ChildDeviceAttached(EnumeratePCIDrivers, pciDevice);
}

void PCIController::Enumerate() {
	uint32_t baseHeaderType = ReadConfig(0, 0, 0, 0x0C);
	int baseBuses = (baseHeaderType & 0x80) ? 8 : 1;

	int busesToScan = 0;

	for (int baseBus = 0; baseBus < baseBuses; baseBus++) {
		uint32_t deviceID = ReadConfig(0, 0, baseBus, 0x00);
		if ((deviceID & 0xFFFF) == 0xFFFF) continue;
		busScanStates[baseBus] = PCI_BUS_SCAN_NEXT;
		busesToScan++;
	}

	if (!busesToScan) {
		KernelPanic("PCIController::Enumerate - No buses found\n");
	}

	while (busesToScan) {
		for (int bus = 0; bus < 256; bus++) {
			if (busScanStates[bus] != PCI_BUS_SCAN_NEXT) continue;

			KernelLog(LOG_INFO, "PCI", "scan bus", "Scanning bus %d...\n", bus);

			busScanStates[bus] = PCI_BUS_SCANNED;
			busesToScan--;

			for (int device = 0; device < 32; device++) {
				uint32_t deviceID = ReadConfig(bus, device, 0, 0x00);
				if ((deviceID & 0xFFFF) == 0xFFFF) continue;

				uint32_t headerType = (ReadConfig(bus, device, 0, 0x0C) >> 16) & 0xFF;
				int functions = (headerType & 0x80) ? 8 : 1;

				for (int function = 0; function < functions; function++) {
					EnumerateFunction(bus, device, function, &busesToScan);
				}
			}
		}
	}
}

size_t KPCIDebugGetEnumeratedDevices(KDriver *driver, K_USER_BUFFER EsPCIDevice *buffer, size_t bufferCount) {
	size_t count = 0;

	for (uintptr_t i = 0; i < ArrayLength(driver->devices) && count < bufferCount; i++) {
		if (driver->devices[i]->driverData.i == 1) {
			KPCIDevice *device = (KPCIDevice *) driver->devices[i];
			K_USER_BUFFER EsPCIDevice *out = buffer + count;

			if (device->classCode == 6) {
				continue;
			}

			out->deviceID = device->deviceID;
			out->classCode = device->classCode;
			out->subclassCode = device->subclassCode;
			out->progIF = device->progIF;
			out->bus = device->bus;
			out->slot = device->slot;
			out->function = device->function;
			out->interruptPin = device->interruptPin;
			out->interruptLine = device->interruptLine;

			for (uintptr_t i = 0; i < 6; i++) {
				out->baseAddressesSizes[i] = device->baseAddressesSizes[i];
				out->baseAddresses[i] = device->baseAddresses[i];
			}

			if (device->implementingDriver) {
				size_t nameBytes = device->implementingDriver->nameBytes > sizeof(out->driverName) 
					? sizeof(out->driverName) : device->implementingDriver->nameBytes;
				EsMemoryCopy(out->driverName, device->implementingDriver->name, nameBytes);
				out->driverNameBytes = nameBytes;
			} else {
				EsMemoryCopy(out->driverName, "(none)", 6);
				out->driverNameBytes = 6;
			}

			count++;
		}
	}

	return count;
}

extern "C" void EntryPCI(KModuleInitilisationArguments *arguments) {
	arguments->driver->deviceAttached = [] (KDriver *driver, KDevice *parent) {
		if (pci) {
			KernelLog(LOG_ERROR, "PCI", "multiple PCI controllers", "EntryPCI - Attempt to register multiple PCI controllers; ignored.\n");
			return;
		}

		pci = (PCIController *) driver->NewDevice(parent, sizeof(PCIController));
		pci->driverData.i = 2; // Indicates this is a PCIController.
		if (pci) pci->Enumerate();
	};
}
