// TODO Prevent Meltdown/Spectre exploits.
// TODO Kernel debugger.
// TODO Passing data to userspace - zeroing padding bits of structures.
// TODO Per-handle permissions.
// TODO Remove file extensions?
// TODO Thread-local variables for native applications (already working under the POSIX subsystem).

#include "kernel.h"
#define IMPLEMENTATION
#include "kernel.h"

extern "C" void KernelMain() {										
	scheduler.SpawnProcess(PROCESS_KERNEL)->Start(EsLiteral("Kernel"));			// Spawn the kernel process.
	ArchInitialise(); 									// Start processors and initialise CPULocalStorage. 
	scheduler.started = true;								// Start the pre-emptive scheduler.
	// Continues in KernelInitialise.
}

void KernelInitialise() {									
	LoadDriver(DriversInitialise());							// Load the root device.
	scheduler.SpawnProcess(PROCESS_DESKTOP)->Start(EsLiteral(K_DESKTOP_EXECUTABLE));	// Spawn the desktop process.
	KThreadTerminate();									// Terminate this thread.
}

void KernelShutdown() {
	KThreadCreate("Shutdown", [] (uintptr_t) {
		scheduler.Destroy();								// Kill user processes.
		FSShutdown();									// Flush file cache and unmount filesystems.
		DriversShutdown();								// Inform drivers of shutdown.
		ArchShutdown();									// Power off the computer. 
	});
}
