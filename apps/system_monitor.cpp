#define ES_INSTANCE_TYPE Instance
#include <essence.h>

struct Instance : EsInstance {
	EsPanel *switcher;
	EsTextbox *textboxGeneralLog;
	EsListView *listViewContextSwitches;
	EsListView *listViewPCIDevices;
};

#define DISPLAY_GENERAL_LOG (3)
#define DISPLAY_CONTEXT_SWITCHES (5)
#define DISPLAY_PCI_DEVICES (6)

EsListViewColumn listViewContextSwitchesColumns[] = {
	{ EsLiteral("Time stamp (ms)"), ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED, 150 },
	{ EsLiteral("CPU"), ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED, 150 },
	{ EsLiteral("Process"), 0, 400 },
	{ EsLiteral("Thread"), 0, 150 },
	{ EsLiteral("Count"), 0, 150 },
};

EsListViewColumn listViewPCIDevicesColumns[] = {
	{ EsLiteral("Driver"), 0, 200 },
	{ EsLiteral("Device ID"), 0, 200 },
	{ EsLiteral("Class"), 0, 250 },
	{ EsLiteral("Subclass"), 0, 250 },
	{ EsLiteral("ProgIF"), 0, 150 },
	{ EsLiteral("Bus"), 0, 100 },
	{ EsLiteral("Slot"), 0, 100 },
	{ EsLiteral("Function"), 0, 100 },
	{ EsLiteral("Interrupt pin"), 0, 100 },
	{ EsLiteral("Interrupt line"), 0, 100 },
	{ EsLiteral("BAR0"), 0, 250 },
	{ EsLiteral("BAR1"), 0, 250 },
	{ EsLiteral("BAR2"), 0, 250 },
	{ EsLiteral("BAR3"), 0, 250 },
	{ EsLiteral("BAR4"), 0, 250 },
	{ EsLiteral("BAR5"), 0, 250 },
};

const EsStyle monospacedTextbox = {
	.inherit = ES_STYLE_TEXTBOX_NO_BORDER,

	.metrics = {
		.mask = ES_THEME_METRICS_FONT_FAMILY,
		.fontFamily = ES_FONT_MONOSPACED,
	},
};

const char *pciClassCodeStrings[] = {
	"Unknown",
	"Mass storage controller",
	"Network controller",
	"Display controller",
	"Multimedia controller",
	"Memory controller",
	"Bridge controller",
	"Simple communication controller",
	"Base system peripheral",
	"Input device controller",
	"Docking station",
	"Processor",
	"Serial bus controller",
	"Wireless controller",
	"Intelligent controller",
	"Satellite communication controller",
	"Encryption controller",
	"Signal processing controller",
};

const char *pciSubclassCodeStrings1[] = {
	"SCSI bus controller",
	"IDE controller",
	"Floppy disk controller",
	"IPI bus controller",
	"RAID controller",
	"ATA controller",
	"Serial ATA",
	"Serial attached SCSI",
	"Non-volatile memory controller",
};

const char *pciSubclassCodeStrings12[] = {
	"FireWire (IEEE 1394) controller",
	"ACCESS bus",
	"SSA",
	"USB controller",
	"Fibre channel",
	"SMBus",
	"InfiniBand",
	"IPMI interface",
	"SERCOS interface (IEC 61491)",
	"CANbus",
};

const char *pciProgIFStrings12_3[] = {
	"UHCI",
	"OHCI",
	"EHCI",
	"XHCI",
};

char generalLogBuffer[256 * 1024];
char contextSwitchesBuffer[2 * 1024 * 1024];
EsPCIDevice pciDevices[1024];

void UpdateDisplay(Instance *instance, int index) {
	if (index == DISPLAY_GENERAL_LOG) {
		EsPanelSwitchTo(instance->switcher, instance->textboxGeneralLog, ES_TRANSITION_NONE);
		size_t bytes = EsSyscall(ES_SYSCALL_DEBUG_COMMAND, index, (uintptr_t) generalLogBuffer, sizeof(generalLogBuffer), 0);
		EsTextboxSelectAll(instance->textboxGeneralLog);
		EsTextboxInsert(instance->textboxGeneralLog, generalLogBuffer, bytes);
		EsTextboxEnsureCaretVisible(instance->textboxGeneralLog, false);
	} else if (index == DISPLAY_CONTEXT_SWITCHES) {
		EsPanelSwitchTo(instance->switcher, instance->listViewContextSwitches, ES_TRANSITION_NONE);
		size_t bytes = EsSyscall(ES_SYSCALL_DEBUG_COMMAND, index, (uintptr_t) contextSwitchesBuffer, sizeof(contextSwitchesBuffer), 0);
		EsListViewRemoveAll(instance->listViewContextSwitches, 0);
		EsListViewInsert(instance->listViewContextSwitches, 0, 0, bytes / sizeof(EsSchedulerLogEntry) - 1);
	} else if (index == DISPLAY_PCI_DEVICES) {
		EsPanelSwitchTo(instance->switcher, instance->listViewPCIDevices, ES_TRANSITION_NONE);
		size_t count = EsSyscall(ES_SYSCALL_DEBUG_COMMAND, index, (uintptr_t) pciDevices, sizeof(pciDevices) / sizeof(pciDevices[0]), 0);
		EsListViewRemoveAll(instance->listViewPCIDevices, 0);
		EsListViewInsert(instance->listViewPCIDevices, 0, 0, count - 1);
	}
}

#define GET_CONTENT(...) message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, __VA_ARGS__)

int ListViewContextSwitchesCallback(EsElement *, EsMessage *message) {
	if (message->type == ES_MSG_LIST_VIEW_GET_CONTENT) {
		int column = message->getContent.column, index = message->getContent.index.i;

		EsSchedulerLogEntry *entry = ((EsSchedulerLogEntry *) contextSwitchesBuffer) + index;

		if (column == 0) {
			GET_CONTENT("%d", entry->timeMs);
		} else if (column == 1) {
			GET_CONTENT("%d", entry->cpu);
		} else if (column == 2) {
			GET_CONTENT("%s", entry->pathBytes, entry->path);
		} else if (column == 3) {
			GET_CONTENT("%s", entry->threadBytes, entry->thread);
		} else if (column == 4) {
			GET_CONTENT("%d", entry->count);
		}

		return ES_HANDLED;
	}

	return 0;
}

int ListViewPCIDevicesCallback(EsElement *, EsMessage *message) {
	if (message->type == ES_MSG_LIST_VIEW_GET_CONTENT) {
		int column = message->getContent.column, index = message->getContent.index.i;

		EsPCIDevice *entry = pciDevices + index;

		if (column == 0) {
			GET_CONTENT("%s", entry->driverNameBytes, entry->driverName);
		} else if (column == 1) {
			GET_CONTENT("%x", entry->deviceID);
		} else if (column == 2) {
			const char *string = entry->classCode < sizeof(pciClassCodeStrings) / sizeof(pciClassCodeStrings[0]) 
				? pciClassCodeStrings[entry->classCode] : "Unknown";
			GET_CONTENT("%d - %z", entry->classCode, string);
		} else if (column == 3) {
			const char *string = 
				entry->classCode == 1  && entry->subclassCode < sizeof(pciSubclassCodeStrings1)  / sizeof(const char *) 
				? pciSubclassCodeStrings1 [entry->subclassCode] 
				: entry->classCode == 12 && entry->subclassCode < sizeof(pciSubclassCodeStrings12) / sizeof(const char *)
				? pciSubclassCodeStrings12[entry->subclassCode] : "";
			GET_CONTENT("%d%z%z", entry->subclassCode, *string ? " - " : "", string);
		} else if (column == 4) {
			const char *string = 
				entry->classCode == 12 && entry->subclassCode == 3 && entry->progIF / 0x10 < sizeof(pciProgIFStrings12_3) / sizeof(const char *)
				? pciProgIFStrings12_3[entry->progIF / 0x10] : "";
			GET_CONTENT("%d%z%z", entry->progIF, *string ? " - " : "", string);
		} else if (column == 5) {
			GET_CONTENT("%d", entry->bus);
		} else if (column == 6) {
			GET_CONTENT("%d", entry->slot);
		} else if (column == 7) {
			GET_CONTENT("%d", entry->function);
		} else if (column == 8) {
			GET_CONTENT("%d", entry->interruptPin);
		} else if (column == 9) {
			GET_CONTENT("%d", entry->interruptLine);
		} else if (column == 10) {
			GET_CONTENT("%x, %D", entry->baseAddresses[0], entry->baseAddressesSizes[0]);
		} else if (column == 11) {
			GET_CONTENT("%x, %D", entry->baseAddresses[1], entry->baseAddressesSizes[1]);
		} else if (column == 12) {
			GET_CONTENT("%x, %D", entry->baseAddresses[2], entry->baseAddressesSizes[2]);
		} else if (column == 13) {
			GET_CONTENT("%x, %D", entry->baseAddresses[3], entry->baseAddressesSizes[3]);
		} else if (column == 14) {
			GET_CONTENT("%x, %D", entry->baseAddresses[4], entry->baseAddressesSizes[4]);
		} else if (column == 15) {
			GET_CONTENT("%x, %D", entry->baseAddresses[5], entry->baseAddressesSizes[5]);
		} else {
			EsAssert(false);
		}

		return ES_HANDLED;
	}

	return 0;
}

void ProcessApplicationMessage(EsMessage *message) {
	if (message->type == ES_MSG_INSTANCE_CREATE) {
		Instance *instance = EsInstanceCreate(message, "System Monitor");
		EsWindow *window = instance->window;
		EsWindowSetIcon(window, ES_ICON_UTILITIES_SYSTEM_MONITOR);
		EsPanel *switcher = EsPanelCreate(window, ES_CELL_FILL | ES_PANEL_SWITCHER, ES_STYLE_PANEL_WINDOW_BACKGROUND);
		instance->switcher = switcher;

		instance->textboxGeneralLog = EsTextboxCreate(switcher, ES_TEXTBOX_MULTILINE | ES_CELL_FILL, &monospacedTextbox);
		EsElementFocus(instance->textboxGeneralLog);

		instance->listViewContextSwitches = EsListViewCreate(switcher, ES_CELL_FILL | ES_LIST_VIEW_COLUMNS);
		instance->listViewContextSwitches->userCallback = ListViewContextSwitchesCallback;
		EsListViewSetColumns(instance->listViewContextSwitches, listViewContextSwitchesColumns, sizeof(listViewContextSwitchesColumns) / sizeof(EsListViewColumn));
		EsListViewInsertGroup(instance->listViewContextSwitches, 0);

		instance->listViewPCIDevices = EsListViewCreate(switcher, ES_CELL_FILL | ES_LIST_VIEW_COLUMNS);
		instance->listViewPCIDevices->userCallback = ListViewPCIDevicesCallback;
		EsListViewSetColumns(instance->listViewPCIDevices, listViewPCIDevicesColumns, sizeof(listViewPCIDevicesColumns) / sizeof(EsListViewColumn));
		EsListViewInsertGroup(instance->listViewPCIDevices, 0);

		EsElement *toolbar = EsWindowGetToolbar(window);

		EsButton *button;

		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "General log");

		EsButtonOnCommand(button, [] (Instance *instance, EsElement *element, EsCommand *) {
			if (EsButtonGetCheck((EsButton *) element) == ES_CHECK_CHECKED) {
				UpdateDisplay(instance, DISPLAY_GENERAL_LOG);
			}
		});

		EsButtonSetCheck(button);
		
		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "Context switches");
		
		EsButtonOnCommand(button, [] (Instance *instance, EsElement *element, EsCommand *) {
			if (EsButtonGetCheck((EsButton *) element) == ES_CHECK_CHECKED) {
				UpdateDisplay(instance, DISPLAY_CONTEXT_SWITCHES);
			}
		});

		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "PCI devices");
		
		EsButtonOnCommand(button, [] (Instance *instance, EsElement *element, EsCommand *) {
			if (EsButtonGetCheck((EsButton *) element) == ES_CHECK_CHECKED) {
				UpdateDisplay(instance, DISPLAY_PCI_DEVICES);
			}
		});
	}
}

void _start() {
	_init();

	while (true) {
		ProcessApplicationMessage(EsMessageReceive());
	}
}
