#ifndef IMPLEMENTATION

// TODO Closing audio streams, and stopping DMA when no streams active on a mixer.

void AudioStreamDestroy(KAudioStream *stream);

KSpinlock audioControllersSpinlock;
KAudioController *firstAudioController;
KAudioMixer audioDefaultOutput;
bool createdAudioDefaultOutput;
KMutex audioMutex;

#else

size_t KAudioFormatGetBytesPerSample(EsAudioFormat audioFormat) {
	if (audioFormat.sampleFormat == ES_SAMPLE_FORMAT_S32LE) {
		return 4;
	} else if (audioFormat.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
		return 2;
	} else if (audioFormat.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		return 1;
	} else {
		KernelPanic("KAudioFormatGetBytesPerSample - Unsupported sample format.\n");
		return 0;
	}
}

size_t KAudioFormatGetBytesPerSampleBlock(EsAudioFormat audioFormat) {
	return KAudioFormatGetBytesPerSample(audioFormat) * audioFormat.channels;
}

void AudioSum(uint8_t *_destination, uint8_t *_source, size_t bytes, EsAudioFormat format) {
	// TODO Vectorise.

	if (format.sampleFormat == ES_SAMPLE_FORMAT_S32LE) {
		int32_t *destination = (int32_t *) _destination;
		int32_t *source = (int32_t *) _source;

		for (uintptr_t i = 0; i < bytes / sizeof(int32_t); i++) {
			destination[i] += source[i];
		}
	} else if (format.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
		int16_t *destination = (int16_t *) _destination;
		int16_t *source = (int16_t *) _source;

		for (uintptr_t i = 0; i < bytes / sizeof(int16_t); i++) {
			destination[i] += source[i];
		}
	} else if (format.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		uint8_t *destination = (uint8_t *) _destination;
		uint8_t *source = (uint8_t *) _source;

		for (uintptr_t i = 0; i < bytes / sizeof(uint8_t); i++) {
			destination[i] += source[i];
		}
	} else {
		KernelPanic("AudioSum - Unsupported sample format.\n");
	}
}

void AudioClear(uint8_t *buffer, size_t bytes, EsAudioFormat format) {
	if (format.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		EsMemoryFill(buffer, buffer + bytes, 0x80);
	} else {
		EsMemoryZero(buffer, bytes);
	}
}

bool KAudioFillBuffersFromMixer(uint8_t **destinations, size_t destinationCount, KAudioMixer *mixer, size_t bytesPerDestination, KEvent *timeout) {
	// TODO Use non-DMA buffer for mixing? Better cache performance?
	// TODO Set overrun flag in status.

	bool firstPass = true;
	size_t totalBytes = bytesPerDestination * destinationCount;
	size_t bytesPerSampleBlock = KAudioFormatGetBytesPerSampleBlock(mixer->format);

	for (uintptr_t i = 0; i < destinationCount; i++) {
		AudioClear(destinations[i], bytesPerDestination, mixer->format);
	}

	if (mixer->paused) {
		return true;
	}

	// uint64_t startTime = ProcessorReadTimeStamp();

	while (true) {
		mixer->mutex.Acquire();
		KAudioStream *stream = mixer->firstStream;
		bool lastPass = true;

		while (stream) {
			if (stream->mirror) {
				stream->writePointer = stream->mirror->writePointer > stream->bufferBytes ? 0 : stream->mirror->writePointer;
				stream->writePointer -= stream->writePointer % bytesPerSampleBlock;
				stream->control = stream->mirror->control;
			}

			if ((stream->control & ES_AUDIO_STREAM_RUNNING) && stream->readPointer != stream->bufferBytes) {
				size_t remainingBytes = firstPass ? totalBytes : stream->remainingBytes;
				uint32_t writePointer = stream->writePointer;
				uint32_t  readPointer = stream->readPointer;
				uint32_t   endPointer = stream->bufferBytes;
				bool            muted = stream->control & ES_AUDIO_STREAM_MUTED;

				if (firstPass) stream->destinationIndex = stream->positionWithinDestination = 0;

				uint64_t clockDelta = 0;

				while (remainingBytes) {
					if (readPointer == writePointer) {
						lastPass = false;
						break;
					}

					// Gather from the stream's circular buffer.

					uint32_t availableBytes = (readPointer > writePointer) ? (endPointer - readPointer) : (writePointer - readPointer);
					uint32_t bytesToCopy = availableBytes < remainingBytes ? availableBytes : remainingBytes;

					uint32_t leftToCopy = bytesToCopy;

					while (leftToCopy) {
						// Scatter to the device's buffers.

						uint32_t space = bytesPerDestination - stream->positionWithinDestination;
						uint32_t copy  = leftToCopy < space ? leftToCopy : space;

						if (!muted) {
							AudioSum(destinations[stream->destinationIndex] + stream->positionWithinDestination, 
									stream->buffer + readPointer, copy, mixer->format);
						}

						if (copy == space) {
							stream->destinationIndex++;
							stream->positionWithinDestination = 0;
						} else {
							stream->positionWithinDestination += copy;
						}

						leftToCopy  -= copy;
						readPointer += copy;
					}

					remainingBytes -= bytesToCopy;
					clockDelta += bytesToCopy / bytesPerSampleBlock;

					if (readPointer == endPointer && (~stream->control & ES_AUDIO_STREAM_ONESHOT)) {
						readPointer = 0;
					}
				}

				stream->remainingBytes = remainingBytes;
				stream->readPointer = readPointer;

				if (stream->mirror) {
					stream->mirror->readPointer = stream->readPointer;
					stream->mirror->clock += clockDelta;
					stream->mirror->status = stream->status;
				}

				if (remainingBytes) stream->fillBuffer.Set(false, true);
			}

			stream = stream->nextStream;
		}

		mixer->mutex.Release();
		firstPass = false;
		if (lastPass) break;

		KEvent *events[2] = { &mixer->streamsReady, timeout };
		uintptr_t index = KWaitEvents(events, timeout ? 2 : 1);
		if (index == 1) return false;
	}

	// uint64_t endTime = ProcessorReadTimeStamp();
	// EsPrint("filled buffer in %dus\n", (endTime - startTime) / (KGetTimeStampTicksPerMs() / 1000));

	return !timeout->Poll();
}

void RegisterAudioController(KDriver *, KDevice *_audioController, EsGeneric) {
	KAudioController *audioController = (KAudioController *) _audioController;
	audioControllersSpinlock.Acquire();

	if (!firstAudioController) {
		firstAudioController = audioController;
	}

	audioControllersSpinlock.Release();
}

KAudioStream *KAudioStreamOpen(EsAudioDeviceID device, size_t bufferLengthUs) {
	if (device != ES_AUDIO_DEFAULT_OUTPUT
			|| bufferLengthUs < 1000 /* 1ms */
			|| bufferLengthUs > 100 * 1000 * 1000 /* 100s */) {
		return nullptr;
	}

	// TODO Select the mixer based on the device.
	KAudioMixer *mixer = &audioDefaultOutput;

	mixer->mutex.Acquire();

	if (!createdAudioDefaultOutput) {
		mixer->streamsReady.autoReset = true;
		mixer->controller = firstAudioController;

		if (!firstAudioController || !firstAudioController->openOutput || !firstAudioController->openOutput(firstAudioController, mixer)) {
			KernelLog(LOG_ERROR, "Audio", "create mixer error", "KAudioStreamOpen - Could not create the default output mixer.\n");
			return nullptr;
		}

		createdAudioDefaultOutput = true;
	} else {
		if (mixer->paused && mixer->controller->setPaused) {
			mixer->controller->setPaused(mixer->controller, mixer, false);
			mixer->paused = false;
		}
	}

	mixer->mutex.Release();

	KAudioStream *stream = (KAudioStream *) EsHeapAllocate(sizeof(KAudioStream), true, K_FIXED);

	if (!stream) {
		return nullptr;
	}

	stream->format = mixer->format;
	stream->fillBuffer.autoReset = true;
	stream->handles = 1;
	
	// TODO Update latency of mixer if necessary - **how should this be locked?!?!?**

	stream->bufferBytes = ((uint64_t) stream->format.sampleRate * bufferLengthUs / 1000000) * KAudioFormatGetBytesPerSampleBlock(stream->format);
	stream->bufferSharedRegion = MMSharedCreateRegion(stream->bufferBytes, true);

	if (!stream->bufferSharedRegion) {
		EsHeapFree(stream, sizeof(KAudioStream), K_FIXED);
		return nullptr;
	}

	stream->buffer = (uint8_t *) MMMapShared(MMGetKernelSpace(), stream->bufferSharedRegion, 0, stream->bufferBytes);

	if (!stream->buffer) {
		CloseHandleToObject(stream->bufferSharedRegion, KERNEL_OBJECT_SHMEM);
		EsHeapFree(stream, sizeof(KAudioStream), K_FIXED);
		return nullptr;
	}

	mixer->mutex.Acquire();
	stream->mixer = mixer;
	stream->nextStream = mixer->firstStream;
	mixer->firstStream = stream;
	mixer->mutex.Release();

	return stream;
}

void AudioStreamDestroy(KAudioStream *stream) {
	KAudioMixer *mixer = stream->mixer;

	// Remove the stream from the mixer.

	mixer->mutex.Acquire();

	{
		KAudioStream **pointer = &mixer->firstStream;
		KAudioStream *current = *pointer;
		bool found = false;

		while (current) {
			if (current == stream) {
				*pointer = current->nextStream;
				found = true;
				break;
			}

			pointer = &current->nextStream;
			current = current->nextStream;
		}

		if (!found) KernelPanic("AudioStreamDestroy - Could not find stream %x in mixer.\n", stream);
	}

	if (!mixer->firstStream && !mixer->paused && mixer->controller->setPaused) {
		mixer->controller->setPaused(mixer->controller, mixer, true);
		mixer->paused = true;
	}

	mixer->mutex.Release();

	// Unmap and close the shared memory regions.

	MMFree(kernelMMSpace, stream->buffer);
	CloseHandleToObject(stream->bufferSharedRegion, KERNEL_OBJECT_SHMEM);

	if (stream->mirror) {
		MMFree(kernelMMSpace, stream->mirror);
		CloseHandleToObject(stream->mirrorSharedRegion, KERNEL_OBJECT_SHMEM);
	}

	// Deallocate the stream object.

	EsHeapFree(stream, sizeof(KAudioStream), K_FIXED);
}

extern "C" void EntryAudio(KModuleInitilisationArguments *arguments) {
	arguments->driver->deviceRegistered = RegisterAudioController;
}

#endif
