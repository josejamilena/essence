// TODO Features:
// 	- Handling errors creating files (prevent further file system operations).
// 	- Limiting the size of the directory/node cache.
// 	- Implement FSShutdown.
// 	- Directory monitors and ES_FILE_CONTROL_NOTIFY_MONITORS. 
// 	- Implement ES_SYSTEM_SNAPSHOT_DRIVES and ES_SYSCALL_OPEN_NODE_RELATIVE.
// 	- Accessing other file systems with FSNodeOpen.
//
// TODO Drivers:
// 	- Parsing GPT partition tables.
// 	- Get NTFS driver working again.
//
// TODO Allocate nodes/directory entries from arenas.

#ifndef IMPLEMENTATION

#define NODE_MAX_ACCESSORS (16777216)

// KNode flags:
#define NODE_HAS_EXCLUSIVE_WRITER (1 << 0)
#define NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES (1 << 1)
#define NODE_CREATED_ON_FILE_SYSTEM (1 << 2)
#define NODE_DELETED (1 << 3)
#define NODE_MODIFIED (1 << 4)

struct FSDirectoryEntry : KNodeMetadata {
	AVLItem<FSDirectoryEntry> item; // item.key.longKey contains the entry's name.
	struct FSDirectory *parent; // The directory containing this entry.
	KNode *node; // nullptr if the node hasn't been loaded.
	char inlineName[16]; // Store the name of the entry inline if it is small enough.
	// Followed by driver data.
};

struct FSDirectory : KNode {
	AVLTree<FSDirectoryEntry> entries;
	size_t entryCount;
};

struct FSFile : KNode {
	int32_t countWrite /* negative indicates non-shared readers */, blockResize;
	EsFileOffset fsFileSize; // Files are lazily resized; this is the size the file system thinks the file is.
	EsFileOffset fsZeroAfter; // This is the smallest size the file has reached without telling the file system.
	CCSpace cache;
	KWriterLock resizeLock; // Take exclusive for resizing or flushing.
};

struct {
	KFileSystem **fileSystems;
	KWriterLock fileSystemsLock;

	KFileSystem *bootFileSystem;
	KEvent foundBootFileSystemEvent;

	KSpinlock updateNodeHandles;

	KDriver *driver;

	bool shutdown;
} fs;

EsError FSNodeOpenHandle(KNode *node, uint32_t flags);
void FSNodeCloseHandle(KNode *node, uint32_t flags);
EsError FSNodeDelete(KNode *node);
EsError FSNodeMove(KNode *node, KNode *destination, const char *newName, size_t nameNameBytes);
EsError FSFileResize(KNode *node, EsFileOffset newSizeBytes);
ptrdiff_t FSDirectoryEnumerateChildren(KNode *node, K_USER_BUFFER EsDirectoryChild *buffer, size_t bufferSize);
EsError FSFileControl(KNode *node, uint32_t flags);

#else

EsFileOffset FSNodeGetTotalSize(KNode *node) {
	return node->directoryEntry->totalSize;
}

bool FSCheckPathForIllegalCharacters(const char *path, size_t pathBytes) {
	uintptr_t i = 0;

	while (i < pathBytes) {
		char c = path[i];

		// Control characters are not allowed.

		if ((c >= 0x00 && c < 0x20) || c == 0x7F) {
			return false;
		}

		// UTF-8 sequences must be valid.

		size_t length;
		UTF8_LENGTH_CHAR(&c, length);
		if (!length || i + length > pathBytes) return false;

		i++, length--;

		while (length) {
			if ((path[i] & 0xC0) != 0x80) return false;
			i++, length--;
		}
	}

	return true;
}

//////////////////////////////////////////
// Accessing files.
//////////////////////////////////////////

EsError FSReadIntoCache(CCSpace *fileCache, void *buffer, EsFileOffset offset, EsFileOffset count) {
	FSFile *node = EsContainerOf(FSFile, cache, fileCache);

	node->writerLock.Take(K_LOCK_SHARED);
	EsDefer(node->writerLock.Return(K_LOCK_SHARED));

	if (node->flags & NODE_DELETED) {
		return ES_ERROR_NODE_DELETED;
	}

	if (offset > node->directoryEntry->totalSize) {
		KernelPanic("FSReadIntoCache - Read out of bounds in node %x.\n", node); 
	}

	if (node->fsZeroAfter < offset + count) {
		if (offset >= node->fsZeroAfter) {
			EsMemoryZero(buffer, count);
		} else {
			if (~node->flags & NODE_CREATED_ON_FILE_SYSTEM) {
				KernelPanic("FSReadIntoCache - Node %x has not been created on the file system.\n", node); 
			}

			size_t realBytes = node->fsZeroAfter - offset, fakeBytes = count - realBytes;
			EsMemoryZero((uint8_t *) buffer + realBytes, fakeBytes);
			count = node->fileSystem->read(node, buffer, offset, realBytes);
		}
	} else {
		if (~node->flags & NODE_CREATED_ON_FILE_SYSTEM) {
			KernelPanic("FSReadIntoCache - Node %x has not been created on the file system.\n", node); 
		}

		count = node->fileSystem->read(node, buffer, offset, count);

		if (ES_CHECK_ERROR(count)) {
			node->error = count;
		}
	}

	return ES_CHECK_ERROR(count) ? count : ES_SUCCESS;
}

EsError FSFileCreateAndResizeOnFileSystem(FSFile *node, EsFileOffset fileSize) {
	node->writerLock.AssertExclusive();

	FSDirectoryEntry *entry = node->directoryEntry;

	if (node->flags & NODE_DELETED) {
		return ES_ERROR_NODE_DELETED;
	}

	if (~node->flags & NODE_CREATED_ON_FILE_SYSTEM) {
		entry->parent->writerLock.Take(K_LOCK_EXCLUSIVE);

		EsError error = ES_SUCCESS;

		if (~entry->parent->flags & NODE_CREATED_ON_FILE_SYSTEM) {
			// TODO Get the node error mark?
			error = ES_ERROR_UNKNOWN;
		}

		if (error == ES_SUCCESS) {
			error = node->fileSystem->create((const char *) entry->item.key.longKey, entry->item.key.longKeyBytes, 
					ES_NODE_FILE, entry->parent, node, entry + 1);
		}

		if (error == ES_SUCCESS) {
			__sync_fetch_and_or(&node->flags, NODE_CREATED_ON_FILE_SYSTEM);
		} else {
			// TODO Mark the node with an error.
		}

		entry->parent->writerLock.Return(K_LOCK_EXCLUSIVE);

		if (error != ES_SUCCESS) {
			return error;
		}
	}

	if (node->fsFileSize != fileSize || node->fsZeroAfter != fileSize) {
		// Resize the file on the file system to match the cache size.
		EsError error = ES_ERROR_UNKNOWN;
		entry->parent->writerLock.Take(K_LOCK_EXCLUSIVE);

		if (node->fsZeroAfter != node->fsFileSize) {
			// TODO Combined truncate-and-grow file operation.
			node->fsFileSize = node->fileSystem->resize(node, node->fsZeroAfter, error);
		}

		// TODO Hint about where to zero upto - since we'll likely be about to write over the sectors!
		node->fsFileSize = node->fileSystem->resize(node, fileSize, error);
		entry->parent->writerLock.Return(K_LOCK_EXCLUSIVE);

		if (node->fsFileSize != fileSize) {
			return ES_ERROR_COULD_NOT_RESIZE_FILE;
		}

		node->fsZeroAfter = fileSize;
	}

	return ES_SUCCESS;
}

EsError FSWriteFromCache(CCSpace *fileCache, const void *buffer, EsFileOffset offset, EsFileOffset count) {
	FSFile *node = EsContainerOf(FSFile, cache, fileCache);

	node->writerLock.Take(K_LOCK_EXCLUSIVE);
	EsDefer(node->writerLock.Return(K_LOCK_EXCLUSIVE));

	FSDirectoryEntry *entry = node->directoryEntry;
	volatile EsFileOffset fileSize = entry->totalSize;

	EsError error = FSFileCreateAndResizeOnFileSystem(node, fileSize);

	if (error != ES_SUCCESS) {
		return error;
	}

	if (offset > fileSize) {
		KernelPanic("VFSWriteFromCache - Write out of bounds in node %x.\n", node); 
	}

	if (count > fileSize - offset) {
		count = fileSize - offset;
	}

	if (node->flags & NODE_DELETED) {
		return ES_ERROR_NODE_DELETED;
	}

	count = node->fileSystem->write(node, buffer, offset, count);

	if (ES_CHECK_ERROR(count)) {
		node->error = count;
	}

	return ES_CHECK_ERROR(count) ? count : ES_SUCCESS;
}

const CCSpaceCallbacks fsFileCacheCallbacks = {
	.readInto = FSReadIntoCache,
	.writeFrom = FSWriteFromCache,
};

ptrdiff_t FSFileReadSync(KNode *node, K_USER_BUFFER void *buffer, EsFileOffset offset, EsFileOffset bytes, uint32_t accessFlags) {
	FSFile *file = (FSFile *) node;
	file->resizeLock.Take(K_LOCK_SHARED);
	EsDefer(file->resizeLock.Return(K_LOCK_SHARED));

	if (offset > file->directoryEntry->totalSize) return ES_ERROR_ACCESS_NOT_WITHIN_FILE_BOUNDS;
	if (bytes > file->directoryEntry->totalSize - offset) bytes = file->directoryEntry->totalSize - offset;
	if (!bytes) return 0;

	EsError error = CCSpaceAccess(&file->cache, buffer, offset, bytes, 
			CC_ACCESS_READ | ((accessFlags & FS_FILE_ACCESS_USER_BUFFER_MAPPED) ? CC_ACCESS_USER_BUFFER_MAPPED : 0));
	return error == ES_SUCCESS ? bytes : error;
}

void _FSFileResize(FSFile *file, EsFileOffset newSize) {
	file->resizeLock.AssertExclusive();

	EsFileOffsetDifference delta = newSize - file->directoryEntry->totalSize;

	if (!delta) {
		return;
	}

	if (delta < 0) {
		// Truncate the space first, so that any pending write-backs past this point can complete.
		CCSpaceTruncate(&file->cache, newSize);
	}

	// No more writes-back can be issued past newSize until the resize lock is returned. Since:
	// - CCSpaceTruncate waits for any queued writes past newSize to finish.
	// - FSFileWriteSync waits for shared access on the resize lock before it can queue any more writes.
	// This means we are safe to possible decrease sizing information.

	if (newSize < file->fsZeroAfter) {
		file->fsZeroAfter = newSize;
	}

	// Take the move lock before we write to the directoryEntry,
	// because FSNodeMove needs to also update ancestors with the file's size when it is moved.
	file->fileSystem->moveMutex.Acquire(); 

	// We'll get the filesystem to resize the file during write-back.
	file->directoryEntry->totalSize = newSize;

	KNode *ancestor = file->directoryEntry->parent;

	while (ancestor) {
		ancestor->directoryEntry->totalSize += delta;
		ancestor = ancestor->directoryEntry->parent;
	}

	file->fileSystem->moveMutex.Release();
}

EsError FSFileResize(KNode *node, EsFileOffset newSize) {
	if (newSize > 1UL << 60) {
		return ES_ERROR_INSUFFICIENT_RESOURCES;
	}

	if (node->directoryEntry->type != ES_NODE_FILE) {
		KernelPanic("FSFileResize - Node %x is not a file.\n", node);
	}

	FSFile *file = (FSFile *) node;
	EsError error = ES_SUCCESS;
	file->resizeLock.Take(K_LOCK_EXCLUSIVE);

	if (file->blockResize) {
		error = ES_ERROR_FILE_IN_EXCLUSIVE_USE;
	} else if (!file->fileSystem->resize) {
		error = ES_ERROR_FILE_ON_READ_ONLY_VOLUME;
	} else {
		_FSFileResize(file, newSize);
	}

	file->resizeLock.Return(K_LOCK_EXCLUSIVE);
	return error;
}

ptrdiff_t FSFileWriteSync(KNode *node, K_USER_BUFFER const void *buffer, EsFileOffset offset, EsFileOffset bytes, uint32_t flags) {
	if (offset + bytes > node->directoryEntry->totalSize) {
		if (ES_SUCCESS != FSFileResize(node, offset + bytes)) {
			return ES_ERROR_COULD_NOT_RESIZE_FILE;
		}
	}

	FSFile *file = (FSFile *) node;
	file->resizeLock.Take(K_LOCK_SHARED);
	EsDefer(file->resizeLock.Return(K_LOCK_SHARED));

	if (!file->fileSystem->write) return ES_ERROR_FILE_ON_READ_ONLY_VOLUME;
	if (offset > file->directoryEntry->totalSize) return ES_ERROR_ACCESS_NOT_WITHIN_FILE_BOUNDS;
	if (bytes > file->directoryEntry->totalSize - offset) bytes = file->directoryEntry->totalSize - offset;
	if (!bytes) return 0;

	__sync_fetch_and_or(&file->flags, NODE_MODIFIED);

	EsError error = CCSpaceAccess(&file->cache, (void *) buffer, offset, bytes, 
			CC_ACCESS_WRITE | ((flags & FS_FILE_ACCESS_USER_BUFFER_MAPPED) ? CC_ACCESS_USER_BUFFER_MAPPED : 0));
	return error == ES_SUCCESS ? bytes : error;
}

EsError FSFileControl(KNode *node, uint32_t flags) {
	FSFile *file = (FSFile *) node;

	if (flags & ES_FILE_CONTROL_FLUSH) {
		file->resizeLock.Take(K_LOCK_EXCLUSIVE);
		EsDefer(file->resizeLock.Return(K_LOCK_EXCLUSIVE));

		CCSpaceFlush(&file->cache);

		file->writerLock.Take(K_LOCK_EXCLUSIVE);
		EsDefer(file->writerLock.Return(K_LOCK_EXCLUSIVE));

		EsError error = FSFileCreateAndResizeOnFileSystem(file, file->directoryEntry->totalSize);
		if (error != ES_SUCCESS) return error;

		if (file->fileSystem->sync) {
			// TODO Should we also sync the parent?
			FSDirectory *parent = file->directoryEntry->parent;

			if (parent) {
				parent->writerLock.Take(K_LOCK_EXCLUSIVE);
				file->fileSystem->sync(parent, file);
				parent->writerLock.Return(K_LOCK_EXCLUSIVE);
			}
		}

		if (file->error != ES_SUCCESS) {
			EsError error = file->error;
			file->error = ES_SUCCESS;
			return error;
		}
	}

	return ES_SUCCESS;
}

//////////////////////////////////////////
// Directories.
//////////////////////////////////////////

EsError FSNodeDelete(KNode *node) {
	EsError error = ES_SUCCESS;

	FSDirectoryEntry *entry = node->directoryEntry;
	FSDirectory *parent = entry->parent;
	FSFile *file = entry->type == ES_NODE_FILE ? (FSFile *) node : nullptr;

	if (file) file->resizeLock.Take(K_LOCK_EXCLUSIVE);
	node->writerLock.Take(K_LOCK_EXCLUSIVE);
	parent->writerLock.Take(K_LOCK_EXCLUSIVE);

	if (file && file->blockResize) {
		error = ES_ERROR_FILE_IN_EXCLUSIVE_USE;
	} else if (node->flags & NODE_DELETED) {
		error = ES_ERROR_NODE_DELETED;
	} else if (entry->type == ES_NODE_DIRECTORY && ((FSDirectory *) node)->entryCount) {
		error = ES_ERROR_DIRECTORY_NOT_EMPTY;
	} else if (!node->fileSystem->remove) {
		error = ES_ERROR_FILE_ON_READ_ONLY_VOLUME;
	} else if (node->flags & NODE_CREATED_ON_FILE_SYSTEM) {
		error = node->fileSystem->remove(parent, node);
	}

	if (error == ES_SUCCESS) {
		if (file) _FSFileResize(file, 0);
		__sync_fetch_and_or(&node->flags, NODE_DELETED);
		TreeRemove(&parent->entries, &entry->item);
		parent->entryCount--;
	}

	parent->writerLock.Return(K_LOCK_EXCLUSIVE);
	node->writerLock.Return(K_LOCK_EXCLUSIVE);
	if (file) file->resizeLock.Return(K_LOCK_EXCLUSIVE);

	return error;
}

EsError FSNodeMove(KNode *node, KNode *_newParent, const char *newName, size_t newNameBytes) {
	if (!FSCheckPathForIllegalCharacters(newName, newNameBytes)) {
		return ES_ERROR_ILLEGAL_PATH;
	}

	FSDirectoryEntry *entry = node->directoryEntry;
	FSDirectory *newParent = (FSDirectory *) _newParent;
	FSDirectory *oldParent = entry->parent;

	// Check the move is valid.

	if (newParent->directoryEntry->type != ES_NODE_DIRECTORY) {
		return ES_ERROR_TARGET_INVALID_TYPE;
	}

	if (!oldParent || oldParent->fileSystem != newParent->fileSystem || oldParent->fileSystem != node->fileSystem) {
		return ES_ERROR_VOLUME_MISMATCH;
	}

	if (!newNameBytes || newNameBytes > ES_MAX_DIRECTORY_CHILD_NAME_LENGTH) {
		return ES_ERROR_INVALID_NAME;
	}

	for (uintptr_t i = 0; i < newNameBytes; i++) {
		if (newName[i] == '/') {
			return ES_ERROR_INVALID_NAME;
		}
	}

	if (!node->fileSystem->move) {
		return ES_ERROR_FILE_ON_READ_ONLY_VOLUME;
	}

	EsError error = ES_SUCCESS;
	bool alreadyExists = false;
	void *newKeyBuffer = nullptr;

	KWriterLock *locks[] = { &node->writerLock, &oldParent->writerLock, &newParent->writerLock };
	KWriterLockTakeMultiple(locks, oldParent == newParent ? 2 : 3, K_LOCK_EXCLUSIVE);

	node->fileSystem->moveMutex.Acquire();
	EsDefer(node->fileSystem->moveMutex.Release());

	KNode *newAncestor = newParent, *oldAncestor;

	while (newAncestor) {
		if (newAncestor == node) {
			// We are trying to move this node into a folder within itself.
			error = ES_ERROR_TARGET_WITHIN_SOURCE;
			goto fail;
		}

		newAncestor = newAncestor->directoryEntry->parent;
	}

	if ((node->flags | newParent->flags) & NODE_DELETED) {
		error = ES_ERROR_NODE_DELETED;
		goto fail;
	}

	// Check a node with the same name doesn't already exist in the new directory.

	alreadyExists = TreeFind(&newParent->entries, MakeLongKey(newName, newNameBytes), TREE_SEARCH_EXACT);

	if (!alreadyExists && (~newParent->flags & NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES)) {
		// The entry is not cached; load it from the file system.
		node->fileSystem->scan(newName, newNameBytes, newParent);
		alreadyExists = TreeFind(&newParent->entries, MakeLongKey(newName, newNameBytes), TREE_SEARCH_EXACT);
	}

	if (alreadyExists) {
		error = ES_ERROR_FILE_ALREADY_EXISTS;
		goto fail;
	}

	// Allocate a buffer for the new key before we try to do anything permanent...

	newKeyBuffer = nullptr;

	if (newNameBytes > sizeof(entry->inlineName)) {
		newKeyBuffer = EsHeapAllocate(newNameBytes, false, K_FIXED);

		if (!newKeyBuffer) {
			error = ES_ERROR_INSUFFICIENT_RESOURCES;
			goto fail;
		}
	}

	// Move the node on the file system, if it has been created.

	if (entry->node && (entry->node->flags & NODE_CREATED_ON_FILE_SYSTEM)) {
		error = node->fileSystem->move(oldParent, node, newParent, newName, newNameBytes);

		if (error != ES_SUCCESS) {
			goto fail;
		}
	}

	// Update the node's parent in our cache.

	entry->parent = newParent;
	
	TreeRemove(&oldParent->entries, &entry->item);

	entry->item.key.longKey = newKeyBuffer ?: entry->inlineName;
	EsMemoryCopy(entry->item.key.longKey, newName, newNameBytes);
	entry->item.key.longKeyBytes = newNameBytes;

	TreeInsert(&newParent->entries, &entry->item, entry, entry->item.key);

	oldParent->entryCount--;

	if (oldParent->directoryEntry->directoryChildren != ES_DIRECTORY_CHILDREN_UNKNOWN) {
		oldParent->directoryEntry->directoryChildren--;
	}

	newParent->entryCount++;

	if (newParent->directoryEntry->directoryChildren != ES_DIRECTORY_CHILDREN_UNKNOWN) {
		newParent->directoryEntry->directoryChildren++;
	}

	// Move the size of the node from the old to the new ancestors.

	oldAncestor = oldParent;

	while (oldAncestor) {
		oldAncestor->directoryEntry->totalSize -= entry->totalSize;
		oldAncestor = oldAncestor->directoryEntry->parent;
	}

	newAncestor = newParent;

	while (newAncestor) {
		newAncestor->directoryEntry->totalSize += entry->totalSize;
		newAncestor = newAncestor->directoryEntry->parent;
	}

	fail:;

	if (error != ES_SUCCESS) {
		if (newKeyBuffer) {
			EsHeapFree(newKeyBuffer, newNameBytes, K_FIXED);
		}
	}

	if (oldParent != newParent) {
		oldParent->writerLock.Return(K_LOCK_EXCLUSIVE);
	}

	newParent->writerLock.Return(K_LOCK_EXCLUSIVE);
	node->writerLock.Return(K_LOCK_EXCLUSIVE);

	return error;
}

void _FSDirectoryEnumerateChildrenVisit(AVLItem<FSDirectoryEntry> *item, K_USER_BUFFER EsDirectoryChild *buffer, size_t bufferSize, uintptr_t *position) {
	if (!item || *position == bufferSize) {
		return;
	}

	FSDirectoryEntry *entry = item->thisItem;
	EsDirectoryChild *output = buffer + *position;
	*position = *position + 1;

	if (entry->node && (entry->node->flags & NODE_DELETED)) {
		KernelPanic("_FSDirectoryEnumerateChildrenVisit - Deleted node %x found in directory tree.\n");
	}

	size_t nameBytes = entry->item.key.longKeyBytes > ES_MAX_DIRECTORY_CHILD_NAME_LENGTH ? ES_MAX_DIRECTORY_CHILD_NAME_LENGTH : entry->item.key.longKeyBytes;
	EsMemoryCopy(output->name, entry->item.key.longKey, nameBytes);
	output->type = entry->type;
	output->fileSize = entry->totalSize;
	output->directoryChildren = entry->directoryChildren;
	output->nameBytes = nameBytes;

	_FSDirectoryEnumerateChildrenVisit(item->children[0], buffer, bufferSize, position);
	_FSDirectoryEnumerateChildrenVisit(item->children[1], buffer, bufferSize, position);
}

ptrdiff_t FSDirectoryEnumerateChildren(KNode *node, K_USER_BUFFER EsDirectoryChild *buffer, size_t bufferSize) {
	// uint64_t start = ProcessorReadTimeStamp();

	if (node->directoryEntry->type != ES_NODE_DIRECTORY) {
		KernelPanic("FSDirectoryEnumerateChildren - Node %x is not a directory.\n", node);
	}

	FSDirectory *directory = (FSDirectory *) node;

	// I think it's safe to modify the user's buffer with this lock.
	directory->writerLock.Take(K_LOCK_EXCLUSIVE);

	if (~directory->flags & NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES) {
		EsError error = directory->fileSystem->enumerate(directory);

		if (error != ES_SUCCESS) {
			directory->writerLock.Return(K_LOCK_EXCLUSIVE);
			return error;
		}

		__sync_fetch_and_or(&directory->flags, NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES);
		directory->directoryEntry->directoryChildren = directory->entryCount;
	}

	uintptr_t position = 0;
	_FSDirectoryEnumerateChildrenVisit(directory->entries.root, buffer, bufferSize, &position);

	directory->writerLock.Return(K_LOCK_EXCLUSIVE);

	// uint64_t end = ProcessorReadTimeStamp();
	// EsPrint("FSDirectoryEnumerateChildren took %dmcs for %d items.\n", (end - start) / KGetTimeStampTicksPerUs(), position);

	return position;
}

void FSNodeFree(KNode *node) {
	FSDirectoryEntry *entry = node->directoryEntry;

	if (entry->node != node) {
		KernelPanic("FSNodeFree - FSDirectoryEntry node mismatch for node %x.\n", node);
	}

	if (entry->type == ES_NODE_FILE) {
		CCSpaceDestroy(&((FSFile *) node)->cache);
	}

	EsHeapFree(node, entry->type == ES_NODE_DIRECTORY ? sizeof(FSDirectory) : sizeof(FSFile), K_FIXED);
	entry->node = nullptr;
}

void FSNodeCreate(FSDirectory *parent, const char *name, size_t nameBytes, EsNodeType type) {
	parent->writerLock.AssertExclusive();
	KFileSystem *fileSystem = parent->fileSystem;

	if (!fileSystem->create) {
		// Read-only file system.
		return;
	}

	KNodeMetadata metadata = {};
	metadata.type = type;

	KNode *node;

	if (ES_SUCCESS != FSDirectoryEntryFound(parent, &metadata, 
			nullptr, name, nameBytes, false, &node)) {
		return;
	}

	if (parent->directoryEntry->directoryChildren != ES_DIRECTORY_CHILDREN_UNKNOWN) {
		parent->directoryEntry->directoryChildren++;
	}
	
	// Only create directories immediately; files are created in FSWriteFromCache.

	if (type != ES_NODE_FILE) {
		if (ES_SUCCESS == fileSystem->create(name, nameBytes, type, parent, node, node->directoryEntry + 1)) {
			__sync_fetch_and_or(&node->flags, NODE_CREATED_ON_FILE_SYSTEM);
		} else {
			// TODO Mark the node with an error.
		}
	}
}

EsError FSDirectoryEntryAllocateNode(FSDirectoryEntry *entry, KFileSystem *fileSystem, bool createdOnFileSystem) {
	KNode *node = (KNode *) EsHeapAllocate(entry->type == ES_NODE_DIRECTORY ? sizeof(FSDirectory) : sizeof(FSFile), true, K_FIXED);

	if (!node) {
		return ES_ERROR_INSUFFICIENT_RESOURCES;
	}

	if (entry->type == ES_NODE_DIRECTORY) {
		FSDirectory *directory = (FSDirectory *) node;
		directory->entries.longKeys = true;

		if (!createdOnFileSystem) {
			// We just created the directory, so we've definitely got all the entries.
			directory->flags |= NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES;
		}
	} else if (entry->type == ES_NODE_FILE) {
		FSFile *file = (FSFile *) node;
		file->fsFileSize = entry->totalSize;
		file->fsZeroAfter = entry->totalSize;
		file->cache.callbacks = &fsFileCacheCallbacks;

		if (!CCSpaceInitialise(&file->cache)) {
			EsHeapFree(node, 0, K_FIXED);
			return ES_ERROR_INSUFFICIENT_RESOURCES;
		}
	}

	static uint64_t nextNodeID = 1;
	node->id = __sync_fetch_and_add(&nextNodeID, 1);

	if (createdOnFileSystem) {
		node->flags |= NODE_CREATED_ON_FILE_SYSTEM;
	}

	node->directoryEntry = entry;
	node->fileSystem = fileSystem;
	node->error = ES_SUCCESS;
	entry->node = node;
	return ES_SUCCESS;
}

EsError FSDirectoryEntryFound(KNode *_parent, KNodeMetadata *metadata, 
		const void *driverData, const void *name, size_t nameBytes,
		bool update, KNode **node, KNodeMetadata **outMetadata) {
	FSDirectory *parent = (FSDirectory *) _parent;
	size_t driverDataBytes = parent->fileSystem->directoryEntryDataBytes;
	parent->writerLock.AssertExclusive();

	AVLItem<FSDirectoryEntry> *existingEntry = TreeFind(&parent->entries, MakeLongKey(name, nameBytes), TREE_SEARCH_EXACT);

	if (existingEntry) {
		if (!driverData) {
			KernelPanic("FSDirectoryEntryFound - Directory entry '%s' in %x already exists, but no driverData is provided.\n",
					nameBytes, name, parent);
		}

		if (node) {
			if (!update) {
				if (existingEntry->thisItem->node) {
					KernelPanic("FSDirectoryEntryFound - Entry exists and is created on file system.\n");
				}

				EsError error = FSDirectoryEntryAllocateNode(existingEntry->thisItem, parent->fileSystem, true);

				if (error != ES_SUCCESS) {
					return error;
				}
			}

			*node = existingEntry->thisItem->node;
			if (outMetadata) *outMetadata = existingEntry->thisItem;
		} else if (update) {
			EsMemoryCopy(existingEntry->thisItem + 1, driverData, driverDataBytes);
		} else if (EsMemoryCompare(existingEntry->thisItem + 1, driverData, driverDataBytes)) {
			KernelPanic("FSDirectoryEntryFound - 'update' is false but driverData has changed.\n");
		}

		return ES_SUCCESS;
	} else if (update) {
		return ES_ERROR_FILE_DOES_NOT_EXIST;
	}

	FSDirectoryEntry *entry = (FSDirectoryEntry *) EsHeapAllocate(sizeof(FSDirectoryEntry) + driverDataBytes, true, K_FIXED);

	if (!entry) {
		return ES_ERROR_INSUFFICIENT_RESOURCES;
	}

	if (nameBytes > sizeof(entry->inlineName)) {
		entry->item.key.longKey = EsHeapAllocate(nameBytes, false, K_FIXED);

		if (!entry->item.key.longKey) {
			EsHeapFree(entry, sizeof(FSDirectoryEntry) + driverDataBytes, K_FIXED);
			return ES_ERROR_INSUFFICIENT_RESOURCES;
		}
	} else {
		entry->item.key.longKey = entry->inlineName;
	}

	EsMemoryCopy(entry->item.key.longKey, name, nameBytes);
	entry->item.key.longKeyBytes = nameBytes;

	EsMemoryCopy(entry, metadata, sizeof(KNodeMetadata));
	if (driverData) EsMemoryCopy(entry + 1, driverData, driverDataBytes);
	entry->parent = parent;

	TreeInsert(&parent->entries, &entry->item, entry, entry->item.key, AVL_DUPLICATE_KEYS_PANIC);
	parent->entryCount++;

	if (node) {
		if (!update) {
			EsError error = FSDirectoryEntryAllocateNode(entry, parent->fileSystem, driverData);

			if (error != ES_SUCCESS) {
				return error;
			}
		}

		*node = entry->node;
		if (outMetadata) *outMetadata = entry + 1;
	}

	return ES_SUCCESS;
}

void FSNodeUpdateDriverData(KNode *node, const void *newDriverData) {
	node->writerLock.AssertExclusive();
	EsMemoryCopy(node->directoryEntry + 1, newDriverData, node->fileSystem->directoryEntryDataBytes);
}

//////////////////////////////////////////
// Opening nodes.
//////////////////////////////////////////

EsError FSNodeOpenHandle(KNode *node, uint32_t flags) {
	{
		// See comment in FSNodeCloseHandle for why we use the spinlock.
		fs.updateNodeHandles.Acquire();
		EsDefer(fs.updateNodeHandles.Release());

		if (node->handles == NODE_MAX_ACCESSORS) { 
			return ES_ERROR_INSUFFICIENT_RESOURCES; 
		} else if (node->flags & NODE_DELETED) {
			return ES_ERROR_NODE_DELETED;
		}

		if (node->directoryEntry->type == ES_NODE_FILE) {
			FSFile *file = (FSFile *) node;

			if (flags & ES_FILE_READ) {
				if (file->countWrite > 0) return ES_ERROR_FILE_HAS_WRITERS; 
			} else if (flags & ES_FILE_WRITE_EXCLUSIVE) {
				if (flags & _ES_NODE_FROM_WRITE_EXCLUSIVE) {
					if (!file->countWrite || (~file->flags & NODE_HAS_EXCLUSIVE_WRITER)) {
						KernelPanic("FSNodeOpenHandle - File %x is invalid state for a handle to have the _ES_NODE_FROM_WRITE_EXCLUSIVE flag.\n", file);
					}
				} else {
					if (file->countWrite) {
						return ES_ERROR_FILE_CANNOT_GET_EXCLUSIVE_USE; 
					}
				}
			} else if (flags & ES_FILE_WRITE) {
				if ((file->flags & NODE_HAS_EXCLUSIVE_WRITER) || file->countWrite < 0) return ES_ERROR_FILE_IN_EXCLUSIVE_USE;
			}

			if (flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE)) {
				if (!file->fileSystem->write) {
					return ES_ERROR_FILE_ON_READ_ONLY_VOLUME;
				}
			}

			if (flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE)) file->countWrite++;
			if (flags & ES_FILE_READ) file->countWrite--;
			if (flags & ES_FILE_WRITE_EXCLUSIVE) __sync_fetch_and_or(&node->flags, NODE_HAS_EXCLUSIVE_WRITER);
		}

		node->handles++;
	}

	if (node->directoryEntry->type == ES_NODE_FILE && (flags & ES_NODE_PREVENT_RESIZE)) {
		// Modify blockResize with the resizeLock, to prevent a resize being in progress when blockResize becomes positive.
		FSFile *file = (FSFile *) node;
		file->resizeLock.Take(K_LOCK_EXCLUSIVE);
		file->blockResize++;
		file->resizeLock.Return(K_LOCK_EXCLUSIVE);
	}

	return ES_SUCCESS;
}

void FSNodeCloseHandle(KNode *node, uint32_t flags) {
	// Don't use the node's writer lock for this.
	// It'd be unnecessarily require getting exclusive access.
	// There's not much to do, so just use a global spinlock.
	fs.updateNodeHandles.Acquire();

	if (node->handles) {
		node->handles--;
	} else {
		KernelPanic("FSNodeCloseHandle - Node %x had no handles.\n", node);
	}

	if (node->directoryEntry->type == ES_NODE_FILE) {
		FSFile *file = (FSFile *) node;

		if ((flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE))) {
			if (file->countWrite <= 0) KernelPanic("FSNodeCloseHandle - Invalid countWrite on node %x.\n", node);
			file->countWrite--;
		}

		if ((flags & ES_FILE_READ)) {
			if (file->countWrite >= 0) KernelPanic("FSNodeCloseHandle - Invalid countWrite on node %x.\n", node);
			file->countWrite++;
		}

		if ((flags & ES_FILE_WRITE_EXCLUSIVE) && file->countWrite == 0) {
			if (~file->flags & NODE_HAS_EXCLUSIVE_WRITER) KernelPanic("FSNodeCloseHandle - Missing exclusive flag on node %x.\n", node);
			__sync_fetch_and_and(&node->flags, ~NODE_HAS_EXCLUSIVE_WRITER);
		}
	}

	bool deleted = (node->flags & NODE_DELETED) && !node->handles;

	fs.updateNodeHandles.Release();

	if (node->directoryEntry->type == ES_NODE_FILE && (flags & ES_NODE_PREVENT_RESIZE)) {
		FSFile *file = (FSFile *) node;
		file->resizeLock.Take(K_LOCK_EXCLUSIVE);
		file->blockResize--;
		file->resizeLock.Return(K_LOCK_EXCLUSIVE);
	}

	if (deleted) {
		// The node has been deleted, and no handles remain.
		// When it was deleted, it should have been removed from its parent directory,
		// both on the file system and in the directory lookup structures.
		// So, we are free to deallocate the node.

		if (node->directoryEntry->type == ES_NODE_FILE) {
			CCSpaceDestroy(&((FSFile *) node)->cache);
		} else if (node->directoryEntry->type == ES_NODE_DIRECTORY) {
			if (((FSDirectory *) node)->entries.root) {
				KernelPanic("FSNodeCloseHandle - Deleted directory %x has entries.\n", node);
			}
		}

		if (node->directoryEntry->item.key.longKey != node->directoryEntry->inlineName) {
			EsHeapFree((void *) node->directoryEntry->item.key.longKey, node->directoryEntry->item.key.longKeyBytes, K_FIXED);
		}

		EsHeapFree(node->directoryEntry, 0, K_FIXED);
		EsHeapFree(node, 0, K_FIXED);
	}
}

#ifdef ENABLE_POSIX_SUBSYSTEM
char *FSConvertPathFromPOSIXNamespace(const char *_name, size_t nameLength, size_t *outNameLength) {
	// TODO Do this conversion in userspace.

	// Convert the path to our format.

	char *name = (char *) EsHeapAllocate(nameLength, true, K_FIXED);
	if (!name) return nullptr;
	const char *posixName = _name;
	size_t posixNameLength = nameLength;
	nameLength = 0;

	while (posixNameLength) {
		const char *entry = posixName;
		size_t entryLength = 0;

		while (posixNameLength) {
			posixNameLength--;
			posixName++;
			if (entry[entryLength] == '/') break;
			entryLength++;
		}

		if (!entryLength || (entryLength == 1 && entry[0] == '.')) {
			// Ignore.
		} else if (entryLength == 2 && entry[0] == '.' && entry[1] == '.' && nameLength) {
			while (name[--nameLength] != '/');
		} else {
			name[nameLength++] = '/';
			EsMemoryCopy(name + nameLength, entry, entryLength);
			nameLength += entryLength;
		}
	}

	if (!nameLength) {
		nameLength++;
		name[0] = '/';
	}

	*outNameLength = nameLength;
	return name;
}
#endif

KNodeInformation FSNodeOpen(const char *path, size_t pathBytes, uint32_t flags) {
	if ((1 << (flags & 0xF)) & ~(0x117)) {
		// You should only pass one access flag! (or none)
		return { ES_ERROR_FILE_PERMISSION_NOT_GRANTED }; 
	}

	if (fs.shutdown) return { ES_ERROR_PATH_NOT_TRAVERSABLE };
	if (pathBytes && path[pathBytes - 1] == '/') pathBytes--;
	if (!fs.bootFileSystem || path[0] != '/') return { ES_ERROR_PATH_NOT_WITHIN_MOUNTED_VOLUME };

	if (!FSCheckPathForIllegalCharacters(path, pathBytes)) {
		return { ES_ERROR_ILLEGAL_PATH };
	}

#ifdef ENABLE_POSIX_SUBSYSTEM
	char *freePath = nullptr;

	if (flags & ES_NODE_POSIX_NAMESPACE) {
		path = freePath = FSConvertPathFromPOSIXNamespace(path, pathBytes, &pathBytes);

		if (!path) {
			return { ES_ERROR_INSUFFICIENT_RESOURCES };
		}
	}

	EsDefer(EsHeapFree(freePath, 0, K_FIXED));
#endif

	KFileSystem *fileSystem = fs.bootFileSystem;
	FSDirectory *directory = (FSDirectory *) fileSystem->rootDirectory;
	uintptr_t sectionStart = 0, sectionEnd = 0;
	KNode *node = pathBytes ? nullptr : directory;

	// TODO What happens if the file system is being unmounted?
	EsError error = FSNodeOpenHandle(directory, 0);
	if (error != ES_SUCCESS) return { error };

	while (sectionEnd < pathBytes) {
		sectionEnd++;
		sectionStart = sectionEnd;

		while (sectionEnd != pathBytes && path[sectionEnd] != '/') {
			sectionEnd++;
		}

		const char *name = path + sectionStart;
		size_t nameBytes = sectionEnd - sectionStart;
		AVLKey key = MakeLongKey(name, nameBytes);
		FSDirectoryEntry *entry = nullptr;
		AVLItem<FSDirectoryEntry> *treeItem = nullptr;

		// First, try to get the cached directory entry with shared access.

		{
			directory->writerLock.Take(K_LOCK_SHARED);

			treeItem = TreeFind(&directory->entries, key, TREE_SEARCH_EXACT);
			bool needExclusiveAccess = true;

			if (treeItem && treeItem->thisItem->node) {
				entry = treeItem->thisItem;
				error = FSNodeOpenHandle(entry->node, 0);
				needExclusiveAccess = false;
			}

			directory->writerLock.Return(K_LOCK_SHARED);

			if (!needExclusiveAccess) {
				goto usedSharedAccess;
			}
		}

		directory->writerLock.Take(K_LOCK_EXCLUSIVE);

		treeItem = TreeFind(&directory->entries, key, TREE_SEARCH_EXACT);

		if (!treeItem && (~directory->flags & NODE_ENUMERATED_ALL_DIRECTORY_ENTRIES)) {
			// The entry is not cached; load it from the file system.
			fileSystem->scan(name, nameBytes, directory);
			treeItem = TreeFind(&directory->entries, key, TREE_SEARCH_EXACT);
		}

		if (!treeItem) {
			// The node does not exist.

			if (sectionEnd == pathBytes) {
				if (~flags & ES_NODE_FAIL_IF_NOT_FOUND) {
					FSNodeCreate(directory, name, nameBytes, flags & ES_NODE_DIRECTORY);
					treeItem = TreeFind(&directory->entries, key, TREE_SEARCH_EXACT);
					flags &= ~ES_NODE_FAIL_IF_FOUND;
				}

				if (!treeItem) {
					error = ES_ERROR_FILE_DOES_NOT_EXIST;
					goto failed;
				}
			} else {
				if (flags & ES_NODE_CREATE_DIRECTORIES) {
					FSNodeCreate(directory, name, nameBytes, ES_NODE_DIRECTORY);
					treeItem = TreeFind(&directory->entries, key, TREE_SEARCH_EXACT);
				}

				if (!treeItem) {
					error = ES_ERROR_PATH_NOT_TRAVERSABLE;
					goto failed;
				}
			}
		}

		entry = treeItem->thisItem;

		if (!entry->node) {
			// The node has not be loaded; load it from the file system.

			error = FSDirectoryEntryAllocateNode(entry, fileSystem, true);

			if (error != ES_SUCCESS) {
				goto failed;
			}

			error = fileSystem->load(directory, entry->node, entry, entry + 1);

			if (error != ES_SUCCESS) {
				FSNodeFree(entry->node);
				goto failed;
			}
		}

		error = FSNodeOpenHandle(entry->node, 0);

		failed:;
		directory->writerLock.Return(K_LOCK_EXCLUSIVE);
		usedSharedAccess:;
		FSNodeCloseHandle(directory, 0);

		if (error != ES_SUCCESS) {
			return { error };
		} 

		if (sectionEnd != pathBytes) {
			if (entry->node->directoryEntry->type != ES_NODE_DIRECTORY) {
				FSNodeCloseHandle(directory, 0);
				return { ES_ERROR_PATH_NOT_TRAVERSABLE };
			}

			directory = (FSDirectory *) entry->node;
		} else {
			node = entry->node;
		}
	}

	if (node->directoryEntry->type != ES_NODE_DIRECTORY) {
		if (flags & ES_NODE_DIRECTORY) { 
			return { ES_ERROR_INCORRECT_NODE_TYPE }; 
		}
	}

	if (flags & ES_NODE_FAIL_IF_FOUND) {
		error = ES_ERROR_FILE_ALREADY_EXISTS;
	} else {
		error = FSNodeOpenHandle(node, flags);
	}

	FSNodeCloseHandle(node, 0);
	if (error != ES_SUCCESS) node = nullptr;
	return { error, node };
}

//////////////////////////////////////////
// DMA transfer buffers.
//////////////////////////////////////////

struct KDMABuffer {
	uintptr_t virtualAddress;
	size_t totalByteCount;
	uintptr_t offsetBytes;
};

uintptr_t KDMABufferGetVirtualAddress(KDMABuffer *buffer) {
	return buffer->virtualAddress;
}

size_t KDMABufferGetTotalByteCount(KDMABuffer *buffer) {
	return buffer->totalByteCount;
}

bool KDMABufferIsComplete(KDMABuffer *buffer) {
	return buffer->offsetBytes == buffer->totalByteCount;
}

KDMASegment KDMABufferNextSegment(KDMABuffer *buffer, bool peek) {
	if (buffer->offsetBytes >= buffer->totalByteCount || !buffer->virtualAddress) {
		KernelPanic("KDMABufferNextSegment - Invalid state in buffer %x.\n", buffer);
	}

	size_t transferByteCount = K_PAGE_SIZE;
	uintptr_t virtualAddress = buffer->virtualAddress + buffer->offsetBytes;
	uintptr_t physicalAddress = MMArchTranslateAddress(MMGetKernelSpace(), virtualAddress);
	uintptr_t offsetIntoPage = virtualAddress & (K_PAGE_SIZE - 1);

	if (!physicalAddress) {
		KernelPanic("KDMABufferNextSegment - Page in buffer %x unmapped.\n", buffer);
	}

	if (offsetIntoPage) {
		transferByteCount = K_PAGE_SIZE - offsetIntoPage;
		physicalAddress += offsetIntoPage;
	}

	if (transferByteCount > buffer->totalByteCount - buffer->offsetBytes) {
		transferByteCount = buffer->totalByteCount - buffer->offsetBytes;
	}

	bool isLast = buffer->offsetBytes + transferByteCount == buffer->totalByteCount;
	if (!peek) buffer->offsetBytes += transferByteCount;
	return { physicalAddress, transferByteCount, isLast };
}

//////////////////////////////////////////
// Block devices.
//////////////////////////////////////////

bool FSBlockDeviceAccess(KBlockDeviceAccessRequest request) {
	KBlockDevice *device = request.device;

	if (!request.count) {
		return true;
	}

	if (device->readOnly && request.operation == K_ACCESS_WRITE) {
		KernelPanic("FSBlockDeviceAccess - Drive %x is read-only.\n", device);
	}

	if (request.offset / device->sectorSize > device->sectorCount 
			|| (request.offset + request.count) / device->sectorSize > device->sectorCount) {
		KernelPanic("FSBlockDeviceAccess - Access out of bounds on drive %x.\n", device);
	}

	if ((request.offset % device->sectorSize) || (request.count % device->sectorSize)) {
		KernelPanic("FSBlockDeviceAccess - Misaligned access.\n");
	}

	KDMABuffer buffer = *request.buffer;

	if (buffer.virtualAddress & 3) {
		KernelPanic("FSBlockDeviceAccess - Buffer must be DWORD aligned.\n");
	}

	KWorkGroup fakeDispatchGroup = {};

	if (!request.dispatchGroup) {
		fakeDispatchGroup.Initialise();
		request.dispatchGroup = &fakeDispatchGroup;
	}

	KBlockDeviceAccessRequest r = {};
	r.device = request.device;
	r.buffer = &buffer;
	r.flags = request.flags;
	r.dispatchGroup = request.dispatchGroup;
	r.operation = request.operation;
	r.offset = request.offset;

	while (request.count) {
		r.count = device->maxAccessSectorCount * device->sectorSize;
		if (r.count > request.count) r.count = request.count;
		buffer.offsetBytes = 0;
		buffer.totalByteCount = r.count;
		r.count = r.count;
		device->access(r);
		r.offset += r.count;
		buffer.virtualAddress += r.count;
		request.count -= r.count;
	}

	if (request.dispatchGroup == &fakeDispatchGroup) {
		return fakeDispatchGroup.Wait();
	} else {
		return true;
	}
}

EsError FSReadIntoBlockCache(CCSpace *fileCache, void *buffer, EsFileOffset offset, EsFileOffset count) {
	KFileSystem *fileSystem = (KFileSystem *) fileCache - 1;
	return fileSystem->Access(offset, count, K_ACCESS_READ, buffer, ES_FLAGS_DEFAULT, nullptr) ? ES_SUCCESS : ES_ERROR_DRIVE_CONTROLLER_REPORTED;
}

EsError FSWriteFromBlockCache(CCSpace *fileCache, const void *buffer, EsFileOffset offset, EsFileOffset count) {
	KFileSystem *fileSystem = (KFileSystem *) fileCache - 1;
	return fileSystem->Access(offset, count, K_ACCESS_WRITE, (void *) buffer, ES_FLAGS_DEFAULT, nullptr) ? ES_SUCCESS : ES_ERROR_DRIVE_CONTROLLER_REPORTED;
}

const CCSpaceCallbacks fsBlockCacheCallbacks = {
	.readInto = FSReadIntoBlockCache,
	.writeFrom = FSWriteFromBlockCache,
};

bool KFileSystem::Access(EsFileOffset offset, size_t count, int operation, void *buffer, uint32_t flags, KWorkGroup *dispatchGroup) {
	bool blockDeviceCachedEnabled = true;

	if (blockDeviceCachedEnabled && (flags & BLOCK_ACCESS_CACHED)) {
		if (dispatchGroup) {
			dispatchGroup->Start();
		}

		// TODO Use the dispatch group.

		EsError result = CCSpaceAccess((CCSpace *) (this + 1), buffer, offset, count, 
				operation == K_ACCESS_READ ? CC_ACCESS_READ : (CC_ACCESS_WRITE | CC_ACCESS_WRITE_BACK));

		if (dispatchGroup) {
			dispatchGroup->End(result == ES_SUCCESS);
		}

		return result == ES_SUCCESS;
	} else {
		KDMABuffer dmaBuffer = { (uintptr_t) buffer, count };
		KBlockDeviceAccessRequest request = {};
		request.device = block;
		request.offset = offset;
		request.count = count;
		request.operation = operation;
		request.buffer = &dmaBuffer;
		request.flags = flags;
		request.dispatchGroup = dispatchGroup;
		return FSBlockDeviceAccess(request);
	}
}

//////////////////////////////////////////
// Partition devices.
//////////////////////////////////////////

struct PartitionDevice : KBlockDevice {
	EsFileOffset sectorOffset;
};

void FSPartitionDeviceAccess(KBlockDeviceAccessRequest request) {
	PartitionDevice *_device = (PartitionDevice *) request.device;
	request.device = (KBlockDevice *) _device->parent;
	request.offset += _device->sectorOffset * _device->sectorSize;
	FSBlockDeviceAccess(request);
}

void FSPartitionDeviceCreate(KBlockDevice *parent, EsFileOffset offset, EsFileOffset sectorCount, unsigned flags, const char *cName) {
	PartitionDevice *child = (PartitionDevice *) fs.driver->NewDevice(parent, sizeof(PartitionDevice));
	if (!child) return;

	child->parent = parent;
	child->sectorSize = parent->sectorSize;
	child->maxAccessSectorCount = parent->maxAccessSectorCount;
	child->sectorOffset = offset;
	child->sectorCount = sectorCount;
	child->noMBR = flags & FS_PARTITION_DEVICE_NO_MBR ? true : false;
	child->readOnly = parent->readOnly;
	child->access = FSPartitionDeviceAccess;
	child->cModel = cName;
	child->nestLevel = parent->nestLevel + 1;

	KRegisterDevice("Files", child, FS_REGISTER_BLOCK_DEVICE);
}

//////////////////////////////////////////
// File system and partition table detection.
//////////////////////////////////////////

bool FSSignatureCheck(KInstalledDriver *driver, KDevice *device) {
	uint8_t *block = ((KFileSystem *) device)->block->information;

	EsINIState s = {};
	s.buffer = driver->config;
	s.bytes = driver->configBytes;

	int64_t offset = -1;

	while (EsINIParse(&s)) {
		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("signature_offset"))) {
			offset = EsIntegerParse(s.value, s.valueBytes);
		} else if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("signature"))) {
			if (offset >= K_SIGNATURE_BLOCK_SIZE || offset >= K_SIGNATURE_BLOCK_SIZE - (int64_t) s.valueBytes || offset < 0) {
				KernelPanic("FSSignatureCheck - Filesystem '%s' has invalid signature detection information.\n", driver->nameBytes, driver->name);
			}

			if (0 == EsMemoryCompare(block + offset, s.value, s.valueBytes)) {
				return true;
			}
		}
	}

	return false;
}

bool FSCheckMBR(KBlockDevice *device) {
	if (device->information[510] != 0x55 || device->information[511] != 0xAA) {
		return false;
	}

	KernelLog(LOG_INFO, "FS", "probing MBR", "First sector on device looks like an MBR...\n");

	uint32_t offsets[4], counts[4];
	bool present[4] = {};

	for (uintptr_t i = 0; i < 4; i++) {
		if (!device->information[4 + 0x1BE + i * 0x10]) {
			continue;
		}

		offsets[i] =  
			  ((uint32_t) device->information[0x1BE + i * 0x10 + 8 ] << 0 )
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 9 ] << 8 )
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 10] << 16)
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 11] << 24);
		counts[i] =
			  ((uint32_t) device->information[0x1BE + i * 0x10 + 12] << 0 )
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 13] << 8 )
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 14] << 16)
			+ ((uint32_t) device->information[0x1BE + i * 0x10 + 15] << 24);
		present[i] = true;

		if (offsets[i] > device->sectorCount || counts[i] > device->sectorCount - offsets[i] || counts[i] < 32) {
			KernelLog(LOG_INFO, "FS", "invalid MBR", "Partition %d has offset %d and count %d, which is invalid. Ignoring the rest of the MBR...\n",
					i, offsets[i], counts[i]);
			return false;
		}
	}

	for (uintptr_t i = 0; i < 4; i++) {
		if (present[i]) {
			KernelLog(LOG_INFO, "FS", "MBR partition", "Found MBR partition %d with offset %d and count %d.\n", 
					i, offsets[i], counts[i]);
			FSPartitionDeviceCreate(device, offsets[i], counts[i], FS_PARTITION_DEVICE_NO_MBR, "MBR partition");
		}
	}

	return true;
}

void FSDetectFileSystem(KBlockDevice *device) {
	KernelLog(LOG_INFO, "FS", "detect file system", "Detecting file system on block device '%z'.\n", device->cModel);

	if (device->nestLevel > 4) {
		KernelLog(LOG_ERROR, "FS", "file system nest limit", "Reached file system nest limit (4), ignoring device.\n");
	}

	uint64_t sectorsToRead = (K_SIGNATURE_BLOCK_SIZE + device->sectorSize - 1) / device->sectorSize;

	if (sectorsToRead > device->sectorCount) {
		KernelLog(LOG_ERROR, "FS", "drive too small", "The drive must be at least %D (K_SIGNATURE_BLOCK_SIZE).\n", K_SIGNATURE_BLOCK_SIZE);
		return;
	}

	uint8_t *information = (uint8_t *) EsHeapAllocate(sectorsToRead * device->sectorSize, false, K_FIXED);
	device->information = information;

	KDMABuffer dmaBuffer = { (uintptr_t) information };
	KBlockDeviceAccessRequest request = {};
	request.device = device;
	request.count = sectorsToRead * device->sectorSize;
	request.operation = K_ACCESS_READ;
	request.buffer = &dmaBuffer;

	if (!FSBlockDeviceAccess(request)) {
		// We could not access the block device.
		KernelLog(LOG_ERROR, "FS", "detect fileSystem read failure", "The signature block could not be read on block device %x.\n", device);
	} else {
		if (!device->noMBR && FSCheckMBR(device)) {
			// Found an MBR.
		} else {
			FSDirectoryEntry *rootEntry = (FSDirectoryEntry *) EsHeapAllocate(sizeof(FSDirectoryEntry), true, K_FIXED);
			rootEntry->type = ES_NODE_DIRECTORY;

			if (rootEntry && ES_SUCCESS == FSDirectoryEntryAllocateNode(rootEntry, nullptr, true)) {
				KFileSystem *fileSystem = (KFileSystem *) fs.driver->NewDevice(device, sizeof(KFileSystem) + sizeof(CCSpace));

				if (fileSystem) {
					fileSystem->rootDirectory = rootEntry->node;
					fileSystem->rootDirectory->fileSystem = fileSystem;
					fileSystem->block = device;
					CCSpace *cache = (CCSpace *) (fileSystem + 1);

					if (!CCSpaceInitialise(cache)) {
						fileSystem->Destroy();
					} else {
						cache->callbacks = &fsBlockCacheCallbacks;
						fs.driver->ChildDeviceAttached(FSSignatureCheck, fileSystem);
					}
				} else {
					FSNodeFree(rootEntry->node);
					EsHeapFree(rootEntry, sizeof(FSDirectoryEntry), K_FIXED);
				}
			} else {
				EsHeapFree(rootEntry, sizeof(FSDirectoryEntry), K_FIXED);
			}
		}
	}

	EsHeapFree(information, sectorsToRead * device->sectorSize, K_FIXED);
}

//////////////////////////////////////////
// Device management.
//////////////////////////////////////////

void FSRegisterBootFileSystem(KFileSystem *fileSystem, EsUniqueIdentifier identifier) {
	FSRegisterFileSystem(fileSystem); 

	if (EsMemoryCompare(&identifier, &installationID, sizeof(EsUniqueIdentifier))) {
		return;
	}

	fs.fileSystemsLock.Take(K_LOCK_EXCLUSIVE);

	if (!fs.bootFileSystem) {
		fs.bootFileSystem = fileSystem;
		fs.foundBootFileSystemEvent.Set();
	} else {
		KernelLog(LOG_ERROR, "FS", "duplicate boot file system", "Found multiple boot file systems; the first registered will be used.\n");
	}

	fs.fileSystemsLock.Return(K_LOCK_EXCLUSIVE);
}

void FSRegisterDevice(KDriver *, KDevice *device, EsGeneric context) {
	if (context.i == FS_REGISTER_FILE_SYSTEM) {
		KFileSystem *fileSystem = (KFileSystem *) device;
		fileSystem->rootDirectory->directoryEntry->directoryChildren = fileSystem->rootDirectoryInitialChildren;
		fileSystem->rootDirectory->handles = 1;
		fs.fileSystemsLock.Take(K_LOCK_EXCLUSIVE);
		ArrayAdd(fs.fileSystems, fileSystem);
		fs.fileSystemsLock.Return(K_LOCK_EXCLUSIVE);
	} else if (context.i == FS_REGISTER_BLOCK_DEVICE) {
#ifdef SERIAL_STARTUP
		FSDetectFileSystem((KBlockDevice *) device);
#else
		KThreadCreate("FSDetect", [] (uintptr_t context) {
			FSDetectFileSystem((KBlockDevice *) context);
			KThreadTerminate();
		}, (uintptr_t) device);
#endif
	} else {
		KernelPanic("RegisterFilesDevice - Invalid context %x.\n", context);
	}
}

void FSShutdown() {
	// TODO.
	fs.shutdown = true;
}

extern "C" void EntryFiles(KModuleInitilisationArguments *arguments) {
	fs.driver = arguments->driver;
	fs.driver->deviceRegistered = FSRegisterDevice;
	ArrayInitialise(fs.fileSystems, K_FIXED);
}

#endif
