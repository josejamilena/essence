#include "unicode.cpp"

#ifndef SHARED_DEFINITIONS_ONLY

#ifdef USE_STB_SPRINTF
#define STB_SPRINTF_IMPLEMENTATION
#include "../shared/stb_sprintf.h"
#endif

void EnterDebugger() {}

struct Corners8 { int8_t tl, tr, bl, br; };
struct Corners32 { int32_t tl, tr, bl, br; };
struct Rectangle8 { int8_t l, r, t, b; };
struct Rectangle16 { int16_t l, r, t, b; };
struct Rectangle32 { int32_t l, r, t, b; };

bool operator==(EsRectangle a, EsRectangle b) {
	return a.l == b.l && a.r == b.r && a.t == b.t && a.b == b.b;
}

int Width(const EsRectangle &rectangle) {
	return rectangle.r - rectangle.l;
}

int Height(const EsRectangle &rectangle) {
	return rectangle.b - rectangle.t;
}

bool IsPointInRectangle(EsRectangle rectangle, int x, int y) {
	if (rectangle.l > x || rectangle.r <= x || rectangle.t > y || rectangle.b <= y 
			|| rectangle.l == rectangle.r || rectangle.t == rectangle.b) {
		return false;
	}
	
	return true;
}

EsRectangle AtOrigin(const EsRectangle &rectangle) {
	return ES_MAKE_RECTANGLE(0, Width(rectangle), 0, Height(rectangle));
}

EsRectangle AddBorder(EsRectangle rectangle, EsRectangle border) {
	rectangle.l += border.l;
	rectangle.r -= border.r;
	rectangle.t += border.t;
	rectangle.b -= border.b;
	return rectangle;
}

EsRectangle AddBorder(EsRectangle rectangle, Rectangle16 border) {
	rectangle.l += border.l;
	rectangle.r -= border.r;
	rectangle.t += border.t;
	rectangle.b -= border.b;
	return rectangle;
}

EsRectangle AddBorder(EsRectangle rectangle, int border) {
	rectangle.l += border;
	rectangle.r -= border;
	rectangle.t += border;
	rectangle.b -= border;
	return rectangle;
}

EsRectangle SubtractBorder(EsRectangle rectangle, EsRectangle border) {
	rectangle.l -= border.l;
	rectangle.r += border.r;
	rectangle.t -= border.t;
	rectangle.b += border.b;
	return rectangle;
}

EsRectangle SubtractBorder(EsRectangle rectangle, int border) {
	rectangle.l -= border;
	rectangle.r += border;
	rectangle.t -= border;
	rectangle.b += border;
	return rectangle;
}

EsRectangle RectangleCenter(EsRectangle container, int width, int height) {
	int centerX = (container.r + container.l) / 2;
	int centerY = (container.b + container.t) / 2;
	return ES_MAKE_RECTANGLE(centerX - width / 2, centerX + (width + 1) / 2, centerY - height / 2, centerY + (height + 1) / 2);
}

EsRectangle operator-(EsRectangle rectangle) {
	return ES_MAKE_RECTANGLE(-rectangle.l, -rectangle.r, -rectangle.t, -rectangle.b);
}

EsRectangle Translate(EsRectangle rectangle, int x, int y) {
	rectangle.l += x;
	rectangle.r += x;
	rectangle.t += y;
	rectangle.b += y;
	return rectangle;
}

EsRectangle Translate(EsRectangle rectangle, EsPoint point) {
	rectangle.l += point.x;
	rectangle.r += point.x;
	rectangle.t += point.y;
	rectangle.b += point.y;
	return rectangle;
}

void BlendPixel(uint32_t *destinationPixel, uint32_t modified, bool fullAlpha) {
	if ((modified & 0xFF000000) == 0xFF000000) {
		*destinationPixel = modified;
		return;
	} else if ((modified & 0xFF000000) == 0x00000000) {
		return;
	}

	uint32_t m1, m2, a;
	uint32_t original = *destinationPixel;

	if ((*destinationPixel & 0xFF000000) != 0xFF000000 && fullAlpha) {
		uint32_t alpha1 = (modified & 0xFF000000) >> 24;
		uint32_t alpha2 = 255 - alpha1;
		uint32_t alphaD = (original & 0xFF000000) >> 24;
		uint32_t alphaD2 = alphaD * alpha2;
		uint32_t alphaOut = alpha1 + (alphaD2 >> 8);

		if (!alphaOut) {
			return;
		}

		m2 = alphaD2 / alphaOut;
		m1 = (alpha1 << 8) / alphaOut;
		if (m2 == 0x100) m2--;
		if (m1 == 0x100) m1--;
		a = alphaOut << 24;
	} else {
		m1 = (modified & 0xFF000000) >> 24;
		m2 = 255 - m1;
		a = 0xFF000000;
	}

	uint32_t r2 = m2 * (original & 0x00FF00FF);
	uint32_t g2 = m2 * (original & 0x0000FF00);
	uint32_t r1 = m1 * (modified & 0x00FF00FF);
	uint32_t g1 = m1 * (modified & 0x0000FF00);
	uint32_t result = a | (0x0000FF00 & ((g1 + g2) >> 8)) | (0x00FF00FF & ((r1 + r2) >> 8));
	*destinationPixel = result;
}

struct EsPaintTarget {
	void *bits;
	uint32_t width, height, stride;
	bool fullAlpha, readOnly;
};

void EsDrawInvert(EsPainter *painter, EsRectangle bounds) {
	EsPaintTarget *target = painter->target;

	if (!EsRectangleClip(bounds, painter->clip, &bounds)) {
		return;
	}

	uintptr_t stride = target->stride / 4;
	uint32_t *lineStart = (uint32_t *) target->bits + bounds.t * stride + bounds.l;

	__m128i mask = _mm_set_epi32(0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF);

	for (int i = 0; i < bounds.b - bounds.t; i++, lineStart += stride) {
		uint32_t *destination = lineStart;
		int j = bounds.r - bounds.l;

		while (j >= 4) {
			*(__m128i *) destination = _mm_xor_si128(*(__m128i *) destination, mask);
			destination += 4;
			j -= 4;
		} 

		while (j > 0) {
			*destination ^= 0xFFFFFF;
			destination++;
			j--;
		} 
	}
}

void EsDrawClear(EsPainter *painter, EsRectangle bounds) {
	EsPaintTarget *target = painter->target;

	if (!EsRectangleClip(bounds, painter->clip, &bounds)) {
		return;
	}

	uintptr_t stride = target->stride / 4;
	uint32_t *lineStart = (uint32_t *) target->bits + bounds.t * stride + bounds.l;

	__m128i zero = {};

	for (int i = 0; i < bounds.b - bounds.t; i++, lineStart += stride) {
		uint32_t *destination = lineStart;
		int j = bounds.r - bounds.l;

		while (j >= 4) {
			_mm_storeu_si128((__m128i *) destination, zero);
			destination += 4;
			j -= 4;
		} 

		while (j > 0) {
			*destination = 0;
			destination++;
			j--;
		} 
	}
}

void _DrawBlock(uintptr_t stride, void *bits, EsRectangle bounds, uint32_t color, bool fullAlpha) {
	stride /= 4;
	uint32_t *lineStart = (uint32_t *) bits + bounds.t * stride + bounds.l;

	__m128i color4 = _mm_set_epi32(color, color, color, color);

	for (int i = 0; i < bounds.b - bounds.t; i++, lineStart += stride) {
		uint32_t *destination = lineStart;
		int j = bounds.r - bounds.l;

		if ((color & 0xFF000000) != 0xFF000000) {
			do {
				BlendPixel(destination, color, fullAlpha);
				destination++;
			} while (--j);
		} else {
			while (j >= 4) {
				_mm_storeu_si128((__m128i *) destination, color4);
				destination += 4;
				j -= 4;
			} 

			while (j > 0) {
				*destination = color;
				destination++;
				j--;
			} 
		}
	}
}

void EsDrawBlock(EsPainter *painter, EsRectangle bounds, uint32_t color) {
	if (!(color & 0xFF000000)) {
		return;
	}

	EsPaintTarget *target = painter->target;

	if (!EsRectangleClip(bounds, painter->clip, &bounds)) {
		return;
	}
	
	_DrawBlock(target->stride, target->bits, bounds, color, target->fullAlpha);
}

void EsDrawBitmap(EsPainter *painter, EsRectangle region, uint32_t *sourceBits, uintptr_t sourceStride, uint16_t alpha) {
	EsPaintTarget *target = painter->target;
	EsRectangle bounds;

	if (!EsRectangleClip(region, painter->clip, &bounds)) {
		return;
	}

	sourceStride /= 4;
	uintptr_t stride = target->stride / 4;
	uint32_t *lineStart = (uint32_t *) target->bits + bounds.t * stride + bounds.l;
	uint32_t *sourceLineStart = sourceBits + (bounds.l - region.l) + sourceStride * (bounds.t - region.t);

	for (int i = 0; i < bounds.b - bounds.t; i++, lineStart += stride, sourceLineStart += sourceStride) {
		uint32_t *destination = lineStart;
		uint32_t *source = sourceLineStart;
		int j = bounds.r - bounds.l;

		if (alpha <= 0xFF) {
			do {
				uint32_t modified = *source;

				if (alpha != 0xFF) {
					modified = (modified & 0xFFFFFF) | (((((modified & 0xFF000000) >> 24) * alpha) << 16) & 0xFF000000);
				}

				BlendPixel(destination, modified, target->fullAlpha);
				destination++;
				source++;
			} while (--j);
		} else {
			while (j >= 4) {
				_mm_storeu_si128((__m128i *) destination, _mm_loadu_si128((__m128i *) source));
				destination += 4;
				source += 4;
				j -= 4;
			} 

			while (j > 0) {
				*destination = *source;
				destination++;
				source++;
				j--;
			} 
		}
	}
}

void EsDrawRectangle(EsPainter *painter, EsRectangle r, uint32_t mainColor, uint32_t borderColor, EsRectangle borderSize) {
	EsDrawBlock(painter, ES_MAKE_RECTANGLE(r.l, r.r, r.t, r.t + borderSize.t), borderColor);
	EsDrawBlock(painter, ES_MAKE_RECTANGLE(r.l, r.l + borderSize.l, r.t + borderSize.t, r.b - borderSize.b), borderColor);
	EsDrawBlock(painter, ES_MAKE_RECTANGLE(r.r - borderSize.r, r.r, r.t + borderSize.t, r.b - borderSize.b), borderColor);
	EsDrawBlock(painter, ES_MAKE_RECTANGLE(r.l, r.r, r.b - borderSize.b, r.b), borderColor);
	EsDrawBlock(painter, ES_MAKE_RECTANGLE(r.l + borderSize.l, r.r - borderSize.r, r.t + borderSize.t, r.b - borderSize.b), mainColor);
}

void ImageDraw(uint32_t *destinationBits, uint32_t destinationWidth, uint32_t destinationHeight, size_t destinationStride,
		uint32_t *sourceBits, uint32_t sourceWidth, uint32_t sourceHeight, size_t sourceStride,
		EsRectangle destinationRegion, EsRectangle sourceRegion, uint16_t alpha, bool fullAlpha) {
	// TODO Replace this!

	if (sourceRegion.l < 0 || sourceRegion.t < 0
			|| sourceRegion.r < 0 || sourceRegion.b < 0
			|| sourceRegion.r > (int32_t) sourceWidth || sourceRegion.b > (int32_t) sourceHeight
			|| sourceRegion.l >= sourceRegion.r || sourceRegion.t >= sourceRegion.b) {
		return;
	}

	EsRectangle clipRegion = ES_MAKE_RECTANGLE(0, destinationWidth, 0, destinationHeight);
	EsRectangle sourceBorderRegion = sourceRegion;

	clipRegion = EsRectangleIntersection(clipRegion, destinationRegion);
	if (clipRegion.r <= clipRegion.l || clipRegion.b <= clipRegion.t) return;
	sourceRegion = EsRectangleAdd(sourceRegion, EsRectangleSubtract(clipRegion, destinationRegion));
	int borderWidth = Width(sourceBorderRegion), borderHeight = Height(sourceBorderRegion);
	if (sourceRegion.l > sourceBorderRegion.l) sourceRegion.l = sourceBorderRegion.l + (clipRegion.l - destinationRegion.l) * borderWidth  / Width(destinationRegion);
	if (sourceRegion.r < sourceBorderRegion.r) sourceRegion.r = sourceBorderRegion.r + (clipRegion.r - destinationRegion.r) * borderWidth  / Width(destinationRegion);
	if (sourceRegion.t > sourceBorderRegion.t) sourceRegion.t = sourceBorderRegion.t + (clipRegion.t - destinationRegion.t) * borderHeight / Height(destinationRegion); 
	if (sourceRegion.b < sourceBorderRegion.b) sourceRegion.b = sourceBorderRegion.b + (clipRegion.b - destinationRegion.b) * borderHeight / Height(destinationRegion); 
	destinationRegion = clipRegion;

	for (intptr_t y = destinationRegion.t; y < destinationRegion.b; y++) {
		intptr_t sy = y - destinationRegion.t + sourceRegion.t;

		intptr_t sourceBorderSize = sourceRegion.b - sourceRegion.t;
		intptr_t destinationBorderSize = destinationRegion.b - destinationRegion.t;
		sy = (y - destinationRegion.t) * sourceBorderSize / destinationBorderSize + sourceRegion.t;

		for (intptr_t x = destinationRegion.l; x < destinationRegion.r; x++) {
			intptr_t sx = x - destinationRegion.l + sourceRegion.l;

			intptr_t sourceBorderSize = sourceRegion.r - sourceRegion.l;
			intptr_t destinationBorderSize = destinationRegion.r - destinationRegion.l;
			sx = (x - destinationRegion.l) * sourceBorderSize / destinationBorderSize + sourceRegion.l;

			uint32_t *destinationPixel = destinationBits + x + y * destinationStride / 4;
			uint32_t *sourcePixel = sourceBits + sx + sy * sourceStride / 4;
			uint32_t modified = *sourcePixel;

			if (alpha != 0xFF) {
				modified = (modified & 0xFFFFFF) | (((((modified & 0xFF000000) >> 24) * alpha) << 16) & 0xFF000000);
			}

			BlendPixel(destinationPixel, modified, fullAlpha);
		}
	}
}

#ifndef KERNEL
void EsResizeArray(void **array, uintptr_t *allocated, uintptr_t needed, uintptr_t itemSize) {
	if (*allocated >= needed) {
		return;
	}

	uintptr_t oldAllocated = *allocated;
	void *oldArray = *array;

	uintptr_t newAllocated = oldAllocated * 2;
	if (newAllocated < needed) newAllocated = needed + 16;
	void *newArray = EsHeapAllocate(newAllocated * itemSize, false);

	EsMemoryCopy(newArray, oldArray, oldAllocated * itemSize);
	EsHeapFree(oldArray);

	*allocated = newAllocated;
	*array = newArray;
}
#endif

bool EsRectangleClip(EsRectangle parent, EsRectangle rectangle, EsRectangle *output) {
	EsRectangle current = parent;
	EsRectangle intersection;

	if (!((current.l > rectangle.r && current.r > rectangle.l)
			|| (current.t > rectangle.b && current.b > rectangle.t))) {
		intersection.l = current.l > rectangle.l ? current.l : rectangle.l;
		intersection.t = current.t > rectangle.t ? current.t : rectangle.t;
		intersection.r = current.r < rectangle.r ? current.r : rectangle.r;
		intersection.b = current.b < rectangle.b ? current.b : rectangle.b;
	} else {
		intersection = ES_MAKE_RECTANGLE(0, 0, 0, 0);
	}

	if (output) {
		*output = intersection;
	}

	return intersection.l < intersection.r && intersection.t < intersection.b;
}

EsRectangle EsRectangleAdd(EsRectangle a, EsRectangle b) {
	a.l += b.l;
	a.t += b.t;
	a.r += b.r;
	a.b += b.b;
	return a;
}

EsRectangle EsRectangleSubtract(EsRectangle a, EsRectangle b) {
	a.l -= b.l;
	a.t -= b.t;
	a.r -= b.r;
	a.b -= b.b;
	return a;
}

EsRectangle EsRectangleBounding(EsRectangle a, EsRectangle b) {
	if (a.l > b.l) a.l = b.l;
	if (a.t > b.t) a.t = b.t;
	if (a.r < b.r) a.r = b.r;
	if (a.b < b.b) a.b = b.b;
	return a;
}

EsRectangle EsRectangleIntersection(EsRectangle a, EsRectangle b) {
	if (a.l < b.l) a.l = b.l;
	if (a.t < b.t) a.t = b.t;
	if (a.r > b.r) a.r = b.r;
	if (a.b > b.b) a.b = b.b;
	return a;
}

size_t EsCStringLength(const char *string) {
	if (!string) {
		return 0;
	}

	size_t size = 0;

	while (true) {
		if (*string) {
			size++;
			string++;
		} else {
			return size;
		}
	}
}

size_t EsStringLength(const char *string, uint8_t end) {
	if (!string) {
		return 0;
	}

	size_t size = 0;

	while (true) {
		if (*string != end) {
			size++;
			string++;
		} else {
			return size;
		}
	}
}

#ifndef KERNEL
char *EsStringZeroTerminate(const char *string, ptrdiff_t stringBytes) {
	if (stringBytes == -1) stringBytes = EsCStringLength(string);
	char *result = (char *) EsHeapAllocate(stringBytes + 1, false);
	if (!result) return nullptr;
	result[stringBytes] = 0;
	EsMemoryCopy(result, string, stringBytes);
	return result;
}
#endif

void EsMemoryCopy(void *_destination, const void *_source, size_t bytes) {
	// TODO Prevent this from being optimised out in the kernel.

	if (!bytes) {
		return;
	}

	uint8_t *destination = (uint8_t *) _destination;
	uint8_t *source = (uint8_t *) _source;

#ifdef ARCH_X86_64
	while (bytes >= 16) {
		_mm_storeu_si128((__m128i *) destination, 
				_mm_loadu_si128((__m128i *) source));

		source += 16;
		destination += 16;
		bytes -= 16;
	}
#endif

	while (bytes >= 1) {
		((uint8_t *) destination)[0] = ((uint8_t *) source)[0];

		source += 1;
		destination += 1;
		bytes -= 1;
	}
}

void EsMemoryCopyReverse(void *_destination, void *_source, size_t bytes) {
	// TODO Prevent this from being optimised out in the kernel./

	if (!bytes) {
		return;
	}

	uint8_t *destination = (uint8_t *) _destination;
	uint8_t *source = (uint8_t *) _source;

	destination += bytes - 1;
	source += bytes - 1;

	while (bytes >= 1) {
		((uint8_t *) destination)[0] = ((uint8_t *) source)[0];

		source -= 1;
		destination -= 1;
		bytes -= 1;
	}
}

void EsMemoryZero(void *destination, size_t bytes) {
	// TODO Prevent this from being optimised out in the kernel.

	if (!bytes) {
		return;
	}

	for (uintptr_t i = 0; i < bytes; i++) {
		((uint8_t *) destination)[i] = 0;
	}
}

bool EsExtractArguments(char *string, size_t bytes, uint8_t delimiterByte, 
		uint8_t replacementDelimiter, size_t argvAllocated, char **argv, size_t *_argc) {
	size_t argc = 0;

	for (uintptr_t i = 0; i < bytes; i++) {
		if (string[i] == delimiterByte) {
			argc++;
		}
	}

	*_argc = argc;

	if (argc > argvAllocated) {
		return false;
	}

	char *next = string;

	for (uintptr_t i = 0; i < argc; i++) {
		argv[i] = next;
		size_t length = EsStringLength(next, delimiterByte);
		next[length] = replacementDelimiter;
		next += length + 1;
	}

	return true;
}

void EsMemoryMove(void *_start, void *_end, intptr_t amount, bool zeroEmptySpace) {
	// TODO Prevent this from being optimised out in the kernel./

	uint8_t *start = (uint8_t *) _start;
	uint8_t *end = (uint8_t *) _end;

	if (end < start) {
		EsPrint("MemoryMove end < start: %x %x %x %d\n", start, end, amount, zeroEmptySpace);
		return;
	}

	if (amount > 0) {
		EsMemoryCopyReverse(start + amount, start, end - start);

		if (zeroEmptySpace) {
			EsMemoryZero(start, amount);
		}
	} else if (amount < 0) {
		EsMemoryCopy(start + amount, start, end - start);

		if (zeroEmptySpace) {
			EsMemoryZero(end + amount, -amount);
		}
	}
}

int EsMemoryCompare(const void *a, const void *b, size_t bytes) {
	if (!bytes) {
		return 0;
	}

	const uint8_t *x = (const uint8_t *) a;
	const uint8_t *y = (const uint8_t *) b;

	for (uintptr_t i = 0; i < bytes; i++) {
		if (x[i] < y[i]) {
			return -1;
		} else if (x[i] > y[i]) {
			return 1;
		}
	}

	return 0;
}

uint8_t EsMemorySumBytes(uint8_t *source, size_t bytes) {
	if (!bytes) {
		return 0;
	}

	uint8_t total = 0;

	for (uintptr_t i = 0; i < bytes; i++) {
		total += source[i];
	}

	return total;
}

typedef void (*OSPutCharacterCallback)(int character, void *data); 

void _FormatInteger(OSPutCharacterCallback callback, void *callbackData, long value, int pad = 0, bool simple = false) {
	char buffer[32];
	if (value < 0) {
		callback('-', callbackData);
		value = -value;
	} else if (value == 0) {
		for (int i = 0; i < (pad ?: 1); i++) {
			callback('0', callbackData);
		}
		return;
	}
	int bp = 0;
	while (value) {
		buffer[bp++] = ('0' + (value % 10));
		value /= 10;
	}
	int cr = bp % 3;
	for (int i = 0; i < pad - bp; i++) {
		callback('0', callbackData);
	}
	for (int i = bp - 1; i >= 0; i--, cr--) {
		if (!cr && !pad) {
			if (i != bp - 1 && !simple) { callback(',', callbackData); }
			cr = 3;
		}

		callback(buffer[i], callbackData);
	}
}

void WriteCStringToCallback(OSPutCharacterCallback callback, void *callbackData, const char *cString) {
	while (cString && *cString) {
		callback(utf8_value(cString), callbackData);
		cString = utf8_advance(cString);
	}
}

void _StringFormat(OSPutCharacterCallback callback, void *callbackData, const char *format, va_list arguments) {
	int c;
	int pad = 0;
	uint32_t flags = 0;

	char buffer[32];
	const char *hexChars = "0123456789ABCDEF";

	while ((c = utf8_value((char *) format))) {
		if (c == '%') {
			repeat:;
			format = utf8_advance((char *) format);
			c = utf8_value((char *) format);

			switch (c) {
				case 'd': {
					long value = va_arg(arguments, long);
					_FormatInteger(callback, callbackData, value, pad, flags & ES_STRING_FORMAT_SIMPLE);
				} break;

				case 'i': {
					int value = va_arg(arguments, int);
					_FormatInteger(callback, callbackData, value, pad, flags & ES_STRING_FORMAT_SIMPLE);
				} break;

				case 'D': {
					long value = va_arg(arguments, long);

					if (value == 0) {
						WriteCStringToCallback(callback, callbackData, "(empty)");
					} else if (value < 1000) {
						_FormatInteger(callback, callbackData, value, pad);
						WriteCStringToCallback(callback, callbackData, " B");
					} else if (value < 1000000) {
						_FormatInteger(callback, callbackData, value / 1000, pad);
						callback('.', callbackData);
						_FormatInteger(callback, callbackData, (value / 100) % 10, pad);
						WriteCStringToCallback(callback, callbackData, " KB");
					} else if (value < 1000000000) {
						_FormatInteger(callback, callbackData, value / 1000000, pad);
						callback('.', callbackData);
						_FormatInteger(callback, callbackData, (value / 100000) % 10, pad);
						WriteCStringToCallback(callback, callbackData, " MB");
					} else {
						_FormatInteger(callback, callbackData, value / 1000000000, pad);
						callback('.', callbackData);
						_FormatInteger(callback, callbackData, (value / 100000000) % 10, pad);
						WriteCStringToCallback(callback, callbackData, " GB");
					}
				} break;

				case 'R': {
					EsRectangle value = va_arg(arguments, EsRectangle);
					callback('{', callbackData);
					_FormatInteger(callback, callbackData, value.l);
					callback('-', callbackData);
					callback('>', callbackData);
					_FormatInteger(callback, callbackData, value.r);
					callback(';', callbackData);
					_FormatInteger(callback, callbackData, value.t);
					callback('-', callbackData);
					callback('>', callbackData);
					_FormatInteger(callback, callbackData, value.b);
					callback('}', callbackData);
				} break;

				case 'X': {
					uintptr_t value = va_arg(arguments, uintptr_t);
					callback(hexChars[(value & 0xF0) >> 4], callbackData);
					callback(hexChars[(value & 0xF)], callbackData);
				} break;

				case 'W': {
					uintptr_t value = va_arg(arguments, uintptr_t);
					callback(hexChars[(value & 0xF000) >> 12], callbackData);
					callback(hexChars[(value & 0xF00) >> 8], callbackData);
					callback(hexChars[(value & 0xF0) >> 4], callbackData);
					callback(hexChars[(value & 0xF)], callbackData);
				} break;

				case 'x': {
					uintptr_t value = va_arg(arguments, uintptr_t);
					bool simple = flags & ES_STRING_FORMAT_SIMPLE;
					if (!simple) callback('0', callbackData);
					if (!simple) callback('x', callbackData);
					int bp = 0;
					while (value) {
						buffer[bp++] = hexChars[value % 16];
						value /= 16;
					}
					int j = 0, k = 0;
					for (int i = 0; i < 16 - bp; i++) {
						callback('0', callbackData);
						j++;k++;if (k != 16 && j == 4 && !simple) { callback('_',callbackData); } j&=3;
					}
					for (int i = bp - 1; i >= 0; i--) {
						callback(buffer[i], callbackData);
						j++;k++;if (k != 16 && j == 4 && !simple) { callback('_',callbackData); } j&=3;
					}
				} break;

				case 'c': {
					callback(va_arg(arguments, int), callbackData);
				} break;

				case '%': {
					callback('%', callbackData);
				} break;

				case 's': {
					size_t length = va_arg(arguments, size_t);
					char *string = va_arg(arguments, char *);
					char *position = string;

					while (position < string + length) {
						callback(utf8_value(position), callbackData);
						position = utf8_advance(position);
					}
				} break;

				case 'w': {
					size_t length = va_arg(arguments, size_t);
					uint16_t *string = va_arg(arguments, uint16_t *);
					uint16_t *position = string;

					// TODO UTF-16 decoding.

					while (position < string + length) {
						callback(*position, callbackData);
						position++;
					}
				} break;

				case 'L': {
					size_t length = va_arg(arguments, size_t);
					char *string = va_arg(arguments, char *);
					char *position = string + length - 1;

					if (length > 1) {
						length = 0;
						position--;

						while (*position != '/') {
							position--;
							length++;
							continue;
						}

						position++;
					}

					string = position;

					while (position < string + length) {
						callback(utf8_value(position), callbackData);
						position = utf8_advance(position);
					}
				} break;

				case 'z': {
					const char *string = va_arg(arguments, const char *);
					if (!string) string = "[null]";
					WriteCStringToCallback(callback, callbackData, string);
				} break;

				case 'S': {
					size_t length = va_arg(arguments, size_t);
					uint16_t *string = va_arg(arguments, uint16_t *);
					uint16_t *position = string;

					while (position != string + (length >> 1)) {
						callback((position[0] >> 8) & 0xFF, callbackData);
						callback(position[0] & 0xFF, callbackData);
						position++;
					}
				} break;

				case 'F': {
					double number = va_arg(arguments, double);

					if (__builtin_isnan(number)) {
						callback('N', callbackData);
						callback('a', callbackData);
						callback('N', callbackData);
						break;
					} else if (__builtin_isinf(number)) {
						if (number < 0) callback('-', callbackData);
						callback('i', callbackData);
						callback('n', callbackData);
						callback('f', callbackData);
						break;
					}

					if (number < 0) {
						callback('-', callbackData);
						number = -number;
					}

					uint64_t integer = (uint64_t) number;
					uint64_t fractional = 0;

					const int max = 12;
					int o = 0;
					double m = 1, n;

					number -= integer;

					while (o < max) {
						n = number * m;
						fractional = (uint64_t) n;

						if ((double) fractional == n) {
							break;
						} else {
							m *= 10;
							o++;
						}
					}

					if (o == max) o--;

					int bp = 0;

					if (!integer) {
						callback('0', callbackData);
					}

					while (integer) {
						buffer[bp++] = (integer % 10) + '0';
						integer /= 10;
					}

					while (--bp != -1) {
						callback(buffer[bp], callbackData);
					}

					if (fractional) {
						callback('.', callbackData);

						bp = 0;

						while (fractional) {
							buffer[bp++] = (fractional % 10) + '0';
							fractional /= 10;
							o--;
						}

						while (o--) buffer[bp++] = '0';

						int trim = 0;

						for (; trim < bp; trim++) {
							if (buffer[trim] != '0') {
								break;
							}
						}

						for (int roundUp = 0; roundUp < bp; roundUp++) {
							if (buffer[roundUp] != '9') {
								if (roundUp > 5) {
									buffer[roundUp]++;
									trim = roundUp;
								}

								break;
							}
						}

						bp--;

						if (pad) {
							while (bp >= trim && pad--) {
								callback(buffer[bp--], callbackData);
							}
						} else {
							while (bp >= trim) {
								callback(buffer[bp--], callbackData);
							}
						}
					}
				} break;

				case '*': {
					pad = va_arg(arguments, int);
					goto repeat;
				} break;

				case 'f': {
					flags = va_arg(arguments, uint32_t);
					goto repeat;
				} break;
			}

			pad = 0;
			flags = 0;
		} else {
			callback(c, callbackData);
		}

		format = utf8_advance((char *) format);
	}
}

typedef struct {
	char *buffer;
	size_t bytesRemaining, bytesWritten;
	bool full;
} EsStringFormatInformation;

void EsStringFormatCallback(int character, void *_fsi) {
	EsStringFormatInformation *fsi = (EsStringFormatInformation *) _fsi;

	if (fsi->full) {
		return;
	}

	char data[4];
	size_t bytes = utf8_encode(character, data);

	if (fsi->buffer) {
		if (fsi->bytesRemaining < bytes && fsi->bytesRemaining != ES_STRING_FORMAT_ENOUGH_SPACE) {
			fsi->full = true;
			return;
		} else {
			utf8_encode(character, fsi->buffer);
			fsi->buffer += bytes;
			fsi->bytesWritten += bytes;
			if (fsi->bytesRemaining != ES_STRING_FORMAT_ENOUGH_SPACE) fsi->bytesRemaining -= bytes;
		}
	}
}

#ifndef KERNEL

void *GetStreamPointer(EsAudioStream *source, uintptr_t offset) {
	uintptr_t position = source->readPointer;
	position += offset;
	if (position >= source->bufferBytes) position -= source->bufferBytes;
	if (position >= source->bufferBytes) return source->buffer;
	return source->buffer + position;
}

double GetSampleLQ(EsAudioStream *source, double time, uint8_t channel) {
	if (channel > source->format.channels) {
		channel %= source->format.channels;
	}

	time *= source->format.sampleRate;

	uintptr_t index1 = time, index2 = index1 + 1;
	double sample1, sample2;

	if (source->format.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		sample1 = (int8_t) (*(uint8_t *) GetStreamPointer(source, index1 * source->format.channels + channel) - 0x80) / 127.0;
		sample2 = (int8_t) (*(uint8_t *) GetStreamPointer(source, index2 * source->format.channels + channel) - 0x80) / 127.0;
	} else if (source->format.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
		sample1 = (*(int16_t *) GetStreamPointer(source, index1 * source->format.channels * 2 + channel * 2)) / 32767.0;
		sample2 = (*(int16_t *) GetStreamPointer(source, index2 * source->format.channels * 2 + channel * 2)) / 32767.0;
	} else if (source->format.sampleFormat == ES_SAMPLE_FORMAT_S32LE) {
		sample1 = (*(int32_t *) GetStreamPointer(source, index1 * source->format.channels * 4 + channel * 4)) / 2147483647.0;
		sample2 = (*(int32_t *) GetStreamPointer(source, index2 * source->format.channels * 4 + channel * 4)) / 2147483647.0;
	} else if (source->format.sampleFormat == ES_SAMPLE_FORMAT_F32LE) {
		sample1 = (*(float *) GetStreamPointer(source, index1 * source->format.channels * 4 + channel * 4));
		sample2 = (*(float *) GetStreamPointer(source, index2 * source->format.channels * 4 + channel * 4));
	} else {
		return 0;
	}

	double t = time - EsCRTfloor(time);
	return sample1 + (sample2 - sample1) * t;
}

EsError EsAudioStreamSend(EsAudioStream *destination, EsAudioStream *source, double *time) {
	EsAudioFormat destinationFormat = destination->format;
	EsAudioFormat sourceFormat = source->format;

	if (!source->bufferBytes || !destination->bufferBytes) {
		return ES_ERROR_BUFFER_TOO_SMALL;
	}

	if (destinationFormat.sampleFormat != ES_SAMPLE_FORMAT_U8
			&& destinationFormat.sampleFormat != ES_SAMPLE_FORMAT_S16LE
			&& destinationFormat.sampleFormat != ES_SAMPLE_FORMAT_S32LE
			&& destinationFormat.sampleFormat != ES_SAMPLE_FORMAT_F32LE) {
		return ES_ERROR_UNSUPPORTED_CONVERSION;
	}

	if (sourceFormat.sampleFormat != ES_SAMPLE_FORMAT_U8
			&& sourceFormat.sampleFormat != ES_SAMPLE_FORMAT_S16LE
			&& sourceFormat.sampleFormat != ES_SAMPLE_FORMAT_S32LE
			&& sourceFormat.sampleFormat != ES_SAMPLE_FORMAT_F32LE) {
		return ES_ERROR_UNSUPPORTED_CONVERSION;
	}

	size_t bytesPerDestinationSample = 
		  destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_U8    ? 1
		: destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_S16LE ? 2
		: destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_S32LE ? 4
		: destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_F32LE ? 4 : 0;
	size_t bytesPerSourceSample = 
		  sourceFormat.sampleFormat == ES_SAMPLE_FORMAT_U8    ? 1
		: sourceFormat.sampleFormat == ES_SAMPLE_FORMAT_S16LE ? 2
		: sourceFormat.sampleFormat == ES_SAMPLE_FORMAT_S32LE ? 4
		: sourceFormat.sampleFormat == ES_SAMPLE_FORMAT_F32LE ? 4 : 0;

	size_t bytesPerDestinationSampleFrame = bytesPerDestinationSample * destinationFormat.channels;
	size_t bytesPerSourceSampleFrame = bytesPerSourceSample * sourceFormat.channels;

	double timeIncrement = 1.0 / destinationFormat.sampleRate;

	size_t sourceSampleFramesAvailable = 
		  source->readPointer <= source->writePointer
		? ((source->writePointer - source->readPointer) / bytesPerSourceSampleFrame)
		: ((source->bufferBytes - source->readPointer + source->writePointer) / bytesPerSourceSampleFrame);

	if (!sourceSampleFramesAvailable) {
		return ES_ERROR_SOURCE_EMPTY;
	}

	double timeLimit = *time + (double) (sourceSampleFramesAvailable - 1) / sourceFormat.sampleRate;

	while (true) {
		uint32_t next = destination->writePointer + bytesPerDestinationSampleFrame;
		if (next == destination->bufferBytes) next = 0;
		if (next == destination->readPointer) return ES_SUCCESS;
		if (*time >= timeLimit) return ES_ERROR_SOURCE_EMPTY;

		if (destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_U8) {
			uint8_t *sample = (uint8_t *) (destination->buffer + destination->writePointer);

			for (uintptr_t i = 0; i < destinationFormat.channels; i++, sample++) {
				*sample = (uint8_t) (127.0 * GetSampleLQ(source, *time, i)) + 0x80;
			}
		} else if (destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
			int16_t *sample = (int16_t *) (destination->buffer + destination->writePointer);

			for (uintptr_t i = 0; i < destinationFormat.channels; i++, sample++) {
				*sample = (int16_t) (32767.0 * GetSampleLQ(source, *time, i));
			}
		} else if (destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_S32LE) {
			int32_t *sample = (int32_t *) (destination->buffer + destination->writePointer);

			for (uintptr_t i = 0; i < destinationFormat.channels; i++, sample++) {
				*sample = (int32_t) (2147483647.0 * GetSampleLQ(source, *time, i));
			}
		} else if (destinationFormat.sampleFormat == ES_SAMPLE_FORMAT_F32LE) {
			float *sample = (float *) (destination->buffer + destination->writePointer);

			for (uintptr_t i = 0; i < destinationFormat.channels; i++, sample++) {
				*sample = (float) GetSampleLQ(source, *time, i);
			}
		}

		*time = *time + timeIncrement;
		destination->writePointer = next;
	}

	return ES_SUCCESS;
}	

static EsMutex printMutex;

#define PRINT_BUFFER_SIZE (65536)
static char printBuffer[65536];
static uintptr_t printBufferPosition = 0;

void PrintCallback(int character, void *) {
	if (printBufferPosition >= PRINT_BUFFER_SIZE - 16) {
		EsSyscall(ES_SYSCALL_PRINT, (uintptr_t) printBuffer, printBufferPosition, 0, 0);
		printBufferPosition = 0;
	}

	printBufferPosition += utf8_encode(character, printBuffer + printBufferPosition); 
}

void EsPrint(const char *format, ...) {
	EsMutexAcquire(&printMutex);
	va_list arguments;
	va_start(arguments, format);
	_StringFormat(PrintCallback, nullptr, format, arguments);
	va_end(arguments);
	EsSyscall(ES_SYSCALL_PRINT, (uintptr_t) printBuffer, printBufferPosition, 0, 0);
	printBufferPosition = 0;
	EsMutexRelease(&printMutex);
}

// #define ENABLE_PRINT_BUFFERING

void PrintBuffered(const char *format, ...) {
	EsMutexAcquire(&printMutex);
	va_list arguments;
	va_start(arguments, format);
	_StringFormat(PrintCallback, nullptr, format, arguments);
	va_end(arguments);
	EsMutexRelease(&printMutex);
#ifndef ENABLE_PRINT_BUFFERING
	EsPrint("");
#endif
}

void EsPrintDirect(const char *string, ptrdiff_t stringLength) {
	if (stringLength == -1) stringLength = EsCStringLength(string);
	EsSyscall(ES_SYSCALL_PRINT, (uintptr_t) string, stringLength, 0, 0);
}
#endif

ptrdiff_t EsStringFormat(char *buffer, size_t bufferLength, const char *format, ...) {
	EsStringFormatInformation fsi = {buffer, bufferLength, 0};
	va_list arguments;
	va_start(arguments, format);
	_StringFormat(EsStringFormatCallback, &fsi, format, arguments);
	va_end(arguments);
	return fsi.bytesWritten;
}

ptrdiff_t EsStringFormatV(char *buffer, size_t bufferLength, const char *format, va_list arguments) {
	EsStringFormatInformation fsi = {buffer, bufferLength, 0};
	_StringFormat(EsStringFormatCallback, &fsi, format, arguments);
	return fsi.bytesWritten;
}

#ifndef KERNEL
char *EsStringAllocateAndFormatV(size_t *bytes, const char *format, va_list arguments1) {
	size_t needed = 0;

	va_list arguments2;
	va_copy(arguments2, arguments1);

	_StringFormat([] (int character, void *data) { 
		size_t *needed = (size_t *) data; 
		*needed = *needed + utf8_encode(character, nullptr); 
	}, &needed, format, arguments1);

	if (bytes) *bytes = needed;
	char *buffer = (char *) EsHeapAllocate(needed + 1, false);
	char *position = buffer;
	buffer[needed] = 0;

	_StringFormat([] (int character, void *data) { 
		char **position = (char **) data; 
		*position = *position + utf8_encode(character, *position); 
	}, &position, format, arguments2);

	va_end(arguments2);

	return buffer;
}

char *EsStringAllocateAndFormat(size_t *bytes, const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);
	char *buffer = EsStringAllocateAndFormatV(bytes, format, arguments);
	va_end(arguments);
	return buffer;
}

const char *EsStringFormatTemporary(const char *format, ...) {
	EsMessageMutexCheck();
	static char *buffer = nullptr;
	EsHeapFree(buffer);
	va_list arguments;
	va_start(arguments, format);
	size_t bytes = 0;
	buffer = EsStringAllocateAndFormatV(&bytes, format, arguments);
	va_end(arguments);
	return buffer;
}

double EsTimeStampMs() {
	if (!esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND]) return 0;
	return (double) EsTimeStamp() / esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND] / 1000;
}
#endif

struct RNGState {
	uint64_t s[4];
	EsSpinlock lock;
};

RNGState rngState;

void EsRandomAddEntropy(uint64_t x) {
	EsSpinlockAcquire(&rngState.lock);

	for (uintptr_t i = 0; i < 4; i++) {
		x += 0x9E3779B97F4A7C15;

		uint64_t result = x;
		result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
		result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
		rngState.s[i] ^= result ^ (result >> 31);
	}

	EsSpinlockRelease(&rngState.lock);
}

void EsRandomSeed(uint64_t x) {
	EsSpinlockAcquire(&rngState.lock);

	rngState.s[0] = rngState.s[1] = rngState.s[2] = rngState.s[3] = 0;

	for (uintptr_t i = 0; i < 4; i++) {
		x += 0x9E3779B97F4A7C15;

		uint64_t result = x;
		result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
		result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
		rngState.s[i] = result ^ (result >> 31);
	}

	EsSpinlockRelease(&rngState.lock);
}

uint64_t EsRandomU64() {
	EsSpinlockAcquire(&rngState.lock);

	uint64_t result = rngState.s[1] * 5;
	result = ((result << 7) | (result >> 57)) * 9;

	uint64_t t = rngState.s[1] << 17;
	rngState.s[2] ^= rngState.s[0];
	rngState.s[3] ^= rngState.s[1];
	rngState.s[1] ^= rngState.s[2];
	rngState.s[0] ^= rngState.s[3];
	rngState.s[2] ^= t;
	rngState.s[3] = (rngState.s[3] << 45) | (rngState.s[3] >> 19);

	EsSpinlockRelease(&rngState.lock);

	return result;
}

uint8_t EsRandomU8() {
	return (uint8_t) EsRandomU64();
}

void EsSortWithSwapCallback(void *_base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, EsGeneric), EsGeneric argument, void (*swap)(const void *, const void *, EsGeneric)) {
	if (nmemb <= 1) return;
	uint8_t *base = (uint8_t *) _base;
	intptr_t i = -1, j = nmemb;

	while (true) {
		while (compar(base + ++i * size, base, argument) < 0);
		while (compar(base + --j * size, base, argument) > 0);
		if (i >= j) break;
		swap(base + i * size, base + j * size, argument);
	}

	EsSortWithSwapCallback(base, ++j, size, compar, argument, swap);
	EsSortWithSwapCallback(base + j * size, nmemb - j, size, compar, argument, swap);
}

void EsSort(void *_base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, EsGeneric), EsGeneric argument) {
	if (nmemb <= 1) return;

	uint8_t *base = (uint8_t *) _base;
	uint8_t *swap = (uint8_t *) alloca(size);

	intptr_t i = -1, j = nmemb;

	while (true) {
		while (compar(base + ++i * size, base, argument) < 0);
		while (compar(base + --j * size, base, argument) > 0);

		if (i >= j) break;

		EsMemoryCopy(swap, base + i * size, size);
		EsMemoryCopy(base + i * size, base + j * size, size);
		EsMemoryCopy(base + j * size, swap, size);
	}

	EsSort(base, ++j, size, compar, argument);
	EsSort(base + j * size, nmemb - j, size, compar, argument);
}

int64_t EsStringParseInteger(const char **string, size_t *length, int base) {
	int64_t value = 0;
	bool overflow = false;

	while (*length) {
		char c = (*string)[0];

		int64_t digit = 0;

		if (c >= 'a' && c <= 'z') {
			digit = c - 'a' + 10;
		} else if (c >= 'A' && c <= 'Z') {
			digit = c - 'A' + 10;
		} else if (c >= '0' && c <= '9') {
			digit = c - '0';
		} else {
			break;
		}

		if (digit >= base) {
			break;
		}

		int64_t oldValue = value;

		value *= base;
		value += digit;

		if (value / base != oldValue) {
			overflow = true;
		}

		(*string)++;
		(*length)--;
	}

	if (overflow) value = LONG_MAX;
	return value;
}

int EsStringCompareRaw(const char *s1, ptrdiff_t length1, const char *s2, ptrdiff_t length2) {
	if (length1 == -1) length1 = EsCStringLength(s1);
	if (length2 == -1) length2 = EsCStringLength(s2);

	while (length1 || length2) {
		if (!length1) return -1;
		if (!length2) return 1;

		char c1 = *s1;
		char c2 = *s2;

		if (c1 != c2) {
			return c1 - c2;
		}

		length1--;
		length2--;
		s1++;
		s2++;
	}

	return 0;
}

int EsStringCompare(const char *s1, ptrdiff_t _length1, const char *s2, ptrdiff_t _length2) {
	if (_length1 == -1) _length1 = EsCStringLength(s1);
	if (_length2 == -1) _length2 = EsCStringLength(s2);
	size_t length1 = _length1, length2 = _length2;

	while (length1 || length2) {
		if (!length1) return -1;
		if (!length2) return 1;

		char c1 = *s1;
		char c2 = *s2;

		if (c1 >= '0' && c1 <= '9' && c2 >= '0' && c2 <= '9') {
			int64_t n1 = EsStringParseInteger(&s1, &length1, 10);
			int64_t n2 = EsStringParseInteger(&s2, &length2, 10);

			if (n1 != n2) {
				if (n1 > n2) return  1;
				if (n1 < n2) return -1;
			}
		} else {
			if (c1 >= 'a' && c1 <= 'z') c1 = c1 - 'a' + 'A';
			if (c2 >= 'a' && c2 <= 'z') c2 = c2 - 'a' + 'A';
			if (c1 == '.') c1 = ' '; else if (c1 == ' ') c1 = '.';
			if (c2 == '.') c2 = ' '; else if (c2 == ' ') c2 = '.';

			if (c1 != c2) {
				if (c1 > c2) return  1;
				if (c1 < c2) return -1;
			}

			length1--;
			length2--;
			s1++;
			s2++;
		}
	}

	return EsStringCompareRaw(s1, _length1, s2, _length2);
}

bool EsStringStartsWith(const char *string, intptr_t _stringBytes, const char *prefix, intptr_t _prefixBytes, bool caseInsensitive) {
	if (_stringBytes == -1) _stringBytes = EsCStringLength(string);
	if (_prefixBytes == -1) _prefixBytes = EsCStringLength(prefix);
	size_t stringBytes = _stringBytes, prefixBytes = _prefixBytes;

	while (true) {
		if (!prefixBytes) return true;
		if (!stringBytes) return false;

		char c1 = *string;
		char c2 = *prefix;

		if (caseInsensitive) {
			if (c1 >= 'a' && c1 <= 'z') c1 = c1 - 'a' + 'A';
			if (c2 >= 'a' && c2 <= 'z') c2 = c2 - 'a' + 'A';
		}

		if (c1 != c2) return false;

		stringBytes--;
		prefixBytes--;
		string++;
		prefix++;
	}
}

uint32_t EsColorParse(const char *string, ptrdiff_t bytes) {
	if (bytes == -1) {
		bytes = EsCStringLength(string);
	}

	int digits[8], digitCount = 0;
	ptrdiff_t position = 0;

	while (position != bytes && !EsCRTisxdigit(string[position])) {
		position++;
	}

	for (int i = 0; i < 8 && position != bytes; i++) {
		char c = string[position++];

		if (EsCRTisxdigit(c)) {
			digits[digitCount++] = EsCRTisdigit(c) ? (c - '0') : EsCRTisupper(c) ? (c - 'A' + 10) : (c - 'a' + 10);
		} else {
			break;
		}
	}

	uint32_t color = 0;

	if (digitCount == 3) {
		color = 0xFF000000 | (digits[0] << 20) | (digits[0] << 16) | (digits[1] << 12) | (digits[1] << 8) | (digits[2] << 4) | (digits[2] << 0);
	} else if (digitCount == 4) {
		color = (digits[0] << 28 | digits[0] << 24) | (digits[1] << 20) | (digits[1] << 16) 
			| (digits[2] << 12) | (digits[2] << 8) | (digits[3] << 4) | (digits[3] << 0);
	} else if (digitCount == 5) {
		color = (digits[0] << 28 | digits[1] << 24) | (digits[2] << 20) | (digits[2] << 16) 
			| (digits[3] << 12) | (digits[3] << 8) | (digits[4] << 4) | (digits[4] << 0);
	} else if (digitCount == 6) {
		color = 0xFF000000 | (digits[0] << 20) | (digits[1] << 16) | (digits[2] << 12) | (digits[3] << 8) | (digits[4] << 4) | (digits[5] << 0);
	} else if (digitCount == 8) {
		color = (digits[0] << 28) | (digits[1] << 24) | (digits[2] << 20) | (digits[3] << 16) 
			| (digits[4] << 12) | (digits[5] << 8) | (digits[6] << 4) | (digits[7] << 0);
	}

	return color;
}

static int64_t ConvertCharacterToDigit(int character, int base) {
	int64_t result = -1;

	if (character >= '0' && character <= '9') {
		result = character - '0';
	} else if (character >= 'A' && character <= 'Z') {
		result = character - 'A' + 10;
	} else if (character >= 'a' && character <= 'z') {
		result = character - 'a' + 10;
	}

	if (result >= base) {
		result = -1;
	}

	return result;
}

#ifndef KERNEL
void EsPrintHelloWorld() {
	EsPrint("Hello, world.\n");
}

size_t EsPathFindUniqueName(char *buffer, size_t originalBytes, size_t bufferBytes) {
	size_t length;
	uintptr_t attempt = 1;

	while (attempt < 1000) {
		if (attempt == 1) {
			length = originalBytes;
		} else {
			length = originalBytes + EsStringFormat(buffer + originalBytes, bufferBytes - originalBytes, " %d", attempt);
		}

		if (!EsPathExists(buffer, length)) {
			return length;
		} else {
			attempt++;
		}
	}

	return 0;
}

uint8_t *EsImageLoad(uint8_t *file, size_t fileSize, uint32_t *imageX, uint32_t *imageY, int imageChannels) {
#ifdef USE_STB_IMAGE
	int unused;
	return stbi_load_from_memory(file, fileSize, (int *) imageX, (int *) imageY, &unused, imageChannels);
#else
	(void) imageChannels;
	PNGReader reader = {};
	reader.buffer = file;
	reader.bytes = fileSize;
	uint32_t *bits;
	bool success = PNGParse(&reader, &bits, imageX, imageY, [] (size_t s) { return EsHeapAllocate(s, false); }, EsHeapFree);
	return success ? (uint8_t *) bits : nullptr;
#endif
}

void LoadImage(const void *path, ptrdiff_t pathBytes, void *destination, int destinationWidth, int destinationHeight, bool fromMemory) {
	int width = 0, height = 0;
	uint32_t *image = nullptr;

	if (!fromMemory) {
		size_t fileSize;
		void *file = EsFileReadAll((const char *) path, pathBytes, &fileSize);

		if (file) {
			image = (uint32_t *) EsImageLoad((uint8_t *) file, fileSize, (uint32_t *) &width, (uint32_t *) &height, 4);
			EsHeapFree(file);
		}
	} else {
		image = (uint32_t *) EsImageLoad((uint8_t *) path, pathBytes, (uint32_t *) &width, (uint32_t *) &height, 4);
	}

	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			uint32_t in = image[i + j * width];
			uint32_t red = ((in & 0xFF) << 16), green = (in & 0xFF00FF00), blue = ((in & 0xFF0000) >> 16);
			image[i + j * width] = red | green | blue;
		}
	}

	int cx = destinationWidth / 2 - width / 2, cy = destinationHeight / 2 - height / 2;

	for (int j = 0; j < destinationHeight; j++) {
		uint32_t *pixel = (uint32_t *) ((uint8_t *) destination + j * destinationWidth * 4);

		for (int i = 0; i < destinationWidth; i++, pixel++) {
			if (i - cx < 0 || i - cx >= width || j - cy < 0 || j - cy >= height) {
				*pixel = 0x738393;
			} else {
				*pixel = image[i - cx + (j - cy) * width];
			}
		}
	}

	EsHeapFree(image);
}
#endif

void EsSpinlockAcquire(EsSpinlock *spinlock) {
	__sync_synchronize();
	while (__sync_val_compare_and_swap(&spinlock->state, 0, 1));
	__sync_synchronize();
}

void EsSpinlockRelease(EsSpinlock *spinlock) {
	__sync_synchronize();

	if (!spinlock->state) {
#ifdef KERNEL
		KernelPanic("EsSpinlockRelease - Spinlock %x not acquired.\n", spinlock);
#else
		EsProcessCrash(ES_FATAL_ERROR_SPINLOCK_NOT_ACQUIRED, EsLiteral("EsSpinlockRelease - Spinlock not acquired.\n"));
#endif
	}

	spinlock->state = 0;
	__sync_synchronize();
}

#ifndef KERNEL
void EsMutexAcquire(EsMutex *mutex) {
	bool acquired = false;

	while (true) {
		EsSpinlockAcquire(&mutex->spinlock);

		if (mutex->event == ES_INVALID_HANDLE) {
			mutex->event = EsEventCreate(false);
		}

		if (mutex->state == 0) {
			acquired = true;
			mutex->state = 1;
		} 

		if (acquired) {
			// TODO Test this.
			EsSpinlockRelease(&mutex->spinlock);
			return;
		}

		__sync_fetch_and_add(&mutex->queued, 1); 
		EsEventReset(mutex->event);
		EsSpinlockRelease(&mutex->spinlock);
		EsWaitSingle(mutex->event);
		__sync_fetch_and_sub(&mutex->queued, 1);
	} 
}

void EsMutexRelease(EsMutex *mutex) {
	volatile bool queued = false;

	EsSpinlockAcquire(&mutex->spinlock);

	if (!mutex->state) {
		EsProcessCrash(ES_FATAL_ERROR_MUTEX_NOT_ACQUIRED_BY_THREAD, EsLiteral("EsMutexRelease - Mutex not acquired."));
	}

	mutex->state = 0;

	if (mutex->queued) {
		queued = true;
		EsEventSet(mutex->event);
	}

	EsSpinlockRelease(&mutex->spinlock);

	if (queued) {
		EsSchedulerYield();
	}
}

void EsMutexDestroy(EsMutex *mutex) {
	if (mutex->event != ES_INVALID_HANDLE) {
		EsHandleClose(mutex->event);
	}
}

#endif

int64_t EsIntegerParse(const char *text, ptrdiff_t bytes) {
	if (bytes == -1) bytes = EsCStringLength(text);

	int base = 10;

	if (bytes > 2 && text[0] == '0' && text[1] == 'x') {
		text += 2, bytes -= 2;
		base = 16;
	}

	const char *end = text + bytes;

	bool negative = false;
	int64_t result = 0;

	while (text < end) {
		char c = *text;

		if (c == '-') {
			negative = true;
		}

		if (c >= '0' && c <= '9') {
			result *= base;
			result += c - '0';
		} else if (c >= 'A' && c <= 'F' && base == 16) {
			result *= base;
			result += c - 'A' + 10;
		} else if (c >= 'a' && c <= 'f' && base == 16) {
			result *= base;
			result += c - 'a' + 10;
		}

		text++;
	}

	return negative ? -result : result;
}

bool EsStringFormatAppendV(char *buffer, size_t bufferLength, size_t *bufferPosition, const char *format, va_list arguments) {
	buffer += *bufferPosition;
	bufferLength -= *bufferPosition;
	EsStringFormatInformation fsi = {buffer, bufferLength, 0};
	_StringFormat(EsStringFormatCallback, &fsi, format, arguments);
	*bufferPosition += fsi.bytesWritten;
	return !fsi.full;
}

bool EsStringFormatAppend(char *buffer, size_t bufferLength, size_t *bufferPosition, const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);
	bool result = EsStringFormatAppendV(buffer, bufferLength, bufferPosition, format, arguments);
	va_end(arguments);
	return result;
}

void EsMemoryFill(void *from, void *to, uint8_t byte) {
	uint8_t *a = (uint8_t *) from;
	uint8_t *b = (uint8_t *) to;
	while (a != b) *a = byte, a++;
}

uint16_t ByteSwap16(uint16_t x) {
	return (x << 8) | (x >> 8);
}

uint32_t ByteSwap32(uint32_t x) {
	return    ((x & 0xFF000000) >> 24) 
		| ((x & 0x000000FF) << 24) 
		| ((x & 0x00FF0000) >> 8) 
		| ((x & 0x0000FF00) << 8);
}

uint16_t SwapBigEndian16(uint16_t x) {
	return ByteSwap16(x);
}

uint32_t SwapBigEndian32(uint32_t x) {
	return ByteSwap32(x);
}

#ifdef KERNEL

#if 0
int __tsi = 1;
#define TS(...) for (int i = 0; i < __tsi; i++) EsPrint("]   "); EsPrint(__VA_ARGS__); uint64_t __ts = KGetTimeInMs(); __tsi++; \
			   EsDefer({__tsi--; for (int i = 0; i < __tsi; i++) EsPrint("]   "); \
					   EsPrint("> %d ms\n", (KGetTimeInMs() - __ts)); \
					   for (int i = 0; i < __tsi; i++) EsPrint("]   "); EsPrint("\n");})
#define TSP(...) for (int i = 0; i < __tsi; i++) EsPrint("]   "); EsPrint(__VA_ARGS__, (KGetTimeInMs() - __ts));
#else
#define TS(...) 
#define TSP(...) 
#endif

#else

#if 0
int __tsi = 1;
#define TS(...) for (int i = 0; i < __tsi; i++) PrintBuffered("|   "); PrintBuffered(__VA_ARGS__); uint64_t __ts = EsTimeStamp(); __tsi++; \
			   EsDefer({__tsi--; for (int i = 0; i < __tsi; i++) PrintBuffered("|   "); \
					   PrintBuffered("> %d ms (%d mcs)\n", (EsTimeStamp() - __ts) / (esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND] * 1000 + 1), (EsTimeStamp() - __ts) / (esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND] + 1)); \
					   for (int i = 0; i < __tsi; i++) PrintBuffered("|   "); PrintBuffered("\n");})
#define TSP(...) for (int i = 0; i < __tsi; i++) PrintBuffered("|   "); PrintBuffered(__VA_ARGS__, (EsTimeStamp() - __ts) / \
		(esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND] * 1000 + 1));
#else
#define TS(...) 
#define TSP(...) 
#endif

#endif

void *EsCRTmemset(void *s, int c, size_t n) {
	uint8_t *s8 = (uint8_t *) s;
	for (uintptr_t i = 0; i < n; i++) {
		s8[i] = (uint8_t) c;
	}
	return s;
}

void *EsCRTmemcpy(void *dest, const void *src, size_t n) {
	uint8_t *dest8 = (uint8_t *) dest;
	const uint8_t *src8 = (const uint8_t *) src;
	for (uintptr_t i = 0; i < n; i++) {
		dest8[i] = src8[i];
	}
	return dest;
}

void *EsCRTmemmove(void *dest, const void *src, size_t n) {
	if ((uintptr_t) dest < (uintptr_t) src) {
		return EsCRTmemcpy(dest, src, n);
	} else {
		uint8_t *dest8 = (uint8_t *) dest;
		const uint8_t *src8 = (const uint8_t *) src;
		for (uintptr_t i = n; i; i--) {
			dest8[i - 1] = src8[i - 1];
		}
		return dest;
	}
}

char *EsCRTstrdup(const char *string) {
	if (!string) return nullptr;
	size_t length = EsCRTstrlen(string) + 1;
	char *memory = (char *) EsCRTmalloc(length);
	if (!memory) return nullptr;
	EsCRTmemcpy(memory, string, length);
	return memory;
}

size_t EsCRTstrlen(const char *s) {
	size_t n = 0;
	while (s[n]) n++;
	return n;
}

size_t EsCRTstrnlen(const char *s, size_t maxlen) {
	size_t n = 0;
	while (s[n] && maxlen--) n++;
	return n;
}

int EsCRTabs(int n) {
	if (n < 0)	return 0 - n;
	else		return n;
}

#ifndef KERNEL
void *EsCRTmalloc(size_t size) {
	void *x = EsHeapAllocate(size, false);
	return x;
}

void *EsCRTcalloc(size_t num, size_t size) {
	return EsHeapAllocate(num * size, true);
}

void EsCRTfree(void *ptr) {
	EsHeapFree(ptr);
}

void *EsCRTrealloc(void *ptr, size_t size) {
	return EsHeapReallocate(ptr, size, false);
}
#else
void *EsHeapAllocate(size_t size, bool zeroMemory, KernelHeap kernelHeap);
void EsHeapFree(void *address, size_t expectedSize, KernelHeap kernelHeap);
void *EsHeapReallocate(void *oldAddress, size_t newAllocationSize, bool zeroNewSpace, KernelHeap heap);

void *EsCRTmalloc(size_t size) {
	void *x = EsHeapAllocate(size, false, K_FIXED);
	return x;
}

void *EsCRTcalloc(size_t num, size_t size) {
	return EsHeapAllocate(num * size, true, K_FIXED);
}

void EsCRTfree(void *ptr) {
	EsHeapFree(ptr, 0, K_FIXED);
}

void *EsCRTrealloc(void *ptr, size_t size) {
	return EsHeapReallocate(ptr, size, false, K_FIXED);
}
#endif

char *EsCRTgetenv(const char *name) {
	(void) name;
	return nullptr;
}

int EsCRTtoupper(int c) {
	if (c >= 'a' && c <= 'z') {
		return c - 'a' + 'A';
	} else {
		return c;
	}
}

int EsCRTtolower(int c) {
	if (c >= 'A' && c <= 'Z') {
		return c - 'A' + 'a';
	} else {
		return c;
	}
}

int EsCRTstrcasecmp(const char *s1, const char *s2) {
	while (true) {
		if (*s1 != *s2 && EsCRTtolower(*s1) != EsCRTtolower(*s2)) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}
}

int EsCRTstrncasecmp(const char *s1, const char *s2, size_t n) {
	while (n--) {
		if (*s1 != *s2 && EsCRTtolower(*s1) != EsCRTtolower(*s2)) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}

	return 0;
}

int EsCRTstrcmp(const char *s1, const char *s2) {
	while (true) {
		if (*s1 != *s2) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}
}

int EsCRTstrncmp(const char *s1, const char *s2, size_t n) {
	while (n--) {
		if (*s1 != *s2) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}

	return 0;
}

int EsCRTisspace(int c) {
	if (c == ' ')  return 1;
	if (c == '\f') return 1;
	if (c == '\n') return 1;
	if (c == '\r') return 1;
	if (c == '\t') return 1;
	if (c == '\v') return 1;

	return 0;
}

uint64_t EsCRTstrtoul(const char *nptr, char **endptr, int base) {
	// TODO errno

	if (base > 36) return 0;

	while (EsCRTisspace(*nptr)) {
		nptr++;
	}

	if (*nptr == '+') {
		nptr++;
	} else if (*nptr == '-') {
		nptr++;
	}

	if (base == 0) {
		if (nptr[0] == '0' && (nptr[1] == 'x' || nptr[1] == 'X')) {
			base = 16;
			nptr += 2;
		} else if (nptr[0] == '0') {
			EsPrint("WARNING: strtoul with base=0, detected octal\n");
			base = 8; // Why?!?
			nptr++;
		} else {
			base = 10;
		}
	}

	uint64_t value = 0;
	bool overflow = false;

	while (true) {
		int64_t digit = ConvertCharacterToDigit(*nptr, base);

		if (digit != -1) {
			nptr++;

			uint64_t x = value;
			value *= base;
			value += (uint64_t) digit;

			if (value / base != x) {
				overflow = true;
			}
		} else {
			break;
		}
	}

	if (overflow) {
		value = ULONG_MAX;
	}

	if (endptr) {
		*endptr = (char *) nptr;
	}

	return value;
}

size_t EsCRTstrcspn(const char *s, const char *reject) {
	size_t count = 0;

	while (true) {
		char character = *s;
		if (!character) return count;

		const char *search = reject;

		while (true) {
			char c = *search;

			if (!c) {
				goto match;
			} else if (character == c) {
				break;
			}

			search++;
		}

		return count;

		match:;
		count++;
		s++;
	}
}

char *EsCRTstrsep(char **stringp, const char *delim) {
	char *string = *stringp;

	if (!string) {
		return NULL;
	}

	size_t tokenLength = EsCRTstrcspn(string, delim);

	if (string[tokenLength] == 0) {
		*stringp = NULL;
	} else {
		string[tokenLength] = 0;
		*stringp = string + tokenLength + 1;
	}

	return string;
}

char *EsCRTstrcat(char *dest, const char *src) {
	char *o = dest;
	dest += EsCRTstrlen(dest);

	while (*src) {
		*dest = *src;
		src++;
		dest++;
	}

	*dest = 0;

	return o;
}

double EsDoubleParse(const char *nptr, ptrdiff_t maxBytes, char **endptr) {
	if (maxBytes == -1) maxBytes = EsCStringLength(nptr);
	const char *end = nptr + maxBytes;
	if (nptr == end) return 0;

	while (nptr != end && EsCRTisspace(*nptr)) {
		nptr++;
	}

	if (nptr == end) return 0;

	bool positive = true;

	if (*nptr == '+') {
		positive = true;
		nptr++;
	} else if (*nptr == '-') {
		positive = false;
		nptr++;
	}

	if (nptr == end) return 0;

	double value = 0, scale = 0.1;
	bool seenDecimalPoint = false;

	while (nptr != end) {
		char c = *nptr;

		if (c == '.' && !seenDecimalPoint) {
			seenDecimalPoint = true;
		} else if (c >= '0' && c <= '9') {
			if (seenDecimalPoint) {
				value += scale * (c - '0');
				scale *= 0.1;
			} else {
				value = value * 10;
				value += c - '0';
			}
		} else {
			break;
		}

		nptr++;
	}

	if (!positive) {
		value = -value;
	}

	if (endptr) *endptr = (char *) nptr;
	return value;
}

long int EsCRTstrtol(const char *nptr, char **endptr, int base) {
	// TODO errno

	if (base > 36) return 0;

	while (EsCRTisspace(*nptr)) {
		nptr++;
	}

	bool positive = true;

	if (*nptr == '+') {
		positive = true;
		nptr++;
	} else if (*nptr == '-') {
		positive = false;
		nptr++;
	}

	if (base == 0) {
		if (nptr[0] == '0' && (nptr[1] == 'x' || nptr[1] == 'X')) {
			base = 16;
			nptr += 2;
		} else if (nptr[0] == '0') {
			EsPrint("WARNING: strtol with base=0, detected octal\n");
			base = 8; // Why?!?
			nptr++;
		} else {
			base = 10;
		}
	}

	int64_t value = 0;
	bool overflow = false;

	while (true) {
		int64_t digit = ConvertCharacterToDigit(*nptr, base);

		if (digit != -1) {
			nptr++;

			int64_t x = value;
			value *= base;
			value += digit;

			if (value / base != x) {
				overflow = true;
			}
		} else {
			break;
		}
	}

	if (!positive) {
		value = -value;
	}

	if (overflow) {
		value = positive ? LONG_MAX : LONG_MIN;
	}

	if (endptr) {
		*endptr = (char *) nptr;
	}

	return value;
}

int EsCRTatoi(const char *nptr) {
	return (int) EsCRTstrtol(nptr, NULL, 10);
}

char *EsCRTstrstr(const char *haystack, const char *needle) {
	size_t haystackLength = EsCRTstrlen(haystack);
	size_t needleLength = EsCRTstrlen(needle);

	if (haystackLength < needleLength) {
		return nullptr;
	}

	for (uintptr_t i = 0; i <= haystackLength - needleLength; i++) {
		for (uintptr_t j = 0; j < needleLength; j++) {
			if (haystack[i + j] != needle[j]) {
				goto tryNext;
			}
		}

		return (char *) haystack + i;

		tryNext:;
	}

	return nullptr;
}

void EsCRTqsort(void *_base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)) {
	if (nmemb <= 1) return;

	uint8_t *base = (uint8_t *) _base;
	uint8_t *swap = (uint8_t *) alloca(size);

	intptr_t i = -1, j = nmemb;

	while (true) {
		while (compar(base + ++i * size, base) < 0);
		while (compar(base + --j * size, base) > 0);

		if (i >= j) break;

		EsCRTmemcpy(swap, base + i * size, size);
		EsCRTmemcpy(base + i * size, base + j * size, size);
		EsCRTmemcpy(base + j * size, swap, size);
	}

	EsCRTqsort(base, ++j, size, compar);
	EsCRTqsort(base + j * size, nmemb - j, size, compar);
}

char *EsCRTstrcpy(char *dest, const char *src) {
	size_t stringLength = EsCRTstrlen(src);
	EsCRTmemcpy(dest, src, stringLength + 1);
	return dest;
}

char *EsCRTstpcpy(char *dest, const char *src) {
	size_t stringLength = EsCRTstrlen(src);
	EsCRTmemcpy(dest, src, stringLength + 1);
	return dest + stringLength;
}

size_t EsCRTstrspn(const char *s, const char *accept) {
	size_t count = 0;

	while (true) {
		char character = *s;

		const char *search = accept;

		while (true) {
			char c = *search;

			if (!c) {
				break;
			} else if (character == c) {
				goto match;
			}

			search++;
		}

		return count;

		match:;
		count++;
		s++;
	}
}

char *EsCRTstrrchr(const char *s, int c) {
	const char *start = s;
	if (!s[0]) return NULL;
	s += EsCRTstrlen(s) - 1;

	while (true) {
		if (*s == c) {
			return (char *) s;
		}

		if (s == start) {
			return NULL;
		}

		s--;
	}
}

char *EsCRTstrchr(const char *s, int c) {
	while (true) {
		if (*s == c) {
			return (char *) s;
		}

		if (*s == 0) {
			return NULL;
		}

		s++;
	}
}

char *EsCRTstrncpy(char *dest, const char *src, size_t n) {
	size_t i;

	for (i = 0; i < n && src[i]; i++) {
		dest[i] = src[i];
	}

	for (; i < n; i++) {
		dest[i] = 0;
	}

	return dest;
}

char *EsCRTstrlcpy(char *dest, const char *src, size_t n) {
	size_t i;

	for (i = 0; i < n - 1 && src[i]; i++) {
		dest[i] = src[i];
	}

	for (; i < n; i++) {
		dest[i] = 0;
	}

	return dest;
}

int EsCRTmemcmp(const void *s1, const void *s2, size_t n) {
	return EsMemoryCompare((void *) s1, (void *) s2, n);
}

void *EsCRTmemchr(const void *_s, int _c, size_t n) {
	uint8_t *s = (uint8_t *) _s;
	uint8_t c = (uint8_t) _c;

	for (uintptr_t i = 0; i < n; i++) {
		if (s[i] == c) {
			return s + i;
		}
	}

	return nullptr;
}

int EsCRTisalpha(int c) {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int EsCRTisdigit(int c) {
	return (c >= '0' && c <= '9');
}

int EsCRTrand() {
	uint8_t a = EsRandomU8();
	uint8_t b = EsRandomU8();
	uint8_t c = EsRandomU8();
	return (a << 16) | (b << 8) | (c << 0);
}

int EsCRTisalnum(int c) {
	return EsCRTisalpha(c) || EsCRTisdigit(c);
}

int EsCRTiscntrl(int c) {
	return c < 0x20 || c == 0x7F;
}

int EsCRTisgraph(int c) {
	return c > ' ' && c < 0x7F;
}

int EsCRTislower(int c) {
	return c >= 'a' && c <= 'z';
}

int EsCRTisprint(int c) {
	return c >= ' ' && c < 127;
}

int EsCRTispunct(int c) {
	return c != ' ' && !EsCRTisalnum(c);
}

int EsCRTisupper(int c) {
	return c >= 'A' && c <= 'Z';
}

int EsCRTisxdigit(int c) {
	return EsCRTisdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
}

char *EsCRTsetlocale(int category, const char *locale) {
	(void) category;
	(void) locale;
	return nullptr;
}

void EsCRTsrand(unsigned int seed) {
	EsRandomSeed(seed);
}

#ifndef KERNEL

void EsCRTexit(int status) {
	EsProcessTerminate(ES_CURRENT_PROCESS, status);
}

void EsCRTabort() {
	EsProcessCrash(ES_ERROR_UNKNOWN, EsLiteral("EsCRTabort\n"));
	while (true);
}

#endif

int EsCRTstrcoll(const char *s1, const char *s2) {
	return EsStringCompare(s1, EsCStringLength(s1), s2, EsCStringLength(s2));
}

char *EsCRTstrerror(int errnum) {
	(void) errnum;
	return (char*) "unknown operation failure";
}

char *EsCRTstrpbrk(const char *s, const char *accept) {
	size_t l1 = EsCStringLength(s), l2 = EsCStringLength(accept);

	for (uintptr_t i = 0; i <  l1; i++) {
		char c = s[i];

		for (uintptr_t j = 0; j < l2; j++) {
			if (accept[j] == c) {
				return (char *) (i + s);
			}
		}
	}

	return nullptr;
}

#ifdef USE_STB_SPRINTF
int EsCRTsprintf(char *buffer, const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);
	int length = stbsp_vsprintf(buffer, format, arguments);
	va_end(arguments);
	return length;
}

int EsCRTsnprintf(char *buffer, size_t bufferSize, const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);
	int length = stbsp_vsnprintf(buffer, bufferSize, format, arguments);
	va_end(arguments);
	return length;
}

int EsCRTvsnprintf(char *buffer, size_t bufferSize, const char *format, va_list arguments) {
	return stbsp_vsnprintf(buffer, bufferSize, format, arguments);
}
#endif

#include <shared/math.cpp>

#endif
