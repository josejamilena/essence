struct FolderEntryPair {
	char *key;
	FolderEntry *value;
};

void InstanceAddContents(struct Instance *instance, FolderEntryPair *newEntries, size_t newEntryCount);
void InstanceAddSingle(struct Instance *instance, ListEntry newEntry);
void InstanceRemoveContents(struct Instance *instance);
ListEntry InstanceRemoveSingle(Instance *instance, FolderEntry *folderEntry);

struct Folder {
	/* DS_MAP */ FolderEntryPair *entries;
	EsArena entryArena;

	uint64_t flags;

	DS_ARRAY(Instance *) attachedInstances;
	DS_ARRAY(Instance *) attachingInstances;

	String path; 	// TODO Use EsNodeGetPath to update this (and the instance copies) when an instance is focused
			// 	OR when searching through loaded folders in FolderAttachInstance.
	bool recurse;

	// Called on the blocking task thread:
	EsError (*createChildFolder)(Folder *folder, String name);
	EsError (*renameItem)(Folder *folder, FolderEntry *entry, String name);

	EsDirectoryMonitor *monitor;
};

EsMutex loadedFoldersMutex;
DS_ARRAY(Folder *) loadedFolders;
EsArena folderArena;

#define MAXIMUM_FOLDERS_WITH_NO_ATTACHED_INSTANCES (0)
DS_ARRAY(Folder *) foldersWithNoAttachedInstances;

void FolderEntryCloseHandle(FolderEntry *entry) {
	entry->handles--;

	if (!entry->handles) {
		String name = entry->GetName();
		StringDestroy(name);
		EsArenaFree(&entry->folder->entryArena, entry);
	}
}

void FolderDestroy(Folder *folder) {
	for (uintptr_t i = 0; i < shlenu(folder->entries); i++) {
		FolderEntryCloseHandle(folder->entries[i].value);
	}

	if (folder->monitor) {
		EsDirectoryMonitorDestroy(folder->monitor);
	}

	EsArenaFree(&folderArena, folder);
}

void FolderDetachInstance(Instance *instance) {
	Folder *folder = instance->folder;
	if (!folder) return;
	instance->folder = nullptr;

	bool found = false;

	for (uintptr_t i = 0; i < arrlenu(folder->attachedInstances); i++) {
		if (folder->attachedInstances[i] == instance) {
			arrdelswap(folder->attachedInstances, i);
			found = true;
			break;
		}
	}

	EsAssert(found); // Instance not attached to folder correctly.

	if (!arrlen(folder->attachedInstances) && !arrlen(folder->attachingInstances)) {
		arrput(foldersWithNoAttachedInstances, folder);

		if (arrlen(foldersWithNoAttachedInstances) > MAXIMUM_FOLDERS_WITH_NO_ATTACHED_INSTANCES) {
			Folder *leastRecentlyUsed = foldersWithNoAttachedInstances[0];

			found = false;

			for (uintptr_t i = 0; i < arrlenu(loadedFolders); i++) {
				if (loadedFolders[i] == leastRecentlyUsed) {
					arrdelswap(loadedFolders, i);
					found = true;
					break;
				}
			}

			EsAssert(found); // Folder not in loadedFolders array.

			arrdel(foldersWithNoAttachedInstances, 0);
			FolderDestroy(leastRecentlyUsed);
		}
	}
}

FolderEntry *FolderAddEntry(Folder *folder, const char *_name, size_t nameBytes, EsDirectoryChild *information) {
	char *name = (char *) EsHeapAllocate(nameBytes + 1, false);
	name[nameBytes] = 0;
	EsMemoryCopy(name, _name, nameBytes);

	FolderEntry *entry = shget(folder->entries, name);

	if (!entry) {
		entry = (FolderEntry *) EsArenaAllocate(&folder->entryArena, true);
		shput(folder->entries, name, entry);
		entry->handles = 1; 
		entry->folder = folder;

		entry->name = name;
		entry->nameBytes = nameBytes;
		entry->extensionOffset = PathGetExtension(entry->GetName()).text - name;
	} else {
		EsHeapFree(name);
		name = entry->name;
	}

	entry->size = information->fileSize;
	entry->isFolder = information->type == ES_NODE_DIRECTORY;
	entry->sizeUnknown = entry->isFolder && information->directoryChildren == ES_DIRECTORY_CHILDREN_UNKNOWN;

	return entry;
}

void FolderAddEntries(Folder *folder, EsDirectoryChild *buffer, size_t entryCount) {
	for (uintptr_t i = 0; i < entryCount; i++) {
		FolderAddEntry(folder, buffer[i].name, buffer[i].nameBytes, &buffer[i]);
	}
}

void FolderNotify(EsDirectoryMonitor *, int event, const char *_path, size_t _pathBytes, const char *_oldPath, size_t _oldPathBytes, EsGeneric _folder) {
	Folder *folder = (Folder *) _folder.p;
	String path = { .text = (char *) _path, .bytes = _pathBytes };
	String oldPath = { .text = (char *) _oldPath, .bytes = _oldPathBytes };

	// TODO Asynchronous processing.
	// TODO Subtree notifications.
	// TODO FS_MODIFY.
	
#if 0
	size_t oldFullPathBytes = folder->path.bytes + oldPath.bytes;
	char *oldFullPath = (char *) EsHeapAllocate(oldFullPathBytes, false);
	EsMemoryCopy(oldFullPath, folder->path.text, folder->path.bytes);
	EsMemoryCopy(oldFullPath + folder->path.bytes, oldPath.text, oldPath.bytes);

	EsHeapFree(oldFullPath);
#endif

#if 1
	(void) folder;
	(void) path;
	(void) oldPath;
	(void) event;

	// TODO Get working again once directory monitors are added back in Kernel.
#else
	if (event == ES_MSG_FS_CREATE) {
		// EsPrint("Create %s in %s\n", STRFMT(path), STRFMT(folder->path));

		EsNodeInformation information;

		if (ES_SUCCESS == EsNodeOpenRelative(folder->handle, path.text, path.bytes, ES_NODE_FAIL_IF_NOT_FOUND, &information)) {
			EsHandleClose(information.handle);

			EsMutexAcquire(&loadedFoldersMutex);
			FolderEntry *entry = FolderAddEntry(folder, path.text, path.bytes, &information);
			ListEntry listEntry = { .entry = entry };

			for (uintptr_t i = 0; i < arrlenu(folder->attachedInstances); i++) {
				InstanceAddSingle(folder->attachedInstances[i], listEntry);
			}

			EsMutexRelease(&loadedFoldersMutex);
		}
	} else if (event == ES_MSG_FS_DELETE) {
		// EsPrint("Delete %s in %s\n", STRFMT(path), STRFMT(folder->path));

		EsMutexAcquire(&loadedFoldersMutex);

		if (!path.bytes) {
			// TODO Deleting a monitored folder.
		}

		char oldChar = path.text[path.bytes];
		path.text[path.bytes] = 0;
		FolderEntry *entry = shget(folder->entries, path.text);
		path.text[path.bytes] = oldChar;

		if (entry) {
			for (uintptr_t i = 0; i < arrlenu(folder->attachedInstances); i++) {
				InstanceRemoveSingle(folder->attachedInstances[i], entry);
			}

			shdel(folder->entries, entry->name);
			FolderEntryCloseHandle(entry);
		}

		EsMutexRelease(&loadedFoldersMutex);
	} else if (event == ES_MSG_FS_MOVE) {
		// EsPrint("Move %s to %s in %s\n", STRFMT(oldPath), STRFMT(path), STRFMT(folder->path));

		EsMutexAcquire(&loadedFoldersMutex);

		if (!path.bytes) {
			// TODO Moving a monitored folder.
		}

		char oldChar = oldPath.text[oldPath.bytes];
		oldPath.text[oldPath.bytes] = 0;
		FolderEntry *entry = shget(folder->entries, oldPath.text);
		oldPath.text[oldPath.bytes] = oldChar;

		if (entry) {
			uint8_t oldNameBytes = entry->nameBytes, oldExtensionOffset = entry->extensionOffset;
			char *oldName = entry->name;

			uint8_t newNameBytes = path.bytes, newExtensionOffset;
			char *newName = (char *) EsHeapAllocate(path.bytes + 1, false);

			newName[path.bytes] = 0;
			EsMemoryCopy(newName, path.text, path.bytes);
			newExtensionOffset = PathGetExtension({ .text = newName, .bytes = newNameBytes }).text - newName;

			for (uintptr_t i = 0; i < arrlenu(folder->attachedInstances); i++) {
				Instance *instance = folder->attachedInstances[i];
				entry->name = oldName, entry->nameBytes = oldNameBytes, entry->extensionOffset = oldExtensionOffset;
				ListEntry listEntry = InstanceRemoveSingle(instance, entry);
				listEntry.entry = entry;
				entry->name = newName, entry->nameBytes = newNameBytes, entry->extensionOffset = newExtensionOffset;
				InstanceAddSingle(instance, listEntry);
			}

			entry->name = newName, entry->nameBytes = newNameBytes, entry->extensionOffset = newExtensionOffset;
			shput(folder->entries, newName, entry);
			EsHeapFree(oldName); // StringDestroy(oldName);
		}

		EsMutexRelease(&loadedFoldersMutex);
	}
#endif
}

EsError FolderAttachInstance(Instance *instance, String path, bool recurse, Folder **newFolder) {
	// (Called on the blocking task thread.)
	
	Folder *folder = nullptr;

	// Check if we've already loaded the folder.

	EsMutexAcquire(&loadedFoldersMutex);

	for (uintptr_t i = 0; i < arrlenu(loadedFolders); i++) {
		if (StringEquals(loadedFolders[i]->path, path) && loadedFolders[i]->recurse == recurse) {
			folder = loadedFolders[i];
			goto success;
		}
	}

	EsMutexRelease(&loadedFoldersMutex);

	// Load a new folder.

	folder = (Folder *) EsArenaAllocate(&folderArena, true);

	folder->path = StringDuplicate(path);
	folder->recurse = recurse;

	folder->createChildFolder = [] (Folder *folder, String name) {
		size_t pathBytes;
		char *path = EsStringAllocateAndFormat(&pathBytes, "%s%s", STRFMT(folder->path), STRFMT(name));
		EsError error = EsPathCreate(path, pathBytes, ES_NODE_DIRECTORY, false);
		EsHeapFree(path);
		return error;
	};

	folder->renameItem = [] (Folder *folder, FolderEntry *entry, String newName) {
		String name = entry->GetName();

		size_t oldPathBytes;
		char *oldPath = EsStringAllocateAndFormat(&oldPathBytes, "%s%s", STRFMT(folder->path), STRFMT(name));

		size_t newPathBytes;
		char *newPath = EsStringAllocateAndFormat(&newPathBytes, "%s%s", STRFMT(folder->path), STRFMT(newName));

		EsError error = EsPathMove(oldPath, oldPathBytes, newPath, newPathBytes);

		EsHeapFree(oldPath);
		EsHeapFree(newPath);

		return error;
	};

	EsArenaInitialise(&folder->entryArena, 1048576, sizeof(FolderEntry));
	arrput(loadedFolders, folder);

	{
		// Get the initial directory children.
		// TODO Make this asynchronous for some folder providers, or recursive requests.
		// TODO Other folder providers (e.g. archive files).
		// TODO Recurse mode.

		EsNodeType type;

		if (!EsPathExists(STRING(path), &type) || type != ES_NODE_DIRECTORY) {
			return ES_ERROR_FILE_DOES_NOT_EXIST;
		}

		// Create the monitor before we enumerate children so that we don't miss any entries.
		uint64_t monitorFlags = ES_DIRECTORY_MONITOR_CONTENTS | ES_DIRECTORY_MONITOR_MODIFY;
		if (recurse) monitorFlags |= ES_DIRECTORY_MONITOR_SUBTREE;
		folder->monitor = EsDirectoryMonitorCreate(STRING(folder->path), monitorFlags, FolderNotify, folder);

		EsDirectoryChild *buffer = nullptr;
		ptrdiff_t _entryCount = EsDirectoryEnumerateChildren(STRING(folder->path), &buffer);

		if (ES_CHECK_ERROR(_entryCount)) {
			EsHeapFree(buffer);
			EsDirectoryMonitorDestroy(folder->monitor);
			EsArenaFree(&folderArena, folder);
			return (EsError) _entryCount;
		}

		FolderAddEntries(folder, buffer, _entryCount);
		EsHeapFree(buffer);
	}

	EsMutexAcquire(&loadedFoldersMutex);

	success:;

	for (uintptr_t i = 0; i < arrlenu(foldersWithNoAttachedInstances); i++) {
		if (foldersWithNoAttachedInstances[i] == folder) {
			arrdelswap(foldersWithNoAttachedInstances, i);
			break;
		}
	}

	arrput(folder->attachingInstances, instance);

	EsMutexRelease(&loadedFoldersMutex);

	*newFolder = folder;
	return ES_SUCCESS;
}
