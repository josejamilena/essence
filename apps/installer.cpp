#include <essence.h>

void _start() {
	_init();

	EsWindow *window = EsWindowCreate(nullptr, ES_WINDOW_PLAIN);

	EsRectangle screen;
	EsSyscall(ES_SYSCALL_GET_SCREEN_BOUNDS, 0, (uintptr_t) &screen, 0, 0);
	EsSyscall(ES_SYSCALL_WINDOW_MOVE, EsWindowGetHandle(window), (uintptr_t) &screen, 0, ES_MOVE_WINDOW_ALWAYS_ON_TOP);
	EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, EsWindowGetHandle(window), ES_WINDOW_SOLID_TRUE, 0, 0);

	EsPanel *root = EsPanelCreate(window, ES_PANEL_VERTICAL | ES_CELL_PUSH | ES_CELL_CENTER, ES_STYLE_INSTALLER_ROOT);
	EsTextDisplayCreate(root, ES_CELL_H_FILL, ES_STYLE_TEXT_HEADING0, EsLiteral("Essence Installation"));

	while (true) EsMessageReceive();
}
