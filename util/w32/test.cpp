#include <essence.h>

const EsStyle styleTestPanel = {
	.inherit = ES_STYLE_PANEL_WINDOW_BACKGROUND,
	
	.metrics = {
		.mask = ES_THEME_METRICS_INSETS | ES_THEME_METRICS_GAP_MAJOR,
		.insets = ES_MAKE_RECTANGLE_ALL(20),
		.gapMajor = 10,
	},
};

const EsStyle styleTestContainer = {
	.inherit = ES_STYLE_PANEL_GROUP_BOX,
	
	.metrics = {
		.mask = ES_THEME_METRICS_INSETS | ES_THEME_METRICS_GAP_MAJOR | ES_THEME_METRICS_CLIP_ENABLED,
		.insets = ES_MAKE_RECTANGLE_ALL(20),
		.clipInsets = ES_MAKE_RECTANGLE_ALL(1),
		.clipEnabled = true,
		.gapMajor = 10,
	},
};

const EsStyle styleBigIcon = {
	.metrics = {
		.mask = ES_THEME_METRICS_PREFERRED_WIDTH | ES_THEME_METRICS_PREFERRED_HEIGHT | ES_THEME_METRICS_ICON_SIZE,
		.preferredWidth = 100,
		.preferredHeight = 100,
		.iconSize = 100,
	},
};

const EsStyle styleSwitchTest = {
	.inherit = ES_STYLE_PANEL_GROUP_BOX,
	
	.metrics = {
		.mask = ES_THEME_METRICS_PREFERRED_WIDTH | ES_THEME_METRICS_PREFERRED_HEIGHT,
		.preferredWidth = 150,
		.preferredHeight = 90,
	},
};

const EsStyle styleButtonRow = {
	.metrics = {
		.mask = ES_THEME_METRICS_GAP_MAJOR,
		.gapMajor = 10,
	},
};

const EsStyle styleParagraph = {
	.inherit = ES_STYLE_TEXT_PARAGRAPH,

	.metrics = {
		.mask = ES_THEME_METRICS_TEXT_SIZE,
		.textSize = 60,
	},
};
	
EsPanel *switcher;
EsElement *switch1, *switch2;
bool switchToggle;

void SwitchSlide(EsInstance *, EsElement *, EsCommand *) {
	if (switchToggle) EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_SLIDE_UP);
	else EsPanelSwitchTo(switcher, switch1, ES_TRANSITION_SLIDE_DOWN);
	switchToggle = !switchToggle;
}

void SwitchSlideParallax(EsInstance *, EsElement *, EsCommand *) {
	if (switchToggle) EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_SLIDE_UP_OVER);
	else EsPanelSwitchTo(switcher, switch1, ES_TRANSITION_SLIDE_DOWN_UNDER);
	switchToggle = !switchToggle;
}

void SwitchCover(EsInstance *, EsElement *, EsCommand *) {
	if (switchToggle) EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_COVER_UP);
	else EsPanelSwitchTo(switcher, switch1, ES_TRANSITION_COVER_DOWN);
	switchToggle = !switchToggle;
}

void SwitchZoom(EsInstance *, EsElement *, EsCommand *) {
	if (switchToggle) EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_ZOOM_IN);
	else EsPanelSwitchTo(switcher, switch1, ES_TRANSITION_ZOOM_OUT);
	switchToggle = !switchToggle;
}

void SwitchFade(EsInstance *, EsElement *, EsCommand *) {
	if (switchToggle) EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_FADE_IN, ES_FLAGS_DEFAULT, 500);
	else EsPanelSwitchTo(switcher, switch1, ES_TRANSITION_FADE_IN, ES_FLAGS_DEFAULT, 500);
	switchToggle = !switchToggle;
}

void _start() {
	while (true) {
		EsMessage *message = EsMessageReceive();
		
		if (message->type == ES_MSG_INSTANCE_CREATE) {
			EsInstance *instance = EsInstanceCreate(message, "Hello");
			EsPanel *panel = EsPanelCreate(instance->window, ES_CELL_FILL, &styleTestPanel);
			
#if 0
			EsButtonCreate(panel, ES_FLAGS_DEFAULT, 0, "Push button", -1);
			EsTextDisplayCreate(panel, ES_FLAGS_DEFAULT, ES_STYLE_TEXT_HEADING0, "Hello, world!", -1);
			EsTextboxCreate(panel, ES_FLAGS_DEFAULT);
			EsIconDisplayCreate(panel, ES_FLAGS_DEFAULT, &styleBigIcon, ES_ICON_EDIT_COPY);
			EsButtonCreate(EsWindowGetToolbar(instance->window), ES_BUTTON_TOOLBAR, 0, "Toolbar button");
			EsPanel *row = EsPanelCreate(panel, ES_PANEL_HORIZONTAL, &styleButtonRow);
			row->cName = "button row";
			EsButton *button = EsButtonCreate(row, ES_FLAGS_DEFAULT, 0, "Slide", -1);
			EsButtonOnCommand(button, SwitchSlide);
			button = EsButtonCreate(row, ES_FLAGS_DEFAULT, 0, "Slide parallax", -1);
			EsButtonOnCommand(button, SwitchSlideParallax);
			button = EsButtonCreate(row, ES_FLAGS_DEFAULT, 0, "Cover", -1);
			EsButtonOnCommand(button, SwitchCover);
			button = EsButtonCreate(row, ES_FLAGS_DEFAULT, 0, "Zoom", -1);
			EsButtonOnCommand(button, SwitchZoom);
			button = EsButtonCreate(row, ES_FLAGS_DEFAULT, 0, "Fade", -1);
			EsButtonOnCommand(button, SwitchFade);
			switcher = EsPanelCreate(panel, ES_PANEL_SWITCHER, &styleSwitchTest);
			switch1 = EsButtonCreate(switcher, ES_FLAGS_DEFAULT, 0, "Button");
			switch2 = EsTextDisplayCreate(switcher, ES_FLAGS_DEFAULT, ES_STYLE_TEXT_HEADING2, "Text display");
			EsPanelSwitchTo(switcher, switch2, ES_TRANSITION_NONE);
			EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_LABEL, "hello\tworld");
			EsTextDisplayCreate(panel, ES_CELL_H_FILL, &styleParagraph, "test\ttest\nhello\tworld\nalpha\tbeta\n\t123\t456\na\t\t456");
			EsTextDisplayCreate(panel, ES_CELL_H_FILL | ES_TEXT_DISPLAY_RICH_TEXT, &styleParagraph, "Script (\aw2]test\a]) Привет. こんにちは 言語 Test.");
			EsTextDisplayCreate(panel, ES_CELL_H_FILL, &styleParagraph,
					"Every time I break word-wrapping I have to come up with some random text to test it out, and I never know what to write.");
			EsTextDisplayCreate(panel, ES_CELL_H_FILL | ES_TEXT_DISPLAY_RICH_TEXT, &styleParagraph,
					"\aw2]Looking for a change of \aw2i]mind.\n"
					"\aw3]Looking for a change of \aw3i]mind.\n"
					"\aw4]Looking for a change of \aw4i]mind.\n"
					"\aw5]Looking for a change of \aw5i]mind.\n"
					"\aw6]Looking for a change of \aw6i]mind.\n"
					"\aw7]Looking for a change of \aw7i]mind.\n"
					"\aw8]Looking for a change of \aw8i]mind.\n"
					"\aw9]Looking for a change of \aw9i]mind.\n");
			EsTextDisplayCreate(panel, ES_CELL_H_FILL | ES_TEXT_DISPLAY_RICH_TEXT, &styleParagraph, "hello\aw7]world\a]test");
#endif

			EsPerformanceTimerPush();
			size_t count = 1e6;
			EsPanel *container = EsPanelCreate(panel, ES_CELL_FILL | ES_PANEL_V_SCROLL_AUTO, &styleTestContainer);

			for (uintptr_t i = 0; i < count; i++) {
				EsPanel *row = EsPanelCreate(container, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL);
				char buffer[32];
				size_t bytes = EsStringFormat(buffer, sizeof(buffer), "Textbox \ai]number\a] %d: ", i + 1);
				EsTextDisplayCreate(row, ES_TEXT_DISPLAY_RICH_TEXT, 0, buffer, bytes);
				EsSpacerCreate(row, ES_FLAGS_DEFAULT, 0, 10, 0);
				EsTextboxCreate(row, ES_FLAGS_DEFAULT);
			}

			EsPrint("Time to create %d item rows: %Fs.\n", count, EsPerformanceTimerPop());
		}
	}
}
