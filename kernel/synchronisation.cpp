#ifdef IMPLEMENTATION

#ifdef DEBUG_BUILD
uintptr_t nextMutexID;
#endif

void KSpinlock::Acquire() {
	if (scheduler.panic) return;

	bool _interruptsEnabled = ProcessorAreInterruptsEnabled();
	ProcessorDisableInterrupts();

	CPULocalStorage *storage = GetLocalStorage();

#ifdef DEBUG_BUILD
	if (storage && storage->currentThread && owner && owner == storage->currentThread) {
		KernelPanic("KSpinlock::Acquire - Attempt to acquire a spinlock owned by the current thread (%x/%x, CPU: %d/%d).\nAcquired at %x.\n", 
				storage->currentThread, owner, storage->processorID, ownerCPU, acquireAddress);
	}
#endif

	if (storage) {
		storage->spinlockCount++;
	}

	while (__sync_val_compare_and_swap(&state, 0, 1));
	__sync_synchronize();

	interruptsEnabled = _interruptsEnabled;

	if (storage) {
#ifdef DEBUG_BUILD
		owner = storage->currentThread;
#endif
		ownerCPU = storage->processorID;
	} else {
		// Because spinlocks can be accessed very early on in initialisation there may not be
		// a CPULocalStorage available for the current processor. Therefore, just set this field to nullptr.

#ifdef DEBUG_BUILD
		owner = nullptr;
#endif
	}

#ifdef DEBUG_BUILD
	acquireAddress = (uintptr_t) __builtin_return_address(0);
#endif
}

void KSpinlock::Release(bool force) {
	if (scheduler.panic) return;

	CPULocalStorage *storage = GetLocalStorage();

	if (storage) {
		storage->spinlockCount--;
	}

	if (!force) {
		AssertLocked();
	}
	
	volatile bool wereInterruptsEnabled = interruptsEnabled;

#ifdef DEBUG_BUILD
	owner = nullptr;
#endif
	__sync_synchronize();
	state = 0;

	if (wereInterruptsEnabled) ProcessorEnableInterrupts();

#ifdef DEBUG_BUILD
	releaseAddress = (uintptr_t) __builtin_return_address(0);
#endif
}

void KSpinlock::AssertLocked() {
	if (scheduler.panic) return;

#ifdef DEBUG_BUILD
	CPULocalStorage *storage = GetLocalStorage();

	if (!state || ProcessorAreInterruptsEnabled() 
			|| (storage && owner != storage->currentThread)) {
#else
	if (!state || ProcessorAreInterruptsEnabled()) {
#endif
		KernelPanic("KSpinlock::AssertLocked - KSpinlock not correctly acquired\n"
				"Return address = %x.\n"
				"state = %d, ProcessorAreInterruptsEnabled() = %d, this = %x\n",
				__builtin_return_address(0), state, 
				ProcessorAreInterruptsEnabled(), this);
	}
}

Thread *AttemptMutexAcquisition(KMutex *mutex, Thread *currentThread) {
	scheduler.lock.Acquire();

	Thread *old = mutex->owner;

	if (!old) {
		mutex->owner = currentThread;
	}

	scheduler.lock.Release();

	return old;
}

bool KMutex::Acquire(bool poll) {
	if (scheduler.panic) return false;

	Thread *currentThread = GetCurrentThread();
	bool hasThread = currentThread;

	if (!currentThread) {
		currentThread = (Thread *) 1;
	} else {
		if (currentThread->terminatableState == THREAD_TERMINATABLE) {
			KernelPanic("KMutex::Acquire - Thread is terminatable.\n");
		}
	}

	if (hasThread && owner && owner == currentThread) {
#ifdef DEBUG_BUILD
		KernelPanic("KMutex::Acquire - Attempt to acquire mutex (%x) at %x owned by current thread (%x) acquired at %x.\n", 
				this, __builtin_return_address(0), currentThread, acquireAddress);
#else
		KernelPanic("KMutex::Acquire - Attempt to acquire mutex (%x) at %x owned by current thread (%x).\n", 
				this, __builtin_return_address(0), currentThread);
#endif
	}

	if (!ProcessorAreInterruptsEnabled()) {
		KernelPanic("KMutex::Acquire - Trying to acquire a mutex while interrupts are disabled.\n");
	}

	while (AttemptMutexAcquisition(this, currentThread)) {
		__sync_synchronize();

		if (poll) {
			return false;
		}

		if (GetLocalStorage() && GetLocalStorage()->schedulerReady) {
			// Instead of spinning on the lock, 
			// let's tell the scheduler to not schedule this thread
			// until it's released.
			scheduler.WaitMutex(this);

			if (currentThread->terminating && currentThread->terminatableState == THREAD_USER_BLOCK_REQUEST) {
				// We didn't acquire the mutex because the thread is terminating.
				return false;
			}
		}
	}

	__sync_synchronize();

	if (owner != currentThread) {
		KernelPanic("KMutex::Acquire - Invalid owner thread (%x, expected %x).\n", owner, currentThread);
	}

#ifdef DEBUG_BUILD
	acquireAddress = (uintptr_t) __builtin_return_address(0);
	AssertLocked();

	if (!id) {
		id = __sync_fetch_and_add(&nextMutexID, 1);
	}

	// EsPrint("$%x:%x:1\n", owner, id);
#endif

	return true;
}

void KMutex::Release() {
	if (scheduler.panic) return;

	AssertLocked();
	Thread *currentThread = GetCurrentThread();
	scheduler.lock.Acquire();

#ifdef DEBUG_BUILD
	// EsPrint("$%x:%x:0\n", owner, id);
#endif

	if (currentThread) {
		Thread *temp = __sync_val_compare_and_swap(&owner, currentThread, nullptr);
		if (currentThread != temp) KernelPanic("KMutex::Release - Invalid owner thread (%x, expected %x).\n", temp, currentThread);
	} else owner = nullptr;

	volatile bool preempt = blockedThreads.count;

	if (scheduler.started) {
		// NOTE We unblock all waiting threads, because of how blockedThreadPriorities works.
		scheduler.NotifyObject(&blockedThreads, true, currentThread); 
	}

	scheduler.lock.Release();
	__sync_synchronize();

#ifdef DEBUG_BUILD
	releaseAddress = (uintptr_t) __builtin_return_address(0);
#endif

#ifdef PREEMPT_AFTER_MUTEX_RELEASE
	if (preempt) ProcessorFakeTimerInterrupt();
#endif
}

void KMutex::AssertLocked() {
	Thread *currentThread = GetCurrentThread();

	if (!currentThread) {
		currentThread = (Thread *) 1;
	}

	if (owner != currentThread) {
#ifdef DEBUG_BUILD
		KernelPanic("KMutex::AssertLocked - Mutex not correctly acquired\n"
				"currentThread = %x, owner = %x\nthis = %x\nReturn %x/%x\nLast used from %x->%x\n", 
				currentThread, owner, this, __builtin_return_address(0), __builtin_return_address(1), 
				acquireAddress, releaseAddress);
#else
		KernelPanic("KMutex::AssertLocked - Mutex not correctly acquired\n"
				"currentThread = %x, owner = %x\nthis = %x\nReturn %x\n", 
				currentThread, owner, this, __builtin_return_address(0));
#endif
	}
}

bool KSemaphore::Poll() {
	bool success = false;
	mutex.Acquire();
	if (units) { success = true; units--; }
	if (!units && available.state) available.Reset();
	mutex.Release();
	return success;
}

#if 0
bool KSemaphore::Take(uintptr_t u, uintptr_t timeoutMs) {
	uintptr_t taken = 0;

	while (u) {
		if (!available.Wait(timeoutMs)) {
			Return(taken);
			return false;
		}

		mutex.Acquire();
		if (units) { units--; u--; taken++; }
		if (!units && available.state) available.Reset();
		mutex.Release();

		lastTaken = (uintptr_t) __builtin_return_address(0);
	}

	return true;
}
#endif

bool KSemaphore::Take(uintptr_t u, uintptr_t timeoutMs) {
	// New version: all-or-nothing approach.
	// The old version is above if I change my mind.

	uintptr_t taken = 0;

	while (u) {
		if (!available.Wait(timeoutMs)) {
			Return(taken);
			return false;
		}

		mutex.Acquire();
		if (units >= u) { units -= u; u = 0; taken += u; }
		if (!units && available.state) available.Reset();
		mutex.Release();

		lastTaken = (uintptr_t) __builtin_return_address(0);
	}

	return true;
}

void KSemaphore::Return(uintptr_t u) {
	mutex.Acquire();
	if (!available.state) available.Set();
	units += u;
	mutex.Release();
}

void KSemaphore::Set(uintptr_t u) {
	mutex.Acquire();
	if (!available.state && u) available.Set();
	else if (available.state && !u) available.Reset();
	units = u;
	mutex.Release();
}

EsError EventSink::Push(EsGeneric data) {
	EsError result = ES_SUCCESS;

	scheduler.lock.AssertLocked();

	spinlock.Acquire();

	if (queueCount == ES_MAX_EVENT_SINK_BUFFER_SIZE) {
		overflow = true;
		result = ES_ERROR_EVENT_SINK_OVERFLOW;
		KernelLog(LOG_VERBOSE, "Event Sinks", "push overflow", "Push %d into %x.\n", data.i, this);
	} else {
		bool ignored = false;

		if (ignoreDuplicates) {
			uintptr_t index = queuePosition;

			for (uintptr_t i = 0; i < queueCount; i++) {
				if (queue[index] == data) {
					ignored = true;
					result = ES_ERROR_EVENT_SINK_DUPLICATE;
					KernelLog(LOG_VERBOSE, "Event Sinks", "push ignored", "Push %d into %x.\n", data.i, this);
					break;
				}

				index++;

				if (index == ES_MAX_EVENT_SINK_BUFFER_SIZE) {
					index = 0;
				}
			}
		}

		if (!ignored) {
			uintptr_t writeIndex = queuePosition + queueCount;
			if (writeIndex >= ES_MAX_EVENT_SINK_BUFFER_SIZE) writeIndex -= ES_MAX_EVENT_SINK_BUFFER_SIZE;
			if (writeIndex >= ES_MAX_EVENT_SINK_BUFFER_SIZE) KernelPanic("EventSink::Push - Invalid event sink (%x) queue.\n", this);
			KernelLog(LOG_VERBOSE, "Event Sinks", "push", "Push %d into %x at %d (%d/%d).\n", data.i, this, writeIndex, queuePosition, queueCount);
			queue[writeIndex] = data;
			queueCount++;
			available.Set(true, true);
		}
	}

	spinlock.Release();

	return result;
}

bool KEvent::Set(bool schedulerAlreadyLocked, bool maybeAlreadySet) {
	if (state && !maybeAlreadySet) {
		KernelLog(LOG_ERROR, "Synchronisation", "event already set", "KEvent::Set - Attempt to set a event that had already been set\n");
	}

	if (!schedulerAlreadyLocked) {
		scheduler.lock.Acquire();
	}
	
	volatile bool unblockedThreads = false;

	if (!state) {
		if (sinkTable) {
			for (uintptr_t i = 0; i < ES_MAX_EVENT_FORWARD_COUNT; i++) {
				if (!sinkTable[i].sink) continue;
				sinkTable[i].sink->Push(sinkTable[i].data);
			}
		}

		state = true;

		if (scheduler.started) {
			if (blockedThreads.count) {
				unblockedThreads = true;
			}

			scheduler.NotifyObject(&blockedThreads, !autoReset /*If this is a manually reset event, unblock all the waiting threads.*/);
		}
	}

	if (!schedulerAlreadyLocked) {
		scheduler.lock.Release();
	}

	return unblockedThreads;
}

void KEvent::Reset() {
	if (blockedThreads.firstItem && state) {
		// TODO Is it possible for this to happen?
		KernelLog(LOG_ERROR, "Synchronisation", "reset event with threads blocking", 
				"KEvent::Reset - Attempt to reset a event while threads are blocking on the event\n");
	}

	state = false;
}

bool KEvent::Poll() {
	if (autoReset) {
		return __sync_val_compare_and_swap(&state, true, false);
	} else {
		return state;
	}
}

bool KEvent::Wait(uint64_t timeoutMs) {
	KEvent *events[2];
	events[0] = this;

	if (timeoutMs == (uint64_t) ES_WAIT_NO_TIMEOUT) {
		int index = scheduler.WaitEvents(events, 1);
		return index == 0;
	} else {
		KTimer timer = {};
		timer.Set(timeoutMs, false);
		events[1] = &timer.event;
		int index = scheduler.WaitEvents(events, 2);
		
		if (index == 1) {
			return false;
		} else {
			timer.Remove();
			return true;
		}
	}
}

void KWriterLock::AssertLocked() {
	if (state == 0) {
		KernelPanic("KWriterLock::AssertLocked - Unlocked.\n");
	}
}

void KWriterLock::AssertShared() {
	if (state == 0) {
		KernelPanic("KWriterLock::AssertShared - Unlocked.\n");
	} else if (state < 0) {
		KernelPanic("KWriterLock::AssertShared - In exclusive mode.\n");
	}
}

void KWriterLock::AssertExclusive() {
	if (state == 0) {
		KernelPanic("KWriterLock::AssertExclusive - Unlocked.\n");
	} else if (state > 0) {
		KernelPanic("KWriterLock::AssertExclusive - In shared mode, with %d readers.\n", state);
	}
}

void KWriterLock::Return(bool write) {
	scheduler.lock.Acquire();

	if (state == -1) {
		if (!write) {
			KernelPanic("KWriterLock::Return - Attempting to return shared access to an exclusively owned lock.\n");
		}

		state = 0;
	} else if (state == 0) {
		KernelPanic("KWriterLock::Return - Attempting to return access to an unowned lock.\n");
	} else {
		if (write) {
			KernelPanic("KWriterLock::Return - Attempting to return exclusive access to an shared lock.\n");
		}

		state--;
	}

	if (!state) {
		scheduler.NotifyObject(&blockedThreads, true);
	}

	scheduler.lock.Release();
}

bool KWriterLock::Take(bool write, bool poll) {
	// TODO Preventing exclusive access starvation.
	// TODO Do this without taking the scheduler's lock?

	bool done = false;

	Thread *thread = GetCurrentThread();

	if (thread) {
		thread->blocking.writerLock = this;
		thread->blocking.writerLockType = write;
	}

	while (true) {
		scheduler.lock.Acquire();

		if (write) {
			if (state == 0) {
				state = -1;
				done = true;
			}
		} else {
			if (state >= 0) {
				state++;
				done = true;
			}
		}

		scheduler.lock.Release();

		if (poll || done) {
			break;
		} else {
			if (!thread) {
				KernelPanic("KWriterLock::Take - Scheduler not ready yet.\n");
			}

			thread->state = THREAD_WAITING_WRITER_LOCK;
			ProcessorFakeTimerInterrupt();
			thread->state = THREAD_ACTIVE;
		}
	}

	return done;
}

void KWriterLockTakeMultiple(KWriterLock **locks, size_t lockCount, bool write) {
	uintptr_t i = 0, taken = 0;

	while (taken != lockCount) {
		if (locks[i]->Take(write, taken)) {
			taken++, i++;
			if (i == lockCount) i = 0;
		} else {
			intptr_t j = i - 1;

			while (taken) {
				if (j == -1) j = lockCount - 1;
				locks[j]->Return(write);
				j--, taken--;
			}
		}
	}
}

void KWriterLock::ConvertExclusiveToShared() {
	scheduler.lock.Acquire();
	AssertExclusive();
	state = 1;
	scheduler.NotifyObject(&blockedThreads, true);
	scheduler.lock.Release();
}

#if 0

volatile int testState;
KWriterLock testWriterLock;

void TestWriterLocksThread1(uintptr_t) {
	KEvent wait = {};
	testWriterLock.Take(K_LOCK_SHARED);
	EsPrint("-->1\n");
	testState = 1;
	while (testState != 2);
	wait.Wait(1000);
	EsPrint("-->3\n");
	testWriterLock.Return(K_LOCK_SHARED);
	testState = 3;
	KThreadTerminate();
}

void TestWriterLocksThread2(uintptr_t) {
	while (testState != 1);
	testWriterLock.Take(K_LOCK_SHARED);
	EsPrint("-->2\n");
	testState = 2;
	while (testState != 3);
	testWriterLock.Return(K_LOCK_SHARED);
	KThreadTerminate();
}

void TestWriterLocksThread3(uintptr_t) {
	while (testState < 1);
	testWriterLock.Take(K_LOCK_EXCLUSIVE);
	EsPrint("!!!!!!!!!!!!!!!!!!! %d\n", testState);
	testWriterLock.Return(K_LOCK_EXCLUSIVE);
	testState = 5;
	KThreadTerminate();
}

#define TEST_WRITER_LOCK_THREADS (4)

void TestWriterLocksThread4(uintptr_t) {
	__sync_fetch_and_add(&testState, 1);

	while (testState < 6 + TEST_WRITER_LOCK_THREADS) {
		bool type = EsRandomU8() < 0xC0;
		testWriterLock.Take(type);
		testWriterLock.Return(type);
	}
	
	__sync_fetch_and_add(&testState, 1);
	KThreadTerminate();
}

void TestWriterLocks() {
	testState = 0;
	EsPrint("TestWriterLocks...\n");
	KThreadCreate("Test1", TestWriterLocksThread1);
	KThreadCreate("Test2", TestWriterLocksThread2);
	KThreadCreate("Test3", TestWriterLocksThread3);
	EsPrint("waiting for state 5...\n");
	while (testState != 5);
	while (true) {
		testState = 5;
		for (int i = 0; i < TEST_WRITER_LOCK_THREADS; i++) {
			KThreadCreate("Test", TestWriterLocksThread4, i);
		}
		while (testState != TEST_WRITER_LOCK_THREADS + 5);
		EsPrint("All threads ready.\n");
		KEvent wait = {};
		wait.Wait(10000);
		testState++;
		while (testState != TEST_WRITER_LOCK_THREADS * 2 + 6);
		EsPrint("Test complete!\n");
	}
	KThreadTerminate();
}

#endif

void KTimer::Set(uint64_t triggerInMs, bool autoReset, KAsyncTaskCallback _callback, void *_argument) {
	scheduler.lock.Acquire();
	EsDefer(scheduler.lock.Release());

	if (item.list) {
		// KernelPanic("KTimer::Set - Setting a timer that hasn't been reset.\n");
		scheduler.activeTimers.Remove(&item);
	}

	event.Reset();
	event.autoReset = autoReset;
	triggerTimeMs = triggerInMs + scheduler.timeMs;
	callback = _callback;
	data2 = _argument;
	item.thisItem = this;
	scheduler.activeTimers.InsertStart(&item);
}

void KTimer::Remove() {
	scheduler.lock.Acquire();
	EsDefer(scheduler.lock.Release());

	if (item.list) {
		scheduler.activeTimers.Remove(&item);
	}
}

void Scheduler::WaitMutex(KMutex *mutex) {
	Thread *thread = GetCurrentThread();

	if (thread->state != THREAD_ACTIVE) {
		KernelPanic("Scheduler::WaitMutex - Attempting to wait on a mutex in a non-active thread.\n");
	}

	lock.Acquire();

	thread->state = THREAD_WAITING_MUTEX;
	thread->blocking.mutex = mutex;

	// Is the owner of this mutex executing?
	// If not, there's no point in spinning on it.
	bool spin = mutex && mutex->owner && mutex->owner->executing;

	lock.Release();

	if (!spin && thread->blocking.mutex->owner) {
		ProcessorFakeTimerInterrupt();
	}

	// Early exit if this is a user request to block the thread and the thread is terminating.
	while ((!thread->terminating || thread->terminatableState != THREAD_USER_BLOCK_REQUEST) && mutex->owner) {
		thread->state = THREAD_WAITING_MUTEX;
	}

	thread->state = THREAD_ACTIVE;
}

uintptr_t Scheduler::WaitEvents(KEvent **events, size_t count) {
	if (count > ES_MAX_WAIT_COUNT) {
		KernelPanic("Scheduler::WaitEvents - count (%d) > ES_MAX_WAIT_COUNT (%d)\n", count, ES_MAX_WAIT_COUNT);
	} else if (!count) {
		KernelPanic("Scheduler::WaitEvents - Count is 0.\n");
	} else if (!ProcessorAreInterruptsEnabled()) {
		KernelPanic("Scheduler::WaitEvents - Interrupts disabled.\n");
	}

	Thread *thread = GetCurrentThread();
	thread->blocking.eventCount = count;

	LinkedItem<Thread> blockedItems[count]; // Max size 16 * 32 = 512.
	EsMemoryZero(blockedItems, count * sizeof(LinkedItem<Thread>));
	thread->blockedItems = blockedItems;
	EsDefer(thread->blockedItems = nullptr);

	for (uintptr_t i = 0; i < count; i++) {
		thread->blockedItems[i].thisItem = thread;
		thread->blocking.events[i] = events[i];
	}

	while (!thread->terminating || thread->terminatableState != THREAD_USER_BLOCK_REQUEST) {
		thread->state = THREAD_WAITING_EVENT;

		for (uintptr_t i = 0; i < count; i++) {
			if (events[i]->autoReset) {
				if (events[i]->state) {
					thread->state = THREAD_ACTIVE;

					if (__sync_val_compare_and_swap(&events[i]->state, true, false)) {
						return i;
					}

					thread->state = THREAD_WAITING_EVENT;
				}
			} else {
				if (events[i]->state) {
					thread->state = THREAD_ACTIVE;
					return i;
				}
			}
		}

		ProcessorFakeTimerInterrupt();
	}

	return -1; // Exited from termination.
}

uintptr_t KWaitEvents(KEvent **events, size_t count) {
	return scheduler.WaitEvents(events, count);
}

void Scheduler::UnblockThread(Thread *unblockedThread, Thread *previousMutexOwner) {
	lock.AssertLocked();

	if (unblockedThread->state == THREAD_WAITING_MUTEX) {
		if (unblockedThread->item.list) {
			// If we get here from KMutex::Release -> Scheduler::NotifyObject -> Scheduler::UnblockedThread,
			// the mutex owner has already been cleared to nullptr, so use the previousMutexOwner parameter.
			// But if we get here from Scheduler::TerminateThread, the mutex wasn't released;
			// rather, the waiting thread was unblocked as it is in the WAIT system call, but needs to terminate.

			if (!previousMutexOwner) {
				KMutex *mutex = EsContainerOf(KMutex, blockedThreads, unblockedThread->item.list);

				if (&mutex->blockedThreads != unblockedThread->item.list) {
					KernelPanic("Scheduler::UnblockThread - Unblocked thread %x was not in a mutex blockedThreads list.\n", 
							unblockedThread);
				}

				previousMutexOwner = mutex->owner;
			}

			if (!previousMutexOwner->blockedThreadPriorities[unblockedThread->priority]) {
				KernelPanic("Scheduler::UnblockThread - blockedThreadPriorities was zero (%x/%x).\n", 
						unblockedThread, previousMutexOwner);
			}

			previousMutexOwner->blockedThreadPriorities[unblockedThread->priority]--;
			MaybeUpdateActiveList(previousMutexOwner);

			unblockedThread->item.RemoveFromList();
		}
	} else if (unblockedThread->state == THREAD_WAITING_EVENT) {
		for (uintptr_t i = 0; i < unblockedThread->blocking.eventCount; i++) {
			if (unblockedThread->blockedItems[i].list) {
				unblockedThread->blockedItems[i].RemoveFromList();
			}
		}
	} else if (unblockedThread->state == THREAD_WAITING_WRITER_LOCK) {
		if (unblockedThread->item.list) {
			KWriterLock *lock = EsContainerOf(KWriterLock, blockedThreads, unblockedThread->item.list);

			if (&lock->blockedThreads != unblockedThread->item.list) {
				KernelPanic("Scheduler::UnblockThread - Unblocked thread %x was not in a writer lock blockedThreads list.\n", 
						unblockedThread);
			}

			if ((unblockedThread->blocking.writerLockType == K_LOCK_SHARED && lock->state >= 0)
					|| (unblockedThread->blocking.writerLockType == K_LOCK_EXCLUSIVE && lock->state == 0)) {
				unblockedThread->item.RemoveFromList();
			}
		}
	} else {
		KernelPanic("Scheduler::UnblockedThread - Blocked thread in invalid state %d.\n", 
				unblockedThread->state);
	}

	unblockedThread->state = THREAD_ACTIVE;

	if (!unblockedThread->executing) {
		// Put the unblocked thread at the start of the activeThreads list
		// so that it is immediately executed when the scheduler yields.
		AddActiveThread(unblockedThread, true);
	} 

	// TODO If any processors are idleing, send them a yield IPI.
}

void Scheduler::NotifyObject(LinkedList<Thread> *blockedThreads, bool unblockAll, Thread *previousMutexOwner) {
	lock.AssertLocked();

	LinkedItem<Thread> *unblockedItem = blockedThreads->firstItem;

	if (!unblockedItem) {
		// There weren't any threads blocking on the object.
		return; 
	}

	do {
		LinkedItem<Thread> *nextUnblockedItem = unblockedItem->nextItem;
		Thread *unblockedThread = unblockedItem->thisItem;
		UnblockThread(unblockedThread, previousMutexOwner);
		unblockedItem = nextUnblockedItem;
	} while (unblockAll && unblockedItem);
}

#endif
