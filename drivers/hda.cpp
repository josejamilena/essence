#include <module.h>

// TODO Locking.
// TODO Hot-plug.
// TODO Getting unexpected responses from codec 0 on real hardware?

#define SAMPLE_FORMAT_20 (200)
#define SAMPLE_FORMAT_24 (201)

#define RD_REGISTER_GCAP()        pci->ReadBAR16(0, 0x00)                  // Global capabilities. 
#define RD_REGISTER_VMIN()        pci->ReadBAR8(0, 0x02)                   // Minor version number.
#define RD_REGISTER_VMAJ()        pci->ReadBAR8(0, 0x03)                   // Major version number.
#define RD_REGISTER_GCTL()        pci->ReadBAR32(0, 0x08)                  // Global control.
#define WR_REGISTER_GCTL(x)       pci->WriteBAR32(0, 0x08, x)              
#define RD_REGISTER_STATESTS()    pci->ReadBAR16(0, 0x0E)                  // State change status.
#define RD_REGISTER_INTCTL()      pci->ReadBAR32(0, 0x20)                  // Interrupt control.
#define WR_REGISTER_INTCTL(x)     pci->WriteBAR32(0, 0x20, x)
#define RD_REGISTER_INTSTS()      pci->ReadBAR32(0, 0x24)                  // Interrupt status.
#define WR_REGISTER_CORBLBASE(x)  pci->WriteBAR32(0, 0x40, x)              // CORB base address.
#define WR_REGISTER_CORBUBASE(x)  pci->WriteBAR32(0, 0x44, x)              
#define RD_REGISTER_CORBWP(x)     pci->ReadBAR16(0, 0x48)                  // CORB write pointer.
#define WR_REGISTER_CORBWP(x)     pci->WriteBAR16(0, 0x48, x)              
#define RD_REGISTER_CORBRP(x)     pci->ReadBAR16(0, 0x4A)                  // CORB read pointer.
#define WR_REGISTER_CORBRP(x)     pci->WriteBAR16(0, 0x4A, x)              
#define RD_REGISTER_CORBCTL()     pci->ReadBAR8(0, 0x4C)                   // CORB control.
#define WR_REGISTER_CORBCTL(x)    pci->WriteBAR8(0, 0x4C, x)
#define RD_REGISTER_CORBSIZE()    pci->ReadBAR8(0, 0x4E)                   // CORB size.
#define WR_REGISTER_CORBSIZE(x)   pci->WriteBAR8(0, 0x4E, x)
#define WR_REGISTER_RIRBLBASE(x)  pci->WriteBAR32(0, 0x50, x)              // RIRB base address.
#define WR_REGISTER_RIRBUBASE(x)  pci->WriteBAR32(0, 0x54, x)              
#define RD_REGISTER_RIRBWP(x)     pci->ReadBAR16(0, 0x58)                  // RIRB write pointer.
#define WR_REGISTER_RIRBWP(x)     pci->WriteBAR16(0, 0x58, x)              
#define RD_REGISTER_RINTCNT()     pci->ReadBAR16(0, 0x5A)                  // Response interrupt count.
#define WR_REGISTER_RINTCNT(x)    pci->WriteBAR16(0, 0x5A, x)              
#define RD_REGISTER_RIRBCTL()     pci->ReadBAR8(0, 0x5C)                   // RIRB control.
#define WR_REGISTER_RIRBCTL(x)    pci->WriteBAR8(0, 0x5C, x)
#define RD_REGISTER_RIRBSTS()     pci->ReadBAR8(0, 0x5D)                   // RIRB status.
#define WR_REGISTER_RIRBSTS(x)    pci->WriteBAR8(0, 0x5D, x)
#define RD_REGISTER_RIRBSIZE()    pci->ReadBAR8(0, 0x5E)                   // RIRB size.
#define WR_REGISTER_RIRBSIZE(x)   pci->WriteBAR8(0, 0x5E, x)
#define WR_REGISTER_ImmComOut(x)  pci->WriteBAR32(0, 0x60, x)              // Immediate command output.
#define RD_REGISTER_ImmComIn()    pci->ReadBAR32(0, 0x64)                  // Immediate command input.
#define RD_REGISTER_ImmComStat()  pci->ReadBAR16(0, 0x68)                  // Immediate command status.
#define WR_REGISTER_ImmComStat(x) pci->WriteBAR16(0, 0x68, x)     
#define RD_REGISTER_SDCTL(n)      pci->ReadBAR32(0, 0x80 + 0x20 * (n))     // Stream descriptor control.
#define WR_REGISTER_SDCTL(n, x)   pci->WriteBAR32(0, 0x80 + 0x20 * (n), x) 
#define RD_REGISTER_SDSTS(n)      pci->ReadBAR8(0, 0x83 + 0x20 * (n))      // Stream descriptor status.
#define WR_REGISTER_SDSTS(n, x)   pci->WriteBAR8(0, 0x83 + 0x20 * (n), x)  
#define RD_REGISTER_SDLPIB(n)     pci->ReadBAR32(0, 0x84 + 0x20 * (n))     // Stream descriptor link position in cyclic buffer.
#define WR_REGISTER_SDCBL(n, x)   pci->WriteBAR32(0, 0x88 + 0x20 * (n), x) // Stream descriptor cyclic buffer length.
#define RD_REGISTER_SDLVI(n)      pci->ReadBAR16(0, 0x8C + 0x20 * (n))     // Stream descriptor last valid index.
#define WR_REGISTER_SDLVI(n, x)   pci->WriteBAR16(0, 0x8C + 0x20 * (n), x)
#define RD_REGISTER_SDFMT(n)      pci->ReadBAR16(0, 0x92 + 0x20 * (n))     // Stream descriptor format.
#define WR_REGISTER_SDFMT(n, x)   pci->WriteBAR16(0, 0x92 + 0x20 * (n), x) 
#define WR_REGISTER_SDBDPL(n, x)  pci->WriteBAR32(0, 0x98 + 0x20 * (n), x) // Stream descriptor BDL pointer lower base address.
#define WR_REGISTER_SDBDPU(n, x)  pci->WriteBAR32(0, 0x9C + 0x20 * (n), x) // Stream Bdescriptor DL pointer upper base address.

#define READ_PARAMETER_VENDOR_ID                   (0xF0000)
#define READ_PARAMETER_REVISION_ID                 (0xF0002)
#define READ_PARAMETER_CHILD_NODES                 (0xF0004)
#define READ_PARAMETER_FUNCTION_GROUP_TYPE         (0xF0005)
#define READ_PARAMETER_AUDIO_FUNCTION_CAPABILITIES (0xF0008)
#define READ_PARAMETER_AUDIO_WIDGET_CAPABILITIES   (0xF0009)
#define READ_PARAMETER_FORMAT_CAPABILITIES         (0xF000A)
#define READ_PARAMETER_STREAM_FORMATS              (0xF000B)
#define READ_PARAMETER_PIN_CAPABILITIES            (0xF000C)
#define READ_PARAMETER_INPUT_AMP_CAPABILITIES      (0xF000D)
#define READ_PARAMETER_CONNECTION_LIST_LENGTH      (0xF000E)
#define READ_PARAMETER_OUTPUT_AMP_CAPABILITIES     (0xF0012)

#define COMMAND_GET_CONNECTION_LIST_ENTRY(offset)                     (0xF0200 | (offset))
#define COMMAND_SET_CONNECTION_SELECT(index)                          (0x70100 | (index))
#define COMMAND_GET_AMPLIFIER_GAIN_MUTE(out, left, index)             (0xB0000 | ((out) ? (1 << 15) : 0) | ((left) ? (1 << 13) : 0) | (index))
#define COMMAND_SET_AMPLIFIER_GAIN_MUTE(out, left, index, mute, gain) (0x30000 | ((out) ? (1 << 15) : (1 << 14)) | ((left) ? (1 << 13) : (1 << 12)) \
		                                                               | ((index) << 8) | ((mute) ? (1 << 7) : 0) | (gain))
#define COMMAND_SET_CONVERTER_FORMAT(format)                          (0x20000 | (format))
#define COMMAND_SET_STREAM_NUMBER(stream, channel)                    (0x70600 | ((stream) << 4) | ((channel) << 0))
#define COMMAND_GET_PIN_WIDGET_CONTROL()                              (0xF0700)
#define COMMAND_SET_PIN_WIDGET_CONTROL(control)                       (0x70700 | (control))
#define COMMAND_PIN_SENSE()                                           (0xF0900)
#define COMMAND_GET_PIN_CONFIGURATION()                               (0xF1C00)
#define COMMAND_RESET()	                                              (0x7FF00)

struct HDAStreamConfiguration {
	uint8_t direction;
	EsAudioFormat format;
	size_t totalBufferSampleBlocks, bufferCount;
};

struct HDAStream {
	uintptr_t index;
	uint8_t number;
	uint32_t format;
	uintptr_t bufferPhysical;
	uint8_t *bufferVirtual;
	uint8_t *firstBuffer;
	size_t bufferStride;
	HDAStream *nextStream;
	uint32_t currentBuffer; 
	HDAStreamConfiguration configuration;
	KAudioMixer *mixer;
	size_t bytesPerSampleBlock;
	size_t sampleBlocksPerBuffer;
	KEvent update, terminated;
	volatile bool terminateUpdateThread;
	struct HDAController *controller;
};

struct HDANode {
	uint16_t *inputConnections; // Zero-terminated.

	uint16_t codec, id, function;

#define NODE_ROOT  (1)
#define NODE_AUDIO (2)
#define NODE_DAC   (3)
#define NODE_PIN   (4)
#define NODE_MIXER (5)
	uint8_t type;

	// Node specific information:

	union {
		struct {
			uint32_t vendorID;
		} root;

		struct {
			uint32_t pinCapabilities, pinConfiguration;
			bool input, output;

#define PIN_MAYBE_CONNECTED (0)
#define PIN_UNCONNECTED     (1)
#define PIN_CONNECTED       (2)
			uint8_t isConnected;
		} pin;
	};

	// Function specific information:

	union {
		struct {
			uint32_t streamFormats, 
				 formatCapabilities, 
				 outputAmpCapabilities,
				 inputAmpCapabilities;
			bool hasOutputAmp, hasInputAmp;
		} audio;
	};
};

struct HDAController : KAudioController {
	KPCIDevice *pci;
	KMutex mutex;
	KSpinlock spinlock; // Used to update stream linked list. Needs to be a spinlock for the IRQ.

	HDANode *nodes;
	HDAStream *firstStream;

	uint8_t supportedOutputStreams;
	uint8_t supportedInputStreams;
	uint8_t supportedBidiStreams;

	uint32_t usedOutputStreams;
	uint32_t usedInputStreams;
	uint32_t usedBidiStreams;

	uint32_t usedStreamNumbers;

	uint32_t corbSize, rirbSize;
	uint32_t corbWritePointer;
	uint32_t *corbVirtual;
	uint64_t *rirbVirtual;

	bool dma64Supported;

	void Initialise();
	void InitialiseAudioFunction(HDANode *node);
	uint32_t SendCommand(uint32_t codec, uint32_t node, uint32_t data);
	inline uint32_t SendCommand(HDANode *node, uint32_t data) { return SendCommand(node->codec, node->id, data); }
	bool AllocateStream(HDAStreamConfiguration *configuration, HDAStream *stream);
	HDANode *FindNode(uint16_t codec, uint16_t id, uint16_t function);
	bool OpenOutputStream(KAudioMixer *stream);
	void SetPaused(HDAStream *stream, bool paused);
	bool HandleIRQ();
	HDANode *FindRouteToDAC(HDANode *node, bool setup, uintptr_t depth = 2);
};

HDANode *HDAController::FindNode(uint16_t codec, uint16_t id, uint16_t function) {
	for (uintptr_t i = 0; i < ArrayLength(nodes); i++) {
		if (nodes[i].id == id && nodes[i].codec == codec && nodes[i].function == function) {
			return nodes + i;
		}
	}

	return nullptr;
}

bool HDAController::AllocateStream(HDAStreamConfiguration *configuration, HDAStream *stream) {
	mutex.AssertLocked();

	bool output = configuration->direction == K_AUDIO_DIRECTION_OUTPUT;

	// Have we used all the available stream numbers?
	
	if (usedStreamNumbers == 0xFFFE) {
		return false;
	}

	// Allocate the needed physical memory.

	size_t bytesPerSampleBlock = configuration->format.channels;

	if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
		bytesPerSampleBlock *= 2;
	} else if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		bytesPerSampleBlock *= 1;
	} else if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_S32LE 
			|| configuration->format.sampleFormat == SAMPLE_FORMAT_24 
			|| configuration->format.sampleFormat == SAMPLE_FORMAT_20) {
		bytesPerSampleBlock *= 4;
	} else {
		KernelPanic("HDAController::AllocateStream - Unrecognised sample format.\n");
	}

	stream->bytesPerSampleBlock = bytesPerSampleBlock;

	size_t sampleBlocksPerBuffer = configuration->totalBufferSampleBlocks / configuration->bufferCount;
	stream->sampleBlocksPerBuffer = sampleBlocksPerBuffer;

	size_t bdlSize = (configuration->bufferCount * 0x10 + 0x7F) & ~0x7F;
	size_t perBufferSize = (sampleBlocksPerBuffer * bytesPerSampleBlock + 0x7F) & ~0x7F;
	size_t totalPhysicalMemoryNeeded = bdlSize + perBufferSize * configuration->bufferCount;
	size_t totalPhysicalPagesNeeded = (totalPhysicalMemoryNeeded + K_PAGE_SIZE - 1) >> K_PAGE_BITS;

	uint8_t *bufferVirtual;
	uintptr_t bufferPhysical;

	if (!MMPhysicalAllocateAndMap(totalPhysicalMemoryNeeded, 0, dma64Supported ? 64 : 32, true, ES_FLAGS_DEFAULT, &bufferVirtual, &bufferPhysical)) {
		KernelLog(LOG_ERROR, "HDA", "allocation failure", "AllocateStream - Could not allocate memory for the stream buffer.\n");
		return false;
	}

	KernelLog(LOG_INFO, "HDA", "BDL address", "AllocateStream - Physical address of the allocated BDL is %x, virtual address is %x.\n", bufferPhysical, bufferVirtual);

	// Setup the BDL.

	for (uintptr_t i = 0; i < configuration->bufferCount; i++) {
		*((uint64_t *) (bufferVirtual + i * 0x10 + 0x00)) = bufferPhysical + i * perBufferSize + bdlSize;
		*((uint32_t *) (bufferVirtual + i * 0x10 + 0x08)) = sampleBlocksPerBuffer * bytesPerSampleBlock;
		*((uint32_t *) (bufferVirtual + i * 0x10 + 0x0C)) = 1 << 0; // Interrupt on completion.
	}

	// Is there a non-bidi stream of this type available?

	intptr_t index = -1;

	if (output) {
		for (uintptr_t i = 0; i < supportedOutputStreams; i++) {
			if (~usedOutputStreams & (1 << i)) {
				index = i + supportedInputStreams;
				usedOutputStreams |= (1 << i);
				break;
			}
		}
	} else {
		for (uintptr_t i = 0; i < supportedInputStreams; i++) {
			if (~usedInputStreams & (1 << i)) {
				index = i;
				usedInputStreams |= (1 << i);
				break;
			}
		}
	}

	// Is there a bidi stream of this type available?

	bool isBidi = false;

	if (index == -1) {
		for (uintptr_t i = 0; i < supportedBidiStreams; i++) {
			if (~usedBidiStreams & (1 << i)) {
				index = i + supportedInputStreams + supportedOutputStreams;
				usedBidiStreams |= (1 << i);
				isBidi = true;
				break;
			}
		}
	}

	// If we couldn't find a stream, return.

	if (index == -1) {
		MMPhysicalFree(bufferPhysical, false, totalPhysicalPagesNeeded);
		MMFree(MMGetKernelSpace(), bufferVirtual);
		return false;
	}

	// Set the stream number and direction.

	uint8_t streamNumber = 0;

	if (index != -1) {
		for (uintptr_t i = 1; i < 16; i++) {
			if (~usedStreamNumbers & (1 << i)) {
				streamNumber = i;
				usedStreamNumbers |= (1 << i);
				break;
			}
		}

		uint32_t control = RD_REGISTER_SDCTL(index) & 0xFFFFFF;

		control = (control & 0x0FFFFF) | (streamNumber << 20);

		if (isBidi) {
			control = (control & ~(1 << 19)) | (output ? (1 << 19) : (0 << 19));
		}

		WR_REGISTER_SDCTL(index, control);
	}

	// Enable stream interrupts.

	WR_REGISTER_INTCTL((1 << index) | RD_REGISTER_INTCTL());

	// Setup the stream registers.
	// 	- Format.
	// 	- BDL address.
	// 	- Buffer size and LVI.
	// 	- Interrupt on buffer completion.
	// 	- Start DMA.

	uint32_t format = configuration->format.channels - 1;

	if (configuration->format.sampleRate == 48000) {
	} else if (configuration->format.sampleRate == 44100) {
		format |= (1 << 14);
	} else {
		KernelPanic("HDAController::AllocateStream - Unrecognised sample rate.\n");
	}

	if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_S32LE) {
		format |= (4 << 4);
	} else if (configuration->format.sampleFormat == SAMPLE_FORMAT_24) {
		format |= (3 << 4);
	} else if (configuration->format.sampleFormat == SAMPLE_FORMAT_20) {
		format |= (2 << 4);
	} else if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_S16LE) {
		format |= (1 << 4);
	} else if (configuration->format.sampleFormat == ES_SAMPLE_FORMAT_U8) {
		format |= (0 << 4);
	} else {
		KernelPanic("HDAController::AllocateStream - Unrecognised sample format.\n");
	}

	KernelLog(LOG_INFO, "HDA", "stream format", "AllocateStream - Configuring stream to use format: %dHz, %d channels, sample format %d; with %d divisions.\n",
			configuration->format.sampleRate, configuration->format.channels, configuration->format.sampleFormat, configuration->bufferCount);
	
	WR_REGISTER_SDFMT(index, (RD_REGISTER_SDFMT(index) & 0x0080) | format);
	WR_REGISTER_SDCBL(index, bytesPerSampleBlock * configuration->totalBufferSampleBlocks);
	WR_REGISTER_SDLVI(index, (RD_REGISTER_SDLVI(index) & 0xFF00) | (configuration->bufferCount - 1));
	WR_REGISTER_SDBDPL(index, bufferPhysical);
	if (dma64Supported) WR_REGISTER_SDBDPU(index, bufferPhysical >> 32);
	WR_REGISTER_SDCTL(index, (RD_REGISTER_SDCTL(index) & 0xFFFFFF) | (1 << 1) | (1 << 2));

	// Return the stream information.

	stream->index = index;
	stream->number = streamNumber;
	stream->bufferPhysical = bufferPhysical;
	stream->bufferVirtual = bufferVirtual;
	stream->firstBuffer = bufferVirtual + bdlSize;
	stream->bufferStride = perBufferSize;
	stream->format = format;

	return true;
}

void HDAController::SetPaused(HDAStream *stream, bool paused) {
	mutex.Acquire();
	EsDefer(mutex.Release());

	uint32_t control = RD_REGISTER_SDCTL(stream->index) & 0xFFFFFF;
	if (paused) control &= ~(1 << 1);
	else control |= (1 << 1);
	WR_REGISTER_SDCTL(stream->index, control);
}

uint32_t HDAController::SendCommand(uint32_t codec, uint32_t node, uint32_t data) {
	mutex.AssertLocked();

	uint32_t command = (codec << 28) | (node << 20) | data;

	// Go to the next position in the CORB.

	corbWritePointer++;
	if (corbWritePointer == corbSize) corbWritePointer = 0;

	// KernelLog(LOG_INFO, "HDA", "send command", "SendCommand - codec %d, node %d, data %x, CWP %d.\n", codec, node, data, corbWritePointer);

	// Write the command in the CORB.

	corbVirtual[corbWritePointer] = command;

	// Note the current position of the RIRB write pointer.

	uint32_t rirbLast = RD_REGISTER_RIRBWP() & 0xFF;

	// Update the CORB write pointer register.

	WR_REGISTER_CORBWP((RD_REGISTER_CORBWP() & 0xFF00) | corbWritePointer);

	// Read responses until we find the first solicited response.

	KTimeout timeout(50);

	while (true) {
		if (timeout.Hit()) {
			KernelLog(LOG_ERROR, "HDA", "RIRB timeout", "SendCommand - Timeout occurred while waiting for response in RIRB.\n");
			// TODO Do we need to reset something?
			return 0;
		}

		uint32_t rirbPosition = RD_REGISTER_RIRBWP() & 0xFF;

		if (RD_REGISTER_RIRBSTS() & 1) {
			WR_REGISTER_RIRBSTS(1); // Clear IRQ status bit.
		}

		if (rirbPosition == rirbLast) {
			continue;
		}

		uint64_t response = rirbVirtual[rirbPosition];
		rirbLast = rirbPosition;

		if (response & (1ul << (32 + 4))) {
			// Unsolicited response.
			KernelLog(LOG_INFO, "HDA", "unsolicited response", "SendCommand - Received unsolicited response in the RIRB, ignoring.\n");
			continue;
		}

		if ((response & (15ul << 32)) != codec) {
			KernelLog(LOG_ERROR, "HDA", "RIRB codec mismatch", "SendCommand - Unexpected response from codec %d.\n", response & (15ul << 32));
		}

		return response & 0xFFFFFFFF;
	}
}

void HDAController::InitialiseAudioFunction(HDANode *function) {
	mutex.AssertLocked();

	// Reset the function and all its widgets.

	SendCommand(function, COMMAND_RESET());

	// Get the number of widgets and their IDs.

	uint32_t childNodes = SendCommand(function, READ_PARAMETER_CHILD_NODES);
	uint8_t widgetCount = (childNodes >> 0) & 0xFF;
	uint8_t widgetNode = (childNodes >> 16) & 0xFF;

	// Get the supported formats and amplifier capabilities.

	function->audio.formatCapabilities = SendCommand(function, READ_PARAMETER_FORMAT_CAPABILITIES);
	function->audio.streamFormats = SendCommand(function, READ_PARAMETER_STREAM_FORMATS);
	function->audio.outputAmpCapabilities = SendCommand(function, READ_PARAMETER_OUTPUT_AMP_CAPABILITIES);
	function->audio.inputAmpCapabilities = SendCommand(function, READ_PARAMETER_INPUT_AMP_CAPABILITIES);

	for (uintptr_t i = widgetNode; i < widgetNode + widgetCount; i++) {
		// Get the type of the of the widget.

		uint32_t capabilities = SendCommand(function->codec, i, READ_PARAMETER_AUDIO_WIDGET_CAPABILITIES);
		uint8_t nodeType = (capabilities >> 20) & 0xF;

		HDANode *node = (HDANode *) ArrayAddPointer(nodes, nullptr);
		if (!node) break;
		node->codec = function->codec, node->id = i, node->function = function->id;

		const char *typeNames[] = {
			"DAC", "ADC", "mixer", "selector", "pin", "power", "volume knob", "beep generator", 
			"??", "??", "??", "??", "??", "??", "??", "vendor specific",
		};

		KernelLog(LOG_INFO, "HDA", "found audio widget", "InitialiseAudioFunction - Found widget in codec %d with node ID %d of type %d (%z) and capabilities %x.\n", 
				node->codec, node->id, nodeType, typeNames[nodeType], capabilities);

		if (capabilities & (1 << 8)) {
			uint32_t connectionListLength = SendCommand(node, READ_PARAMETER_CONNECTION_LIST_LENGTH);
			bool longForm = connectionListLength & 0x80;
			connectionListLength &= 0x7F;

			if (connectionListLength) {
				node->inputConnections = (uint16_t *) EsHeapAllocate((connectionListLength + 1) * sizeof(uint16_t), true, K_FIXED);

				KernelLog(LOG_INFO, "HDA", "connection list length", "Node %d/%d has a connection list length of %d%z.\n",
						node->codec, node->id, connectionListLength, longForm ? " (long form)" : "");

				for (uintptr_t j = 0; j < connectionListLength;) {
					uint32_t connectionList = SendCommand(node, COMMAND_GET_CONNECTION_LIST_ENTRY(j));

					for (uintptr_t k = 0; k < 4 && j < connectionListLength; k += (longForm ? 2 : 1), j++) {
						uint32_t entry = (connectionList >> (k * 8)) & (longForm ? (0xFFFF) : (0xFF));
						bool isRange = longForm ? (entry & 0x8000) : (entry & 0x80);
						EsPrint("%d%z; ", entry, isRange ? " (range)" : "");
						// KernelLog(LOG_INFO, "HDA", "connection list entry", "Connection list entry %d: %d%z.\n", 
						// 		j, entry, isRange ? " (range)" : "");
						node->inputConnections[j] = entry | (isRange ? 0x8000 : 0);
					}
				}

				EsPrint("\n");
			} 
		}

		// Get information about the output amp.

		if (capabilities & (1 << 2)) {
			node->audio.hasOutputAmp = true;

			if (capabilities & (1 << 3)) {
				node->audio.outputAmpCapabilities = SendCommand(node, READ_PARAMETER_OUTPUT_AMP_CAPABILITIES)
					?: function->audio.outputAmpCapabilities;
			} else {
				node->audio.outputAmpCapabilities = function->audio.outputAmpCapabilities;
			}
		}

		// Get information about the input amp.

		if (capabilities & (1 << 1)) {
			node->audio.hasInputAmp = true;

			if (capabilities & (1 << 3)) {
				node->audio.inputAmpCapabilities = SendCommand(node, READ_PARAMETER_INPUT_AMP_CAPABILITIES)
					?: function->audio.inputAmpCapabilities;
			} else {
				node->audio.inputAmpCapabilities = function->audio.inputAmpCapabilities;
			}
		}

		if (nodeType == 0) {
			node->type = NODE_DAC;

			// Get the supported formats, falling back to the capabilities of the function group if necessary.

			node->audio.formatCapabilities = SendCommand(node, READ_PARAMETER_FORMAT_CAPABILITIES)
				?: function->audio.formatCapabilities;
			node->audio.streamFormats = SendCommand(node, READ_PARAMETER_STREAM_FORMATS)
				?: function->audio.streamFormats;

			KernelLog(LOG_INFO, "HDA", "DAC sample formats", "DAC supports the formats %x/%x.\n", 
					node->audio.formatCapabilities, node->audio.streamFormats);
		} else if (nodeType == 2) {
			node->type = NODE_MIXER;
		} else if (nodeType == 4) {
			node->type = NODE_PIN;

			// Get the pin capabilities.

			node->pin.pinCapabilities = SendCommand(node, READ_PARAMETER_PIN_CAPABILITIES);
			node->pin.output = node->pin.pinCapabilities & (1 << 4);
			node->pin.input = node->pin.pinCapabilities & (1 << 5);

			// Get the pin configuration information.

			node->pin.pinConfiguration = SendCommand(node, COMMAND_GET_PIN_CONFIGURATION());

			const char *connectivityStrings[] = { "jack", "none", "integrated", "Jack and integrated" };
			const char *primaryLocationStrings[] = { "external", "internal", "separate", "other" };
			const char *secondaryLocationStrings[] = { "n/a", "rear", "front", "left", "right", "top", "bottom", "7", "8", "9", "10", "11", "12", "13", "14", "15"  };
			const char *defaultDeviceStrings[] = { "line out", "speaker", "headphones out", "CD", "SPDIF out", "digital other out", "modem line side", 
				"modem handset side", "line in", "aux", "microphone in", "telephony", "SPDIF in", "digital other in", "reserved", "other" };
			const char *connectionTypeStrings[] = { "??", "1/8\"", "1/4\"", "ATAPI", "RCA", "optical", "other digital", "other analog", "multichannel analog",
		       		"XLR", "RJ-11 (modem)", "combination", "??", "??", "??", "other" };
			const char *colorStrings[] = { "??", "black", "gray", "blue", "green", "red", "orange", "yellow", "purple", "pink", 
		       		"reserved", "reserved", "reserved", "reserved", "white", "other" };

			KernelLog(LOG_INFO, "HDA", "pin configuration", "Pin has configuration: %x. "
					"Connectivity: %z. Location: %z - %z. Default device: %z. Connection type: %z. Color: %z. "
					"Presence detect capability: %z. Group and index: %d, %d.\n",
					node->pin.pinConfiguration,
					connectivityStrings[(node->pin.pinConfiguration >> 30) & 3],
					primaryLocationStrings[(node->pin.pinConfiguration >> 28) & 3],
					secondaryLocationStrings[(node->pin.pinConfiguration >> 24) & 15],
					defaultDeviceStrings[(node->pin.pinConfiguration >> 20) & 15],
					connectionTypeStrings[(node->pin.pinConfiguration >> 16) & 15],
					colorStrings[(node->pin.pinConfiguration >> 12) & 15],
					(node->pin.pinConfiguration & (1 << 8)) ? "no" : "yes",
					(node->pin.pinConfiguration >> 4) & 15, (node->pin.pinConfiguration >> 0) & 15);

			// Is the pin connected?

			if (node->pin.pinCapabilities & (1 << 2)) {
				node->pin.isConnected = (SendCommand(node, COMMAND_PIN_SENSE()) & (1 << 31))
					? PIN_CONNECTED : PIN_UNCONNECTED;

				KernelLog(LOG_INFO, "HDA", "pin sense", "Pin sense: %z.\n", node->pin.isConnected == PIN_CONNECTED ? "connected" : "unconnected");
			}
		} 
	}
}

HDANode *HDAController::FindRouteToDAC(HDANode *node, bool setup, uintptr_t depth) {
	mutex.AssertLocked();

	// EsPrint("FindRouteToDAC: node id %d, setup %d, depth %d\n", node->id, setup, depth);

	if (!depth) return nullptr;
	if (!node->inputConnections) return nullptr;
	if (!node->inputConnections[0]) return nullptr;

	ptrdiff_t connection = -1;
	bool single = !node->inputConnections[1];
	HDANode *dac = nullptr;

	// Look for a DAC in the node's inputs.

	for (uintptr_t index = 0; node->inputConnections[index]; index++) {
		uint16_t id = node->inputConnections[index];
		if (id & 0x8000) continue; // Ignore ranges.
		dac = FindNode(node->codec, id, node->function);
		if (!dac || dac->type != NODE_DAC) continue;
		// EsPrint("- index %d is DAC (id %d)\n", index, dac->id);
		connection = index;
		goto foundConnection;
	}

	// Look for a mixer in the node's inputs.

	for (uintptr_t index = 0; node->inputConnections[index]; index++) {
		uint16_t id = node->inputConnections[index];
		if (id & 0x8000) continue; // Ignore ranges.
		HDANode *mixer = FindNode(node->codec, id, node->function);
		if (!mixer || mixer->type != NODE_MIXER) continue;
		// EsPrint("- index %d is mixer\n", index);
		dac = FindRouteToDAC(mixer, setup, depth - 1);
		if (!dac) continue;
		connection = index;
		goto foundConnection;
	}

	foundConnection:;

	if (connection != -1 && setup) {
		EsPrint("SETUP to %d: node id %d, setup %d, depth %d\n", connection, node->id, setup, depth);

		// Set the connection.

		if (!single && node->type == NODE_PIN) {
			// EsPrint("- select index %d\n", connection);
			SendCommand(node, COMMAND_SET_CONNECTION_SELECT(connection)); 
		}

		// Unmute the amplifier, and set the gain to 0db.

		if (node->audio.hasOutputAmp) {
			// EsPrint("- unmuting output amp\n");
			uint8_t gain = node->audio.outputAmpCapabilities & 0x7F;
			SendCommand(node, COMMAND_SET_AMPLIFIER_GAIN_MUTE(true, false, 0, false, gain));
			SendCommand(node, COMMAND_SET_AMPLIFIER_GAIN_MUTE(true, true,  0, false, gain));
		}

		// If this is a mixer, unmute the target input, and mute the others.

		if (node->type == NODE_MIXER && node->audio.hasInputAmp) {
			// EsPrint("- setting mixer input amps\n");

			for (ptrdiff_t index = 0; node->inputConnections[index]; index++) {
				uint16_t id = node->inputConnections[index];
				if (id & 0x8000) continue; // Ignore ranges.
				// EsPrint("-- input amp index %d, id %d is %z\n", index, id, index == connection ? "UNMUTE" : "mute");
				uint8_t gain = index == connection ? (node->audio.inputAmpCapabilities & 0x7F) : 0;
				SendCommand(node, COMMAND_SET_AMPLIFIER_GAIN_MUTE(false, false, index, index != connection /* mute */, gain));
				SendCommand(node, COMMAND_SET_AMPLIFIER_GAIN_MUTE(false, true,  index, index != connection /* mute */, gain));
			}
		}
	}

	return connection != -1 ? dac : nullptr;
}

bool HDAController::OpenOutputStream(KAudioMixer *mixer) {
	mutex.Acquire();
	EsDefer(mutex.Release());

	// Select a pin to send output to.
	// We want an output pin that is directly connected to a DAC, and is connected to something.

	HDANode *firstMaybeConnectedOutputPin = nullptr, *firstConnectedOutputPin = nullptr;

	for (uintptr_t i = 0; i < ArrayLength(nodes); i++) {
		HDANode *node = nodes + i;

		// Looking for an output pin.
		if (node->type != NODE_PIN) continue;
		if (!node->pin.output) continue; 

		uint32_t connectivity = (node->pin.pinConfiguration >> 30) & 3;
		uint32_t defaultDevice = (node->pin.pinConfiguration >> 20) & 15;
		uint32_t indexInGroup = node->pin.pinConfiguration & 15;

		KernelLog(LOG_INFO, "HDA", "maybe open pin", "Output pin %d/%d has connectivity %d, device %d, index in group %d.\n", 
				node->codec, node->id, connectivity, defaultDevice, indexInGroup);

		if (indexInGroup != 0) continue; // First two channels of group.
		if (connectivity == 1) continue; // Jack or integrated.
		if (defaultDevice != 0 && defaultDevice != 1) continue; // Speaker or line out.
		if (!FindRouteToDAC(node, false)) continue; // Connected to a DAC.

		KernelLog(LOG_INFO, "HDA", "suitable output pin", "OpenOutputStream - Found suitable output pin in codec %d with ID %d.\n", 
				node->codec, node->id);

		if (!firstMaybeConnectedOutputPin) firstMaybeConnectedOutputPin = node;
		if (!firstConnectedOutputPin && node->pin.isConnected == PIN_CONNECTED) firstConnectedOutputPin = node;
	}

	HDANode *outputPin = firstConnectedOutputPin ?: firstMaybeConnectedOutputPin;

	if (!outputPin) {
		KernelLog(LOG_ERROR, "HDA", "no output pin", "OpenOutputStream - Could not find a suitable output pin.\n");
		return false;
	}

	HDANode *outputDAC = FindRouteToDAC(outputPin, true /* setup route */);

	if (!outputDAC) {
		KernelLog(LOG_ERROR, "HDA", "no DAC input", "OpenOutputStream - Connection list changed, removing the pin's DAC input.\n");
		return false;
	}

	KernelLog(LOG_INFO, "HDA", "using output pin", "OpenOutputStream - Using output pin %d/%d with DAC %d/%d.\n", 
			outputPin->codec, outputPin->id, outputDAC->codec, outputDAC->id);

	// Select the format to use.
	// TODO Allow the audio engine to pick the format.
	// TODO Checking that the format does not exceed the maximum output payload.

	EsAudioFormat format = {};

	if (~outputDAC->audio.streamFormats & (1 << 0)) {
		KernelLog(LOG_ERROR, "HDA", "no supported sample formats", "OpenOutputStream - DAC does not supported PCM.\n");
		return false;
	}

	if (outputDAC->audio.formatCapabilities & (1 << 20)) {
		format.sampleFormat = ES_SAMPLE_FORMAT_S32LE;
	} else if (outputDAC->audio.formatCapabilities & (1 << 19)) {
		format.sampleFormat = SAMPLE_FORMAT_24;
	} else if (outputDAC->audio.formatCapabilities & (1 << 18)) {
		format.sampleFormat = SAMPLE_FORMAT_20;
	} else if (outputDAC->audio.formatCapabilities & (1 << 17)) {
		format.sampleFormat = ES_SAMPLE_FORMAT_S16LE;
	} else if (outputDAC->audio.formatCapabilities & (1 << 16)) {
		format.sampleFormat = ES_SAMPLE_FORMAT_U8;
	} else {
		KernelLog(LOG_ERROR, "HDA", "no supported sample formats", "OpenOutputStream - No supported sample formats.\n");
		return false;
	}

	// TODO Support more sample rates.

	if (outputDAC->audio.formatCapabilities & (1 << 6)) {
		format.sampleRate = 48000;
	} else if (outputDAC->audio.formatCapabilities & (1 << 5)) {
		format.sampleRate = 44100;
	} else {
		KernelLog(LOG_ERROR, "HDA", "no supported sample rates", "OpenOutputStream - No supported sample rates.\n");
		return false;
	}

	format.channels = 2;

	// Create the update thread.

	HDAStream *stream = (HDAStream *) EsHeapAllocate(sizeof(HDAStream), true, K_FIXED);
	stream->update.autoReset = true;
	stream->controller = this;

	bool success = KThreadCreate("HDAudioUpdate", [] (uintptr_t argument) {
		HDAStream *stream = (HDAStream *) argument;

		while (true) {
			stream->update.Wait();
			underrun:;

			if (stream->terminateUpdateThread) {
				stream->terminated.Set();
				KThreadTerminate();
			}

			uint8_t *buffer = stream->firstBuffer + stream->currentBuffer * stream->bufferStride;

			KernelLog(LOG_VERBOSE, "HDA", "fill buffer start", "Fill buffer %d divisions starting at %d.\n", 
					stream->currentBuffer);

			stream->currentBuffer++;
			if (stream->currentBuffer == stream->configuration.bufferCount) stream->currentBuffer = 0;

			if (stream->configuration.direction == K_AUDIO_DIRECTION_OUTPUT) {
				bool success = KAudioFillBuffersFromMixer(&buffer, 1, stream->mixer, 
						stream->bytesPerSampleBlock * stream->sampleBlocksPerBuffer, 
						&stream->update);

				if (!success) {
					KernelLog(LOG_ERROR, "HDA", "underrun", "Audio buffer fill underrun.\n");
					goto underrun;
				}

				KernelLog(LOG_VERBOSE, "HDA", "fill buffer complete", "Fill buffer complete.\n");
			} else {
				KernelPanic("HDAController::UpdateStreams - Input streams are unimplemented.\n");
			}
		}
	}, (uintptr_t) stream);

	if (!success) {
		KernelLog(LOG_ERROR, "HDA", "update thread creation failure", "OpenOutputStream - Could not create the update thread.\n");
		return false;
	}

	// Allocate a stream to send data to the pin.

	uint64_t sampleBlocksPerBuffer = (uint64_t) K_AUDIO_HARDWARE_BUFFER_US * 2 * format.sampleRate / 1000000;

	HDAStreamConfiguration configuration = { 
		.direction = K_AUDIO_DIRECTION_OUTPUT, 
		.format = format, 
		.totalBufferSampleBlocks = ((sampleBlocksPerBuffer / 2) & ~127) * 2,
		.bufferCount = 2, 
	};

	success = AllocateStream(&configuration, stream);

	if (!success) {
		stream->terminateUpdateThread = true;
		stream->update.Set(false, true);
		stream->terminated.Wait();
		EsHeapFree(stream, sizeof(HDAStream), K_FIXED);

		KernelLog(LOG_ERROR, "HDA", "allocation failure", "OpenOutputStream - Could not allocate a stream.\n");
		return false;
	}

	// Set the DAC format and the stream number.

	SendCommand(outputDAC, COMMAND_SET_CONVERTER_FORMAT(stream->format));
	SendCommand(outputDAC, COMMAND_SET_STREAM_NUMBER(stream->number, 0)); 

	// Unmute the DAC and set the gain to 0dB.

	if (outputDAC->audio.hasOutputAmp) {
		uint8_t gain = outputDAC->audio.outputAmpCapabilities & 0x7F;
		SendCommand(outputDAC, COMMAND_SET_AMPLIFIER_GAIN_MUTE(true, false, 0, false, gain));
		SendCommand(outputDAC, COMMAND_SET_AMPLIFIER_GAIN_MUTE(true, true,  0, false, gain));
	}

	// Enable pin output.

	uint32_t control = SendCommand(outputPin, COMMAND_GET_PIN_WIDGET_CONTROL());
	SendCommand(outputPin, COMMAND_SET_PIN_WIDGET_CONTROL(1 << 6 | control));

	// Store information about the stream.

	if (format.sampleFormat == SAMPLE_FORMAT_20 || format.sampleFormat == SAMPLE_FORMAT_24) {
		format.sampleFormat = ES_SAMPLE_FORMAT_S32LE;
	}

	mixer->data = stream;
	mixer->format = format;

	// Register the stream.

	stream->configuration = configuration;
	stream->mixer = mixer;

	spinlock.Acquire();
	stream->nextStream = firstStream;
	firstStream = stream;
	spinlock.Release();

	return true;
}

bool HDAController::HandleIRQ() {
	uint32_t status = RD_REGISTER_INTSTS();

	if (!status) {
		return false;
	}

	spinlock.Acquire();

	HDAStream *stream = firstStream;

	while (stream) {
		if (status & (1 << stream->index)) {
			uint32_t streamStatus = RD_REGISTER_SDSTS(stream->index);

			if (streamStatus & (1 << 2)) {
				stream->update.Set(false, true);

				static uint64_t previousTime = 0;
				uint64_t currentTime = KGetTimeInMs();
				KernelLog(LOG_VERBOSE, "HDA", "request update", "Requesting update after %dms.\n", currentTime - previousTime);
				previousTime = currentTime;
			}
		}

		stream = stream->nextStream;
	}

	for (uintptr_t i = 0; i < 30; i++) {
		if (~status & (1 << i)) continue;
		WR_REGISTER_SDSTS(i, (1 << 2) | (1 << 3) | (1 << 4));
	}

	spinlock.Release();

	return true;
}

void HDAController::Initialise() {
	KernelLog(LOG_INFO, "HDA", "detected controller", "Initialise - HD Audio controller detected.\n");

	mutex.Acquire();
	EsDefer(mutex.Release());

	ArrayInitialise(nodes, K_FIXED);

	openOutput = [] (KAudioController *controller, KAudioMixer *stream) { return ((HDAController *) controller)->OpenOutputStream(stream); };
	setPaused = [] (KAudioController *controller, KAudioMixer *stream, bool paused) { return ((HDAController *) controller)->SetPaused((HDAStream *) stream->data.p, paused); };

	// Start the controller.

	KTimeout timeout(50);

	WR_REGISTER_GCTL(RD_REGISTER_GCTL() | 1);
	while ((~RD_REGISTER_GCTL() & 1) && !timeout.Hit()); 

	if (timeout.Hit()) {
		KernelLog(LOG_ERROR, "HDA", "controller startup timeout", "Initialise - Timeout exceeded while waiting for the controller to start.\n");
		return;
	}

	// Check the version number is supported.

	uint8_t majorVersion = RD_REGISTER_VMAJ();
	uint8_t minorVersion = RD_REGISTER_VMIN();

	if (minorVersion != 0 || majorVersion != 1) {
		KernelLog(LOG_ERROR, "HDA", "unsupported version", "Initialise - HD Audio controller version number unsupported (%d.%d).\n", majorVersion, minorVersion);
		return;
	}

	// Enable interrupts.

	if (!pci->EnableSingleInterrupt([] (uintptr_t, void *context) { return ((HDAController *) context)->HandleIRQ(); }, this, "HD Audio")) {
		KernelLog(LOG_ERROR, "HDA", "IRQ registration failure", "Initialise - Could not register IRQ %d.\n", pci->interruptLine);
		return;
	}

	WR_REGISTER_INTCTL(1 << 31);

	// Get the bitset of the codec addresses in use.

	uint16_t codecAddressBitset = RD_REGISTER_STATESTS();

	if (!codecAddressBitset) {
		KernelLog(LOG_ERROR, "HDA", "no codecs", "Initialise - HD Audio controller reported no available codecs.\n");
		return;
	}

	// Get the number of available input and output streams.

	uint32_t globalCapabilities = RD_REGISTER_GCAP();
	supportedOutputStreams = (globalCapabilities >> 12) & 0xF;
	supportedInputStreams = (globalCapabilities >> 8) & 0xF;
	supportedBidiStreams = (globalCapabilities >> 3) & 0x1F;

#ifdef ARCH_64
	dma64Supported = (globalCapabilities & 1) ? true : false;
#endif

	// Initialise the CORB and RIRB.

	{
		WR_REGISTER_CORBCTL(RD_REGISTER_CORBCTL() & ~(1 << 1));
		WR_REGISTER_RIRBCTL(RD_REGISTER_RIRBCTL() & ~(1 << 1));

		if ((RD_REGISTER_CORBCTL() & (1 << 1)) || (RD_REGISTER_RIRBCTL() & (1 << 1))) {
			KernelLog(LOG_ERROR, "HDA", "RIRB/CORB start error", "Initialise - Could not stop the RIRB/CORB DMA engines.\n"); 
			return;
		}

		uint8_t *bufferVirtual;
		uintptr_t bufferPhysical;

		if (!MMPhysicalAllocateAndMap(0x800, 0, dma64Supported ? 64 : 32, true, ES_FLAGS_DEFAULT, &bufferVirtual, &bufferPhysical)) {
			KernelLog(LOG_ERROR, "HDA", "allocation failure", "Initialise - Could not allocate memory for the CORB/RIRB.\n");
			return;
		}

		uintptr_t corbPhysical = bufferPhysical, rirbPhysical = bufferPhysical + 0x800;
		corbVirtual = (uint32_t *) bufferVirtual, rirbVirtual = (uint64_t *) (bufferVirtual + 0x800);
		
		uint8_t availableCORBSizes = RD_REGISTER_CORBSIZE();
		if      (availableCORBSizes & (1 << 6)) { corbSize = 256; WR_REGISTER_CORBSIZE((availableCORBSizes & ~3) | 2); }
		else if (availableCORBSizes & (1 << 5)) { corbSize = 16;  WR_REGISTER_CORBSIZE((availableCORBSizes & ~3) | 1); }
		else if (availableCORBSizes & (1 << 4)) { corbSize = 2;   WR_REGISTER_CORBSIZE((availableCORBSizes & ~3) | 0); }
		else { KernelLog(LOG_ERROR, "HDA", "bad CORB size", "Initialise - No CORB sizes available.\n"); return; }

		uint8_t availableRIRBSizes = RD_REGISTER_RIRBSIZE();
		if      (availableRIRBSizes & (1 << 6)) { rirbSize = 256; WR_REGISTER_RIRBSIZE((availableRIRBSizes & ~3) | 2); }
		else if (availableRIRBSizes & (1 << 5)) { rirbSize = 16;  WR_REGISTER_RIRBSIZE((availableRIRBSizes & ~3) | 1); }
		else if (availableRIRBSizes & (1 << 4)) { rirbSize = 2;   WR_REGISTER_RIRBSIZE((availableRIRBSizes & ~3) | 0); }
		else { KernelLog(LOG_ERROR, "HDA", "bad RIRB size", "Initialise - No RIRB sizes available.\n"); return; }

		WR_REGISTER_CORBLBASE(corbPhysical & 0xFFFFFFFF);
		if (dma64Supported) WR_REGISTER_CORBUBASE((corbPhysical >> 32) & 0xFFFFFFFF);
		WR_REGISTER_RIRBLBASE(rirbPhysical & 0xFFFFFFFF);
		if (dma64Supported) WR_REGISTER_RIRBUBASE((rirbPhysical >> 32) & 0xFFFFFFFF);

		KTimeout timeout(5);
		WR_REGISTER_CORBWP(RD_REGISTER_CORBWP() & 0xFF00);
		WR_REGISTER_CORBRP(RD_REGISTER_CORBRP() | (1 << 15));
		while ((~RD_REGISTER_CORBRP() & (1 << 15)) && !timeout.Hit()); 
		WR_REGISTER_CORBRP(RD_REGISTER_CORBRP() & ~(1 << 15));
		while ((RD_REGISTER_CORBRP() & (1 << 15)) && !timeout.Hit()); 
		// Don't bother checking if timeout occurred since some implementations, *cough* vbox *cough*, don't implement this sequence properly.

		// Qemu doesn't seem to like leaving this 0, although that should correspond to 256 responses.
		WR_REGISTER_RINTCNT((RD_REGISTER_RINTCNT() & 0xFF00) | 255); 

		WR_REGISTER_CORBCTL(RD_REGISTER_CORBCTL() | (1 << 1));
		WR_REGISTER_RIRBCTL(RD_REGISTER_RIRBCTL() | (1 << 1) | (1 << 0 /* so we can clear the IRQ status bit, otherwise command processing stalls */));

		if ((~RD_REGISTER_CORBCTL() & (1 << 1)) || (~RD_REGISTER_RIRBCTL() & (1 << 1))) {
			KernelLog(LOG_ERROR, "HDA", "RIRB/CORB start error", "Initialise - Could not start the RIRB/CORB DMA engines.\n"); 
			return;
		}
	}

	// Enumerate nodes.

	for (uintptr_t codec = 0; codec < 16; codec++) {
		if (codecAddressBitset & (1 << codec)) {
			uint32_t vendorID = SendCommand(codec, 0, READ_PARAMETER_VENDOR_ID);
			if (!vendorID) continue;

			HDANode *node = (HDANode *) ArrayAddPointer(nodes, nullptr);
			if (!node) break;
			node->codec = codec, node->id = 0, node->root.vendorID = vendorID, node->type = NODE_ROOT;

			uint32_t childNodes = SendCommand(codec, 0, READ_PARAMETER_CHILD_NODES);
			uint8_t functionGroupCount = (childNodes >> 0) & 0xFF;
			uint8_t functionGroupNode = (childNodes >> 16) & 0xFF;

			for (uintptr_t i = functionGroupNode; i < functionGroupNode + functionGroupCount; i++) {
				uint8_t nodeType = SendCommand(codec, i, READ_PARAMETER_FUNCTION_GROUP_TYPE) & 0xFF;

				if (nodeType == 1) {
					HDANode *node = (HDANode *) ArrayAddPointer(nodes, nullptr);
					if (!node) break;
					node->codec = codec, node->id = i, node->type = NODE_AUDIO;
					InitialiseAudioFunction(node);
				}
			}
		}
	}

}

extern "C" void EntryHDAudio(KModuleInitilisationArguments *arguments) {
	arguments->driver->deviceAttached = [] (KDriver *driver, KDevice *_parent) {
		KPCIDevice *parent = (KPCIDevice *) _parent;

		HDAController *device = (HDAController *) driver->NewDevice(parent, sizeof(HDAController));
		if (!device) return;
		device->pci = parent;

		// Enable PCI features.
		parent->EnableFeatures(K_PCI_FEATURE_INTERRUPTS | K_PCI_FEATURE_BUSMASTERING_DMA | K_PCI_FEATURE_MEMORY_SPACE_ACCESS);

		// Initialise the controller.
		device->Initialise();

		// Register the audio controller.
		KRegisterDevice("Audio", device, 0);
	};
}
