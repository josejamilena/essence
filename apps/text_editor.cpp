#define ES_INSTANCE_TYPE Instance
#include <essence.h>

#include <shared/strings.cpp>

// TODO Document save/load model, then merge into API.
// TODO Merge toolbar switching into API.
// TODO Replace toolbar, then merge into API.
// TODO Font toolbar, then merge into API.
// TODO Word wrap (textbox feature).
// TODO Tab key.
// TODO Undo/redo, then merge into API.

// TODO Possible extension features:
// - Syntax highlighting
// - Line numbering
// - Block selection
// - Folding
// - Tab settings and auto-indent
// - Macros
// - Status bar
// - Goto line
// - Find in files
// - Convert case
// - Sort lines
// - Trim trailing space
// - Indent/comment/join/split shortcuts

struct Instance : EsInstance {
	EsTextbox *textboxDocument,
		  *textboxSearch;

	EsPanel *toolbarSwitcher,
		*toolbarMain,
		*toolbarSearch;

	EsTextDisplay *displaySearch;

	EsCommand commandFindNext,
		  commandFindPrevious,
		  commandFind;
};

void Find(Instance *instance, bool backwards) {
	EsPanelSwitchTo(instance->toolbarSwitcher, instance->toolbarSearch, ES_TRANSITION_SLIDE_UP);

	size_t needleBytes;
	char *needle = EsTextboxGetContents(instance->textboxSearch, &needleBytes);

	int32_t line0, byte0, line1, byte1;
	EsTextboxGetSelection(instance->textboxDocument, &line0, &byte0, &line1, &byte1);

	if (backwards) {
		if (line1 < line0) {
			line0 = line1;
			byte0 = byte1;
		} else if (line1 == line0 && byte1 < byte0) {
			byte0 = byte1;
		}
	} else {
		if (line1 > line0) {
			line0 = line1;
			byte0 = byte1;
		} else if (line1 == line0 && byte1 > byte0) {
			byte0 = byte1;
		}
	}

	bool found = EsTextboxFind(instance->textboxDocument, needle, needleBytes, &line0, &byte0, backwards ? ES_TEXTBOX_FIND_BACKWARDS : ES_FLAGS_DEFAULT);

	if (found) {
		EsTextDisplaySetContents(instance->displaySearch, "");
		EsTextboxSetSelection(instance->textboxDocument, line0, byte0, line0, byte0 + needleBytes);
		EsTextboxEnsureCaretVisible(instance->textboxDocument, true);
		EsElementFocus(instance->textboxDocument);
	} else if (!needleBytes) {
		EsTextDisplaySetContents(instance->displaySearch, INTERFACE_STRING(CommonSearchPrompt2));
		EsElementFocus(instance->textboxSearch);
	} else {
		EsTextDisplaySetContents(instance->displaySearch, INTERFACE_STRING(CommonSearchNoMatches));
		EsElementFocus(instance->textboxSearch);
	}

	EsHeapFree(needle);
}

void ProcessApplicationMessage(EsMessage *message) {
	if (message->type == ES_MSG_INSTANCE_CREATE) {
		Instance *instance = EsInstanceCreate(message, INTERFACE_STRING(TextEditorTitle));

		EsWindow *window = instance->window;
		EsWindowSetIcon(window, ES_ICON_ACCESSORIES_TEXT_EDITOR);
		EsElement *toolbarWrapper = EsWindowGetToolbar(window);
		EsPanel *toolbarSwitcher = instance->toolbarSwitcher = EsPanelCreate(toolbarWrapper, ES_CELL_FILL | ES_PANEL_SWITCHER, {});
		EsButton *button;

		// Commands:

		uint32_t stableID = 1;

		EsCommandRegister(&instance->commandFindNext, instance, [] (Instance *instance, EsElement *, EsCommand *) {
			Find(instance, false);
		}, stableID++, "F3"); 

		EsCommandRegister(&instance->commandFindPrevious, instance, [] (Instance *instance, EsElement *, EsCommand *) {
			Find(instance, true);
		}, stableID++, "Shift+F3"); 

		EsCommandRegister(&instance->commandFind, instance, [] (Instance *instance, EsElement *, EsCommand *) {
			EsPanelSwitchTo(instance->toolbarSwitcher, instance->toolbarSearch, ES_TRANSITION_ZOOM_OUT);
			EsElementFocus(instance->textboxSearch);
		}, stableID++, "Ctrl+F");

		EsCommandSetDisabled(&instance->commandFindNext, false);
		EsCommandSetDisabled(&instance->commandFindPrevious, false);
		EsCommandSetDisabled(&instance->commandFind, false);

		// Content:

		EsPanel *panel = EsPanelCreate(window, ES_CELL_FILL, ES_STYLE_PANEL_WINDOW_DIVIDER);
		instance->textboxDocument = EsTextboxCreate(panel, 
				ES_CELL_FILL | ES_TEXTBOX_MULTILINE | ES_TEXTBOX_ALLOW_TABS | ES_TEXTBOX_MARGIN, 
				ES_STYLE_TEXTBOX_NO_BORDER);
		instance->textboxDocument->cName = "document";
		EsTextboxSetUndoManager(instance->textboxDocument, instance->undoManager);
		EsElementFocus(instance->textboxDocument);

		// Main toolbar:

		EsPanel *toolbarMain = instance->toolbarMain = EsPanelCreate(toolbarSwitcher, ES_PANEL_HORIZONTAL | ES_CELL_FILL, ES_STYLE_PANEL_TOOLBAR);

		button = EsButtonCreate(toolbarMain, ES_BUTTON_TOOLBAR | ES_BUTTON_DROPDOWN, {}, INTERFACE_STRING(CommonFileMenu));
		button->cName = "file menu", button->accessKey = 'F';
		
		EsButtonOnCommand(button, [] (Instance *, EsElement *element, EsCommand *) {
			EsMenu *menu = EsMenuCreate(element);
			Instance *instance = menu->instance;

			// TODO.

			EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileNew));
			EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileOpen));
			EsMenuAddSeparator(menu);

			if (instance->documentState == ES_DOCUMENT_STATE_FILE) {
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileMakeCopy));
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileSaveNewVersion));
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileVersionHistory));
				EsMenuAddSeparator(menu);
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileRename));
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileViewInFolder));
			} else {
				EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileSave));
			}

			EsMenuAddSeparator(menu);
			EsMenuAddItem(menu, 0, INTERFACE_STRING(CommonFileExit));

			EsMenuNextColumn(menu);

			EsMenuAddItem(menu, ES_MENU_ITEM_HEADER, INTERFACE_STRING(CommonFileRecent));
			EsMenuAddItem(menu, 0, "Document 1");
			EsMenuAddItem(menu, 0, "Document 2");
			EsMenuAddItem(menu, 0, "Document 3");
			EsMenuAddItem(menu, 0, "Document 4");
			EsMenuAddItem(menu, 0, "Document 5");

			EsMenuShow(menu);
		}); 

		button = EsButtonCreate(toolbarMain, ES_BUTTON_TOOLBAR, {}, INTERFACE_STRING(CommonSearchOpen));
		button->cName = "search", button->accessKey = 'S';
		EsButtonSetIcon(button, ES_ICON_EDIT_FIND_SYMBOLIC);

		EsButtonOnCommand(button, [] (Instance *instance, EsElement *, EsCommand *) {
			EsPanelSwitchTo(instance->toolbarSwitcher, instance->toolbarSearch, ES_TRANSITION_SLIDE_UP);
			EsElementFocus(instance->textboxSearch);
		});

		button = EsButtonCreate(toolbarMain, ES_BUTTON_TOOLBAR, {}, INTERFACE_STRING(TextEditorSyntaxHighlighting));
		button->cName = "highlighting"; button->accessKey = 'H';
		EsButtonSetIcon(button, ES_ICON_FORMAT_TEXT_HIGHLIGHT);

		EsButtonOnCommand(button, [] (Instance *instance, EsElement *, EsCommand *) {
			EsTextStyle style;
			EsTextboxGetTextStyle(instance->textboxDocument, &style);
			style.font.family = ES_FONT_MONOSPACED;
			EsTextboxSetTextStyle(instance->textboxDocument, &style);
		});

		EsPanelSwitchTo(toolbarSwitcher, toolbarMain, ES_TRANSITION_NONE);

		// Search toolbar:

		EsPanel *toolbarSearch = instance->toolbarSearch = EsPanelCreate(toolbarSwitcher, ES_PANEL_HORIZONTAL | ES_CELL_FILL, ES_STYLE_PANEL_TOOLBAR);

		button = EsButtonCreate(toolbarSearch, ES_BUTTON_TOOLBAR, 0);
		button->cName = "go back", button->accessKey = 'X';
		EsButtonSetIcon(button, ES_ICON_GO_FIRST_SYMBOLIC);

		EsButtonOnCommand(button, [] (Instance *instance, EsElement *, EsCommand *) {
			EsPanelSwitchTo(instance->toolbarSwitcher, instance->toolbarMain, ES_TRANSITION_SLIDE_DOWN);
		});

		EsSpacerCreate(toolbarSearch, ES_FLAGS_DEFAULT);
		EsTextDisplayCreate(toolbarSearch, ES_ELEMENT_DEBUG, 0, INTERFACE_STRING(CommonSearchPrompt));
		EsSpacerCreate(toolbarSearch, ES_FLAGS_DEFAULT);

		instance->textboxSearch = EsTextboxCreate(toolbarSearch, ES_FLAGS_DEFAULT, {});
		instance->textboxSearch->cName = "search for";
		instance->textboxSearch->accessKey = 'S';

		instance->textboxSearch->userCallback = [] (EsElement *element, EsMessage *message) {
			Instance *instance = element->instance;

			if (message->type == ES_MSG_KEY_DOWN && message->keyboard.scancode == ES_SCANCODE_ENTER) {
				EsCommand *command = message->keyboard.shift ? &instance->commandFindPrevious : &instance->commandFindNext;
				command->callback(instance, element, command);
				return ES_HANDLED;
			} else if (message->type == ES_MSG_KEY_DOWN && message->keyboard.scancode == ES_SCANCODE_ESCAPE) {
				EsPanelSwitchTo(instance->toolbarSwitcher, instance->toolbarMain, ES_TRANSITION_SLIDE_DOWN);
				EsElementFocus(instance->textboxDocument);
				return ES_HANDLED;
			} else if (message->type == ES_MSG_FOCUSED_START) {
				EsTextboxSelectAll(instance->textboxSearch);
			}

			return 0;
		};

		EsSpacerCreate(toolbarSearch, ES_FLAGS_DEFAULT, 0, 7, 0);
		instance->displaySearch = EsTextDisplayCreate(toolbarSearch, ES_CELL_H_FILL, {}, "");

		button = EsButtonCreate(toolbarSearch, ES_BUTTON_TOOLBAR, {}, INTERFACE_STRING(CommonSearchNext));
		button->cName = "find next", button->accessKey = 'N';
		EsCommandAddButton(&instance->commandFindNext, button);
		button = EsButtonCreate(toolbarSearch, ES_BUTTON_TOOLBAR, {}, INTERFACE_STRING(CommonSearchPrevious));
		button->cName = "find previous", button->accessKey = 'P';
		EsCommandAddButton(&instance->commandFindPrevious, button);
	} else if (message->type == ES_MSG_INSTANCE_OPEN) {
		Instance *instance = message->instanceOpen.instance;
		size_t fileSize;
		// TODO UTF-8 validation.
		char *file = (char *) EsFileReadAll(message->instanceOpen.path, message->instanceOpen.pathBytes, &fileSize);
		EsTextboxSelectAll(instance->textboxDocument);
		EsTextboxInsert(instance->textboxDocument, file, fileSize);
		EsTextboxSetSelection(instance->textboxDocument, 0, 0, 0, 0);
		EsElementRelayout(instance->textboxDocument);
	}
}

void _start() {
	_init();

	while (true) {
		ProcessApplicationMessage(EsMessageReceive());
	}
}
