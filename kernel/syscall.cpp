// TODO Replace ES_ERROR_UNKNOWN with proper errors.
// TODO Clean up the return values for system calls; with FATAL_ERRORs there should need to be less error codes returned.
// TODO Close handles if OpenHandle fails or SendMessage fails.

#ifndef IMPLEMENTATION

struct User {
#define USER_STRING_MAX_LENGTH (4096)
	char name[USER_STRING_MAX_LENGTH], home[USER_STRING_MAX_LENGTH];
	size_t nameBytes, homeBytes;
};

KMutex userMutex, eventForwardMutex;

uintptr_t DoSyscall(EsSyscallType index,
		uintptr_t argument0, uintptr_t argument1,
		uintptr_t argument2, uintptr_t argument3,
		uint64_t flags, bool *fatal, uintptr_t *userStackPointer);

#define DO_SYSCALL_BATCHED (2)

struct MessageQueue {
	bool SendMessage(void *target, EsMessage &message); // Returns false if the message queue is full.
	bool SendMessage(_EsMessageWithObject &message); // Returns false if the message queue is full.
	bool GetMessage(_EsMessageWithObject &message);

#define MESSAGE_QUEUE_MAX_LENGTH (4096)
	/* Array */ _EsMessageWithObject *messages;

	uintptr_t mouseMovedMessage, 
		  windowResizedMessage, 
		  eyedropResultMessage,
		  keyRepeatMessage;

	bool pinged;

	KMutex mutex;
	KEvent notEmpty;
};

#endif

#ifdef IMPLEMENTATION

uint64_t globalSystemConstants[256];

bool MessageQueue::SendMessage(void *object, EsMessage &_message) {
	// TODO Remove unnecessary copy.
	_EsMessageWithObject message = { object, _message };
	return SendMessage(message);
}

bool MessageQueue::SendMessage(_EsMessageWithObject &_message) {
	// TODO Don't send messages if the process has been terminated.

	mutex.Acquire();
	EsDefer(mutex.Release());

	if (!messages) {
		ArrayInitialise(messages, K_FIXED);
	} else if (ArrayLength(messages) == MESSAGE_QUEUE_MAX_LENGTH) {
		return false;
	}

#define MERGE_MESSAGES(variable, change) \
	do { \
		if (variable && messages[variable - 1].object == _message.object) { \
			if (change) EsMemoryCopy(messages + variable - 1, &_message, sizeof(_EsMessageWithObject)); \
		} else if (ArrayAdd(messages, _message)) { \
			variable = ArrayLength(messages); \
		} else { \
			return false; \
		} \
	} while (0)

	// NOTE Don't forget to update GetMessage with the merged messages!

	if (_message.message.type == ES_MSG_MOUSE_MOVED) {
		MERGE_MESSAGES(mouseMovedMessage, true);
	} else if (_message.message.type == ES_MSG_WINDOW_RESIZED) {
		MERGE_MESSAGES(windowResizedMessage, true);
	} else if (_message.message.type == ES_MSG_EYEDROP_REPORT) {
		MERGE_MESSAGES(eyedropResultMessage, true);
	} else if (_message.message.type == ES_MSG_KEY_DOWN && _message.message.keyboard.repeat) {
		MERGE_MESSAGES(keyRepeatMessage, false);
	} else {
		if (!ArrayAdd(messages, _message)) {
			return false;
		}

		if (_message.message.type == ES_MSG_PING) {
			pinged = true;
		}
	}

	if (!notEmpty.Poll()) {
		notEmpty.Set();
	}

	return true;
}

bool MessageQueue::GetMessage(_EsMessageWithObject &_message) {
	mutex.Acquire();
	EsDefer(mutex.Release());

	if (!messages || !ArrayLength(messages)) {
		return false;
	}

	_message = messages[0];
	ArrayDelete(messages, 0);

	if (mouseMovedMessage)    mouseMovedMessage--;
	if (windowResizedMessage) windowResizedMessage--;
	if (eyedropResultMessage) eyedropResultMessage--;
	if (keyRepeatMessage)     keyRepeatMessage--;

	pinged = false;

	if (!ArrayLength(messages)) {
		notEmpty.Reset();
	}

	return true;
}

#define CHECK_OBJECT(x) if (!x.valid) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_HANDLE, !x.softFailure); else x.checked = true
#define SYSCALL_BUFFER_LIMIT (64 * 1024 * 1024) // To prevent overflow and DOS attacks.
#define SYSCALL_BUFFER(address, length, index) \
	MMRegion *_region ## index = MMFindAndPinRegion(currentVMM, (address), (length)); \
	if (!_region ## index) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true); \
	EsDefer(if (_region ## index) MMUnpinRegion(currentVMM, _region ## index));
#define SYSCALL_BUFFER_ALLOW_NULL(address, length, index) \
	MMRegion *_region ## index = MMFindAndPinRegion(currentVMM, (address), (length)); \
	EsDefer(if (_region ## index) MMUnpinRegion(currentVMM, _region ## index));
#define SYSCALL_HANDLE(handle, type, __object, index) \
	KObject _object ## index(currentProcess, handle, type); \
	CHECK_OBJECT(_object ## index); \
	*((void **) &__object) = (_object ## index).object;
#define SYSCALL_READ(destination, source, length) \
	do { if ((length) > 0) { \
		SYSCALL_BUFFER((uintptr_t) (source), (length), 0); \
		EsMemoryCopy((void *) (destination), (const void *) (source), (length)); \
		__sync_synchronize(); \
	}} while (0)
#define SYSCALL_READ_HEAP(destination, source, length) \
	if ((length) > 0) { \
		void *x = EsHeapAllocate((length), false, K_FIXED); \
		if (!x) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false); \
		bool success = false; \
		EsDefer(if (!success) EsHeapFree(x, 0, K_FIXED);); \
		SYSCALL_BUFFER((uintptr_t) (source), (length), 0); \
		*((void **) &(destination)) = x; \
		EsMemoryCopy((void *) (destination), (const void *) (source), (length)); \
		success = true; \
		__sync_synchronize(); \
	} else destination = nullptr; EsDefer(EsHeapFree(destination, 0, K_FIXED))
#define SYSCALL_WRITE(destination, source, length) \
	do { \
		__sync_synchronize(); \
		SYSCALL_BUFFER((uintptr_t) (destination), (length), 0); \
		EsMemoryCopy((void *) (destination), (const void *) (source), (length)); \
	} while (0)
#define SYSCALL_ARGUMENTS uintptr_t argument0, uintptr_t argument1, uintptr_t argument2, uintptr_t argument3, \
		Thread *currentThread, Process *currentProcess, MMSpace *currentVMM, uintptr_t *userStackPointer, bool &fatalError
#define SYSCALL_IMPLEMENT(_type) uintptr_t Do ## _type ( SYSCALL_ARGUMENTS ) 
#define SYSCALL_RETURN(value, fatal) do { fatalError = fatal; return (value); } while (0)
#define SYSCALL_PERMISSION(x) do { if ((x) != (currentProcess->permissions & (x))) { fatalError = true; return ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS; } } while (0)
typedef uintptr_t (*SyscallFunction)(SYSCALL_ARGUMENTS);
#pragma GCC diagnostic ignored "-Wunused-parameter" push

SYSCALL_IMPLEMENT(ES_SYSCALL_COUNT) {
	SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN_SYSCALL, true);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_PRINT) {
	char *buffer; SYSCALL_READ_HEAP(buffer, argument0, argument1);
	EsPrint("%s", argument1, buffer);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_ALLOCATE) {
	EsMemoryProtection protection = (EsMemoryProtection) argument2;
	uintptr_t address = (uintptr_t) MMStandardAllocate(currentVMM, argument0, 
			protection == ES_MEMORY_PROTECTION_READ_ONLY ? MM_REGION_READ_ONLY
			: protection == ES_MEMORY_PROTECTION_EXECUTABLE ? MM_REGION_EXECUTABLE 
			: ES_FLAGS_DEFAULT, nullptr, argument1 & ES_MEMORY_RESERVE_COMMIT_ALL);
	SYSCALL_RETURN(address, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_FREE) {
	if (!MMFree(currentVMM, (void *) argument0, argument1)) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_MEMORY_REGION, true);
	}

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MEMORY_COMMIT) {
	SYSCALL_BUFFER(argument0 << K_PAGE_BITS, argument1 << K_PAGE_BITS, 0);

	argument0 -= _region0->baseAddress >> K_PAGE_BITS;

	if (argument0 >= _region0->pageCount || argument1 > _region0->pageCount - argument0 || (~_region0->flags & MM_REGION_NORMAL) || !argument1) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_MEMORY_REGION, true);
	}

	bool success = false;

	if (argument2 == 0) {
		currentVMM->reserveMutex.Acquire();
		success = MMCommitRange(currentVMM, _region0, argument0, argument1); 
		currentVMM->reserveMutex.Release();
	} else if (argument2 == 1) {
		currentVMM->reserveMutex.Acquire();
		success = MMDecommitRange(currentVMM, _region0, argument0, argument1); 
		currentVMM->reserveMutex.Release();
	} else {
		SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN_SYSCALL, true);
	}

	SYSCALL_RETURN(success ? ES_SUCCESS : ES_ERROR_INSUFFICIENT_RESOURCES, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CREATE_PROCESS) {
	SYSCALL_PERMISSION(ES_PERMISSION_PROCESS_CREATE);

	EsProcessCreationArguments arguments;
	SYSCALL_READ(&arguments, argument0, sizeof(EsProcessCreationArguments));

	if (arguments.permissions == ES_PERMISSION_INHERIT) {
		arguments.permissions = currentProcess->permissions;
	}

	if (arguments.permissions & ~currentProcess->permissions) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS, true);
	}

	if (arguments.executablePathBytes > K_MAX_PATH) {
		SYSCALL_RETURN(ES_FATAL_ERROR_PATH_LENGTH_EXCEEDS_LIMIT, true);
	}

	char *executablePath = (char *) EsHeapAllocate(arguments.executablePathBytes, false, K_FIXED);

	if (!executablePath) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	EsDefer(EsHeapFree(executablePath, arguments.executablePathBytes, K_FIXED));
	if (arguments.executablePathBytes > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ(executablePath, arguments.executablePath, arguments.executablePathBytes);

	EsProcessInformation processInformation;
	EsMemoryZero(&processInformation, sizeof(EsProcessInformation));

	Process *process = scheduler.SpawnProcess();

	if (!process) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	process->creationArgument = arguments.creationArgument.p;
	process->permissions = arguments.permissions;
	process->user = desktopProcess->user;

	processInformation.handle = currentProcess->handleTable.OpenHandle(process, 0, KERNEL_OBJECT_PROCESS); 
	processInformation.pid = process->id;

	if (arguments.environmentBlockBytes) {
		if (arguments.environmentBlockBytes > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
		SYSCALL_BUFFER((uintptr_t) arguments.environmentBlock, arguments.environmentBlockBytes, 1);
		process->creationArgument = (void *) MakeConstantBuffer(arguments.environmentBlock, arguments.environmentBlockBytes, process);
	}

	if (!process->Start(executablePath, arguments.executablePathBytes)) {
		CloseHandleToObject(process, KERNEL_OBJECT_PROCESS);
		SYSCALL_RETURN(ES_ERROR_UNKNOWN, false);
	}

	processInformation.mainThread.handle = currentProcess->handleTable.OpenHandle(process->executableMainThread, 0, KERNEL_OBJECT_THREAD);
	processInformation.mainThread.tid = process->executableMainThread->id;

	SYSCALL_WRITE(argument2, &processInformation, sizeof(EsProcessInformation));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_CREATION_ARGUMENT) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_PROCESS | KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(object);

	uintptr_t creationArgument = 0;

	switch (object.type) {
		case KERNEL_OBJECT_PROCESS: {
			creationArgument = (uintptr_t) ((Process *) object.object)->creationArgument;
		} break;

		case KERNEL_OBJECT_WINDOW: {
			creationArgument = (uintptr_t) ((Window *) object.object)->apiWindow;
		} break;

		default: {
			KernelPanic("DoSyscall - Invalid creation argument object type.\n");
		} break;
	}

	SYSCALL_RETURN(creationArgument, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_FORCE_SCREEN_UPDATE) {
	if (argument0) {
		SYSCALL_PERMISSION(ES_PERMISSION_SCREEN_MODIFY);
	}

	windowManager.mutex.Acquire();

	if (argument0) {
		windowManager.Redraw(ES_MAKE_POINT(0, 0), graphics.frameBuffer.width, graphics.frameBuffer.height, nullptr);
	}

	GraphicsUpdateScreen();
	windowManager.mutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_START_EYEDROP) {
	KObject _avoid(currentProcess, argument1, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_avoid);
	Window *avoid = (Window *) _avoid.object;

	windowManager.StartEyedrop(argument0, avoid, argument2);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_MESSAGE) {
	_EsMessageWithObject message;

	if (currentProcess->messageQueue.GetMessage(message)) {
		SYSCALL_WRITE(argument0, &message, sizeof(_EsMessageWithObject));
		SYSCALL_RETURN(ES_SUCCESS, false);
	} else {
		SYSCALL_RETURN(ES_ERROR_NO_MESSAGES_AVAILABLE, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WAIT_MESSAGE) {
	currentThread->terminatableState = THREAD_USER_BLOCK_REQUEST;
	currentProcess->messageQueue.notEmpty.Wait(argument0 /* timeout */);
	currentThread->terminatableState = THREAD_IN_SYSCALL;

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_EMBED) {
	KObject _container(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_container);
	Window *container = (Window *) _container.object;

	KObject _window(currentProcess, argument1, KERNEL_OBJECT_EMBEDDED_WINDOW | KERNEL_OBJECT_NONE);
	CHECK_OBJECT(_window);
	EmbeddedWindow *window = (EmbeddedWindow *) _window.object;

	windowManager.mutex.Acquire();
	container->SetEmbed(window);
	windowManager.mutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_CREATE) {
	if (argument0 == ES_WINDOW_NORMAL) {
		void *_window = windowManager.CreateEmbeddedWindow(currentProcess, (void *) argument2);
		SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(_window, 0, KERNEL_OBJECT_EMBEDDED_WINDOW), false);
	} else {
		void *_window = windowManager.CreateWindow(currentProcess, (void *) argument2, (EsWindowStyle) argument0);

		if (!_window) {
			SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
		} else {
			SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(_window, 0, KERNEL_OBJECT_WINDOW), false);
		}
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_CLOSE) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW | KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);
	windowManager.mutex.Acquire();

	if (_window.type == KERNEL_OBJECT_EMBEDDED_WINDOW) {
		EmbeddedWindow *window = (EmbeddedWindow *) _window.object;
		window->Close();
	} else {
		Window *window = (Window *) _window.object;
		window->Close();
	}
	
	windowManager.mutex.Release();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_EMBED_OWNER) {
	if (currentProcess != desktopProcess) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS, true);
	}

	EmbeddedWindow *window;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_EMBEDDED_WINDOW, window, 1);
	Process *process;
	SYSCALL_HANDLE(argument1, KERNEL_OBJECT_PROCESS, process, 2);

	OpenHandleToObject(window, KERNEL_OBJECT_EMBEDDED_WINDOW);
	EsHandle handle = process->handleTable.OpenHandle(window, 0, KERNEL_OBJECT_EMBEDDED_WINDOW);

	windowManager.mutex.Acquire();
	window->SetEmbedOwner(process);
	windowManager.mutex.Release();
	SYSCALL_RETURN(handle, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_OBJECT) {
	EmbeddedWindow *window;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_EMBEDDED_WINDOW, window, 1);

	if (window->owner != currentProcess) {
		// TODO Permissions.
	}

	void *old = window->apiWindow;
	window->apiWindow = (void *) argument2;
	__sync_synchronize();

	windowManager.mutex.Acquire();

	if (window->container) {
		EsMessage message;
		EsMemoryZero(&message, sizeof(EsMessage));
		message.type = ES_MSG_WINDOW_RESIZED;
		int embedWidth = window->container->width - WINDOW_INSET * 2;
		int embedHeight = window->container->height - WINDOW_INSET * 2 - CONTAINER_TAB_BAND_HEIGHT;
		message.windowResized.content = ES_MAKE_RECTANGLE(0, embedWidth, 0, embedHeight);
		window->owner->messageQueue.SendMessage((void *) argument2, message);
	}

	windowManager.mutex.Release();

	SYSCALL_RETURN((uintptr_t) old, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_OPAQUE_BOUNDS) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);
	Window *window = (Window *) _window.object;
	SYSCALL_READ(&window->opaqueBounds, argument1, sizeof(EsRectangle));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_BLUR_BOUNDS) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);
	Window *window = (Window *) _window.object;
	SYSCALL_READ(&window->blurBounds, argument1, sizeof(EsRectangle));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_SOLID) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);
	Window *window = (Window *) _window.object;
	window->solid = (argument1 & ES_WINDOW_SOLID_TRUE) != 0;
	window->noClickActivate = (argument1 & ES_WINDOW_SOLID_NO_ACTIVATE) != 0;
	window->shadowSize = argument2;
	windowManager.mutex.Acquire();
	windowManager.MoveCursor(0, 0);
	windowManager.mutex.Release();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_REDRAW) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);
	Window *window = (Window *) _window.object;
	windowManager.mutex.Acquire();
	window->Update(nullptr, false);
	windowManager.mutex.Release();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SET_WINDOW_FRAME_BITMAP) {
	if (desktopProcess != currentProcess) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS, true);
	}

	SYSCALL_BUFFER(argument1, windowManager.windowFrameBitmap.width * windowManager.windowFrameBitmap.height * 4, 1);

	windowManager.mutex.Acquire();
	windowManager.windowFrameBitmap.SetBits((K_USER_BUFFER const void *) argument1, argument3, 
			ES_MAKE_RECTANGLE(0, windowManager.windowFrameBitmap.width, 0, windowManager.windowFrameBitmap.height));
	windowManager.mutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_BITS) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW | KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);

	EsRectangle region;
	SYSCALL_READ(&region, argument1, sizeof(EsRectangle));
	SYSCALL_BUFFER(argument2, Width(region) * Height(region) * 4, 1);

	Surface *surface = _window.type == KERNEL_OBJECT_EMBEDDED_WINDOW 
		? &((EmbeddedWindow *) _window.object)->surface 
		: &((Window *) _window.object)->surface;
	Window *window = _window.type == KERNEL_OBJECT_EMBEDDED_WINDOW
		? ((EmbeddedWindow *) _window.object)->container
		: ((Window *) _window.object);

	if (!window || (_window.type == KERNEL_OBJECT_EMBEDDED_WINDOW 
				&& currentProcess != ((EmbeddedWindow *) _window.object)->owner)) {
		SYSCALL_RETURN(ES_SUCCESS, false);
	}

	int offsetX = 0, offsetY = 0;

	if (_window.type == KERNEL_OBJECT_EMBEDDED_WINDOW) {
		offsetX = WINDOW_INSET;
		offsetY = WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT;
	} else if (window->style == ES_WINDOW_CONTAINER) {
		offsetX = offsetY = WINDOW_INSET;
	}

	EsRectangle offsetRegion = { region.l + offsetX, region.r + offsetX, region.t + offsetY, region.b + offsetY };

	windowManager.mutex.Acquire();
	bool skipUpdate = false;

	if (window->closed) {
		skipUpdate = true;
	} else if (region.l < 0 || region.r > (int32_t) graphics.width * 2
			|| region.t < 0 || region.b > (int32_t) graphics.height * 2
			|| region.l >= region.r || region.t >= region.b) {
		skipUpdate = true;
	} else {
		uintptr_t stride = Width(region) * 4;
		EsRectangle clippedRegion;
		EsRectangleClip(region, { 0, (int32_t) surface->width, 0, (int32_t) surface->height }, &clippedRegion);
		EsRectangle directRegion = { clippedRegion.l + offsetX, clippedRegion.r + offsetX, clippedRegion.t + offsetY, clippedRegion.b + offsetY };
		skipUpdate = window->UpdateDirect((K_USER_BUFFER uint32_t *) argument2, stride, directRegion);
		surface->SetBits((K_USER_BUFFER const void *) argument2, stride, clippedRegion);
	}

	window->Update(&offsetRegion, skipUpdate);
	windowManager.mutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CLIPBOARD_ADD) {
	if (argument0 != ES_CLIPBOARD_PRIMARY) {
		SYSCALL_RETURN(ES_ERROR_INVALID_CLIPBOARD, false);
	}

	EsError error = primaryClipboard.Add((uintptr_t) argument1, (const void *) argument2, argument3);
	SYSCALL_RETURN(error, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CLIPBOARD_HAS) {
	if (argument0 != ES_CLIPBOARD_PRIMARY) {
		SYSCALL_RETURN(ES_ERROR_INVALID_CLIPBOARD, false);
	}

	bool result = primaryClipboard.Has((uintptr_t) argument1);
	SYSCALL_RETURN(result, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CLIPBOARD_READ) {
	if (argument0 != ES_CLIPBOARD_PRIMARY) {
		SYSCALL_RETURN(ES_ERROR_INVALID_CLIPBOARD, false);
	}

	EsHandle handle = primaryClipboard.Read((uintptr_t) argument1, (size_t *) argument3, currentProcess);
	SYSCALL_RETURN(handle, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CREATE_EVENT) {
	KEvent *event = (KEvent *) EsHeapAllocate(sizeof(KEvent), true, K_FIXED);
	if (!event) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	event->handles = 1;
	event->autoReset = argument0;
	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(event, 0, KERNEL_OBJECT_EVENT), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CLOSE_HANDLE) {
	if (!currentProcess->handleTable.CloseHandle(argument0)) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_HANDLE, true);
	}

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_TERMINATE_THREAD) {
	KObject _thread(currentProcess, argument0, KERNEL_OBJECT_THREAD);
	CHECK_OBJECT(_thread);
	Thread *thread = (Thread *) _thread.object;

	// TODO What happens to the handle to the object if we're terminating ourself?
	scheduler.TerminateThread(thread);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_TERMINATE_PROCESS) {
	KObject _process(currentProcess, argument0, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(_process);
	Process *process = (Process *) _process.object;

	// TODO What happens to the handle to the object if we're terminating ourself?
	// TODO Prevent the termination of the kernel/desktop.
	scheduler.TerminateProcess(process, argument1);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CREATE_THREAD) {
	EsThreadInformation thread;
	EsMemoryZero(&thread, sizeof(EsThreadInformation));
	Thread *threadObject = scheduler.SpawnThread("Syscall", argument0, argument3, SPAWN_THREAD_USERLAND, currentProcess, argument1);

	if (!threadObject) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	// Register processObject as a handle.
	thread.handle = currentProcess->handleTable.OpenHandle(threadObject, 0, KERNEL_OBJECT_THREAD); 
	thread.tid = threadObject->id;

	SYSCALL_WRITE(argument2, &thread, sizeof(EsThreadInformation));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_OPEN_SHARED_MEMORY) {
	if (argument0 > ES_SHARED_MEMORY_MAXIMUM_SIZE) SYSCALL_RETURN(ES_FATAL_ERROR_SHARED_MEMORY_REGION_TOO_LARGE, true);
	if (argument1 && !argument2) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	if (argument2 > ES_SHARED_MEMORY_NAME_MAX_LENGTH) SYSCALL_RETURN(ES_FATAL_ERROR_PATH_LENGTH_EXCEEDS_LIMIT, true);

	char *name;
	if (argument2 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ_HEAP(name, argument1, argument2);

	MMSharedRegion *region = MMSharedOpenRegion(name, argument2, argument0, argument3);
	if (!region) SYSCALL_RETURN(ES_INVALID_HANDLE, false);

	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(region, 0, KERNEL_OBJECT_SHMEM), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MAP_OBJECT) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_SHMEM | KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);

	if (object.type == KERNEL_OBJECT_SHMEM) {
		// TODO Access permissions and modes.
		MMSharedRegion *region = (MMSharedRegion *) object.object;

		if (argument2 == ES_MAP_OBJECT_ALL) {
			argument2 = region->sizeBytes;
		}

		uintptr_t address = (uintptr_t) MMMapShared(currentVMM, region, argument1, argument2);
		SYSCALL_RETURN(address, false);
	} else if (object.type == KERNEL_OBJECT_NODE) {
		KNode *file = (KNode *) object.object;

		if (file->directoryEntry->type != ES_NODE_FILE) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);

		if (argument3 == ES_MAP_OBJECT_READ_WRITE) {
			if (!(object.flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE))) {
				SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_FILE_ACCESS, true);
			}
		} else {
			if (!(object.flags & (ES_FILE_READ | ES_FILE_READ_SHARED))) {
				SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_FILE_ACCESS, true);
			}
		}

		if (argument2 == ES_MAP_OBJECT_ALL) {
			argument2 = file->directoryEntry->totalSize;
		}

		uintptr_t address = (uintptr_t) MMMapFile(currentVMM, (FSFile *) file, argument1, argument2, argument3);
		SYSCALL_RETURN(address, false);
	}

	SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN, true);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SHARE_CONSTANT_BUFFER) {
	KObject buffer(currentProcess, argument0, KERNEL_OBJECT_CONSTANT_BUFFER);
	CHECK_OBJECT(buffer);
	ConstantBuffer *object = (ConstantBuffer *) buffer.object;

	KObject process(currentProcess, argument1, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(process);

	SYSCALL_RETURN(MakeConstantBuffer(object + 1, object->bytes, (Process *) process.object), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CREATE_CONSTANT_BUFFER) {
	if (argument2 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_BUFFER(argument0, argument2, 1);

	KObject process(currentProcess, argument1, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(process);

	SYSCALL_RETURN(MakeConstantBuffer((void *) argument0, argument2, (Process *) process.object), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SHARE_MEMORY) {
	KObject _region(currentProcess, argument0, KERNEL_OBJECT_SHMEM);
	CHECK_OBJECT(_region);
	MMSharedRegion *region = (MMSharedRegion *) _region.object;

	KObject _process(currentProcess, argument1, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(_process);
	Process *process = (Process *) _process.object;

	OpenHandleToObject(region, KERNEL_OBJECT_SHMEM); // TODO Flags.

	SYSCALL_RETURN(process->handleTable.OpenHandle(region, argument2, KERNEL_OBJECT_SHMEM), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_OPEN_NODE) {
	char *path;
	if (argument1 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ_HEAP(path, argument0, argument1);

	size_t pathLength = (size_t) argument1;
	uint64_t flags = (uint64_t) argument2;

	if (!pathLength || *path != '/') {
		size_t installationPathLength = currentProcess->executablePathLength;

		for (intptr_t i = currentProcess->executablePathLength - 1; i >= 0; i--) {
			if (currentProcess->executablePath[i] == '/') {
				installationPathLength = i + 1;
				break;
			}
		}

		char *p = (char *) EsHeapAllocate(installationPathLength + pathLength, false, K_FIXED);
		EsMemoryCopy(p, currentProcess->executablePath, installationPathLength);
		EsMemoryCopy(p + installationPathLength, path, pathLength);
		EsHeapFree(path, 0, K_FIXED);
		path = p;
		pathLength += installationPathLength;
		// EsPrint("opennode, replace path with %s\n", pathLength, p);
	}

	flags &= ~_ES_NODE_FROM_WRITE_EXCLUSIVE;

	KNodeInformation _information = FSNodeOpen(path, pathLength, flags);

	if (!_information.node) {
		SYSCALL_RETURN(_information.error, false);
	}

	if (flags & ES_FILE_WRITE_EXCLUSIVE) {
		// Mark this handle as being the exclusive writer for this file.
		// This way, when the handle is used, OpenHandleToObject succeeds.
		// The exclusive writer flag will only be removed from the file where countWrite drops to zero.
		flags |= _ES_NODE_FROM_WRITE_EXCLUSIVE;
	}

	_EsNodeInformation information;
	EsMemoryZero(&information, sizeof(_EsNodeInformation));
	information.type = _information.node->directoryEntry->type;
	information.fileSize = _information.node->directoryEntry->totalSize;
	information.directoryChildren = _information.node->directoryEntry->directoryChildren;
	information.handle = currentProcess->handleTable.OpenHandle(_information.node, flags, KERNEL_OBJECT_NODE);
	SYSCALL_WRITE(argument3, &information, sizeof(_EsNodeInformation));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_DELETE_NODE) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *node = (KNode *) object.object;
	
	if (node->directoryEntry->type == ES_NODE_FILE && (~object.flags & ES_FILE_WRITE_EXCLUSIVE)) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_FILE_ACCESS, true);
	}

	if (node->directoryEntry->type == ES_NODE_DIRECTORY || node->directoryEntry->type == ES_NODE_FILE) {
		SYSCALL_RETURN(FSNodeDelete(node), false);
	} else {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MOVE_NODE) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;

	KObject object2(currentProcess, argument1, KERNEL_OBJECT_NODE | KERNEL_OBJECT_NONE);
	CHECK_OBJECT(object2);
	KNode *directory = (KNode *) object2.object;

	char *newPath;
	if (argument3 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ_HEAP(newPath, argument2, argument3);
	SYSCALL_RETURN(FSNodeMove(file, directory, newPath, (size_t) argument3), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_READ_FILE_SYNC) {
	if (!argument2) SYSCALL_RETURN(0, false);

	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;

	if (file->directoryEntry->type != ES_NODE_FILE) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);

	SYSCALL_BUFFER(argument3, argument2, 1);

	size_t result = FSFileReadSync(file, (void *) argument3, argument1, argument2, 
			(_region1->flags & MM_REGION_FILE) ? FS_FILE_ACCESS_USER_BUFFER_MAPPED : 0);
	SYSCALL_RETURN(result, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WRITE_FILE_SYNC) {
	if (!argument2) SYSCALL_RETURN(0, false);
		
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;

	if (file->directoryEntry->type != ES_NODE_FILE) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);

	SYSCALL_BUFFER(argument3, argument2, 1);

	if (object.flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE)) {
		size_t result = FSFileWriteSync(file, (void *) argument3, argument1, argument2, 
				(_region1->flags & MM_REGION_FILE) ? FS_FILE_ACCESS_USER_BUFFER_MAPPED : 0);
		SYSCALL_RETURN(result, false);
	} else {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_FILE_ACCESS, true);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_FILE_GET_SIZE) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;
	if (file->directoryEntry->type != ES_NODE_FILE) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);
	SYSCALL_RETURN(file->directoryEntry->totalSize, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_RESIZE_FILE) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;

	if (file->directoryEntry->type != ES_NODE_FILE) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);

	if (object.flags & (ES_FILE_WRITE | ES_FILE_WRITE_EXCLUSIVE)) {
		SYSCALL_RETURN(FSFileResize(file, argument1), false);
	} else {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_FILE_ACCESS, true);
	}
}
					     
SYSCALL_IMPLEMENT(ES_SYSCALL_SET_EVENT) {
	KObject _event(currentProcess, argument0, KERNEL_OBJECT_EVENT);
	CHECK_OBJECT(_event);
	KEvent *event = (KEvent *) _event.object;

	event->Set(false, true);

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_RESET_EVENT) {
	KObject _event(currentProcess, argument0, KERNEL_OBJECT_EVENT);
	CHECK_OBJECT(_event);
	KEvent *event = (KEvent *) _event.object;

	event->Reset();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_POLL_EVENT) {
	KObject _event(currentProcess, argument0, KERNEL_OBJECT_EVENT);
	CHECK_OBJECT(_event);
	KEvent *event = (KEvent *) _event.object;

	bool eventWasSet = event->Poll();
	SYSCALL_RETURN(eventWasSet ? ES_SUCCESS : ES_ERROR_EVENT_NOT_SET, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SLEEP) {
	KTimer timer = {};
	timer.Set((argument0 << 32) | argument1, false);
	timer.event.Wait(ES_WAIT_NO_TIMEOUT);
	timer.Remove();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WAIT) {
	if (argument1 >= ES_MAX_WAIT_COUNT - 1 /* leave room for timeout timer */) {
		SYSCALL_RETURN(ES_FATAL_ERROR_TOO_MANY_WAIT_OBJECTS, true);
	}

	EsHandle handles[ES_MAX_WAIT_COUNT];
	SYSCALL_READ(handles, argument0, argument1 * sizeof(EsHandle));

	KEvent *events[ES_MAX_WAIT_COUNT];
	KObject _objects[ES_MAX_WAIT_COUNT] = {};

	for (uintptr_t i = 0; i < argument1; i++) {
		_objects[i].Initialise(currentProcess, handles[i], 
				KERNEL_OBJECT_PROCESS | KERNEL_OBJECT_THREAD | KERNEL_OBJECT_EVENT | KERNEL_OBJECT_EVENT_SINK | KERNEL_OBJECT_AUDIO_STREAM);
		CHECK_OBJECT(_objects[i]);

		void *object = _objects[i].object;

		switch (_objects[i].type) {
			case KERNEL_OBJECT_PROCESS: {
				events[i] = &((Process *) object)->killedEvent;
			} break;

			case KERNEL_OBJECT_THREAD: {
				events[i] = &((Thread *) object)->killedEvent;
			} break;

			case KERNEL_OBJECT_EVENT_SINK: {
				events[i] = &((EventSink *) object)->available;
			} break;

			case KERNEL_OBJECT_AUDIO_STREAM: {
				events[i] = &((KAudioStream *) object)->fillBuffer;
			} break;

			case KERNEL_OBJECT_EVENT: {
				events[i] = (KEvent *) object;
			} break;

			default: {
				KernelPanic("DoSyscall - Unexpected wait object type %d.\n", _objects[i].type);
			} break;
		}
	}

	size_t waitObjectCount = argument1;
	KTimer *timer = nullptr;

	if (argument2 != (uintptr_t) ES_WAIT_NO_TIMEOUT) {
		KTimer _timer = {};
		_timer.Set(argument2, false);
		events[waitObjectCount++] = &_timer.event;
		timer = &_timer;
	}

	uintptr_t waitReturnValue;
	currentThread->terminatableState = THREAD_USER_BLOCK_REQUEST;
	waitReturnValue = scheduler.WaitEvents(events, waitObjectCount);
	currentThread->terminatableState = THREAD_IN_SYSCALL;

	if (waitReturnValue == argument1) {
		waitReturnValue = ES_ERROR_TIMEOUT_REACHED;
	}

	if (timer) {
		timer->Remove();
	}

	SYSCALL_RETURN(waitReturnValue, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_CURSOR) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW | KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);

	uint32_t imageWidth = (argument2 >> 16) & 0xFF;
	uint32_t imageHeight = (argument2 >> 24) & 0xFF;

	SYSCALL_BUFFER(argument1, imageWidth * imageHeight * 4, 1);

	windowManager.mutex.Acquire();
	Window *window;

	if (_window.type == KERNEL_OBJECT_EMBEDDED_WINDOW) {
		EmbeddedWindow *embeddedWindow = (EmbeddedWindow *) _window.object;
		window = embeddedWindow->container;

		if (!window || !window->hoveringOverEmbed || embeddedWindow->owner != currentProcess) {
			windowManager.mutex.Release();
			SYSCALL_RETURN(ES_SUCCESS, false);
		}
	} else {
		window = (Window *) _window.object;

		if (window->hoveringOverEmbed) {
			windowManager.mutex.Release();
			SYSCALL_RETURN(ES_SUCCESS, false);
		}
	}

	bool changedCursor = false;

	if (!window->closed && argument1 != windowManager.cursorID && !windowManager.eyedropping && (windowManager.hoverWindow == window || !windowManager.hoverWindow)) {
		windowManager.cursorID = argument1;
		windowManager.cursorImageOffsetX = (int8_t) ((argument2 >> 0) & 0xFF);
		windowManager.cursorImageOffsetY = (int8_t) ((argument2 >> 8) & 0xFF);

		if (windowManager.cursorSurface.Resize(imageWidth, imageHeight)) {
			windowManager.cursorSwap.Resize(imageWidth, imageHeight);
			windowManager.cursorSurface.SetBits((K_USER_BUFFER const void *) argument1, argument3 & 0xFFFFFF, 
					ES_MAKE_RECTANGLE(0, windowManager.cursorSurface.width, 0, windowManager.cursorSurface.height));
		}

		windowManager.changedCursorImage = true;
		changedCursor = true;
	}

	windowManager.mutex.Release();

	SYSCALL_RETURN(changedCursor, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_MOVE) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);

	Window *window = (Window *) _window.object;

	bool success = true;

	EsRectangle rectangle;

	if (argument1) {
		SYSCALL_READ(&rectangle, argument1, sizeof(EsRectangle));
	} else {
		EsMemoryZero(&rectangle, sizeof(EsRectangle));
	}

	windowManager.mutex.Acquire();

	if (argument3 & ES_MOVE_WINDOW_HIDDEN) {
		windowManager.HideWindow(window);
	} else {
		window->Move(rectangle, argument3);
	}

	if (argument3 & ES_MOVE_WINDOW_UPDATE_SCREEN) {
		GraphicsUpdateScreen();
	}

	windowManager.mutex.Release();

	SYSCALL_RETURN(success ? ES_SUCCESS : ES_ERROR_INVALID_DIMENSIONS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_CURSOR_POSITION) {
	EsPoint *point = (EsPoint *) argument0;
	SYSCALL_BUFFER(argument0, sizeof(EsPoint), 1);

	// I'll presume we don't need the mutex acquired to do this..?
	// What's the worst that could happen?
	point->x = windowManager.cursorX;
	point->y = windowManager.cursorY;
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SET_CURSOR_POSITION) {
	windowManager.mutex.Acquire();
	windowManager.cursorX = argument0;
	windowManager.cursorY = argument1;
	windowManager.cursorXPrecise = argument0 * 10;
	windowManager.cursorYPrecise = argument1 * 10;
	windowManager.mutex.Release();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GAME_CONTROLLER_STATE_POLL) {
	EsGameControllerState gameControllers[ES_GAME_CONTROLLER_MAX_COUNT];
	size_t gameControllerCount;

	windowManager.gameControllersMutex.Acquire();
	gameControllerCount = windowManager.gameControllerCount;
	EsMemoryCopy(gameControllers, windowManager.gameControllers, sizeof(EsGameControllerState) * gameControllerCount);
	windowManager.gameControllersMutex.Release();

	SYSCALL_WRITE(argument0, gameControllers, sizeof(EsGameControllerState) * gameControllerCount);
	SYSCALL_RETURN(gameControllerCount, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_GET_BOUNDS) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW | KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);

	EsRectangle rectangle;
	EsMemoryZero(&rectangle, sizeof(EsRectangle));
	windowManager.mutex.Acquire();

	if (_window.type == KERNEL_OBJECT_WINDOW) {
		Window *window = (Window *) _window.object;
		rectangle.l = window->position.x;
		rectangle.t = window->position.y;
		rectangle.r = window->position.x + window->width;
		rectangle.b = window->position.y + window->height;
	} else if (_window.type == KERNEL_OBJECT_EMBEDDED_WINDOW) {
		EmbeddedWindow *embed = (EmbeddedWindow *) _window.object;
		Window *window = embed->container;

		if (window) {
			rectangle.l = window->position.x + WINDOW_INSET;
			rectangle.t = window->position.y + WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT;
			rectangle.r = window->position.x + window->width - WINDOW_INSET;
			rectangle.b = window->position.y + window->height - WINDOW_INSET;
		} else {
			rectangle.l = 0;
			rectangle.t = 0;
			rectangle.r = 16;
			rectangle.b = 16;
		}
	}

	windowManager.mutex.Release();
	SYSCALL_WRITE(argument1, &rectangle, sizeof(EsRectangle));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_PAUSE_PROCESS) {
	KObject _process(currentProcess, argument0, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(_process);
	Process *process = (Process *) _process.object;

	scheduler.PauseProcess(process, (bool) argument1);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CRASH_PROCESS) {
	KernelLog(LOG_ERROR, "Syscall", "process crash request", "Process crash request, reason %d\n", argument0);
	SYSCALL_RETURN(argument0, true);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_POST_MESSAGE) {
	_EsMessageWithObject message;
	SYSCALL_READ(&message.message, argument0, sizeof(EsMessage));
	message.object = (void *) argument1;

	if (currentProcess->messageQueue.SendMessage(message)) {
		SYSCALL_RETURN(ES_SUCCESS, false);
	} else {
		SYSCALL_RETURN(ES_ERROR_MESSAGE_QUEUE_FULL, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_POST_MESSAGE_REMOTE) {
	KObject object(currentProcess, argument1, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(object);
	void *process = object.object;

	_EsMessageWithObject message;
	SYSCALL_READ(&message.message, argument0, sizeof(EsMessage));
	message.object = nullptr;

	if (((Process *) process)->messageQueue.SendMessage(message)) {
		SYSCALL_RETURN(ES_SUCCESS, false);
	} else {
		SYSCALL_RETURN(ES_ERROR_MESSAGE_QUEUE_FULL, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_START_PROGRAM) {
	KObject buffer(currentProcess, argument0, KERNEL_OBJECT_CONSTANT_BUFFER);
	CHECK_OBJECT(buffer);
	ConstantBuffer *object = (ConstantBuffer *) buffer.object;

	if (object->bytes > 1048576) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	OpenHandleToObject(object, KERNEL_OBJECT_CONSTANT_BUFFER);

	_EsMessageWithObject m = {};
	m.message.type = ES_MSG_DESKTOP_START_PROGRAM;
	m.message.createInstance.data = desktopProcess->handleTable.OpenHandle(object, 0, KERNEL_OBJECT_CONSTANT_BUFFER);
	m.message.createInstance.dataBytes = object->bytes;

	if (m.message.createInstance.data != ES_INVALID_HANDLE) {
		if (desktopProcess->messageQueue.SendMessage(m)) {
			SYSCALL_RETURN(ES_SUCCESS, false);
		} else {
			SYSCALL_RETURN(ES_ERROR_MESSAGE_QUEUE_FULL, false);
		}
	} else {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_THREAD_ID) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_THREAD | KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(object);

	if (object.type == KERNEL_OBJECT_THREAD) {
		SYSCALL_RETURN(((Thread *) object.object)->id, false);
	} else if (object.type == KERNEL_OBJECT_PROCESS) {
		Process *process = (Process *) object.object;

#ifdef ENABLE_POSIX_SUBSYSTEM
		if (currentThread->posixData && currentThread->posixData->forkProcess) {
			SYSCALL_RETURN(currentThread->posixData->forkProcess->id, false);
		} else 
#endif
		{
			SYSCALL_RETURN(process->id, false);
		}
	}

	SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN, true);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_ENUMERATE_DIRECTORY_CHILDREN) {
	KObject _node(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(_node);
	KNode *node = (KNode *) _node.object;
	
	if (node->directoryEntry->type != ES_NODE_DIRECTORY) SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);

	if (argument2 > SYSCALL_BUFFER_LIMIT / sizeof(EsDirectoryChild)) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_BUFFER(argument1, argument2 * sizeof(EsDirectoryChild), 1);

	SYSCALL_RETURN(FSDirectoryEnumerateChildren(node, (K_USER_BUFFER EsDirectoryChild *) argument1, argument2), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_DIRECTORY_MONITOR) {
	SYSCALL_RETURN(ES_ERROR_UNKNOWN, false);

#if 0
	KObject _node(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(_node);
	Node *node = (Node *) _node.object;
	
	if (node->data.type != ES_NODE_DIRECTORY) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);
	}

	DirectoryMonitor *monitor = (DirectoryMonitor *) EsHeapAllocate(sizeof(DirectoryMonitor), true, K_FIXED);

	if (!monitor) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	if (!OpenHandleToObject(node, KERNEL_OBJECT_NODE) /* we don't need any access flags */) { 
		EsHeapFree(monitor, sizeof(DirectoryMonitor), K_FIXED);
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	if (!OpenHandleToObject(currentProcess, KERNEL_OBJECT_PROCESS)) {
		KernelPanic("DoES_SYSCALL_DIRECTORY_MONITOR - Could not open handle to process.\n");
	}

	monitor->handles = 1;
	monitor->process = currentProcess;
	monitor->directory = node;

	monitor->flags = argument1;
	monitor->context.u = argument2;

	node->directory.monitorLock.Take(K_LOCK_EXCLUSIVE);
	if (!node->directory.monitors) ArrayInitialise(node->directory.monitors, K_FIXED);
	ArrayAdd(node->directory.monitors, monitor);
	node->directory.monitorLock.Return(K_LOCK_EXCLUSIVE);

	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(monitor, 0, KERNEL_OBJECT_DIRECTORY_MONITOR), false);
#endif
}

SYSCALL_IMPLEMENT(ES_SYSCALL_FILE_CONTROL) {
	KObject object(currentProcess, argument0, KERNEL_OBJECT_NODE);
	CHECK_OBJECT(object);
	KNode *file = (KNode *) object.object;

	if (file->directoryEntry->type != ES_NODE_FILE) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INCORRECT_NODE_TYPE, true);
	}

	SYSCALL_RETURN(FSFileControl(file, argument1), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_BATCH) {
	EsBatchCall *calls;
	if (argument1 > SYSCALL_BUFFER_LIMIT / sizeof(EsBatchCall)) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ_HEAP(calls, argument0, sizeof(EsBatchCall) * argument1);

	size_t count = argument1;

	for (uintptr_t i = 0; i < count; i++) {
		EsBatchCall call = calls[i];
		bool fatal = false;
		uintptr_t _returnValue = calls[i].returnValue = DoSyscall(call.index, call.argument0, call.argument1, call.argument2, call.argument3, 
				DO_SYSCALL_BATCHED, &fatal, userStackPointer);
		if (fatal) SYSCALL_RETURN(_returnValue, true);
		if (calls->stopBatchIfError && ES_CHECK_ERROR(_returnValue)) break;
		if (currentThread->terminating) break;
	}

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_READ_CONSTANT_BUFFER) {
	KObject _buffer(currentProcess, argument0, KERNEL_OBJECT_CONSTANT_BUFFER);
	CHECK_OBJECT(_buffer);
	ConstantBuffer *buffer = (ConstantBuffer *) _buffer.object;
	SYSCALL_WRITE(argument1, buffer + 1, buffer->bytes);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_PROCESS_STATE) {
	KObject _process(currentProcess, argument0, KERNEL_OBJECT_PROCESS);
	CHECK_OBJECT(_process);
	Process *process = (Process *) _process.object;

	EsProcessState state;
	EsMemoryZero(&state, sizeof(EsProcessState));
	state.crashReason = process->crashReason;
	state.creationArgument = process->creationArgument;
	state.id = process->id;
	state.executableState = process->executableState;
	state.flags = (process->allThreadsTerminated ? ES_PROCESS_STATE_ALL_THREADS_TERMINATED : 0)
		| (process->terminating ? ES_PROCESS_STATE_TERMINATING : 0)
		| (process->crashed ? ES_PROCESS_STATE_CRASHED : 0)
		| (process->messageQueue.pinged ? ES_PROCESS_STATE_PINGED : 0);

	SYSCALL_WRITE(argument1, &state, sizeof(EsProcessState));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SHUTDOWN) {
	KernelShutdown();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_GET_ID) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW | KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);

	if (_window.type == KERNEL_OBJECT_WINDOW) {
		SYSCALL_RETURN(((Window *) _window.object)->id, false);
	} else {
		SYSCALL_RETURN(((EmbeddedWindow *) _window.object)->id, false);
	}

}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_FOCUSED) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_WINDOW);
	CHECK_OBJECT(_window);
	Window *window = (Window *) _window.object;

	windowManager.mutex.Acquire();
	windowManager.ActivateWindow(window);
	windowManager.MoveCursor(0, 0);
	windowManager.mutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_YIELD_SCHEDULER) {
	ProcessorFakeTimerInterrupt();
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_SYSTEM_CONSTANTS) {
	SYSCALL_BUFFER(argument0, sizeof(uint64_t) * 256, 1);
	uint64_t *systemConstants = (uint64_t *) argument0;
	EsMemoryCopy(systemConstants, globalSystemConstants, sizeof(uint64_t) * 256);
	systemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND] = timeStampTicksPerMs / 1000;
	systemConstants[ES_SYSTEM_CONSTANT_REPORTED_PROBLEMS] = kernelReportedProblems;
	systemConstants[ES_SYSTEM_CONSTANT_WINDOW_INSET] = WINDOW_INSET;
	systemConstants[ES_SYSTEM_CONSTANT_CONTAINER_TAB_BAND_HEIGHT] = CONTAINER_TAB_BAND_HEIGHT;
	systemConstants[ES_SYSTEM_CONSTANT_NO_FANCY_GRAPHICS] = graphics.target->reducedColors;
	systemConstants[ES_SYSTEM_CONSTANT_UI_SCALE] = UI_SCALE;
	systemConstants[ES_SYSTEM_CONSTANT_BORDER_THICKNESS] = BORDER_THICKNESS;
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SET_SYSTEM_CONSTANT) {
	if (argument0 < 256) {
		globalSystemConstants[argument0] = argument1;
		
		EsMessage m = {};
		m.type = ES_MSG_SYSTEM_CONSTANT_UPDATED;
		m.systemConstantUpdated.index = argument0;
		m.systemConstantUpdated.newValue = argument1;
		BroadcastMessage(m);
	}

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_TAKE_SYSTEM_SNAPSHOT) {
	SYSCALL_PERMISSION(ES_PERMISSION_TAKE_SYSTEM_SNAPSHOT);

	int type = argument0;
	void *buffer = nullptr;
	size_t bufferSize = 0;
	EsDefer(EsHeapFree(buffer, 0, K_FIXED));

	switch (type) {
		case ES_SYSTEM_SNAPSHOT_PROCESSES: {
			scheduler.lock.Acquire();
			size_t count = scheduler.allProcesses.count;
			bufferSize = sizeof(EsSnapshotProcesses) + sizeof(EsSnapshotProcessesItem) * count;
			scheduler.lock.Release();
			
			buffer = EsHeapAllocate(bufferSize, false, K_FIXED);
			EsMemoryZero(buffer, bufferSize);

			scheduler.lock.Acquire();

			if (scheduler.allProcesses.count < count) {
				count = scheduler.allProcesses.count;
			}

			EsSnapshotProcesses *snapshot = (EsSnapshotProcesses *) buffer;

			LinkedItem<Process> *item = scheduler.allProcesses.firstItem;
			uintptr_t index = 0;

			while (item && index < count) {
				Process *process = item->thisItem;
				if (process->terminating) goto next;

				{
					snapshot->processes[index].pid = process->id;
					snapshot->processes[index].memoryUsage = process->vmm->commit * K_PAGE_SIZE; 
					snapshot->processes[index].cpuTimeSlices = process->timeSlices;

					char *executableName = process->executablePath + process->executablePathLength;
					if (*process->executablePath != '/') executableName = process->executablePath;
					else while (executableName[-1] != '/') executableName--;
					snapshot->processes[index].nameLength = process->executablePathLength + process->executablePath - executableName;
					EsMemoryCopy(snapshot->processes[index].name, executableName, snapshot->processes[index].nameLength);

					index++;
				}

				next:;
				item = item->nextItem;
			}

			snapshot->count = count;
			scheduler.lock.Release();
		} break;

#if 0
		case ES_SYSTEM_SNAPSHOT_DRIVES: {
			vfs.filesystemsMutex.Acquire();
			vfs.mountpointsMutex.Acquire();

			size_t filesystemCount = vfs.filesystems.count;
			buffer = EsHeapAllocate(bufferSize = filesystemCount * sizeof(EsDriveInformation), true, K_FIXED);
			EsDriveInformation *drives = (EsDriveInformation *) buffer;

			LinkedItem<Filesystem> *item = vfs.filesystems.firstItem;

			for (uintptr_t i = 0; i < filesystemCount; i++) {
				Filesystem *fileSystem = item->thisItem;
				EsDriveInformation *drive = drives + i;
				drive->nameBytes = EsStringFormat(drive->name, 64, "%s", fileSystem->nameBytes, fileSystem->name);
				item = item->nextItem;

				{
					LinkedItem<Mountpoint> *item = fileSystem->mountpoints.firstItem;

					while (item) {
						Mountpoint *mountpoint = item->thisItem;
						drive->mountpointBytes = EsStringFormat(drive->mountpoint, 256, "%s", mountpoint->pathLength, mountpoint->path);
						item = item->nextItem;
					}
				}
			}

			vfs.mountpointsMutex.Release();
			vfs.filesystemsMutex.Release();
		} break;
#endif

		default: {
			SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN_SNAPSHOT_TYPE, true);
		} break;
	}

	EsHandle constantBuffer = MakeConstantBuffer(buffer, bufferSize, currentProcess);
	SYSCALL_WRITE(argument1, &bufferSize, sizeof(size_t));
	SYSCALL_RETURN(constantBuffer, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_OPEN_PROCESS) {
	SYSCALL_PERMISSION(ES_PERMISSION_PROCESS_OPEN);

	Process *process = scheduler.OpenProcess(argument0);

	if (process) {
		SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(process, 0, KERNEL_OBJECT_PROCESS), false);
	} else {
		SYSCALL_RETURN(ES_INVALID_HANDLE, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SET_TLS) {
	currentThread->tlsAddress = argument0; // Set this first, otherwise we could get pre-empted and restore without TLS set.
	ProcessorSetThreadStorage(argument0);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_TLS) {
	SYSCALL_RETURN(currentThread->tlsAddress, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_SYSTEM_INFORMATION) {
	EsSystemInformation information;
	EsMemoryZero(&information, sizeof(EsSystemInformation));
	information.commitLimit = pmm.commitLimit * K_PAGE_SIZE;
	information.commit = (pmm.commitFixed + pmm.commitPageable) * K_PAGE_SIZE;
	information.countZeroedPages = pmm.countZeroedPages;
	information.countFreePages = pmm.countFreePages;
	information.countStandbyPages = pmm.countStandbyPages;
	information.countActivePages = pmm.countActivePages;
	information.processCount = scheduler.allProcesses.count;
	information.threadCount = scheduler.allThreads.count;
	information.handleCount = totalHandleCount;
	information.coreHeapSize = heapCore.size;
	information.coreHeapAllocations = heapCore.allocationsCount;
	information.fixedHeapSize = heapFixed.size;
	information.fixedHeapAllocations = heapFixed.allocationsCount;
	information.coreRegions = mmCoreRegionCount;
	information.kernelRegions = kernelMMSpace->usedRegionsNonGuard.count;
	SYSCALL_WRITE(argument0, &information, sizeof(EsSystemInformation));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_SCREEN_BOUNDS) {
	EsRectangle rectangle;
	EsMemoryZero(&rectangle, sizeof(EsRectangle));

	rectangle.l = 0;
	rectangle.t = 0;
	rectangle.r = graphics.width;
	rectangle.b = graphics.height;

	SYSCALL_WRITE(argument1, &rectangle, sizeof(EsRectangle));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SET_SCREEN_WORK_AREA) {
	EsRectangle rectangle;
	SYSCALL_READ(&rectangle, argument1, sizeof(EsRectangle));
	windowManager.workArea = rectangle;
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_SCREEN_WORK_AREA) {
	EsRectangle rectangle = windowManager.workArea;
	SYSCALL_WRITE(argument1, &rectangle, sizeof(EsRectangle));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_METADATA) {
	char *buffer;
	if (argument1 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_READ_HEAP(buffer, argument0, argument1);

	KObject _window(currentProcess, argument2, KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);

	EmbeddedWindow *window = (EmbeddedWindow *) _window.object;

	{
		_EsMessageWithObject m = {};
		m.message.type = ES_MSG_SET_WINDOW_METADATA;
		m.message.windowMetadata.buffer = MakeConstantBufferForDesktop(buffer, argument1);
		m.message.windowMetadata.bytes = argument1;
		m.message.windowMetadata.id = window->id;
		desktopProcess->messageQueue.SendMessage(m);
	}

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_RESIZE_CLEAR_COLOR) {
	KObject _window(currentProcess, argument0, KERNEL_OBJECT_EMBEDDED_WINDOW);
	CHECK_OBJECT(_window);
	EmbeddedWindow *window = (EmbeddedWindow *) _window.object;
	window->resizeClearColor = argument1;
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MAILSLOT_SHARE) {
	Mailslot *mailslot;
	Process *target;

	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_MAILSLOT, mailslot, 1);
	SYSCALL_HANDLE(argument1, KERNEL_OBJECT_PROCESS, target, 2);

	OpenHandleToObject(mailslot, KERNEL_OBJECT_MAILSLOT);

	SYSCALL_RETURN(target->handleTable.OpenHandle(mailslot, 0, KERNEL_OBJECT_MAILSLOT), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MAILSLOT_SEND_DATA) {
	Mailslot *mailslot;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_MAILSLOT, mailslot, 1);
	if (argument2 > SYSCALL_BUFFER_LIMIT) SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	SYSCALL_BUFFER_ALLOW_NULL(argument1, argument2, 2);
	bool success = mailslot->SendData((void *) argument1, argument2);
	SYSCALL_RETURN(success, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_MAILSLOT_SEND_MESSAGE) {
	Mailslot *mailslot;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_MAILSLOT, mailslot, 1);
	EsMessage message;
	SYSCALL_READ(&message, argument1, sizeof(EsMessage));
	bool success = mailslot->SendMessage(&message);
	SYSCALL_RETURN(success, false);
}

#ifdef ENABLE_POSIX_SUBSYSTEM
SYSCALL_IMPLEMENT(ES_SYSCALL_POSIX) {
	_EsPOSIXSyscall syscall;
	SYSCALL_READ(&syscall, argument0, sizeof(_EsPOSIXSyscall));
	long result = POSIX::DoSyscall(syscall, userStackPointer);
	// if (result < 0) EsPrint("-error%d\n", result);
	SYSCALL_RETURN(result, false);
}
#else
SYSCALL_IMPLEMENT(ES_SYSCALL_POSIX) {
	SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN_SYSCALL, true);
}
#endif

SYSCALL_IMPLEMENT(ES_SYSCALL_GET_PROCESS_STATUS) {
	Process *process;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_PROCESS, process, 1);
	SYSCALL_RETURN(process->exitStatus, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_WINDOW_SET_ALPHA) {
	Window *window;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_WINDOW, window, 1);
	window->alpha = argument1 & 0xFF;
	SYSCALL_RETURN(0, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_TIMER_CREATE) {
	KTimer *timer = (KTimer *) EsHeapAllocate(sizeof(KTimer), true, K_FIXED);
	if (!timer) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	timer->handles = 1;
	timer->owner = currentProcess;
	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(timer, 0, KERNEL_OBJECT_TIMER), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_TIMER_SET) {
	KTimer *timer;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_TIMER, timer, 1);
	timer->data1 = (void *) argument2;
	timer->Set(argument1, true, nullptr, (void *) argument3);
	SYSCALL_RETURN(0, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_USER_GET_HOME_FOLDER) {
	userMutex.Acquire();
	EsDefer(userMutex.Release());

	User *user = currentProcess->user;

	if (argument1 < user->homeBytes) {
		SYSCALL_RETURN(ES_ERROR_BUFFER_TOO_SMALL, false);
	}

	SYSCALL_WRITE(argument0, user->home, user->homeBytes);
	SYSCALL_RETURN(user->homeBytes, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_USER_LOGIN) {
	if (desktopProcess != currentProcess) {
		// Only the desktop process can login.
		SYSCALL_RETURN(ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS, true);
	}

	_EsUserLoginArguments arguments;
	SYSCALL_READ(&arguments, argument0, sizeof(_EsUserLoginArguments));

	if (arguments.homeBytes > USER_STRING_MAX_LENGTH || arguments.nameBytes > USER_STRING_MAX_LENGTH) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	}

	User *user = (User *) EsHeapAllocate(sizeof(User), true, K_FIXED);
	if (!user) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	SYSCALL_READ(user->name, arguments.name, arguments.nameBytes);
	SYSCALL_READ(user->home, arguments.home, arguments.homeBytes);
	user->nameBytes = arguments.nameBytes;
	user->homeBytes = arguments.homeBytes;

	userMutex.Acquire();

	if (currentProcess->user) {
		userMutex.Release();
		SYSCALL_RETURN(ES_FATAL_ERROR_UNKNOWN, true);
	}

	currentProcess->user = user;

	userMutex.Release();

	SYSCALL_RETURN(0, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_PIPE_CREATE) {
	Pipe *pipe = (Pipe *) EsHeapAllocate(sizeof(Pipe), true, K_PAGED);
	if (!pipe) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	pipe->writers = pipe->readers = 1;
	pipe->canWrite.Set();
	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(pipe, PIPE_READER | PIPE_WRITER, KERNEL_OBJECT_PIPE), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_PIPE_READ) {
	Pipe *pipe;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_PIPE, pipe, 1);
	SYSCALL_BUFFER(argument1, argument2, 2);
	SYSCALL_RETURN(pipe->Access((void *) argument1, argument2, false, true), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_PIPE_WRITE) {
	Pipe *pipe;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_PIPE, pipe, 1);
	SYSCALL_BUFFER(argument1, argument2, 2);
	SYSCALL_RETURN(pipe->Access((void *) argument1, argument2, true, true), false);
}

KMutex systemConfigurationMutex;
ConstantBuffer *systemConfiguration;

SYSCALL_IMPLEMENT(ES_SYSCALL_SYSTEM_CONFIGURATION_WRITE) {
	// TODO Broadcast message?

	if (desktopProcess != currentProcess) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INSUFFICIENT_PERMISSIONS, true);
	}

	if (argument1 > SYSCALL_BUFFER_LIMIT) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	}

	ConstantBuffer *buffer = (ConstantBuffer *) EsHeapAllocate(sizeof(ConstantBuffer) + argument1, false, K_PAGED);
	if (!buffer) SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	EsMemoryZero(buffer, sizeof(ConstantBuffer));
	buffer->handles = 1;
	buffer->bytes = argument1;
	buffer->isPaged = true;
	EsDefer(CloseHandleToObject(buffer, KERNEL_OBJECT_CONSTANT_BUFFER));
	SYSCALL_READ(buffer + 1, argument0, argument1);

	systemConfigurationMutex.Acquire();
	if (systemConfiguration) CloseHandleToObject(systemConfiguration, KERNEL_OBJECT_CONSTANT_BUFFER);
	OpenHandleToObject(buffer, KERNEL_OBJECT_CONSTANT_BUFFER);
	systemConfiguration = buffer;
	systemConfigurationMutex.Release();

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_SYSTEM_CONFIGURATION_READ) {
	EsHandle handle = ES_INVALID_HANDLE;
	size_t bytes = 0;

	systemConfigurationMutex.Acquire();

	if (systemConfiguration && OpenHandleToObject(systemConfiguration, KERNEL_OBJECT_CONSTANT_BUFFER)) {
		bytes = systemConfiguration->bytes;
		handle = currentProcess->handleTable.OpenHandle(systemConfiguration, 0, KERNEL_OBJECT_CONSTANT_BUFFER);
	}

	systemConfigurationMutex.Release();

	SYSCALL_WRITE(argument2, &bytes, sizeof(bytes));
	SYSCALL_RETURN(handle, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_AUDIO_STREAM_OPEN) {
	KAudioStream *audioStream = KAudioStreamOpen(argument0, argument1);

	if (!audioStream) {
		SYSCALL_RETURN(0, false);
	}

	audioStream->mirrorSharedRegion = MMSharedCreateRegion(sizeof(EsAudioStream), true);

	if (!audioStream->mirrorSharedRegion) {
		CloseHandleToObject(audioStream, KERNEL_OBJECT_AUDIO_STREAM);
		SYSCALL_RETURN(0, false);
	}

	audioStream->mirror = (EsAudioStream *) MMMapShared(MMGetKernelSpace(), audioStream->mirrorSharedRegion, 0, sizeof(EsAudioStream));

	if (!audioStream->mirror) {
		CloseHandleToObject(audioStream, KERNEL_OBJECT_AUDIO_STREAM);
		SYSCALL_RETURN(0, false);
	}

	audioStream->mirror->format = audioStream->format;
	audioStream->mirror->bufferBytes = audioStream->bufferBytes;
	audioStream->mirror->status = audioStream->status;

	audioStream->mirror->handle = currentProcess->handleTable.OpenHandle(audioStream, 0, KERNEL_OBJECT_AUDIO_STREAM); 

	if (audioStream->mirror->handle == ES_INVALID_HANDLE) {
		SYSCALL_RETURN(0, false);
	}

	audioStream->mirror->buffer = (uint8_t *) MMMapShared(currentVMM, audioStream->bufferSharedRegion, 0, audioStream->bufferBytes);

	if (!audioStream->mirror->buffer) {
		currentProcess->handleTable.CloseHandle(audioStream->mirror->handle);
		SYSCALL_RETURN(0, false);
	}

	// Map the mirror region last so the user can't change the handles opened above until they are no longer needed for error handling.
	uintptr_t address = (uintptr_t) MMMapShared(currentVMM, audioStream->mirrorSharedRegion, 0, sizeof(EsAudioStream));

	if (!address) {
		currentProcess->handleTable.CloseHandle(audioStream->mirror->handle);
		MMFree(currentVMM, audioStream->mirror->buffer);
		SYSCALL_RETURN(0, false);
	}

	SYSCALL_RETURN(address, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_AUDIO_STREAM_NOTIFY) {
	KAudioStream *stream;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_AUDIO_STREAM, stream, 1);
	stream->mixer->streamsReady.Set(false, true);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_EVENT_SINK_CREATE) {
	EventSink *sink = (EventSink *) EsHeapAllocate(sizeof(EventSink), true, K_FIXED);
	
	if (!sink) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	sink->ignoreDuplicates = argument0;
	sink->handles = 1;

	SYSCALL_RETURN(currentProcess->handleTable.OpenHandle(sink, 0, KERNEL_OBJECT_EVENT_SINK), false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_EVENT_FORWARD) {
	KEvent *event;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_EVENT, event, 1);
	EventSink *sink;
	SYSCALL_HANDLE(argument1, KERNEL_OBJECT_EVENT_SINK, sink, 2);
	EsGeneric data = argument2;

	bool error = false, limitExceeded = false;

	eventForwardMutex.Acquire();

	if (!event->sinkTable) {
		event->sinkTable = (EventSinkTable *) EsHeapAllocate(sizeof(EventSinkTable) * ES_MAX_EVENT_FORWARD_COUNT, true, K_FIXED);

		if (!event->sinkTable) {
			error = true;
		}
	}

	if (!error) {
		limitExceeded = true;

		for (uintptr_t i = 0; i < ES_MAX_EVENT_FORWARD_COUNT; i++) {
			if (!event->sinkTable[i].sink) {
				if (!OpenHandleToObject(sink, KERNEL_OBJECT_EVENT_SINK, 0, false)) {
					error = true;
					break;
				}

				scheduler.lock.Acquire();
				event->sinkTable[i].sink = sink;
				event->sinkTable[i].data = data;
				scheduler.lock.Release();

				limitExceeded = false;
				break;
			}
		}
	}

	eventForwardMutex.Release();

	if (limitExceeded) {
		SYSCALL_RETURN(ES_FATAL_ERROR_TOO_MANY_WAIT_OBJECTS, true);
	} else if (error) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	} else {
	       	SYSCALL_RETURN(0, false);
	}
}

SYSCALL_IMPLEMENT(ES_SYSCALL_EVENT_SINK_POP) {
	EventSink *sink;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_EVENT_SINK, sink, 1);

	bool empty = false, overflow = false;
	EsGeneric data = {};

	sink->spinlock.Acquire();

	if (!sink->queueCount) {
		if (sink->overflow) {
			overflow = true;
			sink->overflow = false;
		} else {
			empty = true;
		}
	} else {
		data = sink->queue[sink->queuePosition];
		sink->queuePosition++;
		sink->queueCount--;

		if (sink->queuePosition == ES_MAX_EVENT_SINK_BUFFER_SIZE) {
			sink->queuePosition = 0;
		}
	}

	if (!sink->queueCount && !sink->overflow) {
		sink->available.Reset(); // KEvent::Reset doesn't take the scheduler lock, so this won't deadlock!
	}

	sink->spinlock.Release();

	SYSCALL_WRITE(argument1, &data, sizeof(EsGeneric));
	SYSCALL_RETURN(overflow ? ES_ERROR_EVENT_SINK_OVERFLOW : empty ? ES_ERROR_EVENT_NOT_SET : ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_EVENT_SINK_PUSH) {
	EventSink *sink;
	SYSCALL_HANDLE(argument0, KERNEL_OBJECT_EVENT_SINK, sink, 1);
	scheduler.lock.Acquire();
	EsError result = sink->Push(argument1);
	scheduler.lock.Release();
	SYSCALL_RETURN(result, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_DOMAIN_NAME_RESOLVE) {
	if (argument1 > ES_DOMAIN_NAME_MAX_LENGTH) {
		SYSCALL_RETURN(ES_ERROR_BAD_DOMAIN_NAME, false);
	}

	char domainName[ES_DOMAIN_NAME_MAX_LENGTH];
	SYSCALL_READ(domainName, argument0, argument1);

	EsAddress address;
	EsMemoryZero(&address, sizeof(EsAddress));

	KEvent completeEvent = {};

	NetDomainNameResolveTask task = {};
	task.event = &completeEvent;
	task.name = domainName;
	task.nameBytes = (size_t) argument1;
	task.address = &address;
	task.callback = NetDomainNameResolve;
	NetTaskBegin(&task);

	completeEvent.Wait();

	if (task.error == ES_SUCCESS) {
		SYSCALL_WRITE(argument2, &address, sizeof(EsAddress));
	}

	SYSCALL_RETURN(task.error, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_ECHO_REQUEST) {
	if (argument1 > ES_ECHO_REQUEST_MAX_LENGTH) {
		SYSCALL_RETURN(ES_FATAL_ERROR_INVALID_BUFFER, true);
	}

	uint8_t data[48];
	EsMemoryZero(data, sizeof(data));
	SYSCALL_READ(data, argument0, argument1);

	EsAddress address;
	SYSCALL_READ(&address, argument2, sizeof(EsAddress));

	KEvent completeEvent = {};

	NetEchoRequestTask task = {};
	task.event = &completeEvent;
	task.address = &address;
	task.data = data;
	task.callback = NetEchoRequest;
	NetTaskBegin(&task);

	completeEvent.Wait();

	if (task.error == ES_SUCCESS) {
		SYSCALL_WRITE(argument0, data, argument1);
	}

	SYSCALL_RETURN(task.error, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CONNECTION_OPEN) {
	EsConnection connection;
	SYSCALL_READ(&connection, argument0, sizeof(EsConnection));

	if (connection.sendBufferBytes < 1024 || connection.receiveBufferBytes < 1024) {
		SYSCALL_RETURN(ES_ERROR_BUFFER_TOO_SMALL, false);
	}

	// TODO Upper limit on buffer sizes?

	NetConnection *netConnection = NetConnectionOpen(&connection.address, connection.sendBufferBytes, connection.receiveBufferBytes, argument1);

	if (!netConnection) {
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	connection.sendBuffer = (uint8_t *) MMMapShared(currentVMM, netConnection->bufferRegion, 0, connection.sendBufferBytes + connection.receiveBufferBytes);
	connection.receiveBuffer = connection.sendBuffer + connection.sendBufferBytes;

	if (!connection.sendBuffer) {
		CloseHandleToObject(netConnection, KERNEL_OBJECT_CONNECTION);
		SYSCALL_RETURN(ES_ERROR_INSUFFICIENT_RESOURCES, false);
	}

	connection.handle = currentProcess->handleTable.OpenHandle(netConnection, 0, KERNEL_OBJECT_CONNECTION); 
	connection.error = ES_SUCCESS;

	SYSCALL_WRITE(argument0, &connection, sizeof(EsConnection));
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CONNECTION_POLL) {
	SYSCALL_BUFFER(argument0, sizeof(EsConnection), 0);
	EsConnection *connection = (EsConnection *) argument0;
	NetConnection *netConnection;
	SYSCALL_HANDLE(argument3, KERNEL_OBJECT_CONNECTION, netConnection, 1);

	connection->receiveWritePointer = netConnection->receiveWritePointer;
	connection->sendReadPointer = netConnection->sendReadPointer;
	connection->open = netConnection->task.step == TCP_STEP_ESTABLISHED;
	connection->error = netConnection->task.completed ? netConnection->task.error : ES_SUCCESS;

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_CONNECTION_NOTIFY) {
	NetConnection *netConnection;
	SYSCALL_HANDLE(argument3, KERNEL_OBJECT_CONNECTION, netConnection, 1);
	NetConnectionNotify(netConnection, argument1, argument2);
	SYSCALL_RETURN(ES_SUCCESS, false);
}

SYSCALL_IMPLEMENT(ES_SYSCALL_DEBUG_COMMAND) {
#ifdef DEBUG_BUILD
	if (argument0 == 1) {
		ArchResetCPU();
	} else if (argument0 == 2) {
		KernelPanic("Debug command 2.\n");
	} else if (argument0 == 3) {
		extern char kernelLog[];
		extern uintptr_t kernelLogPosition;
		size_t bytes = kernelLogPosition;
		if (argument2 < bytes) bytes = argument2;
		EsMemoryCopy((void *) argument1, kernelLog, bytes);
		SYSCALL_RETURN(bytes, false);
	} else if (argument0 == 4) {
		SYSCALL_BUFFER(argument1, 1, 0);

		if (_region0->data.normal.commitPageCount != (argument3 & 0x7FFFFFFFFFFFFFFF)) {
			KernelPanic("Commit page count mismatch.\n");
		}

		if (_region0->data.normal.commit.Contains(argument2) != (argument3 >> 63)) {
			KernelPanic("Commit contains mismatch at %x.\n", argument1);
		}
	} else if (argument0 == 5) {
		size_t bytes = sizeof(scheduler.log);
		if (argument2 < bytes) bytes = argument2;
		EsMemoryCopy((void *) argument1, scheduler.log, bytes);
		SYSCALL_RETURN(bytes, false);
	} else if (argument0 == 6) {
		SYSCALL_RETURN(DriversDebugGetEnumeratedPCIDevices((EsPCIDevice *) argument1, argument2), false);
	}
#endif

	SYSCALL_RETURN(ES_SUCCESS, false);
}

SyscallFunction syscallFunctions[ES_SYSCALL_COUNT + 1] {
#include <bin/syscall_array.h>
};

#pragma GCC diagnostic pop

uintptr_t DoSyscall(EsSyscallType index,
		uintptr_t argument0, uintptr_t argument1,
		uintptr_t argument2, uintptr_t argument3,
		uint64_t flags, bool *fatal, uintptr_t *userStackPointer) {
	bool batched = flags & DO_SYSCALL_BATCHED;

	// Interrupts need to be enabled during system calls,
	// because many of them block on mutexes or events.
	ProcessorEnableInterrupts();

	Thread *currentThread = GetCurrentThread();
	Process *currentProcess = currentThread->process;
	MMSpace *currentVMM = currentProcess->vmm;

	if (!batched) {
		if (currentThread->terminating) {
			// The thread has been terminated.
			// Yield the scheduler so it can be removed.
			ProcessorFakeTimerInterrupt();
		}

		if (currentThread->terminatableState != THREAD_TERMINATABLE) {
			KernelPanic("DoSyscall - Current thread %x was not terminatable (was %d).\n", 
					currentThread, currentThread->terminatableState);
		}

		currentThread->terminatableState = THREAD_IN_SYSCALL;
	}

	EsError returnValue = ES_FATAL_ERROR_UNKNOWN_SYSCALL;
	bool fatalError = true;

	if (index < ES_SYSCALL_COUNT) {
		SyscallFunction function = syscallFunctions[index];

		if (batched && index == ES_SYSCALL_BATCH) {
			// This could cause a stack overflow, so it's a fatal error.
			SYSCALL_RETURN(ES_FATAL_ERROR_RECURSIVE_BATCH, true);
		}

		if (function) {
			returnValue = (EsError) function(argument0, argument1, argument2, argument3, 
					currentThread, currentProcess, currentVMM, userStackPointer, fatalError);
		}
	}

	if (fatal) *fatal = false;

	if (fatalError) {
		if (fatal) {
			*fatal = true;
		} else {
			EsCrashReason reason;
			reason.errorCode = returnValue;
			KernelLog(LOG_ERROR, "Syscall", "syscall failure", 
					"Process crashed during system call [%x, %x, %x, %x, %x]\n", index, argument0, argument1, argument2, argument3);
			scheduler.CrashProcess(currentProcess, reason);
		}
	}

	if (!batched) {
		currentThread->terminatableState = THREAD_TERMINATABLE;

		if (currentThread->terminating || currentThread->paused) {
			// The thread has been terminated or paused.
			// Yield the scheduler so it can be removed or sent to the paused thread queue.
			ProcessorFakeTimerInterrupt();
		}
	}
	
	return returnValue;
}

bool KCopyToUser(K_USER_BUFFER void *destination, const void *source, size_t length) {
	__sync_synchronize();
	Thread *currentThread = GetCurrentThread();
	MMRegion *region = MMFindAndPinRegion(currentThread->process->vmm, (uintptr_t) destination, length); 
	if (!region) return false;
	EsMemoryCopy(destination, source, length);
	MMUnpinRegion(currentThread->process->vmm, region);
	return true;
}

bool KCopyFromUser(void *destination, K_USER_BUFFER const void *source, size_t length) {
	__sync_synchronize();
	Thread *currentThread = GetCurrentThread();
	MMRegion *region = MMFindAndPinRegion(currentThread->process->vmm, (uintptr_t) source, length); 
	if (!region) return false;
	EsMemoryCopy(destination, source, length);
	MMUnpinRegion(currentThread->process->vmm, region);
	return true;
}

#endif
