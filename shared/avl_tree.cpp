#ifndef IMPLEMENTATION

#define TREE_VALIDATE

#ifdef KERNEL
#define AVLPanic KernelPanic
#else
#define AVLPanic(...) do { EsPrint(__VA_ARGS__); EsAssert(false); } while (0)
#endif

enum TreeSearchMode {
	TREE_SEARCH_EXACT,
	TREE_SEARCH_SMALLEST_ABOVE_OR_EQUAL,
	TREE_SEARCH_LARGEST_BELOW_OR_EQUAL,
};

template <class T>
struct AVLTree;

struct AVLKey {
	union {
		uintptr_t shortKey;

		struct {
			void *longKey;
			size_t longKeyBytes;
		};
	};
};

inline AVLKey MakeShortKey(uintptr_t shortKey) {
	AVLKey key = {};
	key.shortKey = shortKey;
	return key;
}

inline AVLKey MakeLongKey(const void *longKey, size_t longKeyBytes) {
	AVLKey key = {};
	key.longKey = (void *) longKey;
	key.longKeyBytes = longKeyBytes;
	return key;
}

inline AVLKey MakeCStringKey(const char *cString) {
	return MakeLongKey(cString, EsCStringLength(cString) + 1);
}

template <class T>
struct AVLItem {
	T *thisItem;
	AVLItem<T> *children[2], *parent;
#ifdef TREE_VALIDATE
	AVLTree<T> *tree;
#endif
	AVLKey key;
	int height;
};

template <class T>
struct AVLTree {
	AVLItem<T> *root;
	bool modCheck;
	bool longKeys;
};

template <class T>
void TreeRelink(AVLItem<T> *item, AVLItem<T> *newLocation) {
	item->parent->children[item->parent->children[1] == item] = newLocation;
	if (item->children[0]) item->children[0]->parent = newLocation;
	if (item->children[1]) item->children[1]->parent = newLocation;
}

template <class T>
void TreeSwapItems(AVLItem<T> *a, AVLItem<T> *b) {
	// Set the parent of each item to point to the opposite one.
	a->parent->children[a->parent->children[1] == a] = b;
	b->parent->children[b->parent->children[1] == b] = a;

	// Swap the data between items.
	AVLItem<T> ta = *a, tb = *b;
	a->parent = tb.parent;
	b->parent = ta.parent;
	a->height = tb.height;
	b->height = ta.height;
	a->children[0] = tb.children[0];
	a->children[1] = tb.children[1];
	b->children[0] = ta.children[0];
	b->children[1] = ta.children[1];

	// Make all the children point to the correct item.
	if (a->children[0]) a->children[0]->parent = a; 
	if (a->children[1]) a->children[1]->parent = a; 
	if (b->children[0]) b->children[0]->parent = b;
	if (b->children[1]) b->children[1]->parent = b;
}

template <class T>
inline int TreeCompare(AVLTree<T> *tree, AVLKey &key1, AVLKey &key2) {
	if (tree->longKeys) {
		if (!key1.longKey && !key2.longKey) return 0;
		if (!key2.longKey) return  1;
		if (!key1.longKey) return -1;
		return EsStringCompareRaw((const char *) key1.longKey, key1.longKeyBytes, (const char *) key2.longKey, key2.longKeyBytes);
	} else {
		if (key1.shortKey < key2.shortKey) return -1;
		if (key1.shortKey > key2.shortKey) return  1;
		return 0;
	}
}

template <class T>
int TreeValidate(AVLItem<T> *root, bool before, AVLTree<T> *tree, AVLItem<T> *parent = nullptr, int depth = 0) {
#ifdef TREE_VALIDATE
	if (!root) return 0;
	if (root->parent != parent) AVLPanic("TreeValidate - Invalid binary tree 1 (%d).\n", before);
	if (root->tree != tree) AVLPanic("TreeValidate - Invalid binary tree 4 (%d).\n", before);

	AVLItem<T> *left  = root->children[0];
	AVLItem<T> *right = root->children[1];

	if (left  && TreeCompare(tree, left->key,  root->key) > 0) AVLPanic("TreeValidate - Invalid binary tree 2 (%d).\n", before);
	if (right && TreeCompare(tree, right->key, root->key) < 0) AVLPanic("TreeValidate - Invalid binary tree 3 (%d).\n", before);

	int leftHeight = TreeValidate(left, before, tree, root, depth + 1);
	int rightHeight = TreeValidate(right, before, tree, root, depth + 1);
	int height = (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
	if (height != root->height) AVLPanic("TreeValidate - Invalid AVL tree 1 (%d).\n", before);

#if 0
	static int maxSeenDepth = 0;
	if (maxSeenDepth < depth) {
		maxSeenDepth = depth;
		EsPrint("New depth reached! %d\n", maxSeenDepth);
	}
#endif

	return height;
#else
	(void) root;
	(void) before;
	(void) tree;
	(void) parent;
	(void) depth;
	return 0;
#endif
}

template <class T>
AVLItem<T> *TreeRotateLeft(AVLItem<T> *x) {
	AVLItem<T> *y = x->children[1], *t = y->children[0];
	y->children[0] = x, x->children[1] = t;
	if (x) x->parent = y; 
	if (t) t->parent = x;

	int leftHeight, rightHeight, balance;

	leftHeight  = x->children[0] ? x->children[0]->height : 0;
	rightHeight = x->children[1] ? x->children[1]->height : 0;
	balance     = leftHeight - rightHeight;
	x->height   = (balance > 0 ? leftHeight : rightHeight) + 1;

	leftHeight  = y->children[0] ? y->children[0]->height : 0;
	rightHeight = y->children[1] ? y->children[1]->height : 0;
	balance     = leftHeight - rightHeight;
	y->height   = (balance > 0 ? leftHeight : rightHeight) + 1;

	return y;
}

template <class T>
AVLItem<T> *TreeRotateRight(AVLItem<T> *y) {
	AVLItem<T> *x = y->children[0], *t = x->children[1];
	x->children[1] = y, y->children[0] = t;
	if (y) y->parent = x;
	if (t) t->parent = y;

	int leftHeight, rightHeight, balance;

	leftHeight  = y->children[0] ? y->children[0]->height : 0;
	rightHeight = y->children[1] ? y->children[1]->height : 0;
	balance     = leftHeight - rightHeight;
	y->height   = (balance > 0 ? leftHeight : rightHeight) + 1;

	leftHeight  = x->children[0] ? x->children[0]->height : 0;
	rightHeight = x->children[1] ? x->children[1]->height : 0;
	balance     = leftHeight - rightHeight;
	x->height   = (balance > 0 ? leftHeight : rightHeight) + 1;

	return x;
}

enum AVLDuplicateKeyPolicy {
	AVL_DUPLICATE_KEYS_PANIC,
	AVL_DUPLICATE_KEYS_ALLOW,
	AVL_DUPLICATE_KEYS_FAIL,
};

template <class T>
bool TreeInsert(AVLTree<T> *tree, AVLItem<T> *item, T *thisItem, AVLKey key, AVLDuplicateKeyPolicy duplicateKeyPolicy = AVL_DUPLICATE_KEYS_PANIC) {
	if (tree->modCheck) AVLPanic("TreeInsert - Concurrent modification\n");
	tree->modCheck = true; EsDefer({tree->modCheck = false;});

	TreeValidate(tree->root, true, tree);

#ifdef TREE_VALIDATE
	if (item->tree) {
		AVLPanic("TreeInsert - Item %x already in tree %x (adding to %x).\n", item, item->tree, tree);
	}

	item->tree = tree;
#endif

	item->key = key;
	item->children[0] = item->children[1] = nullptr;
	item->thisItem = thisItem;
	item->height = 1;

	AVLItem<T> **link = &tree->root, *parent = nullptr;

	while (true) {
		AVLItem<T> *node = *link;

		if (!node) {
			*link = item;
			item->parent = parent;
			break;
		}

		if (TreeCompare(tree, item->key, node->key) == 0) {
			if (duplicateKeyPolicy == AVL_DUPLICATE_KEYS_PANIC) {
				AVLPanic("TreeInsertRecursive - Duplicate keys: %x and %x both have key %x.\n", item, node, node->key);
			} else if (duplicateKeyPolicy == AVL_DUPLICATE_KEYS_FAIL) {
				return false;
			}
		}

		link = node->children + (TreeCompare(tree, item->key, node->key) > 0);
		parent = node;
	}

	AVLItem<T> fakeRoot = {};
	tree->root->parent = &fakeRoot;
#ifdef TREE_VALIDATE
	fakeRoot.tree = tree;
#endif
	fakeRoot.key = {};
	fakeRoot.children[0] = tree->root;

	item = item->parent;

	while (item != &fakeRoot) {
		int leftHeight  = item->children[0] ? item->children[0]->height : 0;
		int rightHeight = item->children[1] ? item->children[1]->height : 0;
		int balance = leftHeight - rightHeight;

		item->height = (balance > 0 ? leftHeight : rightHeight) + 1;
		AVLItem<T> *newRoot = nullptr;
		AVLItem<T> *oldParent = item->parent;

		if (balance > 1 && TreeCompare(tree, key, item->children[0]->key) <= 0) {
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateRight(item);
		} else if (balance > 1 && TreeCompare(tree, key, item->children[0]->key) > 0 && item->children[0]->children[1]) {
			item->children[0] = TreeRotateLeft(item->children[0]);
			item->children[0]->parent = item;
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateRight(item);
		} else if (balance < -1 && TreeCompare(tree, key, item->children[1]->key) > 0) {
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateLeft(item);
		} else if (balance < -1 && TreeCompare(tree, key, item->children[1]->key) <= 0 && item->children[1]->children[0]) {
			item->children[1] = TreeRotateRight(item->children[1]);
			item->children[1]->parent = item;
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateLeft(item);
		}

		if (newRoot) newRoot->parent = oldParent;
		item = oldParent;
	}

	tree->root = fakeRoot.children[0];
	tree->root->parent = nullptr;

	TreeValidate(tree->root, false, tree);
	return true;
}

template <class T>
AVLItem<T> *TreeFindRecursive(AVLTree<T> *tree, AVLItem<T> *root, AVLKey &key, TreeSearchMode mode) {
	if (!root) return nullptr;
	if (TreeCompare(tree, root->key, key) == 0) return root;

	if (mode == TREE_SEARCH_EXACT) {
		return TreeFindRecursive(tree, root->children[TreeCompare(tree, root->key, key) < 0], key, mode);
	} else if (mode == TREE_SEARCH_SMALLEST_ABOVE_OR_EQUAL) {
		if (TreeCompare(tree, root->key, key) > 0) { 
			AVLItem<T> *item = TreeFindRecursive(tree, root->children[0], key, mode); 
			if (item) return item; else return root;
		} else { 
			return TreeFindRecursive(tree, root->children[1], key, mode); 
		}
	} else if (mode == TREE_SEARCH_LARGEST_BELOW_OR_EQUAL) {
		if (TreeCompare(tree, root->key, key) < 0) { 
			AVLItem<T> *item = TreeFindRecursive(tree, root->children[1], key, mode); 
			if (item) return item; else return root;
		} else { 
			return TreeFindRecursive(tree, root->children[0], key, mode); 
		}
	} else {
		AVLPanic("TreeFindRecursive - Invalid search mode.\n");
		return nullptr;
	}
}

template <class T>
AVLItem<T> *TreeFind(AVLTree<T> *tree, AVLKey key, TreeSearchMode mode) {
	if (tree->modCheck) AVLPanic("TreeFind - Concurrent access\n");

	TreeValidate(tree->root, true, tree);
	return TreeFindRecursive(tree, tree->root, key, mode);
}

template <class T>
int TreeGetBalance(AVLItem<T> *item) {
	if (!item) return 0;

	int leftHeight  = item->children[0] ? item->children[0]->height : 0;
	int rightHeight = item->children[1] ? item->children[1]->height : 0;
	return leftHeight - rightHeight;
}

template <class T>
void TreeRemove(AVLTree<T> *tree, AVLItem<T> *item) {
	if (tree->modCheck) AVLPanic("TreeRemove - Concurrent modification\n");
	tree->modCheck = true; EsDefer({tree->modCheck = false;});

	TreeValidate(tree->root, true, tree);

#ifdef TREE_VALIDATE
	if (item->tree != tree) AVLPanic("TreeRemove - Item %x not in tree %x (in %x).\n", item, tree, item->tree);
#endif

	AVLItem<T> fakeRoot = {};
	tree->root->parent = &fakeRoot;
#ifdef TREE_VALIDATE
	fakeRoot.tree = tree;
#endif
	fakeRoot.key = {}; 
	fakeRoot.children[0] = tree->root;

	if (item->children[0] && item->children[1]) {
		// Swap the item we're removing with the smallest item on its right side.
		AVLKey smallest = {};
		TreeSwapItems(TreeFindRecursive(tree, item->children[1], smallest, TREE_SEARCH_SMALLEST_ABOVE_OR_EQUAL), item);
	}

	AVLItem<T> **link = item->parent->children + (item->parent->children[1] == item);
	*link = item->children[0] ? item->children[0] : item->children[1];
	if (*link) (*link)->parent = item->parent;
#ifdef TREE_VALIDATE
	item->tree = nullptr;
#endif
	if (*link) item = *link; else item = item->parent;

	while (item != &fakeRoot) {
		int leftHeight  = item->children[0] ? item->children[0]->height : 0;
		int rightHeight = item->children[1] ? item->children[1]->height : 0;
		int balance = leftHeight - rightHeight;

		item->height = (balance > 0 ? leftHeight : rightHeight) + 1;
		AVLItem<T> *newRoot = nullptr;
		AVLItem<T> *oldParent = item->parent;

		if (balance > 1 && TreeGetBalance(item->children[0]) >= 0) {
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateRight(item);
		} else if (balance > 1 && TreeGetBalance(item->children[0]) < 0) {
			item->children[0] = TreeRotateLeft(item->children[0]);
			item->children[0]->parent = item;
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateRight(item);
		} else if (balance < -1 && TreeGetBalance(item->children[1]) <= 0) {
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateLeft(item);
		} else if (balance < -1 && TreeGetBalance(item->children[1]) > 0) {
			item->children[1] = TreeRotateRight(item->children[1]);
			item->children[1]->parent = item;
			oldParent->children[oldParent->children[1] == item] = newRoot = TreeRotateLeft(item);
		}

		if (newRoot) newRoot->parent = oldParent;
		item = oldParent;
	}

	tree->root = fakeRoot.children[0];

	if (tree->root) {
		if (tree->root->parent != &fakeRoot) AVLPanic("TreeRemove - Incorrect root parent.\n");
		tree->root->parent = nullptr;
	}

	TreeValidate(tree->root, false, tree);
}

#ifdef KERNEL

struct _MapItem {
	AVLItem<_MapItem> item;
};

struct _MapHeader {
	AVLTree<_MapItem> *tree;
	bool copyKeys;
	_MapItem **mapItems;
	_ArrayHeader array;
};

#define MapHeader(map) ((_MapHeader *) (map) - 1)
#define MapLength(map) (ArrayLength(map))
#define MapInitialise(map, copyKeys, longKeys, heap) (_MapInitialise((void **) &(map), sizeof((map)[0]), heap, copyKeys, longKeys))
#define MapGet(map, key) (_MapCast(map, _MapGet((void **) &(map), key)))
#define MapPut(map, key) (_MapCast(map, _MapPut((void **) &(map), key, sizeof((map)[0]))))
#define MapDelete(map, key) (_MapDelete((void **) &(map), key, sizeof((map)[0])))
#define MapFree(map) (_MapFree((void **) &(map), sizeof((map)[0])))
#define MapIterate(map, callback, context) (_MapIterate((void **) &(map), callback, sizeof((map)[0]), context))

#define MapIterateCallback (AVLKey key, uintptr_t index, void *element, EsGeneric context)
typedef void (*MapIterateCallbackFunction) MapIterateCallback;

template <class T>
inline T *_MapCast(T *, void *element) {
	return (T *) element;
}

bool _MapInitialise(void **map, size_t itemSize, KernelHeap heap, bool copyKeys, bool longKeys) {
	size_t newLength = 4;

	_MapHeader *header = (_MapHeader *) EsHeapAllocate(sizeof(_MapHeader) + itemSize * newLength, true, heap);
	if (!header) return false;

	header->tree = (AVLTree<_MapItem> *) EsHeapAllocate(sizeof(AVLTree<_MapItem>), true, heap);

	if (!header->tree) { 
		EsHeapFree(header, sizeof(_MapHeader) + itemSize * newLength, heap); 
		return false; 
	}

	if (!ArrayInitialise(header->mapItems, heap)) { 
		EsHeapFree(header->tree, sizeof(AVLTree<_MapItem>), heap); 
		EsHeapFree(header, sizeof(_MapHeader) + itemSize * newLength, heap); 
		return false; 
	}

	header->array.length = 0;
	header->array.allocated = newLength;
	header->array.heap = heap;
	header->tree->longKeys = longKeys;
	header->copyKeys = copyKeys;
	*map = header + 1;

	return true;
}

inline void *_MapGet(void **map, AVLKey key) {
	_MapHeader *header = MapHeader(*map);
	AVLItem<_MapItem> *avlItem = TreeFind(header->tree, key, TREE_SEARCH_EXACT);
	return avlItem ? ((uint8_t *) *map + (uintptr_t) avlItem->thisItem) : nullptr;
}

void *_MapPut(void **map, AVLKey key, size_t itemSize) {
	void *current = _MapGet(map, key);
	if (current) return current;

	_MapHeader *header = MapHeader(*map);

	if (header->copyKeys) {
		void *newKey = EsHeapAllocate(key.longKeyBytes, false, header->array.heap);
		if (!newKey) return nullptr;
		EsMemoryCopy(newKey, key.longKey, key.longKeyBytes);
		key.longKey = newKey;
	}

	_MapItem *item = (_MapItem *) EsHeapAllocate(sizeof(_MapItem), true, header->array.heap);

	if (!item) { 
		if (header->copyKeys) EsHeapFree((void *) key.longKey, key.longKeyBytes, header->array.heap); 
		return nullptr; 
	}

	if (!ArrayAdd(header->mapItems, item)) {
		if (header->copyKeys) EsHeapFree((void *) key.longKey, key.longKeyBytes, header->array.heap); 
		EsHeapFree(item, sizeof(_MapItem), header->array.heap); 
		return nullptr; 
	}

	void *element = _ArrayInsert(map, nullptr, itemSize, -1, sizeof(_MapHeader) - sizeof(_ArrayHeader));

	if (!element) { 
		if (header->copyKeys) EsHeapFree((void *) key.longKey, key.longKeyBytes, header->array.heap); 
		ArrayHeader(header->mapItems)->length--;
		EsHeapFree(item, sizeof(_MapItem), header->array.heap); 
		return nullptr; 
	}

	header = MapHeader(*map); // The header may have changed after ArrayInsert.
	TreeInsert(header->tree, &item->item, (_MapItem *) ((uint8_t *) element - (uint8_t *) *map), key);
	EsMemoryZero(element, itemSize);

	return element;
}

bool _MapDelete(void **map, AVLKey key, size_t itemSize) {
	_MapHeader *header = MapHeader(*map);
	AVLItem<_MapItem> *avlItem = TreeFind(header->tree, key, TREE_SEARCH_EXACT);
	_MapItem *mapItem = (_MapItem *) avlItem;
	if (!avlItem) return false;
	TreeRemove(header->tree, avlItem);
	uintptr_t elementOffset = (uintptr_t) avlItem->thisItem;
	void *element = (uint8_t *) *map + elementOffset;
	AVLKey elementKey = avlItem->key;
	void *lastElement = (uint8_t *) *map + itemSize * (header->array.length - 1);

	if (element != lastElement) {
		// Copy the last item to the empty slot.
		EsMemoryCopy(element, lastElement, itemSize); 
		header->mapItems[header->array.length - 1]->item.thisItem = mapItem->item.thisItem;
		header->mapItems[elementOffset / itemSize] = header->mapItems[header->array.length - 1];
	}

	if (header->copyKeys) {
		EsHeapFree((void *) elementKey.longKey, elementKey.longKeyBytes, header->array.heap);
	}

	EsHeapFree(mapItem, sizeof(_MapItem), header->array.heap);
	ArrayHeader(header->mapItems)->length--;
	header->array.length--;
	return true;
}

void _MapFreeItem(AVLItem<_MapItem> *item, KernelHeap heap, bool freeKeys) {
	if (freeKeys) {
		EsHeapFree((void *) item->key.longKey, item->key.longKeyBytes, heap);
	}

	if (item->children[0]) {
		_MapFreeItem(item->children[0], heap, freeKeys);
		EsHeapFree(item->children[0], sizeof(_MapItem), heap);
	}

	if (item->children[1]) {
		_MapFreeItem(item->children[1], heap, freeKeys);
		EsHeapFree(item->children[1], sizeof(_MapItem), heap);
	}
}

void _MapFree(void **map, size_t itemSize) {
	if (!(*map)) return;
	_MapHeader *header = MapHeader(*map);

	if (header->tree->root) {
		_MapFreeItem(header->tree->root, header->array.heap, header->copyKeys);
		EsHeapFree(header->tree->root, sizeof(_MapItem), header->array.heap);
	}

	ArrayFree(header->mapItems);
	EsHeapFree(header->tree, sizeof(AVLTree<_MapItem>), header->array.heap);
	EsHeapFree(header, sizeof(_MapHeader) + itemSize * header->array.allocated, header->array.heap);
	*map = nullptr;
}

void _MapIterateRecurse(AVLItem<_MapItem> *item, MapIterateCallbackFunction callback, size_t itemSize, EsGeneric context, uint8_t *base) {
	if (item->children[0]) _MapIterateRecurse(item->children[0], callback, itemSize, context, base);
	callback(item->key, (uintptr_t) item->thisItem / itemSize, base + (uintptr_t) item->thisItem, context);
	if (item->children[1]) _MapIterateRecurse(item->children[1], callback, itemSize, context, base);
}

inline void _MapIterate(void **map, MapIterateCallbackFunction callback, size_t itemSize, EsGeneric context) {
	_MapHeader *header = MapHeader(*map);

	if (header->tree->root) {
		_MapIterateRecurse(header->tree->root, callback, itemSize, context, (uint8_t *) *map);
	}
}

/*
struct Word {
	const char *value;
};

Word *words;

void TestMap() {
	MapInitialise(words, true, true, K_FIXED);
	MapPut(words, MakeLongKey(EsLiteral("hello")))->value = "world";
	MapPut(words, MakeLongKey(EsLiteral("this")))->value = "isn't";
	MapPut(words, MakeLongKey(EsLiteral("resize1")))->value = "1";
	MapPut(words, MakeLongKey(EsLiteral("resize2")))->value = "2";
	MapPut(words, MakeLongKey(EsLiteral("resize3")))->value = "3";
	MapPut(words, MakeLongKey(EsLiteral("this")))->value = "is";
	MapPut(words, MakeLongKey(EsLiteral("a")))->value = "test";
	EsPrint("%z\n", MapGet(words, MakeLongKey(EsLiteral("hello")))->value);
	EsPrint("%z\n", MapGet(words, MakeLongKey(EsLiteral("this")))->value);
	EsPrint("%z\n", MapGet(words, MakeLongKey(EsLiteral("a")))->value);
	EsPrint("%x\n", MapGet(words, MakeLongKey(EsLiteral("unknown"))));
	EsPrint("---\n");
	MapIterate(words, [] MapIterateCallback { Word word = words[index]; EsPrint("(%d) %s: %z\n", index, key.longKeyBytes, key.longKey, word.value); });
	EsPrint("---\n");
	MapDelete(words, MakeLongKey(EsLiteral("hello")));
	MapIterate(words, [] MapIterateCallback { Word word = words[index]; EsPrint("(%d) %s: %z\n", index, key.longKeyBytes, key.longKey, word.value); });
	EsPrint("---\n");
	MapDelete(words, MakeLongKey(EsLiteral("this")));
	MapIterate(words, [] MapIterateCallback { Word word = words[index]; EsPrint("(%d) %s: %z\n", index, key.longKeyBytes, key.longKey, word.value); });
	EsPrint("---\n");
	MapDelete(words, MakeLongKey(EsLiteral("a")));
	MapIterate(words, [] MapIterateCallback { Word word = words[index]; EsPrint("(%d) %s: %z\n", index, key.longKeyBytes, key.longKey, word.value); });
	EsPrint("---\n");
	MapDelete(words, MakeLongKey(EsLiteral("resize1")));
	MapIterate(words, [] MapIterateCallback { Word word = words[index]; EsPrint("(%d) %s: %z\n", index, key.longKeyBytes, key.longKey, word.value); });
	EsPrint("---\n");
	MapFree(words);
}
*/

#endif

#endif
