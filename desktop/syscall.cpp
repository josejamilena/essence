void *EsMemoryReserve(size_t size, EsMemoryProtection protection, uint32_t flags) {
	intptr_t result = EsSyscall(ES_SYSCALL_ALLOCATE, size, flags, protection, 0);

	if (result >= 0) {
		return (void *) result;
	} else {
		return nullptr;
	}
}

void EsMemoryUnreserve(void *address, size_t size) {
	EsSyscall(ES_SYSCALL_FREE, (uintptr_t) address, size, 0, 0);
}

bool EsMemoryCommit(void *pointer, size_t bytes) {
	EsAssert(((uintptr_t) pointer & (ES_PAGE_SIZE - 1)) == 0 && (bytes & (ES_PAGE_SIZE - 1)) == 0); // Misaligned pointer/bytes in EsMemoryCommit.
	return ES_SUCCESS == (intptr_t) EsSyscall(ES_SYSCALL_MEMORY_COMMIT, (uintptr_t) pointer >> ES_PAGE_BITS, bytes >> ES_PAGE_BITS, 0, 0);
}

bool EsMemoryDecommit(void *pointer, size_t bytes) {
	EsAssert(((uintptr_t) pointer & (ES_PAGE_SIZE - 1)) == 0 && (bytes & (ES_PAGE_SIZE - 1)) == 0); // Misaligned pointer/bytes in EsMemoryDecommit.
	return ES_SUCCESS == (intptr_t) EsSyscall(ES_SYSCALL_MEMORY_COMMIT, (uintptr_t) pointer >> ES_PAGE_BITS, bytes >> ES_PAGE_BITS, 1, 0);
}

EsError EsProcessCreate2(EsProcessCreationArguments *arguments, EsProcessInformation *information) {
	EsProcessInformation _information;
	if (!information) information = &_information;

	EsError error = EsSyscall(ES_SYSCALL_CREATE_PROCESS, (uintptr_t) arguments, 0, (uintptr_t) information, 0);

	if (error == ES_SUCCESS && information == &_information) {
		EsHandleClose(information->handle);
		EsHandleClose(information->mainThread.handle);
	}

	return error;
}

EsError EsProcessCreate(const char *executablePath, ptrdiff_t executablePathLength, EsProcessInformation *information, EsGeneric argument) {
	if (executablePathLength == -1) executablePathLength = EsCStringLength(executablePath);
	EsProcessCreationArguments arguments = {};
	arguments.executablePath = executablePath;
	arguments.executablePathBytes = executablePathLength;
	arguments.creationArgument = argument;
	arguments.permissions = ES_PERMISSION_INHERIT;
	return EsProcessCreate2(&arguments, information);
}

EsGeneric EsGetCreationArgument(EsHandle process) {
	return EsSyscall(ES_SYSCALL_GET_CREATION_ARGUMENT, process, 0, 0, 0);
}

EsError EsMessagePost(EsElement *target, EsMessage *message) {
	return EsSyscall(ES_SYSCALL_POST_MESSAGE, (uintptr_t) message, (uintptr_t) target, 0, 0);
}

EsError EsMessagePostRemote(EsHandle process, EsMessage *message) {
	return EsSyscall(ES_SYSCALL_POST_MESSAGE_REMOTE, (uintptr_t) message, process, 0, 0);
}

EsHandle EsEventCreate(bool autoReset) {
	return EsSyscall(ES_SYSCALL_CREATE_EVENT, autoReset, 0, 0, 0);
}

void EsEventSet(EsHandle handle) {
	EsSyscall(ES_SYSCALL_SET_EVENT, handle, 0, 0, 0);
}

void EsEventReset(EsHandle handle) {
	EsSyscall(ES_SYSCALL_RESET_EVENT, handle, 0, 0, 0);
}

EsError EsEventPoll(EsHandle handle) {
	return EsSyscall(ES_SYSCALL_POLL_EVENT, handle, 0, 0, 0);
}

EsError EsHandleClose(EsHandle handle) {
	return EsSyscall(ES_SYSCALL_CLOSE_HANDLE, handle, 0, 0, 0);
}

void EsThreadTerminate(EsHandle thread) {
	EsSyscall(ES_SYSCALL_TERMINATE_THREAD, thread, 0, 0, 0);
}

void EsProcessTerminate(EsHandle process, int status) {
	EsSyscall(ES_SYSCALL_TERMINATE_PROCESS, process, status, 0, 0);
}

void EsProcessTerminateCurrent() {
	EsSyscall(ES_SYSCALL_TERMINATE_PROCESS, ES_CURRENT_PROCESS, 0, 0, 0);
}

int EsProcessGetExitStatus(EsHandle process) {
	return EsSyscall(ES_SYSCALL_GET_PROCESS_STATUS, process, 0, 0, 0);
}

void ThreadInitialise();

void ThreadEntry(EsGeneric argument, EsThreadEntryFunction entryFunction) {
	ThreadInitialise();
	entryFunction(argument);
	EsThreadTerminate(ES_CURRENT_THREAD);
}

EsError EsThreadCreate(EsThreadEntryFunction entryFunction, EsThreadInformation *information, EsGeneric argument) {
	EsThreadInformation discard = {};

	if (!information) {
		information = &discard;
	}

	return EsSyscall(ES_SYSCALL_CREATE_THREAD, (uintptr_t) ThreadEntry, (uintptr_t) entryFunction, (uintptr_t) information, argument.u);
}

EsError EsFileWriteAll(const char *filePath, ptrdiff_t filePathLength, const void *data, size_t sizes) {
	return EsFileWriteAllGather(filePath, filePathLength, &data, &sizes, 1);
}

EsError EsFileWriteAllGather(const char *filePath, ptrdiff_t filePathLength, const void **data, size_t *sizes, size_t gatherCount) {
	if (filePathLength == -1) filePathLength = EsCStringLength(filePath);

	EsFileInformation information = EsFileOpen((char *) filePath, filePathLength, ES_FILE_WRITE_EXCLUSIVE);

	if (ES_SUCCESS != information.error) {
		return information.error;
	}

	size_t fileSize = 0;

	for (uintptr_t i = 0; i < gatherCount; i++) {
		fileSize += sizes[i];
	}

	EsError error = EsFileResize(information.handle, fileSize);
	if (ES_CHECK_ERROR(error)) return error;

	size_t offset = 0;

	for (uintptr_t i = 0; i < gatherCount; i++) {
		error = EsFileWriteSync(information.handle, offset, sizes[i], data[i]);
		if (ES_CHECK_ERROR(error)) return error;
		offset += sizes[i];
	}

	error = EsFileControl(information.handle, ES_FILE_CONTROL_NOTIFY_MONITORS | ES_FILE_CONTROL_FLUSH);
	EsHandleClose(information.handle);
	return error;
}

void *EsFileReadAll(const char *filePath, ptrdiff_t filePathLength, size_t *fileSize) {
	EsFileInformation information = EsFileOpen((char *) filePath, filePathLength, ES_FILE_READ | ES_NODE_FAIL_IF_NOT_FOUND);

	if (ES_SUCCESS != information.error) {
		return nullptr;
	}

	if (fileSize) *fileSize = information.size;
#ifdef KERNEL
	void *buffer = EsHeapAllocate(information.size + 1, false, K_PAGED);
#else
	void *buffer = EsHeapAllocate(information.size + 1, false);
#endif
	if (!buffer) return nullptr;
	((char *) buffer)[information.size] = 0;

	if (information.size != EsFileReadSync(information.handle, 0, information.size, buffer)) {
#ifdef KERNEL
		EsHeapFree(buffer, information.size + 1, K_PAGED);
#else
		EsHeapFree(buffer);
#endif
		buffer = nullptr;
	}
	
	EsHandleClose(information.handle);
	return buffer;
}

EsHandle EsMemoryOpen(size_t size, const char *name, ptrdiff_t nameLength, unsigned flags) {
	if (nameLength == -1) nameLength = EsCStringLength(name);
	return EsSyscall(ES_SYSCALL_OPEN_SHARED_MEMORY, size, (uintptr_t) name, nameLength, flags);
}

EsHandle EsMemoryShare(EsHandle sharedMemoryRegion, EsHandle targetProcess, bool readOnly) {
	return EsSyscall(ES_SYSCALL_SHARE_MEMORY, sharedMemoryRegion, targetProcess, readOnly, 0);
}

void *EsObjectMap(EsHandle sharedMemoryRegion, uintptr_t offset, size_t size, unsigned flags) {
	intptr_t result = EsSyscall(ES_SYSCALL_MAP_OBJECT, sharedMemoryRegion, offset, size, flags);

	if (result >= 0) {
		return (void *) result;
	} else {
		return nullptr;
	}
}

EsFileInformation EsFileOpen(const char *path, ptrdiff_t pathLength, uint32_t flags) {
	if (pathLength == -1) {
		pathLength = EsCStringLength(path);
	}

	_EsNodeInformation node;
	intptr_t result = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathLength, flags, (uintptr_t) &node);

	if (result == ES_SUCCESS && node.type == ES_NODE_DIRECTORY) {
		result = ES_ERROR_INCORRECT_NODE_TYPE;
		EsHandleClose(node.handle);
	}

	EsFileInformation information = {};
	information.handle = node.handle;
	information.size = node.fileSize;
	information.error = result;
	return information;
}

size_t EsFileReadSync(EsHandle handle, EsFileOffset offset, size_t size, void *buffer) {
	intptr_t result = EsSyscall(ES_SYSCALL_READ_FILE_SYNC, handle, offset, size, (uintptr_t) buffer);
	return result;
}

size_t EsFileWriteSync(EsHandle handle, EsFileOffset offset, size_t size, const void *buffer) {
	intptr_t result = EsSyscall(ES_SYSCALL_WRITE_FILE_SYNC, handle, offset, size, (uintptr_t) buffer);
	return result;
}

EsFileOffset EsFileGetSize(EsHandle handle) {
	return EsSyscall(ES_SYSCALL_FILE_GET_SIZE, handle, 0, 0, 0);
}

EsError EsFileResize(EsHandle handle, EsFileOffset newSize) {
	return EsSyscall(ES_SYSCALL_RESIZE_FILE, handle, newSize, 0, 0);
}

uintptr_t EsWait(EsHandle *handles, size_t count, uintptr_t timeoutMs) {
	return EsSyscall(ES_SYSCALL_WAIT, (uintptr_t) handles, count, timeoutMs, 0);
}

void EsProcessPause(EsHandle process, bool resume) {
	EsSyscall(ES_SYSCALL_PAUSE_PROCESS, process, resume, 0, 0);
}

void EsProcessCrash(EsError error, const char *message, ptrdiff_t messageBytes) {
	if (messageBytes == -1) {
		messageBytes = EsCStringLength(message);
	}

#ifndef KERNEL
	if (message && messageBytes) {
		EsPrintDirect(message, messageBytes);
	}
#else
	EsPrint("Kernel process crash: %s", messageBytes, message);
#endif

	EsSyscall(ES_SYSCALL_CRASH_PROCESS, error, 0, 0, 0);
}

uintptr_t EsThreadGetID(EsHandle thread) {
	if (thread == ES_CURRENT_THREAD) {
		return GetThreadLocalStorage()->id;
	} else {
		return EsSyscall(ES_SYSCALL_GET_THREAD_ID, thread, 0, 0, 0);
	}
}

uintptr_t EsProcessGetID(EsHandle process) {
	return EsSyscall(ES_SYSCALL_GET_THREAD_ID, process, 0, 0, 0);
}

ptrdiff_t EsDirectoryEnumerateChildrenFromHandle(EsHandle directory, EsDirectoryChild *buffer, size_t size) {
	if (!size) return 0;
	return EsSyscall(ES_SYSCALL_ENUMERATE_DIRECTORY_CHILDREN, directory, (uintptr_t) buffer, size, 0);
}

#ifndef KERNEL
ptrdiff_t EsDirectoryEnumerateChildren(const char *path, ptrdiff_t pathBytes, EsDirectoryChild **buffer) {
	*buffer = nullptr;
	if (pathBytes == -1) pathBytes = EsCStringLength(path);
	_EsNodeInformation node;
	EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathBytes, ES_NODE_FAIL_IF_NOT_FOUND | ES_NODE_DIRECTORY, (uintptr_t) &node);
	if (error != ES_SUCCESS) return error;
	*buffer = (EsDirectoryChild *) EsHeapAllocate(sizeof(EsDirectoryChild) * node.directoryChildren, true);
	ptrdiff_t result = EsDirectoryEnumerateChildrenFromHandle(node.handle, *buffer, node.directoryChildren);
	if (ES_CHECK_ERROR(error)) { EsHeapFree(*buffer); *buffer = nullptr; }
	EsHandleClose(node.handle);
	return result;
}
#endif

void EsBatch(EsBatchCall *calls, size_t count) {
#if 0
	for (uintptr_t i = 0; i < count; i++) {
		EsBatchCall *call = calls + i;
		// ... modify system call for version changes ... 
	}
#endif

	EsSyscall(ES_SYSCALL_BATCH, (uintptr_t) calls, count, 0, 0);
}

EsError EsPathDelete(const char *path, ptrdiff_t pathBytes) {
	_EsNodeInformation node;
	if (pathBytes == -1) pathBytes = EsCStringLength(path);
	EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathBytes, ES_NODE_FAIL_IF_NOT_FOUND | ES_FILE_WRITE_EXCLUSIVE, (uintptr_t) &node);
	if (ES_CHECK_ERROR(error)) return error;
	error = EsSyscall(ES_SYSCALL_DELETE_NODE, node.handle, 0, 0, 0);
	EsHandleClose(node.handle);
	return error;
}

void *EsFileMap(const char *path, ptrdiff_t pathBytes, size_t *fileSize, uint32_t flags) {
	EsFileInformation information = EsFileOpen(path, pathBytes, 
			ES_NODE_FAIL_IF_NOT_FOUND | ((flags & ES_MAP_OBJECT_READ_WRITE) ? ES_FILE_WRITE_EXCLUSIVE : ES_FILE_READ));

	if (ES_CHECK_ERROR(information.error)) {
		return nullptr;
	}

	void *base = EsObjectMap(information.handle, 0, information.size, flags); 
	EsHandleClose(information.handle);
	if (fileSize) *fileSize = information.size;
	return base;
}

EsError EsPathMove(const char *oldPath, ptrdiff_t oldPathBytes, const char *newPath, ptrdiff_t newPathBytes) {
	if (oldPathBytes == -1) oldPathBytes = EsCStringLength(oldPath);
	if (newPathBytes == -1) newPathBytes = EsCStringLength(newPath);

	_EsNodeInformation node = {};
	_EsNodeInformation directory = {};
	EsError error;

	error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) oldPath, oldPathBytes, ES_NODE_FAIL_IF_NOT_FOUND, (uintptr_t) &node);
	if (error != ES_SUCCESS) return error;

	uintptr_t s = 0;
	for (intptr_t i = 0; i < newPathBytes; i++) if (newPath[i] == '/') s = i + 1;
	error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) newPath, s, ES_NODE_DIRECTORY | ES_NODE_FAIL_IF_NOT_FOUND, (uintptr_t) &directory);
	if (error != ES_SUCCESS) { EsHandleClose(node.handle); return error; }

	error = EsSyscall(ES_SYSCALL_MOVE_NODE, node.handle, directory.handle, (uintptr_t) newPath + s, newPathBytes - s);
	EsHandleClose(node.handle);
	EsHandleClose(directory.handle);
	return error;
}

bool EsPathExists(const char *path, ptrdiff_t pathBytes, EsNodeType *type) {
	if (pathBytes == -1) pathBytes = EsCStringLength(path);
	_EsNodeInformation node = {};
	EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathBytes, ES_NODE_FAIL_IF_NOT_FOUND, (uintptr_t) &node);
	if (error != ES_SUCCESS) return false;
	EsHandleClose(node.handle);
	if (type) *type = node.type;
	return true;
}

EsError EsPathCreate(const char *path, ptrdiff_t pathBytes, EsNodeType type, bool createLeadingDirectories) {
	if (pathBytes == -1) pathBytes = EsCStringLength(path);
	_EsNodeInformation node = {};
	EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathBytes, 
			ES_NODE_FAIL_IF_FOUND | type | (createLeadingDirectories ? ES_NODE_CREATE_DIRECTORIES : 0), 
			(uintptr_t) &node);
	if (error != ES_SUCCESS) return error;
	EsHandleClose(node.handle);
	return ES_SUCCESS;
}

EsError EsFileControl(EsHandle file, uint32_t flags) {
	return EsSyscall(ES_SYSCALL_FILE_CONTROL, file, flags, 0, 0);
}

void EsConstantBufferRead(EsHandle buffer, void *output) {
	EsSyscall(ES_SYSCALL_READ_CONSTANT_BUFFER, buffer, (uintptr_t) output, 0, 0);
}

void EsProcessGetState(EsHandle process, EsProcessState *state) {
	EsSyscall(ES_SYSCALL_GET_PROCESS_STATE, process, (uintptr_t) state, 0, 0);
}

void EsSchedulerYield() {
	EsSyscall(ES_SYSCALL_YIELD_SCHEDULER, 0, 0, 0, 0);
}

void EsSleep(uint64_t milliseconds) {
	EsSyscall(ES_SYSCALL_SLEEP, milliseconds >> 32, milliseconds & 0xFFFFFFFF, 0, 0);
}

EsHandle EsTakeSystemSnapshot(int type, size_t *bufferSize) {
	return EsSyscall(ES_SYSCALL_TAKE_SYSTEM_SNAPSHOT, type, (uintptr_t) bufferSize, 0, 0);
}

EsHandle EsProcessOpen(uint64_t pid) {
	// TODO This won't work correctly if arguments to system call are 32-bit.
	return EsSyscall(ES_SYSCALL_OPEN_PROCESS, pid, 0, 0, 0);
}

#ifndef KERNEL
EsHandle EsConstantBufferShare(EsHandle constantBuffer, EsHandle targetProcess) {
	return EsSyscall(ES_SYSCALL_SHARE_CONSTANT_BUFFER, constantBuffer, targetProcess, 0, 0);
}

EsHandle EsConstantBufferCreate(const void *data, size_t dataBytes, EsHandle targetProcess) {
	return EsSyscall(ES_SYSCALL_CREATE_CONSTANT_BUFFER, (uintptr_t) data, targetProcess, dataBytes, 0);
}
#endif

void EsGetSystemInformation(EsSystemInformation *information) {
	EsSyscall(ES_SYSCALL_GET_SYSTEM_INFORMATION, (uintptr_t) information, 0, 0, 0);
}

bool EsMailslotSendData(EsHandle pipe, void *data, size_t bytes) {
	return EsSyscall(ES_SYSCALL_MAILSLOT_SEND_DATA, pipe, (uintptr_t) data, bytes, 0);
}

EsHandle EsTimerCreate() {
	return EsSyscall(ES_SYSCALL_TIMER_CREATE, 0, 0, 0, 0);
}

void EsTimerSet(EsHandle handle, uint64_t afterMs, EsTimerCallbackFunction callback, EsGeneric argument) {
	// TODO This won't work correctly if arguments to system call are 32-bit.
	EsSyscall(ES_SYSCALL_TIMER_SET, handle, afterMs, (uintptr_t) callback, argument.u);
}

size_t EsUserGetHomeFolder(char *buffer, size_t bytes) {
	return EsSyscall(ES_SYSCALL_USER_GET_HOME_FOLDER, (uintptr_t) buffer, bytes, 0, 0);
}

EsAudioStream *EsAudioStreamOpen(EsAudioDeviceID device, uint64_t bufferLengthUs) {
	return (EsAudioStream *) EsSyscall(ES_SYSCALL_AUDIO_STREAM_OPEN, device, bufferLengthUs, 0, 0);
}

void EsAudioStreamClose(EsAudioStream *stream) {
	EsHandleClose(stream->handle);
	EsObjectUnmap(stream->buffer);
	EsObjectUnmap(stream);
}

void EsAudioStreamNotify(EsAudioStream *stream) {
	EsSyscall(ES_SYSCALL_AUDIO_STREAM_NOTIFY, stream->handle, 0, 0, 0);
}

EsError EsAddressResolve(const char *domain, ptrdiff_t domainBytes, uint32_t flags, EsAddress *address) {
	return EsSyscall(ES_SYSCALL_DOMAIN_NAME_RESOLVE, (uintptr_t) domain, domainBytes, (uintptr_t) address, flags);
}

EsError EsConnectionOpen(EsConnection *connection, uint32_t flags) {
	connection->error = ES_SUCCESS;
	connection->open = false;

	EsError error = EsSyscall(ES_SYSCALL_CONNECTION_OPEN, (uintptr_t) connection, flags, 0, 0);

	if (error == ES_SUCCESS && (flags & ES_CONNECTION_OPEN_WAIT)) {
		while (!connection->open && connection->error == ES_SUCCESS) {
			EsConnectionPoll(connection);
		}

		return connection->error;
	} else {
		return error;
	}
}

void EsConnectionPoll(EsConnection *connection) {
	EsSyscall(ES_SYSCALL_CONNECTION_POLL, (uintptr_t) connection, 0, 0, connection->handle);
}

void EsConnectionNotify(EsConnection *connection) {
	EsSyscall(ES_SYSCALL_CONNECTION_NOTIFY, 0, connection->sendWritePointer, connection->receiveReadPointer, connection->handle);
}

void EsConnectionClose(EsConnection *connection) {
	EsObjectUnmap(connection->sendBuffer);
	EsHandleClose(connection->handle);
}

EsError EsConnectionWriteSync(EsConnection *connection, const void *_data, size_t dataBytes) {
	const uint8_t *data = (const uint8_t *) _data;

	while (dataBytes) {
		EsConnectionPoll(connection);

		if (connection->error != ES_SUCCESS) {
			return connection->error;
		}

		size_t space = connection->sendWritePointer >= connection->sendReadPointer 
			? connection->sendBufferBytes - connection->sendWritePointer
			: connection->sendReadPointer - connection->sendWritePointer - 1;

		if (!space) {
			continue;
		}

		size_t bytesToWrite = space > dataBytes ? dataBytes : space;
		EsMemoryCopy(connection->sendBuffer + connection->sendWritePointer, data, bytesToWrite);
		data += bytesToWrite, dataBytes -= bytesToWrite;
		connection->sendWritePointer = (connection->sendWritePointer + bytesToWrite) % connection->sendBufferBytes;
		EsConnectionNotify(connection);
	}

	return ES_SUCCESS;
}

EsError EsConnectionRead(EsConnection *connection, void *_buffer, size_t bufferBytes, size_t *bytesRead) {
	uint8_t *buffer = (uint8_t *) _buffer;
	*bytesRead = 0;

	EsConnectionPoll(connection);

	if (connection->error != ES_SUCCESS) {
		return connection->error;
	}

	while (bufferBytes && connection->receiveReadPointer != connection->receiveWritePointer) {
		size_t bytesAvailable = connection->receiveReadPointer > connection->receiveWritePointer
			? connection->receiveBufferBytes - connection->receiveReadPointer
			: connection->receiveWritePointer - connection->receiveReadPointer;
		size_t bytesToRead = bufferBytes > bytesAvailable ? bytesAvailable : bufferBytes;
		EsMemoryCopy(buffer, connection->receiveBuffer + connection->receiveReadPointer, bytesToRead);
		connection->receiveReadPointer = (connection->receiveReadPointer + bytesToRead) % connection->receiveBufferBytes;
		buffer += bytesToRead, bufferBytes -= bytesToRead;
		*bytesRead += bytesToRead;
		EsConnectionNotify(connection);
	}

	return ES_SUCCESS;
}

void EsEventForward(EsHandle event, EsHandle eventSink, EsGeneric data) {
	EsSyscall(ES_SYSCALL_EVENT_FORWARD, event, eventSink, data.u, 0);
}

EsHandle EsEventSinkCreate(bool ignoreDuplicates) {
	return EsSyscall(ES_SYSCALL_EVENT_SINK_CREATE, ignoreDuplicates, 0, 0, 0);
}

EsError EsEventSinkPop(EsHandle eventSink, EsGeneric *data) {
	EsGeneric unused; if (!data) data = &unused;
	return EsSyscall(ES_SYSCALL_EVENT_SINK_POP, eventSink, (uintptr_t) data, 0, 0);
}

EsError EsEventSinkPush(EsHandle eventSink, EsGeneric data) {
	return EsSyscall(ES_SYSCALL_EVENT_SINK_PUSH, eventSink, data.u, 0, 0);
}

size_t EsGameControllerStatePoll(EsGameControllerState *buffer) {
	return EsSyscall(ES_SYSCALL_GAME_CONTROLLER_STATE_POLL, (uintptr_t) buffer, 0, 0, 0);
}

#define CLIPBOARD_FORMAT_TEXT (1)

void EsClipboardAddText(EsClipboard clipboard, const char *text, ptrdiff_t textBytes) {
	EsMessageMutexCheck();

	if (textBytes == -1) {
		textBytes = EsCStringLength(text);
	}

	EsSyscall(ES_SYSCALL_CLIPBOARD_ADD, clipboard, CLIPBOARD_FORMAT_TEXT, (uintptr_t) text, textBytes);
}

bool EsClipboardHasText(EsClipboard clipboard) {
	return EsSyscall(ES_SYSCALL_CLIPBOARD_HAS, clipboard, CLIPBOARD_FORMAT_TEXT, 0, 0);
}

char *EsClipboardReadText(EsClipboard clipboard, size_t *bytes) {
	*bytes = 0;
	EsHandle handle = EsSyscall(ES_SYSCALL_CLIPBOARD_READ, clipboard, CLIPBOARD_FORMAT_TEXT, 0, (uintptr_t) bytes);
	if (handle == ES_INVALID_HANDLE) return nullptr;
	char *buffer = (char *) EsHeapAllocate(*bytes, false);
	if (!buffer) return nullptr;
	EsConstantBufferRead(handle, buffer);
	EsHandleClose(handle);
	return buffer;
}
