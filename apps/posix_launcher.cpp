#define ES_INSTANCE_TYPE Instance
#include <essence.h>

#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MSG_RECEIVED_OUTPUT ((EsMessageType) (ES_MSG_USER_START + 1))

struct Instance : EsInstance {
	EsTextbox *textboxOutput, *textboxInput;
	volatile bool runningCommand;
	char *command;
};

const EsStyle monospacedTextbox = {
	.inherit = ES_STYLE_TEXTBOX_NO_BORDER,

	.metrics = {
		.mask = ES_THEME_METRICS_FONT_FAMILY,
		.fontFamily = ES_FONT_MONOSPACED,
	},
};

char *ParseArgument(char **position) {
	char *start = *position;

	while (*start == ' ') {
		start++;
	}

	if (!(*start)) {
		return nullptr;
	}

	char *end = start;

	while ((*end != ' ' || (end != start && end[-1] == '\\')) && *end) {
		end++;
	}

	if (*end) {
		*end = 0;
		end++;
	}

	*position = end;
	return start;
}

void WriteToOutputTextbox(Instance *instance, const char *string, ptrdiff_t stringBytes) {
	if (stringBytes == -1) stringBytes = EsCRTstrlen(string);
	char *copy = (char *) EsHeapAllocate(stringBytes, false);
	EsMemoryCopy(copy, string, stringBytes);
	EsMessage m = {};
	m.type = MSG_RECEIVED_OUTPUT;
	m.user.context1 = copy;
	m.user.context2 = stringBytes;
	m.user.context3 = instance;
	EsMessagePost(nullptr, &m); 
}

void RunCommandThread(EsGeneric argument) {
	// TODO This thread hasn't been initialised by libc, so it's TLS isn't correct, and it's overwriting memory.

	Instance *instance = (Instance *) argument.p;

	char *argv[64];
	int argc = 0;

	char *commandPosition = instance->command;

	while (argc < 63) {
		argv[argc] = ParseArgument(&commandPosition);
		if (!argv[argc]) break;
		argc++;
	}

	if (argc) {
		argv[argc] = nullptr;

		char executable[4096];

		if (argv[0][0] == '/') {
			EsStringFormat(executable, sizeof(executable), "%z", argv[0]);
		} else {
			EsStringFormat(executable, sizeof(executable), "/Applications/POSIX/bin/%z", argv[0]);
		}

		char *envp[5] = { 
			(char *) "LANG=en_US.UTF-8", 
			(char *) "PWD=/", 
			(char *) "HOME=/", 
			(char *) "PATH=/Applications/POSIX/bin", 
			nullptr 
		};

		int standardOutputPipe[2];
		pipe(standardOutputPipe);
		int standardOutputPipeReadEnd = standardOutputPipe[0];
		int standardOutputPipeWriteEnd = standardOutputPipe[1];

		pid_t pid = vfork();

		if (pid == 0) {
			dup2(standardOutputPipeWriteEnd, 1);
			dup2(standardOutputPipeWriteEnd, 2);
			close(standardOutputPipeWriteEnd);
			execve(executable, argv, envp);
			// TODO Report the error.
			_exit(-1);
		} else if (pid == -1) {
			// TODO Report the error.
		}

		close(standardOutputPipeWriteEnd);

		while (true) {
			char buffer[1024];
			ssize_t bytesRead = read(standardOutputPipeReadEnd, buffer, 1024);

			if (bytesRead <= 0) {
				break;
			} else {
				WriteToOutputTextbox(instance, buffer, bytesRead);
			}
		}

		close(standardOutputPipeReadEnd);

		int status;
		wait4(-1, &status, 0, NULL);

		{
			char buffer[64];
			size_t bytes = EsStringFormat(buffer, sizeof(buffer), "\nProcess exited with status %d.\n", status >> 8);
			if (status) WriteToOutputTextbox(instance, buffer, bytes);
			WriteToOutputTextbox(instance, "\n----------------\n", -1);
		}
	}

	EsHeapFree(instance->command);
	__sync_synchronize();
	instance->runningCommand = false;
}

int ProcessTextboxInputMessage(EsElement *element, EsMessage *message) {
	Instance *instance = element->instance;

	if (message->type == ES_MSG_KEY_DOWN) {
		if (message->keyboard.scancode == ES_SCANCODE_ENTER 
				&& !message->keyboard.ctrl 
				&& !message->keyboard.alt 
				&& !message->keyboard.shift
				&& !instance->runningCommand
				&& EsTextboxGetLineLength(instance->textboxInput)) {
			instance->runningCommand = true;
			instance->command = EsTextboxGetContents(instance->textboxInput);

			EsTextboxInsert(instance->textboxOutput, "\n", -1, false);
			EsTextboxInsert(instance->textboxOutput, instance->command, -1, false);
			EsTextboxInsert(instance->textboxOutput, "\n", -1, false);
			EsTextboxEnsureCaretVisible(instance->textboxOutput, false);

			EsThreadCreate(RunCommandThread, nullptr, instance); 

			EsTextboxClear(instance->textboxInput, false);
			return ES_HANDLED;
		}
	}

	return 0;
}

int main(int argc, char **argv) {
	(void) argc;
	(void) argv;

	while (true) {
		EsMessage *message = EsMessageReceive();

		if (message->type == ES_MSG_INSTANCE_CREATE) {
			Instance *instance = EsInstanceCreate(message, "POSIX Launcher");
			EsWindow *window = instance->window;
			EsWindowSetIcon(window, ES_ICON_UTILITIES_TERMINAL);
			EsPanel *panel = EsPanelCreate(window, ES_PANEL_VERTICAL | ES_CELL_FILL, ES_STYLE_PANEL_WINDOW_BACKGROUND);
			instance->textboxOutput = EsTextboxCreate(panel, ES_TEXTBOX_MULTILINE | ES_CELL_FILL, &monospacedTextbox);
			EsSpacerCreate(panel, ES_CELL_H_FILL, ES_STYLE_SEPARATOR_HORIZONTAL);
			instance->textboxInput = EsTextboxCreate(panel, ES_CELL_H_FILL, &monospacedTextbox);
			instance->textboxInput->userCallback = ProcessTextboxInputMessage;
			EsElementFocus(instance->textboxInput);
		} else if (message->type == MSG_RECEIVED_OUTPUT) {
			char *string = (char *) message->user.context1.p;
			size_t stringBytes = message->user.context2.u;
			Instance *instance = (Instance *) message->user.context3.p;
			EsTextboxMoveCaretRelative(instance->textboxOutput, ES_TEXTBOX_MOVE_CARET_ALL);
			EsTextboxInsert(instance->textboxOutput, string, stringBytes, false);
			EsTextboxEnsureCaretVisible(instance->textboxOutput, false);
			EsHeapFree(string);
		}
	}
}
