// ----------------- Includes:

#ifndef IncludedEssenceAPIHeader
#define IncludedEssenceAPIHeader

#include <limits.h>
#ifndef KERNEL
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#endif
#include <stdarg.h>

// --------- C++/C differences:

#ifdef __cplusplus

#define ES_EXTERN_C extern "C"
#define ES_CONSTRUCTOR(x) x
#define ES_NULL nullptr

// Scoped defer: http://www.gingerbill.org/article/defer-in-cpp.html
template <typename F> struct _EsDefer4 { F f; _EsDefer4(F f) : f(f) {} ~_EsDefer4() { f(); } };
template <typename F> _EsDefer4<F> _EsDeferFunction(F f) { return _EsDefer4<F>(f); }
#define EsDEFER_1(x, y) x ## y
#define EsDEFER_2(x, y) EsDEFER_1(x, y)
#define EsDEFER_3(x) EsDEFER_2(x, __COUNTER__)
#define _EsDefer5(code) auto EsDEFER_3(_defer_) = _EsDeferFunction([&](){code;})
#define EsDefer(code) _EsDefer5(code)

union EsGeneric {
	uintptr_t u;
	intptr_t i;
	void *p;

	inline EsGeneric() = default;

	inline EsGeneric(uintptr_t y) { u = y; }
	inline EsGeneric( intptr_t y) { i = y; }
	inline EsGeneric(unsigned  y) { u = y; }
	inline EsGeneric(     int  y) { i = y; }
	inline EsGeneric(    void *y) { p = y; }

	inline bool operator==(EsGeneric &r) const { return r.u == u; }
};

#else

#define ES_EXTERN_C extern
#define ES_CONSTRUCTOR(x)
#define ES_NULL 0

typedef union {
	uintptr_t u;
	intptr_t i;
	void *p;
} EsGeneric;

typedef struct EsElementPublic EsElementPublic;

#endif

// --------- Macros:

#ifdef ARCH_X86_64
#define ES_API_BASE ((void **) 0x1000)
#define ES_SHARED_MEMORY_MAXIMUM_SIZE ((size_t) (1024) * 1024 * 1024 * 1024)
#define ES_PAGE_SIZE (4096)
#define ES_PAGE_BITS (12)

typedef struct EsCRTjmp_buf {
	uintptr_t rsp, rbp, rbx, r12, r13, r14, r15, rip;
} EsCRTjmp_buf;

ES_EXTERN_C int _EsCRTsetjmp(EsCRTjmp_buf *env);
ES_EXTERN_C __attribute__((noreturn)) void _EsCRTlongjmp(EsCRTjmp_buf *env, int val);
#define EsCRTsetjmp(x) _EsCRTsetjmp(&(x))
#define EsCRTlongjmp(x, y) _EsCRTlongjmp(&(x), (y))
#endif

#define EsContainerOf(type, member, pointer) ((type *) ((uint8_t *) pointer - offsetof(type, member)))

#define ES_CHECK_ERROR(x) (((intptr_t) (x)) < (ES_SUCCESS))

#define ES_MAKE_RECTANGLE_ALL(x) (EsRectangle{(int32_t)(x),(int32_t)(x),(int32_t)(x),(int32_t)(x)})
#define ES_MAKE_RECTANGLE(l, r, t, b) (EsRectangle{(int32_t)(l),(int32_t)(r),(int32_t)(t),(int32_t)(b)})
#define ES_MAKE_POINT(x, y) (EsPoint{(int32_t)(x),(int32_t)(y)})

#define ES_MEMORY_MOVE_BACKWARDS -

#define EsWaitSingle(object) EsWait(&object, 1, ES_WAIT_NO_TIMEOUT)
#define EsObjectUnmap EsMemoryUnreserve

#define EsLiteral(x) (char *) x, EsCStringLength((char *) x)

#define ES_STYLE_CAST(x) ((EsStyle *) (uintptr_t) (x))

#ifndef ES_INSTANCE_TYPE
#define ES_INSTANCE_TYPE struct EsInstance
#else
struct ES_INSTANCE_TYPE;
#endif
#ifdef __cplusplus
#define EsInstanceCreate(_message, ...) (static_cast<ES_INSTANCE_TYPE *>(_EsInstanceCreate(sizeof(ES_INSTANCE_TYPE), _message, __VA_ARGS__)))
#else
#define EsInstanceCreate(_message, ...) ((ES_INSTANCE_TYPE *) _EsInstanceCreate(sizeof(ES_INSTANCE_TYPE), _message, __VA_ARGS__))
#endif

#define ES_STORE_TAG_ALL (-1)

#define ES_SAMPLE_FORMAT_BYTES_PER_SAMPLE(x) \
	((x) == ES_SAMPLE_FORMAT_U8 ? 1 : (x) == ES_SAMPLE_FORMAT_S16LE ? 2 : 4)

#ifndef KERNEL
#ifdef ES_API
ES_EXTERN_C uintptr_t _APISyscall(uintptr_t argument0, uintptr_t argument1, uintptr_t argument2, uintptr_t unused, uintptr_t argument3, uintptr_t argument4);
#define EsSyscall(a, b, c, d, e) _APISyscall((a), (b), (c), 0, (d), (e))
#define _EsSyscall _APISyscall
#else
#define EsSyscall(a, b, c, d, e) _EsSyscall((a), (b), (c), 0, (d), (e))
#endif
#endif

// --------- Algorithms:

#define ES_MACRO_SORT(_name, _type, _compar) void _name(_type *base, size_t nmemb) { \
	if (nmemb <= 1) return; \
	\
	if (nmemb <= 16) { \
		for (intptr_t i = 1; i < nmemb; i++) { \
			for (intptr_t j = i; j > 0; j--) { \
				_type *_left = base + j, *_right = _left - 1; \
				int result; _compar if (result >= 0) break; \
				\
				_type swap = base[j]; \
				base[j] = base[j - 1]; \
				base[j - 1] = swap; \
			} \
		} \
		\
		return; \
	} \
	\
	intptr_t i = -1, j = nmemb; \
	\
	while (true) { \
		_type *_left, *_right = base; \
		int result; \
		\
		while (true) { _left = base + ++i; _compar if (result >= 0) break; } \
		while (true) { _left = base + --j; _compar if (result <= 0) break; } \
		\
		if (i >= j) break; \
		\
		_type swap = base[i]; \
		base[i] = base[j]; \
		base[j] = swap; \
	} \
	\
	_name(base, ++j); \
	_name(base + j, nmemb - j); \
} \

#define ES_MACRO_SEARCH(_count, _compar, _result, _found) \
	do { \
		if (_count) { \
			intptr_t low = 0; \
			intptr_t high = _count - 1; \
			\
			while (low <= high) { \
				uintptr_t index = ((high - low) >> 1) + low; \
				int result; \
				_compar \
				\
				if (result < 0) { \
					high = index - 1; \
				} else if (result > 0) { \
					low = index + 1; \
				} else { \
					_result = index; \
					_found = true; \
					break; \
				} \
			} \
			\
			if (high < low) { \
				_result = low; \
				_found = false; \
			} \
		} else { \
			_result = 0; \
			_found = false; \
		} \
	} while (0)

// --------- Misc:

typedef uint64_t _EsLongConstant;
typedef long double EsLongDouble;
typedef const char *EsCString;

#ifndef ES_API
ES_EXTERN_C void _init();
ES_EXTERN_C void _start();
#endif

#define EsAssert(x) do { if (!(x)) { EsAssertionFailure(__FILE__, __LINE__); } } while (0)
#define EsCRTassert EsAssert

#define ES_INFINITY __builtin_inff()
#define ES_PI (3.1415926535897932384626433832795028841971693994)

#if defined(ES_API) || defined(KERNEL)
struct EsProcessStartupInformation {
	bool isDesktop;
	uintptr_t applicationStartAddress;
	uintptr_t tlsImageStart;
	uintptr_t tlsImageBytes;
	uintptr_t tlsBytes; // All bytes after the image are to be zeroed.
};

struct _EsPOSIXSyscall {
	intptr_t index;
	intptr_t arguments[7];
};

struct _EsUserLoginArguments {
	const char *name;
	size_t nameBytes;
	const char *home;
	size_t homeBytes;
};
#endif

ES_EXTERN_C int EsCRTsnprintf(char *buffer, size_t bufferSize, const char *format, ...); 
ES_EXTERN_C int EsCRTsprintf(char *buffer, const char *format, ...); 
ES_EXTERN_C int EsCRTvsnprintf(char *buffer, size_t bufferSize, const char *format, va_list arguments); 
