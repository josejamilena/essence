struct String {
	char *text;
	size_t bytes, allocated;
};

String StringAllocateAndFormat(const char *format, ...) {
	String string = {};
	va_list arguments;
	va_start(arguments, format);
	string.text = EsStringAllocateAndFormatV(&string.bytes, format, arguments);
	va_end(arguments);
	string.allocated = string.bytes;
	return string;
}

String StringFromLiteral(const char *literal) {
	String string = {};
	string.text = (char *) literal;
	string.bytes = EsCStringLength(literal);
	return string;
}

void StringAppend(String *string, String &with) {
	if (string->bytes + with.bytes > string->allocated) {
		string->allocated = (string->allocated + with.bytes) * 2;
		string->text = (char *) EsHeapReallocate(string->text, string->allocated, false);
	}

	EsMemoryCopy(string->text + string->bytes, with.text, with.bytes);
	string->bytes += with.bytes;
}

void StringDestroy(String &string) {
	EsAssert(string.allocated == string.bytes); // Attempting to free a partial string.
	EsHeapFree(string.text);
	string.text = nullptr;
	string.bytes = string.allocated = 0;
}

String StringDuplicate(String &string) {
	String result = {};
	result.bytes = result.allocated = string.bytes;
	result.text = (char *) EsHeapAllocate(result.bytes + 1, false);
	result.text[result.bytes] = 0;
	EsMemoryCopy(result.text, string.text, result.bytes);
	return result;
}

inline bool StringStartsWith(String a, String b) {
	return a.bytes >= b.bytes && 0 == EsMemoryCompare(a.text, b.text, b.bytes);
}

inline bool StringEquals(String a, String b) {
	return a.bytes == b.bytes && 0 == EsMemoryCompare(a.text, b.text, a.bytes);
}

#define STRING(x) x.text, x.bytes
#define STRFMT(x) x.bytes, x.text

uintptr_t PathCountSections(String string) {
	ptrdiff_t sectionCount = 0;

	for (uintptr_t i = 0; i < string.bytes; i++) {
		if (string.text[i] == '/') {
			sectionCount++;
		}
	}

	return sectionCount;
}

String PathGetSection(String string, ptrdiff_t index) {
	String output = {};
	size_t stringBytes = string.bytes;
	char *text = string.text;

	if (index < 0) {
		index += PathCountSections(string);
	}

	if (index < 0) {
		return output;
	}

	uintptr_t i = 0, bytes = 0;

	for (; index && i < stringBytes; i++) {
		if (text[i] == '/') {
			index--;
		}
	}

	if (index) {
		return output;
	}

	output.text = text + i;

	for (; i < stringBytes; i++) {
		if (text[i] == '/') {
			break;
		} else {
			bytes++;
		}
	}

	output.bytes = bytes;

	return output;
}

String PathGetExtension(String string) {
	String extension = {};
	int lastSeparator = 0;

	for (intptr_t i = string.bytes - 1; i >= 0; i--) {
		if (string.text[i] == '.') {
			lastSeparator = i;
			break;
		}
	}

	if (!lastSeparator && string.text[0] != '.') {
		extension.text = string.text + string.bytes;
		extension.bytes = 0;
		return extension;
	} else {
		extension.text = string.text + lastSeparator + 1;
		extension.bytes = string.bytes - lastSeparator - 1;
		return extension;
	}
}

String PathGetParent(String string) {
	size_t newPathBytes = 1;

	for (uintptr_t i = 0; i < string.bytes - 1; i++) {
		if (string.text[i] == '/') {
			newPathBytes = i + 1;
		}
	}

	String result = {};
	result.bytes = newPathBytes;
	result.text = string.text;

	return result;
}
