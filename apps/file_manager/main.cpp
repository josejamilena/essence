#define ES_INSTANCE_TYPE Instance
#include <essence.h>
#include <shared/stb_ds.h>
#include <shared/strings.cpp>

// TODO Possible candidates for moving in the core API:
// 	- String/paths utils
// 	- Blocking/non-blocking task systems

// TODO Don't show modals if a folder can't be loaded.
// 	Instead, show a list view with an error message,
// 	and disable interactions.

// TODO Renaming/deleting a monitored folder.

// TODO List view empty text wrap loop when too small.
// 	- We avoid this currently by setting the minimumWidth on our list views.

// TODO String translation.
#define TL(x) x

#define ERROR_LOAD_FOLDER (1)
#define ERROR_NEW_FOLDER (2)
#define ERROR_RENAME_ITEM (3)

#define MESSAGE_BLOCKING_TASK_COMPLETE ((EsMessageType) (ES_MSG_USER_START + 1))
#define MESSAGE_NON_BLOCKING_TASK_COMPLETE ((EsMessageType) (ES_MSG_USER_START + 2))

const char *errorTypeStrings[] = {
	interfaceString_FileManagerUnknownError,
	interfaceString_FileManagerOpenFolderError,
	interfaceString_FileManagerNewFolderError,
	interfaceString_FileManagerRenameItemError,
};

void InstanceReportError(struct Instance *instance, int error, EsError code);

#include "string.cpp"

struct FolderEntry {
	uint16_t handles;
	bool isFolder, sizeUnknown;
	uint8_t nameBytes, extensionOffset; // 0 -> 256.
	char *name;
	EsFileOffset size;
	struct Folder *folder;

	inline String GetName() { 
		return { .text = name, .bytes = nameBytes ?: 256u, .allocated = nameBytes ?: 256u }; 
	}

	inline String GetExtension() { 
		uintptr_t offset = extensionOffset ?: 256u;
		return { .text = name + offset, .bytes = (nameBytes ?: 256u) - offset }; 
	}
};

struct ListEntry {
	FolderEntry *entry;
	bool selected;
};

struct Task {
	EsGeneric context, context2;
	String string;
	EsError result;
	const char *cDescription;
	unsigned generation;

	void (*callback)(Instance *instance, Task *task);
	void (*then)(Instance *instance, Task *task); // Called on the main thread.
};

struct HistoryEntry {
	String path;
	// TODO Restoring focused item.
};

struct Instance : EsInstance {
	// Interface elements.

	EsListView *list;
	EsListView *bookmarkView;
	EsTextbox *breadcrumbBar;
	EsButton *newFolderButton;

	union {
		struct {
			EsButton *createButton;
			EsTextbox *nameTextbox;
		} newFolder;

		struct {
			EsButton *renameButton;
			EsTextbox *nameTextbox;
		} rename;
	};

	// Path and history.

	String path;
	DS_ARRAY(HistoryEntry) pathBackwardHistory;
	DS_ARRAY(HistoryEntry) pathForwardHistory;

	// Commands.

	EsCommand commandGoBackwards, commandGoForwards, commandGoParent;
	EsCommand commandNewFolder, commandRename;

	// Active folder.

	struct Folder *folder;

	// Sorted and filtered list contents.

	DS_ARRAY(ListEntry) listContents;

	size_t selectedItemCount;

	// Blocking task thread.
	// Tasks that block the use of the instance,
	// but display progress and can be (optionally) cancelled.
	// Shows the dialog after some threshold.
	// Responsible for freeing the instance.
#define BLOCKING_TASK_DIALOG_THRESHOLD_MS (100)

	EsHandle blockingTaskThread, 
		 blockingTaskQueued, blockingTaskComplete;
	Task blockingTask;
	unsigned blockingTaskGeneration;
	bool blockingTaskInProgress;

	// Non-blocking task thread.

	EsHandle nonBlockingTaskThread,
		 nonBlockingTaskWorkAvailable;
	EsMutex nonBlockingTaskMutex;
	DS_ARRAY(Task *) nonBlockingTasks;
};

void BlockingTaskThread(EsGeneric _instance) {
	Instance *instance = (Instance *) _instance.p;

	while (true) {
		EsWait(&instance->blockingTaskQueued, 1, ES_WAIT_NO_TIMEOUT);
		instance->blockingTask.callback(instance, &instance->blockingTask);

		EsEventSet(instance->blockingTaskComplete);

		EsMessage m = { MESSAGE_BLOCKING_TASK_COMPLETE };
		m.user.context1.p = instance;
		m.user.context2.u = instance->blockingTask.generation;
		EsMessagePost(nullptr, &m);
	}
}

void BlockingTaskQueue(Instance *instance, Task task) {
	EsAssert(!instance->blockingTaskInProgress); // Cannot queue a new blocking task if the previous has not finished.

	task.generation = instance->blockingTaskGeneration;
	instance->blockingTask = task;
	instance->blockingTaskInProgress = true;
	EsEventSet(instance->blockingTaskQueued);

	ptrdiff_t result = EsWait(&instance->blockingTaskComplete, 1, BLOCKING_TASK_DIALOG_THRESHOLD_MS);

	if (result == ES_ERROR_TIMEOUT_REACHED) {
		EsDialogShowAlert(instance->window, task.cDescription, -1, INTERFACE_STRING(FileManagerOngoingTaskDescription), ES_ICON_TOOLS_TIMER_SYMBOLIC);
		// TODO Progress bar.
	} else {
		instance->blockingTaskGeneration++;
		instance->blockingTaskInProgress = false;
		if (task.then) task.then(instance, &instance->blockingTask);
	}
}

void BlockingTaskComplete(EsMessage *message) {
	Instance *instance = (Instance *) message->user.context1.p;

	if (instance->blockingTaskGeneration == message->user.context2.u) {
		EsAssert(instance->blockingTaskInProgress); // Task should have been in progress.
		instance->blockingTaskGeneration++;
		instance->blockingTaskInProgress = false;
		EsDialogClose(instance->window); // TODO Add a delay if the dialog has not been shown for very long.
		EsEventReset(instance->blockingTaskComplete);
		Task *task = &instance->blockingTask;
		if (task->then) task->then(instance, task);
	}
}

void NonBlockingTaskThread(EsGeneric _instance) {
	Instance *instance = (Instance *) _instance.p;

	while (true) {
		EsWait(&instance->nonBlockingTaskWorkAvailable, 1, ES_WAIT_NO_TIMEOUT);

		while (true) {
			EsMutexAcquire(&instance->nonBlockingTaskMutex);

			if (!arrlen(instance->nonBlockingTasks)) {
				EsMutexRelease(&instance->nonBlockingTaskMutex);
				break;
			}

			Task *task = instance->nonBlockingTasks[0];
			arrdel(instance->nonBlockingTasks, 0);
			EsMutexRelease(&instance->nonBlockingTaskMutex);

			task->callback(instance, task);

			EsMessage m = { MESSAGE_NON_BLOCKING_TASK_COMPLETE };
			m.user.context1.p = instance;
			m.user.context2.p = task;
			EsMessagePost(nullptr, &m);
		}
	}
}

void NonBlockingTaskQueue(Instance *instance, Task _task) {
	Task *task = (Task *) EsHeapAllocate(sizeof(Task), false);
	EsMemoryCopy(task, &_task, sizeof(Task));
	EsMutexAcquire(&instance->nonBlockingTaskMutex);
	arrput(instance->nonBlockingTasks, task);
	EsMutexRelease(&instance->nonBlockingTaskMutex);
	EsEventSet(instance->nonBlockingTaskWorkAvailable);
}

void NonBlockingTaskComplete(EsMessage *message) {
	Instance *instance = (Instance *) message->user.context1.p;
	Task *task = (Task *) message->user.context2.p;
	if (task->then) task->then(instance, task);
	EsHeapFree(task);
}

#include "type_database.cpp"
#include "folder.cpp"
#include "ui.cpp"

void _start() {
	_init();

	EsArenaInitialise(&folderArena, 32 * sizeof(Folder), sizeof(Folder));
	AddKnownFileTypes();

	while (true) {
		EsMessage *message = EsMessageReceive();

		if (message->type == ES_MSG_INSTANCE_CREATE) {
			Instance *instance = EsInstanceCreate(message, INTERFACE_STRING(FileManagerTitle));

			instance->blockingTaskQueued           = EsEventCreate(true /* autoReset */);
			instance->blockingTaskComplete         = EsEventCreate(true /* autoReset */);
			instance->nonBlockingTaskWorkAvailable = EsEventCreate(true /* autoReset */);

			EsThreadInformation blockingTaskThread = {};
			EsThreadCreate(BlockingTaskThread, &blockingTaskThread, instance);
			instance->blockingTaskThread = blockingTaskThread.handle;

			EsThreadInformation nonBlockingTaskThread = {};
			EsThreadCreate(NonBlockingTaskThread, &nonBlockingTaskThread, instance);
			instance->nonBlockingTaskThread = nonBlockingTaskThread.handle;

			InstanceCreateUI(instance);
		} else if (message->type == MESSAGE_BLOCKING_TASK_COMPLETE) {
			BlockingTaskComplete(message);
		} else if (message->type == MESSAGE_NON_BLOCKING_TASK_COMPLETE) {
			NonBlockingTaskComplete(message);
		}
	}
}
