#pragma GCC diagnostic ignored "-Wunused-variable" push

#define DEFINE_INTERFACE_STRING(name, text) static const char *interfaceString_ ## name = text;
#define INTERFACE_STRING(name) interfaceString_ ## name, -1

// Common.

DEFINE_INTERFACE_STRING(CommonErrorTitle, "Error");

DEFINE_INTERFACE_STRING(CommonCancel, "Cancel");

DEFINE_INTERFACE_STRING(CommonUndo, "Undo");
DEFINE_INTERFACE_STRING(CommonRedo, "Redo");
DEFINE_INTERFACE_STRING(CommonClipboardCut, "Cut");
DEFINE_INTERFACE_STRING(CommonClipboardCopy, "Copy");
DEFINE_INTERFACE_STRING(CommonClipboardPaste, "Paste");
DEFINE_INTERFACE_STRING(CommonSelectionSelectAll, "Select all");
DEFINE_INTERFACE_STRING(CommonSelectionDelete, "Delete");

DEFINE_INTERFACE_STRING(CommonFileMenu, "File");
DEFINE_INTERFACE_STRING(CommonFileNew, "New");
DEFINE_INTERFACE_STRING(CommonFileOpen, "Open");
DEFINE_INTERFACE_STRING(CommonFileMakeCopy, "Make a copy");
DEFINE_INTERFACE_STRING(CommonFileSaveNewVersion, "Save new version");
DEFINE_INTERFACE_STRING(CommonFileVersionHistory, "Version history");
DEFINE_INTERFACE_STRING(CommonFileRename, "Rename");
DEFINE_INTERFACE_STRING(CommonFileViewInFolder, "View in folder");
DEFINE_INTERFACE_STRING(CommonFileSave, "Save");
DEFINE_INTERFACE_STRING(CommonFileExit, "Exit");
DEFINE_INTERFACE_STRING(CommonFileRecent, "Recent files");

DEFINE_INTERFACE_STRING(CommonSearchOpen, "Search");
DEFINE_INTERFACE_STRING(CommonSearchNoMatches, "No matches found.");
DEFINE_INTERFACE_STRING(CommonSearchNext, "Find next");
DEFINE_INTERFACE_STRING(CommonSearchPrevious, "Find previous");
DEFINE_INTERFACE_STRING(CommonSearchPrompt, "Search for:");
DEFINE_INTERFACE_STRING(CommonSearchPrompt2, "Enter text to search for.");

DEFINE_INTERFACE_STRING(CommonItemFolder, "Folder");
DEFINE_INTERFACE_STRING(CommonItemFile, "File");

DEFINE_INTERFACE_STRING(CommonSystemBrand, "Essence Alpha v0.1");

// Desktop.

DEFINE_INTERFACE_STRING(DesktopCloseTab, "Close tab");
DEFINE_INTERFACE_STRING(DesktopInspectUI, "Inspect UI");
DEFINE_INTERFACE_STRING(DesktopNewTabTitle, "New Tab");
DEFINE_INTERFACE_STRING(DesktopShutdownTitle, "Shutdown");
DEFINE_INTERFACE_STRING(DesktopShutdownAction, "Shutdown");
DEFINE_INTERFACE_STRING(DesktopForceQuit, "Force quit");
DEFINE_INTERFACE_STRING(DesktopCrashedApplication, "The application has crashed. If you're a developer, more information is available in System Monitor.");
DEFINE_INTERFACE_STRING(DesktopNoSuchApplication, "The requested application could not found. It may have been uninstalled.");
DEFINE_INTERFACE_STRING(DesktopApplicationStartupError, "The requested application could not be started. Your system may be low on resources, or the application files may have been corrupted.");
DEFINE_INTERFACE_STRING(DesktopNotResponding, "The application is not responding.\nIf you choose to force quit, any unsaved data may be lost.");
DEFINE_INTERFACE_STRING(DesktopConfirmShutdown, "Are you sure you want to turn off your computer? All applications will be closed.");

// Text Editor.

DEFINE_INTERFACE_STRING(TextEditorTitle, "Text Editor");
DEFINE_INTERFACE_STRING(TextEditorSyntaxHighlighting, "Highlighting");

// File Manager.

DEFINE_INTERFACE_STRING(FileManagerOpenFolderError, "The folder could not be opened.");
DEFINE_INTERFACE_STRING(FileManagerNewFolderError, "Could not create the folder.");
DEFINE_INTERFACE_STRING(FileManagerRenameItemError, "The item could not be renamed.");
DEFINE_INTERFACE_STRING(FileManagerUnknownError, "An unknown error occurred.");
DEFINE_INTERFACE_STRING(FileManagerTitle, "File Manager");
DEFINE_INTERFACE_STRING(FileManagerRootFolder, "Computer");
DEFINE_INTERFACE_STRING(FileManagerColumnName, "Name");
DEFINE_INTERFACE_STRING(FileManagerColumnType, "Type");
DEFINE_INTERFACE_STRING(FileManagerColumnSize, "Size");
DEFINE_INTERFACE_STRING(FileManagerOpenFolderTask, "Opening folder...");
DEFINE_INTERFACE_STRING(FileManagerOpenFileError, "The file could not be opened.");
DEFINE_INTERFACE_STRING(FileManagerNoRegisteredApplicationsForFile, "None of the applications installed on this computer can open this type of file.");
DEFINE_INTERFACE_STRING(FileManagerFolderNamePrompt, "Folder name:");
DEFINE_INTERFACE_STRING(FileManagerNewFolderAction, "Create");
DEFINE_INTERFACE_STRING(FileManagerNewFolderTask, "Creating folder...");
DEFINE_INTERFACE_STRING(FileManagerRenameTitle, "Rename");
DEFINE_INTERFACE_STRING(FileManagerRenamePrompt, "Type the new name of the item:");
DEFINE_INTERFACE_STRING(FileManagerRenameAction, "Rename");
DEFINE_INTERFACE_STRING(FileManagerRenameTask, "Renaming item...");
DEFINE_INTERFACE_STRING(FileManagerEmptyBookmarkView, "Drag folders here to bookmark them.");
DEFINE_INTERFACE_STRING(FileManagerEmptyFolderView, "Drag items here to add them to the folder.");
DEFINE_INTERFACE_STRING(FileManagerNewFolderToolbarItem, "New folder");
DEFINE_INTERFACE_STRING(FileManagerGenericError, "The cause of the error could not be identified.");
DEFINE_INTERFACE_STRING(FileManagerItemAlreadyExistsError, "The item already exists in the folder.");
DEFINE_INTERFACE_STRING(FileManagerItemDoesNotExistError, "The item does not exist.");
DEFINE_INTERFACE_STRING(FileManagerOngoingTaskDescription, "This shouldn't take long.");

// TODO System Monitor.

#if 0
apps/system_monitor.cpp:16:	{ EsLiteral("Time stamp (ms)"), ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED, 150 },
apps/system_monitor.cpp:17:	{ EsLiteral("CPU"), ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED, 150 },
apps/system_monitor.cpp:18:	{ EsLiteral("Process"), 0, 400 },
apps/system_monitor.cpp:19:	{ EsLiteral("Thread"), 0, 150 },
apps/system_monitor.cpp:20:	{ EsLiteral("Count"), 0, 150 },
apps/system_monitor.cpp:24:	{ EsLiteral("Driver"), 0, 200 },
apps/system_monitor.cpp:25:	{ EsLiteral("Device ID"), 0, 200 },
apps/system_monitor.cpp:26:	{ EsLiteral("Class"), 0, 250 },
apps/system_monitor.cpp:27:	{ EsLiteral("Subclass"), 0, 250 },
apps/system_monitor.cpp:28:	{ EsLiteral("ProgIF"), 0, 150 },
apps/system_monitor.cpp:29:	{ EsLiteral("Bus"), 0, 100 },
apps/system_monitor.cpp:30:	{ EsLiteral("Slot"), 0, 100 },
apps/system_monitor.cpp:31:	{ EsLiteral("Function"), 0, 100 },
apps/system_monitor.cpp:32:	{ EsLiteral("Interrupt pin"), 0, 100 },
apps/system_monitor.cpp:33:	{ EsLiteral("Interrupt line"), 0, 100 },
apps/system_monitor.cpp:34:	{ EsLiteral("BAR0"), 0, 250 },
apps/system_monitor.cpp:35:	{ EsLiteral("BAR1"), 0, 250 },
apps/system_monitor.cpp:36:	{ EsLiteral("BAR2"), 0, 250 },
apps/system_monitor.cpp:37:	{ EsLiteral("BAR3"), 0, 250 },
apps/system_monitor.cpp:38:	{ EsLiteral("BAR4"), 0, 250 },
apps/system_monitor.cpp:39:	{ EsLiteral("BAR5"), 0, 250 },
apps/system_monitor.cpp:43:	"Unknown",
apps/system_monitor.cpp:44:	"Mass storage controller",
apps/system_monitor.cpp:45:	"Network controller",
apps/system_monitor.cpp:46:	"Display controller",
apps/system_monitor.cpp:47:	"Multimedia controller",
apps/system_monitor.cpp:48:	"Memory controller",
apps/system_monitor.cpp:49:	"Bridge controller",
apps/system_monitor.cpp:50:	"Simple communication controller",
apps/system_monitor.cpp:51:	"Base system peripheral",
apps/system_monitor.cpp:52:	"Input device controller",
apps/system_monitor.cpp:53:	"Docking station",
apps/system_monitor.cpp:54:	"Processor",
apps/system_monitor.cpp:55:	"Serial bus controller",
apps/system_monitor.cpp:56:	"Wireless controller",
apps/system_monitor.cpp:57:	"Intelligent controller",
apps/system_monitor.cpp:58:	"Satellite communication controller",
apps/system_monitor.cpp:59:	"Encryption controller",
apps/system_monitor.cpp:60:	"Signal processing controller",
apps/system_monitor.cpp:64:	"SCSI bus controller",
apps/system_monitor.cpp:65:	"IDE controller",
apps/system_monitor.cpp:66:	"Floppy disk controller",
apps/system_monitor.cpp:67:	"IPI bus controller",
apps/system_monitor.cpp:68:	"RAID controller",
apps/system_monitor.cpp:69:	"ATA controller",
apps/system_monitor.cpp:70:	"Serial ATA",
apps/system_monitor.cpp:71:	"Serial attached SCSI",
apps/system_monitor.cpp:72:	"Non-volatile memory controller",
apps/system_monitor.cpp:76:	"FireWire (IEEE 1394) controller",
apps/system_monitor.cpp:77:	"ACCESS bus",
apps/system_monitor.cpp:78:	"SSA",
apps/system_monitor.cpp:79:	"USB controller",
apps/system_monitor.cpp:80:	"Fibre channel",
apps/system_monitor.cpp:81:	"SMBus",
apps/system_monitor.cpp:82:	"InfiniBand",
apps/system_monitor.cpp:83:	"IPMI interface",
apps/system_monitor.cpp:84:	"SERCOS interface (IEC 61491)",
apps/system_monitor.cpp:85:	"CANbus",
apps/system_monitor.cpp:89:	"UHCI",
apps/system_monitor.cpp:90:	"OHCI",
apps/system_monitor.cpp:91:	"EHCI",
apps/system_monitor.cpp:92:	"XHCI",
apps/system_monitor.cpp:128:			GET_CONTENT("%d", entry->timeMs);
apps/system_monitor.cpp:130:			GET_CONTENT("%d", entry->cpu);
apps/system_monitor.cpp:132:			GET_CONTENT("%s", entry->pathBytes, entry->path);
apps/system_monitor.cpp:134:			GET_CONTENT("%s", entry->threadBytes, entry->thread);
apps/system_monitor.cpp:136:			GET_CONTENT("%d", entry->count);
apps/system_monitor.cpp:152:			GET_CONTENT("%s", entry->driverNameBytes, entry->driverName);
apps/system_monitor.cpp:154:			GET_CONTENT("%x", entry->deviceID);
apps/system_monitor.cpp:157:				? pciClassCodeStrings[entry->classCode] : "Unknown";
apps/system_monitor.cpp:158:			GET_CONTENT("%d - %z", entry->classCode, string);
apps/system_monitor.cpp:164:				? pciSubclassCodeStrings12[entry->subclassCode] : "";
apps/system_monitor.cpp:165:			GET_CONTENT("%d%z%z", entry->subclassCode, *string ? " - " : "", string);
apps/system_monitor.cpp:169:				? pciProgIFStrings12_3[entry->progIF / 0x10] : "";
apps/system_monitor.cpp:170:			GET_CONTENT("%d%z%z", entry->progIF, *string ? " - " : "", string);
apps/system_monitor.cpp:172:			GET_CONTENT("%d", entry->bus);
apps/system_monitor.cpp:174:			GET_CONTENT("%d", entry->slot);
apps/system_monitor.cpp:176:			GET_CONTENT("%d", entry->function);
apps/system_monitor.cpp:178:			GET_CONTENT("%d", entry->interruptPin);
apps/system_monitor.cpp:180:			GET_CONTENT("%d", entry->interruptLine);
apps/system_monitor.cpp:182:			GET_CONTENT("%x, %D", entry->baseAddresses[0], entry->baseAddressesSizes[0]);
apps/system_monitor.cpp:184:			GET_CONTENT("%x, %D", entry->baseAddresses[1], entry->baseAddressesSizes[1]);
apps/system_monitor.cpp:186:			GET_CONTENT("%x, %D", entry->baseAddresses[2], entry->baseAddressesSizes[2]);
apps/system_monitor.cpp:188:			GET_CONTENT("%x, %D", entry->baseAddresses[3], entry->baseAddressesSizes[3]);
apps/system_monitor.cpp:190:			GET_CONTENT("%x, %D", entry->baseAddresses[4], entry->baseAddressesSizes[4]);
apps/system_monitor.cpp:192:			GET_CONTENT("%x, %D", entry->baseAddresses[5], entry->baseAddressesSizes[5]);
apps/system_monitor.cpp:205:		Instance *instance = EsInstanceCreate(message, "System Monitor");
apps/system_monitor.cpp:228:		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "General log");
apps/system_monitor.cpp:238:		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "Context switches");
apps/system_monitor.cpp:246:		button = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_BUTTON_RADIOBOX, {}, "PCI devices");
#endif

#pragma GCC diagnostic pop
