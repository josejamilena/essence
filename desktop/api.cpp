#define ES_API
#define ES_FORWARD(x) x
#define ES_EXTERN_FORWARD extern "C"
#define ES_DIRECT_API
#include <essence.h>

extern "C" void EsUnimplemented();

#define alloca __builtin_alloca

#define FT_EXPORT(x) extern "C" x

#ifdef USE_STB_IMAGE
#define STB_IMAGE_IMPLEMENTATION
#define STBI_MALLOC(sz)           EsCRTmalloc(sz)
#define STBI_REALLOC(p,newsz)     EsCRTrealloc(p,newsz)
#define STBI_FREE(p)              EsCRTfree(p)
#define STBI_NO_STDIO
#define STBI_ONLY_PNG
#define STBI_ONLY_JPEG
#define STBI_NO_LINEAR
#define STB_IMAGE_STATIC
#include <shared/stb_image.h>
#endif

#include <shared/stb_ds.h>
#include <shared/ini.h>

uint64_t esSystemConstants[256];

#include <emmintrin.h>

#include <shared/avl_tree.cpp>
#include <shared/vga_font.cpp>
#include <shared/heap.cpp>
#include <shared/arena.cpp>
#include <shared/linked_list.cpp>
#include <shared/hash.cpp>
#include <shared/png_decoder.cpp>
#include <shared/common.cpp>
#include <shared/strings.cpp>

#define WINDOW_METADATA_TITLE (1)
#define WINDOW_METADATA_ICON (2)

EsProcessStartupInformation *startupInformation;

extern "C" uintptr_t ProcessorTLSRead(uintptr_t offset);
ptrdiff_t tlsStorageOffset;

struct ThreadLocalStorage {
	// This must be the first field.
	ThreadLocalStorage *self;

	uintptr_t id;
};

ThreadLocalStorage *GetThreadLocalStorage() {
	return (ThreadLocalStorage *) ProcessorTLSRead(tlsStorageOffset);
}

#include "syscall.cpp"

EsMutex _messageMutex;
#define messageMutex (&_messageMutex)
volatile uintptr_t messageMutexThreadID;

void MaybeDestroyElement(EsElement *element);
const char *GetConstantString(const char *key);
void UndoManagerDestroy(EsUndoManager *manager);
int TextGetStringWidth(const EsTextStyle *style, const char *string, size_t stringBytes);

struct ProcessMessageTiming {
	double startLogic, endLogic;
	double startLayout, endLayout;
	double startPaint, endPaint;
	double startUpdate, endUpdate;
};

struct EsUndoManager {
	EsInstance *instance;

	uint8_t *undoStack;
	uint8_t *redoStack;

#define UNDO_MANAGER_STATE_NORMAL (0)
#define UNDO_MANAGER_STATE_UNDOING (1)
#define UNDO_MANAGER_STATE_REDOING (2)
	int state;
};

struct ApplicationInstance {
	DS_MAP(uint32_t, EsCommand *) commands;

	EsApplicationStartupInformation *startupInformation;
	EsHandle mainWindowHandle;

	char *documentPath;
	size_t documentPathBytes;

	EsCommand commandDelete, 
		  commandSelectAll,
		  commandCopy,
		  commandCut,
		  commandPaste,
		  commandUndo,
		  commandRedo;

	const char *applicationName;
	size_t applicationNameBytes;

	struct InspectorWindow *attachedInspector;

	EsUndoManager undoManager;
	EsUndoManager *activeUndoManager;
};

EsSystemConfigurationGroup *systemConfigurationGroups;

EsSystemConfigurationItem *SystemConfigurationGetItem(EsSystemConfigurationGroup *group, const char *key, ptrdiff_t keyBytes) {
	if (keyBytes == -1) keyBytes = EsCStringLength(key);

	for (uintptr_t i = 0; i < arrlenu(group->items); i++) {
		if (0 == EsStringCompareRaw(key, keyBytes, group->items[i].key, group->items[i].keyBytes)) {
			return group->items + i;
		}
	}

	return nullptr;
}

EsSystemConfigurationGroup *SystemConfigurationGetGroup(const char *section, ptrdiff_t sectionBytes) {
	if (sectionBytes == -1) sectionBytes = EsCStringLength(section);

	for (uintptr_t i = 0; i < arrlenu(systemConfigurationGroups); i++) {
		if (0 == EsStringCompareRaw(section, sectionBytes, systemConfigurationGroups[i].section, systemConfigurationGroups[i].sectionBytes)) {
			return systemConfigurationGroups + i;
		}
	}

	return nullptr;
}

char *EsSystemConfigurationGroupReadString(EsSystemConfigurationGroup *group, const char *key, ptrdiff_t keyBytes, size_t *valueBytes) {
	EsSystemConfigurationItem *item = SystemConfigurationGetItem(group, key, keyBytes);
	if (!item) { if (valueBytes) *valueBytes = 0; return nullptr; }
	if (valueBytes) *valueBytes = item->valueBytes;
	char *copy = (char *) EsHeapAllocate(item->valueBytes + 1, false);
	copy[item->valueBytes] = 0;
	EsMemoryCopy(copy, item->value, item->valueBytes);
	return copy;
}

int64_t EsSystemConfigurationGroupReadInteger(EsSystemConfigurationGroup *group, const char *key, ptrdiff_t keyBytes, int64_t defaultValue) {
	EsSystemConfigurationItem *item = SystemConfigurationGetItem(group, key, keyBytes);
	if (!item) return defaultValue;
	return EsIntegerParse(item->value, item->valueBytes); 
}

char *EsSystemConfigurationReadString(const char *section, ptrdiff_t sectionBytes, const char *key, ptrdiff_t keyBytes, size_t *valueBytes) {
	EsSystemConfigurationGroup *group = SystemConfigurationGetGroup(section, sectionBytes);
	if (!group) { if (valueBytes) *valueBytes = 0; return nullptr; }
	return EsSystemConfigurationGroupReadString(group, key, keyBytes, valueBytes);
}

int64_t EsSystemConfigurationReadInteger(const char *section, ptrdiff_t sectionBytes, const char *key, ptrdiff_t keyBytes, int64_t defaultValue) {
	EsSystemConfigurationGroup *group = SystemConfigurationGetGroup(section, sectionBytes);
	if (!group) return defaultValue;
	return EsSystemConfigurationGroupReadInteger(group, key, keyBytes, defaultValue);
}

void SystemConfigurationLoad(char *file, size_t fileBytes) {
	EsINIState s = {};
	s.buffer = file;
	s.bytes = fileBytes;

	EsSystemConfigurationGroup *group = nullptr;

	while (EsINIParse(&s)) {
		if (!s.keyBytes) {
			EsSystemConfigurationGroup _group = {};
			arrput(systemConfigurationGroups, _group);
			group = &arrlast(systemConfigurationGroups);
			group->section = (char *) EsHeapAllocate(s.sectionBytes, false);
			EsMemoryCopy(group->section, s.section, (group->sectionBytes = s.sectionBytes));
			group->sectionClass = (char *) EsHeapAllocate(s.sectionClassBytes, false);
			EsMemoryCopy(group->sectionClass, s.sectionClass, (group->sectionClassBytes = s.sectionClassBytes));
		} else if (group) {
			EsSystemConfigurationItem item = {};
			item.key = (char *) EsHeapAllocate(s.keyBytes, false);
			EsMemoryCopy(item.key, s.key, (item.keyBytes = s.keyBytes));
			item.value = (char *) EsHeapAllocate(s.valueBytes + 1, false);
			item.value[s.valueBytes] = 0;
			EsMemoryCopy(item.value, s.value, (item.valueBytes = s.valueBytes));
			arrput(group->items, item);
			group->itemCount++;
		}
	}

	EsHeapFree(file);
}

EsSystemConfigurationGroup *EsSystemConfigurationReadAll(size_t *groupCount) {
	EsMessageMutexCheck();
	*groupCount = arrlenu(systemConfigurationGroups);
	return systemConfigurationGroups;
}

void EsApplicationStart(const EsApplicationStartupInformation *information) {
	EsApplicationStartupInformation copy = *information;
	if (copy.filePathBytes == -1) copy.filePathBytes = EsCStringLength(copy.filePath);
	size_t bytes = sizeof(EsApplicationStartupInformation) + copy.filePathBytes;
	uint8_t *buffer = (uint8_t *) EsHeapAllocate(bytes, false);
	EsMemoryCopy(buffer, &copy, sizeof(EsApplicationStartupInformation));
	EsMemoryCopy(buffer + sizeof(EsApplicationStartupInformation), copy.filePath, copy.filePathBytes);
	EsHandle constantBuffer = EsConstantBufferCreate(buffer, bytes, ES_CURRENT_PROCESS);
	EsSyscall(ES_SYSCALL_START_PROGRAM, constantBuffer, 0, 0, 0);
	EsHandleClose(constantBuffer);
	EsHeapFree(buffer);
}

EsApplicationStartupInformation *ApplicationStartupInformationParse(const void *data, size_t dataBytes) {
	EsApplicationStartupInformation *startupInformation = (EsApplicationStartupInformation *) data;

	if (sizeof(EsApplicationStartupInformation) <= dataBytes) {
		dataBytes -= sizeof(EsApplicationStartupInformation);
		if ((size_t) startupInformation->filePathBytes > dataBytes) goto error;
		dataBytes -= startupInformation->filePathBytes;
		if (dataBytes) goto error;
		startupInformation->filePath = (const char *) (startupInformation + 1);
	} else {
		error:;
		EsPrint("Warning: received corrupted startup information.\n");
		return nullptr;
	}

	return startupInformation;
}

ApplicationInstance *InstanceSetup(EsInstance *instance) {
	ApplicationInstance *applicationInstance = (ApplicationInstance *) EsHeapAllocate(sizeof(ApplicationInstance), true);

	instance->_private = applicationInstance;

	instance->undoManager = &applicationInstance->undoManager;
	instance->undoManager->instance = instance;
	applicationInstance->activeUndoManager = instance->undoManager;

	EsCommandRegister(&applicationInstance->commandDelete, instance, nullptr, ES_COMMAND_DELETE, "Del");
	EsCommandRegister(&applicationInstance->commandSelectAll, instance, nullptr, ES_COMMAND_SELECT_ALL, "Ctrl+A");
	EsCommandRegister(&applicationInstance->commandCopy, instance, nullptr, ES_COMMAND_COPY, "Ctrl+C|Ctrl+Ins");
	EsCommandRegister(&applicationInstance->commandCut, instance, nullptr, ES_COMMAND_CUT, "Ctrl+X|Shift+Del");
	EsCommandRegister(&applicationInstance->commandPaste, instance, nullptr, ES_COMMAND_PASTE, "Ctrl+V|Shift+Ins");
	EsCommandRegister(&applicationInstance->commandUndo, instance, nullptr, ES_COMMAND_UNDO, "Ctrl+Z");
	EsCommandRegister(&applicationInstance->commandRedo, instance, nullptr, ES_COMMAND_REDO, "Ctrl+Y");

	EsCommandSetCallback(&applicationInstance->commandUndo, [] (EsInstance *instance, EsElement *, EsCommand *) {
		EsUndoInvokeGroup(((ApplicationInstance *) instance->_private)->activeUndoManager, false);
	});

	EsCommandSetCallback(&applicationInstance->commandRedo, [] (EsInstance *instance, EsElement *, EsCommand *) {
		EsUndoInvokeGroup(((ApplicationInstance *) instance->_private)->activeUndoManager, true);
	});

	return applicationInstance;
}

EsInstance *_EsInstanceCreate(size_t bytes, EsMessage *message, const char *applicationName, ptrdiff_t applicationNameBytes) {
	if (applicationNameBytes == -1) {
		applicationNameBytes = EsCStringLength(applicationName);
	}

	EsInstance *instance = (EsInstance *) EsHeapAllocate(bytes, true);
	EsAssert(bytes >= sizeof(EsInstance));
	ApplicationInstance *applicationInstance = InstanceSetup(instance);
	applicationInstance->applicationName = applicationName;
	applicationInstance->applicationNameBytes = applicationNameBytes;

	if (message && message->createInstance.data != ES_INVALID_HANDLE) {
		applicationInstance->startupInformation = (EsApplicationStartupInformation *) EsHeapAllocate(message->createInstance.dataBytes, false);

		if (applicationInstance->startupInformation) {
			EsConstantBufferRead(message->createInstance.data, applicationInstance->startupInformation);

			if (!ApplicationStartupInformationParse(applicationInstance->startupInformation, message->createInstance.dataBytes)) {
				EsHeapFree(applicationInstance->startupInformation);
				applicationInstance->startupInformation = nullptr;
			}
		}
	}

	applicationInstance->mainWindowHandle = message->createInstance.window;
	instance->window = EsWindowCreate(instance, ES_WINDOW_NORMAL);
	EsWindowSetTitle(instance->window, nullptr, 0);

	if (applicationInstance->startupInformation && applicationInstance->startupInformation->filePathBytes) {
		char *documentPath = (char *) EsHeapAllocate(applicationInstance->startupInformation->filePathBytes, false);
		applicationInstance->documentPath = documentPath;
		EsMemoryCopy(documentPath, applicationInstance->startupInformation->filePath, applicationInstance->startupInformation->filePathBytes);
		applicationInstance->documentPathBytes = applicationInstance->startupInformation->filePathBytes;
		instance->documentState = ES_DOCUMENT_STATE_FILE;

		EsMessage m = { ES_MSG_INSTANCE_OPEN };
		m.instanceOpen.instance = instance;
		m.instanceOpen.path = documentPath;
		m.instanceOpen.pathBytes = applicationInstance->documentPathBytes;
		EsMessagePost(nullptr, &m);

		uintptr_t lastSeparator = 0;

		for (uintptr_t i = 0; i < applicationInstance->documentPathBytes; i++) {
			if (documentPath[i] == '/') {
				lastSeparator = i + 1;
			}
		}

		EsWindowSetTitle(instance->window, documentPath + lastSeparator, applicationInstance->documentPathBytes - lastSeparator);
	}

	return instance;
}

#define CHARACTER_MONO     (1) // 1 bit per pixel.
#define CHARACTER_SUBPIXEL (2) // 24 bits per pixel; each byte specifies the alpha of each RGB channel.
#define CHARACTER_IMAGE    (3) // 32 bits per pixel, ARGB.
#define CHARACTER_RECOLOR  (4) // 32 bits per pixel, AXXX.

#include "renderer2.cpp"
#include "theme.cpp"

#define TEXT_RENDERER
#include "text.cpp"
#undef TEXT_RENDERER

#include "gui.cpp"

#ifndef NO_API_TABLE
void *apiTable[] = {
#include <bin/api_array.h>
};
#endif

extern "C" void _init();
typedef void (*StartFunction)();

void EsMessageMutexAcquire() {
	EsMutexAcquire(messageMutex);
	messageMutexThreadID = EsThreadGetID(ES_CURRENT_THREAD);
}

void EsMessageMutexRelease() {
	messageMutexThreadID = 0;
	EsMutexRelease(messageMutex);
}

void EsMessageMutexCheck() {
	EsAssert(messageMutexThreadID == EsThreadGetID(ES_CURRENT_THREAD)); // Expected message mutex to be acquired.
}

int EsMessageSend(EsElement *object, EsMessage *message) {
	int response = 0;

	if (object->userCallback) {
		response = object->userCallback(object, message);
	}

	if (response == 0 && object->classCallback) {
		response = object->classCallback(object, message);
	}

	if (message->type >= ES_MSG_STATE_CHANGE_MESSAGE_START && message->type <= ES_MSG_STATE_CHANGE_MESSAGE_END) {
		((EsElement *) object)->MaybeRefreshStyle();
	}

	return response;
}

struct EsDirectoryMonitor {
	EsHandle handle;
	EsGeneric context;
	EsDirectoryMonitorCallbackFunction callback;
	bool destroyed;
};

void EsInstanceDestroy(EsInstance *_instance) {
	UndoManagerDestroy(_instance->undoManager);

	EsAssert(_instance->window->instance == _instance);
	_instance->window->instance = nullptr;
	EsElementDestroy(_instance->window);

	ApplicationInstance *instance = (ApplicationInstance *) _instance->_private;

	EsHeapFree(instance->startupInformation);
	EsHeapFree(instance->documentPath);

	for (uintptr_t i = 0; i < hmlenu(instance->commands); i++) {
		EsAssert(!arrlenu(instance->commands[i].value->elements));
		arrfree(instance->commands[i].value->elements);
	}

	hmfree(instance->commands);

	EsMessage m = {};
	m.type = ES_MSG_DEALLOCATE_INSTANCE;
	m.instanceDestroy.instance = _instance;
	EsMessagePost(nullptr, &m); 
}

EsMessage *EsMessageReceive() {
	static _EsMessageWithObject message = {};
	EsMessageMutexCheck();

	while (true) {
		TS("Process message\n");

		if (message.message.type == ES_MSG_INSTANCE_CREATE) {
			if (message.message.createInstance.data != ES_INVALID_HANDLE) {
				EsHandleClose(message.message.createInstance.data);
			}
		} else if (message.message.type == ES_MSG_INSTANCE_OPEN) {
			EsUndoClear(message.message.instanceOpen.instance->undoManager);
		}

		EsMessageMutexRelease();

		EsError error = EsSyscall(ES_SYSCALL_GET_MESSAGE, (uintptr_t) &message, 0, 0, 0);

		while (error != ES_SUCCESS) {
			if (!gui.animationSleep && arrlenu(gui.animatingElements)) {
				EsMessageMutexAcquire();
				ProcessAnimations();
				EsMessageMutexRelease();
			} else {
				EsSyscall(ES_SYSCALL_WAIT_MESSAGE, ES_WAIT_NO_TIMEOUT, 0, 0, 0);
			}

			error = EsSyscall(ES_SYSCALL_GET_MESSAGE, (uintptr_t) &message, 0, 0, 0);
		}

		EsMessageMutexAcquire();

		EsMessageType type = message.message.type;

		if (type == ES_MSG_SYSTEM_CONSTANT_UPDATED) {
			esSystemConstants[message.message.systemConstantUpdated.index] = message.message.systemConstantUpdated.newValue;
		}

		if (type == ES_MSG_EYEDROP_REPORT) {
			EsMessageSend((EsElement *) message.object, &message.message);
		} else if (type == ES_MSG_TIMER) {
			((EsTimerCallbackFunction) message.object)((EsGeneric) message.message._argument);
		} else if (type >= ES_MSG_WM_START && type <= ES_MSG_WM_END) {
#if 0
			ProcessMessageTiming timing = {};
			double start = EsTimeStampMs();
			UIProcessWindowManagerMessage((EsWindow *) message.object, &message.message, &timing);
			EsPrint("Processed message from WM %x in %Fms (%Fms logic, %Fms layout, %Fms paint, %Fms update screen).\n", 
					type, EsTimeStampMs() - start, 
					timing.endLogic - timing.startLogic, 
					timing.endLayout - timing.startLayout,
					timing.endPaint - timing.startPaint,
					timing.endUpdate - timing.startUpdate);
#endif
			
			if (message.object) {
				UIProcessWindowManagerMessage((EsWindow *) message.object, &message.message, nullptr);
			}
		} else if (type >= ES_MSG_FS_EVENT_START && type <= ES_MSG_FS_EVENT_END) {
			char *buffer = (char *) EsHeapAllocate(message.message.fsEvent.bufferBytes + 1 /* allow safe manual zero-termination */, false);
			EsConstantBufferRead(message.message.fsEvent.buffer, buffer);
			EsHandleClose(message.message.fsEvent.buffer);
			EsDirectoryMonitor *monitor = (EsDirectoryMonitor *) message.message.fsEvent.context.p;

			if (!monitor->destroyed) {
				monitor->callback(monitor, type, 
						buffer + message.message.fsEvent.pathOffset, message.message.fsEvent.pathBytes, 
						buffer + message.message.fsEvent.oldPathOffset, message.message.fsEvent.oldPathBytes,
						monitor->context);
			}

			EsHeapFree(buffer);
		} else if (type == ES_MSG_FS_MONITOR_DESTROY) {
			EsDirectoryMonitor *monitor = (EsDirectoryMonitor *) message.object;
			EsAssert(monitor->destroyed); // Monitor not destroyed.
			EsHeapFree(monitor);
		} else if (type == ES_MSG_TAB_INSPECT_UI) {
			for (uintptr_t i = 0; i < arrlenu(gui.allWindows); i++) {
				if (EsSyscall(ES_SYSCALL_WINDOW_GET_ID, gui.allWindows[i]->handle, 0, 0, 0) != message.message.tabOperation.id) {
					continue;
				}

				EsInstance *_instance = gui.allWindows[i]->instance;

				if (_instance) {
					ApplicationInstance *instance = (ApplicationInstance *) _instance->_private;

					if (instance->attachedInspector) {
						EsSyscall(ES_SYSCALL_WINDOW_SET_FOCUSED, instance->attachedInspector->window->handle, 0, 0, 0);
					} else {
						EsWindowCreate(_instance, ES_WINDOW_INSPECTOR);
					}
				}

				break;
			}
		} else if (type == ES_MSG_DEALLOCATE_INSTANCE) {
			EsHeapFree(message.message.instanceDestroy.instance->_private);
			EsHeapFree(message.message.instanceDestroy.instance);
		} else {
			return &message.message;
		}
	}
}

uint64_t EsSystemGetConstant(uintptr_t index) {
	return esSystemConstants[index];
}

void ThreadInitialise() {
	// TODO Memory leak: this structure is never freed.
	// 	Perhaps the kernel should send a message when a user thread is terminated to its owner process?

	ThreadLocalStorage *local = (ThreadLocalStorage *) EsHeapAllocate(sizeof(ThreadLocalStorage), true);
	local->id = EsSyscall(ES_SYSCALL_GET_THREAD_ID, ES_CURRENT_THREAD, 0, 0, 0);
	local->self = local;
	EsSyscall(ES_SYSCALL_SET_TLS, (uintptr_t) local, 0, 0, 0);
}

#include "desktop.cpp"

extern "C" void _start(EsProcessStartupInformation *_startupInformation) {
	startupInformation = _startupInformation;
	bool desktop = startupInformation->isDesktop;
	
#ifndef NO_API_TABLE
	if (desktop) {
		// Initialise the API table.

		EsAssert(sizeof(apiTable) <= 0xF000); // API table is too large.
		EsMemoryCopy(ES_API_BASE, apiTable, sizeof(apiTable));
	}
#endif

	{
		// Initialise the API.

		_init();
		EsSyscall(ES_SYSCALL_GET_SYSTEM_CONSTANTS, (uintptr_t) esSystemConstants, 0, 0, 0);
		EsRandomSeed(EsTimeStamp());
	}

	if (desktop) {
		size_t fileSize;
		void *file = EsFileReadAll("/Essence/System Configuration.ini", -1, &fileSize);
		EsAssert(file); 
		SystemConfigurationLoad((char *) file, fileSize);
	} else {
		size_t bytes;
		EsHandle handle = EsSyscall(ES_SYSCALL_SYSTEM_CONFIGURATION_READ, 0, 0, (uintptr_t) &bytes, 0);
		EsAssert(handle);
		char *buffer = (char *) EsHeapAllocate(bytes, false);
		EsConstantBufferRead(handle, buffer);
		EsHandleClose(handle);
		SystemConfigurationLoad(buffer, bytes);
	}

	{
		// Initialise the GUI.

		gui.animationSleepTimer = EsTimerCreate();
		themeScale = esSystemConstants[ES_SYSTEM_CONSTANT_UI_SCALE] / 100.0f;

		size_t pathBytes, fileBytes;
		char *path = EsSystemConfigurationReadString(EsLiteral("general"), EsLiteral("theme"), &pathBytes);
		void *file = EsFileMap(path, pathBytes, &fileBytes, ES_MAP_OBJECT_READ_ONLY);
		EsAssert(ThemeLoadData(file, fileBytes));
		EsHeapFree(path);

		themeCursors.width = ES_THEME_CURSORS_WIDTH;
		themeCursors.height = ES_THEME_CURSORS_HEIGHT;
		themeCursors.stride = ES_THEME_CURSORS_WIDTH * 4;
		themeCursors.bits = EsObjectMap(EsMemoryOpen(themeCursors.height * themeCursors.stride, EsLiteral(ES_THEME_CURSORS_NAME), 0), 
				0, ES_MAP_OBJECT_ALL, ES_MAP_OBJECT_READ_ONLY);
		themeCursors.fullAlpha = true;
		themeCursors.readOnly = true;
	}

	ThreadInitialise();
	EsMessageMutexAcquire();

	if (desktop) {
		DesktopEntry(); 
	} else {
		((StartFunction) startupInformation->applicationStartAddress)();
	}

	EsThreadTerminate(ES_CURRENT_THREAD);
}

void EsAssertionFailure(const char *cFile, int line) {
	EsPrint("Assertion failure at %z:%d.\n", cFile, line);
	EsProcessCrash(ES_FATAL_ERROR_ABORT, EsLiteral("Assertion failure.\n"));
}

void EsUnimplemented() {
	EsAssert(false);
}

void EsCommandAddButton(EsCommand *command, EsButton *button) {
	EsAssert(command->registered); // Command has not been registered.
	arrput(command->elements, button);
	EsButtonOnCommand(button, command->callback, command);
	button->state |= UI_STATE_COMMAND_BUTTON;
	EsElementSetDisabled(button, command->disabled);
}

EsCommand *EsCommandRegister(EsCommand *command, EsInstance *_instance, EsCommandCallbackFunction callback, uint32_t stableID, 
		const char *cDefaultKeyboardShortcut, bool enabled) {
	if (!command) {
		command = (EsCommand *) EsHeapAllocate(sizeof(EsCommand), true);
		command->allocated = true;
	}

	ApplicationInstance *instance = (ApplicationInstance *) _instance->_private;
	command->callback = callback;
	command->registered = true;
	command->stableID = stableID;
	command->cKeyboardShortcut = cDefaultKeyboardShortcut;
	command->disabled = !enabled;
	EsAssert(hmgeti(instance->commands, stableID) == -1); // Command already registered.
	hmput(instance->commands, stableID, command);
	return command;
}

void EsCommandSetDisabled(EsCommand *command, bool disabled) {
	EsAssert(command->registered); // Command has not been registered.

	if (disabled != command->disabled) {
		command->disabled = disabled;

		for (uintptr_t i = 0; i < arrlenu(command->elements); i++) {
			EsElementSetDisabled(command->elements[i], disabled);
		}
	}
}

void EsCommandSetCallback(EsCommand *command, EsCommandCallbackFunction callback) {
	EsAssert(command->registered); // Command has not been registered.

	if (callback != command->callback) {
		command->callback = callback;

		for (uintptr_t i = 0; i < arrlenu(command->elements); i++) {
			if (command->elements[i]->state & UI_STATE_COMMAND_BUTTON) {
				EsButtonOnCommand((EsButton *) command->elements[i], callback, command);
			}
		}
	}

	if (!callback) {
		EsCommandSetDisabled(command, true);
	}
}

EsCommand *EsCommandByID(EsInstance *_instance, uint32_t id) {
	ApplicationInstance *instance = (ApplicationInstance *) _instance->_private;
	EsCommand *command = hmget(instance->commands, id);
	EsAssert(command); // Invalid command ID.
	return command;
}

static EsSpinlock performanceTimerStackLock;
#define PERFORMANCE_TIMER_STACK_SIZE (100)
uint64_t performanceTimerStack[100];
uintptr_t performanceTimerStackCount;

void EsPerformanceTimerPush() {
	EsSpinlockAcquire(&performanceTimerStackLock);

	if (performanceTimerStackCount < PERFORMANCE_TIMER_STACK_SIZE) {
		performanceTimerStack[performanceTimerStackCount++] = EsTimeStamp();
	}

	EsSpinlockRelease(&performanceTimerStackLock);
}

double EsPerformanceTimerPop() {
	double result = 0;
	EsSpinlockAcquire(&performanceTimerStackLock);

	if (performanceTimerStackCount) {
		uint64_t start = performanceTimerStack[--performanceTimerStackCount];
		result = ((double) (EsTimeStamp() - start) / (double) (esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND])) / 1000000.0; 
	}

	EsSpinlockRelease(&performanceTimerStackLock);
	return result;
}

EsDirectoryMonitor *EsDirectoryMonitorCreate(const char *path, ptrdiff_t pathBytes, uint32_t flags, EsDirectoryMonitorCallbackFunction callback, EsGeneric context) {
	EsAssert(flags); // Cannot create a directory monitor without flags.

	_EsNodeInformation directory;
	if (pathBytes == -1) pathBytes = EsCStringLength(path);
	EsError error = EsSyscall(ES_SYSCALL_OPEN_NODE, (uintptr_t) path, pathBytes, ES_NODE_DIRECTORY | ES_NODE_FAIL_IF_NOT_FOUND, (uintptr_t) &directory);
	if (error != ES_SUCCESS) return nullptr;

	EsDirectoryMonitor *monitor = (EsDirectoryMonitor *) EsHeapAllocate(sizeof(EsDirectoryMonitor), true);
	monitor->context = context;
	monitor->callback = callback;
	monitor->handle = EsSyscall(ES_SYSCALL_DIRECTORY_MONITOR, directory.handle, flags, (uintptr_t) monitor, 0);
	EsHandleClose(directory.handle);

	if (ES_CHECK_ERROR(monitor->handle)) {
		EsHeapFree(monitor);
		return nullptr;
	} else {
		return monitor;
	}
}

void EsDirectoryMonitorDestroy(EsDirectoryMonitor *monitor) {
	monitor->destroyed = true;
	EsHandleClose(monitor->handle);
}

#include "icons2.h"

uint32_t EsIconIDFromString(const char *string, ptrdiff_t stringBytes) {
	if (!string) {
		return 0;
	}

	for (uintptr_t i = 0; i < sizeof(iconNames) / sizeof(iconNames[0]); i++) {
		if (0 == EsStringCompareRaw(iconNames[i], -1, string, stringBytes)) {
			return i + 1;
		}
	}

	return 0;
}

struct UndoItemFooter {
	EsUndoCallback callback;
	ptrdiff_t bytes;
	bool endOfGroup;
};

bool EsUndoPeek(EsUndoManager *manager, EsUndoCallback *callback, const void **item) {
	EsMessageMutexCheck();
	uint8_t **stack = manager->state == UNDO_MANAGER_STATE_UNDOING ? &manager->redoStack : &manager->undoStack;

	if (!arrlenu(*stack)) {
		return false;
	}

	UndoItemFooter *footer = (UndoItemFooter *) (*stack + arrlenu(*stack)) - 1;
	*callback = footer->callback;
	*item = (uint8_t *) footer - footer->bytes;
	return true;
}

void EsUndoPop(EsUndoManager *manager) {
	EsMessageMutexCheck();
	EsInstanceSetActiveUndoManager(manager->instance, manager);
	uint8_t **stack = manager->state == UNDO_MANAGER_STATE_UNDOING ? &manager->redoStack : &manager->undoStack;
	size_t oldLength = arrlenu(*stack);
	EsAssert(oldLength);
	UndoItemFooter *footer = (UndoItemFooter *) (*stack + arrlenu(*stack)) - 1;
	EsMessage m = {};
	m.type = ES_MSG_UNDO_CANCEL;
	footer->callback((uint8_t *) footer - footer->bytes, manager, &m);
	arrsetlen(*stack, oldLength - footer->bytes - sizeof(UndoItemFooter));
	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_UNDO), !arrlenu(manager->undoStack));
	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_REDO), !arrlenu(manager->redoStack));
}

void EsUndoClear(EsUndoManager *manager) {
	uint8_t **stack = manager->state == UNDO_MANAGER_STATE_UNDOING ? &manager->redoStack : &manager->undoStack;

	while (arrlenu(*stack)) {
		EsUndoPop(manager);
	}
}

void EsUndoPush(EsUndoManager *manager, EsUndoCallback callback, const void *item, size_t itemBytes) {
	EsMessageMutexCheck();
	EsInstanceSetActiveUndoManager(manager->instance, manager);

	uint8_t **stack = manager->state == UNDO_MANAGER_STATE_UNDOING ? &manager->redoStack : &manager->undoStack;

	UndoItemFooter footer = {};
	footer.callback = callback;
	footer.bytes = (itemBytes + sizeof(uintptr_t) - 1) & ~(sizeof(uintptr_t) - 1);

	size_t oldLength = arrlenu(*stack);
	arrsetlen(*stack, footer.bytes + sizeof(footer) + oldLength);
	EsMemoryCopy(*stack + oldLength, item, itemBytes);
	EsMemoryCopy(*stack + oldLength + footer.bytes, &footer, sizeof(footer));

	if (manager->state == UNDO_MANAGER_STATE_NORMAL) {
		// Clear the redo stack.
		manager->state = UNDO_MANAGER_STATE_UNDOING;
		EsUndoClear(manager);
		manager->state = UNDO_MANAGER_STATE_NORMAL;
	}

	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_UNDO), !arrlenu(manager->undoStack));
	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_REDO), !arrlenu(manager->redoStack));
}

void EsUndoContinueGroup(EsUndoManager *manager) {
	EsMessageMutexCheck();
	EsAssert(manager->state == UNDO_MANAGER_STATE_NORMAL);
	uint8_t *stack = manager->undoStack;
	if (!arrlenu(stack)) return;
	UndoItemFooter *footer = (UndoItemFooter *) (stack + arrlenu(stack)) - 1;
	footer->endOfGroup = false;
}

void EsUndoEndGroup(EsUndoManager *manager) {
	EsMessageMutexCheck();
	EsAssert(manager->state == UNDO_MANAGER_STATE_NORMAL);
	uint8_t *stack = manager->undoStack;
	if (!arrlenu(stack)) return;
	UndoItemFooter *footer = (UndoItemFooter *) (stack + arrlenu(stack)) - 1;
	footer->endOfGroup = true;
}

void EsUndoInvokeGroup(EsUndoManager *manager, bool redo) {
	EsMessageMutexCheck();
	EsInstanceSetActiveUndoManager(manager->instance, manager);
	EsAssert(manager->state == UNDO_MANAGER_STATE_NORMAL);
	manager->state = redo ? UNDO_MANAGER_STATE_REDOING : UNDO_MANAGER_STATE_UNDOING;

	uint8_t **stack = redo ? &manager->redoStack : &manager->undoStack;
	EsAssert(arrlenu(*stack));
	bool first = true;

	while (arrlenu(*stack)) {
		size_t oldLength = arrlenu(*stack);
		UndoItemFooter *footer = (UndoItemFooter *) (*stack + oldLength) - 1;

		if (!first && footer->endOfGroup) break;
		first = false;

		EsMessage m = {};
		m.type = ES_MSG_UNDO_INVOKE;
		footer->callback((uint8_t *) footer - footer->bytes, manager, &m);
		arrsetlen(*stack, oldLength - footer->bytes - sizeof(UndoItemFooter));
	}

	{
		uint8_t *stack = redo ? manager->undoStack : manager->redoStack;
		EsAssert(arrlenu(stack));
		UndoItemFooter *footer = (UndoItemFooter *) (stack + arrlenu(stack)) - 1;
		footer->endOfGroup = true;
	}

	manager->state = UNDO_MANAGER_STATE_NORMAL;

	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_UNDO), !arrlenu(manager->undoStack));
	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_REDO), !arrlenu(manager->redoStack));
}

bool EsUndoInUndo(EsUndoManager *manager) {
	EsMessageMutexCheck();
	return manager->state != UNDO_MANAGER_STATE_NORMAL;
}

void UndoManagerDestroy(EsUndoManager *manager) {
	EsMessageMutexCheck();

	EsAssert(manager->state == UNDO_MANAGER_STATE_NORMAL);
	EsUndoClear(manager);
	manager->state = UNDO_MANAGER_STATE_UNDOING;
	EsUndoClear(manager);
	arrfree(manager->undoStack);
	arrfree(manager->redoStack);
	manager->state = UNDO_MANAGER_STATE_NORMAL;

	if (((ApplicationInstance *) manager->instance->_private)->activeUndoManager == manager) {
		EsInstanceSetActiveUndoManager(manager->instance, manager->instance->undoManager);
	}
}

void EsInstanceSetActiveUndoManager(EsInstance *_instance, EsUndoManager *manager) {
	EsMessageMutexCheck();
	ApplicationInstance *instance = (ApplicationInstance *) _instance->_private;

	if (instance->activeUndoManager == manager) {
		return;
	}

	EsUndoEndGroup(instance->activeUndoManager);
	instance->activeUndoManager = manager;

	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_UNDO), !arrlenu(manager->undoStack));
	EsCommandSetDisabled(EsCommandByID(manager->instance, ES_COMMAND_REDO), !arrlenu(manager->redoStack));
}
