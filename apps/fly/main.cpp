// TODO Icon.
// TODO Music.
// TODO Draw() with rotation.

#include <essence.h>

#include "embed.h"
#include "embed_def.h"

EsInstance *instance;
uint32_t backgroundColor;
int musicIndex;
bool keysPressedSpace, keysPressedEscape, keysPressedX;
uint32_t previousControllerButtons[ES_GAME_CONTROLLER_MAX_COUNT];
uint32_t *targetBits;
size_t targetWidth, targetHeight, targetStride;
double updateTimeAccumulator;
float gameScale;
uint32_t gameOffsetX, gameOffsetY, gameWidth, gameHeight;

#define GAME_SIZE (380)
#define BRAND "fly"

struct Texture { 
	uint32_t width, height; 
	uint32_t *bits;
};

void Transform(float *destination, float *left, float *right) {
	float d[6];
	d[0] = left[0] * right[0] + left[1] * right[3];
	d[1] = left[0] * right[1] + left[1] * right[4];
	d[2] = left[0] * right[2] + left[1] * right[5] + left[2];
	d[3] = left[3] * right[0] + left[4] * right[3];
	d[4] = left[3] * right[1] + left[4] * right[4];
	d[5] = left[3] * right[2] + left[4] * right[5] + left[5];
	destination[0] = d[0];
	destination[1] = d[1];
	destination[2] = d[2];
	destination[3] = d[3];
	destination[4] = d[4];
	destination[5] = d[5];
}

__attribute__((optimize("-O2"))) 
void Draw(Texture *texture, 
		float x, float y, float w = -1, float h = -1, 
		float sx = 0, float sy = 0, float sw = -1, float sh = -1, 
		float _a = 1, float _r = 1, float _g = 1, float _b = 1,
		float rot = 0) {
	(void) rot;

	if (sw == -1 && sh == -1) sw = texture->width, sh = texture->height;
	if (w == -1 && h == -1) w = sw, h = sh;
	if (_a <= 0) return;
	
	if (x + w < 0 || y + h < 0 || x > GAME_SIZE || y > GAME_SIZE) return;

	x *= gameScale, y *= gameScale, w *= gameScale, h *= gameScale;

	float m[6] = {};
	float m1[6] = { sw / w, 0, 0, 0, sh / h, 0 };
	float m2[6] = { 1, 0, -x * (sw / w), 0, 1, -y * (sh / h) };

	Transform(m, m2, m1);

	intptr_t yStart = y - 1, yEnd = y + h + 1, xStart = x - 1, xEnd = x + w + 1;
	// if (rot) yStart -= h * 0.45f, yEnd += h * 0.45f, xStart -= w * 0.45f, xEnd += w * 0.45f;
	if (yStart < 0) yStart = 0; 
	if (yStart > gameHeight) yStart = gameHeight;
	if (yEnd < 0) yEnd = 0; 
	if (yEnd > gameHeight) yEnd = gameHeight;
	if (xStart < 0) xStart = 0; 
	if (xStart > gameWidth) xStart = gameWidth;
	if (xEnd < 0) xEnd = 0; 
	if (xEnd > gameWidth) xEnd = gameWidth;
	m[2] += m[0] * xStart, m[5] += m[3] * xStart;

	uint32_t *scanlineStart = targetBits + (gameOffsetY + yStart) * targetStride / 4 + (gameOffsetX + xStart);

	uint32_t r = _r * 255, g = _g * 255, b = _b * 255, a = _a * 255;

	for (intptr_t y = yStart; y < yEnd; y++, scanlineStart += targetStride / 4) {
		uint32_t *output = scanlineStart;

		float tx = m[1] * y + m[2];
		float ty = m[4] * y + m[5];

		for (intptr_t x = xStart; x < xEnd; x++, output++, tx += m[0], ty += m[3]) {
			if (tx + sx < 0 || ty + sy < 0) continue;
			uintptr_t txi = tx + sx, tyi = ty + sy;
			if (txi < sx || tyi < sy || txi >= sx + sw || tyi >= sy + sh) continue;
			uint32_t modified = texture->bits[tyi * texture->width + txi];
			if (!(modified & 0xFF000000)) continue;
			uint32_t original = *output;
			uint32_t alpha1 = (((modified & 0xFF000000) >> 24) * a) >> 8;
			uint32_t alpha2 = (255 - alpha1) << 8;
			uint32_t r2 = alpha2 * ((original & 0x000000FF) >> 0);
			uint32_t g2 = alpha2 * ((original & 0x0000FF00) >> 8);
			uint32_t b2 = alpha2 * ((original & 0x00FF0000) >> 16);
			uint32_t r1 = b * alpha1 * ((modified & 0x00FF0000) >> 16);
			uint32_t g1 = g * alpha1 * ((modified & 0x0000FF00) >> 8);
			uint32_t b1 = r * alpha1 * ((modified & 0x000000FF) >> 0);
			*output = 0xFF000000 | (0x00FF0000 & ((b1 + b2) << 0)) 
				| (0x0000FF00 & ((g1 + g2) >> 8)) 
				| (0x000000FF & ((r1 + r2) >> 16));
		}
	}
}

void CreateTexture(Texture *texture, uint8_t *data, size_t dataBytes) {
	texture->bits = (uint32_t *) EsImageLoad(data, dataBytes, &texture->width, &texture->height, 4);
	EsAssert(texture->bits);
}

void ExitGame() {
	EsInstanceDestroy(instance);
}

#include "game.cpp"

int ProcessCanvasMessage(EsElement *element, EsMessage *message) {
	if (message->type == ES_MSG_ANIMATE) {
		message->animate.complete = false;
		updateTimeAccumulator += message->animate.deltaUs / 1000000.0;

		while (updateTimeAccumulator > 1 / 60.0) {
			{
				EsGameControllerState state[ES_GAME_CONTROLLER_MAX_COUNT];
				size_t count = EsGameControllerStatePoll(state);

				for (uintptr_t i = 0; i < count; i++) {
					if (state[i].buttons & (1 << 0) && (~previousControllerButtons[i] & (1 << 0))) {
						keysPressedSpace = true;
					} else if (state[i].buttons & (1 << 1) && (~previousControllerButtons[i] & (1 << 1))) {
						keysPressedX = true;
					}

					previousControllerButtons[i] = state[i].buttons;
				}
			}

			UpdateGame();
			updateTimeAccumulator -= 1 / 60.0;
			keysPressedSpace = keysPressedEscape = keysPressedX = false;
		}

		EsElementRepaint(element, true, {});
	} else if (message->type == ES_MSG_KEY_DOWN && !message->keyboard.repeat) {
		if (message->keyboard.scancode == ES_SCANCODE_SPACE || message->keyboard.scancode == ES_SCANCODE_Z) {
			keysPressedSpace = true;
		} else if (message->keyboard.scancode == ES_SCANCODE_ESCAPE) {
			keysPressedEscape = true;
		} else if (message->keyboard.scancode == ES_SCANCODE_X) {
			keysPressedX = true;
		}
	} else if (message->type == ES_MSG_PAINT) {
		EsPainter *painter = message->painter;
		EsPaintTargetStartDirectAccess(painter->target, &targetBits, nullptr, nullptr, &targetStride);
		targetBits = (uint32_t *) ((uint8_t *) targetBits + targetStride * painter->offsetY + 4 * painter->offsetX);
		targetWidth = painter->width, targetHeight = painter->height;

		gameScale = (float) painter->width / GAME_SIZE;
		if (gameScale * GAME_SIZE > painter->height) gameScale = (float) painter->height / GAME_SIZE;
		if (gameScale > 1) gameScale = EsCRTfloorf(gameScale);
		gameWidth = GAME_SIZE * gameScale, gameHeight = GAME_SIZE * gameScale;
		gameOffsetX = painter->width / 2 - gameWidth / 2;
		gameOffsetY = painter->height / 2 - gameHeight / 2;

		// TODO Clear margins.

		Draw(&textureWhite, 0, 0, GAME_SIZE, GAME_SIZE, 0, 0, 1, 1, 1, 
				((backgroundColor >> 16) & 0xFF) / 255.0f, 
				((backgroundColor >> 8) & 0xFF) / 255.0f, 
				((backgroundColor >> 0) & 0xFF) / 255.0f);

		RenderGame();

		EsPaintTargetEndDirectAccess(painter->target);
	}

	return 0;
}

void _start() {
	_init();

	while (true) {
		EsMessage *message = EsMessageReceive();

		if (message->type == ES_MSG_INSTANCE_CREATE) {
			instance = EsInstanceCreate(message, BRAND);
			EsWindow *window = instance->window;
			EsPanel *container = EsPanelCreate(window, ES_CELL_FILL, ES_STYLE_PANEL_WINDOW_DIVIDER);
			EsElement *canvas = EsCustomElementCreate(container, ES_CELL_FILL | ES_ELEMENT_FOCUSABLE, {});
			canvas->userCallback = ProcessCanvasMessage;
			EsElementStartAnimating(canvas);
			EsElementFocus(canvas);

			InitEmbed();
			InitialiseGame();
		}
	}
}
