@ECHO OFF

IF NOT EXIST bin\res\ (
	MKDIR bin\res
	XCOPY /E res bin\res
)

SET CFLAGS=%* -g -fno-exceptions -Ibin -I. -DARCH_X86_64 -DARCH_X86_COMMON -DARCH_64 -DNO_API_TABLE -DUSE_PLATFORM_HEAP -DUSE_HARFBUZZ -DUSE_FREETYPE -DUSE_STB_SPRINTF -Wno-empty-body -Wno-deprecated-declarations -Wno-unknown-pragmas -Wno-unknown-attributes -Wno-c99-designator
SET LINKFLAGS=/DEBUG /NOLOGO /NODEFAULTLIB /IGNORE:4217 /subsystem:windows /OPT:REF /OPT:ICF /STACK:0x100000,0x100000
SET APPFLAGS=-D_start=_ApplicationStart -DES_FORWARD -DES_EXTERN_FORWARD=ES_EXTERN_C -DES_DIRECT_API

clang -o bin\header_generator.exe util\header_generator.c %CFLAGS%
bin\header_generator c bin\essence.h

clang++ -o bin\desktop.o -c desktop\api.cpp -DOS_ESSENCE %CFLAGS%
clang++ -o bin\w32.o -c util\w32\platform.cpp %CFLAGS%
clang++ -o bin\test.o -c util\w32\test.cpp %APPFLAGS% %CFLAGS%
link %LINKFLAGS% /OUT:bin\desktop.exe bin\desktop.o bin\w32.o bin\test.o bin\freetype.lib bin\harfbuzz.lib kernel32.lib user32.lib gdi32.lib
