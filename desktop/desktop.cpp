// TODO Tabs:
//	- Dragging out of the window.
//	- Dragging onto other windows.
//	- Keyboard shortcuts.
//	- New tab page - search; recent files.
//	- Closing tabs.
// 	- Right click menu.
// 	- Duplicate tabs.
// 	- Activating a tab doesn't cause it to clear it ctrl/shift/alt flags.
// 	- If a process exits, its tabs won't close because Desktop has a handle.

// TODO Graphical issues:
// 	- New tab button isn't flush with right border when tab band full.
// 	- Cursor doesn't update after switching embed window owners.
// 	- Closing tabs isn't animating.
// 	- Inactivate windows don't dim outline around tabs.
// 	- Resizing windows doesn't redraw old shadow sometimes.

// TODO Task bar:
// 	- Right click menu.
// 	- Notification area.

// TODO Desktop experience:
// 	- Handling force quit for non-responding applications.
// 	- Alt+tab.
// 	- Clipboard.
// 	- Setting wallpaper.
//
// TODO Global shortcuts:
// 	- Opening new windows.
// 	- Closing tabs and windows.
// 	- Restoring closed tabs.
// 	- Switch to window.
// 	- Maximising, hiding and docking on left/right windows.
// 	- Print screen.

// TODO Maybe extend tab hitbox to top of window when maximised? Might need a visual change too.
// TODO Restarting Desktop if it crashes.

#define MSG_SETUP_DESKTOP_UI ((EsMessageType) (ES_MSG_USER_START + 1))

struct InstalledApplication {
	char *cName;
	char *cExecutable;
	int64_t id;
	uint32_t iconID;
	bool hidden, useSingleProcess;

	EsHandle singleProcessHandle; 
};

DS_MAP(char *, InstalledApplication) installedApplications;

struct ReorderItem : EsElement {
	double sizeProgress, sizeTarget;
	double offsetProgress, offsetTarget;
	bool dragging;
	int dragOffset, dragPosition;
};

struct ReorderList : EsElement {
	int targetWidth, extraWidth;
	ReorderItem **itemsByDepth;
};

struct CrashedTabInstance : EsInstance {
};

struct BlankTabInstance : EsInstance {
};

struct WindowTab : ReorderItem {
	uint32_t iconID;
	uint64_t processID;
	uint64_t embeddedWindowID;
	EsHandle embeddedWindowHandle;
	char title[128];
	size_t titleBytes;
	struct ContainerWindow *container;
	int tabIndex;
	CrashedTabInstance *crashedTabInstance;
	
	bool notResponding;
	EsHandle restoreEmbeddedWindowHandle;
	uint64_t restoreEmbeddedWindowID;
};

struct WindowTabBand : ReorderList {
	uint16_t additionalElements, tabCount;
	struct ContainerWindow *container;
	bool preventNextTabSizeAnimation;
};

struct TaskBarButton : ReorderItem {
	ContainerWindow *containerWindow;
};

struct TaskList : ReorderList {
};

struct TaskBar : EsElement {
	double enterProgress;
	EsRectangle targetBounds;
	TaskList taskList;
};

struct ContainerWindow {
	WindowTabBand *tabBand;
	TaskBarButton *taskBarButton;
	EsWindow *window;
	int activeTab;
};

const EsStyle styleNewTabContent = {
	.metrics = {
		.mask = ES_THEME_METRICS_INSETS | ES_THEME_METRICS_GAP_MAJOR,
		.insets = ES_MAKE_RECTANGLE(50, 50, 50, 50),
		.gapMajor = 30,
	},
};

const EsStyle styleButtonGroupContainer = {
	.inherit = ES_STYLE_BUTTON_GROUP_CONTAINER,

	.metrics = {
		.mask = ES_THEME_METRICS_PREFERRED_WIDTH,
		.preferredWidth = 400,
	},
};

DS_ARRAY(WindowTab *) allWindowTabs;
DS_ARRAY(ContainerWindow *) allContainerWindows;
TaskBar taskBar;
EsWindow *wallpaperWindow;
bool shutdownWindowOpen;

#define INSTALLATION_STATE_NONE (0)
#define INSTALLATION_STATE_INSTALLER (1)
int installationState;

WindowTab *WindowTabFind(uint64_t windowID, bool remove = false) {
	for (uintptr_t i = 0; i < arrlenu(allWindowTabs); i++) {
		if (allWindowTabs[i]->embeddedWindowID == windowID || (allWindowTabs[i]->restoreEmbeddedWindowID == windowID && allWindowTabs[i]->notResponding)) {
			if (remove) {
				arrdel(allWindowTabs, i);
			}

			return allWindowTabs[i];
		}
	}

	return nullptr;
}

void WindowTabActivate(WindowTab *tab) {
	if (tab->container->activeTab != tab->tabIndex) {
		tab->container->activeTab = tab->tabIndex;
		EsElementRelayout(tab->container->tabBand);
		tab->container->taskBarButton->Repaint(true);

		// EsPrint("Activating tab %d...\n", tab->tabIndex);
		EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED, tab->container->window->handle, (uintptr_t) tab->embeddedWindowHandle, 0, 0);
	}
}

WindowTab *StartInstanceOfApplication(const char *cName, EsHandle data, size_t dataBytes, ContainerWindow *container);

int ProcessContainerWindowMessage(EsElement *element, EsMessage *message) {
	ContainerWindow *container = (ContainerWindow *) element->userData.p;

	if (message->type == ES_MSG_WINDOW_ACTIVATED) {
		container->taskBarButton->customStyleState |= THEME_STATE_SELECTED;
		container->taskBarButton->MaybeRefreshStyle();
	} else if (message->type == ES_MSG_WINDOW_DEACTIVATED) {
		container->taskBarButton->customStyleState &= ~THEME_STATE_SELECTED;
		container->taskBarButton->MaybeRefreshStyle();
	} else if (message->type == ES_MSG_KEY_DOWN) {
		bool ctrl = message->keyboard.ctrl, alt = message->keyboard.alt, shift = message->keyboard.shift;
		int scancode = message->keyboard.scancode;

		if (ctrl && !alt && message->keyboard.scancode == ES_SCANCODE_TAB) {
			int tab = container->activeTab + (shift ? -1 : 1);
			if (tab == -1) tab = container->tabBand->tabCount - 1;
			if (tab == container->tabBand->tabCount) tab = 0;
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + tab));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_T) {
			StartInstanceOfApplication("Desktop.BlankTab", ES_INVALID_HANDLE, 0, container);
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_1 && container->tabBand->tabCount >= 1) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 0));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_2 && container->tabBand->tabCount >= 2) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 1));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_3 && container->tabBand->tabCount >= 3) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 2));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_4 && container->tabBand->tabCount >= 4) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 3));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_5 && container->tabBand->tabCount >= 5) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 4));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_6 && container->tabBand->tabCount >= 6) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 5));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_7 && container->tabBand->tabCount >= 7) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 6));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_8 && container->tabBand->tabCount >= 8) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 7));
		} else if (ctrl && !shift && !alt && scancode == ES_SCANCODE_9 && container->tabBand->tabCount >= 9) {
			WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 8));
		}
	} else if (message->type == ES_MSG_WINDOW_RESIZED) {
		container->tabBand->preventNextTabSizeAnimation = true;
	}

	return 0;
}

bool ReorderItemAnimate(ReorderItem *item, uint64_t deltaUs) {
	item->sizeProgress += (item->sizeTarget - item->sizeProgress) 
		* (1 - EsCRTexp(deltaUs * -0.003 / GetConstantNumber("taskBarButtonEntranceDuration")));
	item->offsetProgress += (item->offsetTarget - item->offsetProgress) 
		* (1 - EsCRTexp(deltaUs * -0.003 / GetConstantNumber("taskBarButtonMoveDuration")));

	bool complete = true;

	if (EsCRTfabs(item->sizeTarget - item->sizeProgress) < 1.5) {
		item->sizeProgress = item->sizeTarget;
	} else {
		complete = false;
	}

	if (EsCRTfabs(item->offsetTarget - item->offsetProgress) < 1.5) {
		item->offsetProgress = item->offsetTarget;
	} else {
		complete = false;
	}

	EsElementRelayout(item->parent);
	return complete;
}

bool ReorderItemDragged(ReorderItem *item, int mouseX, size_t additionalElements) {
	ReorderList *list = (ReorderList *) item->parent;
	size_t childCount = list->GetChildCount() - additionalElements;

	if (!item->dragging) {
		item->dragOffset = gui.lastClickX - item->offsetX;

		for (uintptr_t i = 0; i < arrlenu(list->itemsByDepth); i++) {
			if (list->itemsByDepth[i] == item) {
				arrdel(list->itemsByDepth, i);
				arrput(list->itemsByDepth, item);
				break;
			}
		}
	}

	item->dragPosition = mouseX + item->GetWindowBounds().l - item->dragOffset;

	int draggedIndex = (item->dragPosition + list->targetWidth / 2) / list->targetWidth;
	if (draggedIndex < 0) draggedIndex = 0;
	if (draggedIndex >= (int) childCount) draggedIndex = childCount - 1;
	int currentIndex = -1;

	for (uintptr_t i = 0; i < childCount; i++) {
		if (list->GetChild(i + additionalElements) == item) {
			currentIndex = i;
			break;
		}
	}

	EsAssert(currentIndex != -1);

	bool changed = false;

	if (draggedIndex != currentIndex) {
		arrdel(list->children, currentIndex + additionalElements);
		arrins(list->children, draggedIndex + additionalElements, item);

		for (uintptr_t i = 0, x = list->currentStyle->insets.l; i < childCount; i++) {
			ReorderItem *child = (ReorderItem *) list->GetChild(i + additionalElements);

			if ((int) i != draggedIndex) {
				int oldPosition = child->offsetX, newPosition = x + child->offsetProgress;

				if (child->offsetProgress != oldPosition - newPosition) {
					child->offsetProgress = oldPosition - newPosition;
					child->StartAnimating();
				}
			}

			x += child->sizeProgress;
		}

		changed = true;
	}

	item->dragging = true;
	EsElementRelayout(item->parent);

	return changed;
}

void ReorderItemDragComplete(ReorderItem *item, size_t additionalElements) {
	if (!item->dragging) {
		return;
	}

	ReorderList *list = (ReorderList *) item->parent;

	for (uintptr_t i = 0, x = list->currentStyle->insets.l; i < list->GetChildCount() - additionalElements; i++) {
		if (list->GetChild(i + additionalElements) == item) {
			int oldPosition = item->offsetX, newPosition = x + item->offsetProgress;

			if (item->offsetProgress != oldPosition - newPosition) {
				item->offsetProgress = oldPosition - newPosition;
				item->StartAnimating();
			}
		}

		x += item->sizeTarget;
	}

	item->dragging = false;
}

int ProcessTaskBarButtonMessage(EsElement *element, EsMessage *message) {
	TaskBarButton *button = (TaskBarButton *) element;

	if (message->type == ES_MSG_PAINT) {
		ContainerWindow *containerWindow = button->containerWindow;
		WindowTab *tab = (WindowTab *) containerWindow->tabBand->GetChild(containerWindow->activeTab 
				+ containerWindow->tabBand->additionalElements);
		EsPainterDrawStandardContent(message->painter, tab->title, tab->titleBytes, tab->iconID);
	} else if (message->type == ES_MSG_CLICKED) {
		if (button->customStyleState & THEME_STATE_SELECTED) {
			EsSyscall(ES_SYSCALL_WINDOW_MOVE, button->containerWindow->window->handle, 0, 0, ES_MOVE_WINDOW_HIDDEN);
		} else {
			EsSyscall(ES_SYSCALL_WINDOW_SET_FOCUSED, button->containerWindow->window->handle, 0, 0, 0);
		}
	} else if (message->type == ES_MSG_ANIMATE) {
		message->animate.complete = ReorderItemAnimate(button, message->animate.deltaUs);
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		ReorderItemDragged(button, message->mouseDragged.newPositionX, 0);
		return ES_HANDLED;
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP) {
		ReorderItemDragComplete(button, 0);
	}

	return 0;
}

int ProcessWindowTabMessage(EsElement *element, EsMessage *message) {
	WindowTab *tab = (WindowTab *) element;
	WindowTabBand *band = (WindowTabBand *) tab->parent;

	if (message->type == ES_MSG_PRESSED_START) {
		for (uintptr_t i = 0; i < arrlenu(band->itemsByDepth); i++) {
			if (band->itemsByDepth[i] == tab) {
				arrdel(band->itemsByDepth, i);
				arrput(band->itemsByDepth, tab);
				break;
			}
		}

		WindowTabActivate(tab);
	} else if (message->type == ES_MSG_HIT_TEST) {
		EsRectangle bounds = tab->GetBounds();

		if (message->hitTest.x <= 12) {
			message->hitTest.inside = (bounds.b - message->hitTest.y) * 14 < message->hitTest.x * bounds.b;
		} else if (message->hitTest.x > bounds.r - 12) {
			message->hitTest.inside = (bounds.b - message->hitTest.y) * 14 < (bounds.r - message->hitTest.x) * bounds.b;
		}
	} else if (message->type == ES_MSG_PAINT) {
		EsPainterDrawStandardContent(message->painter, tab->title, tab->titleBytes, tab->iconID);
	} else if (message->type == ES_MSG_ANIMATE) {
		message->animate.complete = ReorderItemAnimate(tab, message->animate.deltaUs);
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		if (band->tabCount == 1) {
			EsPoint screenPosition = EsMouseGetPosition();
			ChangeWindowBounds(RESIZE_MOVE, screenPosition.x, screenPosition.y, gui.lastClickX, gui.lastClickY, band->window);
		} else {
			bool isActive = band->container->activeTab == tab->tabIndex;

			if (ReorderItemDragged(tab, message->mouseDragged.newPositionX, band->additionalElements)) {
				for (uint16_t i = 0; i < band->tabCount; i++) {
					WindowTab *tab = (WindowTab *) band->GetChild(i + band->additionalElements);
					tab->tabIndex = i;
				}

				if (isActive) band->container->activeTab = tab->tabIndex;
			}
		}

		EsElementSetDisabled(band->GetChild(0), true);
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP) {
		ReorderItemDragComplete(tab, band->additionalElements);
		EsElementSetDisabled(band->GetChild(0), false);
	} else if (message->type == ES_MSG_MOUSE_RIGHT_UP) {
		EsMenu *menu = EsMenuCreate(tab, ES_FLAGS_DEFAULT);

		// TODO.
		EsMenuAddItem(menu, ES_FLAGS_DEFAULT, INTERFACE_STRING(DesktopCloseTab));

		if (EsKeyboardIsShiftHeld() && !tab->crashedTabInstance && !tab->notResponding && tab->processID) {
			EsMenuAddSeparator(menu);

			EsMenuAddItem(menu, ES_FLAGS_DEFAULT, INTERFACE_STRING(DesktopInspectUI), [] (EsMenu *, EsGeneric context) {
				WindowTab *tab = (WindowTab *) context.p;

				if (tab->crashedTabInstance || tab->notResponding) {
					return;
				}

				EsMessage m = { ES_MSG_TAB_INSPECT_UI };
				m.tabOperation.id = tab->embeddedWindowID;
				EsHandle process = EsProcessOpen(tab->processID); 

				if (process) {
					EsMessagePostRemote(process, &m);
					EsHandleClose(process);
				}
			}, tab);
		}

		EsMenuShow(menu);
	} else {
		return 0;
	}

	return ES_HANDLED;
}

WindowTab *NewWindowTab(ContainerWindow *container) {
	WindowTab *tab = (WindowTab *) EsHeapAllocate(sizeof(WindowTab), true);
	tab->container = container;
	tab->tabIndex = container->tabBand->tabCount;
	tab->title[0] = ' ';
	tab->titleBytes = 1;
	tab->Initialise(container->tabBand, ES_CELL_H_SHRINK | ES_CELL_V_BOTTOM, ProcessWindowTabMessage, nullptr);
	tab->cName = "window tab";
	arrput(allWindowTabs, tab);
	container->tabBand->tabCount++;
	return tab;
}

int ProcessTaskBarWindowMessage(EsElement *, EsMessage *message) {
	if (message->type == ES_MSG_ANIMATE) {
		taskBar.enterProgress += (1 - taskBar.enterProgress) 
			* (1 - EsCRTexp(message->animate.deltaUs * -0.003 / GetConstantNumber("taskBarEntranceDuration")));

		if (EsCRTfabs(1 - taskBar.enterProgress) < 0.001) {
			taskBar.enterProgress = 1;
			message->animate.complete = true;
		} else {
			message->animate.complete = false;
		}

		EsRectangle bounds = taskBar.targetBounds;
		bounds = Translate(bounds, 0, Height(bounds) * (1 - taskBar.enterProgress));
		EsSyscall(ES_SYSCALL_WINDOW_MOVE, taskBar.window->handle, (uintptr_t) &bounds, 0, ES_MOVE_WINDOW_UPDATE_SCREEN);
	}

	return 0;
}

int ReorderListLayout(ReorderList *list, int additionalRightMargin, size_t additionalElements, bool clampDraggedItem, bool preventTabSizeAnimation) {
	EsRectangle bounds = list->GetBounds();
	bounds.l += list->currentStyle->insets.l;
	bounds.r -= list->currentStyle->insets.r;
	bounds.r -= additionalRightMargin;

	size_t childCount = list->GetChildCount() - additionalElements;

	if (!childCount) {
		return 0;
	}

	int totalWidth = 0;

	for (uintptr_t i = 0; i < childCount; i++) {
		ReorderItem *child = (ReorderItem *) list->GetChild(i + additionalElements);
		totalWidth += child->currentStyle->metrics->maximumWidth + list->currentStyle->metrics->gapMinor;
	}

	bool widthClamped = false;

	if (totalWidth > Width(bounds)) {
		totalWidth = Width(bounds);
		widthClamped = true;
	}

	int targetWidth = totalWidth / childCount;
	int extraWidth = totalWidth % childCount;

	list->targetWidth = targetWidth;
	list->extraWidth = extraWidth;

	for (uintptr_t i = 0; i < childCount; i++) {
		ReorderItem *child = (ReorderItem *) list->GetChild(i + additionalElements);

		int sizeTarget = targetWidth;
		if (extraWidth) sizeTarget++, extraWidth--;

		if (preventTabSizeAnimation) {
			child->sizeTarget = child->sizeProgress = sizeTarget;
		}

		if (child->sizeTarget != sizeTarget) {
			child->sizeTarget = sizeTarget;
			child->StartAnimating();
		}
	}

	int x = bounds.l;

	for (uintptr_t i = 0; i < childCount; i++) {
		ReorderItem *child = (ReorderItem *) list->GetChild(i + additionalElements);
		int width = (i == childCount - 1 && widthClamped) ? (totalWidth - x) : child->sizeProgress;
		int gap = list->currentStyle->metrics->gapMinor;

		if (child->dragging) {
			int p = child->dragPosition;

			if (clampDraggedItem) {
				if (p + width > bounds.r) p = bounds.r - width;
				if (p < bounds.l) p = bounds.l;
			}

			child->InternalMove(width - gap, Height(bounds), p, 0);
		} else {
			child->InternalMove(width - gap, Height(bounds), x + child->offsetProgress, 0);
		}

		x += width;
	}

	return x;
}

int ProcessTaskListMessage(EsElement *element, EsMessage *message) {
	ReorderList *list = (ReorderList *) element;

	if (message->type == ES_MSG_LAYOUT) {
		ReorderListLayout(list, 0, 0, false, false);
	} else if (message->type == ES_MSG_Z_ORDER) {
		EsAssert(message->zOrder.index < arrlenu(list->itemsByDepth));
		message->zOrder.child = list->itemsByDepth[message->zOrder.index];
	} else if (message->type == ES_MSG_ADD_CHILD) {
		arrput(list->itemsByDepth, (ReorderItem *) message->child);
	} else if (message->type == ES_MSG_REMOVE_CHILD) {
		bool found = false;

		for (uintptr_t i = 0; i < arrlenu(list->itemsByDepth); i++) {
			if (list->itemsByDepth[i] == message->child) {
				arrdel(list->itemsByDepth, i);
				found = true;
				break;
			}
		}

		EsAssert(found);
	}

	return 0;
}

int ProcessWindowTabBandMessage(EsElement *element, EsMessage *message) {
	WindowTabBand *band = (WindowTabBand *) element;

	if (message->type == ES_MSG_LAYOUT) {
		for (uint16_t i = 0; i < band->tabCount; i++) {
			WindowTab *tab = (WindowTab *) band->GetChild(i + band->additionalElements);
			tab->SetStylePart(i == band->container->activeTab ? ES_STYLE_WINDOW_TAB_ACTIVE : ES_STYLE_WINDOW_TAB_INACTIVE);
			tab->tabIndex = i;

			if (i == band->container->activeTab) {
				for (uintptr_t i = 0; i < arrlenu(band->itemsByDepth); i++) {
					if (band->itemsByDepth[i] == tab) {
						arrdel(band->itemsByDepth, i);
						arrput(band->itemsByDepth, tab);
						break;
					}
				}
			}
		}

		int x = ReorderListLayout(band, band->GetChild(0)->currentStyle->preferredWidth + 10, 
				band->additionalElements, true, band->preventNextTabSizeAnimation);
		band->GetChild(0)->InternalMove(band->GetChild(0)->currentStyle->preferredWidth, 
				band->GetChild(0)->currentStyle->preferredHeight, x + 10, 4);
		band->preventNextTabSizeAnimation = false;
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		EsPoint screenPosition = EsMouseGetPosition();
		ChangeWindowBounds(RESIZE_MOVE, screenPosition.x, screenPosition.y, gui.lastClickX, gui.lastClickY, band->window);
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP) {
		if (band->window->restoreOnNextMove) {
			band->window->resetPositionOnNextMove = true;
		}
	} else if (message->type == ES_MSG_Z_ORDER) {
		EsAssert(message->zOrder.index < arrlenu(band->itemsByDepth));
		message->zOrder.child = band->itemsByDepth[message->zOrder.index];
	} else if (message->type == ES_MSG_ADD_CHILD) {
		arrput(band->itemsByDepth, (ReorderItem *) message->child);
	} else if (message->type == ES_MSG_REMOVE_CHILD) {
		bool found = false;

		for (uintptr_t i = 0; i < arrlenu(band->itemsByDepth); i++) {
			if (band->itemsByDepth[i] == message->child) {
				arrdel(band->itemsByDepth, i);
				found = true;
				break;
			}
		}

		EsAssert(found);
	} else {
		return 0;
	}

	return ES_HANDLED;
}

int ProcessTaskBarMessage(EsElement *element, EsMessage *message) {
	if (message->type == ES_MSG_LAYOUT) {
		EsRectangle bounds = element->GetBounds();
		element->GetChild(0)->InternalMove(Width(bounds), Height(bounds), 0, 0);
	}

	return 0;
}

ContainerWindow *NewContainerWindow();

#define CRASHED_TAB_FATAL_ERROR (1)
#define CRASHED_TAB_PROGRAM_NOT_FOUND (2)
#define CRASHED_TAB_INVALID_EXECUTABLE (3)
#define CRASHED_TAB_NOT_RESPONDING (4)

void NewCrashedTabInstance(int reason, WindowTab *tab) {
	EsMessage m = {};
	m.type = ES_MSG_INSTANCE_CREATE;
	m.createInstance.window = EsSyscall(ES_SYSCALL_WINDOW_CREATE, ES_WINDOW_NORMAL, 0, 0, 0);
	CrashedTabInstance *instance = (CrashedTabInstance *) _EsInstanceCreate(sizeof(CrashedTabInstance), &m, nullptr);
	instance->window->toolbarFillMode = true;
	if (reason != CRASHED_TAB_NOT_RESPONDING) EsWindowSetIcon(instance->window, ES_ICON_DIALOG_ERROR);
	EsElement *toolbar = EsWindowGetToolbar(instance->window);
	EsPanel *panel = EsPanelCreate(toolbar, ES_CELL_V_CENTER | ES_CELL_V_PUSH | ES_CELL_H_SHRINK | ES_CELL_H_PUSH | ES_PANEL_VERTICAL, ES_STYLE_PANEL_CRASH_INFO);

	if (reason == CRASHED_TAB_FATAL_ERROR) {
		EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(DesktopCrashedApplication));
	} else if (reason == CRASHED_TAB_PROGRAM_NOT_FOUND) {
		EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(DesktopNoSuchApplication));
		EsWindowSetTitle(instance->window, INTERFACE_STRING(CommonErrorTitle));
	} else if (reason == CRASHED_TAB_INVALID_EXECUTABLE) {
		EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(DesktopApplicationStartupError));
		EsWindowSetTitle(instance->window, INTERFACE_STRING(CommonErrorTitle));
	} else if (reason == CRASHED_TAB_NOT_RESPONDING) {
		EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(DesktopNotResponding));
		EsButtonCreate(panel, ES_CELL_H_RIGHT, ES_STYLE_PUSH_BUTTON_DANGEROUS, INTERFACE_STRING(DesktopForceQuit));
	}

	tab->embeddedWindowHandle = m.createInstance.window;
	tab->embeddedWindowID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, tab->embeddedWindowHandle, 0, 0, 0);
	tab->crashedTabInstance = instance;
}

EsHandle NewBlankTabInstance();

void StartInstanceOfApplicationInExistingTab(const char *cName, EsHandle data, size_t dataBytes, WindowTab *tab) {
	if (tab->restoreEmbeddedWindowHandle) {
		EsHandleClose(tab->restoreEmbeddedWindowHandle);
		tab->restoreEmbeddedWindowHandle = ES_INVALID_HANDLE;
	}

	if (0 == EsCRTstrcmp(cName, "Desktop.BlankTab")) {
		tab->embeddedWindowHandle = NewBlankTabInstance();
		tab->embeddedWindowID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, tab->embeddedWindowHandle, 0, 0, 0);
		return;
	}

	ptrdiff_t index = shgeti(installedApplications, cName);

	if (index == -1) {
		NewCrashedTabInstance(CRASHED_TAB_PROGRAM_NOT_FOUND, tab);
		return;
	}

	InstalledApplication *application = &installedApplications[index].value;

	if (application->useSingleProcess && application->singleProcessHandle) {
		EsProcessState state;
		EsProcessGetState(application->singleProcessHandle, &state); 

		if (state.flags & (ES_PROCESS_STATE_ALL_THREADS_TERMINATED | ES_PROCESS_STATE_TERMINATING | ES_PROCESS_STATE_CRASHED)) {
			EsHandleClose(application->singleProcessHandle);
			application->singleProcessHandle = ES_INVALID_HANDLE;
		}
	}

	EsHandle process = application->singleProcessHandle;

	if (!application->useSingleProcess || process == ES_INVALID_HANDLE) {
		EsProcessInformation information;

		if (!ES_CHECK_ERROR(EsProcessCreate(application->cExecutable, -1, &information, nullptr))) {
			process = information.handle;
			EsHandleClose(information.mainThread.handle);
		} else {
			NewCrashedTabInstance(CRASHED_TAB_INVALID_EXECUTABLE, tab);
			return;
		}
	}

	if (application->useSingleProcess) {
		application->singleProcessHandle = process;
	}

	tab->processID = EsProcessGetID(process);

	EsMessage m = { ES_MSG_INSTANCE_CREATE };
	if (dataBytes) m.createInstance.data = EsConstantBufferShare(data, process);
	m.createInstance.dataBytes = dataBytes;
	tab->embeddedWindowHandle = EsSyscall(ES_SYSCALL_WINDOW_CREATE, ES_WINDOW_NORMAL, 0, 0, 0);
	tab->embeddedWindowID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, tab->embeddedWindowHandle, 0, 0, 0);
	m.createInstance.window = EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED_OWNER, tab->embeddedWindowHandle, process, 0, 0);
	EsMessagePostRemote(process, &m);

	if (!application->useSingleProcess) {
		EsHandleClose(process);
	}
}

EsHandle NewBlankTabInstance() {
	EsMessage m = {};
	m.type = ES_MSG_INSTANCE_CREATE;
	m.createInstance.window = EsSyscall(ES_SYSCALL_WINDOW_CREATE, ES_WINDOW_NORMAL, 0, 0, 0);
	EsInstance *instance = _EsInstanceCreate(sizeof(BlankTabInstance), &m, nullptr);
	EsWindowSetTitle(instance->window, INTERFACE_STRING(DesktopNewTabTitle));
	EsPanel *windowBackground = EsPanelCreate(instance->window, ES_CELL_FILL, ES_STYLE_PANEL_WINDOW_BACKGROUND);
	EsPanel *content = EsPanelCreate(windowBackground, ES_CELL_FILL | ES_PANEL_V_SCROLL_AUTO, &styleNewTabContent);

	{
		// Installed applications list.

		EsPanel *buttonGroup = EsPanelCreate(content, ES_PANEL_VERTICAL | ES_CELL_H_SHRINK, &styleButtonGroupContainer);

		buttonGroup->separatorStylePart = ES_STYLE_BUTTON_GROUP_SEPARATOR;
		buttonGroup->separatorFlags = ES_CELL_H_FILL;

		InstalledApplication **sortedApplications = (InstalledApplication **) EsHeapAllocate(sizeof(InstalledApplication *) * shlenu(installedApplications), false);

		for (uintptr_t i = 0; i < shlenu(installedApplications); i++) {
			sortedApplications[i] = &installedApplications[i].value;
		}

		EsSort(sortedApplications, shlenu(installedApplications), sizeof(InstalledApplication *), [] (const void *_left, const void *_right, EsGeneric) {
			InstalledApplication *left = *(InstalledApplication **) _left;
			InstalledApplication *right = *(InstalledApplication **) _right;
			return EsCRTstrcmp(left->cName, right->cName);
		}, 0);

		for (uintptr_t i = 0; i < shlenu(installedApplications); i++) {
			InstalledApplication *application = sortedApplications[i];

			if (application->hidden) {
				continue;
			}

			EsButton *button = EsButtonCreate(buttonGroup, ES_CELL_H_FILL, ES_STYLE_BUTTON_GROUP_ITEM, application->cName);

			button->userData = (void *) application->cName,
			
			EsButtonSetIcon(button, (EsStandardIcon) application->iconID ?: ES_ICON_APPLICATION_DEFAULT_ICON);

			EsButtonOnCommand(button, [] (EsInstance *, EsElement *element, EsCommand *) {
				uint64_t tabID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, element->window->handle, 0, 0, 0);
				WindowTab *tab = WindowTabFind(tabID);
				StartInstanceOfApplicationInExistingTab((const char *) element->userData.p, ES_INVALID_HANDLE, 0, tab);
				EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED, tab->container->window->handle, (uintptr_t) tab->embeddedWindowHandle, 0, 0);
				EsInstanceDestroy(element->instance);
			});
		}
	}

	return m.createInstance.window;
}

WindowTab *StartInstanceOfApplication(const char *cName, EsHandle data, size_t dataBytes, ContainerWindow *container) {
	WindowTab *tab = NewWindowTab(container ?: NewContainerWindow());
	StartInstanceOfApplicationInExistingTab(cName, data, dataBytes, tab);
	WindowTabActivate(tab);
	return tab;
}

ContainerWindow *NewContainerWindow() {
	ContainerWindow *container = (ContainerWindow *) EsHeapAllocate(sizeof(ContainerWindow), true);
	container->window = EsWindowCreate(nullptr, ES_WINDOW_CONTAINER);
	arrput(allContainerWindows, container);
	container->activeTab = -1;
	container->window->userCallback = ProcessContainerWindowMessage;
	container->window->userData = container;

	container->tabBand = (WindowTabBand *) EsHeapAllocate(sizeof(WindowTabBand), true);
	container->tabBand->container = container;
	container->tabBand->Initialise(container->window, ES_CELL_FILL, ProcessWindowTabBandMessage, ES_STYLE_WINDOW_TAB_BAND);
	container->tabBand->cName = "window tab band";
	container->tabBand->additionalElements = 1;

	EsButton *newTabButton = EsButtonCreate(container->tabBand, ES_FLAGS_DEFAULT, ES_STYLE_PUSH_BUTTON_WINDOW_NEW_TAB);
	
	EsButtonOnCommand(newTabButton, [] (EsInstance *, EsElement *element, EsCommand *) {
		StartInstanceOfApplication("Desktop.BlankTab", ES_INVALID_HANDLE, 0, (ContainerWindow *) element->window->userData.p);
	});

	container->taskBarButton = (TaskBarButton *) EsHeapAllocate(sizeof(TaskBarButton), true);
	container->taskBarButton->customStyleState = THEME_STATE_SELECTED;
	container->taskBarButton->containerWindow = container;
	container->taskBarButton->Initialise(&taskBar.taskList, ES_CELL_FILL, 
			ProcessTaskBarButtonMessage, ES_STYLE_TASK_BAR_BUTTON);
	container->taskBarButton->cName = "task bar button";

	return container;
}

void CheckForegroundWindowResponding(EsGeneric handle) {
	EsTimerSet(handle.u, 2500, CheckForegroundWindowResponding, handle); 

	for (uintptr_t i = 0; i < arrlenu(allWindowTabs); i++) {
		WindowTab *tab = allWindowTabs[i];

		if ((tab->container->taskBarButton->customStyleState & THEME_STATE_SELECTED) 
				&& tab->container->activeTab == tab->tabIndex
				&& tab->processID) {
			EsHandle process = EsProcessOpen(tab->processID); 

			if (process) {
				EsProcessState state;
				EsProcessGetState(process, &state);

				if (state.flags & ES_PROCESS_STATE_PINGED) {
					if (tab->notResponding) {
						// Tab is already in a non-responding state.
					} else {
						tab->notResponding = true;
						tab->restoreEmbeddedWindowHandle = tab->embeddedWindowHandle;
						tab->restoreEmbeddedWindowID = tab->embeddedWindowID;
						NewCrashedTabInstance(CRASHED_TAB_NOT_RESPONDING, tab);
						EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED, tab->container->window->handle, tab->embeddedWindowHandle, 0, 0);
					}
				} else {
					if (tab->notResponding) {
						tab->notResponding = false;
						tab->embeddedWindowHandle = tab->restoreEmbeddedWindowHandle;
						tab->restoreEmbeddedWindowHandle = ES_INVALID_HANDLE;
						tab->embeddedWindowID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, tab->embeddedWindowHandle, 0, 0, 0);
						EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED, tab->container->window->handle, tab->embeddedWindowHandle, 0, 0);
						tab->Repaint(true);
						tab->container->taskBarButton->Repaint(true);
						EsAssert(tab->crashedTabInstance);
						EsInstanceDestroy(tab->crashedTabInstance);
						tab->crashedTabInstance = nullptr;
					} else {
						EsMessage m;
						EsMemoryZero(&m, sizeof(EsMessage));
						m.type = ES_MSG_PING;
						EsMessagePostRemote(process, &m); 
					}
				}

				EsHandleClose(process);
			}

			return;
		}
	}
}

struct WaveRIFFHeader {
	uint32_t magic;
	uint32_t size;
	uint32_t type;
};

struct WaveChunkHeader {
	uint32_t magic;
	uint32_t size;
};

struct WaveFormatChunk {
	uint16_t formatTag;
	uint16_t channels;
	uint32_t sampleRate;
	uint32_t bytesPerSecond;
	uint16_t blockAlign;
	uint16_t bitsPerSample;
};

void PlaySound(const char *path, size_t pathBytes) {
	size_t fileSize;
	uint8_t *file = (uint8_t *) EsFileReadAll(path, pathBytes, &fileSize);
	if (!file) return;
	EsDefer(EsHeapFree(file));
	if (fileSize > 64 * 1024 * 1024) return;

	WaveRIFFHeader *riffHeader = (WaveRIFFHeader *) file;
	if (riffHeader->magic != 0x46464952 || riffHeader->type != 0x45564157) return;

	WaveFormatChunk *format = nullptr;
	uint8_t *data = nullptr;
	uintptr_t position = sizeof(WaveRIFFHeader);
	size_t dataBytes = 0;

	while (position < fileSize && position + sizeof(WaveChunkHeader) <= fileSize) {
		WaveChunkHeader *chunk = (WaveChunkHeader *) (file + position);

		if (chunk->size > fileSize - position || chunk->size + sizeof(WaveChunkHeader) > fileSize - position) {
			break;
		}

		if (chunk->magic == 0x20746D66 && chunk->size == sizeof(WaveFormatChunk)) {
			format = (WaveFormatChunk *) (chunk + 1);
		} else if (chunk->magic == 0x61746164) {
			data = (uint8_t *) (chunk + 1);
			dataBytes = chunk->size;
		}

		position += chunk->size + sizeof(WaveChunkHeader);
	}

	if (!data || !format) return;

	size_t bytesPerSourceSample = format->bitsPerSample == 8 ? 1
		: format->bitsPerSample == 16 ? 2
		: format->bitsPerSample == 32 ? 4 : 0;

	if (format->sampleRate < 8000 || format->sampleRate > 1000000 
			|| format->channels < 1 || format->channels > 8 
			|| format->formatTag != 1 
			|| !bytesPerSourceSample) {
		return;
	}

	EsAudioStream *stream = EsAudioStreamOpen(ES_AUDIO_DEFAULT_OUTPUT, 50 * 1000 /* 50 ms */);
	if (!stream) return;

	double sourceLength = (double) (dataBytes / bytesPerSourceSample / format->channels) / (double) format->sampleRate;
	double currentTime = 0;

	EsAudioStream sourceStream = { 
		.format = {
			.sampleRate = format->sampleRate,
			.sampleFormat = (uint8_t) (format->bitsPerSample == 8 ? ES_SAMPLE_FORMAT_U8 
				: format->bitsPerSample == 16 ? ES_SAMPLE_FORMAT_S16LE : ES_SAMPLE_FORMAT_S32LE),
			.channels = (uint8_t) format->channels,
		},

		.buffer = data,
		.bufferBytes = (uint32_t) dataBytes,
		.writePointer = (uint32_t) dataBytes,
	};

	while (currentTime < sourceLength) {
		if (ES_ERROR_SOURCE_EMPTY == EsAudioStreamSend(stream, &sourceStream, &currentTime)) {
			break;
		}

		stream->control = ES_AUDIO_STREAM_RUNNING;
		EsAudioStreamNotify(stream);
		EsWaitSingle(stream->handle);
	}

	stream->control = 0;
	EsAudioStreamNotify(stream);
	EsAudioStreamClose(stream);
}

void SetupDesktopUI() {
	{
		// Get the installation state.
		installationState = EsSystemConfigurationReadInteger(EsLiteral("general"), EsLiteral("installation_state"));
	}

	{
		// Load the theme bitmap.

		EsHandle handle = EsMemoryOpen(ES_THEME_CURSORS_WIDTH * ES_THEME_CURSORS_HEIGHT * 4, EsLiteral(ES_THEME_CURSORS_NAME), ES_FLAGS_DEFAULT); 
		void *destination = EsObjectMap(handle, 0, ES_THEME_CURSORS_WIDTH * ES_THEME_CURSORS_HEIGHT * 4, ES_MAP_OBJECT_READ_WRITE);
		LoadImage(themeSystem.buffer + themeSystem.byteCount - themeHeader->bitmapBytes, themeHeader->bitmapBytes, 
				destination, ES_THEME_CURSORS_WIDTH, ES_THEME_CURSORS_HEIGHT, true);
		EsObjectUnmap(destination);
		EsHandleClose(handle);
	}

	{
		// Generate the window frame bitmap.

		UIStyleKey key = MakeStyleKey(ES_STYLE_CONTAINER_WINDOW_ACTIVE, 0);
		UIStyle *style = GetStyle(key, false);

		int each = style->preferredWidth;

		EsPaintTarget target = {};
		target.width = each * 4;
		target.height = style->preferredHeight;
		target.stride = target.width * 4;
		target.fullAlpha = true;
		target.bits = EsHeapAllocate(target.stride * target.height, true);

		EsPainter painter = {};
		painter.clip = ES_MAKE_RECTANGLE(0, target.width, 0, target.height);
		painter.target = &target;

		style->PaintLayers(&painter, each, target.height, 0, 0, THEME_CHILD_TYPE_NONE, false);
		style->CloseReference(key);

		key = MakeStyleKey(ES_STYLE_CONTAINER_WINDOW_ACTIVE_MAXIMISED, 0);
		style = GetStyle(key, false);
		style->PaintLayers(&painter, each, target.height, 1 * each, 0, THEME_CHILD_TYPE_NONE, false);
		style->CloseReference(key);

		key = MakeStyleKey(ES_STYLE_CONTAINER_WINDOW_INACTIVE, 0);
		style = GetStyle(key, false);
		style->PaintLayers(&painter, each, target.height, 2 * each, 0, THEME_CHILD_TYPE_NONE, false);
		style->CloseReference(key);

		key = MakeStyleKey(ES_STYLE_CONTAINER_WINDOW_INACTIVE_MAXIMISED, 0);
		style = GetStyle(key, false);
		style->PaintLayers(&painter, each, target.height, 3 * each, 0, THEME_CHILD_TYPE_NONE, false);
		style->CloseReference(key);

		EsSyscall(ES_SYSCALL_SET_WINDOW_FRAME_BITMAP, 0, (uintptr_t) target.bits, 0, target.stride);
		EsHeapFree(target.bits);
	}
	
	{
		// Create the wallpaper window.

		wallpaperWindow = EsWindowCreate(nullptr, ES_WINDOW_PLAIN);
		wallpaperWindow->isSpecial = true;
		EsRectangle screen;
		EsSyscall(ES_SYSCALL_GET_SCREEN_BOUNDS, 0, (uintptr_t) &screen, 0, 0);
		EsSyscall(ES_SYSCALL_WINDOW_MOVE, wallpaperWindow->handle, (uintptr_t) &screen, 0, ES_MOVE_WINDOW_AT_BOTTOM);
		EsSyscall(ES_SYSCALL_WINDOW_SET_OPAQUE_BOUNDS, wallpaperWindow->handle, (uintptr_t) &screen, 0, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, wallpaperWindow->handle, ES_FLAGS_DEFAULT, 0, 0);
		wallpaperWindow->windowWidth = Width(screen);
		wallpaperWindow->windowHeight = Height(screen);

#ifndef SIMPLE_GRAPHICS
		EsThreadCreate([] (EsGeneric) {
			size_t pathBytes;
			char *path = EsSystemConfigurationReadString(EsLiteral("general"), EsLiteral("wallpaper"), &pathBytes);

			if (path) {
				void *buffer = EsHeapAllocate(wallpaperWindow->windowWidth * wallpaperWindow->windowHeight * 4, false);
				LoadImage(path, pathBytes, buffer, wallpaperWindow->windowWidth, wallpaperWindow->windowHeight, false);
				EsRectangle region = { 0, (int32_t) wallpaperWindow->windowWidth, 0, (int32_t) wallpaperWindow->windowHeight };
				EsSyscall(ES_SYSCALL_WINDOW_SET_BITS, wallpaperWindow->handle, (uintptr_t) &region, (uintptr_t) buffer, 0);
				EsHeapFree(buffer);
				EsHeapFree(path);
			}

			// TODO Fade wallpaper in.
		}, nullptr, 0);
#endif
	}

	if (installationState == INSTALLATION_STATE_NONE) {
		// Create the taskbar.

		EsWindow *window = EsWindowCreate(nullptr, ES_WINDOW_PLAIN);
		window->userCallback = ProcessTaskBarWindowMessage;
		window->StartAnimating();
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_WINDOW_SOLID_TRUE | ES_WINDOW_SOLID_NO_ACTIVATE, 0, 0);

		taskBar.Initialise(window, ES_CELL_FILL, ProcessTaskBarMessage, ES_STYLE_TASK_BAR_BAR);
		taskBar.cName = "task bar";
		EsThemeMetrics metrics = EsElementGetMetrics(&taskBar);
		window->userData = &taskBar;

		EsRectangle screen;
		EsSyscall(ES_SYSCALL_GET_SCREEN_BOUNDS, 0, (uintptr_t) &screen, 0, 0);

		EsRectangle bounds = screen;
		bounds.t = bounds.b - metrics.preferredHeight;
		taskBar.targetBounds = bounds;

		screen.b = bounds.t;
		EsSyscall(ES_SYSCALL_SET_SCREEN_WORK_AREA, 0, (uintptr_t) &screen, 0, 0);

		bounds.r -= bounds.l, bounds.b -= bounds.t;
		bounds.l = bounds.t = 0;
		EsSyscall(ES_SYSCALL_WINDOW_SET_BLUR_BOUNDS, window->handle, (uintptr_t) &bounds, 0, 0);

		EsPanel *panel = EsPanelCreate(&taskBar, ES_PANEL_HORIZONTAL | ES_CELL_FILL, {});

		EsButton *newWindowButton = EsButtonCreate(panel, ES_FLAGS_DEFAULT, ES_STYLE_TASK_BAR_NEW_WINDOW);

		EsButtonOnCommand(newWindowButton, [] (EsInstance *, EsElement *, EsCommand *) {
			StartInstanceOfApplication("Desktop.BlankTab", ES_INVALID_HANDLE, 0, nullptr);
		});

		taskBar.taskList.Initialise(panel, ES_CELL_FILL, ProcessTaskListMessage, nullptr);
		taskBar.taskList.cName = "task list";
	}

	{
		// Launch the first application.

		char *firstApplication = EsSystemConfigurationReadString(EsLiteral("general"), EsLiteral("first_application"));

		if (firstApplication && firstApplication[0] != '-') {
			StartInstanceOfApplication(firstApplication, ES_INVALID_HANDLE, 0, nullptr);
			EsHeapFree(firstApplication);
		}
	}

	{
		// Setup the timer callback to check if the foreground window is responding.

		EsHandle handle = EsTimerCreate();
		EsTimerSet(handle, 2500, CheckForegroundWindowResponding, handle); 
	}

	if (installationState == INSTALLATION_STATE_NONE) {
		// Play the startup sound.

		EsThreadCreate([] (EsGeneric) {
			size_t pathBytes;
			char *path = EsSystemConfigurationReadString(EsLiteral("general"), EsLiteral("startup_sound"), &pathBytes);

			if (path) {
				PlaySound(path, pathBytes);
				EsHeapFree(path);
			}
		}, nullptr, 0);
	}

	if (installationState == INSTALLATION_STATE_INSTALLER) {
		// Start the installer.

		EsProcessCreate(EsLiteral("/Applications/Installer/Entry.esx"), nullptr, 0); 
	}
}

void NewShutdownModal() {
	if (shutdownWindowOpen) {
		return;
	}

	shutdownWindowOpen = true;

	// Setup the window.

	EsWindow *window = EsWindowCreate(nullptr, ES_WINDOW_PLAIN);
	EsRectangle screen;
	EsSyscall(ES_SYSCALL_GET_SCREEN_BOUNDS, 0, (uintptr_t) &screen, 0, 0);
	EsSyscall(ES_SYSCALL_WINDOW_MOVE, window->handle, (uintptr_t) &screen, 0, ES_MOVE_WINDOW_ALWAYS_ON_TOP);
	EsSyscall(ES_SYSCALL_WINDOW_SET_BLUR_BOUNDS, window->handle, (uintptr_t) &screen, 0, 0);
	EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_WINDOW_SOLID_TRUE, 0, 0);
	EsSyscall(ES_SYSCALL_WINDOW_SET_FOCUSED, window->handle, 0, 0, 0);

	// Setup the UI.

	EsPanel *stack = EsPanelCreate(window, ES_CELL_FILL | ES_PANEL_Z_STACK, ES_STYLE_PANEL_NORMAL_WINDOW_ROOT);
	stack->cName = "window stack";
	EsPanelCreate(stack, ES_CELL_FILL, ES_STYLE_PANEL_SHUTDOWN_OVERLAY)->cName = "modal overlay";
	EsPanel *dialog = EsPanelCreate(stack, ES_PANEL_VERTICAL | ES_CELL_CENTER, ES_STYLE_PANEL_DIALOG_ROOT);
	dialog->cName = "dialog";
	EsPanel *heading = EsPanelCreate(dialog, ES_PANEL_HORIZONTAL | ES_CELL_H_FILL, ES_STYLE_DIALOG_HEADING);
	EsIconDisplayCreate(heading, ES_FLAGS_DEFAULT, {}, ES_ICON_SYSTEM_SHUTDOWN);
	EsTextDisplayCreate(heading, ES_CELL_H_FILL | ES_CELL_V_CENTER, ES_STYLE_TEXT_HEADING2, 
			INTERFACE_STRING(DesktopShutdownTitle))->cName = "dialog heading";
	EsTextDisplayCreate(EsPanelCreate(dialog, ES_PANEL_VERTICAL | ES_CELL_H_FILL, ES_STYLE_DIALOG_CONTENT), 
			ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(DesktopConfirmShutdown))->cName = "dialog contents";
	EsPanel *buttonArea = EsPanelCreate(dialog, ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_CELL_H_FILL, ES_STYLE_DIALOG_BUTTON_AREA);
	EsButton *cancelButton = EsButtonCreate(buttonArea, ES_BUTTON_DEFAULT, 0, INTERFACE_STRING(CommonCancel));
	EsButton *shutdownButton = EsButtonCreate(buttonArea, ES_FLAGS_DEFAULT, ES_STYLE_PUSH_BUTTON_DANGEROUS, INTERFACE_STRING(DesktopShutdownAction));
	EsElementFocus(cancelButton);

	// Setup command callbacks when the buttons are pressed.

	EsButtonOnCommand(shutdownButton, [] (EsInstance *, EsElement *, EsCommand *) {
		EsSyscall(ES_SYSCALL_SHUTDOWN, 0, 0, 0, 0);
	});

	EsButtonOnCommand(cancelButton, [] (EsInstance *, EsElement *element, EsCommand *) {
		EsElementDestroy(element->window);
		shutdownWindowOpen = false;
	});
}

void DesktopEntry() {
	for (uintptr_t i = 0; i < arrlenu(systemConfigurationGroups); i++) {
		// Load information about installed applications.

		EsSystemConfigurationGroup *group = systemConfigurationGroups + i;

		if (0 == EsStringCompareRaw(group->sectionClass, group->sectionClassBytes, EsLiteral("application"))) {
			InstalledApplication application = {};
			application.cName = EsSystemConfigurationGroupReadString(group, EsLiteral("name"));
			size_t installationFolderBytes = 0, executableBytes = 0;
			char *icon = EsSystemConfigurationGroupReadString(group, EsLiteral("icon"));
			application.iconID = EsIconIDFromString(icon);
			EsHeapFree(icon);
			char *installationFolder = EsSystemConfigurationGroupReadString(group, EsLiteral("installation_folder"), &installationFolderBytes);
			char *executable = EsSystemConfigurationGroupReadString(group, EsLiteral("executable"), &executableBytes);
			application.cExecutable = (char *) EsHeapAllocate(installationFolderBytes + executableBytes + 2, false);
			EsMemoryCopy(application.cExecutable, installationFolder, installationFolderBytes);
			application.cExecutable[installationFolderBytes] = '/';
			EsMemoryCopy(application.cExecutable + installationFolderBytes + 1, executable, executableBytes);
			application.cExecutable[installationFolderBytes + executableBytes + 1] = 0;
			EsHeapFree(installationFolder);
			EsHeapFree(executable);
			application.useSingleProcess = EsSystemConfigurationGroupReadInteger(group, EsLiteral("use_single_process"), true);
			application.hidden = EsSystemConfigurationGroupReadInteger(group, EsLiteral("hidden"), false);
			application.id = EsIntegerParse(group->section, group->sectionBytes);
			shput(installedApplications, (char *) application.cName, application);
		}
	}

	{
		// Set the system configuration for other applications to read.

		// TODO Enforce a limit of 4MB on the size of the system configuration.
		const size_t bufferSize = 4194304;
		char *buffer = (char *) EsHeapAllocate(bufferSize, false);
		size_t position = 0;

		for (uintptr_t i = 0; i < arrlenu(systemConfigurationGroups); i++) {
			EsSystemConfigurationGroup *group = systemConfigurationGroups + i;

			bool inGeneralSection = 0 == EsStringCompareRaw(group->section, group->sectionBytes, EsLiteral("general"));

			if (EsStringCompareRaw(group->sectionClass, group->sectionClassBytes, EsLiteral("font"))
					&& EsStringCompareRaw(group->sectionClass, group->sectionClassBytes, EsLiteral("file_type"))
					&& !inGeneralSection) {
				continue;
			}

			EsINIState s = {};
			s.sectionClass = group->sectionClass, s.sectionClassBytes = group->sectionClassBytes;
			s.section = group->section, s.sectionBytes = group->sectionBytes;
			position += EsINIFormat(&s, buffer + position, bufferSize - position);

			for (uintptr_t i = 0; i < arrlenu(group->items); i++) {
				EsSystemConfigurationItem *item = group->items + i;

				if (inGeneralSection && EsStringCompareRaw(item->key, item->keyBytes, EsLiteral("theme"))
						&& EsStringCompareRaw(item->key, item->keyBytes, EsLiteral("icon_pack"))) {
					continue;
				}

				if (!item->keyBytes || item->key[0] == ';') {
					continue;
				}

				s.key = item->key, s.keyBytes = item->keyBytes;
				s.value = item->value, s.valueBytes = item->valueBytes;
				position += EsINIFormat(&s, buffer + position, bufferSize - position);
			}
		}

		EsSyscall(ES_SYSCALL_SYSTEM_CONFIGURATION_WRITE, (uintptr_t) buffer, position, 0, 0);
		EsHeapFree(buffer);
	}

	EsMessage m = { MSG_SETUP_DESKTOP_UI };
	EsMessagePost(nullptr, &m);

	while (true) {
		EsMessage *message = EsMessageReceive();

		if (message->type == ES_MSG_POWER_BUTTON_PRESSED) {
			NewShutdownModal();
		} else if (message->type == ES_MSG_EMBEDDED_WINDOW_DESTROYED) {
			EsMenuCloseAll();
			WindowTab *tab = WindowTabFind(message->windowMetadata.id, true /* remove if found */);

			if (tab) {
				ContainerWindow *container = tab->container;
				EsHandleClose(tab->embeddedWindowHandle);
				if (tab->restoreEmbeddedWindowHandle) EsHandleClose(tab->restoreEmbeddedWindowHandle);
				tab->embeddedWindowID = 0;
				container->tabBand->tabCount--;

				if (!container->tabBand->tabCount) {
					EsElementDestroy(container->window);
					EsElementDestroy(container->taskBarButton);

					bool found = false;
					
					for (uintptr_t i = 0; i < arrlenu(allContainerWindows); i++) {
						if (allContainerWindows[i] == container) {
							arrdel(allContainerWindows, i);
							found = true;
							break;
						}
					}

					EsAssert(found);
				} else {
					if (container->tabBand->GetChild(container->tabBand->additionalElements + container->activeTab) == tab) {
						if (container->activeTab) {
							WindowTabActivate((WindowTab *) container->tabBand->GetChild(
									container->tabBand->additionalElements + container->activeTab - 1));
						} else {
							WindowTabActivate((WindowTab *) container->tabBand->GetChild(container->tabBand->additionalElements + 1));
							container->activeTab--;
						}
					}

					EsElementDestroy(tab);
				}
			}
		} else if (message->type == ES_MSG_SET_WINDOW_METADATA) {
			uint8_t *buffer = (uint8_t *) EsHeapAllocate(message->windowMetadata.bytes, false);
			WindowTab *tab = WindowTabFind(message->windowMetadata.id);

			if (buffer && tab) {
				EsConstantBufferRead(message->windowMetadata.buffer, buffer);

				if (buffer[0] == WINDOW_METADATA_TITLE) {
					tab->titleBytes = EsStringFormat(tab->title, sizeof(tab->title), "%s", message->windowMetadata.bytes - 1, buffer + 1);
				} else if (buffer[0] == WINDOW_METADATA_ICON) {
					if (message->windowMetadata.bytes == 5) {
						EsMemoryCopy(&tab->iconID, buffer + 1, sizeof(uint32_t));
					}
				}

				tab->Repaint(true);

				if (tab->tabIndex == tab->container->activeTab) {
					tab->container->taskBarButton->Repaint(true);
				}
			}

			EsHandleClose(message->windowMetadata.buffer);
			EsHeapFree(buffer);
		} else if (message->type == ES_MSG_DESKTOP_START_PROGRAM) {
			void *buffer = EsHeapAllocate(message->createInstance.dataBytes, false);

			if (buffer) {
				EsConstantBufferRead(message->createInstance.data, buffer);
				EsApplicationStartupInformation *information = ApplicationStartupInformationParse(buffer, message->createInstance.dataBytes);

				if (information) {
					for (uintptr_t i = 0; i < shlenu(installedApplications); i++) {
						if (installedApplications[i].value.id == information->id) {
							StartInstanceOfApplication(installedApplications[i].key, 
									message->createInstance.data, message->createInstance.dataBytes, nullptr);
							break;
						}
					}
				}

				EsHeapFree(buffer);
			}

			EsHandleClose(message->createInstance.data);
		} else if (message->type == ES_MSG_APPLICATION_CRASH) {
			for (uintptr_t i = 0; i < arrlenu(allWindowTabs); i++) {
				WindowTab *tab = allWindowTabs[i];

				if (tab->processID != message->crash.pid) {
					continue;
				}

				if (tab->notResponding) {
					tab->notResponding = false;
					tab->embeddedWindowHandle = tab->restoreEmbeddedWindowHandle;
					tab->restoreEmbeddedWindowHandle = ES_INVALID_HANDLE;
					tab->embeddedWindowID = EsSyscall(ES_SYSCALL_WINDOW_GET_ID, tab->embeddedWindowHandle, 0, 0, 0);
					EsAssert(tab->crashedTabInstance);
					EsInstanceDestroy(tab->crashedTabInstance);
					tab->crashedTabInstance = nullptr;
				}

				tab->processID = 0;
				EsHandleClose(tab->embeddedWindowHandle);
				NewCrashedTabInstance(CRASHED_TAB_FATAL_ERROR, tab);

				if (tab == tab->container->tabBand->children[tab->container->tabBand->additionalElements + tab->container->activeTab]) {
					EsSyscall(ES_SYSCALL_WINDOW_SET_EMBED, tab->container->window->handle, (uintptr_t) tab->embeddedWindowHandle, 0, 0);
				}
			}

			for (uintptr_t i = 0; i < hmlenu(installedApplications); i++) {
				if (installedApplications[i].value.useSingleProcess && installedApplications[i].value.singleProcessHandle
						&& EsProcessGetID(installedApplications[i].value.singleProcessHandle) == message->crash.pid) {
					EsHandleClose(installedApplications[i].value.singleProcessHandle);
					installedApplications[i].value.singleProcessHandle = ES_INVALID_HANDLE;
				}
			}

			EsProcessTerminate(message->crash.process, 1); 
			EsHandleClose(message->crash.process);
		} else if (message->type == MSG_SETUP_DESKTOP_UI) {
			SetupDesktopUI();
		}
	}
}
