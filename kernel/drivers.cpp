// TODO Instead of storing devices in a tree structure, perhaps a table is better?

#ifndef IMPLEMENTATION

KInstalledDriver *InitialiseDriverDatabase(); // Returns the root device.

size_t DriversDebugGetEnumeratedPCIDevices(K_USER_BUFFER EsPCIDevice *buffer, size_t bufferCount);

#endif

#ifdef IMPLEMENTATION

void *ResolveKernelSymbol(const char *name, size_t nameBytes);

KDevice *KDriver::NewDevice(KDevice *parent, size_t bytes) {
	if (bytes < sizeof(KDevice)) {
		KernelPanic("KNewDevice - Device structure size is too small (less than KDevice).\n");
	}

	KDevice *device = (KDevice *) EsHeapAllocate(bytes, true, K_FIXED);
	
	if (!device) {
		return nullptr;
	}

	device->driver = this;
	device->parent = parent;

	mutex.Acquire();

	if (!ArrayAdd(devices, device)) {
		EsHeapFree(device, bytes, K_FIXED);
		return nullptr;
	}

	mutex.Release();

	return device;
}

void KDevice::Destroy() {
	bool foundDevice = false;

	driver->mutex.Acquire();

	for (uintptr_t i = 0; i < ArrayLength(driver->devices); i++) {
		if (driver->devices[i] == this) {
			ArrayDelete(driver->devices, i);
			foundDevice = true;
			break;
		}
	}

	driver->mutex.Release();

	if (!foundDevice) {
		KernelPanic("KDevice::Destroy - Could not find device %x in driver's list.\n", this);
	}

	EsHeapFree(this, 0, K_FIXED);
}

KDriver *NewDriver(KInstalledDriver *installedDriver, KModule *module) {
	KDriver *driver = (KDriver *) EsHeapAllocate(sizeof(KDriver), true, K_FIXED);

	if (!driver) {
		return nullptr;
	}

	driver->installedDriver = installedDriver;
	driver->module = module;
	installedDriver->loadedDriver = driver;

	if (!ArrayInitialise(driver->devices, K_FIXED)) {
		EsHeapFree(driver, sizeof(KDriver), K_FIXED);
		return nullptr;
	}

	loadedDriversMutex.Acquire();

	if (!ArrayAdd(loadedDrivers, driver)) {
		EsHeapFree(driver, sizeof(KDriver), K_FIXED);
		return nullptr;
	}

	loadedDriversMutex.Release();

	return driver;
}

KDriver *LoadDriver(KInstalledDriver *installedDriver) {
	if (installedDriver->loadedDriver) {
		return installedDriver->loadedDriver;
	}

	char *entryFunction = nullptr;
	size_t entryFunctionBytes = 0;

	{
		EsINIState s = {};
		s.buffer = installedDriver->config;
		s.bytes = installedDriver->configBytes;

		while (EsINIParse(&s)) {
			if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("entry"))) {
				entryFunction = s.value;
				entryFunctionBytes = s.valueBytes;
			}
		}
	}

	KModuleInitilisationArguments arguments = {};

	if (installedDriver->builtin) {
		KModuleInitialiseCallback callback = (KModuleInitialiseCallback) ResolveKernelSymbol(entryFunction, entryFunctionBytes);

		if (!callback) {
			KernelPanic("LoadDriver - Could not find entry function for builtin driver '%s'.\n", 
					installedDriver->nameBytes, installedDriver->name);
		}

		arguments.driver = NewDriver(installedDriver, nullptr);

		if (!arguments.driver) {
			return nullptr;
		}

		callback(&arguments);
	} else {
		char *buffer = (char *) EsHeapAllocate(K_MAX_PATH, true, K_FIXED);
		if (!buffer) return nullptr;
		EsDefer(EsHeapFree(buffer, K_MAX_PATH, K_FIXED));

		KModule *module = (KModule *) EsHeapAllocate(sizeof(KModule), true, K_FIXED);
		if (!module) return nullptr;

		module->path = buffer;
		module->pathBytes = EsStringFormat(buffer, K_MAX_PATH, K_OS_FOLDER "/Modules/%s.ekm", 
				installedDriver->nameBytes, installedDriver->name);
		module->resolveSymbol = ResolveKernelSymbol;

		EsError error = KLoadELFModule(module);

		if (error == ES_SUCCESS) {
			KernelLog(LOG_INFO, "Modules", "module loaded", "Successfully loaded module '%s'.\n", 
					installedDriver->nameBytes, installedDriver->name);

			KModuleInitialiseCallback initialise = (KModuleInitialiseCallback) KFindSymbol(module, entryFunction, entryFunctionBytes);

			if (initialise) {
				arguments.driver = NewDriver(installedDriver, module);

				if (!arguments.driver) {
					return nullptr;
				}

				initialise(&arguments);
			} else {
				KernelLog(LOG_ERROR, "Modules", "no entry function in module", "LoadDriver - Could not find entry function for module '%s'.\n", 
						installedDriver->nameBytes, installedDriver->name);
			}
		} else {
			KernelLog(LOG_ERROR, "Modules", "module load failure", "Could not load module '%s' (error = %d).\n", 
					installedDriver->nameBytes, installedDriver->name, error);
			EsHeapFree(module, sizeof(KModule), K_FIXED);
		}
	}

	return arguments.driver;
}

struct AttachDeviceData {
	KInstalledDriver *installedDriver;
	KDevice *parentDevice;
};

AttachDeviceData *delayedDevices;
KInstalledDriver *installedDrivers;

void AttachDevice(AttachDeviceData *attach) {
	TS("AttachDevice to %s\n", attach->installedDriver->nameBytes, attach->installedDriver->name);

	if (!attach->installedDriver->builtin && !fs.bootFileSystem) {
		KernelLog(LOG_INFO, "Modules", "delayed device", "Delaying attach device to driver '%s' until boot file system mounted.\n", 
				attach->installedDriver->nameBytes, attach->installedDriver->name);
		ArrayAddPointer(delayedDevices, attach);
	} else {
		KernelLog(LOG_INFO, "Modules", "device attach", "Attaching device to driver '%s'.\n", 
				attach->installedDriver->nameBytes, attach->installedDriver->name);

		KDriver *driver = LoadDriver(attach->installedDriver);

		if (driver && driver->deviceAttached) { 
			driver->deviceAttached(driver, attach->parentDevice);
		}
	}
}

void KRegisterDevice(const char *cDriverName, KDevice *device, EsGeneric context) {
	KDriver *driver = LoadDriver(MapGet(installedDrivers, MakeLongKey(cDriverName, EsCStringLength(cDriverName))));

	if (driver && driver->deviceRegistered) { 
		driver->deviceRegistered(driver, device, context);
	} else {
		KernelLog(LOG_ERROR, "Modules", "could not register device", "Could not register device %x with context %x for driver '%z' (%x).\n",
				device, context, cDriverName, driver);
	}
}

KInstalledDriver *KDriver::ChildDeviceAttached(KDriverIsImplementorCallback callback, KDevice *parentDevice) {
	for (uintptr_t i = ArrayLength(installedDriver->children); i > 0; i--) {
		if (callback(installedDriver->children[i - 1], parentDevice)) {
			AttachDeviceData attach = {};
			attach.parentDevice = parentDevice;
			attach.installedDriver = installedDriver->children[i - 1];
			AttachDevice(&attach);
			return attach.installedDriver;
		}
	}

	return nullptr;
}

extern "C" void EntryRoot(KModuleInitilisationArguments *arguments) {
	KDriver *rootDriver = arguments->driver;
	ArrayInitialise(delayedDevices, K_FIXED);

	// Load all the root drivers, create their devices.

	for (uintptr_t i = 0; i < ArrayLength(rootDriver->installedDriver->children); i++) {
		AttachDeviceData attach = {};
		attach.installedDriver = rootDriver->installedDriver->children[i];
		AttachDevice(&attach);
	}

	// Check we have found the drive from which we booted.

	// TODO How to decide this time?
	if (!fs.foundBootFileSystemEvent.Wait(10000)) {
		KernelPanic("EntryRoot - Could not find the boot fileSystem.\n");
	}

	// Load any devices that were waiting for the boot fileSystem to be loaded.

	for (uintptr_t i = 0; i < ArrayLength(delayedDevices); i++) {
		AttachDevice(delayedDevices + i);
	}

	ArrayFree(delayedDevices);
}

KInstalledDriver *DriversInitialise() {
	ArrayInitialise(loadedDrivers, K_FIXED);

	// Initialise the installed drivers database.

	MapInitialise(installedDrivers, false, true, K_FIXED);
	KInstalledDriver *rootDriver = nullptr;

	EsINIState s = {};
	s.buffer = (char *) _kernelConfig;
	s.bytes = EsCStringLength(_kernelConfig);

	char *moduleName = nullptr, *parentName = nullptr, *dataStart = s.buffer;
	size_t moduleNameBytes = 0, parentNameBytes = 0;
	bool builtin = false;
	bool foundMatchingArchitecture = false, anyArchitecturesListed = false;

	while (EsINIParse(&s)) {
		if (EsStringCompareRaw(s.sectionClass, s.sectionClassBytes, EsLiteral("driver"))) {
			continue;
		}

		moduleName = s.section;
		moduleNameBytes = s.sectionBytes;

		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("parent"))) {
			parentName = s.value, parentNameBytes = s.valueBytes;
		}

		if (0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("builtin"))) {
			builtin = !!EsIntegerParse(s.value, s.valueBytes);
		}

		if (!foundMatchingArchitecture && 0 == EsStringCompareRaw(s.key, s.keyBytes, EsLiteral("arch"))) {
			anyArchitecturesListed = true;

#ifdef ARCH_X86_COMMON
			if (0 == EsStringCompareRaw(s.value, s.valueBytes, EsLiteral("x86_common"))) foundMatchingArchitecture = true;
#endif
#ifdef ARCH_X86_64
			if (0 == EsStringCompareRaw(s.value, s.valueBytes, EsLiteral("x86_64"))) foundMatchingArchitecture = true;
#endif
		}

		char *dataEnd = s.buffer;

		if (!EsINIPeek(&s) || !s.keyBytes) {
			if (foundMatchingArchitecture || !anyArchitecturesListed) {
				// Allocate the installed driver.

				KInstalledDriver *installedDriver = MapPut(installedDrivers, MakeLongKey(moduleName, moduleNameBytes));

				if (!installedDriver) {
					KernelPanic("InitialiseDriverDatabase - Could not initialise driver database (out of memory).\n");
				}

				installedDriver->name = moduleName;
				installedDriver->nameBytes = moduleNameBytes;
				installedDriver->parent = parentName;
				installedDriver->parentBytes = parentNameBytes;
				installedDriver->config = dataStart;
				installedDriver->configBytes = dataEnd - dataStart;
				installedDriver->builtin = builtin;
				ArrayInitialise(installedDriver->children, K_FIXED);
			}

			moduleName = parentName = nullptr;
			moduleNameBytes = parentNameBytes = 0;
			builtin = false;
			foundMatchingArchitecture = anyArchitecturesListed = false;
			dataStart = dataEnd;
		}
	}

	for (uintptr_t i = 0; i < ArrayLength(installedDrivers); i++) {
		KInstalledDriver *installedDriver = installedDrivers + i;
		bool isRoot = 0 == EsStringCompareRaw(installedDriver->name, installedDriver->nameBytes, EsLiteral("Root"));

		if (isRoot) {
			rootDriver = installedDriver;
		} else {
			if (!installedDriver->parentBytes) {
				continue;
			}

			// Add the driver to its parent.

			KInstalledDriver *parentDriver = MapGet(installedDrivers, MakeLongKey(installedDriver->parent, installedDriver->parentBytes));

			if (!parentDriver) {
				KernelPanic("InitialiseDriverDatabase - Parent driver '%s' of child '%s' not found.\n", 
						installedDriver->parentBytes, installedDriver->parent, installedDriver->nameBytes, installedDriver->name);
			}

			if (!ArrayAdd(parentDriver->children, installedDriver)) {
				KernelPanic("InitialiseDriverDatabase - Could not initialise driver database (out of memory).\n");
			}
		}
	}

	return rootDriver;
}

void DriversShutdown() {
	loadedDriversMutex.Acquire();

	for (uintptr_t i = 0; i < ArrayLength(loadedDrivers); i++) {
		if (!loadedDrivers[i]->shutdown) continue;
		loadedDrivers[i]->shutdown(loadedDrivers[i]);
	}

	loadedDriversMutex.Release();
}

size_t DriversDebugGetEnumeratedPCIDevices(K_USER_BUFFER EsPCIDevice *buffer, size_t bufferCount) {
	size_t count = 0;
	loadedDriversMutex.Acquire();

	for (uintptr_t i = 0; i < ArrayLength(loadedDrivers); i++) {
		KInstalledDriver *driver = loadedDrivers[i]->installedDriver;

		if (0 == EsStringCompareRaw(driver->name, driver->nameBytes, EsLiteral("PCI"))) {
			count = KPCIDebugGetEnumeratedDevices(loadedDrivers[i], buffer, bufferCount);
			break;
		}
	}

	loadedDriversMutex.Release();
	return count;
}

#endif
