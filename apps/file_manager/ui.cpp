const char *rootFolderName = interfaceString_FileManagerRootFolder;

// TODO Custom columns.
EsListViewColumn folderOutputColumns[] = {
	{ INTERFACE_STRING(FileManagerColumnName), ES_LIST_VIEW_COLUMN_HAS_MENU | ES_LIST_VIEW_COLUMN_DESCENDING },
	{ INTERFACE_STRING(FileManagerColumnType), ES_LIST_VIEW_COLUMN_HAS_MENU },
	{ INTERFACE_STRING(FileManagerColumnSize), ES_LIST_VIEW_COLUMN_HAS_MENU | ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED },
};

const EsStyle styleFolderView = {
	.inherit = ES_STYLE_LIST_VIEW,

	.metrics = {
		.mask = ES_THEME_METRICS_MINIMUM_WIDTH | ES_THEME_METRICS_PREFERRED_WIDTH,
		.preferredWidth = 200,
		.minimumWidth = 150,
	},
};

const EsStyle styleBookmarkView = {
	.inherit = ES_STYLE_LIST_VIEW,

	.metrics = {
		.mask = ES_THEME_METRICS_MINIMUM_WIDTH | ES_THEME_METRICS_PREFERRED_WIDTH,
		.preferredWidth = 200,
		.minimumWidth = 150,
	},
};

#define LOAD_FOLDER_BACK (1)
#define LOAD_FOLDER_FORWARD (2)
#define LOAD_FOLDER_START (3)

bool InstanceLoadFolder(Instance *instance, String path /* takes ownership */, int historyMode = 0) {
	// Check if the path hasn't changed.

	if (instance->folder && StringEquals(path, instance->folder->path)) {
		StringDestroy(path);
		return true;
	}

	BlockingTaskQueue(instance, {
		.context = historyMode,
		.string = path,
		.cDescription = interfaceString_FileManagerOpenFolderTask,

		.callback = [] (Instance *instance, Task *task) {
			Folder *newFolder = nullptr;
			task->result = FolderAttachInstance(instance, task->string, false, &newFolder);
			task->context2 = newFolder;
			StringDestroy(task->string);
		},

		.then = [] (Instance *instance, Task *task) {
			if (ES_CHECK_ERROR(task->result)) {
				EsTextboxSelectAll(instance->breadcrumbBar);
				EsTextboxInsert(instance->breadcrumbBar, STRING(instance->path));
				InstanceReportError(instance, ERROR_LOAD_FOLDER, task->result);
				return;
			}

			int historyMode = task->context.i;
			Folder *folder = (Folder *) task->context2.p;

			// Add the path to the history array.

			HistoryEntry historyEntry = {};
			historyEntry.path = instance->path;

			if (historyMode == LOAD_FOLDER_BACK) {
				arrput(instance->pathForwardHistory, historyEntry);
			} else if (historyMode == LOAD_FOLDER_FORWARD) {
				arrput(instance->pathBackwardHistory, historyEntry);
			} else if (historyMode == LOAD_FOLDER_START) {
			} else {
				arrput(instance->pathBackwardHistory, historyEntry);

				for (int i = 0; i < arrlen(instance->pathForwardHistory); i++) {
					StringDestroy(instance->pathForwardHistory[i].path);
				}

				arrclear(instance->pathForwardHistory);
			}

			// Update commands.

			EsCommandSetDisabled(&instance->commandGoBackwards, !arrlen(instance->pathBackwardHistory));
			EsCommandSetDisabled(&instance->commandGoForwards, !arrlen(instance->pathForwardHistory));
			EsCommandSetDisabled(&instance->commandGoParent, folder->path.bytes == 1);
			EsCommandSetDisabled(&instance->commandNewFolder, !folder->createChildFolder);
			EsCommandSetDisabled(&instance->commandRename, true);

			// Attach to the new folder.

			EsMutexAcquire(&loadedFoldersMutex);

			InstanceRemoveContents(instance);
			FolderDetachInstance(instance);
			instance->folder = folder;

			bool found = false;

			for (uintptr_t i = 0; i < arrlenu(folder->attachingInstances); i++) {
				if (folder->attachingInstances[i] == instance) {
					arrdelswap(folder->attachingInstances, i);
					found = true;
					break;
				}
			}

			EsAssert(found); // Could not find instance in folder's attachingInstances array.
			arrput(folder->attachedInstances, instance);

			InstanceAddContents(instance, folder->entries, shlen(folder->entries));

			EsMutexRelease(&loadedFoldersMutex);

			// Set the new path.

			instance->path = StringDuplicate(folder->path);
			EsTextboxSelectAll(instance->breadcrumbBar);
			EsTextboxInsert(instance->breadcrumbBar, STRING(folder->path));

			String title = PathGetSection(folder->path, -1);
			if (folder->path.bytes == 1) title = StringFromLiteral(rootFolderName);
			EsWindowSetTitle(instance->window, STRING(title));

			EsElementFocus(instance->list, true);
		},
	});

	return true;
}

void InstanceUpdateItemSelectionCountCommands(Instance *instance) {
	EsCommandSetDisabled(&instance->commandRename, instance->selectedItemCount != 1 && instance->folder->renameItem);
}

inline int InstanceCompareFolderEntries(FolderEntry *left, FolderEntry *right) {
	// TODO Sorting by different columns.

	if (!left->isFolder && right->isFolder) {
		return 1;
	} else if (!right->isFolder && left->isFolder) {
		return -1;
	}

	int result = EsStringCompare(STRING(left->GetName()), STRING(right->GetName()));

	if (result) {
		return result;
	}

	return (uintptr_t) right - (uintptr_t) left; // Only return 0 if the pointers are the same.
}

inline bool InstanceAddInternal(Instance *instance, ListEntry *entry) {
	// TODO Filtering.

	entry->entry->handles++;

	if (entry->selected) {
		instance->selectedItemCount++;
		InstanceUpdateItemSelectionCountCommands(instance);
	}

	return true;
}

void InstanceAddSingle(Instance *instance, ListEntry entry) {
	// Call with loadedFoldersMutex and message mutex.

	if (!InstanceAddInternal(instance, &entry)) {
		return; // Filtered out.
	}

	uintptr_t low = 0, high = arrlen(instance->listContents);

	while (low < high) {
		uintptr_t middle = (low + high) / 2;
		int compare = InstanceCompareFolderEntries(instance->listContents[middle].entry, entry.entry);

		if (compare == 0) {
			return; // Already in the list.
		} else if (compare > 0) {
			high = middle;
		} else {
			low = middle + 1;
		}
	}

	arrins(instance->listContents, low, entry);
	EsListViewInsert(instance->list, 0, low, low);
}

ES_MACRO_SORT(InstanceSortListContents, ListEntry, {
	result = InstanceCompareFolderEntries(_left->entry, _right->entry);
});

void InstanceAddContents(Instance *instance, FolderEntryPair *newEntries, size_t newEntryCount) {
	// Call with loadedFoldersMutex and message mutex.

	size_t oldListEntryCount = arrlenu(instance->listContents);

	for (uintptr_t i = 0; i < newEntryCount; i++) {
		ListEntry entry = { .entry = newEntries[i].value };

		if (InstanceAddInternal(instance, &entry)) {
			arrput(instance->listContents, entry);
		}
	}

	if (oldListEntryCount) {
		EsListViewRemove(instance->list, 0, 0, oldListEntryCount - 1);
	}

	InstanceSortListContents(instance->listContents, arrlenu(instance->listContents));

	if (arrlen(instance->listContents)) {
		EsListViewInsert(instance->list, 0, 0, arrlen(instance->listContents) - 1);
	}
}

inline void InstanceRemoveInternal(Instance *instance, ListEntry *entry) {
	FolderEntryCloseHandle(entry->entry);

	if (entry->selected) {
		instance->selectedItemCount--;
		InstanceUpdateItemSelectionCountCommands(instance);
	}
}

ListEntry InstanceRemoveSingle(Instance *instance, FolderEntry *folderEntry) {
	uintptr_t low = 0, high = arrlen(instance->listContents);

	while (low <= high) {
		uintptr_t middle = (low + high) / 2;
		int compare = InstanceCompareFolderEntries(instance->listContents[middle].entry, folderEntry);

		if (compare == 0) {
			ListEntry entry = instance->listContents[middle];
			InstanceRemoveInternal(instance, &entry);
			arrdel(instance->listContents, middle);
			EsListViewRemove(instance->list, 0, middle, middle);
			return entry;
		} else if (compare > 0) {
			high = middle;
		} else {
			low = middle + 1;
		}
	}

	// It wasn't in the list.
	return {};

}

void InstanceRemoveContents(Instance *instance) {
	for (uintptr_t i = 0; i < arrlenu(instance->listContents); i++) {
		InstanceRemoveInternal(instance, instance->listContents + i);
	}

	EsAssert(instance->selectedItemCount == 0); // After removing all items none should be selected.

	if (arrlen(instance->listContents)) {
		EsListViewRemove(instance->list, 0, 0, arrlen(instance->listContents) - 1);
		EsListViewContentChanged(instance->list);
	}

	arrfree(instance->listContents);
}

ListEntry *InstanceGetSelectedListEntry(Instance *instance) {
	for (uintptr_t i = 0; i < arrlenu(instance->listContents); i++) {
		ListEntry *entry = instance->listContents + i;

		if (entry->selected) {
			return entry;
		}
	}

	return nullptr;
}

int ListCallback(EsElement *element, EsMessage *message) {
	Instance *instance = element->instance;

	if (message->type == ES_MSG_LIST_VIEW_GET_CONTENT) {
		int column = message->getContent.column, index = message->getContent.index.i;
		EsAssert(index < arrlen(instance->listContents) && index >= 0); // Invalid index in list contents.
		ListEntry *listEntry = instance->listContents + index;
		FolderEntry *entry = listEntry->entry;
		FileType *fileType = FolderEntryGetType(entry);

		if (column == 0) {
			String name = entry->GetName();
			message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, "%s", name.bytes, name.text);
			message->getContent.icon = fileType->iconID;
		} else if (column == 1) {
			message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, "%z", fileType->name);
		} else if (column == 2) {
			if (entry->sizeUnknown) {
				message->getContent.bytes = 0;
			} else {
				message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, "%D", entry->size);
			}
		}
	} else if (message->type == ES_MSG_LIST_VIEW_SELECT_RANGE) {
		for (intptr_t i = message->selectRange.fromIndex.i; i <= message->selectRange.toIndex.i; i++) {
			ListEntry *entry = instance->listContents + i;
			if (entry->selected) instance->selectedItemCount--;
			entry->selected = message->selectRange.toggle ? !entry->selected : message->selectRange.select;
			if (entry->selected) instance->selectedItemCount++;
		}

		InstanceUpdateItemSelectionCountCommands(instance);
	} else if (message->type == ES_MSG_LIST_VIEW_SELECT) {
		ListEntry *entry = instance->listContents + message->selectItem.index.i;
		if (entry->selected) instance->selectedItemCount--;
		entry->selected = message->selectItem.isSelected;
		if (entry->selected) instance->selectedItemCount++;
		InstanceUpdateItemSelectionCountCommands(instance);
	} else if (message->type == ES_MSG_LIST_VIEW_IS_SELECTED) {
		ListEntry *entry = instance->listContents + message->selectItem.index.i;
		message->selectItem.isSelected = entry->selected;
	} else if (message->type == ES_MSG_LIST_VIEW_CHOOSE_ITEM) {
		ListEntry *listEntry = instance->listContents + message->chooseItem.index.i;

		if (listEntry) {
			FolderEntry *entry = listEntry->entry;

			if (entry->isFolder) {
				String path = StringAllocateAndFormat("%s%s/", STRFMT(instance->folder->path), STRFMT(entry->GetName()));
				InstanceLoadFolder(instance, path);
			} else {
				FileType *fileType = FolderEntryGetType(entry);

				if (fileType->openHandler) {
					String path = StringAllocateAndFormat("%s%s", STRFMT(instance->folder->path), STRFMT(entry->GetName()));
					EsApplicationStartupInformation information = {};
					information.id = fileType->openHandler;
					information.filePath = path.text;
					information.filePathBytes = path.bytes;
					EsApplicationStart(&information);
					StringDestroy(path);
				} else {
					EsDialogShowAlert(instance->window, INTERFACE_STRING(FileManagerOpenFileError),
							INTERFACE_STRING(FileManagerNoRegisteredApplicationsForFile),
							ES_ICON_DIALOG_ERROR, ES_DIALOG_ALERT_OK_BUTTON); 
				}
			}
		}
	} else {
		return 0;
	}

	return ES_HANDLED;
}

void InstanceCreateUI(Instance *instance) {
	uint32_t stableCommandID = 1;

	EsCommandRegister(&instance->commandGoBackwards, instance, [] (Instance *instance, EsElement *, EsCommand *) {
		EsAssert(arrlen(instance->pathBackwardHistory)); 
		InstanceLoadFolder(instance, arrpop(instance->pathBackwardHistory).path, LOAD_FOLDER_BACK);
	}, stableCommandID++, "Backspace|Alt+Left");

	EsCommandRegister(&instance->commandGoForwards, instance, [] (Instance *instance, EsElement *, EsCommand *) {
		EsAssert(arrlen(instance->pathForwardHistory));
		InstanceLoadFolder(instance, arrpop(instance->pathForwardHistory).path, LOAD_FOLDER_FORWARD);
	}, stableCommandID++, "Alt+Right");

	EsCommandRegister(&instance->commandGoParent, instance, [] (Instance *instance, EsElement *, EsCommand *) {
		String parent = PathGetParent(instance->folder->path);
		InstanceLoadFolder(instance, StringDuplicate(parent));
	}, stableCommandID++, "Alt+Up");

	EsCommandRegister(&instance->commandNewFolder, instance, [] (Instance *instance, EsElement *element, EsCommand *) {
		EsMenu *menu = EsMenuCreate(element ?: instance->newFolderButton, ES_FLAGS_DEFAULT);
		EsPanel *panel = EsPanelCreate(menu, ES_FLAGS_DEFAULT, ES_STYLE_PANEL_POPUP);
		EsTextDisplayCreate(panel, ES_CELL_H_FILL, ES_STYLE_TEXT_LABEL, INTERFACE_STRING(FileManagerFolderNamePrompt));
		instance->newFolder.nameTextbox = EsTextboxCreate(panel, ES_CELL_H_FILL, nullptr);
		EsPanel *row = EsPanelCreate(panel, ES_PANEL_HORIZONTAL | ES_CELL_H_FILL | ES_PANEL_REVERSE);
		instance->newFolder.createButton = EsButtonCreate(row, ES_BUTTON_DEFAULT | ES_ELEMENT_DISABLED, nullptr, INTERFACE_STRING(FileManagerNewFolderAction));
		EsMenuShow(menu);

		instance->newFolder.nameTextbox->userCallback = [] (EsElement *element, EsMessage *message) {
			EsTextbox *textbox = (EsTextbox *) element;

			if (message->type == ES_MSG_TEXTBOX_UPDATED) {
				EsElementSetDisabled(textbox->instance->newFolder.createButton, !EsTextboxGetLineLength(textbox));
			}

			return 0;
		};
		
		EsElementFocus(instance->newFolder.nameTextbox);

		EsButtonOnCommand(instance->newFolder.createButton, [] (Instance *instance, EsElement *element, EsCommand *) {
			String name = {};
			name.text = EsTextboxGetContents(instance->newFolder.nameTextbox, &name.bytes);
			name.allocated = name.bytes;

			// EsDialogClose(instance->window);
			EsElementDestroy(element->window);
			EsElementFocus(instance->list);

			BlockingTaskQueue(instance, {
				.string = name,
				.cDescription = interfaceString_FileManagerNewFolderTask,

				.callback = [] (Instance *instance, Task *task) {
					task->result = instance->folder->createChildFolder(instance->folder, task->string);
					StringDestroy(task->string);
				},

				.then = [] (Instance *instance, Task *task) {
					if (task->result != ES_SUCCESS) {
						InstanceReportError(instance, ERROR_NEW_FOLDER, task->result);
					}
				},
			});
		});
	}, stableCommandID++, "Ctrl+Shift+N");

	EsCommandRegister(&instance->commandRename, instance, [] (Instance *instance, EsElement *, EsCommand *) {
		EsElement *dialog = EsDialogShow(instance->window);

		EsPanel *heading = EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_DIALOG_HEADING);
		EsIconDisplayCreate(heading, ES_FLAGS_DEFAULT, {}, ES_ICON_DOCUMENT_EDIT);
		EsTextDisplayCreate(heading, ES_CELL_H_FILL | ES_CELL_V_CENTER, ES_STYLE_TEXT_HEADING2, INTERFACE_STRING(FileManagerRenameTitle));

		EsPanel *contents = EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_VERTICAL, ES_STYLE_DIALOG_CONTENT);
		EsTextDisplayCreate(contents, ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, INTERFACE_STRING(FileManagerRenamePrompt));
		instance->rename.nameTextbox = EsTextboxCreate(contents, ES_CELL_H_FILL, {});

		instance->rename.nameTextbox->userCallback = [] (EsElement *element, EsMessage *message) {
			EsTextbox *textbox = (EsTextbox *) element;

			if (message->type == ES_MSG_TEXTBOX_UPDATED) {
				EsElementSetDisabled(textbox->instance->rename.renameButton, !EsTextboxGetLineLength(textbox));
			}

			return 0;
		};
		
		EsTextboxInsert(instance->rename.nameTextbox, STRING(InstanceGetSelectedListEntry(instance)->entry->GetName()), false);
		EsTextboxSelectAll(instance->rename.nameTextbox);
		EsElementFocus(instance->rename.nameTextbox);

		EsPanel *buttonArea = EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE, ES_STYLE_DIALOG_BUTTON_AREA);

		EsButtonOnCommand(EsButtonCreate(buttonArea, ES_BUTTON_CANCEL, {}, INTERFACE_STRING(CommonCancel)), [] (Instance *instance, EsElement *, EsCommand *) {
			EsDialogClose(instance->window);
			EsElementFocus(instance->list);
		});

		instance->rename.renameButton = EsButtonCreate(buttonArea, ES_BUTTON_DEFAULT, {}, INTERFACE_STRING(FileManagerRenameAction));
		
		EsButtonOnCommand(instance->rename.renameButton, [] (Instance *instance, EsElement *, EsCommand *) {
			String name = {};
			name.text = EsTextboxGetContents(instance->rename.nameTextbox, &name.bytes);
			name.allocated = name.bytes;

			EsDialogClose(instance->window);
			EsElementFocus(instance->list);

			BlockingTaskQueue(instance, {
				.context = InstanceGetSelectedListEntry(instance)->entry,
				.string = name,
				.cDescription = interfaceString_FileManagerRenameTask,

				.callback = [] (Instance *instance, Task *task) {
					FolderEntry *entry = (FolderEntry *) task->context.p;

					if (StringEquals(entry->GetName(), task->string)) {
						task->result = ES_SUCCESS;
					} else {
						task->result = instance->folder->renameItem(instance->folder, entry, task->string);
					}

					StringDestroy(task->string);
				},

				.then = [] (Instance *instance, Task *task) {
					if (task->result != ES_SUCCESS) {
						InstanceReportError(instance, ERROR_RENAME_ITEM, task->result);
					}
				},
			});
		});
	}, stableCommandID++, "F2");

	EsWindowSetIcon(instance->window, ES_ICON_SYSTEM_FILE_MANAGER);

	EsSplitter *splitter = EsSplitterCreate(instance->window, ES_SPLITTER_HORIZONTAL | ES_CELL_FILL, ES_STYLE_PANEL_WINDOW_DIVIDER);

	instance->bookmarkView = EsListViewCreate(splitter, ES_CELL_EXPAND | ES_CELL_COLLAPSABLE | ES_LIST_VIEW_SINGLE_SELECT | ES_CELL_V_FILL, &styleBookmarkView);
	EsListViewSetEmptyMessage(instance->bookmarkView, INTERFACE_STRING(FileManagerEmptyBookmarkView));

	instance->list = EsListViewCreate(splitter, ES_CELL_FILL | ES_LIST_VIEW_COLUMNS | ES_LIST_VIEW_MULTI_SELECT, &styleFolderView);
	instance->list->userCallback = ListCallback;
	EsListViewSetColumns(instance->list, folderOutputColumns, sizeof(folderOutputColumns) / sizeof(folderOutputColumns[0]));
	EsListViewInsertGroup(instance->list, 0);
	EsListViewSetEmptyMessage(instance->list, INTERFACE_STRING(FileManagerEmptyFolderView));

	EsElement *toolbar = EsWindowGetToolbar(instance->window);
	EsButton *button;

#define ADD_BUTTON_TO_TOOLBAR(_command, _label, _icon, _accessKey, _name) \
	{ \
		_name = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR, {}, TL(_label)); \
		EsButtonSetIcon(_name, _icon);  \
		EsCommandAddButton(&instance->_command, _name); \
		_name->accessKey = _accessKey; \
	}

	ADD_BUTTON_TO_TOOLBAR(commandGoBackwards, nullptr, ES_ICON_GO_PREVIOUS_SYMBOLIC, 'B', button);
	ADD_BUTTON_TO_TOOLBAR(commandGoForwards, nullptr, ES_ICON_GO_NEXT_SYMBOLIC, 'F', button);
	ADD_BUTTON_TO_TOOLBAR(commandGoParent, nullptr, ES_ICON_GO_UP_SYMBOLIC, 'U', button);

	EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT);

	instance->breadcrumbBar = EsTextboxCreate(toolbar, ES_CELL_H_FILL | ES_TEXTBOX_EDIT_BASED | ES_TEXTBOX_REJECT_EDIT_IF_LOST_FOCUS, {});

	instance->breadcrumbBar->userCallback = [] (EsElement *element, EsMessage *message) {
		Instance *instance = element->instance;

		if (message->type == ES_MSG_TEXTBOX_ACTIVATE_BREADCRUMB) {
			String section = PathGetSection(instance->folder->path, message->activateBreadcrumb);
			size_t bytes = section.text + section.bytes - instance->folder->path.text;

			String path = StringAllocateAndFormat("%s%z", 
					bytes, instance->folder->path.text,
					instance->folder->path.text[bytes - 1] != '/' ? "/" : "");
			InstanceLoadFolder(instance, path);
		} else if (message->type == ES_MSG_TEXTBOX_EDIT_END) {
			String section;
			section.text = EsTextboxGetContents(instance->breadcrumbBar, &section.bytes);
			section.allocated = section.bytes;

			String path = StringAllocateAndFormat("%s%z", 
					section.bytes, section.text,
					section.text[section.bytes - 1] != '/' ? "/" : "");

			if (!InstanceLoadFolder(instance, path)) {
				StringDestroy(section);
				return ES_REJECTED;
			}

			StringDestroy(section);
		} else if (message->type == ES_MSG_TEXTBOX_GET_BREADCRUMB) {
			if (!instance->folder || PathCountSections(instance->folder->path) == message->getBreadcrumb.index) {
				return ES_REJECTED;
			} else if (message->getBreadcrumb.index == 0) {
				message->getBreadcrumb.text = rootFolderName;
				message->getBreadcrumb.textBytes = -1;
				return ES_HANDLED;
			} else {
				String section = PathGetSection(instance->folder->path, message->getBreadcrumb.index);
				message->getBreadcrumb.text = section.text;
				message->getBreadcrumb.textBytes = section.bytes;
				return ES_HANDLED;
			}
		}

		return 0;
	};

	EsTextboxUseBreadcrumbOverlay(instance->breadcrumbBar);

	ADD_BUTTON_TO_TOOLBAR(commandNewFolder, interfaceString_FileManagerNewFolderToolbarItem, ES_ICON_FOLDER_NEW_SYMBOLIC, 'N', instance->newFolderButton);

	String path = StringAllocateAndFormat("/");
	InstanceLoadFolder(instance, path, LOAD_FOLDER_START);
}

void InstanceReportError(Instance *instance, int error, EsError code) {
	EsPrint("FM error %d/%d.\n", error, code);

	// TODO Error messages.

	const char *message = interfaceString_FileManagerGenericError;

	if (code == ES_ERROR_FILE_ALREADY_EXISTS) {
		message = interfaceString_FileManagerItemAlreadyExistsError;
	} else if (code == ES_ERROR_FILE_DOES_NOT_EXIST) {
		message = interfaceString_FileManagerItemDoesNotExistError;
	}

	EsDialogShowAlert(instance->window, errorTypeStrings[error], -1, message, -1, 
			ES_ICON_DIALOG_ERROR, ES_DIALOG_ALERT_OK_BUTTON); 
}
