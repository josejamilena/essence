// TODO Better configuration over what files are imported to the drive image.
// TODO Replace in_build_full.
// TODO Make build_core responsible for generating the header.

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <stdbool.h>

#ifdef PARALLEL_BUILD
#include <pthread.h>
#endif

#ifdef OS_ESSENCE
#include <essence.h>
#endif

#include "../shared/hash.cpp"
#include "build_common.h"
#include "esfs2.h"

// Toolchain flags:

char commonCompileFlags[4096] = " -Wall -Wextra -Wno-missing-field-initializers -Wno-frame-address "
	"-Wno-unused-function -Wno-format-truncation -ffreestanding -fno-exceptions -g -I. ";
char cCompileFlags[4096] = "";
char cppCompileFlags[4096] = " -std=c++14 -Wno-pmf-conversions -Wno-invalid-offsetof -fno-rtti ";
char kernelCompileFlags[4096] = " -mno-red-zone -mcmodel=kernel -fno-omit-frame-pointer ";
char commonLinkFlags[4096] = " -ffreestanding -nostdlib -lgcc -g -z max-page-size=0x1000 -T util/linker_userland64.ld ";
char apiLinkFlags1[4096] = " -T util/linker_api64.ld -ffreestanding -nostdlib -g -z max-page-size=0x1000 -Wl,--start-group ";
char apiLinkFlags2[4096] = " -lgcc ";
char apiLinkFlags3[4096] = " -Wl,--end-group -Lroot/Applications/POSIX/lib ";
char kernelLinkFlags[4096] = " -ffreestanding -nostdlib -lgcc -g -z max-page-size=0x1000 ";

// Specific configuration options:

const char *firstApplication = "";
bool inBuildFull;
bool verbose;
bool useColoredOutput;

// State:

char *builtinModules;
volatile uint8_t encounteredErrors;

//////////////////////////////////

#define COLOR_ERROR "\033[0;33m"
#define COLOR_HIGHLIGHT "\033[0;36m"
#define COLOR_NORMAL "\033[0m"

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

const char *toolchainAR = "/Applications/POSIX/bin/ar";
const char *toolchainCC = "/Applications/POSIX/bin/gcc";
const char *toolchainCXX = "/Applications/POSIX/bin/g++";
const char *toolchainLD = "/Applications/POSIX/bin/ld";
const char *toolchainNM = "/Applications/POSIX/bin/nm";
const char *toolchainStrip = "/Applications/POSIX/bin/strip";
const char *toolchainNasm = "/Applications/POSIX/bin/nasm";

char *executeEnvironment[2] = {
	(char *) "PATH=/Applications/POSIX/bin",
	NULL,
};

#define Execute(...) _Execute(NULL, __VA_ARGS__, NULL, NULL)
#define ExecuteWithOutput(...) _Execute(__VA_ARGS__, NULL, NULL)
#define ExecuteForApp(application, ...) if (!application->error && ExecuteWithOutput(&application->output, __VA_ARGS__)) application->error = true
#define ArgString(x) NULL, x

int _Execute(char **output, const char *executable, ...) {
	char *argv[64];
	va_list argList;
	va_start(argList, executable);

	argv[0] = (char *) executable;

	for (uintptr_t i = 1; i <= 64; i++) {
		assert(i != 64);
		argv[i] = va_arg(argList, char *);

		if (!argv[i]) {
			char *string = va_arg(argList, char *);

			if (!string) {
				break;
			}

			char *copy = (char *) malloc(strlen(string) + 2);
			strcpy(copy, string);
			strcat(copy, " ");
			uintptr_t start = 0;

			for (uintptr_t j = 0; copy[j]; j++) {
				assert(i != 64);

				if (copy[j] == ' ' || !copy[j]) {
					if (start != j) {
						argv[i++] = copy + start;
					}

					copy[j] = 0;
					start = j + 1;
				}
			}

			i--;
		}
	}

	va_end(argList);

	if (verbose) {
		for (uintptr_t i = 0; i < 64; i++) {
			if (!argv[i]) break;
			fprintf(stderr, "\"%s\" ", argv[i]);
		}

		fprintf(stderr, "\n");
	}

	int stdoutPipe[2];

	if (output) {
		pipe(stdoutPipe);
	}

	int status = -1;
	pid_t pid = vfork();

	if (pid == 0) {
		if (output) {
			dup2(stdoutPipe[1], 1);
			dup2(stdoutPipe[1], 2);
			close(stdoutPipe[1]);
		}

		execve(executable, argv, executeEnvironment);
		_exit(-1);
	} else if (pid > 0) {
		if (output) {
			close(stdoutPipe[1]);

			while (true) {
				char buffer[1024];
				ssize_t bytesRead = read(stdoutPipe[0], buffer, 1024);

				if (bytesRead <= 0) {
					break;
				} else {
					size_t previousLength = arrlenu(*output);
					arrsetlen(*output, previousLength + bytesRead);
					memcpy(*output + previousLength, buffer, bytesRead);
				}
			}
		}

		wait4(-1, &status, 0, NULL);
	} else {
		fprintf(stderr, "Error: could not vfork process.\n");
		exit(1);
	}

	if (output) {
		close(stdoutPipe[0]);
	}

	if (verbose && status) {
		fprintf(stderr, "(status = %d)\n", status);
	}

	if (status) __sync_fetch_and_or(&encounteredErrors, 1);
	return status;
}

void MakeDirectory(const char *path) {
	mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

void DeleteFile(const char *path) {
	unlink(path);
}

void MoveFile(const char *oldPath, const char *newPath) {
	rename(oldPath, newPath);
}

bool FileExists(const char *path) {
	struct stat s;
	return !stat(path, &s);
}

void CreateImportNode(const char *path, ImportNode *node) {
	char pathBuffer[256];

	DIR *d = opendir(path);
	struct dirent *dir;

	if (!d) {
		return;
	}

	while ((dir = readdir(d))) {
		if (dir->d_name[0] == '.') {
			continue;
		}

		snprintf(pathBuffer, sizeof(pathBuffer), "%s/%s", path, dir->d_name);
		struct stat s = {};
		lstat(pathBuffer, &s);

		ImportNode child = {};

		if (S_ISDIR(s.st_mode)) {
			CreateImportNode(pathBuffer, &child);
		} else if ((s.st_mode & S_IFMT) == S_IFLNK) {
			continue;
		} else {
			child.isFile = true;
		}

		child.name = strdup(dir->d_name);
		child.path = strdup(pathBuffer);
		arrput(node->children, child);
	}

	closedir(d);
}

//////////////////////////////////

typedef struct FileType {
	const char *extension;
	const char *name;
	const char *icon;
	int id, openID;
} FileType;

typedef struct Handler {
	const char *extension;
	const char *action;
	int fileTypeID;
} Handler;

typedef struct DepdendencyFile {
	char path[256];
	const char *name;
} DepdendencyFile;

typedef struct Application {
	const char *name; 
	const char *icon;
	int id;
	bool useSingleProcess, hidden;

	FileType *fileTypes;
	Handler *handlers;

	bool install, builtin;

	const char *source;
	const char *compileFlags;
	const char *linkFlags;
	const char *customCompileCommand;
	const char *manifestPath;

	DepdendencyFile *dependencyFiles;
	char *output;
	bool error, skipped;

	void (*buildCallback)(struct Application *); // Called on a build thread.
} Application;

int nextID = 1;
Application *applications;

void BuildDesktop(Application *application) {
	ExecuteForApp(application, toolchainNasm, "-felf64", "desktop/api.s", "-MD", "bin/api1.d", "-o", "bin/api1.o", "-Fdwarf");

	ExecuteForApp(application, toolchainCXX, "-MD", "-c", "desktop/api.cpp", "-o", "bin/api2.o", ArgString(commonCompileFlags));
	ExecuteForApp(application, toolchainCXX, "-MD", "-c", "desktop/cstdlib.cpp", "-o", "bin/api3.o", ArgString(commonCompileFlags));
	ExecuteForApp(application, toolchainCC, "-o", "bin/Desktop.esx_symbols", "bin/crti.o", "bin/crtbegin.o", 
			"bin/api1.o", "bin/api2.o", "bin/api3.o", "bin/crtend.o", "bin/crtn.o", 
			ArgString(apiLinkFlags1), ArgString(apiLinkFlags2), ArgString(apiLinkFlags3));

	ExecuteForApp(application, toolchainStrip, "-o", "root/Essence/Desktop.esx", "--strip-all", "bin/Desktop.esx_symbols");
}

void BuildApplication(Application *application) {
	{
		char buffer[4096];
		snprintf(buffer, sizeof(buffer), "root/Applications/%s", application->name);
		MakeDirectory(buffer);
	}

	if (application->customCompileCommand) {
		system(application->customCompileCommand);
	} else {
		char symbolFile[256];
		snprintf(symbolFile, sizeof(symbolFile), "bin/%s.esx_symbols", application->name);
		char objectFile[256];
		snprintf(objectFile, sizeof(objectFile), "bin/%s.o", application->name);
		char executable[256];
		snprintf(executable, sizeof(executable), "root/Applications/%s/Entry.esx", application->name);

		size_t sourceBytes = strlen(application->source);

		if (sourceBytes > 2 && application->source[sourceBytes - 1] == 'c' && application->source[sourceBytes - 2] == '.') {
			ExecuteForApp(application, toolchainCC, "-MD", "-o", objectFile, "-c", application->source, 
					ArgString(cCompileFlags), ArgString(commonCompileFlags), ArgString(application->compileFlags));
		} else {
			ExecuteForApp(application, toolchainCXX, "-MD", "-o", objectFile, "-c", application->source, 
					ArgString(cppCompileFlags), ArgString(commonCompileFlags), ArgString(application->compileFlags));
		}

		ExecuteForApp(application, toolchainCC, "-o", symbolFile, "-Wl,--start-group", 
				ArgString(application->linkFlags), "bin/crti.o",
				"bin/crtbegin.o", objectFile, "bin/crtend.o", "bin/crtn.o", "-Wl,--end-group", ArgString(commonLinkFlags));
		ExecuteForApp(application, toolchainStrip, "-o", executable, symbolFile);
	}
}

#ifdef PARALLEL_BUILD
volatile uintptr_t applicationsIndex = 0;

void *BuildApplicationThread(void *_unused) {
	while (true) {
		uintptr_t i = __sync_fetch_and_add(&applicationsIndex, 1);

		if (i >= arrlenu(applications)) {
			return NULL;
		}

		if (applications[i].skipped) continue;
		applications[i].buildCallback(&applications[i]);
	}
}
#endif

void ParseApplicationManifest(const char *manifestPath) {
	EsINIState s = {};
	char *manifest = (char *) LoadFile(manifestPath, &s.bytes);
	s.buffer = manifest;

	const char *require = "";
	bool disabled = false;

	Application application = {};
	application.useSingleProcess = true;
	application.id = nextID++;
	application.manifestPath = manifestPath;
	application.compileFlags = "";
	application.linkFlags = "";
	Handler *handler = NULL;
	FileType *fileType = NULL;

	while (EsINIParse(&s)) {
		EsINIZeroTerminate(&s);

		if (0 == strcmp(s.section, "build")) {
			INI_READ_STRING_PTR(source, application.source);
			INI_READ_STRING_PTR(compile_flags, application.compileFlags);
			INI_READ_STRING_PTR(link_flags, application.linkFlags);
			INI_READ_STRING_PTR(custom_compile_command, application.customCompileCommand);
			INI_READ_STRING_PTR(require, require);
		} else if (0 == strcmp(s.section, "general")) {
			INI_READ_STRING_PTR(name, application.name);
			INI_READ_STRING_PTR(icon, application.icon);
			INI_READ_BOOL(use_single_process, application.useSingleProcess);
			INI_READ_BOOL(hidden, application.hidden);
			INI_READ_STRING_PTR(name, application.name);
			INI_READ_BOOL(disabled, disabled);
		} else if (0 == strcmp(s.sectionClass, "handler")) {
			if (!s.keyBytes) {
				Handler _handler = {};
				arrput(application.handlers, _handler);
				handler = &arrlast(application.handlers);
			}

			INI_READ_STRING_PTR(extension, handler->extension);
			INI_READ_STRING_PTR(action, handler->action);
		} else if (0 == strcmp(s.sectionClass, "file_type")) {
			if (!s.keyBytes) {
				FileType _fileType = {};
				_fileType.id = nextID++;
				arrput(application.fileTypes, _fileType);
				fileType = &arrlast(application.fileTypes);
			}

			INI_READ_STRING_PTR(extension, fileType->extension);
			INI_READ_STRING_PTR(name, fileType->name);
			INI_READ_STRING_PTR(icon, fileType->icon);
		}
	}

	if (disabled || (require[0] && !FileExists(require))) {
		return;
	}

	DepdendencyFile dependencyFile = {};
	dependencyFile.name = application.name;
	snprintf(dependencyFile.path, sizeof(dependencyFile.path), "bin/%s.d", application.name);
	arrput(application.dependencyFiles, dependencyFile);

	application.buildCallback = BuildApplication;
	application.install = true;

	arrput(applications, application);
}

void OutputSystemConfiguration() {
	EsINIState s = {};
	char *config = (char *) LoadFile("res/System Configuration Template.ini", &s.bytes);
	s.buffer = config;
	FILE *file = fopen("root/Essence/System Configuration.ini", "wb");

	while (EsINIParse(&s)) {
		EsINIZeroTerminate(&s);

		char buffer[4096];
		fwrite(buffer, 1, EsINIFormat(&s, buffer, sizeof(buffer)), file);

		if (0 == strcmp(s.section, "general") && (!EsINIPeek(&s) || !s.keyBytes)) {
			fprintf(file, "next_id=%d\n", nextID);
			fprintf(file, "first_application=%s\n", firstApplication);
		}
	}

	for (uintptr_t i = 0; i < arrlenu(applications); i++) {
		if (!applications[i].install) {
			continue;
		}

		for (uintptr_t j = 0; j < arrlenu(applications[i].handlers); j++) {
			Handler *handler = applications[i].handlers + j;
			int handlerID = applications[i].id;

			for (uintptr_t i = 0; i < arrlenu(applications); i++) {
				for (uintptr_t j = 0; j < arrlenu(applications[i].fileTypes); j++) {
					FileType *fileType = applications[i].fileTypes + j;

					if (0 == strcmp(handler->extension, fileType->extension)) {
						handler->fileTypeID = fileType->id;

						if (0 == strcmp(handler->action, "open")) {
							fileType->openID = handlerID;
						} else {
							fprintf(stderr, "Warning: unrecognised handler action '%s'.\n", handler->action);
						}
					}
				}
			}

			if (!handler->fileTypeID) {
				fprintf(stderr, "Warning: could not find a file_type entry for handler with extension '%s' in application '%s'.\n",
						handler->extension, applications[i].name);
			}
		}
	}

	for (uintptr_t i = 0; i < arrlenu(applications); i++) {
		if (!applications[i].install) {
			continue;
		}

		fprintf(file, "\n[@application %d]\n", applications[i].id);
		fprintf(file, "name=%s\n", applications[i].name);
		fprintf(file, "icon=%s\n", applications[i].icon);
		fprintf(file, "use_single_process=%d\n", applications[i].useSingleProcess);
		fprintf(file, "hidden=%d\n", applications[i].hidden);
		fprintf(file, "installation_folder=/Applications/%s\n", applications[i].name);
		fprintf(file, "executable=Entry.esx\n");

		for (uintptr_t j = 0; j < arrlenu(applications[i].fileTypes); j++) {
			fprintf(file, "\n[@file_type %d]\n", applications[i].fileTypes[j].id);
			fprintf(file, "extension=%s\n", applications[i].fileTypes[j].extension);
			fprintf(file, "name=%s\n", applications[i].fileTypes[j].name);
			fprintf(file, "icon=%s\n", applications[i].fileTypes[j].icon);
			fprintf(file, "open=%d\n", applications[i].fileTypes[j].openID);
		}

		for (uintptr_t j = 0; j < arrlenu(applications[i].handlers); j++) {
			fprintf(file, "\n[@handler]\n");
			fprintf(file, "action=%s\n", applications[i].handlers[j].action);
			fprintf(file, "application=%d\n", applications[i].id);
			fprintf(file, "file_type=%d\n", applications[i].handlers[j].fileTypeID);
		}
	}

	fclose(file);
}

void BuildModule(Application *application) {
	char output[4096];
	snprintf(output, sizeof(output), "bin/%s.ekm", application->name);

	ExecuteForApp(application, toolchainCXX, "-MD", "-c", application->source, "-o", 
			output, ArgString(cppCompileFlags), ArgString(kernelCompileFlags), ArgString(commonCompileFlags), 
			application->builtin ? "-DBUILTIN_MODULE" : "-DKERNEL_MODULE");

	if (!application->builtin) {
		char target[4096];
		snprintf(target, sizeof(target), "root/Essence/Modules/%s.ekm", application->name);
		MoveFile(output, target);
	}
}

void ParseKernelConfiguration() {
	size_t kernelConfigBytes;
	char *kernelConfig = (char *) LoadFile("kernel/config.ini", &kernelConfigBytes);

	if (CheckDependencies("Kernel Config")) {
		FILE *f = fopen("root/Applications/POSIX/include/kernel_config.h", "wb");
		fprintf(f, "const char *_kernelConfig = R\"(\n");
		fwrite(kernelConfig, 1, kernelConfigBytes, f);
		fprintf(f, "\n)\";\n");
		fclose(f);

		f = fopen("bin/system_config.d", "wb");
		fprintf(f, ": kernel/config.ini\n");
		fclose(f);
		ParseDependencies("bin/system_config.d", "Kernel Config");
		DeleteFile("bin/system_config.d");
	}

	EsINIState s = {};
	s.buffer = kernelConfig;
	s.bytes = kernelConfigBytes;

	char *source = NULL, *name = NULL;
	bool builtin = false;

	while (EsINIParse(&s)) {
		EsINIZeroTerminate(&s);

		if (strcmp(s.sectionClass, "driver")) {
			continue;
		}

		name = s.section;

		if (0 == strcmp(s.key, "source")) source = s.value;
		if (0 == strcmp(s.key, "builtin")) builtin = !!atoi(s.value);

		if (!EsINIPeek(&s) || !s.keyBytes) {
			if (source && *source) {
				DepdendencyFile dependencyFile = {};
				dependencyFile.name = name;
				snprintf(dependencyFile.path, sizeof(dependencyFile.path), "bin/%s.d", name);

				Application application = {};
				application.source = source;
				application.name = name;
				application.builtin = builtin;
				application.buildCallback = BuildModule;
				arrput(application.dependencyFiles, dependencyFile);
				arrput(applications, application);

				if (builtin) {
					char append[256];
					snprintf(append, sizeof(append), " bin/%s.ekm ", name);
					size_t previousLength = arrlenu(builtinModules);
					arrsetlen(builtinModules, previousLength + strlen(append));
					memcpy(builtinModules + previousLength, append, strlen(append));
				}
			}

			source = name = NULL;
			builtin = false;
		}
	}
}

void LinkKernel() {
	arrput(builtinModules, 0);

	if (Execute(toolchainLD, "-r", "bin/kernel.o", "bin/kernel_x86_64.o", ArgString(builtinModules), "-o" "bin/kernel_all.o")) {
		return;
	}
	
	{
		char *output = NULL;

		if (_Execute(&output, toolchainNM, "bin/kernel_all.o", NULL, NULL)) {
			return;
		} else {
			FILE *f = fopen("bin/kernel_symbols.tmp", "wb");
			uintptr_t lineStart = 0, position = 0;

			while (position < arrlenu(output)) {
				if (output[position] == '\n') {
					output[position] = 0;
					const char *line = output + lineStart;
					const char *t = strstr(line, " T ");

					if (t) {
						fprintf(f, "{ (void *) 0x%.*s, \"%s\" },\n", (int) (t - line), line, t + 3);
					}

					lineStart = position + 1;
				}

				position++;
			}

			fclose(f);

			Execute(toolchainCXX, "-c", "kernel/symbols.cpp", "-o", "bin/kernel_symbols.o", 
					ArgString(cppCompileFlags), ArgString(kernelCompileFlags), ArgString(commonCompileFlags));
			DeleteFile("bin/kernel_symbols.tmp");
		}
	}

	Execute(toolchainCXX, "-T", "util/linker64.ld", "-o", "bin/Kernel.esx_symbols", "bin/kernel_symbols.o", "bin/kernel_all.o", "-mno-red-zone", ArgString(kernelLinkFlags));
	Execute(toolchainStrip, "-o", "root/Essence/Kernel.esx", "--strip-all", "bin/Kernel.esx_symbols");
}

void BuildKernel(Application *application) {
	ExecuteForApp(application, toolchainNasm, "-MD", "bin/kernel2.d", "-D", "COM_OUTPUT", 
			"-felf64", "kernel/x86_64.s", "-o", "bin/kernel_x86_64.o", "-Fdwarf");
	ExecuteForApp(application, toolchainCXX, "-MD", "-c", "kernel/main.cpp", "-o", "bin/kernel.o", 
			ArgString(kernelCompileFlags), ArgString(cppCompileFlags), ArgString(commonCompileFlags));
}

void BuildBootloader(Application *application) {
	ExecuteForApp(application, toolchainNasm, "-MD", "bin/boot1.d", "-fbin", 
			inBuildFull ? "boot/x86/mbr-emu.s" : "boot/x86/mbr.s", "-obin/mbr");
	ExecuteForApp(application, toolchainNasm, "-MD", "bin/boot2.d", "-fbin", 
			"boot/x86/esfs-stage1.s", "-obin/stage1");
	ExecuteForApp(application, toolchainNasm, "-MD", "bin/boot3.d", "-fbin", 
			"boot/x86/loader.s", "-obin/stage2", 
			"-Pboot/x86/esfs-stage2.s", inBuildFull ? "-D BOOT_USE_VBE" : "-D BOOT_NO_VIDEO");
}

FILE *_drive;
uint64_t _partitionOffset;

void ReadBlock(uint64_t block, uint64_t count, void *buffer) {
	fseek(_drive, block * blockSize + _partitionOffset, SEEK_SET);
	// printf("read of block %ld\n", block);

	if (fread(buffer, 1, blockSize * count, _drive) != blockSize * count) {
		printf("Error: Could not read blocks %d->%d of drive.\n", (int) block, (int) (block + count));
		exit(1);
	}
}

void WriteBlock(uint64_t block, uint64_t count, void *buffer) {
	fseek(_drive, block * blockSize + _partitionOffset, SEEK_SET);
	assert(block < 4294967296);

	if (fwrite(buffer, 1, blockSize * count, _drive) != blockSize * count) {
		printf("Error: Could not write to blocks %d->%d of drive.\n", (int) block, (int) (block + count));
		exit(1);
	}
}

void WriteBytes(uint64_t offset, uint64_t count, void *buffer) {
	fseek(_drive, offset + _partitionOffset, SEEK_SET);

	if (fwrite(buffer, 1, count, _drive) != count) {
		printf("Error: Could not write to bytes %d->%d of drive.\n", (int) offset, (int) (offset + count));
		exit(1);
	}
}

void Install(const char *driveFile, uint64_t partitionSize, const char *partitionLabel) {
	fprintf(stderr, "Installing...\n");

	EsUniqueIdentifier installationIdentifier;

	for (int i = 0; i < 16; i++) {
		installationIdentifier.d[i] = rand();
	}

	FILE *f = fopen(driveFile, "wb");

	FILE *mbr = fopen("bin/mbr", "rb");
	char mbrBuffer[446] = {};
	fread(mbrBuffer, 1, 446, mbr);
	fwrite(mbrBuffer, 1, 446, f);
	fclose(mbr);

	uint32_t partitions[16] = { 0x80, 0x83, 0x800, (uint32_t) ((partitionSize / 0x200) - 0x800) };
	uint16_t bootSignature = 0xAA55;
	fwrite(partitions, 1, 64, f);
	fwrite(&bootSignature, 1, 2, f);

	void *blank = calloc(1, 0x800 * 0x200 - 0x200);
	fwrite(blank, 1, 0x800 * 0x200 - 0x200, f);

	FILE *stage1 = fopen("bin/stage1", "rb");
	char stage1Buffer[0x200] = {};
	fread(stage1Buffer, 1, 0x200, stage1);
	fwrite(stage1Buffer, 1, 0x200, f);
	fclose(stage1);

	FILE *stage2 = fopen("bin/stage2", "rb");
	char stage2Buffer[0x200 * 7] = {};
	fread(stage2Buffer, 1, 0x200 * 7, stage2);
	fwrite(stage2Buffer, 1, 0x200 * 7, f);
	fclose(stage2);

	fclose(f);

	size_t kernelBytes;
	void *kernel = LoadFile("root/Essence/Kernel.esx", &kernelBytes);

	if (truncate(driveFile, partitionSize)) {
		printf("Error: Could not change the file's size to %d bytes.\n", (int) partitionSize);
		exit(1);
	}

	_drive = fopen(driveFile, "r+b");
	_partitionOffset = 1048576;
	Format(partitionSize - _partitionOffset, partitionLabel, installationIdentifier, kernel, kernelBytes);

	fprintf(stderr, "Copying files to the drive... ");

	ImportNode root = {};
	CreateImportNode("root", &root);
	CreateImportNode("res/Fonts", ImportNodeMakeDirectory(ImportNodeFindChild(&root, "Essence"), "Fonts"));
	CreateImportNode("res/Icons", ImportNodeMakeDirectory(ImportNodeFindChild(&root, "Essence"), "Icons"));
	CreateImportNode("res/Media", ImportNodeMakeDirectory(ImportNodeFindChild(&root, "Essence"), "Media"));
	ImportNodeAddFile(ImportNodeFindChild(&root, "Essence"), "Theme.dat", "res/Theme.dat");

	MountVolume();
	Import(root, superblock.root);
	UnmountVolume();
	fprintf(stderr, "(%u MB)\n", (unsigned) (copiedCount / 1048576));

	fclose(_drive);
}

//////////////////////////////////

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: build_core <configuration>\n");
		return 1;
	}

	EsINIState s = {};
	s.buffer = (char *) LoadFile(argv[1], &s.bytes);

	if (!s.buffer) {
		fprintf(stderr, "Error: could not load configuration file '%s'.\n", argv[1]);
		return 1;
	}

	char **applicationManifests = NULL;
	bool skipCompile = false;

	const char *driveFile = NULL;
	uint64_t partitionSize = 0;
	const char *partitionLabel = "New Volume";

#ifdef PARALLEL_BUILD
	size_t threadCount = 1;
#endif

	while (EsINIParse(&s)) {
		EsINIZeroTerminate(&s);

		if (0 == strcmp(s.section, "toolchain")) {
			if (0 == strcmp(s.key, "path")) {
				executeEnvironment[0] = (char *) malloc(5 + s.valueBytes + 1);
				strcpy(executeEnvironment[0], "PATH=");
				strcat(executeEnvironment[0], s.value);
			} else if (0 == strcmp(s.key, "ar")) {
				toolchainAR = s.value;
			} else if (0 == strcmp(s.key, "cc")) {
				toolchainCC = s.value;
			} else if (0 == strcmp(s.key, "cxx")) {
				toolchainCXX = s.value;
			} else if (0 == strcmp(s.key, "ld")) {
				toolchainLD = s.value;
			} else if (0 == strcmp(s.key, "nm")) {
				toolchainNM = s.value;
			} else if (0 == strcmp(s.key, "strip")) {
				toolchainStrip = s.value;
			} else if (0 == strcmp(s.key, "nasm")) {
				toolchainNasm = s.value;
			}
		} else if (0 == strcmp(s.sectionClass, "application")) {
			if (0 == strcmp(s.key, "manifest")) {
				arrput(applicationManifests, s.value);
			}
		} else if (0 == strcmp(s.section, "options")) {
			if (0 == strcmp(s.key, "Dependency.ACPICA") && atoi(s.value)) {
				strcat(kernelLinkFlags, " -lacpica -Lports/acpica ");
				strcat(kernelCompileFlags, " -DUSE_ACPICA ");
			} else if (0 == strcmp(s.key, "Dependency.stb_image") && atoi(s.value)) {
				strcat(commonCompileFlags, " -DUSE_STB_IMAGE ");
			} else if (0 == strcmp(s.key, "Dependency.stb_sprintf") && atoi(s.value)) {
				strcat(commonCompileFlags, " -DUSE_STB_SPRINTF ");
			} else if (0 == strcmp(s.key, "Dependency.HarfBuzz") && atoi(s.value)) {
				strcat(apiLinkFlags2, " -lharfbuzz ");
				strcat(commonCompileFlags, " -DUSE_HARFBUZZ ");
			} else if (0 == strcmp(s.key, "Dependency.FreeType") && atoi(s.value)) {
				strcat(apiLinkFlags2, " -lfreetype ");
				strcat(commonCompileFlags, " -DUSE_FREETYPE ");
			}
		} else if (0 == strcmp(s.section, "general")) {
			if (0 == strcmp(s.key, "first_application")) {
				firstApplication = s.value;
			} else if (0 == strcmp(s.key, "skip_compile")) {
				skipCompile = !!atoi(s.value);
			} else if (0 == strcmp(s.key, "verbose")) {
				verbose = !!atoi(s.value);
			} else if (0 == strcmp(s.key, "in_build_full")) {
				inBuildFull = !!atoi(s.value);
			} else if (0 == strcmp(s.key, "optimise")) {
				if (atoi(s.value)) {
					strcat(commonCompileFlags, " -O2 ");
				}
			} else if (0 == strcmp(s.key, "colored_output")) {
				if (atoi(s.value)) {
					strcat(commonCompileFlags, " -fdiagnostics-color=always ");
					useColoredOutput = true;
				}
			} else if (0 == strcmp(s.key, "common_compile_flags")) {
				strcat(commonCompileFlags, s.value);
			} else if (0 == strcmp(s.key, "thread_count")) {
#ifndef PARALLEL_BUILD
				fprintf(stderr, "Warning: thread_count not supported.\n");
#else
				threadCount = atoi(s.value);
				if (threadCount < 1) threadCount = 1;
				if (threadCount > 100) threadCount = 100;
#endif
			}
		} else if (0 == strcmp(s.section, "install")) {
			if (0 == strcmp(s.key, "file")) {
				driveFile = s.value;
			} else if (0 == strcmp(s.key, "partition_label")) {
				partitionLabel = s.value;
			} else if (0 == strcmp(s.key, "partition_size")) {
				partitionSize = atoi(s.value) * 1048576UL;
			}
		}

		if (0 != strcmp(s.section, "install")) {
			configurationHash = CalculateCRC64(s.sectionClass, s.sectionClassBytes, configurationHash);
			configurationHash = CalculateCRC64(s.section, s.sectionBytes, configurationHash);
			configurationHash = CalculateCRC64(s.key, s.keyBytes, configurationHash);
			configurationHash = CalculateCRC64(s.value, s.valueBytes, configurationHash);
		}
	}

	sh_new_strdup(applicationDependencies);
	DependenciesListRead();
	buildStartTimeStamp = time(NULL);

	MakeDirectory("bin");
	MakeDirectory("root");
	MakeDirectory("root/Applications");
	MakeDirectory("root/Essence");
	MakeDirectory("root/Essence/Modules");

	if (!skipCompile) {
		{
			Execute(toolchainNasm, "-felf64", "desktop/crti.s", "-o", "bin/crti.o", "-Fdwarf");
			Execute(toolchainNasm, "-felf64", "desktop/crtn.s", "-o", "bin/crtn.o", "-Fdwarf");

			Execute(toolchainCXX, "-c", "desktop/glue.cpp", "-o" "bin/glue.o", ArgString(cppCompileFlags), ArgString(commonCompileFlags));
			Execute(toolchainAR, "-rcs", "root/Applications/POSIX/lib/libglue.a", "bin/glue.o");
		}

#define ADD_DEPENDENCY_FILE(application, _path, _name) \
		{ \
			DepdendencyFile file = {}; \
			strcpy(file.path, _path); \
			file.name = _name; \
			arrput(application.dependencyFiles, file); \
		}

		{
			Application application = {};
			application.name = "Bootloader";
			application.buildCallback = BuildBootloader;
			ADD_DEPENDENCY_FILE(application, "bin/boot1.d", "Boot1");
			ADD_DEPENDENCY_FILE(application, "bin/boot2.d", "Boot2");
			ADD_DEPENDENCY_FILE(application, "bin/boot3.d", "Boot3");
			arrput(applications, application);
		}

		{
			Application application = {};
			application.name = "Desktop";
			application.buildCallback = BuildDesktop;
			ADD_DEPENDENCY_FILE(application, "bin/api1.d", "API1");
			ADD_DEPENDENCY_FILE(application, "bin/api2.d", "API2");
			ADD_DEPENDENCY_FILE(application, "bin/api3.d", "API3");
			arrput(applications, application);
		}

		for (uintptr_t i = 0; i < arrlenu(applicationManifests); i++) {
			ParseApplicationManifest(applicationManifests[i]);
		}

		ParseKernelConfiguration();

		{
			Application application = {};
			application.name = "Kernel";
			application.buildCallback = BuildKernel;
			ADD_DEPENDENCY_FILE(application, "bin/kernel.d", "Kernel1");
			ADD_DEPENDENCY_FILE(application, "bin/kernel2.d", "Kernel2");
			arrput(applications, application);
		}

		// Check which applications need to be rebuilt.

		for (uintptr_t i = 0; i < arrlenu(applications); i++) {
			bool rebuild = false;

			for (uintptr_t j = 0; j < arrlenu(applications[i].dependencyFiles); j++) {
				if (CheckDependencies(applications[i].dependencyFiles[j].name)) {
					rebuild = true;
					break;
				}
			}

			if (!rebuild && arrlenu(applications[i].dependencyFiles)) {
				applications[i].skipped = true;
			}
		}

		// Build all these applications.

#ifdef PARALLEL_BUILD
		if (useColoredOutput) StartSpinner();
		pthread_t *threads = (pthread_t *) malloc(sizeof(pthread_t) * threadCount);

		for (uintptr_t i = 0; i < threadCount; i++) {
			pthread_create(threads + i, NULL, BuildApplicationThread, NULL);
		}

		for (uintptr_t i = 0; i < threadCount; i++) {
			pthread_join(threads[i], NULL);
		}

		if (useColoredOutput) StopSpinner();
#else
		for (uintptr_t i = 0; i < arrlenu(applications); i++) {
			if (applications[i].skipped) continue;
			applications[i].buildCallback(&applications[i]);
		}
#endif

		// Output information about the built applications,
		// and parse the dependency files for successfully built ones.

		bool firstEmptyOutput = true;

		for (uintptr_t i = 0; i < arrlenu(applications); i++) {
			if (applications[i].skipped) {
				continue;
			}

			if (!applications[i].error && !arrlenu(applications[i].output)) {
				if (firstEmptyOutput) {
					firstEmptyOutput = false;
					fprintf(stderr, "Compiled ");
				} else {
					fprintf(stderr, ", ");
				}

				fprintf(stderr, COLOR_HIGHLIGHT "%s" COLOR_NORMAL, applications[i].name);
			}
		}

		if (!firstEmptyOutput) {
			fprintf(stderr, ".\n");
		}

		for (uintptr_t i = 0; i < arrlenu(applications); i++) {
			if (applications[i].skipped) {
				continue;
			}

			if (applications[i].error) {
				fprintf(stderr, ">> Could not build " COLOR_ERROR "%s" COLOR_NORMAL ".\n", applications[i].name);
			} else if (arrlenu(applications[i].output)) {
				fprintf(stderr, ">> Built " COLOR_HIGHLIGHT "%s" COLOR_NORMAL ".\n", applications[i].name);
			}

			fwrite(applications[i].output, 1, arrlen(applications[i].output), stderr);

			if (!applications[i].error) {
				for (uintptr_t j = 0; j < arrlenu(applications[i].dependencyFiles); j++) {
					ParseDependencies(applications[i].dependencyFiles[j].path, applications[i].dependencyFiles[j].name);
				}
			}
		}

		LinkKernel();
		OutputSystemConfiguration();
	}

	if (driveFile) {
		Install(driveFile, partitionSize, partitionLabel);
	}

	DependenciesListWrite();

	return encounteredErrors;
}
