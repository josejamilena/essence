struct FileType {
	char *name;
	size_t nameBytes;
	int iconID;
	int64_t openHandler;
};

#define KNOWN_FILE_TYPE_DIRECTORY (0)
#define KNOWN_FILE_TYPE_UNKNOWN (1)
DS_ARRAY(FileType) knownFileTypes; 
DS_MAP(char *, uintptr_t /* index into knownFileTypes */) knownFileTypesByExtension;

void AddKnownFileTypes() {
#define ADD_FILE_TYPE(_extension, _name, _iconID) \
	{ \
		FileType type = {}; \
		type.name = (char *) _name; \
		type.iconID = _iconID; \
		uintptr_t index = arrlen(knownFileTypes); \
		arrput(knownFileTypes, type); \
		shput(knownFileTypesByExtension, _extension, index); \
	}

	ADD_FILE_TYPE("", interfaceString_CommonItemFolder, ES_ICON_FOLDER);
	ADD_FILE_TYPE("", interfaceString_CommonItemFile, ES_ICON_UNKNOWN);

	size_t groupCount;
	EsSystemConfigurationGroup *groups = EsSystemConfigurationReadAll(&groupCount); 

	for (uintptr_t i = 0; i < groupCount; i++) {
		EsSystemConfigurationGroup *group = groups + i;

		if (EsStringCompareRaw(group->sectionClass, group->sectionClassBytes, EsLiteral("file_type"))) {
			continue;
		}

		FileType type = {};

		type.name = EsSystemConfigurationGroupReadString(group, "name", -1, &type.nameBytes);
		type.openHandler = EsSystemConfigurationGroupReadInteger(group, "open", -1);

		char *iconName = EsSystemConfigurationGroupReadString(group, "icon", -1);

		if (iconName) {
			type.iconID = EsIconIDFromString(iconName);
			EsHeapFree(iconName);
		}

		char *extension = EsSystemConfigurationGroupReadString(group, "extension", -1);
		uintptr_t index = arrlen(knownFileTypes);
		arrput(knownFileTypes, type);
		shput(knownFileTypesByExtension, extension, index);
	}
}

FileType *FolderEntryGetType(FolderEntry *entry) {
	if (entry->isFolder) {
		return knownFileTypes + KNOWN_FILE_TYPE_DIRECTORY;
	} else {
		String extension = entry->GetExtension();
		char buffer[32];
		uintptr_t i = 0;

		for (; i < extension.bytes && i < 31; i++) {
			if (EsCRTisupper(extension.text[i])) {
				buffer[i] = EsCRTtolower(extension.text[i]);
			} else {
				buffer[i] = extension.text[i];
			}
		}

		buffer[i] = 0;

		uintptr_t index = shget(knownFileTypesByExtension, buffer);
		return knownFileTypes + (index ? index : KNOWN_FILE_TYPE_UNKNOWN);
	}
}
