#if 1

#define ES_INSTANCE_TYPE Instance
#include <essence.h>
#include <math.h>
#include <algorithm>
#include <functional>
#include <shared/stb_ds.h>

#define malloc EsCRTmalloc
#define free EsCRTfree
#define realloc EsCRTrealloc
#define strcpy EsCRTstrcpy
#define strcmp EsCRTstrcmp
#define memcmp EsCRTmemcmp
#define strlen EsCRTstrlen
#define strcat EsCRTstrcat
#define memcpy EsCRTmemcpy
#define assert EsAssert
#define memmove EsCRTmemmove

#if 0
struct Instance : EsInstance {
	EsWindow *window;
	// EsTextPlan *plan;
};
#endif

// EsTextPlanProperties properties = {};
// const char *string = "Hello, world.";

#if 0
EsTextRun textRuns[] = {
#if 1
	{ { .size = 36, .color = 0xFFFF0000 }, 5 },
	{ { .size = 36, .color = 0xFF000000 }, 2 },
	{ { .size = 36, .color = 0xFF0000FF }, 5 },
	{ { .size = 36, .color = 0xFF000000 }, 1 },
	{ { .size = 36, .color = 0xFF412757 }, (uint32_t) EsCStringLength(string) - 14 },
	{ { .size = 36, .color = 0xFF000000 }, 1 },
#else
	{ { .font = { .family = ES_FONT_SERIF }, .size = 9, .color = 0xFF000000 }, 5 },
	{ { .size = 14, .color = 0xFF000000 }, 5 },
	{ { .size = 9, .color = 0xFF000000 }, (uint32_t) EsCStringLength(string) - 10 },
#endif
};

void ProcessDisplayMessage(EsElement *element, EsMessage *message) {
	Instance *instance = element->GetInstance();
	*response = ES_HANDLED;

	if (message->type == ES_MSG_PAINT) {
		EsDrawText(message->painter, instance->plan, EsPainterBoundsInset(message->painter));
	} else if (message->type == ES_MSG_MEASURE) {
		if (instance->plan) instance->plan->Destroy();
		EsRectangle insets = element->GetInsets();
		instance->plan = EsTextPlanCreate(&properties, ES_MAKE_RECTANGLE(0, message->measure.width ? (message->measure.width - insets.left - insets.right) : 0, 0, 0), 
				string, textRuns, sizeof(textRuns) / sizeof(textRuns[0]), false);
		message->measure.width = instance->plan->GetWidth();
		message->measure.height = instance->plan->GetHeight();
	} else {
		*response = 0;
	}
}
#endif

struct Instance : EsInstance {
	EsTextbox *textboxDomainName, *textboxIPAddress, *textboxEchoMessage;
};

const EsStyle styleTestPanel = {
	.inherit = ES_STYLE_PANEL_WINDOW_BACKGROUND,

	.metrics = {
		.mask = ES_THEME_METRICS_INSETS | ES_THEME_METRICS_GAP_MAJOR,
		.insets = { 10, 10, 10, 10 },
		.gapMajor = 10,
	},
};

const EsStyle styleBigParagraph = {
	.inherit = ES_STYLE_TEXT_PARAGRAPH,
	
	.metrics = {
		.mask = ES_THEME_METRICS_TEXT_SIZE,
		.textSize = 100,
	},
};

#if 0
void CreateDialog(EsElement *container, bool hasTextbox) {
	// My response to the last post in this thread: 
	// https://web.archive.org/web/20070825122218/http://www.mollyrocket.com/forums/viewtopic.php?t=192&sid=cb913629aa8310947c0476848a8824dd

	static double sliderSum = 0;
	static EsTextDisplay *errorLabel = nullptr;

	EsButton *checkbox = EsButtonCreate(container, ES_BUTTON_CHECKBOX, {}, "Enable editors");
	EsPanel *editorContainer = EsPanelCreate(container, ES_CELL_NEW_BAND, { .cStyle = "Panel.GroupBox" });
	checkbox->SetCheckBuddy(editorContainer);

	if (hasTextbox) {
		EsTextboxCreate(editorContainer, 0);
	} else {
		EsTextDisplayCreate(editorContainer, ES_FLAGS_DEFAULT, {}, "Don't make the sum more than 20!");

		uint64_t flags = ES_CELL_NEW_BAND | ES_TEXTBOX_EDIT_BASED | ES_TEXTBOX_COMPACT;

		for (uintptr_t i = 0; i < 5; i++) {
			EsTextboxCreate(editorContainer, flags, { .callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(TEXTBOX_NUMBER_UPDATED) {
					sliderSum += message->numberUpdated.delta;
					errorLabel->SetHidden(sliderSum <= 20);
				}
			}})->UseNumberOverlay(true)->Insert("0");
		}
	}

	errorLabel = EsTextDisplayCreate(editorContainer, ES_CELL_NEW_BAND, {}, "Oh no!")->SetHidden();
}
#endif

float AbsoluteFloat(float x) {
	return x > 0 ? x : -x;
}

#if 0
void AnimateTextSizeExample(EsPanel *container) {
	static EsElement *canvas = nullptr;
	static float startFontSize = 12, currentFontSize = 12, targetFontSize = 12;
	static EsPaintTarget startFrame = {}, currentFrame = {}, targetFrame = {};

	EsTextboxCreate(container, ES_TEXTBOX_EDIT_BASED, { .callback = [] (EsElement *element, EsMessage *message) {
		ES_ON(TEXTBOX_NUMBER_UPDATED) {
			if (message->numberUpdated.newValue < 6) {
				message->numberUpdated.newValue = 6;
			} else if (message->numberUpdated.newValue > 100) {
				message->numberUpdated.newValue = 100;
			}

			EsPaintTarget temporary = startFrame;
			startFrame = currentFrame;
			currentFrame = temporary;
			startFontSize = currentFontSize;
			targetFontSize = message->numberUpdated.newValue;

			EsPaintTargetClear(&targetFrame);
			EsRectangle bounds = { 0, 400, 0, 200 };
			EsPainter painter = { bounds, 0, 0, 400, 200, nullptr, &targetFrame };
			EsTextPlanProperties properties = {};
			EsTextRun textRun = { .style = { .size = (uint16_t) targetFontSize, .color = 0xFF000000 }, .bytes = (uint32_t) -1 };
			EsDrawText(&painter, EsTextPlanCreate(&properties, bounds, "Hello, world!", &textRun, 1, true), bounds); 

			canvas->StartAnimating();
		}
	}})->Insert("12")->UseNumberOverlay(true);

	const char *canvasStyle = R"(
		preferredWidth = 400; 
		preferredHeight = 200; 
		border = 1; 
		borderColor = black;
	)";

	EsPaintTargetTake(container->GetWindow(), &startFrame, 400, 200);
	EsPaintTargetTake(container->GetWindow(), &currentFrame, 400, 200);
	EsPaintTargetTake(container->GetWindow(), &targetFrame, 400, 200);

	canvas = EsCustomElementCreate(container, ES_CELL_NEW_BAND, { .cStyleOverride = canvasStyle, .callback = [] (EsElement *element, EsMessage *message) {
		ES_ON(ANIMATE) {
			float speed = 1.01f;
			float amount = (1 - powf(speed, -message->animate.deltaUs / 1000.0f));
			currentFontSize += (targetFontSize - currentFontSize) * amount;

			if (AbsoluteFloat(targetFontSize - currentFontSize) < 0.1f) {
				currentFontSize = targetFontSize;
				message->animate.complete = true;
			}

			canvas->Repaint(true, {});
		}

		ES_ON(PAINT) {
			EsPaintTargetClear(&currentFrame);
			EsPainter painter = { { 0, 400, 0, 200 }, 0, 0, 400, 200, nullptr, &currentFrame };
			float progress = (currentFontSize - startFontSize) / (targetFontSize - startFontSize);
			float startScale = currentFontSize / startFontSize;
			float targetScale = currentFontSize / targetFontSize;
			uint8_t alpha = progress * 255.0f;
			EsDrawSurface2(&painter, startFrame.surface, { 0, 400 * startScale, 0, 200 * startScale }, { 0, 400, 0, 200 }, 0xFF - alpha);
			EsDrawSurface2(&painter, targetFrame.surface, { 0, 400 * targetScale, 0, 200 * targetScale }, { 0, 400, 0, 200 }, alpha);
			EsDrawSurface2(message->painter, currentFrame.surface, EsPainterBoundsClient(message->painter), { 0, 400, 0, 200 });
		}
	}});
}
#else
#if 0
void AnimateTextSizeExample(EsPanel *container) {
	static EsElement *canvas = nullptr;
	static int fontSize = 12;
	static float animationFontSize = 12;

	EsTextboxCreate(container, ES_TEXTBOX_EDIT_BASED, { .callback = [] (EsElement *element, EsMessage *message) {
		ES_ON(TEXTBOX_NUMBER_UPDATED) {
			if (message->numberUpdated.newValue < 6) {
				message->numberUpdated.newValue = 6;
			} else if (message->numberUpdated.newValue > 100) {
				message->numberUpdated.newValue = 100;
			}

			fontSize = message->numberUpdated.newValue;
			canvas->StartAnimating();
		}
	}})->Insert("12")->UseNumberOverlay(true);

#if 0
	const char *canvasStyle = R"(
		preferredWidth = 400; 
		preferredHeight = 200; 
		border = 1; 
		inset = 5; 
		borderColor = black;
	)";
#endif

	canvas = EsCustomElementCreate(container, ES_CELL_NEW_BAND, { .callback = [] (EsElement *element, EsMessage *message) {
		ES_ON(ANIMATE) {
			float amount = (1 - powf(1.01f, -message->animate.deltaUs / 1000.0f));
			animationFontSize += (fontSize - animationFontSize) * amount;

			if (AbsoluteFloat(fontSize - animationFontSize) < 1.0f) {
				animationFontSize = fontSize;
				message->animate.complete = true;
			}

			canvas->Repaint(true, {});
		}

		ES_ON(PAINT) {
			EsRectangle bounds = EsPainterBoundsInset(message->painter);
			EsTextPlanProperties properties = {};
			const char *string = "Hello, world!";
			EsTextRun textRun = { .style = { .size = (uint16_t) animationFontSize, .color = 0xFF000000 }, .bytes = (uint32_t) EsCStringLength(string) };
			EsDrawText(message->painter, EsTextPlanCreate(&properties, bounds, string, &textRun, 1, true), bounds); 
		}
	}});
}
#endif
#endif

void AudioTest() {
	EsAudioStream *stream = EsAudioStreamOpen(ES_AUDIO_DEFAULT_OUTPUT, 10 * 1000 /* 10ms */);

	if (!stream || stream->format.sampleFormat != ES_SAMPLE_FORMAT_S16LE || stream->format.channels != 2) {
		// TODO Support more formats.
		return;
	}	

	float phase = 0;
	uint8_t *buffer = (uint8_t *) stream->buffer;
	size_t samplesSent = 0;

	while (samplesSent < stream->format.sampleRate * 10) {
		while (true) {
			uint32_t next = stream->writePointer + 4;
			if (next == stream->bufferBytes) next = 0;
			if (next == stream->readPointer) break;

			float sum = phase;
			phase += 180.0f / stream->format.sampleRate;
			if (phase > 1.0f) phase -= 2.0f;

			*(uint16_t *) (buffer + stream->writePointer + 0) = (uint16_t) (sum * 10000.0f);
			*(uint16_t *) (buffer + stream->writePointer + 2) = (uint16_t) (sum * 10000.0f);

			stream->writePointer = next;
			samplesSent++;
		}

		stream->control = ES_AUDIO_STREAM_RUNNING;
		EsAudioStreamNotify(stream);
		EsWaitSingle(stream->handle);
	}

	stream->control = 0;
	EsAudioStreamNotify(stream);
	EsAudioStreamClose(stream);
}

EsHandle sink, event1, event2, event3, event4, event5;

void EventSinkTest() {
	sink = EsEventSinkCreate(true /* ignore duplicates */);

	EsGeneric data;
	EsError error = EsEventSinkPop(sink, &data);
	EsPrint("data = %d, error = %d\n", data.i, error);

	event1 = EsEventCreate(false);
	event2 = EsEventCreate(false);
	event3 = EsEventCreate(false);
	event4 = EsEventCreate(false);
	event5 = EsEventCreate(false);

	EsEventForward(event1, sink, 1);
	EsEventForward(event1, sink, 1);
	EsEventForward(event2, sink, 2);
	EsEventForward(event3, sink, 3);
	EsEventForward(event4, sink, 4);
	EsEventForward(event5, sink, 5);
	EsEventForward(event5, sink, 5);

	EsThreadCreate([] (EsGeneric) {
		while (true) {
			EsWaitSingle(sink);

			EsGeneric data;
			EsError error = EsEventSinkPop(sink, &data);

			EsPrint("data = %d, error = %d\n", data.i, error);

			if (data.u == 100) {
				EsHandleClose(sink);
			}

			break;
		}
	}, nullptr, nullptr);

	EsSleep(1000); 

	EsEventSet(event1);
	EsEventSet(event2);
	EsEventSet(event3);
	EsEventSet(event4);
	EsEventSet(event5); 

	EsSleep(1000); 

	EsEventSet(event1);
	EsEventSet(event2);
	EsEventSet(event3);
	EsEventSet(event4);
	EsEventSet(event5); 

	EsSleep(1000); 

	EsEventSet(event1);
	EsEventSet(event2);
	EsEventSet(event3);
	EsEventSet(event4);
	EsEventSet(event5); 

	EsSleep(1000); 

	EsEventSinkPush(sink, 100);

	EsHandleClose(event1);
	EsHandleClose(event2);
	EsHandleClose(event3);
	EsHandleClose(event4);
	EsHandleClose(event5);
}

void FileCacheTest() {
	static EsFileInformation node;
	node = EsFileOpen(EsLiteral("/large.txt"), ES_FILE_WRITE_EXCLUSIVE);

	uint8_t *buffer = (uint8_t *) EsHeapAllocate(4096, false);
	EsMemoryFill(buffer, buffer + 4096, EsRandomU8());
	EsFileWriteSync(node.handle, 0, 4096, buffer);

#if 0
	for (uintptr_t i = 0; i < 10; i++) {
		EsThreadInformation thread;

		EsThreadCreate([] (EsGeneric) {
			uint8_t *buffer = (uint8_t *) EsHeapAllocate(4096, false);
			EsMemoryFill(buffer, buffer + 4096, EsRandomU8());
			int index = 0;

			while (true) {
				uint64_t position = EsRandomU64() % (node.fileSize - 4096);
				if ((index % 100) == 0) EsPrint("write %d %d %x\n", index, buffer[0], position);
				index++;
				EsFileWriteSync(node.handle, position, 4096, buffer);
			}
		}, &thread, 0);
	}
#endif

	while (true);

#if 0
	EsNodeInformation node;
	EsNodeOpen(EsLiteral("/test.txt"), ES_NODE_READ_ACCESS | ES_NODE_WRITE_ACCESS | ES_NODE_RESIZE_BLOCK, &node);

	char *comparison2 = (char *) EsFileReadAll(EsLiteral("/test2.txt"), nullptr);

	for (uintptr_t i = 0; i < 1; i++) {
		int start = (EsRandomU64() << 11) % 262144;
		int end = (EsRandomU64() << 11) % 262144;
		end += 2048;

		if (start > end) {
			int temp = start;
			start = end;
			end = temp;
		}

		char *data = (char *) EsHeapAllocate(end - start, true);
		EsMemoryFill(data, data + end - start, EsRandomU8());
		EsFileWriteSync(node.handle, start, end - start, data);
		EsMemoryCopy(comparison2 + start, data, end - start);

		char *comparison = (char *) EsFileReadAll(EsLiteral("/test.txt"), nullptr);

		EsPrint("%x->%x\n", start, end);

		if (EsMemoryCompare(comparison, comparison2, 262144)) {
			EsPrint("Fail\n");
			while (true);
		}
	}

	while (true);

	EsSyscall(ES_SYSCALL_DEBUG_COMMAND, 1, 0, 0, 0); 
#endif

#if 0
	for (uintptr_t i = 0; i < 512; i++) {
		EsAssert(buffer[i] == buffer[i + 512], "File cache test fail.\n");
	}

	uint8_t *mapped = (uint8_t *) EsObjectMap(node.handle, 0, node.fileSize, ES_MAP_OBJECT_READ_ONLY); 
	EsFileWriteSync(node.handle, 4096, 4096, mapped);
	EsFileReadSync(node.handle, 0, 8192, buffer);

	for (uintptr_t i = 0; i < 4096; i++) {
		EsAssert(buffer[i] == buffer[i + 4096], "File cache test fail.\n");
	}
#endif

#if 0
	char buffer[262144];

	EsPrint("start...\n");

	for (uintptr_t i = 0; i < node.fileSize; i += sizeof(buffer)) {
		EsFileReadSync(node.handle, i, sizeof(buffer), buffer);
		// EsPrint("%D\n", i);
	}

	EsPrint("end...\n");
#endif
}

__thread int threadVariable;

bool commitMarked[100];
int commitCount;
uint8_t *commitBase;

void CheckCommit() {
	for (uintptr_t i = 0; i < 100; i++) {
		EsSyscall(ES_SYSCALL_DEBUG_COMMAND, 4, (uintptr_t) commitBase, i, commitCount | ((uintptr_t) commitMarked[i] << 63));
	}
}

void CommitSet(uintptr_t from, uintptr_t to) {
	EsPrint("Set %d -> %d.\n", from, to);

	for (uintptr_t i = from; i < to; i++) {
		if (!commitMarked[i]) {
			commitCount++;
		}

		commitMarked[i] = true;
	}

	EsMemoryCommit(commitBase + from * ES_PAGE_SIZE, (to - from) * ES_PAGE_SIZE);
	CheckCommit();
}

void CommitClear(uintptr_t from, uintptr_t to) {
	EsPrint("Clear %d -> %d.\n", from, to);
		
	for (uintptr_t i = from; i < to; i++) {
		if (commitMarked[i]) {
			commitCount--;
		}

		commitMarked[i] = false;
	}

	EsMemoryDecommit(commitBase + from * ES_PAGE_SIZE, (to - from) * ES_PAGE_SIZE);
	CheckCommit();
}

void CommitTest() {
	commitBase = (uint8_t *) EsMemoryReserve(ES_PAGE_SIZE * 100, ES_MEMORY_PROTECTION_READ_WRITE, false); 
	CheckCommit();

	CommitSet(0, 10);
	CommitSet(0, 5);
	CommitSet(0, 15);
	CommitSet(0, 10);
	CommitClear(10, 15);
	CommitClear(8, 12);
	CommitClear(8, 15);
	CommitClear(6, 15);
	CommitSet(0, 10);
	CommitSet(0, 12);
	CommitSet(5, 12);
	CommitSet(5, 20);
	CommitClear(5, 10);
	CommitClear(0, 6);
	CommitClear(9, 21);

	// CommitSet(10, 40);
	// CommitClear(5, 20);
	CommitSet(20, 40);
	CommitSet(15, 30);

	while (true) {
		uintptr_t from = EsRandomU64() % 100, to = EsRandomU64() % 100;
		if (from >= to) continue;

		if (EsRandomU64() & 1) {
			CommitSet(from, to);
		} else {
			CommitClear(from, to);
		}
	}

	EsMemoryUnreserve(commitBase); 
}

void RunTCPTest(Instance *instance, EsElement *, EsCommand *) {
	char message[256];
	size_t messageBytes;
	EsError error;

	uint8_t *buffer = nullptr;
	uintptr_t totalExpectedBytes = 0;

	const char *domain = "handmade.network";
	const char *request = "GET / HTTP/1.1\r\nHost: handmade.network\r\nUser-Agent: curl/7.74.0\r\nAccept: */*\r\n\r\n";
	size_t requestBytes = EsCStringLength(request);

	EsConnection connection = {};
	connection.sendBufferBytes = 65536;
	connection.receiveBufferBytes = 65536;

	error = EsAddressResolve(EsLiteral(domain), ES_FLAGS_DEFAULT, &connection.address);

	if (error != ES_SUCCESS) {
		messageBytes = EsStringFormat(message, sizeof(message), "Could not resolve domain '%z' (%d).", domain, error);
		goto failed;
	}

	connection.address.port = 80;

	error = EsConnectionOpen(&connection, ES_FLAGS_DEFAULT);

	if (error != ES_SUCCESS) {
		messageBytes = EsStringFormat(message, sizeof(message), "Could not begin opening the connection (%d).", error);
		goto failed;
	}

	while (!connection.open && connection.error == ES_SUCCESS) {
		EsConnectionPoll(&connection);
	}

	if (connection.error != ES_SUCCESS) {
		messageBytes = EsStringFormat(message, sizeof(message), "An error occured before the connection was open (%d).", connection.error);
		goto failed;
	}

	EsMemoryCopy(connection.sendBuffer, request, requestBytes);
	connection.sendWritePointer = requestBytes;
	EsConnectionNotify(&connection);

	while (!totalExpectedBytes || arrlenu(buffer) < totalExpectedBytes) {
		EsConnectionPoll(&connection);

		if (connection.error != ES_SUCCESS) {
			messageBytes = EsStringFormat(message, sizeof(message), "An error occured while receiving data (%d).", connection.error);
			goto failed;
		}

		if (connection.receiveReadPointer == connection.receiveWritePointer) {
			continue;
		}

		size_t bufferBytes = arrlenu(buffer);

		if (connection.receiveReadPointer < connection.receiveWritePointer) {
			EsPrint("reading %D\n", connection.receiveWritePointer - connection.receiveReadPointer);
			arrinsn(buffer, bufferBytes, connection.receiveWritePointer - connection.receiveReadPointer);
			EsMemoryCopy(buffer + bufferBytes, connection.receiveBuffer + connection.receiveReadPointer, 
					connection.receiveWritePointer - connection.receiveReadPointer);
		} else {
			EsPrint("reading (1) %D\n", connection.receiveBufferBytes - connection.receiveReadPointer);
			arrinsn(buffer, bufferBytes, connection.receiveBufferBytes - connection.receiveReadPointer);
			EsMemoryCopy(buffer + bufferBytes, connection.receiveBuffer + connection.receiveReadPointer, 
					connection.receiveBufferBytes - connection.receiveReadPointer);
			bufferBytes += connection.receiveBufferBytes - connection.receiveReadPointer;
			EsPrint("reading (2) %D\n", connection.receiveWritePointer);
			arrinsn(buffer, bufferBytes, connection.receiveWritePointer);
			EsMemoryCopy(buffer + bufferBytes, connection.receiveBuffer, 
					connection.receiveWritePointer);
		}

		connection.receiveReadPointer = connection.receiveWritePointer;
		EsConnectionNotify(&connection);

		if (!totalExpectedBytes) {
			for (uintptr_t i = 0; i < arrlenu(buffer) - 3; i++) {
				if (buffer[i] == '\r' && buffer[i + 1] == '\n' && buffer[i + 2] == '\r' && buffer[i + 3] == '\n') {
					const char *needle = "Content-Length: ";
					size_t needleBytes = EsCStringLength(needle);

					for (uintptr_t j = 0; j <= i - needleBytes; j++) {
						uintptr_t integerBytes = 0;

						for (uintptr_t k = 0; k < needleBytes; k++) {
							if (buffer[j + k] != needle[k]) {
								goto nextPosition;
							}
						}

						for (uintptr_t k = 0; k < i - j - needleBytes; k++, integerBytes++) {
							if (buffer[j + k + needleBytes] == '\r') {
								break;
							}
						}

						totalExpectedBytes = i + 4 + EsIntegerParse((char *) (buffer + j + needleBytes), integerBytes); 
						EsPrint("totalExpectedBytes = %D\n", totalExpectedBytes);
						break;

						nextPosition:;
					}

					break;
				}
			}
		}

		if (arrlenu(buffer) > 5) {
			size_t bufferBytes = arrlenu(buffer);

			if (buffer[bufferBytes - 5] == '0'
					&& buffer[bufferBytes - 4] == '\r'
					&& buffer[bufferBytes - 3] == '\n'
					&& buffer[bufferBytes - 2] == '\r'
					&& buffer[bufferBytes - 1] == '\n') {
				break;
			}
		}
	}

	EsPrint("Received %D: %s\n", arrlenu(buffer), arrlenu(buffer), buffer);
	EsDialogShowAlert(instance->window, EsLiteral("Test success"), EsLiteral("It worked!"), ES_ICON_DIALOG_INFORMATION, true); 
	EsConnectionClose(&connection);
	return;
	failed:;
	EsDialogShowAlert(instance->window, EsLiteral("Test failed"), message, messageBytes, ES_ICON_DIALOG_ERROR, true); 
	EsConnectionClose(&connection);
}

int IconListView(EsElement *, EsMessage *message) {
	if (message->type == ES_MSG_LIST_VIEW_CREATE_ITEM) {
		EsIconDisplayCreate(message->createItem.parent, ES_ELEMENT_NO_HOVER, {}, message->createItem.index.i);
	}

	return 0;
}

void FileResizeTest2(EsGeneric) {
	EsFileInformation information = EsFileOpen("/Test.dat", -1, ES_FILE_WRITE_EXCLUSIVE);
	EsAssert(information.error == ES_SUCCESS);
	EsHandle file = information.handle;

	char buffer[262144 * 2];
	for (int i = 0; i < 262144 * 2; i++) buffer[i] = EsRandomU8();

	EsFileResize(file, 262144 * 4);
	EsFileResize(file, 0);

	EsFileResize(file, 262144 * 4);
	EsFileWriteSync(file, 0, 262144, &buffer);
	EsFileResize(file, 262144 * 2);
	EsFileResize(file, 0);
	EsSleep(1500);

	EsFileResize(file, 262144 * 4);
	EsFileWriteSync(file, 262144, 262144 * 2, &buffer);
	EsFileWriteSync(file, 262144 * 3, 262144, &buffer);
	EsFileResize(file, 262144 * 3 / 2);
	EsSleep(1500);
	EsFileWriteSync(file, 262144, 262144 * 2, &buffer);

	for (int i = 0; i < 262144 * 2; i++) buffer[i] = 0x8C;

	EsSleep(3000);
	EsFileWriteSync(file, 262144, 262144 * 2, &buffer);
	EsSleep(1500);
	EsFileResize(file, 262144 * 3 / 2);
	EsSleep(1500);

	for (int i = 0; i < 262144 * 2; i++) buffer[i] = 0x4C;

	EsFileWriteSync(file, 262144, 262144 * 2, &buffer);
	EsFileResize(file, 262144 * 3 / 2 - 1);
	EsFileResize(file, 262144 * 4);
	EsMemoryZero(buffer, 262144 * 2);
	EsFileReadSync(file, 262144, 262144 * 2, &buffer);
	for (int i = 0; i < 131072 - 1; i++) EsAssert(buffer[i] == 0x4C);
	for (int i = 131072 - 1; i < 262144 * 2; i++) EsAssert(buffer[i] == 0x00);
	EsSleep(1500);

	EsHandleClose(file);

	EsPrint("Done FileResizeTest2.\n");
}

void FileResizeTest3(EsGeneric data) {
	EsHandle file;
	EsFileInformation information = EsFileOpen("/Test3.dat", -1, ES_FILE_WRITE);
	EsAssert(information.error == ES_SUCCESS);
	file = information.handle;
	int k = 0;

	while (true) {
		EsFileResize(file, (EsRandomU64() % (16 * 1024 * 1024)) & ~(0x3FF));
		EsFileWriteSync(file, (EsRandomU64() % (16 * 1024 * 1024)), (EsRandomU64() % (16 * 1024 * 1024)), data.p);

		if (k++ == 10) {
			EsPrint("Thread %d alive.\n", EsThreadGetID(ES_CURRENT_THREAD));
			k = 0;
		}
	}
}

void FileResizeTest() {
	EsThreadInformation thread;

	EsThreadCreate(FileResizeTest2, &thread, 0); 

	uint8_t *compare = (uint8_t *) EsHeapAllocate(16 * 1024 * 1024, false);

#if 1
	for (uintptr_t i = 0; i < 4; i++) {
		EsThreadCreate(FileResizeTest3, &thread, compare); 
	}
#endif

#if 0
	{
		EsNodeInformation information;
		EsError error = EsNodeOpen("/Test2.dat", -1, ES_FILE_WRITE_EXCLUSIVE, &information);
		EsAssert(error == ES_SUCCESS);
		EsHandle file = information.handle;

		while (true) {
			EsFileResize(file, 256 * 1024 * 1024);
			EsFileWriteSync(file, 1, 1, &error);
			EsSleep(2000);
			EsFileResize(file, 0);
		}
	}
#endif

	{
		// TODO Periodically clear the cache and read from the file system.
		
		EsError error;
		EsHandle file;
		EsFileInformation information = EsFileOpen("/Test2.dat", -1, ES_FILE_WRITE_EXCLUSIVE);
		EsAssert(information.error == ES_SUCCESS);
		file = information.handle;
		uint8_t *data = nullptr;

		EsFileResize(file, 0);
		EsRandomSeed(0);
		uint64_t seed = 0;

#if 0
		uint64_t sizes[] = {
			2000000,
			1571840,
			3442688,
		};

		int index = 0;
#endif

		while (true) {
			size_t size = (EsRandomU64() % (16 * 1024 * 1024)) & ~(0x3FF);
			// size_t size = sizes[index % (sizeof(sizes) / sizeof(uint64_t))];
			// index++;
			size_t old = arrlen(data);
			arrsetlen(data, size);
			EsPrint("Resize to size %d.\n", size);
			EsFileResize(file, size);

			if (old < size) {
				for (uintptr_t i = old; i < size; i++) {
					data[i] = 0;
				}

				EsPrint("Read back zeroes.\n");
				error = EsFileReadSync(file, 0, size, compare);

				if ((size_t) error != size) {
					EsPrint("EsFileReadSync failed\n");
					EsAssert(false);
				}

				if (EsMemoryCompare(compare, data, size)) {
					EsPrint("Read back zeroes failed\nold: %x, size: %x\n", old, size);

					for (uintptr_t i = 0; i < size; i++) {
						if (compare[i] != data[i]) {
							EsPrint("First error at %x\n", i);
							break;
						}
					}

					EsAssert(false);
				}

				for (uintptr_t i = old; i < size; i++) {
					seed = seed * 214013 + 2531011;
					data[i] = seed >> 16;
				}

				EsPrint("Write new data.\n");
				error = EsFileWriteSync(file, old, size - old, data + old);

				if ((size_t) error != size - old) {
					EsPrint("EsFileWriteSync failed with %d\n", error);
					EsAssert(false);
				}
			}

			EsPrint("Read back.\n");
			error = EsFileReadSync(file, 0, size, compare);

			if ((size_t) error != size) {
				EsPrint("EsFileReadSync failed\n");
				EsAssert(false);
			}

			if (EsMemoryCompare(compare, data, size)) {
				EsPrint("Resize test failed\nold: %x, size: %x\n", old, size);

				for (uintptr_t i = 0; i < size; i++) {
					if (compare[i] != data[i]) {
						EsPrint("First error at %x\n", i);
						break;
					}
				}

				EsAssert(false);
			}
		}
	}
}

void FileFlushTest() {
	EsThreadInformation information;

	EsThreadCreate([] (EsGeneric) {
		EsFileInformation information = EsFileOpen(EsLiteral("/test.txt"), ES_FILE_READ_SHARED);
		char buffer[32];

		while (true) {
			EsFileReadSync(information.handle, 0, sizeof(buffer), buffer);
		}
	}, &information, 0);

	const char *string = "Hello, world!\n";

	while (true) {
		EsFileWriteAll(EsLiteral("/test.txt"), EsLiteral(string));
	}
}

////////////////////////////////

struct File {
	char *path;
	char *data;
	int children;
};

File *files;
File *directories;

int createCount, deleteCount, writeCount, checkCount, compareCount, renameCount;
double createTime, deleteTime, writeTime, checkTime, compareTime, renameTime;
#define UPDATE_TIME(x) double start = EsTimeStampMs(); EsDefer(x += EsTimeStampMs() - start);

////////////////////////////////

void PlatformCreateFile(const char *path) {
	UPDATE_TIME(createTime);
	EsPrint("Create %z\n", path);
	EsAssert(ES_SUCCESS == EsPathCreate(path, -1, ES_NODE_FILE, false));
	createCount++;
}

void PlatformDeleteFile(const char *path) {
	UPDATE_TIME(deleteTime);
	EsPrint("Delete %z\n", path);
	EsAssert(ES_SUCCESS == EsPathDelete(path, -1));
	deleteCount++;
}

void PlatformDeleteDirectory(const char *path) {
	UPDATE_TIME(deleteTime);
	EsPrint("Delete directory %z\n", path);
	EsAssert(ES_SUCCESS == EsPathDelete(path, -1));
	deleteCount++;
}

void PlatformWriteFile(const char *path, const char *data, size_t dataBytes) {
	UPDATE_TIME(writeTime);
	EsPrint("Write %z %D\n", path, dataBytes);
	EsAssert(ES_SUCCESS == EsFileWriteAll(path, -1, data, dataBytes));
	writeCount += dataBytes;
}

void PlatformCheckFileDoesNotExist(const char *path) {
	UPDATE_TIME(checkTime);
	EsPrint("Check does not exist %z\n", path);
	EsAssert(!EsPathExists(path, -1));
	checkCount++;
}

void PlatformCompareFile(const char *path, const char *data, size_t dataBytes) {
	UPDATE_TIME(compareTime);
	EsPrint("Compare %z\n", path);
	size_t bytes;
	void *in = EsFileReadAll(path, -1, &bytes);
	EsAssert(bytes == dataBytes);
	EsAssert(0 == EsMemoryCompare(in, data, dataBytes));
	compareCount += dataBytes;
}

void PlatformRenameFile(const char *oldPath, const char *newPath) {
	UPDATE_TIME(renameTime);
	EsPrint("Rename %z %z\n", oldPath, newPath);
	EsAssert(ES_SUCCESS == EsPathMove(EsLiteral(oldPath), EsLiteral(newPath)));
	renameCount++;
}

void PlatformRenameDirectory(const char *oldPath, const char *newPath) {
	PlatformRenameFile(oldPath, newPath);
}

void PlatformCreateDirectory(const char *path) {
	UPDATE_TIME(createTime);
	EsPrint("Create directory %z\n", path);
	EsAssert(ES_SUCCESS == EsPathCreate(path, -1, ES_NODE_DIRECTORY, false));
	createCount++;
}

void PlatformResetDirectory(const char *path) {
	PlatformCreateDirectory(path);
}

void PlatformEnumerateDirectory(const char *path, int _count) {
	EsDirectoryChild *buffer;
	ptrdiff_t count = EsDirectoryEnumerateChildren(path, -1, &buffer); // Free buffer with EsHeapFree. Returns number of children.
	EsAssert(count == _count);

	for (intptr_t i = 0; i < count; i++) {
		char child[4096];
		if ((size_t) EsStringFormat(child, sizeof(child), "%z/%s%c", path, buffer[i].nameBytes, buffer[i].name, 0) >= sizeof(child) - 1) {
			continue;
		}

		bool found = false;

		for (uintptr_t j = 0; j < arrlenu(files); j++) {
			if (0 == strcmp(files[j].path, child)) {
				found = true;
				break;
			}
		}

		for (uintptr_t j = 0; j < arrlenu(directories); j++) {
			if (0 == strcmp(directories[j].path, child)) {
				found = true;
				break;
			}
		}

		if (!found) {
			EsPrint("Could not find %z.\n", child);
			EsAssert(false);
		}
	}

	EsHeapFree(buffer);
}

////////////////////////////////

char *strdup(const char *in) {
	char *out = (char *) malloc(strlen(in) + 1);
	strcpy(out, in);
	return out;
}
		
void FileRename(const char *oldPath, const char *newPath, File *file) {
	if (memcmp(oldPath, file->path, strlen(oldPath))) {
		return;
	}
	
	if (file->path[strlen(oldPath)] != '/') {
		return;
	}
	
	char path[4096];
	assert(strlen(file->path) - strlen(oldPath) + strlen(newPath) < sizeof(path) - 1);
	strcpy(path, file->path);
	memmove(path + strlen(newPath), path + strlen(oldPath), strlen(path) - strlen(oldPath) + 1);
	memcpy(path, newPath, strlen(newPath));
	free(file->path);
	file->path = strdup(path);
}

File *FindParent(File *file) {
	int end = 0;

	for (int i = 0; file->path[i]; i++) {
		if (file->path[i] == '/') {
			end = i;
		}
	}

	EsAssert(end);

	for (int i = 0; i < arrlen(directories); i++) {
		if (0 == memcmp(directories[i].path, file->path, end)
				&& directories[i].path[end] == 0) {
			return directories + i;
		}
	}

	EsAssert(false);
	return nullptr;
}

void DirectoryTest(EsGeneric) {
	// TODO Periodically remount the testing file system.
	// TODO Multi-threaded testing.

	File root = {};
	root.path = strdup("fs_test_data");
	arrput(directories, root);
	PlatformResetDirectory(root.path);

	int testCount = 10000;

	double startTime = EsTimeStampMs();
	
	for (int i = 0; i < testCount; i++) {
		tryAgain:;

		EsPrint("---- Test %d/%d ----\n", i, testCount);
		
		// Pick an action to perform at random.
		int action = rand() % 20;
		
		for (int repeat = 0; repeat < 10; repeat++) {
			if (action < 3) {
				// Create a file or directory.
				
				// Pick a parent directory at random.
				File *parent = directories + (rand() % arrlen(directories));
				
				// Create a file or directory?
				bool makeDirectory = rand() & 1;
				
				// Pick a random name.
				char name[8];
				int nameBytes = (rand() % 4) + 3;
				for (int i = 0; i < nameBytes; i++) name[i] = (rand() % 26) + 'a';
				name[nameBytes] = 0;
				
				// Work out the path.
				char path[4096];
				if ((size_t) EsStringFormat(path, sizeof(path), "%z/%z%c", parent->path, name, 0) >= sizeof(path) - 1) {
					goto tryAgain;
				}
				
				// Check the file does not already exist.
				for (int i = 0; i < arrlen(files); i++) if (0 == strcmp(files[i].path, path)) goto tryAgain;
				for (int i = 0; i < arrlen(directories); i++) if (0 == strcmp(directories[i].path, path)) goto tryAgain;
				
				parent->children++;

				// Create the file.
				File f = {};
				f.path = strdup(path);
				
				if (makeDirectory) {
					PlatformCreateDirectory(path);
					arrput(directories, f);
				} else {
					PlatformCreateFile(path);
					
					// Pick some text at random.
					char text[1024];
					int textBytes = (rand() % 1022) + 1;
					for (int i = 0; i < textBytes; i++) text[i] = (rand() % 26) + 'a';
					text[textBytes] = 0;
					// Set the file contents.
					PlatformWriteFile(f.path, text, textBytes);
					f.data = strdup(text);
					
					arrput(files, f);
				}
				
			} else if (action < 4 && arrlen(files)) {
				// Modify a file's contents.
				File *file = files + (rand() % arrlen(files));
				
				// Pick some text at random.
				char text[1024];
				int textBytes = (rand() % 1022) + 1;
				for (int i = 0; i < textBytes; i++) text[i] = (rand() % 26) + 'a';
				text[textBytes] = 0;
				
				// Set the file contents.
				PlatformCompareFile(file->path, file->data, strlen(file->data));
				PlatformWriteFile(file->path, text, textBytes);
				free(file->data);
				file->data = strdup(text);
			} else if (action < 6 && arrlen(files)) {
				// Delete a file at random.
				int index = rand() % arrlen(files);
				File *file = files + index;
				FindParent(file)->children--;
				PlatformCompareFile(file->path, file->data, strlen(file->data));
				PlatformDeleteFile(file->path);
				PlatformCheckFileDoesNotExist(file->path);
				free(file->path);
				free(file->data);
				arrdel(files, index);
			} else if (action < 8 && arrlen(files)) {
				// Rename a file at random.
				File *file = files + (rand() % arrlen(files));
				char path[4096];
				strcpy(path, file->path);
				
				// Trim the old name from the path.
				for (int i = strlen(path) - 1; i >= 0; i--) {
					if (path[i] == '/') {
						path[i + 1] = 0;
						break;
					}
				}
				
				if (sizeof(path) - strlen(path) < 10) {
					goto tryAgain;
				}
				
				FindParent(file)->children--;
				
				// Pick a random name.
				char name[8];
				int nameBytes = (rand() % 4) + 3;
				for (int i = 0; i < nameBytes; i++) name[i] = (rand() % 26) + 'a';
				name[nameBytes] = 0;
				strcat(path, name);
				
				// Check the file does not already exist.
				for (int i = 0; i < arrlen(files); i++) if (0 == strcmp(files[i].path, path)) goto tryAgain;
				for (int i = 0; i < arrlen(directories); i++) if (0 == strcmp(directories[i].path, path)) goto tryAgain;
				
				// Rename the file.
				PlatformCompareFile(file->path, file->data, strlen(file->data));
				PlatformRenameFile(file->path, path);
				PlatformCheckFileDoesNotExist(file->path);
				free(file->path);
				file->path = strdup(path);
				PlatformCompareFile(file->path, file->data, strlen(file->data));
				FindParent(file)->children++;
			} else if (action < 10 && arrlen(files)) {
				// Validate a file's data at random.
				File *file = files + (rand() % arrlen(files));
				if (!file->data) goto tryAgain;
				PlatformCompareFile(file->path, file->data, strlen(file->data));
			} else if (action < 11 && arrlen(directories)) {
				// Rename a directory at random.
				File *file = directories + (rand() % arrlen(directories));
				
				char path[4096];
				strcpy(path, file->path);
				
				int i = strlen(path) - 1;
				
				// Trim the old name from the path.
				for (; i >= 0; i--) {
					if (path[i] == '/') {
						path[i + 1] = 0;
						break;
					}
				}
				
				// Make sure there's enough room for the new name and a NUL terminator.
				// Also check this isn't the root.
				if (sizeof(path) - strlen(path) < 10 || i < 0) {
					goto tryAgain;
				}

				FindParent(file)->children--;
				
				// Pick a random name.
				char name[8];
				int nameBytes = (rand() % 4) + 3;
				for (int i = 0; i < nameBytes; i++) name[i] = (rand() % 26) + 'a';
				name[nameBytes] = 0;
				strcat(path, name);
				
				// Check the file does not already exist.
				for (int i = 0; i < arrlen(files); i++) if (0 == strcmp(files[i].path, path)) goto tryAgain;
				for (int i = 0; i < arrlen(directories); i++) if (0 == strcmp(directories[i].path, path)) goto tryAgain;
				
				// Rename the directory.
				PlatformRenameDirectory(file->path, path);
				char *oldPath = file->path;
				file->path = strdup(path);
				
				// Rename files in the directory.
				
				for (int i = 0; i < arrlen(files); i++) {
					FileRename(oldPath, path, files + i);
				}
				
				for (int i = 0; i < arrlen(directories); i++) {
					FileRename(oldPath, path, directories + i);
				}
				
				free(oldPath);
				FindParent(file)->children++;
			} else if (action < 14 && arrlen(directories)) {
				// Enumerate a directory at random.
				File *file = directories + (rand() % arrlen(directories));
				PlatformEnumerateDirectory(file->path, file->children);
			} else if (action < 16 && arrlen(directories)) {
				// Delete a directory at random.
				for (int attempt = 0; attempt < 10; attempt++) {
					int index = rand() % arrlen(directories);
					File *file = directories + index;

					if (!file->children && index) {
						FindParent(file)->children--;
						PlatformDeleteDirectory(file->path);
						PlatformCheckFileDoesNotExist(file->path);
						free(file->path);
						arrdel(directories, index);
						break;
					}
				}
			} else if (action < 18 && arrlen(directories) && arrlen(files)) {
				// Move a file at random.
				File *file = files + (rand() % arrlen(files));
				FindParent(file)->children--;
				File *directory = directories + (rand() % arrlen(directories));
				char *newName = (char *) malloc(strlen(directory->path) + 22);
				strcpy(newName, directory->path);
				char end[22];
				end[0] = '/', end[21] = 0;
				for (int i = 1; i <= 20; i++) end[i] = (rand() % 26) + 'a';
				strcat(newName, end);
				PlatformRenameFile(file->path, newName);
				free(file->path);
				file->path = newName;
				PlatformCompareFile(file->path, file->data, strlen(file->data));
				FindParent(file)->children++;
			} else if (action < 20 && arrlen(directories) > 2) {
				// Move a directory at random.
				File *file = directories + (rand() % arrlen(directories));
				File *directory = directories + (rand() % arrlen(directories));
				if (file == directory || file == directories) continue;
				File *oldParent = FindParent(file);
				char *newName = (char *) malloc(strlen(directory->path) + 22);
				strcpy(newName, directory->path);
				char end[22];
				end[0] = '/', end[21] = 0;
				for (int i = 1; i <= 20; i++) end[i] = (rand() % 26) + 'a';
				strcat(newName, end);
				if (0 == memcmp(newName, file->path, strlen(file->path))) continue;
				FindParent(file)->children--;
				PlatformRenameFile(file->path, newName);
				char *oldPath = file->path;
				file->path = newName;
				FindParent(file)->children++;

				for (int i = 0; i < arrlen(files); i++) {
					FileRename(oldPath, newName, files + i);
				}
				
				for (int i = 0; i < arrlen(directories); i++) {
					FileRename(oldPath, newName, directories + i);
				}
				
				free(oldPath);
				PlatformEnumerateDirectory(oldParent->path, oldParent->children);
				PlatformEnumerateDirectory(file->path, file->children);
				PlatformEnumerateDirectory(directory->path, directory->children);
			} else {
				goto tryAgain;
			}

#if 0
			for (int i = 0; i < arrlen(directories); i++) {
				PlatformEnumerateDirectory(directories[i].path, directories[i].children);
			}
#endif
		}
	}

	double endTime = EsTimeStampMs();

	EsPrint("Directory test time: %dms.\n", (int) (endTime - startTime));
	EsPrint("- Create %d, %d\n", createCount, (int) createTime);
	EsPrint("- Delete %d, %d\n", deleteCount, (int) deleteTime);
	EsPrint("- Write %D, %d\n", writeCount, (int) writeTime);
	EsPrint("- Check %d, %d\n", checkCount, (int) checkTime);
	EsPrint("- Compare %D, %d\n", compareCount, (int) compareTime);
	EsPrint("- Rename %d, %d\n", renameCount, (int) renameTime);
}

////////////////////////////////

void ProcessApplicationMessage(EsMessage *message) {
	if (message->type == ES_MSG_INSTANCE_CREATE) {
		Instance *instance = EsInstanceCreate(message, "Test");
		static EsWindow *window;
		window = instance->window;
		// EsWindowSetIcon(window, ES_ICON_COLOR_FILL);
		EsWindowSetTitle(window, "Test Application");

		EsPanel *panel = EsPanelCreate(window, ES_PANEL_VERTICAL | ES_CELL_FILL, &styleTestPanel);

#if 0
		EsButtonOnCommand(EsButtonCreate(panel, ES_FLAGS_DEFAULT, 0, EsLiteral("Crash")), [] (Instance *, EsElement *, EsCommand *) {
			int x = 5, y = 0;
			x /= y;
			EsPrint("5 / 0 = %d\n", x);
		});

		EsButtonOnCommand(EsButtonCreate(panel, ES_FLAGS_DEFAULT, 0, EsLiteral("Wait forever")), [] (Instance *, EsElement *, EsCommand *) {
			while (true);
		});

		EsTextboxCreate(panel, ES_TEXTBOX_EDIT_BASED, 0);
		EsColorWellCreate(panel, ES_COLOR_WELL_HAS_OPACITY);

		EsButtonOnCommand(EsButtonCreate(panel, ES_FLAGS_DEFAULT, 0, EsLiteral("Show alert")), [] (Instance *, EsElement *, EsCommand *) {
			const char *longContent = "Every time I break word-wrapping I have to come up with some random text to test it out, and I never know what to write.";
			EsDialogShowAlert(window, EsLiteral("Title of the alert dialog"), EsLiteral(longContent), ES_ICON_DIALOG_INFORMATION, true); 
		});
#endif

		// EsWindowCreate(instance, ES_WINDOW_INSPECTOR);

#if 0
		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Wait for 10 seconds"))->OnCommand([] EsCommandCallback {
			EsSleep(10 * 1000);
		});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Wait then crash"))->OnCommand([] EsCommandCallback {
			EsSleep(10 * 1000);
			int x = 5, y = 0;
			x /= y;
			EsPrint("5 / 0 = %d\n", x);
		});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Close tab"))->OnCommand([] EsCommandCallback {
			EsInstanceDestroy(element->instance);
		});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Terminate blocking thread"))->OnCommand([] EsCommandCallback {
			EsMutex mutex = {};
			EsMutexAcquire(&mutex);

			EsThreadInformation information;

			EsThreadCreate([] (EsGeneric argument) {
				EsMutexAcquire((EsMutex *) argument.p);
			}, &information, &mutex);

			EsSleep(1000);
			EsThreadTerminate(information.handle);
			EsHandleClose(information.handle);
		});

		instance->textboxDomainName = EsTextboxCreate(panel, ES_CELL_NEW_BAND, {});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Resolve domain name"))->OnCommand([] EsCommandCallback {
			char *domainName = instance->textboxDomainName->GetContents();
			EsAddress address;
			EsError error = EsSyscall(ES_SYSCALL_DOMAIN_NAME_RESOLVE, (uintptr_t) domainName, EsCStringLength(domainName), (uintptr_t) &address, 0);
			char content[256];
			size_t contentBytes;

			if (error == ES_SUCCESS) {
				contentBytes = EsStringFormat(content, sizeof(content), "%d.%d.%d.%d", address.d[0], address.d[1], address.d[2], address.d[3]);
				instance->textboxIPAddress->Clear(true);
				instance->textboxIPAddress->Insert(content, contentBytes);
			} else {
				EsPrint("Error: %d\n", error);
				contentBytes = EsStringFormat(content, sizeof(content), "Could not resolve domain '%z'.", domainName);
				EsDialogShowAlert(instance->window, EsLiteral("Resolve IP address"), content, contentBytes, ES_ICON_DIALOG_ERROR, true); 
			}

			EsHeapFree(domainName);
		});

		instance->textboxIPAddress = EsTextboxCreate(panel, ES_CELL_NEW_BAND, {});
		instance->textboxEchoMessage = EsTextboxCreate(panel, ES_FLAGS_DEFAULT, {});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Ping!"))->OnCommand([] EsCommandCallback {
			char *ipAddress = instance->textboxIPAddress->GetContents();
			char *echoMessage = instance->textboxEchoMessage->GetContents();

			char data[ES_ECHO_REQUEST_MAX_LENGTH];
			size_t dataBytes = EsCStringLength(echoMessage) > ES_ECHO_REQUEST_MAX_LENGTH 
				? ES_ECHO_REQUEST_MAX_LENGTH : EsCStringLength(echoMessage);
			EsMemoryCopy(data, echoMessage, dataBytes);

			EsAddress address = {};
			char *position = ipAddress;
			address.d[0] = EsCRTatoi(position);
			position = EsCRTstrchr(position, '.') + 1;
			if (position) address.d[1] = EsCRTatoi(position);
			if (position) position = EsCRTstrchr(position, '.') + 1;
			if (position) address.d[2] = EsCRTatoi(position);
			if (position) position = EsCRTstrchr(position, '.') + 1;
			if (position) address.d[3] = EsCRTatoi(position);

			EsError error = EsSyscall(ES_SYSCALL_ECHO_REQUEST, (uintptr_t) data, dataBytes, (uintptr_t) &address, 0);

			if (error == ES_SUCCESS) {
				char content[256];
				size_t contentBytes = EsStringFormat(content, sizeof(content), "Server says: \"%s\"", dataBytes, data);
				EsDialogShowAlert(instance->window, EsLiteral("Pong!"), content, contentBytes, ES_ICON_DIALOG_INFORMATION, true); 
			} else {
				EsDialogShowAlert(instance->window, EsLiteral("Pong!"), EsLiteral("Something went wrong..."), ES_ICON_DIALOG_ERROR, true); 
			}

			EsHeapFree(ipAddress);
			EsHeapFree(echoMessage);
		});

		EsButtonCreate(panel, ES_CELL_NEW_BAND, {}, EsLiteral("Run TCP test"))->OnCommand(RunTCPTest);

		EsTextboxCreate(panel, ES_CELL_NEW_BAND | ES_TEXTBOX_EDIT_BASED, {})->UseNumberOverlay(true);

		EsTextDisplayCreate(panel, ES_CELL_NEW_BAND, {}, EsLiteral("Move file:"));
		static EsTextbox *moveFileFrom;
		moveFileFrom = EsTextboxCreate(panel, ES_FLAGS_DEFAULT, {})->Insert("/Essence/A Study in Scarlet.txt");
		EsTextDisplayCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("to:"));
		static EsTextbox *moveFileTo;
		moveFileTo = EsTextboxCreate(panel, ES_FLAGS_DEFAULT, {})->Insert("/Applications/test.txt");

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Go!"))->OnCommand([] EsCommandCallback {
			EsNodeInformation node = {};
			EsNodeInformation directory = {};
			EsError error = EsNodeOpen(moveFileFrom->GetContents(), -1, ES_NODE_FAIL_IF_NOT_FOUND, &node);
			char *to = moveFileTo->GetContents();
			int s = 0;
			for (int i = 0; to[i]; i++) if (to[i] == '/') s = i + 1;
			if (error == ES_SUCCESS) error = EsNodeOpen(to, s, ES_NODE_DIRECTORY | ES_NODE_FAIL_IF_NOT_FOUND, &directory);
			if (error == ES_SUCCESS) error = EsNodeMove(node.handle, directory.handle, to + s, -1);
			char message[64];
			size_t messageBytes = EsStringFormat(message, sizeof(message), "Status: %d", error);
			EsDialogShowAlert(window, EsLiteral("Move file"), message, messageBytes, ES_ICON_DIALOG_INFORMATION, true);
			if (node.handle) EsHandleClose(node.handle);
			if (directory.handle) EsHandleClose(directory.handle);
		});

		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, EsLiteral("Delete"))->OnCommand([] EsCommandCallback {
			EsNodeInformation node = {};
			EsError error = EsNodeOpen(moveFileTo->GetContents(), -1, ES_FILE_WRITE_EXCLUSIVE | ES_NODE_FAIL_IF_NOT_FOUND, &node);
			if (error == ES_SUCCESS) error = EsNodeDelete(node.handle);
			char message[64];
			size_t messageBytes = EsStringFormat(message, sizeof(message), "Status: %d", error);
			EsDialogShowAlert(window, EsLiteral("Delete file"), message, messageBytes, ES_ICON_DIALOG_INFORMATION, true);
			if (node.handle) EsHandleClose(node.handle);
		});
#endif

#if 1
		{
			static EsStyle itemStyle;
			itemStyle.inherit = ES_STYLE_LIST_ITEM;
			itemStyle.metrics.mask = ES_THEME_METRICS_PREFERRED_WIDTH | ES_THEME_METRICS_PREFERRED_HEIGHT;
			itemStyle.metrics.preferredWidth = 45;
			itemStyle.metrics.preferredHeight = 45;

			static EsStyle listStyle;
			listStyle.inherit = ES_STYLE_LIST_VIEW_BORDERED;
			listStyle.metrics.mask = ES_THEME_METRICS_GAP_MINOR | ES_THEME_METRICS_GAP_MAJOR;
			listStyle.metrics.gapMajor = 10;
			listStyle.metrics.gapMinor = 10;

			EsListView *iconList = EsListViewCreate(panel, ES_CELL_FILL | ES_LIST_VIEW_TILED, 
					&listStyle, &itemStyle);
			iconList->userCallback = IconListView;
			EsListViewInsertGroup(iconList, 0);
			EsListViewInsert(iconList, 0, 1, 1065);
			// EsListViewInsert(iconList, 0, 1, 20);
		}
#endif

		EsIconDisplayCreate(panel, ES_FLAGS_DEFAULT, 0, ES_ICON_APPLICATION_ILLUSTRATOR);

#if 0
		EsTextDisplayCreate(EsPanelCreate(panel, ES_CELL_H_FILL, "Panel.GroupBox"), ES_CELL_H_FILL, "User.Paragraph", 
				"Every time I break word-wrapping I have to come up with some random text to test it out, and I never know what to write.");
#else
		EsTextDisplayCreate(EsPanelCreate(panel, ES_CELL_H_FILL, ES_STYLE_PANEL_GROUP_BOX), ES_CELL_H_FILL, ES_STYLE_TEXT_LABEL, 
				"Every time I break word-wrapping I have to come up with some random text to test it out, and I never know what to write.");
#endif

#if 0
		{
			EsPerformanceTimerPush();
			size_t count = 1e6;

			for (uintptr_t i = 0; i < count; i++) {
				EsButtonCreate(panel, ES_FLAGS_DEFAULT, 0, "Push", -1);
			}

			EsPrint("Time to create %d buttons: %F.\n", count, EsPerformanceTimerPop());
		}
#endif

		// EsThreadCreate(DirectoryTest, nullptr, 0);

		// FileFlushTest();
		// FileResizeTest();

#if 0
		CommitTest();

		// threadVariable = 5;

		// char *argv[] = { (char *) "test", 0, 0, 0 };
		// __libc_start_main(0, 1, argv);

		// FileCacheTest();

		// DecodeTest();

#if 0
		{
			EsNodeInformation node;
			EsError error = EsNodeOpen(EsLiteral("/OS/Volume1/COOL.TXT;1"), ES_NODE_READ_ACCESS, &node);
			EsPrint("Opened file with error %d and size %d.\n", error, node.fileSize);
			char buffer[64] = {};
			size_t bytesRead = EsFileReadSync(node.handle, 1, 8, buffer);
			EsPrint("Read %d bytes from file: '%z'.\n", bytesRead, buffer);
			EsHandleClose(node.handle);
		}

		AudioTest();
#endif
		// EventSinkTest();

		// EsSplitter *splitter = EsSplitterCreate(window, ES_SPLITTER_VERTICAL | ES_CELL_FILL, {});
		// EsPanelCreate(splitter, ES_CELL_COLLAPSABLE | ES_CELL_EXPAND, { .styleOverride = EsStyleParse("backgroundColor = #F8ECEC; minimumHeight = 100; preferredHeight = 200; ") });
		// EsPanelCreate(splitter, ES_CELL_COLLAPSABLE | ES_CELL_EXPAND, { .styleOverride = EsStyleParse("backgroundColor = #ECF8EC; minimumHeight = 100; preferredHeight = 200; ") });
		// EsPanelCreate(splitter, ES_CELL_COLLAPSABLE | ES_CELL_FILL,   { .styleOverride = EsStyleParse("backgroundColor = #ECECF8; ") });
		// EsPanelCreate(splitter, ES_CELL_COLLAPSABLE | ES_CELL_EXPAND, { .styleOverride = EsStyleParse("backgroundColor = #F8ECF8; preferredHeight = 100; ") });

#if 1
#if 0
		static EsPanel *container = nullptr;
		static EsButton *button1 = nullptr, *button2 = nullptr;
		static EsTransitionType transition1 = ES_TRANSITION_SLIDE_UP, transition2 = ES_TRANSITION_SLIDE_DOWN;

		EsElement *wrapper = EsPanelCreate(window, ES_CELL_FILL, { 
			.cStyle = "Panel.WindowBackground",
			.cStyleOverride = "gap = 10; inset = 10;",
		       	.cName = "wrapper",
		});

		EsChoiceCreate(wrapper, ES_FLAGS_DEFAULT, { .callback = [] (EsElement *element, EsMessage *message) {
			ES_ON(CHOICE_ADD_ITEMS) {
				EsElement *column = EsMenuColumnCreate(message->addChoiceItems.menu, ES_CELL_H_FILL);
				EsMenuItemCreate(column, ES_FLAGS_DEFAULT, EsLiteral("Slide"),  message->addChoiceItems.callback, 0);
				EsMenuItemCreate(column, ES_FLAGS_DEFAULT, EsLiteral("Cover"),  message->addChoiceItems.callback, 1);
				EsMenuItemCreate(column, ES_FLAGS_DEFAULT, EsLiteral("Squish"), message->addChoiceItems.callback, 2);
			}

			ES_ON(CHOICE_ITEM_TO_STRING) {
				if (message->itemToString.item.u == 0) {
					message->itemToString.text = "Slide";
				} else if (message->itemToString.item.u == 1) {
					message->itemToString.text = "Cover";
				} else if (message->itemToString.item.u == 2) {
					message->itemToString.text = "Squish";
				}

				message->itemToString.textBytes = EsCStringLength(message->itemToString.text);
			}

			ES_ON(CHOICE_UPDATE) {
				transition1 = (EsTransitionType) (message->choiceUpdated.newItem.u * 2 + 1);
				transition2 = (EsTransitionType) (message->choiceUpdated.newItem.u * 2 + 2);
			}
		}})->SetItem(0);

		container = EsPanelCreate(wrapper, ES_CELL_H_FILL | ES_PANEL_SWITCHER | ES_CELL_NEW_BAND, { 
			.cStyleOverride = "inset = 5; backgroundColor = #191A1F; borderColor = #12171D; border = 1;",
			.cName = "container",
		});

		button1 = EsButtonCreate(container, ES_CELL_CENTER, {}, "Button 1")->OnCommand([] EsCommandCallback { container->SwitchTo(button2, transition1); });
		button2 = EsButtonCreate(container, ES_CELL_CENTER | ES_ELEMENT_HIDDEN, {}, "Button 2")->OnCommand([] EsCommandCallback { container->SwitchTo(button1, transition2); });
		container->SwitchTo(button1, ES_TRANSITION_NONE);
#endif

		// EsButtonCreate(wrapper, ES_CELL_NEW_BAND, {}, "Button 3");
		
#if 0
		EsPanel *groupBox = EsPanelCreate(container, ES_CELL_H_FILL, { .cStyle = "Panel.GroupBox", .styleOverride = EsStyleParse("gap = 10; inset = 10;") });
		EsButtonCreate(groupBox, ES_FLAGS_DEFAULT, {}, "Open the dialog")->OnCommand([] EsCommandCallback { 
			EsElement *buttonArea = EsDialogShowAlert(instance->window, "Hi", -1, "Dialog content", -1, ES_ICON_DIALOG_INFORMATION); 
			EsButtonCreate(buttonArea, ES_BUTTON_DEFAULT, {}, "Close the dialog")->Focus()->OnCommand([] EsCommandCallback {
				EsDialogClose(instance->window);
			});
		});
		EsButtonCreate(groupBox, ES_FLAGS_DEFAULT, {}, "Test button 2");
		EsTextDisplayCreate(groupBox, ES_FLAGS_DEFAULT, {}, "Foo bar:");
		EsTextboxCreate(groupBox, ES_TEXTBOX_EDIT_BASED | ES_CELL_H_FILL, {})->UseNumberOverlay(true)->Insert("0");
		EsColorWellCreate(groupBox, ES_FLAGS_DEFAULT, {});
#endif

		EsPanel *container = EsPanelCreate(window, ES_CELL_FILL, { .cStyle = "Panel.WindowBackground" });
		// AnimateTextSizeExample(container);

		EsCustomElementCreate(container, ES_CELL_NEW_BAND, { .callback = [] (EsElement *element, EsMessage *message) {
			ES_ON(PAINT) {
				// EsRectangle bounds = EsPainterBoundsInset(message->painter);
				// EsDrawTestShape(message->painter, bounds);
#if 0
				uint32_t icon = ES_ICON_EDIT_FIND_SYMBOLIC;
				EsDrawStandardIcon(message->painter, icon, 16, 
						ES_MAKE_RECTANGLE(bounds.left, bounds.left + 16, bounds.top, bounds.bottom), 0xFF000000);
				EsDrawStandardIcon(message->painter, icon, 24, 
						ES_MAKE_RECTANGLE(bounds.left + 16, bounds.left + 16 + 24, bounds.top, bounds.bottom), 0xFF000000);
				EsDrawStandardIcon(message->painter, icon, 128, 
						ES_MAKE_RECTANGLE(bounds.left + 16 + 24, bounds.left + 16 + 24 + 128, bounds.top, bounds.bottom), 0xFF000000);
#endif
			}
		}});

#if 0
		EsElement *container = EsPanelCreate(window, ES_CELL_FILL, { .cStyle = "Panel.WindowBackground" });

		static const char **array = nullptr;

		arrput(array, "Alfa");
		arrput(array, "Bravo");
		arrput(array, "Charlie");
		arrput(array, "Delta");
		arrput(array, "Echo");
		arrput(array, "Foxtrot");
		arrput(array, "Golf");
		arrput(array, "Hotel");
		arrput(array, "India");
		arrput(array, "Juliett");
		arrput(array, "Kilo");
		arrput(array, "Lima");
		arrput(array, "Mike");
		arrput(array, "November");
		arrput(array, "Oscar");
		arrput(array, "Papa");
		arrput(array, "Quebec");
		arrput(array, "Romeo");
		arrput(array, "Sierra");
		arrput(array, "Tango");
		arrput(array, "Uniform");
		arrput(array, "Victor");
		arrput(array, "Whiskey");
		arrput(array, "X-ray");
		arrput(array, "Yankee");
		arrput(array, "Zulu");

		static bool isSelected[1000000] = {};
		static EsListView *listView = nullptr;

		listView = EsListViewCreate(container, ES_CELL_FILL | ES_LIST_VIEW_MULTI_SELECT | ES_LIST_VIEW_COLUMNS, {
			.cStyle = "List.View",
			// .cStyleOverride = "gap = 5;",

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(LIST_VIEW_GET_CONTENT) {
					// static char text[256];
					// message->getContent.text = text;
					// message->getContent.textBytes = EsStringFormat(text, sizeof(text), 
					// 		"Item %d", message->getContent.index);
					EsAssert(message->getContent.index.u < arrlenu(array), "Invalid index.\n");
					message->getContent.text = array[message->getContent.index];
					message->getContent.textBytes = -1;
				}

				ES_ON(LIST_VIEW_MEASURE_ITEM) {
					message->measureItem.result = (message->measureItem.index.i & 1) ? 50 : 30;
				}

				ES_ON(LIST_VIEW_SELECT_RANGE) {
					for (uintptr_t i = message->selectRange.fromIndex.u; i <= message->selectRange.toIndex.u; i++) {
						if (message->selectRange.toggle) {
							isSelected[i] = !isSelected[i];
						} else {
							isSelected[i] = message->selectRange.select;
						}
					}
				}

				ES_ON(LIST_VIEW_IS_SELECTED) {
					message->selectItem.isSelected = isSelected[message->selectItem.index.i];
				}

				ES_ON(LIST_VIEW_SELECT) {
					isSelected[message->selectItem.index.i] = message->selectItem.isSelected;
				}

#if 0
				ES_ON(LIST_VIEW_CREATE_ITEM) {
					EsButtonCreate(message->createItem.parent, ES_FLAGS_DEFAULT, {}, "Delete")->OnCommand([] EsCommandCallback {
						uintptr_t index = listView->GetIndex(element);
						listView->Remove(0, index, index);
						arrdel(array, index);
					});
				}
#endif

#if 0
				ES_ON(LIST_VIEW_CREATE_ITEM) {
					EsListViewCreate(message->createItem.parent, ES_CELL_FILL, {
						.cStyle = "List.View.Bordered",
						.cStyleOverride = "gap = 5;",

						.callback = [] (EsElement *element, EsMessage *message) {
							ES_ON(LIST_VIEW_CREATE_ITEM) {
								char label[256];
								size_t labelBytes = EsStringFormat(label, sizeof(label), 
										"Btn %d/%d", element->GetData(), message->createItem.index);
								EsButtonCreate(message->createItem.parent, ES_CELL_FILL, {}, label, labelBytes);
							}
						},

						.data = message->createItem.index,
					}, "User.ButtonTile")->InsertGroup(0)->Insert(0, 0, 1000000 - 1);
				}
#endif
			},
		} /*, "User.ListTile"*/);
		
		listView->InsertGroup(0);
		listView->Insert(0, 0, arrlen(array) - 1);

		static EsListViewColumn columns[] = {
			{ "Name", -1, ES_LIST_VIEW_COLUMN_HAS_MENU | ES_LIST_VIEW_COLUMN_ASCENDING },
			{ "Type", -1, ES_LIST_VIEW_COLUMN_HAS_MENU },
			{ "Size", -1, ES_LIST_VIEW_COLUMN_HAS_MENU },
		};

		listView->SetColumns(columns, sizeof(columns) / sizeof(columns[0]));
#endif

#if 0
		// static DS_ARRAY(int) list = nullptr;
		static EsListView *view = nullptr;
		static bool header = true, footer = true;

		view = EsListViewCreate(container, ES_CELL_FILL | ES_LIST_VIEW_TILED, { 
			.cStyle = "List.View",
			.cStyleOverride = "gap = 20, 5, 10;",

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(LIST_VIEW_GET_CONTENT) {
					static char buffer[64];
					message->getContent.text = buffer;

					if (message->getContent.index.i == 0 && header) {
						message->getContent.textBytes = EsStringFormat(buffer, sizeof(buffer), "%d: Header", message->getContent.group);
					} else if (message->getContent.index.i == 10 + (header ? 1 : 0)) {
						message->getContent.textBytes = EsStringFormat(buffer, sizeof(buffer), "%d: Footer", message->getContent.group);
					} else {
						message->getContent.textBytes = EsStringFormat(buffer, sizeof(buffer), "%d: Item %d", message->getContent.group, 
								message->getContent.index.i);
					}
				}
			},
		}, "List.Item.Tile");

#if 0
		for (uintptr_t i = 0; i < 100; i++) {
			arrput(list, i);
		}
#endif

		for (int i = 0; i < 10; i++) {
			view->InsertGroup(i, (header ? ES_LIST_VIEW_GROUP_HAS_HEADER : 0) | (footer ? ES_LIST_VIEW_GROUP_HAS_FOOTER : 0) | ES_LIST_VIEW_GROUP_INDENT);
			view->Insert(i, 0 + (header ? 1 : 0), 9 + (header ? 1 : 0));
		}
#endif

#if 0
		static int additionalIndex = 100;

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Add item at 50")->OnCommand([] EsCommandCallback {
			arrins(list, 50, additionalIndex);
			additionalIndex++;
			view->Insert(0, 50, 50);
		});

		EsButtonCreate(container, 0, {}, "Add item at 0")->OnCommand([] EsCommandCallback {
			arrins(list, 0, additionalIndex);
			additionalIndex++;
			view->Insert(0, 0, 0);
		});

		EsButtonCreate(container, 0, {}, "Add item at end")->OnCommand([] EsCommandCallback {
			int length = arrlen(list);
			arrins(list, length, additionalIndex);
			additionalIndex++;
			view->Insert(0, length, length);
		});
#endif

#if 0
		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Create file")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Administrator/Test.txt", -1, ES_FLAGS_DEFAULT, &node);
			EsHandleClose(node.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Rename file")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Administrator/Test.txt", -1, ES_FLAGS_DEFAULT, &node);
			EsNodeMove(node.handle, ES_INVALID_HANDLE, "Renamed.txt", -1); 
			EsHandleClose(node.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Move to parent")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Administrator/Renamed.txt", -1, ES_FLAGS_DEFAULT, &node);

			EsNodeInformation parent;
			EsNodeOpen("/Users", -1, ES_NODE_DIRECTORY, &parent);

			EsNodeMove(node.handle, parent.handle, "Renamed.txt", -1); 

			EsHandleClose(node.handle);
			EsHandleClose(parent.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Move to child")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Renamed.txt", -1, ES_FLAGS_DEFAULT, &node);

			EsNodeInformation parent;
			EsNodeOpen("/Users/Administrator", -1, ES_NODE_DIRECTORY, &parent);

			EsNodeMove(node.handle, parent.handle, "Renamed.txt", -1); 

			EsHandleClose(node.handle);
			EsHandleClose(parent.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Move to sibling")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Administrator/Renamed.txt", -1, ES_FLAGS_DEFAULT, &node);

			EsNodeInformation parent;
			EsNodeOpen("/Users/Owner", -1, ES_NODE_DIRECTORY, &parent);

			EsNodeMove(node.handle, parent.handle, "Renamed.txt", -1); 

			EsHandleClose(node.handle);
			EsHandleClose(parent.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Move folder up")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Administrator", -1, ES_NODE_DIRECTORY, &node);

			EsNodeInformation parent;
			EsNodeOpen("/", -1, ES_NODE_DIRECTORY, &parent);

			EsNodeMove(node.handle, parent.handle, "We've moved!", -1); 

			EsHandleClose(node.handle);
			EsHandleClose(parent.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Write file")->OnCommand([] EsCommandCallback {
			EsFileWriteAll(EsLiteral("/Users/Owner/Renamed.txt"), EsLiteral("Hello, world!"));
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Delete file")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Owner/Renamed.txt", -1, ES_NODE_RESIZE_ACCESS, &node);
			EsNodeDelete(node.handle);
			EsHandleClose(node.handle);
		});

		EsButtonCreate(container, ES_CELL_NEW_BAND, {}, "Delete folder")->OnCommand([] EsCommandCallback {
			EsNodeInformation node;
			EsNodeOpen("/Users/Owner", -1, 0, &node);
			EsNodeDelete(node.handle);
			EsHandleClose(node.handle);
		});

		{
			EsFileWriteAll(EsLiteral("/delete test.txt"), EsLiteral("Hello, world!"));
			EsNodeDeleteByPath(EsLiteral("/delete test.txt"));
		}
#endif

#if 0
		EsStyle panelStyle = EsStyleParse(R"(
			inset = 10;
			gap = -1; 

			border = 1;
			if     HasChildren { borderColor = #F00; }
			if not HasChildren { borderColor = #00F; }
		)");

		EsStyle buttonStyle = EsStyleParse(R"(
			cornerRadius = 0;
			if OnlyChild  { cornerRadius = 5; }
			if FirstChild { cornerRadius = 5, 0, 5, 0; }
			if LastChild  { cornerRadius = 0, 5, 0, 5; }
		)");
#endif

#if 0
		EsDirectoryChild *children;
		ptrdiff_t count = EsDirectoryEnumerateChildren("/", -1, &children);
		if (ES_CHECK_ERROR(count)) { /* TODO Handle error. */ }

		for (ptrdiff_t i = 0; i < count; i++) {
			EsPrint("%d: %s, %z, %D\n", i, 
					children[i].nameBytes, children[i].name, 
					children[i].information.type == ES_NODE_DIRECTORY ? "Directory" : "Folder",
					children[i].information.fileSize);
		}

		EsHeapFree(children);

		// EsPanel *panel1 = EsPanelCreate(container, ES_CELL_NEW_BAND, { .styleOverride = panelStyle });

		EsTextboxCreate(container, ES_TEXTBOX_EDIT_BASED, { .callback = [] (EsElement *element, EsMessage *message) {
			ES_ON(TEXTBOX_NUMBER_UPDATED) {
				EsUISetDPI(message->numberUpdated.newValue);
			}
		}})->UseNumberOverlay(true)->Insert("100");

		EsButtonCreate(container, 0, {}, "Show test dialog")->OnCommand([] EsCommandCallback {
			EsElement *buttonArea = EsDialogShowAlert(instance->window,
					"And that's exactly what I was hoping you'd say.", -1,
					"We need a cool headed man like you on our team, Greg. That was a test, and you passed. Welcome aboard!", -1,
					ES_ICON_DIALOG_INFORMATION);

			EsButtonCreate(buttonArea, ES_BUTTON_DEFAULT, {}, "Thank you!")->OnCommand([] EsCommandCallback {
				EsDialogClose(instance->window);
			})->Focus();
		});

		EsColorWellCreate(container, ES_FLAGS_DEFAULT, {}, 0xFF0000);

		EsCustomElementCreate(container, ES_FLAGS_DEFAULT, {
			.styleOverride = EsStyleParse("preferredWidth = 100; preferredHeight = 100; backgroundColor = #FFA500; borderColor = black; border = 1; cornerRadius = 50;")
		});

		{
#define SORT_COUNT (1000000)
			int *numbers = (int *) EsHeapAllocate(sizeof(int) * SORT_COUNT, true);

			for (int i = 0; i < SORT_COUNT; i++) {
				numbers[i] = (int) EsRandomU64() % SORT_COUNT;
			}

			EsPerformanceTimerPush(); 
			EsSort(numbers, SORT_COUNT, sizeof(numbers[0]), [] (const void *left, const void *right, EsGeneric) { return *(int *) left - *(int *) right; }, nullptr);
			EsPrint("EsSort: %Fs\n", EsPerformanceTimerPop()); 

			for (int i = 0; i < SORT_COUNT - 1; i++) {
				EsAssert(numbers[i] <= numbers[i + 1], "Oops");
			}

			for (int i = 0; i < SORT_COUNT; i++) {
				numbers[i] = (int) EsRandomU64() % SORT_COUNT;
			}

			EsPerformanceTimerPush(); 
			ES_MACRO_SORT(numbers, SORT_COUNT, int, { result = *_left - *_right; }, false, ;);
			EsPrint("ES_MACRO_SORT: %Fs\n", EsPerformanceTimerPop()); 

			for (int i = 0; i < SORT_COUNT - 1; i++) {
				EsAssert(numbers[i] <= numbers[i + 1], "Oops");
			}

#if 0
			for (int i = 0; i < SORT_COUNT; i++) {
				numbers[i] = (int) EsRandomU64() % SORT_COUNT;
			}

			EsPerformanceTimerPush(); 
			qsort(numbers, SORT_COUNT, sizeof(numbers[0]), [] (const void *left, const void *right) { return *(int *) left - *(int *) right; });
			EsPrint("qsort: %Fs\n", EsPerformanceTimerPop()); 

			for (int i = 0; i < SORT_COUNT - 1; i++) {
				EsAssert(numbers[i] <= numbers[i + 1], "Oops");
			}
#endif

			for (int i = 0; i < SORT_COUNT; i++) {
				numbers[i] = (int) EsRandomU64() % SORT_COUNT;
			}

			EsPerformanceTimerPush(); 
			std::sort(numbers, numbers + SORT_COUNT, std::greater<int>());
			EsPrint("std::sort: %Fs\n", EsPerformanceTimerPop()); 

			for (int i = 0; i < SORT_COUNT - 1; i++) {
				EsAssert(numbers[i] >= numbers[i + 1], "Oops");
			}
		}
#endif

#if 0
		EsPanel *panel2 = EsPanelCreate(container, ES_CELL_NEW_BAND, { .styleOverride = panelStyle });
		EsButtonCreate(panel2, 0, { .styleOverride = buttonStyle }, "First button");
		EsButtonCreate(panel2, 0, { .styleOverride = buttonStyle }, "Middle button 1");
		EsButtonCreate(panel2, 0, { .styleOverride = buttonStyle }, "Middle button 2");
		EsButtonCreate(panel2, 0, { .styleOverride = buttonStyle }, "Last button");

		EsPanel *panel3 = EsPanelCreate(container, ES_CELL_NEW_BAND, { .styleOverride = panelStyle });
		(void) panel3;
		// No children.
#endif

		// CreateDialog(panel, false);
		
		// EsButtonCreate(panel, ES_FLAGS_DEFAULT, { .styleOverride = EsStyleParse("preferredWidth = 300; preferredHeight = 90; textAlign = left, top;") }, "Bottom");
		// EsButtonCreate(panel, ES_FLAGS_DEFAULT, { .styleOverride = EsStyleParse("preferredWidth = 200; preferredHeight = 60; textAlign = left, top;") }, "Middle");
		// EsButtonCreate(panel, ES_FLAGS_DEFAULT, { .styleOverride = EsStyleParse("preferredWidth = 100; preferredHeight = 30; textAlign = left, top;") }, "Top");
#if 0
		EsCustomElementCreate(panel, ES_CELL_FILL, 
				{ .styleOverride = EsStyleParse("cornerRadius = 3; backgroundColor = white; textSize = 36;"
						"shadowBlur = 3; shadowColor = #40000000; inset = 20, 10; fontFamily = sans;"),
			       .callback = ProcessDisplayMessage });
#endif
		// EsTextboxCreate(EsPanelCreate(panel, ES_CELL_FILL, { .styleOverride = EsStyleParse("backgroundColor = #F00;") }), 0, {});
		// EsCustomElementCreate(panel, ES_CELL_FILL, { .styleOverride = EsStyleParse("backgroundColor = #80000000;") });
		// EsButtonCreate(panel, ES_FLAGS_DEFAULT, {}, "Push");
		// EsTextDisplayCreate(panel, ES_FLAGS_DEFAULT, {}, "Pick an option:");
		// EsChoiceCreate(panel);

#if 0
		EsButtonCreate(panel, ES_FLAGS_DEFAULT, {
			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(CLICKED) {
					EsMenuCreate(element, ES_FLAGS_DEFAULT, [] EsMenuCallback {
						NewColorPicker(menu, 0xFFFFFF);
					});
				}
			},
		}, "Color picker");
#endif

#if 0
		EsColorWellCreate(panel, ES_FLAGS_DEFAULT, {}, 0xFF0000);
		EsColorWellCreate(panel, ES_COLOR_WELL_HAS_OPACITY, {}, 0x800000FF);
		EsButtonCreate(panel, ES_BUTTON_CHECKBOX, {}, "Checkbox");

		{
			EsPanel *group = EsPanelCreate(panel, ES_CELL_NEW_BAND, { .cStyle = ES_PANEL_STYLE_GROUP_BOX });

			EsButton *option1 = EsButtonCreate(group, ES_BUTTON_RADIOBOX | ES_CELL_NEW_BAND, {}, "Option 1");
			EsPanel *subgroup1 = EsPanelCreate(group, ES_CELL_NEW_BAND, { .cStyle = ES_PANEL_STYLE_INDENT });
			EsButtonCreate(subgroup1, ES_BUTTON_CHECKBOX | ES_CELL_NEW_BAND, {}, "Checkbox 1(a)");
			EsButtonCreate(subgroup1, ES_BUTTON_CHECKBOX | ES_CELL_NEW_BAND, {}, "Checkbox 1(b)");
			EsButtonCreate(subgroup1, ES_BUTTON_CHECKBOX | ES_CELL_NEW_BAND, {}, "Checkbox 1(c)");
			option1->SetCheckBuddy(subgroup1)->SetCheck();

			EsButton *option2 = EsButtonCreate(group, ES_BUTTON_RADIOBOX | ES_CELL_NEW_BAND, {}, "Option 2");
			EsPanel *subgroup2 = EsPanelCreate(group, ES_CELL_NEW_BAND, { .cStyle = ES_PANEL_STYLE_INDENT });
			EsButtonCreate(subgroup2, ES_BUTTON_CHECKBOX, {}, "Checkbox 2");
			option2->SetCheckBuddy(subgroup2);

			EsButton *option3 = EsButtonCreate(group, ES_BUTTON_RADIOBOX | ES_CELL_NEW_BAND, {}, "Option 3");
			EsPanel *subgroup3 = EsPanelCreate(group, ES_CELL_NEW_BAND, { .cStyle = ES_PANEL_STYLE_INDENT });
			EsButtonCreate(subgroup3, ES_BUTTON_CHECKBOX, {}, "Checkbox 3");
			option3->SetCheckBuddy(subgroup3);
		}
#endif

#if 0
		EsChoiceCreate(panel, ES_FLAGS_DEFAULT, {
			.callback = [] (EsElement *element, EsMessage *message) {
				static char buffer[64];

				ES_ON(CHOICE_ADD_ITEMS) {
					EsElement *parent = EsMenuColumnCreate(message->addChoiceItems.menu, ES_CELL_H_FILL);
					
					for (uintptr_t i = 1; i <= 10; i++) {
						EsMenuItemCreate(parent, ES_FLAGS_DEFAULT, buffer, EsStringFormat(buffer, sizeof(buffer) - 1, "Choice %d", i), message->addChoiceItems.callback, i);
					}
				}

				ES_ON(CHOICE_ITEM_TO_STRING) {
					message->itemToString.text = buffer;
					message->itemToString.textBytes = EsStringFormat(buffer, sizeof(buffer) - 1, "Choice %d", message->itemToString.item.i);
				}
			},
		})->SetItem(1);

		EsChoiceCreate(panel, ES_FLAGS_DEFAULT, {
			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsRectangle bounds = EsPainterBoundsInset(message->painter);
					EsStyledBox box = {};
					box.ox = (bounds.left + bounds.right) / 2 - 20, box.oy = bounds.top + 6;
					box.width = 40, box.height = bounds.bottom - bounds.top - 12;
					box.clip = message->painter->clip;
					box.borders = ES_MAKE_RECTANGLE_16_ALL(1);
					box.backgroundColor = ((EsChoice *) element)->GetItem().u;
					box.borderColor = EsColorBlend(box.backgroundColor, 0x40000000, false);
					DrawStyledBox(message->painter, box);
				}

				ES_ON(CHOICE_ADD_ITEMS) {
					EsElement *parent = EsMenuColumnCreate(message->addChoiceItems.menu, ES_CELL_H_FILL);
					EsMenuItemCreate(parent, ES_FLAGS_DEFAULT, EsLiteral("Red"), message->addChoiceItems.callback, 0xFFFC2523);
					EsMenuItemCreate(parent, ES_FLAGS_DEFAULT, EsLiteral("Green"), message->addChoiceItems.callback, 0xFF1DE82E);
					EsMenuItemCreate(parent, ES_FLAGS_DEFAULT, EsLiteral("Blue"), message->addChoiceItems.callback, 0xFF4FC7EE);
				}
			},
		})->SetItem(0xFFFC2523);
#endif

		// EsTextboxCreate(panel, ES_TEXTBOX_EDIT_BASED, {});
		// EsTextboxCreate(panel, 0, {});

		// EsTextDisplayCreate(panel, ES_FLAGS_DEFAULT, {}, "Your color:");
		// EsColorWellCreate(panel, 0, {})->SetIndeterminate();

#if 0
		EsStyle styleSquare = EsStyleParse(R"(
				preferredWidth = 170; 
				preferredHeight = 170; 
				clipMode = clip_none; 
				shadowColor = #4555; 
				shadowBlur = 5; 
				cursorStyle = cross_hair_pick;)");

		EsCustomElementCreate(panel, ES_ELEMENT_DO_NOT_FREE_STYLE_OVERRIDE, {
			.styleOverride = styleSquare,

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsPerformanceTimerPush();

					EsPainter *painter = message->painter;
					EsRectangle bounds = EsPainterBoundsInset(painter);
					EsRectangle clip = painter->clip;
					EsRectangleClip(clip, bounds, &clip);
					uint32_t *bitmap = (uint32_t *) painter->bits;

					float hueIncrement = 6.0f / (bounds.right - bounds.left),
						valueIncrement = -1.0f / (bounds.bottom - bounds.top - 1),
						value = 1.0f + (clip.top - bounds.top) * valueIncrement;

					for (int j = clip.top; j < clip.bottom; j++, value += valueIncrement) {
						float hue = (clip.left - bounds.left) * hueIncrement;

						for (int i = clip.left; i < clip.right; i++, hue += hueIncrement) {
							bitmap[i + j * (painter->linearBuffer.stride >> 2)] 
								= 0xFF000000 | EsColorConvertToRGB(hue, 1.0f, EsCRTsqrtf(value) * EsCRTsqrtf(EsCRTsqrtf(value)));
						}
					}

					EsPrint("Rendered color selector 1 in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
				}
			},
		});

		EsCustomElementCreate(panel, ES_ELEMENT_DO_NOT_FREE_STYLE_OVERRIDE, {
			.styleOverride = styleSquare,

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsPerformanceTimerPush();

					EsPainter *painter = message->painter;
					EsRectangle bounds = EsPainterBoundsInset(painter);
					EsRectangle clip = painter->clip;
					EsRectangleClip(clip, bounds, &clip);
					uint32_t *bitmap = (uint32_t *) painter->bits;

					float hueIncrement = 6.0f / (bounds.right - bounds.left),
						saturationIncrement = -1.0f / (bounds.bottom - bounds.top - 1),
						saturation = 1.0f + (clip.top - bounds.top) * saturationIncrement;

					for (int j = clip.top; j < clip.bottom; j++, saturation += saturationIncrement) {
						float hue = (clip.left - bounds.left) * hueIncrement;

						for (int i = clip.left; i < clip.right; i++, hue += hueIncrement) {
							bitmap[i + j * (painter->linearBuffer.stride >> 2)] 
								= 0xFF000000 | EsColorConvertToRGB(hue, saturation, 1.0f);
						}
					}

					EsPrint("Rendered color selector 2 in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
				}
			},
		});

		EsCustomElementCreate(panel, ES_ELEMENT_DO_NOT_FREE_STYLE_OVERRIDE, {
			.styleOverride = styleSquare,

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsPerformanceTimerPush();

					EsPainter *painter = message->painter;
					EsRectangle bounds = EsPainterBoundsInset(painter);
					EsRectangle clip = painter->clip;
					EsRectangleClip(clip, bounds, &clip);
					uint32_t *bitmap = (uint32_t *) painter->bits;

					float valueIncrement = -1.0f / (bounds.right - bounds.left - 1),
						saturationIncrement = -1.0f / (bounds.bottom - bounds.top - 1),
						saturation = 1.0f + (clip.top - bounds.top) * saturationIncrement;

					for (int j = clip.top; j < clip.bottom; j++, saturation += saturationIncrement) {
						float value = 1.0f;

						for (int i = clip.left; i < clip.right; i++, value += valueIncrement) {
							bitmap[i + j * (painter->linearBuffer.stride >> 2)] 
								= 0xFF000000 | EsColorConvertToRGB(3.41f, saturation, EsCRTsqrtf(value) * EsCRTsqrtf(EsCRTsqrtf(value)));
						}
					}

					EsPrint("Rendered color selector 3 in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
				}
			},
		});

		EsCustomElementCreate(panel, ES_ELEMENT_DO_NOT_FREE_STYLE_OVERRIDE, {
			.styleOverride = styleCircle,

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsPerformanceTimerPush();

					EsPainter *painter = message->painter;
					EsRectangle bounds = EsPainterBoundsInset(painter);
					EsRectangle clip = painter->clip;
					EsRectangleClip(clip, bounds, &clip);
					uint32_t *bitmap = (uint32_t *) painter->bits;
					float epsilon = 1.0f / (bounds.bottom - bounds.top);

					for (int j = clip.top; j < clip.bottom; j++) {
						for (int i = clip.left; i < clip.right; i++) {
							float x = (float) (i - bounds.left) / (float) (bounds.right - bounds.left) * 2.0f - 1.0f;
							float y = (float) (j - bounds.top)  / (float) (bounds.bottom - bounds.top) * 2.0f - 1.0f;
							float radius = EsCRTsqrtf(x * x + y * y), hue = atan2f(y, x) * 0.954929659f + 3;
							if (hue >= 6) hue -= 6;

							if (radius > 1.0f + epsilon) {
								// Outside the circle.
							} else if (radius > 1.0f - epsilon) {
								// On the edge.
								uint32_t over = EsColorConvertToRGB(hue, 1, 1);
								uint32_t alpha = (((uint32_t) (255.0f * (1.0f - ((radius - (1.0f - epsilon)) / epsilon * 0.5f)))) & 0xFF) << 24;
								uint32_t *under = &bitmap[i + j * (painter->linearBuffer.stride >> 2)];
								*under = EsColorBlend(*under, over | alpha, true);
							} else {
								// Inside the circle.
								uint32_t over = EsColorConvertToRGB(hue, 1, EsCRTsqrt(radius));
								bitmap[i + j * (painter->linearBuffer.stride >> 2)] = over | 0xFF000000;
							}
						}
					}

					EsPrint("Rendered color selector 4 in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
				}
			},
		});

		EsCustomElementCreate(panel, ES_ELEMENT_DO_NOT_FREE_STYLE_OVERRIDE, {
			.styleOverride = styleCircle,

			.callback = [] (EsElement *element, EsMessage *message) {
				ES_ON(PAINT) {
					EsPerformanceTimerPush();

					EsPainter *painter = message->painter;
					EsRectangle bounds = EsPainterBoundsInset(painter);
					EsRectangle clip = painter->clip;
					EsRectangleClip(clip, bounds, &clip);
					uint32_t *bitmap = (uint32_t *) painter->bits;
					float epsilon = 1.0f / (bounds.bottom - bounds.top);

					for (int j = clip.top; j < clip.bottom; j++) {
						for (int i = clip.left; i < clip.right; i++) {
							float x = (float) (i - bounds.left) / (float) (bounds.right - bounds.left) * 2.0f - 1.0f;
							float y = (float) (j - bounds.top)  / (float) (bounds.bottom - bounds.top) * 2.0f - 1.0f;
							float radius = EsCRTsqrtf(x * x + y * y), value = atan2f(y, x) * 0.159154943 + 0.5f;

							if (radius > 1.0f + epsilon) {
								// Outside the circle.
							} else if (radius > 1.0f - epsilon) {
								// On the edge.
								uint32_t over = EsColorConvertToRGB(3.41, 1, value);
								uint32_t alpha = (((uint32_t) (255.0f * (1.0f - ((radius - (1.0f - epsilon)) / epsilon * 0.5f)))) & 0xFF) << 24;
								uint32_t *under = &bitmap[i + j * (painter->linearBuffer.stride >> 2)];
								*under = EsColorBlend(*under, over | alpha, true);
							} else {
								// Inside the circle.
								uint32_t over = EsColorConvertToRGB(3.41, radius, value);
								bitmap[i + j * (painter->linearBuffer.stride >> 2)] = over | 0xFF000000;
							}
						}
					}

					EsPrint("Rendered color selector 6 in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
				}
			},
		});
#endif
#endif
#endif
	}
}

void _start() {
	_init();

	while (true) {
		ProcessApplicationMessage(EsMessageReceive());
	}
}

#endif
