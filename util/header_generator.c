#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdarg.h>

const char **apiTableEntries;

typedef struct EsINIState {
	char *buffer, *sectionClass, *section, *key, *value;
	size_t bytes, sectionClassBytes, sectionBytes, keyBytes, valueBytes;
} EsINIState;

#include "../shared/stb_ds.h"
#include "../shared/ini.h"

FILE *output, *outputAPIArray, *outputSyscallArray, *outputDependencies;
char *buffer;
int position;

#define DEST_OS "root/Applications/POSIX/include/essence.h"
#define DEST_API_ARRAY "bin/api_array.h"
#define DEST_SYSCALL_ARRAY "bin/syscall_array.h"
#define DEST_DEPENDENCIES "bin/api_header.d"

typedef struct Token {
#define TOKEN_IDENTIFIER (1)
#define TOKEN_LEFT_BRACE (2) 
#define TOKEN_RIGHT_BRACE (3) 
#define TOKEN_EQUALS (4) 
#define TOKEN_ENUM (5) 
#define TOKEN_STRUCT (6) 
#define TOKEN_NUMBER (8) 
#define TOKEN_ASTERISK (9) 
#define TOKEN_COMMA (10) 
#define TOKEN_SEMICOLON (11) 
#define TOKEN_DEFINE (12) 
#define TOKEN_FUNCTION (13) 
#define TOKEN_FUNCTION_NOT_IN_KERNEL (15) 
#define TOKEN_EOF (16) 
#define TOKEN_ELLIPSIS (17)
#define TOKEN_UNION (18)
#define TOKEN_VOLATILE (19)
#define TOKEN_CONST (20)
#define TOKEN_LEFT_BRACKET (21) 
#define TOKEN_RIGHT_BRACKET (22) 
#define TOKEN_LEFT_PAREN (23) 
#define TOKEN_RIGHT_PAREN (24) 
#define TOKEN_INCLUDE (26)
#define TOKEN_API_TYPE (29)
#define TOKEN_FUNCTION_POINTER (30)
#define TOKEN_TYPE_NAME (31)
#define TOKEN_DEFINE_PRIVATE (32) 
	int type, value;
	char *text;
} Token;

#define ENTRY_ROOT (0)
#define ENTRY_DEFINE (1)
#define ENTRY_DEFINE_PRIVATE (2)
#define ENTRY_ENUM (3)
#define ENTRY_STRUCT (4)
#define ENTRY_UNION (5)
#define ENTRY_FUNCTION (6)
#define ENTRY_VARIABLE (7)
#define ENTRY_API_TYPE (8)
#define ENTRY_TYPE_NAME (9)

typedef struct Entry {
	int type;

	char *name;
	struct Entry *children;

	union {
		struct {
			char *type, *arraySize, *initialValue;
			int pointer;
			bool isArray, isVolatile, isConst, isForwardDeclared;
		} variable;

		struct {
			char *value;
		} define;

		struct {
			bool inKernel, functionPointer;
			int apiArrayIndex;
		} function;

		struct {
			char *parent;
		} apiType;

		char *oldTypeName;
	};
} Entry;

char *TokenToString(Token token) {
	if (!token.value) return NULL;
	char *string = (char *) malloc(token.value + 1);
	memcpy(string, token.text, token.value);
	string[token.value] = 0;
	return string;
}

int currentLine = 1;

Token NextToken() {
	tryAgain:;

	char c = buffer[position++];

	if (c == '\t') goto tryAgain;
	if (c == '\n') { currentLine++; goto tryAgain; }
	if (c == ' ') goto tryAgain;

	if (c == '/' && buffer[position] == '/') { while (buffer[position++] != '\n'); goto tryAgain; } 

	Token token = {};

	if (c == 0) token.type = TOKEN_EOF;

	else if (c == '{') token.type = TOKEN_LEFT_BRACE;
	else if (c == '}') token.type = TOKEN_RIGHT_BRACE;
	else if (c == '[') token.type = TOKEN_LEFT_BRACKET;
	else if (c == ']') token.type = TOKEN_RIGHT_BRACKET;
	else if (c == '(') token.type = TOKEN_LEFT_PAREN;
	else if (c == ')') token.type = TOKEN_RIGHT_PAREN;
	else if (c == '=') token.type = TOKEN_EQUALS;
	else if (c == '*') token.type = TOKEN_ASTERISK;
	else if (c == ',') token.type = TOKEN_COMMA;
	else if (c == ';') token.type = TOKEN_SEMICOLON;

	else if (c == '.' && buffer[position] == '.' && buffer[position + 1] == '.') position += 2, token.type = TOKEN_ELLIPSIS;

	else if ((c >= '0' && c <= '9') || c == '-') {
		token.type = TOKEN_NUMBER;
		token.text = buffer + position - 1;

		do {
			token.value++;
			c = buffer[position++];
		} while ((c >= '0' && c <= '9'));

		position--;
	}

	else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_') {
		token.type = TOKEN_IDENTIFIER;
		token.text = buffer + position - 1;

		do {
			token.value++;
			c = buffer[position++];
		} while ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_' || (c >= '0' && c <= '9'));

		position--;

#define COMPARE_KEYWORD(x, y) if (strlen(x) == token.value && 0 == memcmp(x, token.text, token.value)) token.type = y
		COMPARE_KEYWORD("define", TOKEN_DEFINE);
		COMPARE_KEYWORD("define_private", TOKEN_DEFINE_PRIVATE);
		COMPARE_KEYWORD("enum", TOKEN_ENUM);
		COMPARE_KEYWORD("struct", TOKEN_STRUCT);
		COMPARE_KEYWORD("function", TOKEN_FUNCTION);
		COMPARE_KEYWORD("function_not_in_kernel", TOKEN_FUNCTION_NOT_IN_KERNEL);
		COMPARE_KEYWORD("union", TOKEN_UNION);
		COMPARE_KEYWORD("volatile", TOKEN_VOLATILE);
		COMPARE_KEYWORD("const", TOKEN_CONST);
		COMPARE_KEYWORD("include", TOKEN_INCLUDE);
		COMPARE_KEYWORD("opaque_type", TOKEN_API_TYPE);
		COMPARE_KEYWORD("function_pointer", TOKEN_FUNCTION_POINTER);
		COMPARE_KEYWORD("type_name", TOKEN_TYPE_NAME);
	}

	else {
		printf("unrecognised token '%c', at '%.*s'\n", c, 10, buffer + position - 5);
		exit(1);
	}

	return token;
}

bool FoundEndOfLine(int length) {
	if (buffer[position + length] == '\n') return true;
	if (buffer[position + length] == '/' && buffer[position + length + 1] == '/') return true;
	return false;
}

Token ParseVariable(Token token, bool forFunction, Entry *_variable, Entry *parent) {
	int pointer = 0;
	bool array = false, isVolatile = false, isConst = false, isForwardDeclared = false;
	Token arraySize = {};

	Token type = token;

	if (type.type == TOKEN_VOLATILE) { isVolatile = true; type = NextToken(); }
	if (type.type == TOKEN_CONST) { isConst = true; type = NextToken(); }
	if (type.type == TOKEN_STRUCT) { isForwardDeclared = true; type = NextToken(); }

	commaRepeat:;
	pointer = 0;
	
	Token name = {};

	if (type.type != TOKEN_ELLIPSIS) {
		name = NextToken();

		while (name.type == TOKEN_ASTERISK) {
			name = NextToken();
			pointer++;
		}

		if (name.type == TOKEN_FUNCTION) name.type = TOKEN_IDENTIFIER;
	}

	Token semicolon = NextToken();

	if (semicolon.type == TOKEN_LEFT_BRACKET) {
		arraySize = NextToken();
		assert(arraySize.type == TOKEN_IDENTIFIER || arraySize.type == TOKEN_NUMBER || arraySize.type == TOKEN_RIGHT_BRACKET);
		array = true;

		if (arraySize.type != TOKEN_RIGHT_BRACKET) {
			assert(NextToken().type == TOKEN_RIGHT_BRACKET);
		}

		semicolon = NextToken();
	}

	if (type.type != TOKEN_ELLIPSIS) {
		assert(type.type == TOKEN_IDENTIFIER);
		assert(name.type == TOKEN_IDENTIFIER);
	}

	Entry entry = { .type = ENTRY_VARIABLE, .name = TokenToString(name), .variable = {
		.type = TokenToString(type), .arraySize = TokenToString(arraySize), .pointer = pointer, .isArray = array, 
		.isVolatile = isVolatile, .isConst = isConst, .isForwardDeclared = isForwardDeclared
	} };

	if (forFunction && semicolon.type == TOKEN_EQUALS) {
		entry.variable.initialValue = TokenToString(NextToken());
		semicolon = NextToken();
	}

	if (_variable) {
		*_variable = entry;
	}

	arrput(parent->children, entry);

	if (forFunction) {
		return semicolon;
	}

	if (semicolon.type == TOKEN_SEMICOLON) {
	} else if (semicolon.type == TOKEN_COMMA) {
		goto commaRepeat;
	} else {
		assert(false);
	}

	return semicolon;
}

Entry ParseRecord(bool isUnion) {
	Entry entry = { .type = isUnion ? ENTRY_UNION : ENTRY_STRUCT };
	Token token = NextToken();

	while (true) {
		if (token.type == TOKEN_RIGHT_BRACE) {
			break;
		} else if (token.type == TOKEN_UNION) {
			assert(NextToken().type == TOKEN_LEFT_BRACE);
			arrput(entry.children, ParseRecord(true));
			assert(NextToken().type == TOKEN_SEMICOLON);
		} else if (token.type == TOKEN_STRUCT) {
			assert(NextToken().type == TOKEN_LEFT_BRACE);
			Entry child = ParseRecord(false);
			Token name = NextToken();

			if (name.type == TOKEN_IDENTIFIER) {
				assert(NextToken().type == TOKEN_SEMICOLON);
				child.name = TokenToString(name);
			} else {
				assert(name.type == TOKEN_SEMICOLON);
			}

			arrput(entry.children, child);
		} else {
			ParseVariable(token, false, NULL, &entry);
		}

		token = NextToken();
	}

	return entry;
}

void *LoadFile(const char *path, size_t *length) {
	FILE *file = fopen(path, "rb");

	if (file) {
		fseek(file, 0, SEEK_END);
		size_t fileSize = ftell(file);
		fseek(file, 0, SEEK_SET);
		char *buffer = (char *) malloc(fileSize + 1);
		buffer[fileSize] = 0;
		fread(buffer, 1, fileSize, file);
		fclose(file);
		if (length) *length = fileSize;
		return buffer;
	} else {
		return NULL;
	}
}

void ParseFile(Entry *root, const char *name) {
	if (outputDependencies) {
		fprintf(outputDependencies, "%s\n", name);
	}

	char *oldBuffer = buffer;
	int oldPosition = position;
	buffer = (char *) LoadFile(name, NULL);
	assert(buffer);
	position = 0;

	Token token;
	bool nextToken = true;

	while (true) {
		if (nextToken) token = NextToken();
		nextToken = true;

		if (token.type == TOKEN_DEFINE) {
			Token identifier = NextToken();
			size_t length = 0;
			while (!FoundEndOfLine(length)) length++;
			Entry entry = { .type = ENTRY_DEFINE, .name = TokenToString(identifier) };
			entry.define.value = TokenToString((Token) { .value = (int) length, .text = buffer + position });
			arrput(root->children, entry);
			position += length;
		} else if (token.type == TOKEN_DEFINE_PRIVATE) {
			Token identifier = NextToken();
			size_t length = 0;
			while (!FoundEndOfLine(length)) length++;
			Entry entry = { .type = ENTRY_DEFINE_PRIVATE, .name = TokenToString(identifier) };
			entry.define.value = TokenToString((Token) { .value = (int) length, .text = buffer + position });
			arrput(root->children, entry);
			position += length;
		} else if (token.type == TOKEN_INCLUDE) {
			size_t length = 0;
			while (!FoundEndOfLine(length)) length++;
			char a = buffer[position + length];
			int oldCurrentLine = currentLine;
			currentLine = 1;
			buffer[position + length] = 0;
			ParseFile(root, buffer + position + 1);
			currentLine = oldCurrentLine;
			buffer[position + length] = a;
			position += length;
		} else if (token.type == TOKEN_ENUM) {
			Token name = NextToken();
			assert(name.type == TOKEN_IDENTIFIER);
			assert(NextToken().type == TOKEN_LEFT_BRACE);

			Entry entry = { .type = ENTRY_ENUM, .name = TokenToString(name) };
			Token token = NextToken();

			while (true) {
				if (token.type == TOKEN_RIGHT_BRACE) {
					break;
				}

				Token entryName = token;
				token = NextToken();
				assert(entryName.type == TOKEN_IDENTIFIER);
				Entry define = { .type = ENTRY_DEFINE, .name = TokenToString(entryName) };

				if (token.type == TOKEN_EQUALS) {
					size_t length = 0;
					while (!FoundEndOfLine(length)) length++;
					define.define.value = TokenToString((Token) { .value = (int) length, .text = buffer + position });
					position += length;
					token = NextToken();
				}

				arrput(entry.children, define);
			}

			arrput(root->children, entry);
		} else if (token.type == TOKEN_STRUCT) {
			Token structName = NextToken();
			assert(structName.type == TOKEN_IDENTIFIER);
			assert(NextToken().type == TOKEN_LEFT_BRACE);
			Entry entry = ParseRecord(false);
			entry.name = TokenToString(structName);
			arrput(root->children, entry);
		} else if (token.type == TOKEN_FUNCTION || token.type == TOKEN_FUNCTION_NOT_IN_KERNEL 
				|| token.type == TOKEN_FUNCTION_POINTER) {
			bool inKernel = token.type != TOKEN_FUNCTION_NOT_IN_KERNEL; 
			Entry objectFunctionType;
			bool firstVariable = true;
			Entry entry = { .type = ENTRY_FUNCTION, .function = { .inKernel = inKernel, .apiArrayIndex = 0 } };

			if (token.type == TOKEN_FUNCTION_POINTER) {
				entry.function.functionPointer = true;
			}

			Token leftParen = ParseVariable(NextToken(), true, NULL, &entry);
			assert(leftParen.type == TOKEN_LEFT_PAREN);
			Token token = NextToken();

			while (true) {
				if (token.type == TOKEN_RIGHT_PAREN) break;
				if (token.type == TOKEN_COMMA) token = NextToken();
				token = ParseVariable(token, true, firstVariable ? &objectFunctionType : NULL, &entry);
				firstVariable = false;
			}

			arrput(root->children, entry);
		} else if (token.type == TOKEN_API_TYPE) {
			Token name = NextToken(), parent = NextToken();
			Entry entry = { .type = ENTRY_API_TYPE, .name = TokenToString(name), .apiType = { .parent = TokenToString(parent) } };
			arrput(root->children, entry);
		} else if (token.type == TOKEN_TYPE_NAME) {
			Token oldName = NextToken(), newName = NextToken();
			Entry entry = { .type = ENTRY_TYPE_NAME, .name = TokenToString(newName), .oldTypeName = TokenToString(oldName) };
			arrput(root->children, entry);
		} else if (token.type == TOKEN_SEMICOLON) {
		} else if (token.type == TOKEN_EOF) {
			break;
		} else {
			printf("unexpected token '%.*s' at top level\n", token.value, token.text);
			exit(1);
		}
	}

	free(buffer);
	buffer = oldBuffer;
	position = oldPosition;
}

bool OutputCVariable(Entry *variable, bool noInitialValue, const char *nameOverride, 
		bool forFunction, bool forFunctionPointer) {
	fprintf(output, "%s%s%s", variable->variable.isVolatile ? "volatile " : "", 
			variable->variable.isConst ? "const " : "",
			variable->variable.isForwardDeclared ? "struct " : "");

	if (variable->variable.type) {
		if (0 == strcmp(variable->variable.type, "STRING")) {
			fprintf(output, "const char *%s", nameOverride ?: variable->name);
			assert(!variable->variable.pointer && !variable->variable.isArray);
			char *initialValue = variable->variable.initialValue;
			bool isBlankString = initialValue && 0 == strcmp(initialValue, "BLANK_STRING");
			if (!isBlankString) assert(!initialValue || !initialValue[0]);
			if (!noInitialValue && isBlankString) fprintf(output, " = nullptr");

			if (!forFunction) {
				fprintf(output, "; ptrdiff_t %sBytes; ", nameOverride ?: variable->name);
			} else {
				fprintf(output, ", ptrdiff_t %sBytes", nameOverride ?: variable->name);
			}

			if (!noInitialValue && isBlankString) { fprintf(output, " = -1"); return true; }
		} else {
			fprintf(output, "%s %.*s%s%s%s", variable->variable.type, variable->variable.pointer, "********", forFunctionPointer ? "(*" : "", 
					nameOverride ?: variable->name, forFunctionPointer ? ")" : "");

			if (variable->variable.isArray) {
				fprintf(output, "[%s]", variable->variable.arraySize ?: "");
			}

			char *initialValue = variable->variable.initialValue;

			if (initialValue && !noInitialValue) {
				fprintf(output, " = %s", initialValue);
				return true;
			}
		}
	} else {
		fprintf(output, "...");
	}

	return false;
}

void OutputCRecord(Entry *record, int indent) {
	for (int i = 0; i < arrlen(record->children); i++) {
		Entry *entry = record->children + i;
		for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");

		if (entry->type == ENTRY_VARIABLE) {
			OutputCVariable(entry, true, NULL, false, false);
			fprintf(output, ";\n");
		} else if (entry->type == ENTRY_UNION) {
			fprintf(output, "union {\n");
			OutputCRecord(entry, indent + 1);
			for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");
			fprintf(output, "};\n\n");
		} else if (entry->type == ENTRY_STRUCT) {
			fprintf(output, "struct {\n");
			OutputCRecord(entry, indent + 1);
			for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");
			fprintf(output, "} %s;\n\n", entry->name ?: "");
		}
	}
}

void OutputCFunction(Entry *entry) {
	if (entry->function.functionPointer) {
		fprintf(output, "typedef ");

		for (int i = 0; i < arrlen(entry->children); i++) {
			Entry *variable = entry->children + i;
			if (i >= 2) fprintf(output, ", ");
			OutputCVariable(variable, true, NULL, true, i == 0);
			if (i == 0) fprintf(output, "(");
		}

		fprintf(output, ");\n");

		return;
	}

	bool inKernel = entry->function.inKernel;
	if (!inKernel) fprintf(output, "#ifndef KERNEL\n");
	fprintf(output, "#ifdef ES_FORWARD\n#ifndef __cplusplus\nES_EXTERN_FORWARD ");

	// C code in API.

	for (int i = 0; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");
		OutputCVariable(variable, true, NULL, true, false);
		if (i == 0) fprintf(output, "(");
	}

	fprintf(output, ");\n#else\nES_EXTERN_FORWARD ");

	// C++ code in API.

	bool anyDefaultArguments = false;

	for (int i = 0; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");
		if (OutputCVariable(variable, false, NULL, true, false)) anyDefaultArguments = true;
		if (i == 0) fprintf(output, "(");
	}

	fprintf(output, ");\n#endif\n#endif\n#ifndef ES_DIRECT_API\ntypedef ");

	// Code in application.

	Entry *functionVariable = entry->children + 0;
	char *functionName = functionVariable->name;

	for (int i = 0; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");

		if (i == 0) {
			char name[256];
			sprintf(name, "(*__typeof_%s)", functionName);
			OutputCVariable(variable, true, name, true, false);
			fprintf(output, "(");
		} else {
			OutputCVariable(variable, true, NULL, true, false);
		}
	}

	fprintf(output, ");\n#ifndef __cplusplus\n#define %s ((__typeof_%s) ES_API_BASE[%d])\n#else\n", 
			functionName, functionName, entry->function.apiArrayIndex);

	if (anyDefaultArguments) {
		fprintf(output, "__attribute__((always_inline)) inline \n");

		// C++ code in application with default arguments.

		for (int i = 0; i < arrlen(entry->children); i++) {
			Entry *variable = entry->children + i;
			if (i >= 2) fprintf(output, ", ");
			OutputCVariable(variable, false, NULL, true, false);
			if (i == 0) fprintf(output, "(");
		}

		fprintf(output, ") { \n\t%s((__typeof_%s) ES_API_BASE[%d])(", 
				(functionVariable->variable.pointer == 0 && 0 == strcmp(functionVariable->variable.type, "void")) ? "" : "return ", 
				functionName, entry->function.apiArrayIndex);

		for (int i = 1; i < arrlen(entry->children); i++) {
			Entry *variable = entry->children + i;
			if (i > 1) fprintf(output, ", ");
			fprintf(output, "%s", variable->name);

			if (0 == strcmp(variable->variable.type, "STRING")) {
				fprintf(output, ", %sBytes", variable->name);
			}
		}

		fprintf(output, "); }");
	} else {
		// C/C++ code in application without default arguments.

		fprintf(output, "#define %s ((__typeof_%s) ES_API_BASE[%d])", 
				functionName, functionName, entry->function.apiArrayIndex);
	}

	fprintf(output, "\n#endif\n#endif\n");
	if (!inKernel) fprintf(output, "#endif\n");
}

void OutputC(Entry *root) {
	{
		char *buffer = (char *) malloc(4194304);
		FILE *input = fopen("desktop/prefix.h", "r");
		if (outputDependencies) fprintf(outputDependencies, "%s\n", "desktop/prefix.h");
		buffer[fread(buffer, 1, 4194304, input)] = 0;
		fprintf(output, "%s\n", buffer);
		free(buffer);
	}

	for (int i = 0; i < arrlen(root->children); i++) {
		Entry *entry = root->children + i;

		if (entry->type == ENTRY_DEFINE) {
			fprintf(output, "#define %s (%s)\n", entry->name, entry->define.value);
		} else if (entry->type == ENTRY_DEFINE_PRIVATE) {
			fprintf(output, "#ifdef ES_API\n#define %s (%s)\n#endif\n", entry->name, entry->define.value);
		} else if (entry->type == ENTRY_STRUCT) {
			fprintf(output, "typedef struct %s {\n", entry->name);
			OutputCRecord(entry, 0);
			fprintf(output, "} %s;\n\n", entry->name);
		} else if (entry->type == ENTRY_ENUM) {
			bool isSyscallType = 0 == strcmp(entry->name, "EsSyscallType");
			fprintf(output, "typedef enum %s {\n", entry->name);

			for (int i = 0; i < arrlen(entry->children); i++) {
				if (entry->children[i].define.value) {
					fprintf(output, "\t%s = %s,\n", entry->children[i].name, entry->children[i].define.value);
				} else {
					fprintf(output, "\t%s,\n", entry->children[i].name);
				}

				if (isSyscallType && outputSyscallArray) {
					fprintf(outputSyscallArray, "Do%s,\n", entry->children[i].name);
				}
			}

			fprintf(output, "} %s;\n\n", entry->name);
		} else if (entry->type == ENTRY_API_TYPE) {
			fprintf(output, "#ifdef __cplusplus\nstruct %s;\n#else\n#define %s %s\n#endif\n", 
					entry->name, entry->name, 0 == strcmp(entry->apiType.parent, "none") ? "void" : entry->apiType.parent);
		} else if (entry->type == ENTRY_FUNCTION) {
			OutputCFunction(entry);
		} else if (entry->type == ENTRY_TYPE_NAME) {
			fprintf(output, "typedef %s %s;\n", entry->oldTypeName, entry->name);
		}
	}

	for (int i = 0; i < arrlen(root->children); i++) {
		Entry *entry = root->children + i;

		if (entry->type == ENTRY_API_TYPE) {
			bool hasParent = 0 != strcmp(entry->apiType.parent, "none");

			if (!hasParent) {
				fprintf(output, "#ifdef __cplusplus\n#ifndef ES_API\n#ifndef KERNEL\nstruct %s {\n\tvoid *_private;\n", entry->name);
			} else {
				fprintf(output, "#ifdef __cplusplus\n#ifndef ES_API\n#ifndef KERNEL\nstruct %s : %s {\n", entry->name, entry->apiType.parent);
			}

			fprintf(output, "};\n#endif\n#endif\n#endif\n");
		}
	}

	fprintf(output, "#endif\n");
}

char *OdinTrimPrefix(char *in) {
	if (in[0] == 'E' && in[1] == 's' && isupper(in[2])) {
		return in + 2;
	} else if (in[0] == 'E' && in[1] == 'S' && in[2] == '_') {
		return in + 3;
	} else {
		return in;
	}
}

char *OdinReplaceTypes(const char *string, bool exact) {
	char *copy = (char *) malloc(strlen(string) + 1);
	strcpy(copy, string);

#define ODIN_REPLACE(x, y) (0 == strcmp(copy, x) || (!exact && strstr(copy, x))) memcpy(strstr(copy, x), y "                              ", strlen(x))

	if ODIN_REPLACE("uint64_t", "u64");
	else if ODIN_REPLACE("int64_t", "i64");
	else if ODIN_REPLACE("uint32_t", "u32");
	else if ODIN_REPLACE("int32_t", "i32");
	else if ODIN_REPLACE("uint16_t", "u16");
	else if ODIN_REPLACE("int16_t", "i16");
	else if ODIN_REPLACE("uint8_t", "u8");
	else if ODIN_REPLACE("int8_t", "i8");
	else if ODIN_REPLACE("char", "i8");
	else if ODIN_REPLACE("intptr_t", "int");
	else if ODIN_REPLACE("size_t", "int");
	else if ODIN_REPLACE("ptrdiff_t", "int");
	else if ODIN_REPLACE("uintptr_t", "uint");
	else if ODIN_REPLACE("(_EsLongConstant)", "");
	else if ODIN_REPLACE("unsigned", "u32");
	else if ODIN_REPLACE("int", "i32");
	else if ODIN_REPLACE("long", "i64");
	else if ODIN_REPLACE("double", "f64");
	else if ODIN_REPLACE("float", "f32");
	else if ODIN_REPLACE("EsLongDouble", "[16]u8");
	else if ODIN_REPLACE("EsCString", "cstring");

	while ODIN_REPLACE("Es", "");
	while ODIN_REPLACE("ES_", "");

	return OdinTrimPrefix(copy);
}

void OutputOdinType(Entry *entry) {
	if (0 == strcmp(entry->variable.type, "void")) {
		assert(entry->variable.pointer);
		entry->variable.type = (char *) "rawptr";
		entry->variable.pointer--;
	}

	fprintf(output, "%c%s%c%.*s%s", entry->variable.isArray ? '[' : ' ', entry->variable.arraySize ? OdinReplaceTypes(entry->variable.arraySize, true) : "",
			entry->variable.isArray ? ']' : ' ', entry->variable.pointer, "^^^^^^^^^^^^^^^^^", OdinReplaceTypes(entry->variable.type, true));
}

void OutputOdinVariable(Entry *entry, bool expandStrings, const char *nameSuffix) {
	if (!entry->variable.type) {
		fprintf(output, "_varargs%s : ..any", nameSuffix);
		return;
	}

	if (0 == strcmp(entry->name, "context")) {
		entry->name = (char *) "_context";
	}

	if (0 == strcmp(entry->variable.type, "STRING")) {
		if (expandStrings) {
			fprintf(output, "%s%s : ^u8, %sBytes%s : int", entry->name, nameSuffix, entry->name, nameSuffix);
		} else {
			fprintf(output, "%s%s : string", entry->name, nameSuffix);
		}
	} else {
		fprintf(output, "%s%s : ", entry->name, nameSuffix);
		OutputOdinType(entry);
	}
}

void OutputOdinRecord(Entry *record, int indent) {
	for (int i = 0; i < arrlen(record->children); i++) {
		Entry *entry = record->children + i;
		for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");

		if (entry->type == ENTRY_VARIABLE) {
			OutputOdinVariable(entry, false, "");
			fprintf(output, ",\n");
		} else if (entry->type == ENTRY_UNION) {
			fprintf(output, "using _ : struct #raw_union {\n");
			OutputOdinRecord(entry, indent + 1);
			for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");
			fprintf(output, "},\n");
		} else if (entry->type == ENTRY_STRUCT) {
			fprintf(output, "%s : struct {\n", entry->name ?: "using _ ");
			OutputOdinRecord(entry, indent + 1);
			for (int i = 0; i < indent + 1; i++) fprintf(output, "\t");
			fprintf(output, "},\n");
		}
	}
}

void OutputOdinFunction(Entry *entry) {
	bool hasReturnValue = strcmp(entry->children[0].variable.type, "void") || entry->children[0].variable.pointer;

	for (int i = 0; i < arrlen(entry->children); i++) {
		if (entry->children[i].variable.type && strstr(entry->children[i].variable.type, "va_list")) {
			return;
		}
	}

	if (entry->function.functionPointer) {
		fprintf(output, "%s :: distinct #type proc \"c\" (", OdinTrimPrefix(entry->children[0].name));

		for (int i = 1; i < arrlen(entry->children); i++) {
			Entry *variable = entry->children + i;
			if (i >= 2) fprintf(output, ", ");
			OutputOdinType(variable);
		}

		fprintf(output, ")");

		if (hasReturnValue) {
			fprintf(output, " -> ");
			OutputOdinType(entry->children + 0);
		}

		fprintf(output, ";\n");

		return;
	}

	fprintf(output, "%s :: #force_inline proc \"c\" (", OdinTrimPrefix(entry->children[0].name));

	for (int i = 1; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");
		OutputOdinVariable(variable, false, "_");

		if (variable->variable.initialValue) {
			// fprintf(stderr, "initial value: %s\n", variable->variable.initialValue);

			const char *initialValue = OdinTrimPrefix(variable->variable.initialValue);

			if (0 == strcmp(initialValue, "NULL")) {
				initialValue = "nil";
			} else if (0 == strcmp(initialValue, "DEFAULT_PROPERTIES")) {
				initialValue = "{}";
			} else if (0 == strcmp(initialValue, "BLANK_STRING")) {
				initialValue = "\"\"";
			}

			fprintf(output, " = %s", initialValue);
		}
	}

	fprintf(output, ")");

	if (hasReturnValue) {
		fprintf(output, " -> ");
		OutputOdinType(entry->children + 0);
	}

	fprintf(output, "{ addr := 0x1000 + %d * size_of(int); fp := (rawptr(((^uintptr)(uintptr(addr)))^)); ", entry->function.apiArrayIndex);

	if (hasReturnValue) {
		fprintf(output, "return ");
	}

	fprintf(output, "((proc \"c\" (");

	for (int i = 1; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");

		if (variable->variable.type) {
			if (0 == strcmp(variable->variable.type, "STRING")) {
				fprintf(output, "^u8, int");
			} else {
				OutputOdinType(variable);
			}
		} else {
			fprintf(output, "..any");
		}
	}

	fprintf(output, ")");

	if (hasReturnValue) {
		fprintf(output, " -> ");
		OutputOdinType(entry->children + 0);
	}

	fprintf(output, ") (fp))(");

	for (int i = 1; i < arrlen(entry->children); i++) {
		Entry *variable = entry->children + i;
		if (i >= 2) fprintf(output, ", ");

		if (variable->variable.type && 0 == strcmp(variable->variable.type, "STRING")) {
			fprintf(output, "raw_data(%s_), len(%s_)", variable->name, variable->name);
		} else {
			fprintf(output, "%s_", variable->name ?: "_varargs");
		}
	}

	fprintf(output, "); ");

	fprintf(output, "}\n");
}

void OutputOdin(Entry *root) {
	fprintf(output, "package es\n");

	fprintf(output, "Generic :: rawptr;\n");
	fprintf(output, "INSTANCE_TYPE :: Instance;\n");

	for (int i = 0; i < arrlen(root->children); i++) {
		Entry *entry = root->children + i;

		if (entry->type == ENTRY_DEFINE) {
			fprintf(output, "%s :: %s;\n", OdinTrimPrefix(entry->name), OdinReplaceTypes(entry->define.value, false));
		} else if (entry->type == ENTRY_STRUCT) {
			fprintf(output, "%s :: struct {\n", OdinTrimPrefix(entry->name));
			OutputOdinRecord(entry, 0);
			fprintf(output, "}\n");
		} else if (entry->type == ENTRY_ENUM) {
			fprintf(output, "using %s :: enum i32 {\n", OdinTrimPrefix(entry->name));

			for (int i = 0; i < arrlen(entry->children); i++) {
				if (entry->children[i].define.value) {
					fprintf(output, "\t%s = %s,\n", OdinTrimPrefix(entry->children[i].name), entry->children[i].define.value);
				} else {
					fprintf(output, "\t%s,\n", OdinTrimPrefix(entry->children[i].name));
				}
			}

			fprintf(output, "}\n");
		} else if (entry->type == ENTRY_API_TYPE) {
			bool hasParent = 0 != strcmp(entry->apiType.parent, "none");
			fprintf(output, "%s :: %s;\n", OdinTrimPrefix(entry->name), hasParent ? OdinTrimPrefix(entry->apiType.parent) : "rawptr");
		} else if (entry->type == ENTRY_FUNCTION) {
			OutputOdinFunction(entry);
		} else if (entry->type == ENTRY_TYPE_NAME) {
			fprintf(output, "%s :: %s;\n", OdinTrimPrefix(entry->name), OdinTrimPrefix(OdinReplaceTypes(entry->oldTypeName, true)));
		}
	}
}

int main(int argc, char **argv) {
	if (argc < 2) {
		outputDependencies = fopen(DEST_DEPENDENCIES ".tmp", "wb");
		fprintf(outputDependencies, ": \n");
	}

	Entry root = {};
	ParseFile(&root, "desktop/os.header");

	const char *language = "c";

	if (argc == 3) {
		language = argv[1];
		output = fopen(argv[2], "wb");
	} else if (argc == 1) {
		output = fopen(DEST_OS, "wb");
		outputAPIArray = fopen(DEST_API_ARRAY, "wb");
		outputSyscallArray = fopen(DEST_SYSCALL_ARRAY, "wb");
	} else {
		fprintf(stderr, "Usage: %s <language> <path-to-output-file>\n", argv[0]);
		return 1;
	}

	{
		{
			EsINIState s = { (char *) LoadFile("util/api_table.ini", &s.bytes) };

			int32_t highestIndex = -1;

			struct {
				const char *cName;
				int32_t index;
			} *entries = NULL;

			while (EsINIParse(&s)) {
				EsINIZeroTerminate(&s);
				int32_t index = atoi(s.value);
				if (index < 0 || !s.key[0]) goto done;
				if (index > highestIndex) highestIndex = index;
				arraddn(entries, 1);
				char *copy = (char *) malloc(strlen(s.key) + 1);
				arrlast(entries).cName = copy;
				strcpy(copy, s.key);
				arrlast(entries).index = index;
			}

			done:;

			arrsetlen(apiTableEntries, (size_t) (highestIndex + 1));

			for (int i = 0; i < highestIndex + 1; i++) {
				apiTableEntries[i] = "EsUnimplemented";
			}

			for (int i = 0; i < arrlen(entries); i++) {
				apiTableEntries[entries[i].index] = entries[i].cName;
			}

			arrfree(entries);
		}

		for (int i = 0; i < arrlen(root.children); i++) {
			Entry *entry = root.children + i;
			if (entry->type != ENTRY_FUNCTION || entry->function.functionPointer) continue;
			const char *name = entry->children[0].name;
			bool found = false;

			for (int i = 0; i < arrlen(apiTableEntries); i++) {
				if (0 == strcmp(apiTableEntries[i], name)) {
					entry->function.apiArrayIndex = i;
					found = true;
					break;
				}
			}

			if (!found) {
				for (int i = 0; i < arrlen(apiTableEntries); i++) {
					if (0 == strcmp(apiTableEntries[i], "EsUnimplemented")) {
						apiTableEntries[i] = name;
						entry->function.apiArrayIndex = i;
						found = true;
						break;
					}
				}

				if (!found) {
					entry->function.apiArrayIndex = arrlen(apiTableEntries);
					arrput(apiTableEntries, name);
				}
			}
		}

		for (int i = 0; i < arrlen(apiTableEntries); i++) {
			apiTableEntries[i] = "EsUnimplemented";
		}

		for (int i = 0; i < arrlen(root.children); i++) {
			Entry *entry = root.children + i;
			if (entry->type != ENTRY_FUNCTION || entry->function.functionPointer) continue;
			const char *name = entry->children[0].name;
			apiTableEntries[entry->function.apiArrayIndex] = name;
		}

		if (outputAPIArray) {
			FILE *f = fopen("util/api_table.ini", "wb");

			for (int i = 0; i < arrlen(apiTableEntries); i++) {
				fprintf(outputAPIArray, "(void *) %s,\n", apiTableEntries[i]);

				if (strcmp(apiTableEntries[i], "EsUnimplemented")) {
					fprintf(f, "%s=%d\n", apiTableEntries[i], i);
				}
			}

			fclose(f);
		}
	}

	if (0 == strcmp(language, "c")) {
		OutputC(&root);
	} else if (0 == strcmp(language, "odin")) {
		OutputOdin(&root);
	} else {
		fprintf(stderr, "Unsupported language '%s'.\nLanguage must be one of: 'c', 'odin'.\n", language);
	}

	if (argc < 2) {
		system("mv " DEST_DEPENDENCIES ".tmp " DEST_DEPENDENCIES);
	}

	return 0;
}
