#ifndef IMPLEMENTATION

// TODO Don't send key released messages if the focused window has changed.
// TODO Allowing menus to receive keyboard input.

struct EmbeddedWindow {
	void Destroy();
	void Close();
	void SetEmbedOwner(Process *process);

	Surface surface;
	Process *volatile owner;
	void *volatile apiWindow;
	volatile uint32_t handles;
	struct Window *container;
	uint64_t id;
	uint32_t resizeClearColor;
	bool closed;
};

struct Window {
	void Update(EsRectangle *region, bool skipUpdateScreen);
	bool UpdateDirect(K_USER_BUFFER void *bits, uintptr_t stride, EsRectangle region);
	void Destroy(); 
	void Close();
	bool Move(EsRectangle newBounds, uint32_t flags);
	void SetEmbed(EmbeddedWindow *window);
	bool IsVisible();

	// State:
	EsWindowStyle style;
	int shadowSize;
	bool solid, noClickActivate, hidden, isMaximised, alwaysOnTop, hoveringOverEmbed;
	volatile bool closed;

	// Appearance:
	Surface surface;
	EsRectangle opaqueBounds, blurBounds;
	uint8_t alpha;
	uint8_t material;

	// Owner and children:
	Process *owner;
	void *apiWindow;
	EmbeddedWindow *embed;
	volatile uint64_t handles;
	uint64_t id;

	// Location:
	EsPoint position;
	size_t width, height;
};

struct WindowManager {
	void *CreateWindow(Process *process, void *apiWindow, EsWindowStyle style);
	void *CreateEmbeddedWindow(Process *process, void *apiWindow);
	Window *FindWindowAtPosition(int cursorX, int cursorY);

	void Initialise();

	void MoveCursor(int xMovement, int yMovement);
	void ClickCursor(unsigned buttons);
	void UpdateCursor(int xMovement, int yMovement, unsigned buttons);
	void PressKey(unsigned scancode);

	void Redraw(EsPoint position, int width, int height, Window *except = nullptr, int startingAt = 0, bool addToModifiedRegion = true);

	bool ActivateWindow(Window *window); // Returns true if any menus were closed.
	void HideWindow(Window *window);
	Window *FindWindowToActivate(Window *excluding = nullptr);
	uintptr_t GetActivationZIndex();
	void ChangeWindowDepth(Window *window, bool alwaysRedraw, ptrdiff_t newZDepth);
	intptr_t FindWindowDepth(Window *window);
	bool CloseMenus(); // Returns true if any menus were closed.
	
	void StartEyedrop(uintptr_t object, Window *avoid, uint32_t cancelColor);
	void EndEyedrop(bool cancelled);

	bool initialised, drawing;

	Window **windows; // Sorted by z.
	EmbeddedWindow **embeddedWindows;
	Window *pressedWindow, *activeWindow, *hoverWindow;

	KMutex mutex;

	KEvent windowsToCloseEvent;

	int cursorX, cursorY;
	int cursorXPrecise, cursorYPrecise; // Scaled up by a factor of 10.

	Surface cursorSurface, cursorSwap;
	int cursorImageOffsetX, cursorImageOffsetY;
	uintptr_t cursorID;
	bool changedCursorImage;

	unsigned lastButtons;
	bool shift, alt, ctrl;
	bool shift2, alt2, ctrl2;
	bool numlock;

	uint64_t clickChainStartMs;
	unsigned clickChainCount;
	int clickChainX, clickChainY;
	unsigned altTabCount;

	uintptr_t eyedropObject;
	bool eyedropping;
	Process *eyedropProcess;
	uint64_t eyedropAvoidID;
	uint32_t eyedropCancelColor;

	EsRectangle workArea;

	uint64_t currentWindowID;

	Surface windowFrameBitmap;

	KMutex gameControllersMutex;
	EsGameControllerState gameControllers[ES_GAME_CONTROLLER_MAX_COUNT];
	size_t gameControllerCount;
	uint64_t gameControllerID;
};

struct ClipboardItem {
	uint64_t format;
	ConstantBuffer *buffer;
};

struct Clipboard {
	KMutex mutex;
#define CLIPBOARD_ITEM_COUNT (1)
	ClipboardItem items[CLIPBOARD_ITEM_COUNT];

#define CLIPBOARD_ITEM_SIZE_LIMIT (64 * 1024 * 1024)
	EsError Add(uint64_t format, K_USER_BUFFER const void *data, size_t dataBytes);
	bool Has(uint64_t format);
	EsHandle Read(uint64_t format, K_USER_BUFFER size_t *bytes, Process *process);
};

WindowManager windowManager;
Clipboard primaryClipboard;

void SendMessageToWindow(Window *window, EsMessage &message);
void BroadcastMessage(EsMessage &message);

#define UI_SCALE (100)
#define WINDOW_INSET (19 * UI_SCALE / 100)
#define CONTAINER_TAB_BAND_HEIGHT (33 * UI_SCALE / 100)
#define BORDER_THICKNESS (9 * UI_SCALE / 100)

#else

bool Window::IsVisible() {
	return !hidden && !closed && (id != windowManager.eyedropAvoidID || !windowManager.eyedropping);
}

void SendMessageToWindow(Window *window, EsMessage &message) {
	windowManager.mutex.AssertLocked();

	if (window->closed) {
		return;
	}

	if (!window->owner->handles) {
		KernelPanic("SendMessageToWindow - (%x:%d/%x:%d) No handles.\n", window, window->handles, window->owner, window->owner->handles);
	}

	if (window->style != ES_WINDOW_CONTAINER || !window->embed) {
		window->owner->messageQueue.SendMessage(window->apiWindow, message);
		return;
	}

	if (message.type == ES_MSG_WINDOW_RESIZED) {
		message.windowResized.content = ES_MAKE_RECTANGLE(0, window->width, 0, window->height);
		window->owner->messageQueue.SendMessage(window->apiWindow, message);

		message.windowResized.content = ES_MAKE_RECTANGLE(0, window->width - WINDOW_INSET * 2, 0, window->height - WINDOW_INSET * 2 - CONTAINER_TAB_BAND_HEIGHT);
		window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);
	} else if (message.type == ES_MSG_WINDOW_DEACTIVATED || message.type == ES_MSG_WINDOW_ACTIVATED) {
		window->owner->messageQueue.SendMessage(window->apiWindow, message);
		window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);
	} else if (message.type == ES_MSG_MOUSE_MOVED) {
		EsRectangle embedRegion = ES_MAKE_RECTANGLE(WINDOW_INSET, window->width - WINDOW_INSET, WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT, window->height - WINDOW_INSET);
		bool inEmbed = windowManager.pressedWindow 
			? window->hoveringOverEmbed 
			: IsPointInRectangle(embedRegion, message.mouseMoved.newPositionX, message.mouseMoved.newPositionY);

		if (inEmbed) {
			message.mouseMoved.oldPositionX -= WINDOW_INSET;
			message.mouseMoved.newPositionX -= WINDOW_INSET;
			message.mouseMoved.oldPositionY -= WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT;
			message.mouseMoved.newPositionY -= WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT;

			window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);

			if (!windowManager.pressedWindow && !window->hoveringOverEmbed) {
				message.type = ES_MSG_MOUSE_EXIT;
				window->owner->messageQueue.SendMessage(window->apiWindow, message);
			}
		} else {
			window->owner->messageQueue.SendMessage(window->apiWindow, message);

			if (!windowManager.pressedWindow && window->hoveringOverEmbed) {
				message.type = ES_MSG_MOUSE_EXIT;
				window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);
			}
		}

		if (!windowManager.pressedWindow) {
			window->hoveringOverEmbed = inEmbed;
		}
	} else if (message.type >= ES_MSG_MOUSE_LEFT_DOWN && message.type <= ES_MSG_MOUSE_MIDDLE_UP) {
		if (window->hoveringOverEmbed) {
			message.mouseDown.positionX -= WINDOW_INSET;
			message.mouseDown.positionY -= WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT;

			window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);
		} else {
			window->owner->messageQueue.SendMessage(window->apiWindow, message);
		}
	} else if (message.type == ES_MSG_KEY_DOWN || message.type == ES_MSG_KEY_UP) {
		window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);

		// TODO Better method of handling keyboard shortcuts that Desktop needs to process?
		// 	By sending the message to both processes, two threads will wake up,
		// 	which could increase latency of handling the message.
		window->owner->messageQueue.SendMessage(window->apiWindow, message);
	} else if (message.type == ES_MSG_MOUSE_EXIT || message.type == ES_MSG_PRIMARY_CLIPBOARD_UPDATED) {
		window->embed->owner->messageQueue.SendMessage(window->embed->apiWindow, message);
		window->owner->messageQueue.SendMessage(window->apiWindow, message);
	} else {
		window->owner->messageQueue.SendMessage(window->apiWindow, message);
	}
}

Window *WindowManager::FindWindowAtPosition(int cursorX, int cursorY) {
	mutex.AssertLocked();

	for (intptr_t i = ArrayLength(windows) - 1; i >= 0; i--) {
		Window *window = windows[i];

		if (window->width > graphics.width * 10 || window->height > graphics.height * 10) {
			KernelPanic("WindowManager::FindWindowAtPosition - Window %x has invalid dimensions.\n", window);
		}

		if (window->solid && !window->hidden
				&& cursorX >= window->position.x + window->shadowSize && cursorX < window->position.x + (int) window->width  - window->shadowSize
				&& cursorY >= window->position.y + window->shadowSize && cursorY < window->position.y + (int) window->height - window->shadowSize) {
			return window;
		}
	}

	return nullptr;
}

void WindowManager::UpdateCursor(int xMovement, int yMovement, unsigned buttons) {
	if (!initialised) {
		return;
	}

	if (xMovement || yMovement) {
		if (xMovement * xMovement + yMovement * yMovement < 10 && buttons != lastButtons) {
			// This seems to be movement noise generated when the buttons were pressed/released.
		} else {
			mutex.Acquire();
			MoveCursor(xMovement, yMovement);
			mutex.Release();
		}
	} 

	ClickCursor(buttons);
}

void WindowManager::EndEyedrop(bool cancelled) {
	mutex.AssertLocked();

	eyedropping = false;

	EsMessage m = { ES_MSG_EYEDROP_REPORT };
	uint32_t color = *(uint32_t *) ((uint8_t *) graphics.frameBuffer.bits + 4 * cursorX + graphics.frameBuffer.stride * cursorY) | 0xFF000000;
	m.eyedrop.color = cancelled ? eyedropCancelColor : color;
	m.eyedrop.cancelled = cancelled;
	eyedropProcess->messageQueue.SendMessage((void *) eyedropObject, m);
	CloseHandleToObject(eyedropProcess, KERNEL_OBJECT_PROCESS);
	eyedropProcess = nullptr;

	Redraw(ES_MAKE_POINT(0, 0), graphics.width, graphics.height);
}

void WindowManager::PressKey(unsigned scancode) {
	if (!initialised) {
		return;
	}

	bool moveCursorNone = false;

	mutex.Acquire();

	if (scancode == ES_SCANCODE_NUM_DIVIDE) {
		KernelPanic("WindowManager::PressKey - Panic key pressed.\n");
	}

	if (eyedropping) {
		if (scancode == (ES_SCANCODE_ESCAPE | K_SCANCODE_KEY_RELEASED)) {
			EndEyedrop(true);
			moveCursorNone = true;
		}

		goto done;
	}

	// TODO Caps lock.

	if (scancode == ES_SCANCODE_LEFT_CTRL) ctrl = true;
	if (scancode == (ES_SCANCODE_LEFT_CTRL | K_SCANCODE_KEY_RELEASED)) ctrl = false;
	if (scancode == ES_SCANCODE_LEFT_SHIFT) shift = true;
	if (scancode == (ES_SCANCODE_LEFT_SHIFT | K_SCANCODE_KEY_RELEASED)) shift = false;
	if (scancode == ES_SCANCODE_LEFT_ALT) alt = true;
	if (scancode == (ES_SCANCODE_LEFT_ALT | K_SCANCODE_KEY_RELEASED)) alt = false;

	if (scancode == ES_SCANCODE_RIGHT_CTRL) ctrl2 = true;
	if (scancode == (ES_SCANCODE_RIGHT_CTRL | K_SCANCODE_KEY_RELEASED)) ctrl2 = false;
	if (scancode == ES_SCANCODE_RIGHT_SHIFT) shift2 = true;
	if (scancode == (ES_SCANCODE_RIGHT_SHIFT | K_SCANCODE_KEY_RELEASED)) shift2 = false;
	if (scancode == ES_SCANCODE_RIGHT_ALT) alt2 = true;
	if (scancode == (ES_SCANCODE_RIGHT_ALT | K_SCANCODE_KEY_RELEASED)) alt2 = false;

	if (scancode == (ES_SCANCODE_LEFT_ALT | K_SCANCODE_KEY_RELEASED) || scancode == (ES_SCANCODE_RIGHT_ALT | K_SCANCODE_KEY_RELEASED)) {
		altTabCount = 0;
	}

	if (activeWindow) {
		Window *window = activeWindow;

		EsMessage message;
		EsMemoryZero(&message, sizeof(EsMessage));
		message.type = (scancode & K_SCANCODE_KEY_RELEASED) ? ES_MSG_KEY_UP : ES_MSG_KEY_DOWN;
		message.keyboard.alt = alt | alt2;
		message.keyboard.ctrl = ctrl | ctrl2;
		message.keyboard.shift = shift | shift2;
		message.keyboard.scancode = scancode & ~K_SCANCODE_KEY_RELEASED;

		static uint8_t heldKeys[512 / 8] = {};

		if (message.keyboard.scancode >= 512) {
			KernelPanic("WindowManager::PressKey - Scancode outside valid range.\n");
		}
		
		if (message.type == ES_MSG_KEY_DOWN && (heldKeys[message.keyboard.scancode / 8] & (1 << (message.keyboard.scancode % 8)))) {
			message.keyboard.repeat = true;
		}

		if (message.type == ES_MSG_KEY_DOWN) {
			heldKeys[message.keyboard.scancode / 8] |= (1 << (message.keyboard.scancode % 8));
		} else {
			heldKeys[message.keyboard.scancode / 8] &= ~(1 << (message.keyboard.scancode % 8));
		}

		SendMessageToWindow(window, message);

		int numpad = 0, nshift = 0;

		if (numlock) {
			// TODO This doesn't correctly work with releasing keys?

			if (scancode == ES_SCANCODE_NUM_DIVIDE  ) { numpad = ES_SCANCODE_SLASH; }
			if (scancode == ES_SCANCODE_NUM_MULTIPLY) { numpad = ES_SCANCODE_8; nshift = 1; }
			if (scancode == ES_SCANCODE_NUM_SUBTRACT) { numpad = ES_SCANCODE_HYPHEN; }
			if (scancode == ES_SCANCODE_NUM_ADD	) { numpad = ES_SCANCODE_EQUALS; nshift = 1; }
			if (scancode == ES_SCANCODE_NUM_ENTER	) { numpad = ES_SCANCODE_ENTER; }
			if (scancode == ES_SCANCODE_NUM_POINT	) { numpad = ES_SCANCODE_PERIOD; }
			if (scancode == ES_SCANCODE_NUM_0	) { numpad = ES_SCANCODE_0; }
			if (scancode == ES_SCANCODE_NUM_1	) { numpad = ES_SCANCODE_1; }
			if (scancode == ES_SCANCODE_NUM_2	) { numpad = ES_SCANCODE_2; }
			if (scancode == ES_SCANCODE_NUM_3	) { numpad = ES_SCANCODE_3; }
			if (scancode == ES_SCANCODE_NUM_4	) { numpad = ES_SCANCODE_4; }
			if (scancode == ES_SCANCODE_NUM_5	) { numpad = ES_SCANCODE_5; }
			if (scancode == ES_SCANCODE_NUM_6	) { numpad = ES_SCANCODE_6; }
			if (scancode == ES_SCANCODE_NUM_7	) { numpad = ES_SCANCODE_7; }
			if (scancode == ES_SCANCODE_NUM_8	) { numpad = ES_SCANCODE_8; }
			if (scancode == ES_SCANCODE_NUM_9	) { numpad = ES_SCANCODE_9; }
		} else {
			if (scancode == ES_SCANCODE_NUM_DIVIDE  ) { numpad = ES_SCANCODE_SLASH; }
			if (scancode == ES_SCANCODE_NUM_MULTIPLY) { numpad = ES_SCANCODE_8; nshift = 1; }
			if (scancode == ES_SCANCODE_NUM_SUBTRACT) { numpad = ES_SCANCODE_HYPHEN; }
			if (scancode == ES_SCANCODE_NUM_ADD	) { numpad = ES_SCANCODE_EQUALS; nshift = 1; }
			if (scancode == ES_SCANCODE_NUM_ENTER	) { numpad = ES_SCANCODE_ENTER; }
			if (scancode == ES_SCANCODE_NUM_POINT	) { numpad = ES_SCANCODE_DELETE; }
			if (scancode == ES_SCANCODE_NUM_0	) { numpad = ES_SCANCODE_INSERT; }
			if (scancode == ES_SCANCODE_NUM_1	) { numpad = ES_SCANCODE_END; }
			if (scancode == ES_SCANCODE_NUM_2	) { numpad = ES_SCANCODE_DOWN_ARROW; }
			if (scancode == ES_SCANCODE_NUM_3	) { numpad = ES_SCANCODE_PAGE_DOWN; }
			if (scancode == ES_SCANCODE_NUM_4	) { numpad = ES_SCANCODE_LEFT_ARROW; }
			if (scancode == ES_SCANCODE_NUM_6	) { numpad = ES_SCANCODE_RIGHT_ARROW; }
			if (scancode == ES_SCANCODE_NUM_7	) { numpad = ES_SCANCODE_HOME; }
			if (scancode == ES_SCANCODE_NUM_8	) { numpad = ES_SCANCODE_UP_ARROW; }
			if (scancode == ES_SCANCODE_NUM_9	) { numpad = ES_SCANCODE_PAGE_UP; }
		}

		if (numpad && !shift && !shift2) {
			EsMessage message;
			EsMemoryZero(&message, sizeof(EsMessage));
			message.type = (scancode & K_SCANCODE_KEY_RELEASED) ? ES_MSG_KEY_UP : ES_MSG_KEY_DOWN;
			message.keyboard.alt = alt | alt2;
			message.keyboard.ctrl = ctrl | ctrl2;
			message.keyboard.shift = nshift;
			message.keyboard.scancode = numpad;
			message.keyboard.numpad = true;
			SendMessageToWindow(window, message);
		}
	}

	done:;
	if (moveCursorNone) MoveCursor(0, 0);
	mutex.Release();
}

Window *WindowManager::FindWindowToActivate(Window *excluding) {
	for (uintptr_t i = ArrayLength(windows); i > 0; i--) {
		if (!windows[i - 1]->hidden 
				&& windows[i - 1]->style == ES_WINDOW_CONTAINER 
				&& windows[i - 1] != excluding) {
			return windows[i - 1];
		}
	}

	return nullptr;
}

uintptr_t WindowManager::GetActivationZIndex() {
	for (uintptr_t i = ArrayLength(windows); i > 0; i--) {
		if (!windows[i - 1]->alwaysOnTop) {
			return i;
		}
	}

	return 0;
}

void WindowManager::HideWindow(Window *window) {
	mutex.AssertLocked();

	if (window->hidden) return;
	window->hidden = true;

	if (window == activeWindow) {
		ActivateWindow(FindWindowToActivate());
	} 

	Redraw(ES_MAKE_POINT(window->position.x, window->position.y), window->width, window->height);
}

intptr_t WindowManager::FindWindowDepth(Window *window) {
	windowManager.mutex.AssertLocked();

	for (uintptr_t i = 0; i < ArrayLength(windows); i++) {
		if (windows[i] == window) {
			return i;
		}
	}

	return -1;
}

void WindowManager::ChangeWindowDepth(Window *window, bool redraw, ptrdiff_t _newZDepth) {
	windowManager.mutex.AssertLocked();

	// Reorder the windows in the array, and update the depth buffer.
	intptr_t oldZDepth = FindWindowDepth(window);
	if (oldZDepth == -1) KernelPanic("WindowManager::ChangeWindowDepth - Window %x was not in array.\n", window);
	ArrayDelete(windows, oldZDepth);
	intptr_t newZDepth = _newZDepth != -1 ? _newZDepth : GetActivationZIndex();
	ArrayInsert(windows, window, newZDepth);

	if (oldZDepth != newZDepth || redraw) {
		// Redraw the modified area of the screen.
		windowManager.Redraw(ES_MAKE_POINT(window->position.x, window->position.y), window->width, window->height);
	}
}

bool WindowManager::CloseMenus() {
	mutex.AssertLocked();

	EsMessage message;
	EsMemoryZero(&message, sizeof(EsMessage));

	bool result = false;

	for (uintptr_t i = 0; i < ArrayLength(windows); i++) {
		// Close any open menus.

		if (windows[i]->style == ES_WINDOW_MENU) {
			message.type = ES_MSG_WINDOW_DEACTIVATED;
			SendMessageToWindow(windows[i], message);
			result = true;
		}
	}

	return result;
}

bool WindowManager::ActivateWindow(Window *window) {
	mutex.AssertLocked();

	if (window && (window->style == ES_WINDOW_TIP || window->style == ES_WINDOW_MENU || window->closed)) {
		return false;
	}

	Window *oldWindow = activeWindow;

	EsMessage message;
	EsMemoryZero(&message, sizeof(EsMessage));

	bool result = CloseMenus();

	// Set the active window, unless it hasn't changed.

	if (activeWindow == window && (!window || !window->hidden)) {
		// Bring the window to the front anyway.
		if (window) ChangeWindowDepth(window, false, -1);
		return result;
	}

	activeWindow = window;
	if (window) window->hidden = false;

	if (window) {
		// Bring the window to the front.
		ChangeWindowDepth(window, true, -1);

		// Activate the new window.
		message.type = ES_MSG_WINDOW_ACTIVATED;
		SendMessageToWindow(window, message);
	} else {
		// No window is active.
	}

	if (oldWindow && oldWindow != window) {
		// Deactivate the old window.

		if (oldWindow != FindWindowAtPosition(cursorX, cursorY)) {
			message.type = ES_MSG_MOUSE_EXIT;
			SendMessageToWindow(oldWindow, message);
		}

		message.type = ES_MSG_WINDOW_DEACTIVATED;
		SendMessageToWindow(oldWindow, message);
	}

	return result;
}

void WindowManager::ClickCursor(unsigned buttons) {
	mutex.Acquire();

	unsigned delta = lastButtons ^ buttons;
	lastButtons = buttons;

	bool moveCursorNone = false;

	if (eyedropping && delta) {
		if ((delta & K_LEFT_BUTTON) && (~buttons & K_LEFT_BUTTON)) {
			EndEyedrop(false);
			moveCursorNone = true;
		}
	} else if (delta) {
		// Send a mouse pressed message to the window the cursor is over.
		Window *window = FindWindowAtPosition(cursorX, cursorY);

		bool activationClick = false, activationClickFromMenu = false;

		if (buttons == delta) {
			if (activeWindow != window) {
				activationClick = true;
			}

			if (window && window->noClickActivate) {
				ChangeWindowDepth(window, false, -1);

				if (window->style != ES_WINDOW_MENU) {
					activationClickFromMenu = CloseMenus();
				}
			} else {
				activationClickFromMenu = ActivateWindow(window);
			}

			
#define CLICK_CHAIN_TIMEOUT (500) // TODO Make this configurable.
			if (clickChainStartMs + CLICK_CHAIN_TIMEOUT < scheduler.timeMs
					|| DistanceSquared(cursorX, cursorY, clickChainX, clickChainY) >= 5
					|| !(delta & K_LEFT_BUTTON)
					|| activationClick) {
				// Start a new click chain.
				clickChainStartMs = scheduler.timeMs;
				clickChainCount = 1;
				clickChainX = cursorX;
				clickChainY = cursorY;
			} else {
				clickChainStartMs = scheduler.timeMs;
				clickChainCount++;
			}
		}

		{
			EsMessage message;
			EsMemoryZero(&message, sizeof(EsMessage));

			if (delta & K_LEFT_BUTTON) message.type = (buttons & K_LEFT_BUTTON) ? ES_MSG_MOUSE_LEFT_DOWN : ES_MSG_MOUSE_LEFT_UP;
			if (delta & K_RIGHT_BUTTON) message.type = (buttons & K_RIGHT_BUTTON) ? ES_MSG_MOUSE_RIGHT_DOWN : ES_MSG_MOUSE_RIGHT_UP;
			if (delta & K_MIDDLE_BUTTON) message.type = (buttons & K_MIDDLE_BUTTON) ? ES_MSG_MOUSE_MIDDLE_DOWN : ES_MSG_MOUSE_MIDDLE_UP;

			if (message.type == ES_MSG_MOUSE_LEFT_DOWN) {
				pressedWindow = window;
			} else if (message.type == ES_MSG_MOUSE_LEFT_UP) {
				if (pressedWindow) {
					// Always send the messages to the pressed window, if there is one.
					window = pressedWindow;
				}

				pressedWindow = nullptr;
				moveCursorNone = true; // We might have moved outside the window.
			}
			
			if (window) {
				if (!activationClickFromMenu) {
					message.mouseDown.positionX = cursorX - window->position.x;
					message.mouseDown.positionY = cursorY - window->position.y;
					message.mouseDown.positionXScreen = cursorX;
					message.mouseDown.positionYScreen = cursorY;
					message.mouseDown.clickChainCount = clickChainCount;
					message.mouseDown.alt = alt | alt2;
					message.mouseDown.ctrl = ctrl | ctrl2;
					message.mouseDown.shift = shift | shift2;
					message.mouseDown.activationClick = activationClick;

					SendMessageToWindow(window, message);
				}
			}
		}
	}

	if (moveCursorNone) {
		MoveCursor(0, 0);
	} else {
		GraphicsUpdateScreen();
	}

	mutex.Release();
}

void WindowManager::MoveCursor(int xMovement, int yMovement) {
	mutex.AssertLocked();

	xMovement *= alt ? 2 : 10;
	yMovement *= alt ? 2 : 10;

	int oldCursorX = cursorXPrecise;
	int oldCursorY = cursorYPrecise;
	int _cursorX = oldCursorX + xMovement;
	int _cursorY = oldCursorY + yMovement;

	if (!lastButtons) {
		if (_cursorX < 0) {
			_cursorX = 0;
		}

		if (_cursorY < 0) {
			_cursorY = 0;
		}

		if (_cursorX >= (int) graphics.width * 10) {
			_cursorX = graphics.width * 10 - 1;
		}

		if (_cursorY >= (int) graphics.height * 10) {
			_cursorY = graphics.height * 10 - 1;
		}
	}

	cursorXPrecise = _cursorX;
	cursorYPrecise = _cursorY;
	cursorX = _cursorX / 10;
	cursorY = _cursorY / 10;
	oldCursorX /= 10;
	oldCursorY /= 10;

	if (eyedropping) {
		EsMessage m = { ES_MSG_EYEDROP_REPORT };
		uint32_t color = *(uint32_t *) ((uint8_t *) graphics.frameBuffer.bits + 4 * cursorX + graphics.frameBuffer.stride * cursorY);
		m.eyedrop.color = color;
		m.eyedrop.cancelled = false;
		eyedropProcess->messageQueue.SendMessage((void *) eyedropObject, m);
	} else {
		Window *window = pressedWindow;

		if (!window) {
			// Work out which window the mouse is now over.
			window = FindWindowAtPosition(cursorX, cursorY);

			if (hoverWindow != window && hoverWindow) {
				EsMessage message;
				EsMemoryZero(&message, sizeof(EsMessage));
				message.type = ES_MSG_MOUSE_EXIT;
				SendMessageToWindow(hoverWindow, message);
			}

			hoverWindow = window;
		}

		if (window) {
			EsMessage message;
			EsMemoryZero(&message, sizeof(EsMessage));
			message.type = ES_MSG_MOUSE_MOVED;
			message.mouseMoved.newPositionX = cursorX - window->position.x;
			message.mouseMoved.newPositionY = cursorY - window->position.y;
			message.mouseMoved.newPositionXScreen = cursorX;
			message.mouseMoved.newPositionYScreen = cursorY;
			message.mouseMoved.oldPositionX = oldCursorX - window->position.x;
			message.mouseMoved.oldPositionY = oldCursorY - window->position.y;
			SendMessageToWindow(window, message);
		}
	}

	GraphicsUpdateScreen();
}

void _CloseWindows(uintptr_t) {
	while (true) {
		windowManager.windowsToCloseEvent.Wait();
		windowManager.mutex.Acquire();

		for (uintptr_t i = 0; i < ArrayLength(windowManager.windows); i++) {
			Window *window = windowManager.windows[i];

			if (window->handles > 1) {
				continue;
			}

			// Remove the window from the array.
			ArrayDelete(windowManager.windows, i);

			if (!window->closed) {
				// Only the window manager's handle to the window remains, 
				// but the window has not been closed.
				// This probably means the process crashed before it could close its windows;
				// we should close the window ourselves.
				window->Close();
			}

			// Close the window manager's handle to the window.
			CloseHandleToObject(window, KERNEL_OBJECT_WINDOW);

			i--;
		}

		for (uintptr_t i = 0; i < ArrayLength(windowManager.embeddedWindows); i++) {
			// Apply a similar set of operations to embedded windows.
			EmbeddedWindow *window = windowManager.embeddedWindows[i];
			if (window->handles > 1) continue;
			if (!window->closed) window->Close();
			ArrayDelete(windowManager.embeddedWindows, i);
			CloseHandleToObject(window, KERNEL_OBJECT_EMBEDDED_WINDOW);
			i--;
		}

		windowManager.mutex.Release();
	}
}

void WindowManager::Initialise() {
	windowManager.windowsToCloseEvent.autoReset = true;
	KThreadCreate("CloseWindows", _CloseWindows);
	mutex.Acquire();
	ArrayInitialise(windows, K_FIXED);
	ArrayInitialise(embeddedWindows, K_FIXED);
	MoveCursor(graphics.width / 2, graphics.height / 2);
	GraphicsUpdateScreen();
	initialised = true;
	windowFrameBitmap.Resize((int) (39 * UI_SCALE / 100) * 4, 72 * UI_SCALE / 100);
	mutex.Release();
}

void *WindowManager::CreateEmbeddedWindow(Process *process, void *apiWindow) {
	EmbeddedWindow *window = (EmbeddedWindow *) EsHeapAllocate(sizeof(EmbeddedWindow), true, K_PAGED);
	if (!window) return nullptr;

	if (!ArrayAdd(embeddedWindows, window)) {
		EsHeapFree(window, sizeof(EmbeddedWindow), K_PAGED);
		return nullptr;
	}

	window->apiWindow = apiWindow;
	window->owner = process;
	window->handles = 2;  // One handle for the window manager, and one handle for the calling process.

	mutex.Acquire();
	window->id = ++currentWindowID;
	mutex.Release();

	OpenHandleToObject(window->owner, KERNEL_OBJECT_PROCESS);

	return window;
}

void *WindowManager::CreateWindow(Process *process, void *apiWindow, EsWindowStyle style) {
	mutex.Acquire();
	EsDefer(mutex.Release());

	// Allocate and initialise the window object.

	Window *window = (Window *) EsHeapAllocate(sizeof(Window), true, K_PAGED);
	if (!window) return nullptr;

	window->style = style;
	window->apiWindow = apiWindow;
	window->alpha = 0xFF;
	window->position = ES_MAKE_POINT(-8, -8);
	window->owner = process;
	window->handles = 2; // One handle for the window manager, and one handle for the calling process.
	window->hidden = true;
	window->id = ++currentWindowID;
	window->material = BLEND_WINDOW_MATERIAL_GLASS;

	// Insert the window into the window array.

	uintptr_t insertionPoint = GetActivationZIndex();

	if (!ArrayInsert(windows, window, insertionPoint)) {
		EsHeapFree(window->surface.bits, 0, K_PAGED);
		EsHeapFree(window, sizeof(Window), K_PAGED);
		return nullptr;
	}

	// Get a handle to the owner process.

	OpenHandleToObject(window->owner, KERNEL_OBJECT_PROCESS);

	MoveCursor(0, 0);
	return window;
}

bool Window::Move(EsRectangle rectangle, uint32_t flags) {
	windowManager.mutex.AssertLocked();

	if (closed) {
		return false;
	}

	bool result = true;

	isMaximised = flags & ES_MOVE_WINDOW_MAXIMISED;
	alwaysOnTop = flags & ES_MOVE_WINDOW_ALWAYS_ON_TOP;

	// TS("Move window\n");

	if (flags & ES_MOVE_WINDOW_ADJUST_TO_FIT_SCREEN) {
		if (rectangle.r > windowManager.workArea.r) {
			rectangle.l -= rectangle.r - windowManager.workArea.r;
			rectangle.r -= rectangle.r - windowManager.workArea.r;
		}

		if (rectangle.b > windowManager.workArea.b) {
			rectangle.t -= rectangle.b - windowManager.workArea.b;
			rectangle.b -= rectangle.b - windowManager.workArea.b;
		}

		if (rectangle.l < windowManager.workArea.l) {
			rectangle.r -= rectangle.l - windowManager.workArea.l;
			rectangle.l = windowManager.workArea.l;
		}

		if (rectangle.t < 0) {
			rectangle.b -= rectangle.t - windowManager.workArea.t;
			rectangle.t = windowManager.workArea.t;
		}
	}

	size_t newWidth = rectangle.r - rectangle.l;
	size_t newHeight = rectangle.b - rectangle.t;

	if (newWidth < 4 || newHeight < 4 
			|| rectangle.l > rectangle.r 
			|| rectangle.t > rectangle.b
			|| newWidth > graphics.width * 2
			|| newHeight > graphics.height * 2) {
		return false;
	}

	if (!hidden) {
		// TS("Clear previous image\n");
		windowManager.Redraw(ES_MAKE_POINT(position.x, position.y), width, height, this);
	}

	hidden = false;
	position = ES_MAKE_POINT(rectangle.l, rectangle.t);
	bool changedSize = width != newWidth || height != newHeight;
	width = newWidth, height = newHeight;

	if (changedSize) {
		if (style == ES_WINDOW_CONTAINER && embed) {
			surface.Resize(width - WINDOW_INSET * 2, CONTAINER_TAB_BAND_HEIGHT);
			embed->surface.Resize(width - WINDOW_INSET * 2, height - WINDOW_INSET * 2 - CONTAINER_TAB_BAND_HEIGHT, embed->resizeClearColor);
		} else {
			surface.Resize(width, height);
		}

		EsMessage message;
		EsMemoryZero(&message, sizeof(EsMessage));
		message.type = ES_MSG_WINDOW_RESIZED;
		message.windowResized.content = ES_MAKE_RECTANGLE(0, width, 0, height);
		SendMessageToWindow(this, message);
	}

	if (flags & ES_MOVE_WINDOW_AT_BOTTOM) {
		windowManager.ChangeWindowDepth(this, false, 0);
	}

	{
		// TS("Redraw region\n");
		windowManager.Redraw(position, width, height, nullptr);
	}

	return result;
}

void EmbeddedWindow::Destroy() {
	KernelLog(LOG_VERBOSE, "Window Manager", "destroy embedded window", "EmbeddedWindow::Destroy - Destroying embedded window.\n");
	EsHeapFree(this, sizeof(EmbeddedWindow), K_PAGED);
}

void EmbeddedWindow::Close() {
	windowManager.mutex.AssertLocked();

	EsMessage message;
	EsMemoryZero(&message, sizeof(EsMessage));
	message.type = ES_MSG_WINDOW_DESTROYED;
	owner->messageQueue.SendMessage(apiWindow, message); 

	message.type = ES_MSG_EMBEDDED_WINDOW_DESTROYED;
	message.windowMetadata.id = id;
	desktopProcess->messageQueue.SendMessage(nullptr, message); 

	if (container) {
		container->SetEmbed(nullptr);
	}

	EsHeapFree(surface.bits, 0, K_PAGED);
	surface.bits = nullptr;

	SetEmbedOwner(nullptr);
	closed = true;
}

void Window::Destroy() {
	if (!closed) {
		KernelPanic("Window::Destroy - Window %x has not been closed.\n", this);
	}

	EsHeapFree(this, sizeof(Window), K_PAGED);
}

void Window::Close() {
	windowManager.mutex.AssertLocked();
	
	SetEmbed(nullptr);

	// Send the destroy message - the last message sent to the window.
	EsMessage message;
	EsMemoryZero(&message, sizeof(EsMessage));
	message.type = ES_MSG_WINDOW_DESTROYED;
	owner->messageQueue.SendMessage(apiWindow, message); 

	hidden = true;
	solid = false;

	bool findActiveWindow = false;

	if (windowManager.pressedWindow == this) windowManager.pressedWindow = nullptr;
	if (windowManager.hoverWindow   == this) windowManager.hoverWindow = nullptr; 
	if (windowManager.activeWindow  == this) windowManager.activeWindow = nullptr, findActiveWindow = true;

	windowManager.Redraw(ES_MAKE_POINT(position.x, position.y), width, height);

	if (findActiveWindow) {
		windowManager.ActivateWindow(windowManager.FindWindowToActivate());
	}

	windowManager.MoveCursor(0, 0);
	GraphicsUpdateScreen();

	EsHeapFree(surface.bits, 0, K_PAGED);
	CloseHandleToObject(owner, KERNEL_OBJECT_PROCESS);

	surface.bits = nullptr;
	owner = nullptr;
	__sync_synchronize();
	closed = true;
}

void DrawTransparentWindow(Window *window, EsRectangle rectangle, bool addToModifiedRegion) {
	// TS("Draw transparent window %x within %R\n", window, rectangle);

	EsRectangleClip(rectangle, ES_MAKE_RECTANGLE(0, window->width, 0, window->height), &rectangle);
	EsRectangle r; 
	uint8_t alpha = window->alpha;

	Surface *surface = &window->surface;

	if (window->style == ES_WINDOW_CONTAINER) {
		EsRectangle clip = Translate(rectangle, window->position.x, window->position.y);

		if (EsRectangleClip(clip, ES_MAKE_RECTANGLE(0, graphics.frameBuffer.width, 0, graphics.frameBuffer.height), &clip)) {
			int blurSize = WINDOW_INSET - BORDER_THICKNESS;

			graphics.frameBuffer.Blur(ES_MAKE_RECTANGLE(window->position.x + blurSize, window->position.x + window->width - blurSize,
						window->position.y + (13 * UI_SCALE / 100), window->position.y + (52 * UI_SCALE / 100)), clip);

			if (!window->isMaximised) {
				graphics.frameBuffer.Blur(ES_MAKE_RECTANGLE(window->position.x + blurSize, window->position.x + WINDOW_INSET,
							window->position.y + (52 * UI_SCALE / 100), window->position.y + window->height - blurSize - BORDER_THICKNESS), clip);
				graphics.frameBuffer.Blur(ES_MAKE_RECTANGLE(window->position.x + window->width - WINDOW_INSET, window->position.x + window->width - blurSize,
							window->position.y + (52 * UI_SCALE / 100), window->position.y + window->height - blurSize - BORDER_THICKNESS), clip);
				graphics.frameBuffer.Blur(ES_MAKE_RECTANGLE(window->position.x + blurSize, window->position.x + window->width - blurSize,
							window->position.y + window->height - WINDOW_INSET, window->position.y + window->height - blurSize), clip);
			}

			int width = 39 * UI_SCALE / 100;
			int height = 72 * UI_SCALE / 100;

			EsRectangle source = ES_MAKE_RECTANGLE(0, width, 0, height); 
			EsRectangle border = ES_MAKE_RECTANGLE(19 * UI_SCALE / 100, 20 * UI_SCALE / 100, 52 * UI_SCALE / 100, 53 * UI_SCALE / 100);

			EsRectangle destination = ES_MAKE_RECTANGLE(window->position.x, window->position.x + window->width, 
					window->position.y, window->position.y + window->height);

			if (window->isMaximised) source.l += width, source.r += width, border.l += width, border.r += width;
			if (windowManager.activeWindow != window) source.l += width * 2, source.r += width * 2, border.l += width * 2, border.r += width * 2;

			clip = EsRectangleIntersection(clip, destination);
			source = EsRectangleBounding(EsRectangleAdd(source, EsRectangleSubtract(clip, destination)), border);
			graphics.frameBuffer.DrawWindowFrame(windowManager.windowFrameBitmap, clip, source, border);
		}

		if (EsRectangleClip(Translate(rectangle, -WINDOW_INSET, -WINDOW_INSET), 
				ES_MAKE_RECTANGLE(0, window->width - WINDOW_INSET * 2, 0, CONTAINER_TAB_BAND_HEIGHT), &r)) {
			EsPoint point = ES_MAKE_POINT(window->position.x + r.l + WINDOW_INSET, window->position.y + r.t + WINDOW_INSET);
			graphics.frameBuffer.BlendWindow(*surface, point, r, BLEND_WINDOW_MATERIAL_NONE, alpha, ES_MAKE_RECTANGLE_ALL(0));
		}

		if (window->embed && EsRectangleClip(Translate(rectangle, -WINDOW_INSET, -(WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT)), 
					ES_MAKE_RECTANGLE(0, window->width - WINDOW_INSET * 2, 0, window->height - WINDOW_INSET * 2 - CONTAINER_TAB_BAND_HEIGHT), &r)) {
			Surface *embedSurface = &window->embed->surface;
			EsRectangleClip(r, ES_MAKE_RECTANGLE(0, window->embed->surface.width, 0, window->embed->surface.height), &r);
			EsPoint point = ES_MAKE_POINT(window->position.x + r.l + WINDOW_INSET, window->position.y + r.t + WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT);

			if (alpha == 0xFF) {
				graphics.frameBuffer.Copy(*embedSurface, point, r, addToModifiedRegion);
			} else {
				graphics.frameBuffer.BlendWindow(*embedSurface, point, r, BLEND_WINDOW_MATERIAL_NONE, alpha, ES_MAKE_RECTANGLE_ALL(0));
			}
		}
	} else {
		EsPoint point = ES_MAKE_POINT(window->position.x + rectangle.l, window->position.y + rectangle.t);
		EsRectangle blurRegion = Translate(window->blurBounds, window->position);
		EsRectangleClip(blurRegion, Translate(rectangle, window->position), &blurRegion);

		if (window->opaqueBounds.l <= rectangle.l && window->opaqueBounds.r >= rectangle.r 
				&& window->opaqueBounds.t <= rectangle.t && window->opaqueBounds.b >= rectangle.b) {
			graphics.frameBuffer.Copy(*surface, point, rectangle, addToModifiedRegion);
		} else {
			graphics.frameBuffer.BlendWindow(*surface, point, rectangle, window->material, alpha, blurRegion);
		}
	}
}

void WindowManager::Redraw(EsPoint position, int width, int height, Window *except, int startingAt, bool addToModifiedRegion) {
	// TS("Window manager redraw at [%d, %d] with size [%d, %d]\n", position.x, position.y, width, height);

	mutex.AssertLocked();

	if (!width || !height) return;
	if (scheduler.shutdown) return;

	drawing = true;

	for (int index = startingAt; index < (int) ArrayLength(windows); index++) {
		Window *window = windows[index];

		if (!window->IsVisible() || window == except) {
			continue;
		}

		if (position.x >= window->position.x + window->opaqueBounds.l
				&& position.x + width <= window->position.x + window->opaqueBounds.r
				&& position.y >= window->position.y + window->opaqueBounds.t
				&& position.y + height <= window->position.y + window->opaqueBounds.b) {
			startingAt = index;
		}
	}

	for (int index = startingAt; index < (int) ArrayLength(windows); index++) {
		Window *window = windows[index];

		int windowWidth = window->width, windowHeight = window->height, windowX = window->position.x, windowY = window->position.y;

		if (position.x > windowX + (int) windowWidth
				|| position.x + width < windowX
				|| position.y > windowY + (int) windowHeight
				|| position.y + height < windowY
				|| !window->IsVisible() || window == except) {
			continue;
		}

		// Work out the region of the window we need to repaint.
		EsRectangle rectangle = {0, (int) windowWidth, 0, (int) windowHeight};
		if (position.x > windowX) rectangle.l = position.x - windowX;
		if (position.y > windowY) rectangle.t = position.y - windowY;
		if (position.x + width < windowX + (int) windowWidth) rectangle.r = position.x - windowX + width;
		if (position.y + height < windowY + (int) windowHeight) rectangle.b = position.y - windowY + height;

		// Clip to the borders of the screen.
		if (rectangle.l + windowX < 0) rectangle.l = -windowX;
		if (rectangle.t + windowY < 0) rectangle.t = -windowY;
		if (rectangle.r + windowX >= (int32_t) graphics.frameBuffer.width) rectangle.r = graphics.frameBuffer.width - windowX;
		if (rectangle.b + windowY >= (int32_t) graphics.frameBuffer.height) rectangle.b = graphics.frameBuffer.height - windowY;

		DrawTransparentWindow(window, rectangle, addToModifiedRegion);
	}

	drawing = false;
}

bool Window::UpdateDirect(K_USER_BUFFER void *bits, uintptr_t stride, EsRectangle region) {
	windowManager.mutex.AssertLocked();

	intptr_t z = windowManager.FindWindowDepth(this);

	if (z == -1) {
		return false;
	}

	if (windowManager.changedCursorImage) {
		// The entire cursor needs to be redrawn about its image changes,
		// which might not happen with a direct update.
		return false;
	}

	if (region.l + position.x < 0) {
		bits = (K_USER_BUFFER uint8_t *) bits + 4 * (-position.x - region.l);
		region.l = -position.x;
	}

	if (region.t + position.y < 0) {
		bits = (K_USER_BUFFER uint8_t *) bits + stride * (-position.y - region.t);
		region.t = -position.y;
	}

	if ((uint32_t) (region.r + position.x) > graphics.width) {
		region.r = graphics.width - position.x;
	}

	if ((uint32_t) (region.b + position.y) > graphics.height) {
		region.b = graphics.height - position.y;
	}

	// If the update region is completely obscured, then we don't need to update.

	for (uintptr_t i = z + 1; i < ArrayLength(windowManager.windows); i++) {
		Window *other = windowManager.windows[i];
		EsRectangle otherBounds = Translate(other->opaqueBounds, other->position);

		if (region.l + position.x > otherBounds.l && region.r + position.x < otherBounds.r 
				&& region.t + position.y > otherBounds.t && region.b + position.y < otherBounds.b
				&& other->alpha == 0xFF && other->IsVisible()) {
			return true;
		} 
	}

	// If the update region isn't opaque, we cannot do a direct update.

	if (region.l < opaqueBounds.l || region.r > opaqueBounds.r || region.t < opaqueBounds.t || region.b > opaqueBounds.b
			|| alpha != 0xFF) {
		return false;
	}

	// If any window overlaps the update region, we cannot do a direct update.

	EsRectangle thisBounds = ES_MAKE_RECTANGLE(position.x, position.x + width, position.y, position.y + height);

	for (uintptr_t i = z + 1; i < ArrayLength(windowManager.windows); i++) {
		Window *other = windowManager.windows[i];
		EsRectangle otherBounds = ES_MAKE_RECTANGLE(other->position.x, other->position.x + other->width,
				other->position.y, other->position.y + other->height);

		if (EsRectangleClip(thisBounds, otherBounds, nullptr)) {
			return false;
		} 
	}

	region = Translate(region, position);

	// Write the updated bits directly to the frame buffer!
	GraphicsUpdateScreen(bits, &region, stride);
	return true;
}

void Window::Update(EsRectangle *_region, bool skipUpdateScreen) {
	windowManager.mutex.AssertLocked();

	intptr_t z = windowManager.FindWindowDepth(this);

	if (z == -1) {
		return;
	}

	// TS("Update window %x within %R\n", this, _region ? *_region : ES_MAKE_RECTANGLE_ALL(0));

	EsRectangle region = _region ? *_region : ES_MAKE_RECTANGLE(0, width, 0, height);

	// Is this updated region completely obscured?
	for (uintptr_t i = z + 1; i < ArrayLength(windowManager.windows); i++) {
		Window *other = windowManager.windows[i];
		EsRectangle otherBounds = Translate(other->opaqueBounds, other->position);

		if (region.l + position.x > otherBounds.l && region.r + position.x < otherBounds.r 
				&& region.t + position.y > otherBounds.t && region.b + position.y < otherBounds.b
				&& other->alpha == 0xFF && other->IsVisible()) {
			return;
		} 
	}

	EsRectangle r;
	// EsPrint("Update window width region %d/%d/%d/%d\n", region.left, region.right, region.top, region.bottom);

	if (alpha == 0xFF) {
		EsRectangle translucentBorder = opaqueBounds;

		// Draw the opaque region, then everything above it.
		EsRectangleClip(region, ES_MAKE_RECTANGLE(translucentBorder.l, translucentBorder.r, translucentBorder.t, translucentBorder.b), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, z, !skipUpdateScreen);

		// Draw the transparent regions after everything below them, then everything above them.
		EsRectangleClip(region, ES_MAKE_RECTANGLE(0, translucentBorder.l, 0, height), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, 0, !skipUpdateScreen);
		EsRectangleClip(region, ES_MAKE_RECTANGLE(translucentBorder.r, width, 0, height), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, 0, !skipUpdateScreen);
		EsRectangleClip(region, ES_MAKE_RECTANGLE(translucentBorder.l, translucentBorder.r, 0, translucentBorder.t), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, 0, !skipUpdateScreen);
		EsRectangleClip(region, ES_MAKE_RECTANGLE(translucentBorder.l, translucentBorder.r, translucentBorder.b, height), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, 0, !skipUpdateScreen);
	} else {
		// The whole window is translucent; draw it.
		EsRectangleClip(region, ES_MAKE_RECTANGLE(0, width, 0, height), &r);
		windowManager.Redraw(ES_MAKE_POINT(position.x + r.l, position.y + r.t), r.r - r.l, r.b - r.t, nullptr, 0, !skipUpdateScreen);
	}

	if (!skipUpdateScreen) {
		// TS("Update screen\n");
		GraphicsUpdateScreen();
	}
}

void EmbeddedWindow::SetEmbedOwner(Process *process) {
	windowManager.mutex.AssertLocked();

	apiWindow = nullptr;

	if (process) {
		OpenHandleToObject(process, KERNEL_OBJECT_PROCESS);
	}

	if (owner) {
		CloseHandleToObject(owner, KERNEL_OBJECT_PROCESS);
	}

	owner = process;
}

void Window::SetEmbed(EmbeddedWindow *newEmbed) {
	windowManager.mutex.AssertLocked();

	if (newEmbed && (newEmbed->container || newEmbed->closed)) {
		return;
	}

	if (newEmbed == embed) {
		return;
	}

	if (newEmbed) {
		newEmbed->container = this;
		EsMessage message;
		EsMemoryZero(&message, sizeof(EsMessage));
		message.type = ES_MSG_WINDOW_RESIZED;
		int embedWidth = width - WINDOW_INSET * 2;
		int embedHeight = height - WINDOW_INSET * 2 - CONTAINER_TAB_BAND_HEIGHT;
		message.windowResized.content = ES_MAKE_RECTANGLE(0, embedWidth, 0, embedHeight);
		newEmbed->surface.Resize(embedWidth, embedHeight, newEmbed->resizeClearColor);
		newEmbed->owner->messageQueue.SendMessage(newEmbed->apiWindow, message);
	}

	if (embed) {
		if (embed->closed) {
			KernelPanic("Window::SetEmbed - Previous embed %x was closed.\n");
		}

		embed->container = nullptr;

		EsMessage message;
		EsMemoryZero(&message, sizeof(EsMessage));
		message.type = ES_MSG_WINDOW_RESIZED;
		message.windowResized.content = ES_MAKE_RECTANGLE(0, 0, 0, 0);
		message.windowResized.hidden = true;
		embed->surface.Resize(0, 0);
		embed->owner->messageQueue.SendMessage(embed->apiWindow, message);
	}

	embed = newEmbed;
}

void WindowManager::StartEyedrop(uintptr_t object, Window *avoid, uint32_t cancelColor) {
	mutex.Acquire();

	if (!eyedropping) {
		eyedropObject = object;
		eyedropping = true;
		eyedropAvoidID = avoid->id;
		eyedropCancelColor = cancelColor;
		Redraw(avoid->position, avoid->width, avoid->height);

		if (hoverWindow) {
			EsMessage message;
			EsMemoryZero(&message, sizeof(EsMessage));
			message.type = ES_MSG_MOUSE_EXIT;
			SendMessageToWindow(hoverWindow, message);
		}

		hoverWindow = pressedWindow = nullptr;

		eyedropProcess = GetCurrentThread()->process;
		OpenHandleToObject(eyedropProcess, KERNEL_OBJECT_PROCESS);
	}

	GraphicsUpdateScreen();
	mutex.Release();
}

void BroadcastMessage(EsMessage &message) {
	windowManager.mutex.Acquire();

	for (uintptr_t i = 0; i < ArrayLength(windowManager.windows); i++) {
		Window *window = windowManager.windows[i];
		SendMessageToWindow(window, message);
	}

	windowManager.mutex.Release();
}

void KCursorUpdate(int xMovement, int yMovement, unsigned buttons) {
	windowManager.UpdateCursor(xMovement, yMovement, buttons);
}

void KKeyPress(unsigned scancode) {
	windowManager.PressKey(scancode);
}

void KKeyboardUpdate(uint16_t *keysDown, size_t keysDownCount) {
	// TODO Key repeat.

	static uint16_t previousKeysDown[32] = {};
	static size_t previousKeysDownCount = 0;
	static KMutex mutex = {};

	mutex.Acquire();
	EsDefer(mutex.Release());

	if (keysDownCount > 32) {
		keysDownCount = 32;
	}

	for (uintptr_t i = 0; i < keysDownCount; i++) {
		bool found = false;

		for (uintptr_t j = 0; j < previousKeysDownCount; j++) {
			if (keysDown[i] == previousKeysDown[j]) {
				found = true;
				break;
			}
		}

		if (!found && keysDown[i]) {
			if (keysDown[i] == ES_SCANCODE_PAUSE) {
				KDebugKeyPressed(); // TODO Doesn't work if scheduler not functioning correctly.
			}

			KKeyPress(keysDown[i] | K_SCANCODE_KEY_PRESSED);
		}
	}

	for (uintptr_t i = 0; i < previousKeysDownCount; i++) {
		bool found = false;

		for (uintptr_t j = 0; j < keysDownCount; j++) {
			if (keysDown[j] == previousKeysDown[i]) {
				found = true;
				break;
			}
		}

		if (!found) {
			KKeyPress(previousKeysDown[i] | K_SCANCODE_KEY_RELEASED);
		}
	}

	previousKeysDownCount = keysDownCount;
	EsMemoryCopy(previousKeysDown, keysDown, sizeof(uint16_t) * keysDownCount);
}

uint64_t KGameControllerConnect() {
	windowManager.gameControllersMutex.Acquire();

	uint64_t id = ++windowManager.gameControllerID;

	if (windowManager.gameControllerCount != ES_GAME_CONTROLLER_MAX_COUNT) {
		windowManager.gameControllers[windowManager.gameControllerCount++].id = id;
	} else {
		id = 0;
	}

	windowManager.gameControllersMutex.Release();

	return id;
}

void KGameControllerDisconnect(uint64_t id) {
	windowManager.gameControllersMutex.Acquire();
	
	for (uintptr_t i = 0; i < windowManager.gameControllerCount; i++) {
		if (windowManager.gameControllers[i].id == id) {
			EsMemoryMove(windowManager.gameControllers + i + 1, 
					windowManager.gameControllers + windowManager.gameControllerCount, 
					-sizeof(EsGameControllerState), false);
			windowManager.gameControllerCount--;
			break;
		}
	}

	windowManager.gameControllersMutex.Release();
}

void KGameControllerUpdateState(EsGameControllerState *state) {
	windowManager.gameControllersMutex.Acquire();
	
	for (uintptr_t i = 0; i < windowManager.gameControllerCount; i++) {
		if (windowManager.gameControllers[i].id == state->id) {
			windowManager.gameControllers[i] = *state;
#if 0
			EsPrint("game controller %d: buttons %x analog %d %d %d %d %d %d dpad %d\n",
				state->id, state->buttons, state->analog[0].x, state->analog[0].y, state->analog[0].z, 
				state->analog[1].x, state->analog[1].y, state->analog[1].z, state->directionalPad);
#endif
			break;
		}
	}

	windowManager.gameControllersMutex.Release();
}

EsError Clipboard::Add(uint64_t format, const void *data, size_t dataBytes) {
	if (dataBytes > CLIPBOARD_ITEM_SIZE_LIMIT) {
		return ES_ERROR_INSUFFICIENT_RESOURCES;
	}

	ClipboardItem item = {};
	item.buffer = MakeConstantBuffer(data, dataBytes);
	item.format = format;

	if (!item.buffer) {
		return ES_ERROR_INSUFFICIENT_RESOURCES;
	}

	mutex.Acquire();

	if (items[CLIPBOARD_ITEM_COUNT - 1].buffer) {
		CloseHandleToObject(items[CLIPBOARD_ITEM_COUNT - 1].buffer, KERNEL_OBJECT_CONSTANT_BUFFER);
	}

	EsMemoryMove(items, items + CLIPBOARD_ITEM_COUNT - 1, sizeof(ClipboardItem), false);
	items[0] = item;

	mutex.Release();

	if (this == &primaryClipboard) {
		windowManager.mutex.Acquire();

		if (windowManager.activeWindow) {
			EsMessage m;
			EsMemoryZero(&m, sizeof(EsMessage));
			m.type = ES_MSG_PRIMARY_CLIPBOARD_UPDATED;
			SendMessageToWindow(windowManager.activeWindow, m);
		}

		windowManager.mutex.Release();
	}

	return ES_SUCCESS;
}

bool Clipboard::Has(uint64_t format) {
	mutex.Acquire();
	bool result = items[0].format == format;
	mutex.Release();
	return result;
}

EsHandle Clipboard::Read(uint64_t format, K_USER_BUFFER size_t *_bytes, Process *process) {
	ConstantBuffer *buffer = nullptr;
	mutex.Acquire();

	if (items[0].format == format) {
		buffer = items[0].buffer;
		OpenHandleToObject(buffer, KERNEL_OBJECT_CONSTANT_BUFFER);
	}

	mutex.Release();

	if (buffer) {
		*_bytes = buffer->bytes;
		return process->handleTable.OpenHandle(buffer, 0, KERNEL_OBJECT_CONSTANT_BUFFER);
	} else {
		return ES_INVALID_HANDLE;
	}
}

#endif
