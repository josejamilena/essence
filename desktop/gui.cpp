// TODO Styling features:
// 	- Specifying aspect ratio of element.
// 	- Animation parts (list of keyframes).
// 	- Ripple animations.
// 	- Exiting animations.
// 	- Morph from/to entrance/exit animations.
// TODO Close menus within menus (bug).
// TODO Keyboard navigation - menus; escape to restore default focus.
// TODO Middle click panning.
// TODO Scrollbar middle click and zooming; scroll wheel.
// TODO Textboxes: date/time overlays, keyboard shortcut overlay, custom overlays.
// TODO Breadcrumb bar overflow menu; keep hover after recreating UI.
// TODO Textbox embedded objects.
// TODO Closing windows in menu/access key mode.
// TODO Space partitioning in elements with lots of children.

// Behaviour of activation clicks. --> Only ignore activation clicks from menus.
// Behaviour of the scroll wheel with regards to focused/hovered elements --> Scroll the hovered element only.

#define WINDOW_INSET 			((int) esSystemConstants[ES_SYSTEM_CONSTANT_WINDOW_INSET])
#define CONTAINER_TAB_BAND_HEIGHT 	((int) esSystemConstants[ES_SYSTEM_CONSTANT_CONTAINER_TAB_BAND_HEIGHT])
#define BORDER_THICKNESS 		((int) esSystemConstants[ES_SYSTEM_CONSTANT_BORDER_THICKNESS])

// #define TRACE_LAYOUT

#ifdef SIMPLE_GRAPHICS
#define DISABLE_ALL_ANIMATIONS
#endif

#ifdef DISABLE_ALL_ANIMATIONS
#define ANIMATION_SPEED (0)
#else
#define ANIMATION_SPEED (1000)
#endif

struct AccessKeyEntry {
	char character;
	int number;
	EsElement *element;
	int x, y;
};

struct {
	// Animations.
	EsHandle animationSleepTimer;
	bool animationSleep;
	DS_ARRAY(EsElement *) animatingElements;

	// Input.
	bool draggingStarted, mouseButtonDown;
	bool holdingShift, holdingCtrl, holdingAlt, lastClickWasActivation;
	bool inRightClickDrag;
	int lastClickX, lastClickY, lastClickChainCount, lastClickButton, resizeType;

	// Menus.
	bool menuMode;

	// Access keys.
	bool accessKeyMode, unhandledAltPress;

	struct {
		AccessKeyEntry *entries;
		int numbers[26];
		struct UIStyle *hintStyle;
		EsWindow *window;
		char typedCharacter;
	} accessKeys;

	// Misc.
	DS_ARRAY(EsWindow *) allWindows;
	DS_MAP(uint32_t, const char *) keyboardShortcutNames;
	EsCursorStyle resizeCursor;
	bool resizing;
	EsElement *insertAfter;
} gui;

struct TableCell {
	uint16_t from[2], to[2];
};

// Miscellanous forward declarations.
EsElement *WindowGetMainPanel(EsWindow *window);
void AccessKeyHintsShow(EsPainter *painter);

void InspectorSetup(EsWindow *window);
void InspectorNotifyElementCreated(EsElement *element);
void InspectorNotifyElementDestroyed(EsElement *element);
void InspectorNotifyElementMoved(EsElement *element, EsRectangle takenBounds, EsRectangle givenBounds);
void InspectorNotifyElementPainted(EsElement *element, EsPainter *painter);
void InspectorNotifyElementContentChanged(EsElement *element);

#define UI_STATE_RELAYOUT 		(1 <<  2)
#define UI_STATE_RELAYOUT_CHILD		(1 <<  3)
#define UI_STATE_DESTROYING		(1 <<  4)
#define UI_STATE_DESTROYING_CHILD	(1 <<  5)

#define UI_STATE_HOVERED		(1 <<  6)
#define UI_STATE_PRESSED		(1 <<  7)
#define UI_STATE_STRONG_PRESSED		(1 <<  8)
#define UI_STATE_FOCUS_WITHIN		(1 <<  9)
#define UI_STATE_FOCUSED		(1 << 10)
#define UI_STATE_LOST_STRONG_FOCUS	(1 << 11)
#define UI_STATE_MENU_SOURCE		(1 << 12)

#define UI_STATE_ANIMATING		(1 << 13)
#define UI_STATE_ENTERED		(1 << 14)
#define UI_STATE_EXITING		(1 << 15)
#define UI_STATE_BLOCK_INTERACTION	(1 << 16)

#define UI_STATE_TEMP			(1 << 17)
#define UI_STATE_Z_STACK		(1 << 18)
#define UI_STATE_COMMAND_BUTTON		(1 << 19)
#define UI_STATE_USE_MEASUREMENT_CACHE	(1 << 20)

struct EsElement : EsElementPublic {
	EsUICallbackFunction classCallback;
	EsElement *parent;
	DS_ARRAY(EsElement *) children; 
	uint32_t state;

	UIStyle *currentStyle; 
	UIStyleKey currentStyleKey;
	ThemeAnimation animation; 
	uint64_t lastTimeStamp;
	uint16_t customStyleState; // ORed to the style state in RefreshStyle.
	uint16_t previousStyleState; // Set by RefreshStyleState.
	EsPaintTarget *exitTransitionFrame; // Created in ::Destroy.

	int width, height, offsetX, offsetY;
	uint8_t internalOffsetLeft, internalOffsetRight, internalOffsetTop, internalOffsetBottom;
	TableCell tableCell;

	void Destroy(bool manual = true);
	void PrintTree(int depth = 0);

	inline size_t GetChildCount() {
		return arrlen(children);
	}

	inline EsElement *GetChild(uintptr_t index) {
		EsAssert(index < arrlenu(children)); // Invalid child index.
		return children[index];
	}

	inline EsElement *GetChildByZ(uintptr_t index) {
		EsMessage m = { ES_MSG_Z_ORDER };
		m.zOrder.index = index, m.zOrder.child = GetChild(index);
		if (m.zOrder.child->flags & ES_ELEMENT_NON_CLIENT) return m.zOrder.child;
		if (ES_REJECTED == EsMessageSend(this, &m)) return nullptr;
		EsAssert(!m.zOrder.child || m.zOrder.child->parent == this); // Child obtained from ES_MSG_Z_ORDER had different parent.
		return m.zOrder.child;
	}

	void BringToFront() {
		for (uintptr_t i = 0; i < arrlenu(parent->children); i++) {
			if (parent->children[i] == this) {
				InspectorNotifyElementDestroyed(this);
				EsElement *swap = arrlast(parent->children);
				arrlast(parent->children) = this;
				parent->children[i] = swap;
				InspectorNotifyElementCreated(this);
				return;
			}
		}

		EsAssert(false);
	}

	bool IsFocusable() {
		if ((~flags & ES_ELEMENT_FOCUSABLE) || (flags & ES_ELEMENT_DISABLED) || (state & UI_STATE_DESTROYING)) {
			return false;
		}

		EsElement *element = this;

		while (element) {
			if ((element->flags & ES_ELEMENT_BLOCK_FOCUS) || (element->state & UI_STATE_BLOCK_INTERACTION) || (element->flags & ES_ELEMENT_HIDDEN)) {
				return false;
			}

			element = element->parent;
		}

		return true;
	}

	bool RefreshStyleState(); // Returns true if any observed bits have changed.
	void RefreshStyle(UIStyleKey *oldStyleKey = nullptr, bool alreadyRefreshStyleState = false, bool force = false);
	bool StartAnimating();
	void SetStylePart(const EsStyle *stylePart, bool refreshIfChanged = true);
	EsCursorStyle GetCursor();

	inline void MaybeRefreshStyle() {
		if (RefreshStyleState()) {
			RefreshStyle(nullptr, true);
		}
	}

	EsRectangle GetWindowBounds(bool client = true);
	EsRectangle GetScreenBounds(bool client = true);

	inline EsRectangle GetBounds() { 
		return ES_MAKE_RECTANGLE(0, width - internalOffsetLeft - internalOffsetRight, 0, height - internalOffsetTop - internalOffsetBottom); 
	}

#define PAINT_SHADOW        (1 << 0) // Paint the shadow layers.
#define PAINT_NO_OFFSET     (1 << 1) // Don't add the element's offset to the painter.
#define PAINT_NO_TRANSITION (1 << 2) // Ignore entrance/exit transitions.
	void InternalPaint(EsPainter *painter, int flags);

	void InternalMove(int _width, int _height, int _offsetX, int _offsetY); // Non-client offset.
	void InternalCalculateRepaintRegion(int x, int y, bool forwards, bool overlappedBySibling = false);
	bool InternalDestroy(); // Called after processing each message, to destroy any elements marked by ::Destroy.

	int GetWidth(int height);
	int GetHeight(int width);

	inline void InternalMove(EsRectangle bounds) { 
		InternalMove(bounds.r - bounds.l, bounds.b - bounds.t, bounds.l, bounds.t);
	}

	void Repaint(bool all, EsRectangle region = ES_MAKE_RECTANGLE_ALL(0) /* client coordinates */);

	void Initialise(EsElement *_parent, uint64_t _flags, EsUICallbackFunction _classCallback, const EsStyle *style);
};

struct EsElementGroup {
	DS_ARRAY(EsElement *) elements;
};

struct MeasurementCache {
	int width0, width2, width2Height;
	int height0, height2, height2Width;

	bool Get(EsMessage *message, uint32_t *state) {
		if (~(*state) & UI_STATE_USE_MEASUREMENT_CACHE) {
			width0 = 0, width2 = 0, width2Height = 0;
			height0 = 0, height2 = 0, height2Width = 0;
			*state |= UI_STATE_USE_MEASUREMENT_CACHE;
		}

		if (message->type == ES_MSG_GET_WIDTH) {
			if (message->measure.height && message->measure.height == width2Height) {
				message->measure.width = width2;
			} else if (!message->measure.height && width0) {
				message->measure.width = width0;
			} else {
				return false;
			}
		} else {
			if (message->measure.width && message->measure.width == height2Width) {
				message->measure.height = height2;
			} else if (!message->measure.width && height0) {
				message->measure.height = height0;
			} else {
				return false;
			}
		}

		return true;
	}

	void Store(EsMessage *message) {
		if (message->type == ES_MSG_GET_WIDTH) {
			if (message->measure.height) {
				width2 = message->measure.width;
				width2Height = message->measure.height;
			} else {
				width0 = message->measure.width;
			}
		} else {
			if (message->measure.width) {
				height2Width = message->measure.width;
				height2 = message->measure.height;
			} else {
				height0 = message->measure.height;
			}
		}
	}
};

struct EsButton : EsElement {
	char *label;
	size_t labelBytes;
	EsGeneric menuItemContext;
	uint32_t iconID;
	MeasurementCache measurementCache;
	EsCommand *command;
	EsCommandCallbackFunction onCommand;
	EsElement *checkBuddy;
};

struct ScrollPane {
	EsElement *parent, *pad;
	EsScrollbar *barX, *barY;
	double x, y;
	int64_t xLimit, yLimit;
	int32_t fixedViewportWidth, fixedViewportHeight;
	bool dragScrolling;

#define SCROLL_MODE_NONE    (0)      // No scrolling takes place on this axis.
#define SCROLL_MODE_HIDDEN  (1)      // Scrolling takes place, but there is no visible scrollbar.
#define SCROLL_MODE_FIXED   (2)      // The scrollbar is always visible.
#define SCROLL_MODE_AUTO    (3)      // The scrollbar is only visible if the content is larger than the viewport.
	uint8_t xMode, yMode;
#define SCROLL_X_DRAG (1 << 0)
#define SCROLL_Y_DRAG (1 << 1)
	uint16_t flags;

	void Setup(EsElement *parent, uint8_t xMode, uint8_t yMode, uint16_t flags);
	void SetX(double scrollX, bool sendMovedMessage = true);
	void SetY(double scrollY, bool sendMovedMessage = true);
	void Refresh();
	void ReceivedMessage(EsMessage *message);

	// Internal.
	bool RefreshXLimit(int64_t *content);
	bool RefreshYLimit(int64_t *content);
};

struct EsPanel : EsElement {
	ScrollPane scroll;

	const EsStyle *separatorStylePart;
	bool addingSeparator;
	uint64_t separatorFlags;
	
	union {
		struct {
			uint16_t bandCount[2];
			EsPanelBand *bands[2];
			uintptr_t tableIndex;
		};
		
		struct {
			bool destroyPreviousAfterTransitionCompletes;
			uint16_t transitionType;
			uint32_t transitionTimeMcs,
				 transitionLengthMcs;
			EsElement *switchedTo, 
				  *switchedFrom;
		};
	};

	MeasurementCache measurementCache;

	int GetGapMajor() {
		return currentStyle->gapMajor;
	}

	int GetGapMinor() {
		return currentStyle->gapMinor;
	}

	EsRectangle GetInsets() {
		return currentStyle->insets;
	}

	int GetInsetWidth() {
		EsRectangle insets = GetInsets();
		return insets.l + insets.r;
	}

	int GetInsetHeight() {
		EsRectangle insets = GetInsets();
		return insets.t + insets.b;
	}
};

struct EsTextDisplay : EsElement {
	EsTextPlanProperties properties; 
	EsTextRun *textRuns;
	size_t textRunCount;
	char *contents;

	MeasurementCache measurementCache;
	EsTextPlan *plan;
	int planWidth, planHeight;
};

struct ColorPickerHost {
	struct EsElement *well;
	bool *indeterminate;
	bool hasOpacity;
};

void NewColorPicker(EsElement *parent, ColorPickerHost host, uint32_t initialColor, bool showTextbox);

void HeapDuplicate(void **pointer, const void *data, size_t bytes) {
	if (*pointer) {
		EsHeapFree(*pointer);
	}

	if (!data && !bytes) {
		*pointer = nullptr;
	} else {
		void *buffer = EsHeapAllocate(bytes, false);
		EsMemoryCopy(buffer, data, bytes);
		*pointer = buffer;
	}
}

// --------------------------------- Windows.

struct EsWindow : EsElement {
	EsHandle handle;
	uint32_t windowWidth, windowHeight;

	bool willUpdate, isSpecial, toolbarFillMode;
	bool restoreOnNextMove, resetPositionOnNextMove, receivedFirstResize, isMaximised;
	bool hovering, activated;
	bool hasDialog;

	EsElement *hovered, 
		  *pressed, 
		  *focused,
		  *inactiveFocus;

	EsButton *enterButton, 
		 *escapeButton, 
		 *defaultEnterButton;

	EsElement *dialogOverlay, *dialogPanel;
	EsWindowStyle windowStyle;
	EsRectangle beforeMaximiseBounds;

	EsRectangle updateRegion;

	EsElement *source; // Menu source.
	EsWindow *targetMenu; // The menu that keyboard events should be sent to.
};

#define RESIZE_LEFT 		(1)
#define RESIZE_RIGHT 		(2)
#define RESIZE_TOP 		(4)
#define RESIZE_BOTTOM 		(8)
#define RESIZE_TOP_LEFT 	(5)
#define RESIZE_TOP_RIGHT 	(6)
#define RESIZE_BOTTOM_LEFT 	(9)
#define RESIZE_BOTTOM_RIGHT 	(10)
#define RESIZE_MOVE		(0)

void ChangeWindowBounds(int direction, int newX, int newY, int &originalX, int &originalY, EsWindow *window) {
	EsRectangle bounds = EsWindowGetBounds(window), bounds2;
	bounds2 = bounds;

	int oldWidth = bounds.r - bounds.l;
	int oldHeight = bounds.b - bounds.t;
	bool restored = false, canSnap = true;

	if (window->restoreOnNextMove) {
		window->restoreOnNextMove = false;
		oldWidth = window->beforeMaximiseBounds.r - window->beforeMaximiseBounds.l;
		oldHeight = window->beforeMaximiseBounds.b - window->beforeMaximiseBounds.t;
		restored = true;
		// SetMaximised(window, false);
	}

	if (direction & RESIZE_LEFT)   bounds.l = newX + BORDER_THICKNESS / 2 - WINDOW_INSET;
	if (direction & RESIZE_RIGHT)  bounds.r = newX - BORDER_THICKNESS / 2 + WINDOW_INSET;
	if (direction & RESIZE_TOP)    bounds.t = newY + BORDER_THICKNESS / 2 - WINDOW_INSET;
	if (direction & RESIZE_BOTTOM) bounds.b = newY - BORDER_THICKNESS / 2 + WINDOW_INSET;

	EsRectangle screen;
	EsSyscall(ES_SYSCALL_GET_SCREEN_WORK_AREA, 0, (uintptr_t) &screen, 0, 0);

	int newWidth = bounds.r - bounds.l;
	int newHeight = bounds.b - bounds.t;

	// TODO Move these into the style file.
#define WINDOW_SNAP_RANGE (6)
#define WINDOW_MINIMUM_WIDTH (224)
#define WINDOW_MINIMUM_HEIGHT (116)
#define WINDOW_RESTORE_DRAG_Y_POSITION (20)

	window->isMaximised = false;

	if (newWidth  < WINDOW_MINIMUM_WIDTH  && direction & RESIZE_LEFT)   bounds.l = bounds.r - WINDOW_MINIMUM_WIDTH;
	if (newWidth  < WINDOW_MINIMUM_WIDTH  && direction & RESIZE_RIGHT)  bounds.r = bounds.l + WINDOW_MINIMUM_WIDTH;
	if (newHeight < WINDOW_MINIMUM_HEIGHT && direction & RESIZE_TOP)    bounds.t = bounds.b - WINDOW_MINIMUM_HEIGHT;
	if (newHeight < WINDOW_MINIMUM_HEIGHT && direction & RESIZE_BOTTOM) bounds.b = bounds.t + WINDOW_MINIMUM_HEIGHT;

	if (direction == RESIZE_MOVE) {
		if (newY < screen.t + WINDOW_SNAP_RANGE && canSnap) {
			if (!window->restoreOnNextMove && !restored) window->beforeMaximiseBounds = bounds;
			window->restoreOnNextMove = true;
			window->isMaximised = true;
			bounds.t = screen.t - 16 * themeScale;
			bounds.b = screen.b + 19 * themeScale;
			bounds.l = screen.l - 19 * themeScale;
			bounds.r = screen.r + 19 * themeScale;
		} else if (newX < screen.l + WINDOW_SNAP_RANGE && canSnap) {
			if (!window->restoreOnNextMove && !restored) window->beforeMaximiseBounds = bounds;
			window->restoreOnNextMove = true;
			bounds.t = screen.t;
			bounds.b = screen.b;
			bounds.l = screen.l;
			bounds.r = (screen.r + screen.l) / 2;
		} else if (newX >= screen.r - WINDOW_SNAP_RANGE && canSnap) {
			if (!window->restoreOnNextMove && !restored) window->beforeMaximiseBounds = bounds;
			window->restoreOnNextMove = true;
			bounds.t = screen.t;
			bounds.b = screen.b;
			bounds.l = (screen.r + screen.l) / 2;
			bounds.r = screen.r;
		} else {
			if (restored && window->resetPositionOnNextMove) {
				// The user previously snapped/maximised the window in a previous operation.
				// Therefore, the movement anchor won't be what the user expects.
				// Try to put it in the center.
				int positionAlongWindow = originalX - bounds2.l;
				int maxPosition = bounds2.r - bounds2.l;
				if (positionAlongWindow > maxPosition - oldWidth / 2) originalX = gui.lastClickX = positionAlongWindow - maxPosition + oldWidth;
				else if (positionAlongWindow > oldWidth / 2) originalX = gui.lastClickX = oldWidth / 2;
				originalY = gui.lastClickY = WINDOW_RESTORE_DRAG_Y_POSITION;
				window->resetPositionOnNextMove = false;
			}

			bounds.l = newX - originalX;
			bounds.t = newY - originalY;
			bounds.r = bounds.l + oldWidth;
			bounds.b = bounds.t + oldHeight;
		}
	} else {
		window->resetPositionOnNextMove = window->restoreOnNextMove = false;
	}

	EsSyscall(ES_SYSCALL_WINDOW_MOVE, window->handle, (uintptr_t) &bounds, 0, window->isMaximised ? ES_MOVE_WINDOW_MAXIMISED : ES_FLAGS_DEFAULT);
}

EsElement *WindowGetMainPanel(EsWindow *window) {
	if (window->windowStyle == ES_WINDOW_NORMAL) {
		return window->children[0]->children[0];
	} else if (window->windowStyle == ES_WINDOW_MENU) {
		if (!window->children[0]->GetChildCount()) {
			EsMenuNextColumn((EsMenu *) window, ES_FLAGS_DEFAULT);
		}

		return window->children[0]->GetChild(window->children[0]->GetChildCount() - 1);
	} else {
		return window->children[0];
	}
}

void InternalDestroyWindow(EsWindow *window) {
	EsSyscall(ES_SYSCALL_WINDOW_CLOSE, window->handle, 0, 0, 0);
	EsHandleClose(window->handle);
	window->handle = ES_INVALID_HANDLE;
}

int ProcessWindowBorderMessage(EsWindow *window, EsMessage *message, EsRectangle bounds, int from, int to) {
	if (message->type == ES_MSG_GET_CURSOR) {
		EsPoint position = EsMouseGetPosition(window);
		message->cursorStyle = ES_CURSOR_NORMAL;

		if (window->isMaximised) {
			gui.resizeType = 0;
			gui.resizeCursor = message->cursorStyle;
		} else {
			bool left = position.x < to, right = position.x >= bounds.r - to, 
			     top = position.y < to, bottom = position.y >= bounds.b - to;

			if (gui.resizing) {
				message->cursorStyle = gui.resizeCursor;
			} else if (position.x < from || position.y < from 
					|| position.x >= bounds.r - from || position.y >= bounds.b - from) {
			} else if ((right && top) || (bottom && left)) {
				message->cursorStyle = ES_CURSOR_RESIZE_DIAGONAL_1;
			} else if ((left && top) || (bottom && right)) {
				message->cursorStyle = ES_CURSOR_RESIZE_DIAGONAL_2;
			} else if (left || right) {
				message->cursorStyle = ES_CURSOR_RESIZE_HORIZONTAL;
			} else if (top || bottom) {
				message->cursorStyle = ES_CURSOR_RESIZE_VERTICAL;
			}

			if (!window->pressed && !gui.mouseButtonDown) {
				gui.resizeType = (left ? RESIZE_LEFT : 0) | (right ? RESIZE_RIGHT : 0) | (top ? RESIZE_TOP : 0) | (bottom ? RESIZE_BOTTOM : 0);
				gui.resizeCursor = message->cursorStyle;
			}
		}

		return ES_HANDLED;
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		EsPoint screenPosition = EsMouseGetPosition(nullptr);

		if (!window->isMaximised || gui.resizeType == RESIZE_MOVE) {
			ChangeWindowBounds(gui.resizeType, 
					screenPosition.x, screenPosition.y, 
					gui.lastClickX, gui.lastClickY, window);
			gui.resizing = true;
		}
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP) {
		if (window->restoreOnNextMove) {
			window->resetPositionOnNextMove = true;
		}

		gui.resizing = false;
	}

	return 0;
}

int ProcessRootMessage(EsElement *element, EsMessage *message) {
	EsWindow *window = (EsWindow *) element;
	EsRectangle bounds = window->GetBounds();
	int response = 0;

	if (window->windowStyle == ES_WINDOW_CONTAINER) {
		if (message->type == ES_MSG_PAINT) {
			EsDrawClear(message->painter, ES_MAKE_RECTANGLE(0, message->painter->target->width, 0, message->painter->target->height));
		} else if (message->type == ES_MSG_LAYOUT) {
			window->GetChild(0)->InternalMove(bounds.r - WINDOW_INSET * 2, CONTAINER_TAB_BAND_HEIGHT, WINDOW_INSET, WINDOW_INSET);
		} else {
			response = ProcessWindowBorderMessage(window, message, bounds, WINDOW_INSET - BORDER_THICKNESS, WINDOW_INSET);
		}
	} else if (window->windowStyle == ES_WINDOW_MENU) {
		if (message->type == ES_MSG_PAINT_BACKGROUND) {
			EsDrawClear(message->painter, message->painter->clip);
		} else if (message->type == ES_MSG_LAYOUT) {
			if (window->GetChildCount()) {
				window->GetChild(0)->InternalMove(bounds.r, bounds.b, 0, 0);
			}
		}
	} else if (window->windowStyle == ES_WINDOW_INSPECTOR) {
		if (message->type == ES_MSG_LAYOUT) {
			if (window->GetChildCount()) {
				window->GetChild(0)->InternalMove(bounds.r - BORDER_THICKNESS * 2, 
						bounds.b - BORDER_THICKNESS * 2 - 30, BORDER_THICKNESS, BORDER_THICKNESS + 30);
			}
		} else {
			response = ProcessWindowBorderMessage(window, message, bounds, 0, BORDER_THICKNESS);
		}
	} else if (window->windowStyle == ES_WINDOW_NORMAL) {
		if (message->type == ES_MSG_LAYOUT) {
			if (window->GetChildCount()) {
				EsElement *toolbar = window->GetChild(1);

				if (window->toolbarFillMode) {
					window->GetChild(1)->InternalMove(bounds.r, bounds.b, 0, 0);
				} else {
					int toolbarHeight = toolbar->GetChildCount() ? toolbar->currentStyle->metrics->maximumHeight 
						: toolbar->currentStyle->metrics->minimumHeight;
					window->GetChild(0)->InternalMove(bounds.r, bounds.b - toolbarHeight, 0, toolbarHeight);
					window->GetChild(1)->InternalMove(bounds.r, toolbarHeight, 0, 0);
					window->GetChild(2)->InternalMove(bounds.r, bounds.b, 0, 0);
				}
			}
		}
	} else if (window->windowStyle == ES_WINDOW_TIP || window->windowStyle == ES_WINDOW_PLAIN) {
		if (message->type == ES_MSG_LAYOUT) {
			if (window->GetChildCount()) {
				window->GetChild(0)->InternalMove(bounds.r, bounds.b, 0, 0);
			}
		}
	}

	if (message->type == ES_MSG_DESTROY) {
		if (window->windowStyle != ES_WINDOW_NORMAL) {
			EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_FLAGS_DEFAULT, 0, 0);
		}

		if (window->windowStyle == ES_WINDOW_MENU) {
			window->source->state &= ~UI_STATE_MENU_SOURCE;
			window->source->MaybeRefreshStyle();
			EsAssert(window->source->window->targetMenu == window);
			window->source->window->targetMenu = nullptr;
		}
	}

	return response;
}

void EsDialogClose(EsWindow *window) {
	EsMessageMutexCheck();
	EsAssert(window->hasDialog); // The window does not have an open dialog.
	window->dialogPanel->Destroy();
	window->dialogOverlay->Destroy(); // TODO This looks bad if we immediately open a new dialog. But maybe it'll look alright with exiting transitions?
	window->dialogOverlay = window->dialogPanel = nullptr;
	window->children[0]->children[0]->flags &= ~ES_ELEMENT_BLOCK_FOCUS;
	window->children[1]->state &= ~UI_STATE_BLOCK_INTERACTION;
	window->hasDialog = false;
}

EsElement *EsDialogShowAlert(EsWindow *window, const char *title, ptrdiff_t titleBytes, 
		const char *content, ptrdiff_t contentBytes, uint32_t iconID, uint32_t flags) {
	EsElement *dialog = EsDialogShow(window);
	EsPanel *heading = EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_DIALOG_HEADING);

	if (iconID) {
		EsIconDisplayCreate(heading, ES_FLAGS_DEFAULT, {}, iconID);
	}

	EsTextDisplayCreate(heading, ES_CELL_H_FILL | ES_CELL_V_CENTER, ES_STYLE_TEXT_HEADING2, 
			title, titleBytes)->cName = "dialog heading";
	EsTextDisplayCreate(EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_VERTICAL, ES_STYLE_DIALOG_CONTENT), 
			ES_CELL_H_FILL, ES_STYLE_TEXT_PARAGRAPH, 
			content, contentBytes)->cName = "dialog contents";

	EsPanel *buttonArea = EsPanelCreate(dialog, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE, ES_STYLE_DIALOG_BUTTON_AREA);

	if (flags & ES_DIALOG_ALERT_OK_BUTTON) {
		EsButton *button = EsButtonCreate(buttonArea, ES_BUTTON_DEFAULT, 0, "OK");
		EsButtonOnCommand(button, [] (EsInstance *instance, EsElement *, EsCommand *) { EsDialogClose(instance->window); });
		EsElementFocus(button);
	}

	return buttonArea;
}

EsElement *EsDialogShow(EsWindow *window) {
	EsAssert(window->windowStyle == ES_WINDOW_NORMAL); // Can only show dialogs on normal windows.
	EsAssert(!window->hasDialog); // Cannot nest dialogs.

	EsElement *mainStack = window->children[0];
	mainStack->children[0]->flags |= ES_ELEMENT_BLOCK_FOCUS;
	window->children[1]->state |= UI_STATE_BLOCK_INTERACTION;
	window->hasDialog = true;
	window->dialogOverlay = EsPanelCreate(mainStack, ES_CELL_FILL, ES_STYLE_PANEL_MODAL_OVERLAY);
	window->dialogOverlay->cName = "modal overlay";
	window->dialogPanel = EsPanelCreate(mainStack, ES_PANEL_VERTICAL | ES_CELL_CENTER | ES_CELL_SHRINK, ES_STYLE_PANEL_DIALOG_ROOT);
	window->dialogPanel->cName = "dialog";

	return window->dialogPanel;
}

EsWindow *EsWindowCreate(EsInstance *instance, EsWindowStyle style) {
	EsMessageMutexCheck();

	EsWindow *window = (EsWindow *) EsHeapAllocate(sizeof(EsWindow), true);
	arrput(gui.allWindows, window);
	window->instance = instance;

	if (style == ES_WINDOW_NORMAL) {
		EsAssert(instance);
		window->handle = ((ApplicationInstance *) instance->_private)->mainWindowHandle;
		EsSyscall(ES_SYSCALL_WINDOW_SET_OBJECT, window->handle, 0, (uintptr_t) window, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_RESIZE_CLEAR_COLOR, window->handle, 0xFF000000 | GetConstantNumber("windowFillColor"), 0, 0);
		window->activated = true;
	} else if (style == ES_WINDOW_MENU) {
		window->handle = EsSyscall(ES_SYSCALL_WINDOW_CREATE, style, 0, (uintptr_t) window, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_WINDOW_SOLID_TRUE, 9 * themeScale, 0);
	} else if (style == ES_WINDOW_INSPECTOR) {
		window->handle = EsSyscall(ES_SYSCALL_WINDOW_CREATE, style, 0, (uintptr_t) window, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_WINDOW_SOLID_TRUE, 0, 0);
	} else if (style == ES_WINDOW_CONTAINER) {
		window->windowWidth = GetConstantNumber("windowDefaultWidth");
		window->windowHeight = GetConstantNumber("windowDefaultHeight");
		static int cascadeX = -1, cascadeY = -1;
		EsRectangle workArea;
		EsSyscall(ES_SYSCALL_GET_SCREEN_WORK_AREA, 0, (uintptr_t) &workArea, 0, 0);
		int cascadeMargin = GetConstantNumber("windowCascadeMargin");
		int cascadeOffset = GetConstantNumber("windowCascadeOffset");
		if (cascadeX == -1 || cascadeX + (int) window->windowWidth > workArea.r - cascadeMargin) cascadeX = workArea.l + cascadeMargin;
		if (cascadeY == -1 || cascadeY + (int) window->windowHeight > workArea.b - cascadeMargin) cascadeY = workArea.t + cascadeMargin;
		EsRectangle bounds = ES_MAKE_RECTANGLE(cascadeX, cascadeX + window->windowWidth, cascadeY, cascadeY + window->windowHeight);
		cascadeX += cascadeOffset, cascadeY += cascadeOffset;
		window->handle = EsSyscall(ES_SYSCALL_WINDOW_CREATE, style, 0, (uintptr_t) window, 0);
		EsSyscall(ES_SYSCALL_WINDOW_MOVE, window->handle, (uintptr_t) &bounds, 0, ES_FLAGS_DEFAULT);
		EsSyscall(ES_SYSCALL_WINDOW_SET_FOCUSED, window->handle, 0, 0, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, ES_WINDOW_SOLID_TRUE, 10, 0);
	} else if (style == ES_WINDOW_TIP || style == ES_WINDOW_PLAIN) {
		window->handle = EsSyscall(ES_SYSCALL_WINDOW_CREATE, style, 0, (uintptr_t) window, 0);
		EsSyscall(ES_SYSCALL_WINDOW_SET_SOLID, window->handle, style == ES_WINDOW_PLAIN ? ES_WINDOW_SOLID_TRUE : ES_FLAGS_DEFAULT, 0, 0);
	}

	window->Initialise(nullptr, ES_CELL_FILL, ProcessRootMessage, nullptr);
	window->cName = "window";
	window->window = window;
	window->width = window->windowWidth, window->height = window->windowHeight;
	window->hovered = window;
	window->windowStyle = style;
	window->RefreshStyle();

	if (style == ES_WINDOW_NORMAL) {
		EsPanel *panel = EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_CELL_FILL | ES_PANEL_Z_STACK, ES_STYLE_PANEL_NORMAL_WINDOW_ROOT);
		panel->cName = "window stack";
		EsPanelCreate(panel, ES_CELL_FILL, ES_STYLE_PANEL_NORMAL_WINDOW_ROOT)->cName = "window root";
		EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_PANEL_HORIZONTAL | ES_CELL_FILL, ES_STYLE_PANEL_TOOLBAR_ROOT)->cName = "toolbar";

		EsElement *accessKeyLayer = EsCustomElementCreate(window, ES_ELEMENT_NON_CLIENT | ES_CELL_FILL | ES_ELEMENT_NO_HOVER, nullptr);
		
		accessKeyLayer->cName = "access key layer";

		accessKeyLayer->userCallback = [] (EsElement *element, EsMessage *message) {
			if (message->type == ES_MSG_PAINT && gui.accessKeyMode && gui.accessKeys.window == element->window) {
				AccessKeyHintsShow(message->painter);
			}

			return 0;
		};

		window->state |= UI_STATE_Z_STACK;
	} else if (style == ES_WINDOW_CONTAINER) {
		EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_CELL_FILL, ES_STYLE_PANEL_CONTAINER_WINDOW_ROOT);
	} else if (style == ES_WINDOW_INSPECTOR) {
		window->SetStylePart(ES_STYLE_PANEL_INSPECTOR_WINDOW_ROOT);
		EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_CELL_FILL, ES_STYLE_PANEL_INSPECTOR_WINDOW_CONTAINER);
		EsRectangle bounds = { 10, 600, 10, 500 };
		EsSyscall(ES_SYSCALL_WINDOW_MOVE, window->handle, (uintptr_t) &bounds, 0, ES_MOVE_WINDOW_ADJUST_TO_FIT_SCREEN);
	} else if (style == ES_WINDOW_TIP || style == ES_WINDOW_PLAIN) {
		EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_CELL_FILL, nullptr);
	} else if (style == ES_WINDOW_MENU) {
		window->SetStylePart(ES_STYLE_PANEL_MENU_ROOT);
		EsPanel *panel = EsPanelCreate(window, ES_ELEMENT_NON_CLIENT | ES_PANEL_HORIZONTAL | ES_CELL_FILL, ES_STYLE_PANEL_MENU_CONTAINER);
		panel->cName = "menu";

		panel->separatorStylePart = ES_STYLE_MENU_SEPARATOR_VERTICAL;
		panel->separatorFlags = ES_CELL_V_FILL;

		panel->userCallback = [] (EsElement *element, EsMessage *message) {
			if (message->type == ES_MSG_PAINT) {
				EsPainter *painter = message->painter;
				EsRectangle blurBounds = element->GetWindowBounds();
				blurBounds = AddBorder(blurBounds, ((UIStyle *) painter->style)->insets);
				EsSyscall(ES_SYSCALL_WINDOW_SET_BLUR_BOUNDS, element->window->handle, (uintptr_t) &blurBounds, 0, 0);
			}

			return 0;
		};
	}

	if (style == ES_WINDOW_INSPECTOR) {
		InspectorSetup(window);
	}

	return window;
}

struct EsMenu : EsWindow {};

void EsMenuAddSeparator(EsMenu *menu) {
	EsCustomElementCreate(menu, ES_CELL_H_FILL, ES_STYLE_MENU_SEPARATOR_HORIZONTAL)->cName = "menu separator";
}

void EsMenuNextColumn(EsMenu *menu, uint64_t flags) {
	EsPanelCreate(menu->children[0], ES_PANEL_VERTICAL | ES_CELL_V_TOP | flags, ES_STYLE_PANEL_MENU_COLUMN);
}

EsElement *EsMenuGetSource(EsMenu *menu) {
	return ((EsWindow *) menu)->source;
}

EsMenu *EsMenuCreate(EsElement *source, uint64_t flags) {
	EsWindow *menu = (EsWindow *) EsWindowCreate(source->instance, ES_WINDOW_MENU);
	menu->flags |= flags;
	menu->activated = true;
	menu->source = source;
	return (EsMenu *) menu;
}

void EsMenuShow(EsMenu *menu, int fixedWidth, int fixedHeight) {
	menu->source->state |= UI_STATE_MENU_SOURCE;
	menu->source->MaybeRefreshStyle();
	menu->source->window->targetMenu = menu;

	EsRectangle menuInsets = menu->GetChild(0)->currentStyle->insets;
	if (fixedWidth) fixedWidth += menuInsets.l + menuInsets.r;
	if (fixedHeight) fixedHeight += menuInsets.t + menuInsets.b;
	int width = fixedWidth ?: menu->GetChild(0)->GetWidth(fixedHeight);
	int height = menu->GetChild(0)->GetHeight(width);

	if (fixedHeight) {
		if (menu->flags & ES_MENU_MAXIMUM_HEIGHT) {
			if (height >= fixedHeight) {
				height = fixedHeight;
			}
		} else {
			height = fixedHeight;
		}
	}

	EsPoint position;

	if (!(menu->flags & ES_MENU_AT_CURSOR)) {
		EsRectangle bounds = menu->source->GetScreenBounds(false);
		position = ES_MAKE_POINT(bounds.l - (width + menuInsets.l - menuInsets.r) / 2 + menu->source->width / 2, 
				bounds.b - menuInsets.t);
	} else {
		position = EsMouseGetPosition();
		position.x -= menuInsets.l, position.y -= menuInsets.t;
	}

	menu->width = width, menu->height = height;
	EsRectangle bounds = ES_MAKE_RECTANGLE(position.x, position.x + width, position.y, position.y + height);
	EsSyscall(ES_SYSCALL_WINDOW_MOVE, menu->handle, (uintptr_t) &bounds, 0, ES_MOVE_WINDOW_ADJUST_TO_FIT_SCREEN | ES_MOVE_WINDOW_ALWAYS_ON_TOP);
}

void EsMenuCloseAll() {
	for (uintptr_t i = 0; i < arrlenu(gui.allWindows); i++) {
		if (gui.allWindows[i]->windowStyle == ES_WINDOW_MENU) {
			EsElementDestroy(gui.allWindows[i]);
		}
	}
}

// --------------------------------- Painting and styling.

#include "renderer.cpp"

// --------------------------------- Layouting.

EsRectangle LayoutCell(EsElement *element, int width, int height) {
	uint64_t layout = element->flags;

	int maximumWidth  = element->currentStyle->metrics->maximumWidth  ?: ES_PANEL_BAND_SIZE_DEFAULT;
	int minimumWidth  = element->currentStyle->metrics->minimumWidth  ?: ES_PANEL_BAND_SIZE_DEFAULT;
	int maximumHeight = element->currentStyle->metrics->maximumHeight ?: ES_PANEL_BAND_SIZE_DEFAULT;
	int minimumHeight = element->currentStyle->metrics->minimumHeight ?: ES_PANEL_BAND_SIZE_DEFAULT;

	if (layout & ES_CELL_H_EXPAND) maximumWidth = INT_MAX;
	if (layout & ES_CELL_H_SHRINK) minimumWidth = 0;
	if (layout & ES_CELL_V_EXPAND) maximumHeight = INT_MAX;
	if (layout & ES_CELL_V_SHRINK) minimumHeight = 0;

	if (maximumWidth == ES_PANEL_BAND_SIZE_DEFAULT || minimumWidth == ES_PANEL_BAND_SIZE_DEFAULT) {
		int width = element->GetWidth(height);
		if (maximumWidth == ES_PANEL_BAND_SIZE_DEFAULT) maximumWidth = width;
		if (minimumWidth == ES_PANEL_BAND_SIZE_DEFAULT) minimumWidth = width;
	}

	int preferredWidth = ClampInteger(minimumWidth,  maximumWidth,  width);

	if (maximumHeight == ES_PANEL_BAND_SIZE_DEFAULT || minimumHeight == ES_PANEL_BAND_SIZE_DEFAULT) {
		int height = element->GetHeight(preferredWidth);
		if (maximumHeight == ES_PANEL_BAND_SIZE_DEFAULT) maximumHeight = height;
		if (minimumHeight == ES_PANEL_BAND_SIZE_DEFAULT) minimumHeight = height;
	}

	int preferredHeight = ClampInteger(minimumHeight, maximumHeight, height);

	EsRectangle bounds = ES_MAKE_RECTANGLE(0, width, 0, height);

	if ((layout & (ES_CELL_H_LEFT | ES_CELL_H_RIGHT)) == ES_CELL_H_LEFT) {
		bounds.r = bounds.l + preferredWidth;
	} else if ((layout & (ES_CELL_H_LEFT | ES_CELL_H_RIGHT)) == ES_CELL_H_RIGHT) {
		bounds.l = bounds.r - preferredWidth;
	} else {
		bounds.l = bounds.l + width / 2 - preferredWidth / 2;
		bounds.r = bounds.l + preferredWidth;
	}

	if ((layout & (ES_CELL_V_TOP | ES_CELL_V_BOTTOM)) == ES_CELL_V_TOP) {
		bounds.b = bounds.t + preferredHeight;
	} else if ((layout & (ES_CELL_V_TOP | ES_CELL_V_BOTTOM)) == ES_CELL_V_BOTTOM) {
		bounds.t = bounds.b - preferredHeight;
	} else {
		bounds.t = bounds.t + height / 2 - preferredHeight / 2;
		bounds.b = bounds.t + preferredHeight;
	}

	return bounds;
}

void LayoutTable(EsPanel *panel, EsMessage *message) {
	bool debug = false;

	bool has[2] = {};
	int in[2] = {};
	int out[2] = {};

	if (message->type == ES_MSG_GET_WIDTH) {
		in[1] = message->measure.height;
		has[1] = has[1];
	} else if (message->type == ES_MSG_GET_HEIGHT) {
		in[0] = message->measure.width;
		has[0] = in[0];
	} else {
		EsRectangle bounds = panel->GetBounds();
		in[0] = bounds.r, in[1] = bounds.b;
		has[0] = has[1] = true;
	}

	// if (debug) EsPrint("LayoutTable, %z, %d/%d\n", isMeasure ? "measure" : "layout", in[0], in[1]);

	size_t childCount = panel->GetChildCount();

	uint8_t *memoryBase = (uint8_t *) EsHeapAllocate(sizeof(int) * childCount * 2 + sizeof(EsPanelBand) * (panel->bandCount[0] + panel->bandCount[1]), true), *memory = memoryBase;

	int *calculatedSize[2];
	calculatedSize[0] = (int *) memory; memory += sizeof(int) * childCount;
	calculatedSize[1] = (int *) memory; memory += sizeof(int) * childCount;
	EsPanelBand *calculatedProperties[2];
	calculatedProperties[0] = (EsPanelBand *) memory; memory += sizeof(EsPanelBand) * panel->bandCount[0];
	calculatedProperties[1] = (EsPanelBand *) memory; memory += sizeof(EsPanelBand) * panel->bandCount[1];

	for (int axis = 0; axis < 2; axis++) {
		if (panel->bands[axis]) {
			EsMemoryCopy(calculatedProperties[axis], panel->bands[axis], sizeof(EsPanelBand) * panel->bandCount[axis]);
		} else {
			for (uintptr_t i = 0; i < panel->bandCount[axis]; i++) {
				calculatedProperties[axis][i].preferredSize 
					= calculatedProperties[axis][i].maximumSize 
					= calculatedProperties[axis][i].minimumSize 
					= ES_PANEL_BAND_SIZE_DEFAULT;
			}
		}
	}

	EsRectangle insets = panel->GetInsets();

#define TABLE_AXIS_HORIZONTAL (0)
#define TABLE_AXIS_VERTICAL (1)

	for (int _axis = 0; _axis < 2; _axis++) {
		int axis = (~panel->flags & ES_PANEL_HORIZONTAL) ? (1 - _axis) : _axis;
		int gapSize = _axis ? panel->GetGapMinor() : panel->GetGapMajor();
		int insetStart = axis ? insets.t : insets.l;
		int insetEnd = axis ? insets.b : insets.r;

		if (debug) EsPrint("\tAxis %d\n", axis);

		for (uintptr_t i = 0; i < childCount; i++) {
			EsElement *child = panel->GetChild(i);
			if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;

			// Step 1: Find the preferred size of the children for this axis.

			int size;

			if ((child->flags & (axis ? ES_CELL_V_PUSH : ES_CELL_H_PUSH)) && has[axis]) {
				size = 0;
			} else {
				int alternate = _axis ? calculatedSize[1 - axis][i] : 0;
				size = axis == TABLE_AXIS_HORIZONTAL ? child->GetWidth(alternate) : child->GetHeight(alternate);
			}

			if (debug) EsPrint("\tChild %d (%z) in cells %d->%d has size %d\n", i, child->cName, child->tableCell.from[axis], child->tableCell.to[axis], size);

			// Step 2: Find the preferred size of each band on this axis.

			int bandSpan = child->tableCell.to[axis] - child->tableCell.from[axis] + 1;
			int totalGapSize = (bandSpan - 1) * gapSize;

			int preferredSizePerBand = (size - totalGapSize) / bandSpan;
			int maximumSizeValue = axis ? child->currentStyle->metrics->maximumHeight : child->currentStyle->metrics->maximumWidth;
			int minimumSizeValue = axis ? child->currentStyle->metrics->minimumHeight : child->currentStyle->metrics->minimumWidth;
			int maximumSizePerBand = maximumSizeValue ? (((int) maximumSizeValue - totalGapSize) / bandSpan) : ES_PANEL_BAND_SIZE_DEFAULT;
			int minimumSizePerBand = maximumSizeValue ? (((int) minimumSizeValue - totalGapSize) / bandSpan) : ES_PANEL_BAND_SIZE_DEFAULT;

			for (int j = child->tableCell.from[axis]; j <= child->tableCell.to[axis]; j++) {
				EsAssert(j >= 0 && j < panel->bandCount[axis]); // Invalid element cell.

				EsPanelBand *band = calculatedProperties[axis] + j;

				if (child->flags & (axis ? ES_CELL_V_PUSH : ES_CELL_H_PUSH)) {
					if (!band->push) {
						band->push = 1;
					}
				}

				if (!panel->bands[axis] || panel->bands[axis][j].preferredSize == ES_PANEL_BAND_SIZE_DEFAULT) {
					if (band->preferredSize != ES_PANEL_BAND_SIZE_DEFAULT) {
						if (band->preferredSize < preferredSizePerBand) {
							band->preferredSize = preferredSizePerBand;
						}
					} else {
						band->preferredSize = preferredSizePerBand;
					}
				}

				if (!panel->bands[axis] || panel->bands[axis][j].maximumSize == ES_PANEL_BAND_SIZE_DEFAULT) {
					if (maximumSizePerBand != ES_PANEL_BAND_SIZE_DEFAULT) {
						if (band->maximumSize != ES_PANEL_BAND_SIZE_DEFAULT) {
							if (band->maximumSize > maximumSizePerBand) {
								band->maximumSize = maximumSizePerBand;
							}
						} else {
							band->maximumSize = maximumSizePerBand;
						}
					}
				}

				if (!panel->bands[axis] || panel->bands[axis][j].minimumSize == ES_PANEL_BAND_SIZE_DEFAULT) {
					if (minimumSizePerBand != ES_PANEL_BAND_SIZE_DEFAULT) {
						if (band->minimumSize != ES_PANEL_BAND_SIZE_DEFAULT) {
							if (band->minimumSize < minimumSizePerBand) {
								band->minimumSize = minimumSizePerBand;
							}
						} else {
							band->minimumSize = minimumSizePerBand;
						}
					}
				}
			}
		}

		// Step 3: Work out the size of each band.

		if (has[axis]) {
			int contentSpace = in[axis] - insetStart - insetEnd - (panel->bandCount[axis] - 1) * gapSize;

			for (int i = 0; i < panel->bandCount[axis]; i++) {
				EsPanelBand *band = calculatedProperties[axis] + i;
				if (band->minimumSize   == ES_PANEL_BAND_SIZE_DEFAULT) band->minimumSize   = 0;
				if (band->preferredSize == ES_PANEL_BAND_SIZE_DEFAULT) band->preferredSize = 0;
				if (band->maximumSize   == ES_PANEL_BAND_SIZE_DEFAULT) band->maximumSize   = INT_MAX;
			}

			int usedSpace = 0;
			
			for (int i = 0; i < panel->bandCount[axis]; i++) {
				usedSpace += calculatedProperties[axis][i].preferredSize;
			}
			
			bool shrink = usedSpace > contentSpace;
			int remainingDifference = AbsoluteInteger(usedSpace - contentSpace);

			while (remainingDifference > 0) {
				int availableWeight = 0;
				
				for (int i = 0; i < panel->bandCount[axis]; i++) {
					EsPanelBand *band = calculatedProperties[axis] + i;
					availableWeight += shrink ? band->pull : band->push;
				}
				
				if (!availableWeight) {
					break; // There are no more flexible bands.
				}
				
				int perWeight = remainingDifference / availableWeight, perWeightExtra = remainingDifference % availableWeight;
				bool stable = true;
				
				for (int i = 0; i < panel->bandCount[axis]; i++) {
					EsPanelBand *band = calculatedProperties[axis] + i;
					int available = shrink ? (band->preferredSize - band->minimumSize) : (band->maximumSize - band->preferredSize);
					int weight = shrink ? band->pull : band->push;
					int change = weight * perWeight;
					int extra = MinimumInteger(perWeightExtra, weight);
					change += extra, perWeightExtra -= extra;
					
					if (change > available) {
						band->preferredSize = shrink ? band->minimumSize : band->maximumSize;
						band->pull = band->push = 0;
						remainingDifference -= available;
						stable = false;
					}
				}
				
				if (stable) {
					perWeightExtra = remainingDifference % availableWeight;
				
					for (int i = 0; i < panel->bandCount[axis]; i++) {
						EsPanelBand *band = calculatedProperties[axis] + i;
						int weight = shrink ? band->pull : band->push;
						int change = weight * perWeight;
						int extra = MinimumInteger(perWeightExtra, weight);
						change += extra, perWeightExtra -= extra;
						band->preferredSize += (shrink ? -1 : 1) * change;
						if (band->preferredSize < band->minimumSize) band->preferredSize = band->minimumSize;
						if (band->preferredSize > band->maximumSize) band->preferredSize = band->maximumSize;
					}
					
					break; // We've found a working configuration.
				}
			}
		}

		// Step 4: Work out the final size of each child.

		for (uintptr_t i = 0; i < childCount; i++) {
			EsElement *child = panel->GetChild(i);
			if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;

			int size = (child->tableCell.to[axis] - child->tableCell.from[axis]) * gapSize;

			for (int j = child->tableCell.from[axis]; j <= child->tableCell.to[axis]; j++) {
				EsPanelBand *band = calculatedProperties[axis] + j;
				size += band->preferredSize;
			}

			calculatedSize[axis][i] = size;
		}

		// Step 5: Calculate the position of the bands.

		int position = insetStart;

		for (int i = 0; i < panel->bandCount[axis]; i++) {
			if (i) position += gapSize;
			EsPanelBand *band = calculatedProperties[axis] + i;
			int size = band->preferredSize;
			band->maximumSize = position; // Aliasing maximumSize with position.
			position += size;
		}

		out[axis] = position + insetEnd;
	}

	// Step 6: Move the children to their new location.

	if (message->type == ES_MSG_GET_WIDTH) {
		message->measure.width = out[0];
	} else if (message->type == ES_MSG_GET_HEIGHT) {
		message->measure.height = out[1];
	} else {
		for (uintptr_t i = 0; i < childCount; i++) {
			EsElement *element = panel->GetChild(i);

			if (element->flags & ES_ELEMENT_NON_CLIENT) {
				continue;
			} else if (element->flags & ES_ELEMENT_HIDDEN) {
				element->InternalMove(0, 0, -1, -1);
				continue;
			}

			int position[2], size[2];

			for (int axis = 0; axis < 2; axis++) {
				position[axis] = calculatedProperties[axis][element->tableCell.from[axis]].maximumSize;
				size[axis] = calculatedSize[axis][i];
			}

			element->InternalMove(size[0], size[1], position[0] - panel->scroll.x, position[1] - panel->scroll.y);
		}
	}

	if (debug) {
		EsPrint("\t%d/%d\n", out[0], out[1]);
	}

	EsHeapFree(memoryBase);
}

int LayoutStackDeterminePerPush(EsPanel *panel, int available, int secondary) {
	size_t childCount = panel->GetChildCount();
	int fill = 0, count = 0, perPush = 0;

	for (uintptr_t i = 0; i < childCount; i++) {
		EsElement *child = panel->GetChild(i);
		if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;

		count++;

		if (panel->flags & ES_PANEL_HORIZONTAL) {
			if (child->flags & ES_CELL_H_PUSH) {
				fill++;
			} else if (available > 0) {
				available -= child->GetWidth(secondary);
			}
		} else {
			if (child->flags & ES_CELL_V_PUSH) {
				fill++;
			} else if (available > 0) {
				available -= child->GetHeight(secondary);
			}
		}
	}

	if (count) {
		available -= (count - 1) * panel->GetGapMajor();
	}

	if (available > 0 && fill) {
		perPush = available / fill;
	}

	return perPush;
}

void LayoutStackSecondary(EsPanel *panel, EsMessage *message) {
	bool horizontal = panel->flags & ES_PANEL_HORIZONTAL;
	size_t childCount = panel->GetChildCount();
	EsRectangle insets = panel->GetInsets();
	int size = 0;

	int primary = horizontal ? message->measure.width : message->measure.height;
	int perPush = 0;

	if (primary) {
		if (horizontal) primary -= insets.l + insets.r;
		else primary -= insets.t + insets.b;
		perPush = LayoutStackDeterminePerPush(panel, primary, 0);
	}

	for (uintptr_t i = 0; i < childCount; i++) {
		EsElement *child = panel->GetChild(i);
		if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;

		if (horizontal) {
			int height = child->GetHeight((child->flags & ES_CELL_H_PUSH) ? perPush : 0);
			if (height > size) size = height;
		} else {
			int width = child->GetWidth((child->flags & ES_CELL_V_PUSH) ? perPush : 0);
			if (width > size) size = width;
		}
	}

	if (horizontal) message->measure.height = size + insets.t + insets.b;
	else message->measure.width = size + insets.l + insets.r;
}

void LayoutStackPrimary(EsPanel *panel, EsMessage *message) {
	bool horizontal = panel->flags & ES_PANEL_HORIZONTAL;
	bool reverse = panel->flags & ES_PANEL_REVERSE;
	EsRectangle bounds = panel->GetBounds();
	size_t childCount = panel->GetChildCount();

	EsRectangle insets = panel->GetInsets();
	int gap = panel->GetGapMajor();

	if (message->type != ES_MSG_LAYOUT && (panel->flags & ES_ELEMENT_DEBUG)) {
		EsPrint("Measuring stack %x on primary axis with %d children, gap %d, insets %R.\n", panel, childCount, gap, insets);
	}

	if (message->type == ES_MSG_LAYOUT && (panel->flags & ES_ELEMENT_DEBUG)) {
		EsPrint("LayoutStack %x into %R with %d children, gap %d, insets %R.\n", panel, bounds, childCount, gap, insets);
	}

	int hBase = message->type == ES_MSG_GET_HEIGHT ? message->measure.width : Width(bounds);
	int vBase = message->type == ES_MSG_GET_WIDTH ? message->measure.height : Height(bounds);

	int hSpace = hBase ? (hBase - insets.l - insets.r) : 0;
	int vSpace = vBase ? (vBase - insets.t - insets.b) : 0;
	int available = horizontal ? hSpace : vSpace;
	int perPush = LayoutStackDeterminePerPush(panel, available, horizontal ? vSpace : hSpace);

	int position = horizontal ? (reverse ? insets.r : insets.l) : (reverse ? insets.b : insets.t);
	bool anyNonHiddenChildren = false;

	for (uintptr_t i = 0; i < childCount; i++) {
		EsElement *child = panel->GetChild(i);
		if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;
		EsRectangle relative;
		anyNonHiddenChildren = true;

		if (horizontal) {
			int width = (child->flags & ES_CELL_H_PUSH) ? perPush : child->GetWidth(vSpace);

			if (reverse) {
				relative = ES_MAKE_RECTANGLE(bounds.r - position - width, bounds.r - position, insets.t, bounds.b - insets.b);
			} else {
				relative = ES_MAKE_RECTANGLE(position, position + width, insets.t, bounds.b - insets.b);
			}

			position += width + gap;
		} else {
			int height = (child->flags & ES_CELL_V_PUSH) ? perPush : child->GetHeight(hSpace);

			if (reverse) {
				relative = ES_MAKE_RECTANGLE(insets.l, bounds.r - insets.r, bounds.b - position - height, bounds.b - position);
			} else {
				relative = ES_MAKE_RECTANGLE(insets.l, bounds.r - insets.r, position, position + height);
			}

			position += height + gap;
		}

		if (message->type == ES_MSG_LAYOUT) {
			if (message->type == ES_MSG_LAYOUT && (panel->flags & ES_ELEMENT_DEBUG)) {
				EsPrint("\tMove child %d into %R.\n", i, relative);
			}

			child->InternalMove(Translate(relative, -panel->scroll.x, -panel->scroll.y));
		}
	}

	if (anyNonHiddenChildren) position -= gap;

	if (message->type == ES_MSG_GET_WIDTH) {
		message->measure.width = position + (reverse ? insets.l : insets.r);
	} else if (message->type == ES_MSG_GET_HEIGHT) {
		message->measure.height = position + (reverse ? insets.t : insets.b);
	}
}

void LayoutStack(EsPanel *panel, EsMessage *message) {
	bool horizontal = panel->flags & ES_PANEL_HORIZONTAL;

	if (message->type == ES_MSG_LAYOUT 
			|| (message->type == ES_MSG_GET_WIDTH && horizontal) 
			|| (message->type == ES_MSG_GET_HEIGHT && !horizontal)) {
		LayoutStackPrimary(panel, message);
	} else {
		LayoutStackSecondary(panel, message);
	}
}

int EsElement::GetWidth(int height) {
	if (currentStyle->preferredWidth) return currentStyle->preferredWidth;
	if (!height) height = currentStyle->preferredHeight;
	else if (currentStyle->preferredHeight && currentStyle->preferredHeight > height && (~flags & (ES_CELL_V_SHRINK))) height = currentStyle->preferredHeight;
	else if (currentStyle->preferredHeight && currentStyle->preferredHeight < height && (~flags & (ES_CELL_V_EXPAND))) height = currentStyle->preferredHeight;
	else if (currentStyle->metrics->minimumHeight && currentStyle->metrics->minimumHeight > height) height = currentStyle->metrics->minimumHeight;
	else if (currentStyle->metrics->maximumHeight && currentStyle->metrics->maximumHeight < height) height = currentStyle->metrics->maximumHeight;
	if (height) height -= internalOffsetTop + internalOffsetBottom;
	EsMessage m = { ES_MSG_GET_WIDTH };
	m.measure.height = height;
	EsMessageSend(this, &m);
	return m.measure.width + internalOffsetLeft + internalOffsetRight;
}

int EsElement::GetHeight(int width) {
	if (currentStyle->preferredHeight) return currentStyle->preferredHeight;
	if (!width) width = currentStyle->preferredWidth;
	else if (currentStyle->preferredWidth && currentStyle->preferredWidth > width && (~flags & (ES_CELL_H_SHRINK))) width = currentStyle->preferredWidth;
	else if (currentStyle->preferredWidth && currentStyle->preferredWidth < width && (~flags & (ES_CELL_H_EXPAND))) width = currentStyle->preferredWidth;
	else if (currentStyle->metrics->minimumWidth && currentStyle->metrics->minimumWidth > width) width = currentStyle->metrics->minimumWidth;
	else if (currentStyle->metrics->maximumWidth && currentStyle->metrics->maximumWidth < width) width = currentStyle->metrics->maximumWidth;
	if (width) width -= internalOffsetLeft + internalOffsetRight;
	EsMessage m = { ES_MSG_GET_HEIGHT };
	m.measure.width = width;
	EsMessageSend(this, &m);
	return m.measure.height + internalOffsetTop + internalOffsetBottom;
}

void EsElement::InternalMove(int _width, int _height, int _offsetX, int _offsetY) {
	if (state & UI_STATE_EXITING) {
		return;
	}

#ifdef TRACE_LAYOUT
	if (parent) {
		EsElement *parent = this->parent->parent;
		while (parent) parent = parent->parent, EsPrint("\t");
		EsPrint("(move) %z\n", debugName);
	}
#endif

	// Work out where in the cell we should be placed (using the element's alignment flags).

	EsRectangle givenBounds = { _offsetX, _offsetX + _width, _offsetY, _offsetY + _height };
	EsRectangle bounds = LayoutCell(this, _width, _height);
	_width = bounds.r - bounds.l, _height = bounds.b - bounds.t;
	_offsetX += bounds.l, _offsetY += bounds.t;

#ifdef TRACE_LAYOUT
	if (parent) {
		EsElement *parent = this->parent->parent;
		while (parent) parent = parent->parent, EsPrint("\t");
		EsPrint("(move) %z :: in %d, %d; %d, %d :: ", debugName, _offsetX, _offsetY, _width, _height);
	}
#endif

	// Add the internal offset.

	if (parent) {
		_offsetX += parent->internalOffsetLeft;
		_offsetY += parent->internalOffsetTop;
	}

	// What has changed?

	bool hasPositionChanged = _offsetX != offsetX || _offsetY != offsetY;
	bool hasSizeChanged = _width != width || _height != height;
	bool relayoutRequested = state & UI_STATE_RELAYOUT;
	bool relayoutChild = state & UI_STATE_RELAYOUT_CHILD;
	int oldOffsetX = offsetX, oldOffsetY = offsetY;

#ifdef TRACE_LAYOUT
	if (parent) {
		EsPrint("align %d, %d; %d, %d ::%z%z%z%z\n", _offsetX, _offsetY, _width, _height,
				hasPositionChanged ? " pos" : "", hasSizeChanged ? " size" : "", 
				relayoutRequested ? " rel" : "", relayoutChild ? " child" : "");
	}
#endif

	// Update the variables.

	offsetX = _offsetX;
	offsetY = _offsetY;
	width = _width;
	height = _height;
	state &= ~(UI_STATE_RELAYOUT | UI_STATE_RELAYOUT_CHILD);

	if (!relayoutRequested && !hasSizeChanged) {
		// If our size hasn't changed and a relayout wasn't requested, then we don't need to do any layouting.

		if (hasPositionChanged) {
			// Clear the old position.

			if (parent) {
				EsRectangle paintOutsets = currentStyle->paintOutsets;
				EsRectangle rectangle = ES_MAKE_RECTANGLE(oldOffsetX - paintOutsets.l, oldOffsetX + width + paintOutsets.r,
						oldOffsetY - paintOutsets.t, oldOffsetY + height + paintOutsets.b);
				parent->Repaint(false, rectangle);
			}

			// Repaint if we've moved.

			Repaint(true);
		}

		if (relayoutChild) {
			for (uintptr_t i = 0; i < arrlenu(children); i++) {
				if (children[i]->state & (UI_STATE_RELAYOUT | UI_STATE_RELAYOUT_CHILD)) {
					children[i]->InternalMove(children[i]->width, children[i]->height, children[i]->offsetX, children[i]->offsetY);
				}
			}
		}
	} else {
		// Tell the element to layout its contents.

		EsMessage m = { ES_MSG_LAYOUT };
		m.layout.sizeChanged = hasSizeChanged;
		EsMessageSend(this, &m);

		// Repaint.

		Repaint(true);
	}

	if (hasPositionChanged || hasSizeChanged) {
		InspectorNotifyElementMoved(this, ES_MAKE_RECTANGLE(offsetX, offsetX + width, offsetY, offsetY + height), givenBounds);
	}
}

void EsElementMove(EsElement *element, int x, int y, int width, int height) {
	EsMessageMutexCheck();

	element->InternalMove(width, height, x, y);
}

EsRectangle EsElementGetPreferredSize(EsElement *element) {
	EsMessageMutexCheck();

	return ES_MAKE_RECTANGLE(0, element->currentStyle->preferredWidth, 0, element->currentStyle->preferredHeight);
}

void EsElementRelayout(EsElement *element) {
	if (element->state & UI_STATE_DESTROYING) return;
	element->state |= UI_STATE_RELAYOUT;
	EnsureWindowWillUpdate(element->window);

	while (element) {
		element->state |= UI_STATE_RELAYOUT_CHILD;
		element = element->parent;
	}
}

void EsElementUpdateContentSize(EsElement *element, uint32_t flags) {
	if (element->state & UI_STATE_DESTROYING) return;
	if (!flags) flags = ES_ELEMENT_UPDATE_CONTENT_WIDTH | ES_ELEMENT_UPDATE_CONTENT_HEIGHT;

	while (element && flags) {
		element->state &= ~UI_STATE_USE_MEASUREMENT_CACHE;
		EsElementRelayout(element);

		if (element->currentStyle->preferredWidth || ((element->flags & ES_CELL_H_FILL) == ES_CELL_H_FILL)) {
			flags &= ~ES_ELEMENT_UPDATE_CONTENT_WIDTH;
		}

		if (element->currentStyle->preferredHeight || ((element->flags & ES_CELL_V_FILL) == ES_CELL_V_FILL)) {
			flags &= ~ES_ELEMENT_UPDATE_CONTENT_HEIGHT;
		}

		element = element->parent;
	}
}

// --------------------------------- Scrollbars.

// #define ENABLE_SMOOTH_SCROLLING

struct EsScrollbar : EsElement {
	EsButton *up, *down;
	EsElement *thumb;
	double position, autoScrollSpeed, smoothScrollTarget;
	int viewportSize, contentSize, thumbSize, oldThumbPosition, thumbPosition, originalThumbPosition, oldPosition;
	bool horizontal;
};

void ScrollbarLayout(EsScrollbar *scrollbar) {
	if (scrollbar->viewportSize >= scrollbar->contentSize || scrollbar->viewportSize <= 0 || scrollbar->contentSize <= 0) {
		EsElementSetDisabled(scrollbar, true);
	} else {
		EsElementSetDisabled(scrollbar, false);
		EsRectangle bounds = scrollbar->GetBounds();

		if (scrollbar->horizontal) {
			scrollbar->thumbSize = scrollbar->viewportSize * (bounds.r - scrollbar->height * 2) / scrollbar->contentSize;

			if (scrollbar->thumbSize < scrollbar->thumb->currentStyle->preferredWidth) {
				scrollbar->thumbSize = scrollbar->thumb->currentStyle->preferredWidth;
			}

			if (scrollbar->thumbSize > Width(bounds) - scrollbar->height * 2) {
				scrollbar->thumbSize = Width(bounds) - scrollbar->height * 2;
			}

			scrollbar->thumbPosition = LinearMap(0, scrollbar->contentSize - scrollbar->viewportSize, 
					scrollbar->height, bounds.r - scrollbar->thumbSize - scrollbar->height, scrollbar->smoothScrollTarget);

			scrollbar->up->InternalMove((int) scrollbar->thumbPosition + scrollbar->thumbSize / 2, scrollbar->thumb->currentStyle->preferredHeight, 0, 0);
			scrollbar->thumb->InternalMove(scrollbar->thumbSize, scrollbar->thumb->currentStyle->preferredHeight, (int) scrollbar->thumbPosition, 0);
			scrollbar->down->InternalMove(bounds.r - scrollbar->thumbSize / 2 - (int) scrollbar->thumbPosition, 
					scrollbar->thumb->currentStyle->preferredHeight, 
					(int) scrollbar->thumbPosition + scrollbar->thumbSize / 2, 0);
		} else {
			scrollbar->thumbSize = scrollbar->viewportSize * (bounds.b - scrollbar->width * 2) / scrollbar->contentSize;

			if (scrollbar->thumbSize < scrollbar->thumb->currentStyle->preferredHeight) {
				scrollbar->thumbSize = scrollbar->thumb->currentStyle->preferredHeight;
			}

			if (scrollbar->thumbSize > Height(bounds) - scrollbar->width * 2) {
				scrollbar->thumbSize = Height(bounds) - scrollbar->width * 2;
			}

			scrollbar->thumbPosition = LinearMap(0, scrollbar->contentSize - scrollbar->viewportSize, 
					scrollbar->width, bounds.b - scrollbar->thumbSize - scrollbar->width, scrollbar->smoothScrollTarget);

			scrollbar->up->InternalMove(scrollbar->thumb->currentStyle->preferredWidth, (int) scrollbar->thumbPosition + scrollbar->thumbSize / 2, 0, 0);
			scrollbar->thumb->InternalMove(scrollbar->thumb->currentStyle->preferredWidth, scrollbar->thumbSize, 0, (int) scrollbar->thumbPosition);
			scrollbar->down->InternalMove(scrollbar->thumb->currentStyle->preferredWidth, 
					bounds.b - scrollbar->thumbSize / 2 - (int) scrollbar->thumbPosition, 
					0, (int) scrollbar->thumbPosition + scrollbar->thumbSize / 2);
		}
	}
}

void ScrollbarSetMeasurements(EsScrollbar *scrollbar, int viewportSize, int contentSize) {
	EsMessageMutexCheck();

	if (scrollbar->viewportSize == viewportSize && scrollbar->contentSize == contentSize) {
		return;
	}

	scrollbar->viewportSize = viewportSize;
	scrollbar->contentSize = contentSize;

	ScrollbarLayout(scrollbar);
}

void ScrollbarSetPosition(EsScrollbar *scrollbar, double position, bool sendMovedMessage, bool smoothScroll) {
	EsMessageMutexCheck();

	if (position > scrollbar->contentSize - scrollbar->viewportSize) position = scrollbar->contentSize - scrollbar->viewportSize;
	if (position < 0) position = 0;

	scrollbar->smoothScrollTarget = position;

	int previous = scrollbar->position;

#ifdef ENABLE_SMOOTH_SCROLLING
	if (smoothScroll && OSCRTabsf(position - scrollbar->position) > 10) {
		scrollbar->StartAnimating();
	} else {
		scrollbar->position = position;
	}
#else
	(void) smoothScroll;
	scrollbar->position = position;
#endif

	EsRectangle bounds = scrollbar->GetBounds();

	scrollbar->thumbPosition = LinearMap(0, scrollbar->contentSize - scrollbar->viewportSize, 
			0, (scrollbar->horizontal ? bounds.r : bounds.b) - scrollbar->thumbSize, position);

	if (scrollbar->thumbPosition != scrollbar->oldThumbPosition) {
		ScrollbarLayout(scrollbar);
	}

	if (sendMovedMessage && scrollbar->oldPosition != (int) scrollbar->position) {
		EsMessage m = { ES_MSG_SCROLLBAR_MOVED };
		m.scrollbarMoved.scroll = (int) position;
		m.scrollbarMoved.previous = previous;
		EsMessageSend(scrollbar, &m);
	}

	scrollbar->oldThumbPosition = scrollbar->thumbPosition;
	scrollbar->oldPosition = scrollbar->position;
}

int ProcessScrollbarButtonMessage(EsElement *element, EsMessage *message) {
	EsScrollbar *scrollbar = (EsScrollbar *) element->parent;

	if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
		element->state |= UI_STATE_STRONG_PRESSED;

#define UI_SCROLLBAR_AUTO_SPEED (10.0)
		scrollbar->autoScrollSpeed = scrollbar->viewportSize / UI_SCROLLBAR_AUTO_SPEED / (100 * 1000);

		if (scrollbar->up == element) {
			scrollbar->autoScrollSpeed *= -1;
		}

		element->StartAnimating();
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP) {
		element->state &= ~UI_STATE_STRONG_PRESSED;
		scrollbar->autoScrollSpeed = 0;
	} else if (message->type == ES_MSG_ANIMATE) {
		if (scrollbar->autoScrollSpeed) {
			ScrollbarSetPosition(scrollbar, scrollbar->autoScrollSpeed * message->animate.deltaUs + scrollbar->position, true, false);
			message->animate.waitUs = 0;
			message->animate.complete = false;
		} else {
			message->animate.complete = true;
		}
	} else {
		return 0;
	}

	return ES_HANDLED;
}

EsScrollbar *ScrollbarCreate(EsElement *parent, uint64_t flags) {
	EsScrollbar *scrollbar = (EsScrollbar *) EsHeapAllocate(sizeof(EsScrollbar), true);
	scrollbar->thumb = (EsElement *) EsHeapAllocate(sizeof(EsElement), true);

	if (flags & ES_SCROLLBAR_HORIZONTAL) {
		scrollbar->horizontal = true;
	}

	scrollbar->Initialise(parent, flags, [] (EsElement *element, EsMessage *message) {
		EsScrollbar *scrollbar = (EsScrollbar *) element;

		if (message->type == ES_MSG_LAYOUT) {
			ScrollbarLayout(scrollbar);
		} else if (message->type == ES_MSG_ANIMATE) {
			if (scrollbar->position != scrollbar->smoothScrollTarget) {
				double factor = EsCRTexp2f(-5000.0f / message->animate.deltaUs);
				// EsPrint("%dmcs -> %F\n", message->animate.deltaUs, factor);
				scrollbar->position += (scrollbar->smoothScrollTarget - scrollbar->position) * factor;
				ScrollbarSetPosition(scrollbar, scrollbar->smoothScrollTarget, true, true);
				bool done = scrollbar->position == scrollbar->smoothScrollTarget;
				message->animate.waitUs = 0, message->animate.complete = done;
			} else message->animate.complete = true;
		} else {
			return 0;
		}

		return ES_HANDLED;
	}, nullptr);

	scrollbar->cName = "scrollbar";

	scrollbar->up = EsButtonCreate(scrollbar, ES_CELL_FILL);
	scrollbar->up->userCallback = ProcessScrollbarButtonMessage;
	scrollbar->down = EsButtonCreate(scrollbar, ES_CELL_FILL);
	scrollbar->down->userCallback = ProcessScrollbarButtonMessage;

	scrollbar->thumb->Initialise(scrollbar, ES_CELL_FILL, [] (EsElement *element, EsMessage *message) {
		EsScrollbar *scrollbar = (EsScrollbar *) element->parent;
		EsRectangle bounds = scrollbar->GetBounds();

		if (message->type == ES_MSG_MOUSE_DRAGGED) {
			if (scrollbar->horizontal) {
				ScrollbarSetPosition(scrollbar, LinearMap(scrollbar->height, bounds.r - scrollbar->thumbSize - scrollbar->height, 0, scrollbar->contentSize - scrollbar->viewportSize,
							message->mouseDragged.newPositionX - message->mouseDragged.originalPositionX + scrollbar->originalThumbPosition), true, true);
			} else {
				ScrollbarSetPosition(scrollbar, LinearMap(scrollbar->width, bounds.b - scrollbar->thumbSize - scrollbar->width, 0, scrollbar->contentSize - scrollbar->viewportSize,
							message->mouseDragged.newPositionY - message->mouseDragged.originalPositionY + scrollbar->originalThumbPosition), true, true);
			}
		} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
			scrollbar->originalThumbPosition = scrollbar->thumbPosition;
		} else {
			return 0;
		}

		return ES_HANDLED;
	}, nullptr);

	scrollbar->thumb->cName = "scrollbar thumb";

	if (scrollbar->horizontal) {
		scrollbar->up->SetStylePart(ES_STYLE_PUSH_BUTTON_SCROLLBAR_LEFT);
		scrollbar->down->SetStylePart(ES_STYLE_PUSH_BUTTON_SCROLLBAR_RIGHT);
		scrollbar->thumb->SetStylePart(ES_STYLE_SCROLLBAR_THUMB_HORIZONTAL);
		scrollbar->SetStylePart(ES_STYLE_SCROLLBAR_BAR_HORIZONTAL);
	} else {
		scrollbar->up->SetStylePart(ES_STYLE_PUSH_BUTTON_SCROLLBAR_UP);
		scrollbar->down->SetStylePart(ES_STYLE_PUSH_BUTTON_SCROLLBAR_DOWN);
		scrollbar->thumb->SetStylePart(ES_STYLE_SCROLLBAR_THUMB_VERTICAL);
		scrollbar->SetStylePart(ES_STYLE_SCROLLBAR_BAR_VERTICAL);
	}

	scrollbar->up->flags &= ~ES_ELEMENT_FOCUSABLE;
	scrollbar->down->flags &= ~ES_ELEMENT_FOCUSABLE;

	return scrollbar;
}


void ScrollPane::Setup(EsElement *_parent, uint8_t _xMode, uint8_t _yMode, uint16_t _flags) {
	parent = _parent;
	xMode = _xMode;
	yMode = _yMode;
	flags = _flags;

	if (xMode == SCROLL_MODE_NONE) flags &= ~SCROLL_X_DRAG;
	if (yMode == SCROLL_MODE_NONE) flags &= ~SCROLL_Y_DRAG;

	if (xMode == SCROLL_MODE_FIXED || xMode == SCROLL_MODE_AUTO) {
		barX = ScrollbarCreate(parent, ES_CELL_FILL | ES_SCROLLBAR_HORIZONTAL | ES_ELEMENT_NON_CLIENT);
		barX->userData = this;

		barX->userCallback = [] (EsElement *element, EsMessage *message) {
			ScrollPane *pane = (ScrollPane *) element->userData.p;

			if (message->type == ES_MSG_SCROLLBAR_MOVED) {
				EsMessage m = *message;
				m.type = ES_MSG_SCROLL_X;
				pane->x = m.scrollbarMoved.scroll;
				EsMessageSend(pane->parent, &m);
			}

			return 0;
		};
	}

	if (yMode == SCROLL_MODE_FIXED || yMode == SCROLL_MODE_AUTO) {
		barY = ScrollbarCreate(parent, ES_CELL_FILL | ES_SCROLLBAR_VERTICAL | ES_ELEMENT_NON_CLIENT);
		barY->userData = this;

		barY->userCallback = [] (EsElement *element, EsMessage *message) {
			ScrollPane *pane = (ScrollPane *) element->userData.p;

			if (message->type == ES_MSG_SCROLLBAR_MOVED) {
				EsMessage m = *message;
				m.type = ES_MSG_SCROLL_Y;
				pane->y = m.scrollbarMoved.scroll;
				EsMessageSend(pane->parent, &m);
			}

			return 0;
		};
	}

	if (barX && barY) {
		pad = EsCustomElementCreate(parent, ES_CELL_FILL | ES_ELEMENT_NON_CLIENT, ES_STYLE_SCROLLBAR_PAD);
		pad->cName = "scrollbar pad";
	}
}

void ScrollPane::ReceivedMessage(EsMessage *message) {
	if (message->type == ES_MSG_LAYOUT) {
		Refresh();
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		if (flags & (SCROLL_X_DRAG | SCROLL_Y_DRAG)) {
			parent->StartAnimating();
			dragScrolling = true;
		}
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP || message->type == ES_MSG_MOUSE_RIGHT_UP || message->type == ES_MSG_MOUSE_MIDDLE_UP) {
		dragScrolling = false;
	} else if (message->type == ES_MSG_ANIMATE) {
		if (dragScrolling) {
			EsPoint point = EsMouseGetPosition(parent); 
			EsRectangle bounds = parent->GetBounds();
			double distanceX = point.x < bounds.l ? point.x - bounds.l : point.x >= bounds.r ? point.x - bounds.r + 1 : 0;
			double distanceY = point.y < bounds.t ? point.y - bounds.t : point.y >= bounds.b ? point.y - bounds.b + 1 : 0;
			double deltaX = message->animate.deltaUs * distanceX / 300000.0;
			double deltaY = message->animate.deltaUs * distanceY / 300000.0;
			if (deltaX && (flags & SCROLL_X_DRAG)) SetX(x + deltaX, true);
			if (deltaY && (flags & SCROLL_Y_DRAG)) SetY(y + deltaY, true);
			message->animate.complete = false;
		}
	}
}

void ScrollPane::SetX(double newScroll, bool sendMovedMessage) {
	if (xMode == SCROLL_MODE_NONE) return;
	if (newScroll < 0) newScroll = 0;
	else if (newScroll > xLimit) newScroll = xLimit;
	if (newScroll == x) return;
	double previous = x;
	x = newScroll;
	if (barX) ScrollbarSetPosition(barX, x, false, false);

	if (sendMovedMessage) {
		EsMessage m = {};
		m.type = ES_MSG_SCROLL_X;
		m.scrollbarMoved.scroll = x;
		m.scrollbarMoved.previous = previous;
		EsMessageSend(parent, &m);
	}
}

void ScrollPane::SetY(double newScroll, bool sendMovedMessage) {
	if (yMode == SCROLL_MODE_NONE) return;
	if (newScroll < 0) newScroll = 0;
	else if (newScroll > yLimit) newScroll = yLimit;
	if (newScroll == y) return;
	double previous = y;
	y = newScroll;
	if (barY) ScrollbarSetPosition(barY, y, false, false);

	if (sendMovedMessage) {
		EsMessage m = {};
		m.type = ES_MSG_SCROLL_Y;
		m.scrollbarMoved.scroll = y;
		m.scrollbarMoved.previous = previous;
		EsMessageSend(parent, &m);
	}
}

bool ScrollPane::RefreshXLimit(int64_t *contentWidth) {
	if (xMode != SCROLL_MODE_NONE) {
		EsRectangle bounds = parent->GetBounds();
		EsMessage m = {};
		m.type = ES_MSG_GET_WIDTH;
		m.measure.height = bounds.b;
		EsMessageSend(parent, &m);
		xLimit = m.measure.width - bounds.r + fixedViewportWidth;
		*contentWidth = m.measure.width;
		if (xLimit < 0) xLimit = 0;

		if (xMode == SCROLL_MODE_AUTO && xLimit > 0 && !parent->internalOffsetBottom) {
			parent->internalOffsetBottom = barX->currentStyle->preferredHeight;
			return true;
		}
	}

	return false;
}

bool ScrollPane::RefreshYLimit(int64_t *contentHeight) {
	if (yMode != SCROLL_MODE_NONE) {
		EsRectangle bounds = parent->GetBounds();
		EsMessage m = {};
		m.type = ES_MSG_GET_HEIGHT;
		m.measure.width = bounds.r;
		EsMessageSend(parent, &m);
		// EsPrint("ScrollPane::RefreshYLimit - %z, %R, %d\n", parent->cName, bounds, m.measure.height);
		yLimit = m.measure.height - bounds.b + fixedViewportHeight;
		*contentHeight = m.measure.height;
		if (yLimit < 0) yLimit = 0;

		if (yMode == SCROLL_MODE_AUTO && yLimit > 0 && !parent->internalOffsetRight) {
			parent->internalOffsetRight = barY->currentStyle->preferredWidth;
			return true;
		}
	}

	return false;
}

void ScrollPane::Refresh() {
	parent->internalOffsetRight = yMode == SCROLL_MODE_FIXED ? barY->currentStyle->preferredWidth : 0;
	parent->internalOffsetBottom = xMode == SCROLL_MODE_FIXED ? barX->currentStyle->preferredHeight : 0;

	int64_t contentWidth = 0, contentHeight = 0;

	bool recalculateLimits1 = RefreshXLimit(&contentWidth);
	bool recalculateLimits2 = RefreshYLimit(&contentHeight);

	if (recalculateLimits1 || recalculateLimits2) {
		RefreshXLimit(&contentWidth);
		RefreshYLimit(&contentHeight);
	}

	EsRectangle bounds = parent->GetBounds();

	if (barX) ScrollbarSetMeasurements(barX, bounds.r - fixedViewportWidth, contentWidth);
	if (barY) ScrollbarSetMeasurements(barY, bounds.b - fixedViewportHeight, contentHeight);

	SetX(x, true);
	SetY(y, true);

	if (barX) {
		barX->InternalMove(parent->width - parent->internalOffsetRight, barX->currentStyle->preferredHeight, 
				0, parent->height - parent->internalOffsetBottom);
	}

	if (barY) {
		barY->InternalMove(barY->currentStyle->preferredWidth, parent->height - parent->internalOffsetBottom, 
				parent->width - parent->internalOffsetRight, 0);
	}

	if (pad) {
		pad->InternalMove(parent->internalOffsetRight, parent->internalOffsetBottom, 
				parent->width - parent->internalOffsetRight, parent->height - parent->internalOffsetBottom);
	}
}

// --------------------------------- Panels.

void SwitcherTransitionComplete(EsPanel *panel) {
	if (panel->switchedFrom) {
		if (panel->destroyPreviousAfterTransitionCompletes) {
			panel->switchedFrom->Destroy();
		} else {
			EsElementSetHidden(panel->switchedFrom, true);
		}

		panel->switchedFrom = nullptr;
	}

	panel->transitionType = ES_TRANSITION_NONE;
}

int ProcessPanelMessage(EsElement *element, EsMessage *message) {
	EsPanel *panel = (EsPanel *) element;
	EsRectangle bounds = panel->GetBounds();

	panel->scroll.ReceivedMessage(message);

	if (message->type == ES_MSG_LAYOUT) {
		if (panel->flags & ES_PANEL_TABLE) {
			LayoutTable(panel, message);
		} else if (panel->flags & ES_PANEL_SWITCHER) {
			EsRectangle insets = panel->GetInsets();

			if (panel->switchedFrom) {
				panel->switchedFrom->InternalMove(bounds.r - bounds.l - insets.r - insets.l, 
						bounds.b - bounds.t - insets.b - insets.t, 
						bounds.l + insets.l, bounds.t + insets.t);
			}

			if (panel->switchedTo) {
				panel->switchedTo->InternalMove(bounds.r - bounds.l - insets.r - insets.l, 
						bounds.b - bounds.t - insets.b - insets.t, 
						bounds.l + insets.l, bounds.t + insets.t);
			}
		} else if (panel->flags & ES_PANEL_Z_STACK) {
			EsRectangle insets = panel->GetInsets();

			for (uintptr_t i = 0; i < element->GetChildCount(); i++) {
				EsElement *child = element->GetChild(i);
				if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;
				child->InternalMove(bounds.r - bounds.l - insets.r - insets.l, 
						bounds.b - bounds.t - insets.b - insets.t, 
						bounds.l + insets.l, bounds.t + insets.t);
			}
		} else {
			LayoutStack(panel, message);
		}
	} else if (message->type == ES_MSG_PAINT_CHILDREN) {
		if ((panel->flags & ES_PANEL_SWITCHER) && panel->transitionType != ES_TRANSITION_NONE) {
			double progress = SmoothAnimationTimeSharp((double) panel->transitionTimeMcs / (double) panel->transitionLengthMcs);
			EsRectangle bounds = EsPainterBoundsClient(message->painter);
			int width = Width(bounds), height = Height(bounds);
			EsPaintTarget target;

			if (EsPaintTargetTake(&target, width, height)) {
				EsPainter painter = { .clip = ES_MAKE_RECTANGLE(0, width, 0, height), .width = width, .height = height, .target = &target };

				// TODO 'Clip'-style transitions. ES_TRANSITION_REVEAL_UP/ES_TRANSITION_REVEAL_DOWN.

				if (panel->switchedFrom) {
					panel->switchedFrom->InternalPaint(&painter, PAINT_SHADOW);
					panel->switchedFrom->InternalPaint(&painter, ES_FLAGS_DEFAULT);
					DrawTransitionEffect(message->painter, &target, bounds, (EsTransitionType) panel->transitionType, progress, false);
					EsPaintTargetClear(&target); 
				}

				if (panel->switchedTo) {
					panel->switchedTo->InternalPaint(&painter, PAINT_SHADOW);
					panel->switchedTo->InternalPaint(&painter, ES_FLAGS_DEFAULT);
					DrawTransitionEffect(message->painter, &target, bounds, (EsTransitionType) panel->transitionType, progress, true);
				}

				EsPaintTargetReturn(&target);
			} else {
				// Not enough memory to get a paint target.
				return 0;
			}
		} else {
			return 0;
		}
	} else if (message->type == ES_MSG_GET_WIDTH) {
		if (!panel->measurementCache.Get(message, &panel->state)) {
			if (panel->flags & ES_PANEL_TABLE) {
				LayoutTable(panel, message);
			} else if (panel->flags & (ES_PANEL_Z_STACK | ES_PANEL_SWITCHER)) {
				int maximum = 0;

				for (uintptr_t i = 0; i < element->GetChildCount(); i++) {
					EsElement *child = element->GetChild(i);
					if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;
					int size = child->GetWidth(message->measure.height);
					if (size > maximum) maximum = size;
				}

				message->measure.width = maximum + panel->GetInsetWidth();
			} else {
				LayoutStack(panel, message);
			}

			panel->measurementCache.Store(message);
		}
	} else if (message->type == ES_MSG_GET_HEIGHT) {
		if (!panel->measurementCache.Get(message, &panel->state)) {
			if (panel->flags & ES_PANEL_TABLE) {
				LayoutTable(panel, message);
			} else if (panel->flags & (ES_PANEL_Z_STACK | ES_PANEL_SWITCHER)) {
				int maximum = 0;

				for (uintptr_t i = 0; i < element->GetChildCount(); i++) {
					EsElement *child = element->GetChild(i);
					if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;
					int size = child->GetHeight(message->measure.width);
					if (size > maximum) maximum = size;
				}

				message->measure.height = maximum + panel->GetInsetHeight();
			} else {
				LayoutStack(panel, message);
			}
			panel->measurementCache.Store(message);
		}
	} else if (message->type == ES_MSG_ENSURE_VISIBLE) {
		EsElement *child = message->ensureVisible.child, *e = child;
		int offsetX = panel->scroll.x, offsetY = panel->scroll.y;
		while (e != element) offsetX += e->offsetX, offsetY += e->offsetY, e = e->parent;
		EsRectangle bounds = panel->GetBounds();
		panel->scroll.SetX(offsetX + child->width / 2 - bounds.r / 2);
		panel->scroll.SetY(offsetY + child->height / 2 - bounds.b / 2);
	} else if (message->type == ES_MSG_PRE_ADD_CHILD) {
		if (!panel->addingSeparator && panel->separatorStylePart && panel->GetChildCount()) {
			panel->addingSeparator = true;
			EsCustomElementCreate(panel, panel->separatorFlags, panel->separatorStylePart)->cName = "panel separator";
			panel->addingSeparator = false;
		}
	} else if (message->type == ES_MSG_ADD_CHILD) {
		if (panel->flags & ES_PANEL_TABLE) {
			uintptr_t index = panel->tableIndex++;
			TableCell cell = {};

			if (panel->flags & ES_PANEL_HORIZONTAL) {
				cell.from[0] = cell.to[0] = index % panel->bandCount[0];
				cell.from[1] = cell.to[1] = index / panel->bandCount[0];

				if (panel->bandCount[1] <= cell.from[1] && !panel->bands[1]) {
					panel->bandCount[1] = cell.from[1] + 1;
				}
			} else {
				cell.from[0] = cell.to[0] = index / panel->bandCount[1];
				cell.from[1] = cell.to[1] = index % panel->bandCount[1];

				if (panel->bandCount[0] <= cell.from[0] && !panel->bands[0]) {
					panel->bandCount[0] = cell.from[0] + 1;
				}
			}

			EsElement *child = (EsElement *) message->child;
			child->tableCell = cell;
		} else if (panel->flags & ES_PANEL_SWITCHER) {
			EsElement *child = (EsElement *) message->child;
			child->state |= UI_STATE_BLOCK_INTERACTION;
		}
	} else if (message->type == ES_MSG_SCROLL_X || message->type == ES_MSG_SCROLL_Y) {
		int delta = message->scrollbarMoved.scroll - message->scrollbarMoved.previous;
		int deltaX = message->type == ES_MSG_SCROLL_X ? delta : 0; 
		int deltaY = message->type == ES_MSG_SCROLL_Y ? delta : 0; 

		for (uintptr_t i = 0; i < panel->GetChildCount(); i++) {
			EsElement *child = panel->GetChild(i);
			if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) continue;
			child->InternalMove(child->width, child->height, child->offsetX - deltaX, child->offsetY - deltaY);
		}
	} else if (message->type == ES_MSG_ANIMATE) {
		if (panel->flags & ES_PANEL_SWITCHER) {
			panel->transitionTimeMcs += message->animate.deltaUs;
			message->animate.complete = panel->transitionTimeMcs >= panel->transitionLengthMcs;
			panel->Repaint(true);

			if (message->animate.complete) {
				SwitcherTransitionComplete(panel);
			}
		} else {
			// Why are we animating..?
			return 0;
		}
	} else if (message->type == ES_MSG_DESTROY_CONTENTS) {
		if ((panel->flags & ES_PANEL_TABLE)) {
			panel->tableIndex = 0;
		}
	} else if (message->type == ES_MSG_KEY_DOWN) {
		if (!(panel->flags & (ES_PANEL_TABLE | ES_PANEL_SWITCHER))
				&& panel->window->focused && panel->window->focused->parent == panel
				&& (panel->flags & ES_PANEL_HORIZONTAL)) {
			bool reverse = panel->flags & ES_PANEL_REVERSE,
			     left = message->keyboard.scancode == ES_SCANCODE_LEFT_ARROW,
			     right = message->keyboard.scancode == ES_SCANCODE_RIGHT_ARROW;

			if ((left && !reverse) || (right && reverse)) {
				EsElement *focus = nullptr;

				for (uintptr_t i = 0; i < panel->GetChildCount(); i++) {
					EsElement *child = panel->GetChild(i);

					if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) {
						continue;
					}

					if (child == panel->window->focused) {
						break;
					} else if (child->IsFocusable()) {
						focus = child;
					}
				}

				if (focus) {
					EsElementFocus(focus);
				}
			} else if ((left && reverse) || (right && !reverse)) {
				EsElement *focus = nullptr;

				for (uintptr_t i = panel->GetChildCount(); i > 0; i--) {
					EsElement *child = panel->GetChild(i - 1);

					if (child->flags & (ES_ELEMENT_HIDDEN | ES_ELEMENT_NON_CLIENT)) {
						continue;
					}

					if (child == panel->window->focused) {
						break;
					} else if (child->IsFocusable()) {
						focus = child;
					}
				}

				if (focus) {
					EsElementFocus(focus);
				}
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	} else if (message->type == ES_MSG_GET_INSPECTOR_INFORMATION) {
		char *buffer = message->getContent.buffer;
		size_t bufferSpace = message->getContent.bufferSpace;

		if (panel->flags & ES_PANEL_Z_STACK) {
			message->getContent.bytes = EsStringFormat(buffer, bufferSpace, "z-stack");
		} else if (panel->flags & ES_PANEL_SWITCHER) {
			message->getContent.bytes = EsStringFormat(buffer, bufferSpace, "switcher");
		} else if (panel->flags & ES_PANEL_TABLE) {
			message->getContent.bytes = EsStringFormat(buffer, bufferSpace, "table");
		} else {
			message->getContent.bytes = EsStringFormat(buffer, bufferSpace, "%z%z stack", 
					(panel->flags & ES_PANEL_REVERSE) ? "reverse " : "",
					(panel->flags & ES_PANEL_HORIZONTAL) ? "horizontal" : "vertical");
		}
	} else if (message->type == ES_MSG_BEFORE_Z_ORDER) {
		bool isStack = !(panel->flags & (ES_PANEL_TABLE | ES_PANEL_SWITCHER | ES_PANEL_Z_STACK));

		if (isStack && arrlenu(panel->children) > 100) {
			// Count the number of client children.

			size_t childCount = arrlenu(panel->children);

			while (childCount) {
				if (panel->children[childCount - 1]->flags & ES_ELEMENT_NON_CLIENT) {
					childCount--;
				} else {
					break;
				}
			}

			if (childCount < 100) {
				return 0;
			}

			message->beforeZOrder.nonClient = childCount;

			// Binary search for an early visible child.

			bool found = false;
			uintptr_t position = 0;

			if (panel->flags & ES_PANEL_HORIZONTAL) {
				ES_MACRO_SEARCH(childCount, result = message->beforeZOrder.clip.l - panel->children[index]->offsetX;, position, found);
			} else {
				ES_MACRO_SEARCH(childCount, result = message->beforeZOrder.clip.t - panel->children[index]->offsetY;, position, found);
			}

			// Search back until we find the first.
			// Assumption: children with paint outsets do not extend beyond the next child.

			while (position) {
				if (position < childCount) {
					EsElement *child = panel->children[position];

					if (panel->flags & ES_PANEL_HORIZONTAL) {
						if (child->offsetX + child->width + child->currentStyle->paintOutsets.r < message->beforeZOrder.clip.l) {
							break;
						}
					} else {
						if (child->offsetY + child->height + child->currentStyle->paintOutsets.b < message->beforeZOrder.clip.t) {
							break;
						}
					}
				}

				position--;
			}

			message->beforeZOrder.start = position;

			// Search forward until we find the last visible child.

			while (position < childCount) {
				EsElement *child = panel->children[position];

				if (panel->flags & ES_PANEL_HORIZONTAL) {
					if (child->offsetX - child->currentStyle->paintOutsets.l > message->beforeZOrder.clip.r) {
						break;
					}
				} else {
					if (child->offsetY - child->currentStyle->paintOutsets.t > message->beforeZOrder.clip.b) {
						break;
					}
				}

				position++;
			}

			message->beforeZOrder.end = position;
		}
	} else {
		return 0;
	}

	return ES_HANDLED;
}

EsPanel *EsPanelCreate(EsElement *parent, uint64_t flags, const EsStyle *style) {
	EsPanel *panel = (EsPanel *) EsHeapAllocate(sizeof(EsPanel), true);

	panel->Initialise(parent, flags, ProcessPanelMessage, style);
	panel->cName = "panel";

	if (flags & ES_PANEL_Z_STACK)    panel->state |= UI_STATE_Z_STACK;
	if (flags & ES_PANEL_HORIZONTAL) panel->flags |= ES_ELEMENT_LAYOUT_HINT_HORIZONTAL;
	if (flags & ES_PANEL_REVERSE)    panel->flags |= ES_ELEMENT_LAYOUT_HINT_REVERSE;

	panel->scroll.Setup(panel, 
			((flags & ES_PANEL_H_SCROLL_FIXED) ? SCROLL_MODE_FIXED : (flags & ES_PANEL_H_SCROLL_AUTO) ? SCROLL_MODE_AUTO : SCROLL_MODE_NONE),
			((flags & ES_PANEL_V_SCROLL_FIXED) ? SCROLL_MODE_FIXED : (flags & ES_PANEL_V_SCROLL_AUTO) ? SCROLL_MODE_AUTO : SCROLL_MODE_NONE),
			ES_FLAGS_DEFAULT);

	return panel;
}

struct EsSpacer : EsElement {
	int width, height;
};

int ProcessSpacerMessage(EsElement *element, EsMessage *message) {
	EsSpacer *spacer = (EsSpacer *) element;

	if (message->type == ES_MSG_GET_WIDTH) {
		message->measure.width = spacer->width * spacer->currentStyle->scale;
	} else if (message->type == ES_MSG_GET_HEIGHT) {
		message->measure.height = spacer->height * spacer->currentStyle->scale;
	}

	return 0;
}

EsElement *EsSpacerCreate(EsElement *panel, uint64_t flags, const EsStyle *style, int width, int height) {
	EsSpacer *spacer = (EsSpacer *) EsHeapAllocate(sizeof(EsSpacer), true);
	spacer->Initialise(panel, flags, ProcessSpacerMessage, style);
	spacer->cName = "spacer";
	spacer->width = width == -1 ? 4 : width;
	spacer->height = height == -1 ? 4 : height;
	return spacer;
}

EsElement *EsCustomElementCreate(EsElement *parent, uint64_t flags, const EsStyle *style) {
	EsElement *element = (EsElement *) EsHeapAllocate(sizeof(EsElement), true);
	element->Initialise(parent, flags, nullptr, style);
	element->cName = "custom element";
	return element;
}

void EsElementSetCellRange(EsElement *element, int xFrom, int yFrom, int xTo, int yTo) {
	EsMessageMutexCheck();

	if (xFrom == -1) xFrom = element->tableCell.from[0];
	if (yFrom == -1) yFrom = element->tableCell.from[1];
	if (xTo == -1) xTo = xFrom;
	if (yTo == -1) yTo = yFrom;

	EsPanel *panel = (EsPanel *) element->parent;
	EsAssert(panel->classCallback == ProcessPanelMessage && panel->flags & ES_PANEL_TABLE); // Invalid parent for SetCellRange.

	TableCell cell = {};
	cell.from[0] = xFrom, cell.from[1] = yFrom;
	cell.to[0] = xTo, cell.to[1] = yTo;
	element->tableCell = cell;
}

void EsPanelSetBands(EsPanel *panel, size_t columnCount, size_t rowCount, EsPanelBand *columns, EsPanelBand *rows) {
	EsMessageMutexCheck();
	EsAssert(panel->flags & ES_PANEL_TABLE); // Cannot set the bands layout for a non-table panel.

	panel->bandCount[0] = columnCount, panel->bandCount[1] = rowCount;
	panel->bands[0] = columns, panel->bands[1] = rows;
}

void RemoveFocusFromElement(EsElement *oldFocus) {
	if (!oldFocus) return;
	EsMessage m = {};

	if (~oldFocus->state & UI_STATE_LOST_STRONG_FOCUS) {
		m.type = ES_MSG_STRONG_FOCUS_END;
		oldFocus->state |= UI_STATE_LOST_STRONG_FOCUS;
		EsMessageSend(oldFocus, &m);
	}

	m.type = ES_MSG_FOCUSED_END;
	oldFocus->state &= ~(UI_STATE_FOCUSED | UI_STATE_LOST_STRONG_FOCUS);
	EsMessageSend(oldFocus, &m);
}

void MaybeRemoveFocusedElement(EsWindow *window) {
	if (window->focused && !window->focused->IsFocusable()) {
		EsElement *oldFocus = window->focused;
		window->focused = nullptr;
		RemoveFocusFromElement(oldFocus);
	}
}

void EsPanelSwitchTo(EsPanel *panel, EsElement *targetChild, EsTransitionType transitionType, uint32_t flags, uint32_t timeMs) {
	EsMessageMutexCheck();
	EsAssert(panel->flags & ES_PANEL_SWITCHER); // Cannot switch element for a non-switcher panel.

	if (targetChild == panel->switchedTo) {
		return;
	}

	if (panel->switchedFrom) {
		// We're interrupting the previous transition.
		SwitcherTransitionComplete(panel);
	}

	panel->transitionType = transitionType;
	panel->transitionTimeMcs = 0;
	panel->transitionLengthMcs = timeMs * ANIMATION_SPEED; 
	panel->switchedFrom = panel->switchedTo;
	panel->switchedTo = targetChild;
	panel->destroyPreviousAfterTransitionCompletes = flags & ES_PANEL_SWITCHER_DESTROY_PREVIOUS_AFTER_TRANSITION;

	if (panel->switchedTo) {
		EsElementSetHidden(panel->switchedTo, false);
		panel->switchedTo->state &= ~UI_STATE_BLOCK_INTERACTION;
		panel->switchedTo->BringToFront();
	}

	if (panel->switchedFrom) {
		panel->switchedFrom->state |= UI_STATE_BLOCK_INTERACTION;
		MaybeRemoveFocusedElement(panel->window);
	}

	if (transitionType == ES_TRANSITION_NONE || panel->switchedFrom == panel->switchedTo || !panel->transitionLengthMcs) {
		SwitcherTransitionComplete(panel);
	} else {
		panel->StartAnimating();
	}

	EsElementRelayout(panel);
}

// --------------------------------- Text displays and textboxes.

#define TEXT_ELEMENTS
#include "text.cpp"
#undef TEXT_ELEMENTS

// --------------------------------- Buttons.

int ProcessButtonMessage(EsElement *element, EsMessage *message) {
	EsButton *button = (EsButton *) element;

	if (message->type == ES_MSG_PAINT) {
		((UIStyle *) message->painter->style)->PaintText(message->painter, message->painter->width, message->painter->height, button->label, button->labelBytes, 
			0, 0, button->iconID, (button->flags & ES_BUTTON_DROPDOWN) ? MARKER_DROPDOWN : 0);
	} else if (message->type == ES_MSG_GET_WIDTH) {
		if (!button->measurementCache.Get(message, &button->state)) {
			int stringWidth = button->currentStyle->MeasureTextWidth(button->label, button->labelBytes);
			int iconWidth = button->iconID ? button->currentStyle->metrics->iconSize : 0;
			int contentWidth = stringWidth + iconWidth + ((stringWidth && iconWidth) ? button->currentStyle->gapMinor : 0)
				+ button->currentStyle->insets.l + button->currentStyle->insets.r;

			if (button->flags & ES_BUTTON_DROPDOWN) {
				int64_t width = 0;
				GetPreferredSizeFromStylePart(ES_STYLE_MARKER_DROPDOWN, &width, nullptr);
				contentWidth += width + button->currentStyle->gapMinor;
			}

			int minimumReportedWidth = GetConstantNumber("pushButtonMinimumReportedWidth");
			if (button->flags & ES_BUTTON_MENU_ITEM) minimumReportedWidth = GetConstantNumber("menuItemMinimumReportedWidth");
			if (!stringWidth || (button->flags & ES_BUTTON_COMPACT)) minimumReportedWidth = 0;

			message->measure.width = minimumReportedWidth > contentWidth ? minimumReportedWidth : contentWidth;

			button->measurementCache.Store(message);
		}
	} else if (message->type == ES_MSG_DESTROY) {
		EsHeapFree(button->label);

		if (button->command) {
			bool found = false;

			for (uintptr_t i = 0; i < arrlenu(button->command->elements); i++) {
				if (button->command->elements[i] == button) {
					arrdelswap(button->command->elements, i);
					found = true;
					break;
				}
			}

			EsAssert(found); // Button incorrectly attached to command.
		}
	} else if (message->type == ES_MSG_CLICKED) {
		if (button->flags & ES_BUTTON_CHECKBOX) {
			button->customStyleState &= ~THEME_STATE_INDETERMINATE;
			button->customStyleState ^= THEME_STATE_CHECKED;
		} else if (button->flags & ES_BUTTON_RADIOBOX) {
			button->customStyleState |= THEME_STATE_CHECKED;

			EsMessage m = { ES_MSG_RADIO_GROUP_UPDATED };

			for (uintptr_t i = 0; i < button->parent->GetChildCount(); i++) {
				if (button->parent->GetChild(i) != button) {
					EsMessageSend(button->parent->GetChild(i), &m);
				}
			}
		}

		if (button->checkBuddy) {
			EsElementSetDisabled(button->checkBuddy, !(button->customStyleState & (THEME_STATE_CHECKED | THEME_STATE_INDETERMINATE)));
		}

		if (button->onCommand) {
			button->onCommand(button->instance, button, button->command);
		}

		if (button->flags & ES_BUTTON_MENU_ITEM) {
			button->window->Destroy();
		}
	} else if (message->type == ES_MSG_RADIO_GROUP_UPDATED && (button->flags & ES_BUTTON_RADIOBOX)) {
		EsButtonSetCheck(button, ES_CHECK_UNCHECKED);
	} else if (message->type == ES_MSG_FOCUSED_START) {
		if (button->window->defaultEnterButton && (button->flags & ES_BUTTON_PUSH)) {
			button->window->enterButton->customStyleState &= ~THEME_STATE_DEFAULT_BUTTON;
			button->window->enterButton->MaybeRefreshStyle();
			button->customStyleState |= THEME_STATE_DEFAULT_BUTTON;
			button->window->enterButton = button;
		}
	} else if (message->type == ES_MSG_FOCUSED_END) {
		if (button->window->enterButton == button) {
			button->customStyleState &= ~THEME_STATE_DEFAULT_BUTTON;
			button->window->enterButton = button->window->defaultEnterButton;
			button->window->enterButton->customStyleState |= THEME_STATE_DEFAULT_BUTTON;
			button->window->enterButton->MaybeRefreshStyle();
		}
	} else if (message->type == ES_MSG_GET_INSPECTOR_INFORMATION) {
		message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace,
				"'%s'", button->labelBytes, button->label);
	} else {
		return 0;
	}

	return ES_HANDLED;
}

EsButton *EsButtonCreate(EsElement *parent, uint64_t flags, const EsStyle *style, const char *label, ptrdiff_t labelBytes) {
	EsStyle *style2;

	if (flags & ES_BUTTON_MENU_ITEM) {
		if (flags & ES_MENU_ITEM_HEADER) {
			style2 = ES_STYLE_MENU_ITEM_HEADER;
		} else {
			style2 = ES_STYLE_MENU_ITEM_NORMAL;
		}
	} else if (flags & ES_BUTTON_TOOLBAR) {
		style2 = ES_STYLE_PUSH_BUTTON_TOOLBAR;
		flags |= ES_BUTTON_COMPACT | ES_BUTTON_NOT_FOCUSABLE;
	} else if (flags & ES_BUTTON_CHECKBOX) {
		style2 = ES_STYLE_CHECKBOX_NORMAL;
		flags |= ES_BUTTON_COMPACT;
	} else if (flags & ES_BUTTON_RADIOBOX) {
		style2 = ES_STYLE_CHECKBOX_RADIOBOX;
		flags |= ES_BUTTON_COMPACT;
	} else if (flags & ES_BUTTON_DEFAULT) {
		style2 = ES_STYLE_PUSH_BUTTON_NORMAL;
		flags |= ES_BUTTON_PUSH;
	} else {
		style2 = ES_STYLE_PUSH_BUTTON_NORMAL;
		flags |= ES_BUTTON_PUSH;
	}

	if (~flags & ES_BUTTON_NOT_FOCUSABLE) flags |= ES_ELEMENT_FOCUSABLE;

	EsButton *button = (EsButton *) EsHeapAllocate(sizeof(EsButton), true);
	button->Initialise(parent, flags, ProcessButtonMessage, style ?: style2);
	button->cName = "button";

	if (flags & ES_BUTTON_DEFAULT) {
		button->window->defaultEnterButton = button;
		button->window->enterButton = button;
		button->customStyleState |= THEME_STATE_DEFAULT_BUTTON;
	} else if (flags & ES_BUTTON_CANCEL) {
		button->window->escapeButton = button;
	}

	if (labelBytes == -1) labelBytes = EsCStringLength(label);
	HeapDuplicate((void **) &button->label, label, labelBytes);
	button->labelBytes = labelBytes;

	if ((flags & ES_BUTTON_MENU_ITEM) && (flags & ES_MENU_ITEM_HEADER)) {
		EsElementSetDisabled(button, true);
	}

	return button;
}

void EsButtonSetIcon(EsButton *button, uint32_t iconID) {
	EsMessageMutexCheck();

	button->iconID = iconID;
	button->Repaint(true);
}

void EsButtonOnCommand(EsButton *button, EsCommandCallbackFunction onCommand, EsCommand *command) {
	EsMessageMutexCheck();

	button->onCommand = onCommand;
	button->command = command;
}

void EsButtonSetCheckBuddy(EsButton *button, EsElement *checkBuddy) {
	EsMessageMutexCheck();

	button->checkBuddy = checkBuddy;
	EsElementSetDisabled(button->checkBuddy, !(button->customStyleState & (THEME_STATE_CHECKED | THEME_STATE_INDETERMINATE)));
}

EsElement *EsButtonGetCheckBuddy(EsButton *button) {
	EsMessageMutexCheck();

	return button->checkBuddy;
}

EsCheckState EsButtonGetCheck(EsButton *button) {
	EsMessageMutexCheck();

	if (button->customStyleState & THEME_STATE_CHECKED)       return ES_CHECK_CHECKED;
	if (button->customStyleState & THEME_STATE_INDETERMINATE) return ES_CHECK_INDETERMINATE;
	return ES_CHECK_UNCHECKED;
}

void EsButtonSetCheck(EsButton *button, EsCheckState checkState, bool sendUpdatedMessage) {
	EsMessageMutexCheck();

	button->customStyleState &= ~(THEME_STATE_CHECKED | THEME_STATE_INDETERMINATE);

	if (checkState == ES_CHECK_CHECKED)       button->customStyleState |= THEME_STATE_CHECKED;
	if (checkState == ES_CHECK_INDETERMINATE) button->customStyleState |= THEME_STATE_INDETERMINATE;

	if (sendUpdatedMessage) {
		EsMessage m = { ES_MSG_CHECK_UPDATED };
		m.checkState = checkState;
		EsMessageSend(button, &m);

		if (button->onCommand) {
			button->onCommand(button->instance, button, button->command);
		}
	}

	if (button->checkBuddy) {
		EsElementSetDisabled(button->checkBuddy, !(button->customStyleState & (THEME_STATE_CHECKED | THEME_STATE_INDETERMINATE)));
	}

	button->MaybeRefreshStyle();
}

void EsMenuAddItem(EsMenu *menu, uint64_t flags, const char *label, ptrdiff_t labelBytes, EsMenuCallbackFunction callback, EsGeneric context) {
	EsButton *button = (EsButton *) EsButtonCreate(menu, 
		ES_BUTTON_NOT_FOCUSABLE | ES_BUTTON_MENU_ITEM | ES_CELL_H_FILL | flags, 0,
		label, labelBytes != -1 ? labelBytes : EsCStringLength(label));
	button->userData = (void *) callback;

	button->userCallback = [] (EsElement *element, EsMessage *message) {
		if (message->type == ES_MSG_CLICKED) {
			EsMenuCallbackFunction callback = (EsMenuCallbackFunction) element->userData.p;
			if (callback) callback((EsMenu *) element->window, ((EsButton *) element)->menuItemContext);
		}

		return 0;
	};

	button->menuItemContext = context;
}

void EsMenuAddCommand(EsMenu *menu, uint64_t flags, const char *label, ptrdiff_t labelBytes, EsCommand *command) {
	EsButton *button = (EsButton *) EsButtonCreate(menu, 
			ES_BUTTON_NOT_FOCUSABLE | ES_BUTTON_MENU_ITEM | ES_CELL_H_FILL | flags, 
			0, label, labelBytes);
	EsCommandAddButton(command, button);
}

// --------------------------------- Color wells and pickers.

struct EsColorWell : EsElement {
	uint32_t color;
	struct ColorPicker *picker;
	bool indeterminate;
};

int ProcessColorWellMessage(EsElement *element, EsMessage *message);

struct ColorPicker {
	uint32_t color;
	float hue, saturation, value, opacity;
	float dragStartHue, dragStartSaturation;
	int dragComponent;
	bool indeterminateBeforeEyedrop;
	bool modified;

	ColorPickerHost host;
	EsPanel *panel;
	EsElement *circle, *slider, *circlePoint, *sliderPoint, *opacitySlider, *opacitySliderPoint;
	EsTextbox *textbox;

	uint32_t GetColorForHost() {
		return color | (host.hasOpacity ? ((uint32_t) (255.0f * opacity) << 24) : 0);
	}

	void Sync(EsElement *excluding) {
		if (excluding != textbox && textbox) {
			char string[16];
			size_t length;

			if (host.indeterminate && *host.indeterminate) {
				string[0] = '#';
				length = 1;
			} else {
				const char *hexChars = "0123456789ABCDEF";

				if (host.hasOpacity) {
					uint8_t alpha = (uint8_t) (opacity * 0xFF);
					length = EsStringFormat(string, sizeof(string), "#%c%c%c%c%c%c%c%c", 
							hexChars[(alpha >> 4) & 0xF], hexChars[(alpha >> 0) & 0xF], hexChars[(color >> 20) & 0xF], hexChars[(color >> 16) & 0xF], 
							hexChars[(color >> 12) & 0xF], hexChars[(color >> 8) & 0xF], hexChars[(color >> 4) & 0xF], hexChars[(color >> 0) & 0xF]);
				} else {
					length = EsStringFormat(string, sizeof(string), "#%c%c%c%c%c%c", 
							hexChars[(color >> 20) & 0xF], hexChars[(color >> 16) & 0xF], hexChars[(color >> 12) & 0xF], 
							hexChars[(color >> 8) & 0xF], hexChars[(color >> 4) & 0xF], hexChars[(color >> 0) & 0xF]);
				}
			}

			EsTextboxSelectAll(textbox);
			EsTextboxInsert(textbox, string, length, false);
		}

		if (excluding != circle) circle->Repaint(true);
		if (excluding != slider) slider->Repaint(true);
		if (excluding != opacitySlider && opacitySlider) opacitySlider->Repaint(true);

		if (excluding != circlePoint) {
			if (host.indeterminate && *host.indeterminate) {
				EsElementSetHidden(circlePoint, true);
			} else {
				EsElementSetHidden(circlePoint, false);
				float x = saturation * EsCRTcosf((hue - 3) * 1.047197551) * 0.5f + 0.5f;
				float y = saturation * EsCRTsinf((hue - 3) * 1.047197551) * 0.5f + 0.5f;
				EsRectangle pointSize = EsElementGetPreferredSize(circlePoint), circleSize = EsElementGetPreferredSize(circle);
				int x2 = x * circleSize.r - pointSize.r / 2, y2 = y * circleSize.b - pointSize.b / 2;
				EsElementMove(circlePoint, x2, y2, pointSize.r, pointSize.b);
				circlePoint->Repaint(true);
			}
		}

		if (excluding != sliderPoint) {
			if (host.indeterminate && *host.indeterminate) {
				EsElementSetHidden(sliderPoint, true);
			} else {
				EsElementSetHidden(sliderPoint, false);
				float x = 0.5f, y = 1.0f - EsCRTpowf(value, 1.333f);
				EsRectangle pointSize = EsElementGetPreferredSize(sliderPoint), sliderSize = EsElementGetPreferredSize(slider);
				int x2 = x * sliderSize.r - pointSize.r / 2, y2 = y * sliderSize.b - pointSize.b / 2;
				EsElementMove(sliderPoint, x2, y2, pointSize.r, pointSize.b);
				sliderPoint->Repaint(true);
			}
		}

		if (excluding != opacitySliderPoint && opacitySliderPoint) {
			if (host.indeterminate && *host.indeterminate) {
				EsElementSetHidden(opacitySliderPoint, true);
			} else {
				EsElementSetHidden(opacitySliderPoint, false);
				float x = 0.5f, y = 1.0f - opacity;
				EsRectangle pointSize = EsElementGetPreferredSize(opacitySliderPoint), sliderSize = EsElementGetPreferredSize(opacitySlider);
				int x2 = x * sliderSize.r - pointSize.r / 2, y2 = y * sliderSize.b - pointSize.b / 2;
				EsElementMove(opacitySliderPoint, x2, y2, pointSize.r, pointSize.b);
				opacitySliderPoint->Repaint(true);
			}
		}

		if (excluding != host.well && host.well) {
			if (host.well->classCallback == ProcessColorWellMessage) {
				((EsColorWell *) host.well)->color = GetColorForHost();
				host.well->Repaint(true);
			}

			EsMessage m = { ES_MSG_COLOR_CHANGED };
			m.colorChanged.newColor = GetColorForHost();
			m.colorChanged.pickerClosed = false;
			EsMessageSend(host.well, &m);

			modified = true;
		}
	}

	void PositionOnCircleToColor(int _x, int _y) {
		EsRectangle size = EsElementGetInsetSize(circle);
		float x = (float) _x / (float) (size.r  - 1) * 2.0f - 1.0f;
		float y = (float) _y / (float) (size.b - 1) * 2.0f - 1.0f;
		float newSaturation = EsCRTsqrtf(x * x + y * y), newHue = EsCRTatan2f(y, x) * 0.954929659f + 3;
		if (!EsKeyboardIsAltHeld() && newSaturation < 0.1f) newSaturation = 0;
		if (newSaturation > 1) newSaturation = 1;
		if (newHue >= 6) newHue -= 6;
		if (newHue < 0 || newHue >= 6) newHue = 0;

		if (EsKeyboardIsShiftHeld()) {
			float deltaHue = dragStartHue - newHue;
			float deltaSaturation = EsCRTfabs(dragStartSaturation - newSaturation);

			if (-3 < deltaHue && deltaHue < 3) deltaHue = EsCRTfabs(deltaHue);
			if (deltaHue < -3) deltaHue += 6;
			if (deltaHue >  3) deltaHue = -deltaHue + 6;
			deltaHue /= 2;

			if (deltaHue < deltaSaturation) {
				newHue = dragStartHue;
			} else {
				newSaturation = dragStartSaturation;
			}
		}

		uint32_t newColor = EsColorConvertToRGB(newHue, newSaturation, value);
		hue = newHue, color = newColor, saturation = newSaturation;
		if (host.indeterminate) *host.indeterminate = false;
		Sync(circle);
	}

	void PositionOnSliderToColor(int _x, int _y) {
		(void) _x;
		EsRectangle size = EsElementGetInsetSize(slider);
		float y = 1 - (float) _y / (float) (size.b - 1);
		if (y < 0) y = 0;
		y = EsCRTsqrtf(y) * EsCRTsqrtf(EsCRTsqrtf(y));
		if (y > 1) y = 1;
		if (y < 0) y = 0;
		uint32_t newColor = EsColorConvertToRGB(hue, saturation, y);
		color = newColor;
		value = y;
		if (host.indeterminate) *host.indeterminate = false;
		Sync(slider);
	}

	void PositionOnOpacitySliderToColor(int _x, int _y) {
		(void) _x;
		EsRectangle size = EsElementGetInsetSize(opacitySlider);
		float y = 1 - (float) _y / (float) (size.b - 1);
		if (y > 1) y = 1;
		if (y < 0) y = 0;
		opacity = y;
		if (host.indeterminate) *host.indeterminate = false;
		Sync(opacitySlider);
	}
};

int ProcessColorChosenPointMessage(EsElement *element, EsMessage *message) {
	ColorPicker *picker = (ColorPicker *) element->userData.p;

	if (message->type == ES_MSG_PAINT) {
		EsRectangle bounds = EsPainterBoundsInset(message->painter);
		EsStyledBox box = {};
		box.bounds = bounds;
		box.clip = message->painter->clip;
		box.borderColor = 0xFFFFFFFF;
		box.backgroundColor = picker->color | 0xFF000000;
		box.backgroundColor2 = picker->color | ((uint32_t) (255.0f * picker->opacity) << 24);
		box.borders = ES_MAKE_RECTANGLE_ALL(2);
		box.cornerRadiusTopLeft = box.cornerRadiusTopRight = box.cornerRadiusBottomLeft = box.cornerRadiusBottomRight = Width(box.bounds) / 2;

		if (picker->opacity < 1 && picker->host.hasOpacity) {
			box.fragmentShader = [] (int x, int y, EsStyledBox *box) -> uint32_t {
				// TODO Move the alpha background as the chosen point moves.
				return EsColorBlend(((((x - 2) >> 3) ^ ((y + 5) >> 3)) & 1) ? 0xFFFFFFFF : 0xFFC0C0C0, 
						box->backgroundColor2, false);
			};
		}

		DrawStyledBox(message->painter, box);
	}

	return 0;
}

void NewColorPicker(EsElement *parent, ColorPickerHost host, uint32_t initialColor, bool showTextbox) {
	ColorPicker *picker = (ColorPicker *) EsHeapAllocate(sizeof(ColorPicker), true);
	picker->host = host;
	picker->color = initialColor & 0xFFFFFF;
       	picker->opacity = (float) (initialColor >> 24) / 255.0f;
	if (host.well && host.well->classCallback == ProcessColorWellMessage) ((EsColorWell *) host.well)->picker = picker;
	EsColorConvertToHSV(picker->color, &picker->hue, &picker->saturation, &picker->value);
	
	picker->panel = EsPanelCreate(parent, ES_PANEL_HORIZONTAL | ES_PANEL_TABLE, ES_STYLE_COLOR_PICKER_MAIN_PANEL);

	picker->panel->userData = picker;

	picker->panel->userCallback = [] (EsElement *element, EsMessage *message) {
		ColorPicker *picker = (ColorPicker *) element->userData.p;

		if (message->type == ES_MSG_DESTROY) {
			if (picker->host.well && picker->modified) {
				EsMessage m = { ES_MSG_COLOR_CHANGED };
				m.colorChanged.newColor = picker->GetColorForHost();
				m.colorChanged.pickerClosed = true;
				EsMessageSend(picker->host.well, &m);

				if (picker->host.well->classCallback == ProcessColorWellMessage) {
					((EsColorWell *) picker->host.well)->picker = nullptr;
				}
			}

			EsHeapFree(picker); 
		}

		return 0;
	};

	bool hasOpacity = picker->host.hasOpacity;

	EsPanelSetBands(picker->panel, hasOpacity ? 3 : 2, showTextbox ? 2 : 1);

	picker->circle = EsCustomElementCreate(picker->panel, ES_ELEMENT_FOCUSABLE | ES_ELEMENT_NOT_TAB_TRAVERSABLE, ES_STYLE_COLOR_CIRCLE);

	picker->circle->cName = "hue-saturation wheel";
	picker->circle->userData = picker;

	picker->circle->userCallback = [] (EsElement *element, EsMessage *message) {
		ColorPicker *picker = (ColorPicker *) element->userData.p;

		if (message->type == ES_MSG_PAINT) {
			// EsPerformanceTimerPush();

			EsPainter *painter = message->painter;
			EsRectangle bounds = EsPainterBoundsInset(painter);
			EsRectangle clip = painter->clip;
			EsRectangleClip(clip, bounds, &clip);
			uint32_t stride = painter->target->stride;
			uint32_t *bitmap = (uint32_t *) painter->target->bits;
			float epsilon = 1.0f / (bounds.b - bounds.t - 1);

			for (int j = clip.t; j < clip.b; j++) {
				for (int i = clip.l; i < clip.r; i++) {
					float x = (float) (i - bounds.l) / (float) (bounds.r - bounds.l - 1) * 2.0f - 1.0f;
					float y = (float) (j - bounds.t)  / (float) (bounds.b - bounds.t - 1) * 2.0f - 1.0f;
					float radius = EsCRTsqrtf(x * x + y * y), hue = EsCRTatan2f(y, x) * 0.954929659f + 3;
					if (hue >= 6) hue -= 6;

					if (radius > 1.0f + epsilon) {
						// Outside the circle.
					} else if (radius > 1.0f - epsilon) {
						// On the edge.
						uint32_t over = EsColorConvertToRGB(hue, 1, picker->value);
						// float opacity = (1.0f - ((radius - (1.0f - epsilon)) / epsilon * 0.5f));
						float opacity = 0.5f - (radius - 1.0f) / epsilon * 0.5f;
						uint32_t alpha = (((uint32_t) (255.0f * opacity)) & 0xFF) << 24;
						uint32_t *under = &bitmap[i + j * (stride >> 2)];
						*under = EsColorBlend(*under, over | alpha, true);
					} else {
						// Inside the circle.
						uint32_t over = EsColorConvertToRGB(hue, radius, picker->value);
						bitmap[i + j * (stride >> 2)] = over | 0xFF000000;
					}
				}
			}

			// EsPrint("Rendered color circle in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
		} else if (message->type == ES_MSG_HIT_TEST) {
			EsRectangle size = EsElementGetInsetSize(element);
			float x = (float) message->hitTest.x / (float) (size.r  - 1) * 2.0f - 1.0f;
			float y = (float) message->hitTest.y / (float) (size.b - 1) * 2.0f - 1.0f;
			message->hitTest.inside = x * x + y * y <= 1;
		} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
			picker->PositionOnCircleToColor(message->mouseDragged.newPositionX, message->mouseDragged.newPositionY);
			return ES_HANDLED;
		} else if (message->type == ES_MSG_KEY_DOWN) {
			if ((message->keyboard.scancode == ES_SCANCODE_LEFT_SHIFT || message->keyboard.scancode == ES_SCANCODE_RIGHT_SHIFT) && !message->keyboard.repeat) {
				picker->dragStartHue = picker->hue;
				picker->dragStartSaturation = picker->saturation;
				EsPrint("shift pressed\n");
				return ES_HANDLED;
			}
		} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
			picker->PositionOnCircleToColor(message->mouseDown.positionX, message->mouseDown.positionY);
			picker->dragStartHue = picker->hue;
			picker->dragStartSaturation = picker->saturation;
		}

		return 0;
	};

	picker->circlePoint = EsCustomElementCreate(picker->circle, ES_ELEMENT_NO_HOVER, ES_STYLE_COLOR_CHOSEN_POINT);
	picker->circlePoint->userCallback = ProcessColorChosenPointMessage;
	picker->circlePoint->cName = "selected hue-saturation";
	picker->circlePoint->userData = picker;

	picker->slider = EsCustomElementCreate(picker->panel, ES_ELEMENT_FOCUSABLE | ES_ELEMENT_NOT_TAB_TRAVERSABLE, ES_STYLE_COLOR_SLIDER);
	picker->slider->cName = "value slider";
	picker->slider->userData = picker;

	picker->slider->userCallback = [] (EsElement *element, EsMessage *message) {
		ColorPicker *picker = (ColorPicker *) element->userData.p;

		if (message->type == ES_MSG_PAINT) {
			// EsPerformanceTimerPush();

			EsPainter *painter = message->painter;
			EsRectangle bounds = EsPainterBoundsInset(painter);
			EsRectangle clip = painter->clip;
			EsRectangleClip(clip, bounds, &clip);
			uint32_t stride = painter->target->stride;
			uint32_t *bitmap = (uint32_t *) painter->target->bits;

			float valueIncrement = -1.0f / (bounds.b - bounds.t - 1), value = 1.0f;

			for (int j = clip.t; j < clip.b; j++, value += valueIncrement) {
				// float valueSqrt = EsCRTsqrtf(value);
				// uint32_t color = EsColorConvertToRGB(picker->hue, picker->saturation, valueSqrt * EsCRTsqrtf(valueSqrt));

				for (int i = clip.l; i < clip.r; i++) {
					float i2 = (float) (i - ((bounds.l + bounds.r) >> 1)) / (float) (bounds.r - bounds.l);
					uint32_t color = EsColorConvertToRGB(picker->hue, picker->saturation, EsCRTpowf(value, 0.75f + i2 * i2 * 0.3f));
					bitmap[i + j * (stride >> 2)] = 0xFF000000 | color;
				}
			}

			// EsPrint("Rendered color slider in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
		} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
			picker->PositionOnSliderToColor(message->mouseDragged.newPositionX, message->mouseDragged.newPositionY);
			return ES_HANDLED;
		} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
			picker->PositionOnSliderToColor(message->mouseDown.positionX, message->mouseDown.positionY);
		}

		return 0;
	};

	picker->sliderPoint = EsCustomElementCreate(picker->slider, ES_ELEMENT_NO_HOVER, ES_STYLE_COLOR_CHOSEN_POINT);
	picker->sliderPoint->userCallback = ProcessColorChosenPointMessage;
	picker->sliderPoint->cName = "selected value";
	picker->sliderPoint->userData = picker;

	if (hasOpacity) {
		picker->opacitySlider = EsCustomElementCreate(picker->panel, ES_ELEMENT_FOCUSABLE | ES_ELEMENT_NOT_TAB_TRAVERSABLE, ES_STYLE_COLOR_SLIDER);
		picker->opacitySlider->cName = "opacity slider";
		picker->opacitySlider->userData = picker;

		picker->opacitySlider->userCallback = [] (EsElement *element, EsMessage *message) {
			ColorPicker *picker = (ColorPicker *) element->userData.p;

			if (message->type == ES_MSG_PAINT) {
				// EsPerformanceTimerPush();

				EsPainter *painter = message->painter;
				EsRectangle bounds = EsPainterBoundsInset(painter);
				EsRectangle clip = painter->clip;
				EsRectangleClip(clip, bounds, &clip);
				uint32_t stride = painter->target->stride;
				uint32_t *bitmap = (uint32_t *) painter->target->bits;

				float opacityIncrement = -1.0f / (bounds.b - bounds.t - 1), opacity = 1.0f;

				for (int j = clip.t; j < clip.b; j++, opacity += opacityIncrement) {
					uint32_t alpha = (uint32_t) (opacity * 255.0f) << 24;

					for (int i = clip.l; i < clip.r; i++) {
						bitmap[i + j * (stride >> 2)] 
							= EsColorBlend(((((i - bounds.l + 1) >> 3) ^ ((j - bounds.t + 2) >> 3)) & 1) ? 0xFFFFFFFF : 0xFFC0C0C0, 
									alpha | (picker->color & 0xFFFFFF), false);
					}
				}

				// EsPrint("Rendered opacity slider in %*Fms.\n", 3, 1000 * EsPerformanceTimerPop());
			} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
				picker->PositionOnOpacitySliderToColor(message->mouseDragged.newPositionX, message->mouseDragged.newPositionY);
				return ES_HANDLED;
			} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
				picker->PositionOnOpacitySliderToColor(message->mouseDown.positionX, message->mouseDown.positionY);
			}

			return 0;
		};

		picker->opacitySliderPoint = EsCustomElementCreate(picker->opacitySlider, ES_ELEMENT_NO_HOVER, ES_STYLE_COLOR_CHOSEN_POINT);
		picker->opacitySliderPoint->userCallback = ProcessColorChosenPointMessage;
		picker->opacitySliderPoint->cName = "selected opacity";
		picker->opacitySliderPoint->userData = picker;
	}

	if (showTextbox) {
		picker->textbox = EsTextboxCreate(picker->panel, ES_TEXTBOX_EDIT_BASED | ES_CELL_EXPAND | ES_TEXTBOX_NO_SMART_CONTEXT_MENUS, ES_STYLE_COLOR_HEX_TEXTBOX);
		picker->textbox->userData = picker;

		picker->textbox->userCallback = [] (EsElement *element, EsMessage *message) {
			ColorPicker *picker = (ColorPicker *) element->userData.p;

			if (message->type == ES_MSG_TEXTBOX_UPDATED) {
				size_t bytes;
				char *string = EsTextboxGetContents(picker->textbox, &bytes);
				uint32_t color = EsColorParse(string, bytes);
				picker->opacity = (float) (color >> 24) / 255.0f;
				color &= 0xFFFFFF;
				EsColorConvertToHSV(color, &picker->hue, &picker->saturation, &picker->value);
				picker->color = color;
				if (picker->host.indeterminate) *picker->host.indeterminate = false;
				picker->Sync(picker->textbox);
				EsHeapFree(string);
			} else if (message->type == ES_MSG_TEXTBOX_EDIT_START) {
				EsTextboxSetSelection((EsTextbox *) element, 0, 1, 0, -1);
				return ES_HANDLED;
			} else if (message->type == ES_MSG_TEXTBOX_EDIT_END) {
				picker->Sync(nullptr);
				return ES_HANDLED;
			} else if (message->type == ES_MSG_TEXTBOX_NUMBER_DRAG_DELTA) {
				int componentCount = picker->host.hasOpacity ? 4 : 3;
				int componentIndex = (message->numberDragDelta.hoverCharacter - 1) / 2;
				if (componentIndex < 0) componentIndex = 0;
				if (componentIndex >= componentCount) componentIndex = componentCount - 1;
				componentIndex = componentCount - componentIndex - 1;
				picker->dragComponent = componentIndex;

				if (componentIndex == 3) {
					picker->opacity += message->numberDragDelta.delta / 255.0f;
					if (picker->opacity < 0) picker->opacity = 0;
					if (picker->opacity > 1) picker->opacity = 1;
				} else {
					int32_t componentValue = (picker->color >> (componentIndex << 3)) & 0xFF;
					componentValue += message->numberDragDelta.delta;
					if (componentValue < 0) componentValue = 0;
					if (componentValue > 255) componentValue = 255;
					picker->color &= ~(0xFF << (componentIndex << 3));
					picker->color |= (uint32_t) componentValue << (componentIndex << 3);
					EsColorConvertToHSV(picker->color, &picker->hue, &picker->saturation, &picker->value);
				}

				picker->Sync(nullptr);
				return ES_HANDLED;
			}

			return 0;
		};

		EsTextboxUseNumberOverlay(picker->textbox, false);

		EsButton *eyedropperButton = EsButtonCreate(picker->panel, ES_CELL_EXPAND, 0);
		eyedropperButton->userData = picker;

		eyedropperButton->userCallback = [] (EsElement *element, EsMessage *message) {
			ColorPicker *picker = (ColorPicker *) element->userData.p;

			if (message->type == ES_MSG_CLICKED) {
				picker->indeterminateBeforeEyedrop = picker->host.indeterminate && *picker->host.indeterminate;
				EsSyscall(ES_SYSCALL_START_EYEDROP, (uintptr_t) element, picker->circle->window->handle, picker->color, 0);
			} else if (message->type == ES_MSG_EYEDROP_REPORT) {
				if (message->eyedrop.cancelled && picker->indeterminateBeforeEyedrop) {
					if (picker->host.well && picker->host.well->classCallback == ProcessColorWellMessage) {
						EsColorWellSetIndeterminate((EsColorWell *) picker->host.well);
					}
				} else {
					picker->color = message->eyedrop.color;
					EsColorConvertToHSV(picker->color, &picker->hue, &picker->saturation, &picker->value);
					if (picker->host.indeterminate) *picker->host.indeterminate = false;
					picker->Sync(nullptr);
				}
			}

			return 0;
		};

		EsButtonSetIcon(eyedropperButton, ES_ICON_COLOR_SELECT_SYMBOLIC);

		if (hasOpacity) {
			EsElementSetCellRange(eyedropperButton, 1, 1, 2, 1);
		}
	}

	picker->Sync(picker->host.well);
	if (picker->textbox) EsElementFocus(picker->textbox);
}

uint32_t EsColorWellGetRGB(EsColorWell *well) {
	EsMessageMutexCheck();

	return well->color & ((well->flags & ES_COLOR_WELL_HAS_OPACITY) ? 0xFFFFFFFF : 0x00FFFFFF);
}

void EsColorWellSetRGB(EsColorWell *well, uint32_t color, bool sendChangedMessage) {
	EsMessageMutexCheck();

	well->color = color;
	well->indeterminate = false;
	well->Repaint(true);

	if (sendChangedMessage) {
		EsMessage m = { ES_MSG_COLOR_CHANGED };
		m.colorChanged.newColor = color;
		m.colorChanged.pickerClosed = true;
		EsMessageSend(well, &m);
	}

	if (well->picker) {
		well->picker->color = color & 0xFFFFFF;
		well->picker->opacity = (color >> 24) / 255.0f;
		EsColorConvertToHSV(well->picker->color, &well->picker->hue, &well->picker->saturation, &well->picker->value);
		well->picker->Sync(well);
	}
}

void EsColorWellSetIndeterminate(EsColorWell *well) {
	EsMessageMutexCheck();

	well->color = 0xFFFFFFFF;
	well->indeterminate = true;
	well->Repaint(true);

	if (well->picker) {
		well->picker->color = 0xFFFFFF;
		well->picker->opacity = 1.0f;
		EsColorConvertToHSV(well->picker->color, &well->picker->hue, &well->picker->saturation, &well->picker->value);
		well->picker->Sync(well);
	}
}

int ProcessColorWellMessage(EsElement *element, EsMessage *message) {
	EsColorWell *well = (EsColorWell *) element;

	if (message->type == ES_MSG_PAINT) {
		EsRectangle bounds = EsPainterBoundsInset(message->painter);
		EsStyledBox box = {};
		box.bounds = bounds;
		box.clip = message->painter->clip;
		box.borders = ES_MAKE_RECTANGLE_ALL(1);

		if (well->indeterminate) {
			box.backgroundColor = 0;
			box.borderColor = 0x40000000;
		} else {
			box.backgroundColor = well->color;
			if (~well->flags & ES_COLOR_WELL_HAS_OPACITY) box.backgroundColor |= 0xFF000000;
			box.borderColor = EsColorBlend(well->color | 0xFF000000, 0x40000000, false);

			if ((well->flags & ES_COLOR_WELL_HAS_OPACITY) && ((well->color & 0xFF000000) != 0xFF000000)) {
				box.fragmentShader = [] (int x, int y, EsStyledBox *box) -> uint32_t {
					return EsColorBlend(((((x - box->bounds.l - 4) >> 3) ^ ((y - box->bounds.t + 2) >> 3)) & 1) 
							? 0xFFFFFFFF : 0xFFC0C0C0, box->backgroundColor, false);
				};
			}
		}

		DrawStyledBox(message->painter, box);
	} else if (message->type == ES_MSG_CLICKED) {
		EsMenu *menu = EsMenuCreate(well, ES_FLAGS_DEFAULT);
		ColorPickerHost host = { well, &well->indeterminate, (well->flags & ES_COLOR_WELL_HAS_OPACITY) ? true : false };
		NewColorPicker((EsElement *) menu, host, well->color, true);
		EsMenuShow(menu);
	}

	return 0;
}

EsColorWell *EsColorWellCreate(EsElement *parent, uint64_t flags, uint32_t initialColor) {
	EsColorWell *well = (EsColorWell *) EsHeapAllocate(sizeof(EsColorWell), true);
	well->color = initialColor;
	well->Initialise(parent, flags | ES_ELEMENT_FOCUSABLE, ProcessColorWellMessage, ES_STYLE_PUSH_BUTTON_NORMAL_COLOR_WELL);
	well->cName = "color well";
	return well;
}

// --------------------------------- Splitters.

// TODO With dockable UI, show split bars at the start and end of the splitter as drop targets.
// 	The root splitter will also need two split bars at the start and end of the other axis.
// 	Split bars should also be enlarged when actings as drop targets.
// 	When dropping on an existing non-splitter panel, you can either form a tab group,
// 	or create a new split on the other axis, on one of the two sides.

struct EsSplitter : EsElement {
	bool horizontal;
	bool addingSplitBar;
	int previousSize;
	DS_ARRAY(int64_t) resizeStartSizes;
	bool calculatedInitialSize;
};

struct SplitBar : EsElement {
	int position, dragStartPosition;

	void Move(int newPosition, bool fromKeyboard) {
		EsSplitter *splitter = (EsSplitter *) parent;
		EsElement *panelBefore = nullptr, *panelAfter = nullptr;
		int barBefore = 0, barAfter;
		if (splitter->horizontal) barAfter = AddBorder(splitter->GetBounds(), splitter->currentStyle->borders).r  - currentStyle->preferredWidth;
		else                      barAfter = AddBorder(splitter->GetBounds(), splitter->currentStyle->borders).b - currentStyle->preferredHeight;
		int preferredSize = splitter->horizontal ? currentStyle->preferredWidth : currentStyle->preferredHeight;
		arrfree(splitter->resizeStartSizes);

		for (uintptr_t i = 0; i < splitter->GetChildCount(); i++) {
			if (splitter->GetChild(i) == this) {
				EsAssert(i & 1); // Expected split bars between each EsSplitter child.
				panelBefore = splitter->GetChild(i - 1);
				panelAfter = splitter->GetChild(i + 1);

				if (i != 1) {
					barBefore = ((SplitBar *) splitter->GetChild(i - 2))->position + preferredSize;
				}

				if (i != splitter->GetChildCount() - 2) {
					barAfter = ((SplitBar *) splitter->GetChild(i + 2))->position - preferredSize;
				}

				break;
			}
		}

		EsAssert(panelBefore && panelAfter); // Could not find split bar in parent.

		barBefore -= splitter->horizontal ? currentStyle->borders.l  : currentStyle->borders.t;
		barAfter  += splitter->horizontal ? currentStyle->borders.r : currentStyle->borders.b;

		int minimumPosition, maximumPosition, minimumPosition1, maximumPosition1, minimumPosition2, maximumPosition2;

		if (splitter->horizontal) {
			minimumPosition1 = barBefore + panelBefore->currentStyle->metrics->minimumWidth;
			maximumPosition1 = barAfter  - panelAfter ->currentStyle->metrics->minimumWidth;
			minimumPosition2 = barAfter  - panelAfter ->currentStyle->metrics->maximumWidth;
			maximumPosition2 = barBefore + panelBefore->currentStyle->metrics->maximumWidth;
			if (!panelAfter ->currentStyle->metrics->maximumWidth) minimumPosition2 = INT_MIN;
			if (!panelBefore->currentStyle->metrics->maximumWidth) maximumPosition2 = INT_MAX;
		} else {
			minimumPosition1 = barBefore + panelBefore->currentStyle->metrics->minimumHeight;
			maximumPosition1 = barAfter  - panelAfter ->currentStyle->metrics->minimumHeight;
			minimumPosition2 = barAfter  - panelAfter ->currentStyle->metrics->maximumHeight;
			maximumPosition2 = barBefore + panelBefore->currentStyle->metrics->maximumHeight;
			if (!panelAfter ->currentStyle->metrics->maximumHeight) minimumPosition2 = INT_MIN;
			if (!panelBefore->currentStyle->metrics->maximumHeight) maximumPosition2 = INT_MAX;
		}

		minimumPosition = minimumPosition1 > minimumPosition2 ? minimumPosition1 : minimumPosition2;
		maximumPosition = maximumPosition1 < maximumPosition2 ? maximumPosition1 : maximumPosition2;

		if (minimumPosition < maximumPosition) {
			int oldPosition = position;

			if (newPosition < minimumPosition) {
				if (newPosition > minimumPosition2 
						&& (fromKeyboard || newPosition < (barBefore + minimumPosition1) / 2) 
						&& (!fromKeyboard || newPosition < position) 
						&& panelBefore->flags & ES_CELL_COLLAPSABLE) {
					position = barBefore > minimumPosition2 ? barBefore : minimumPosition2;
				} else {
					position = minimumPosition;
				}
			} else if (newPosition > maximumPosition) {
				if (newPosition < maximumPosition2 
						&& (fromKeyboard || newPosition > (barAfter + maximumPosition1) / 2)
						&& (!fromKeyboard || newPosition > position)
						&& panelAfter->flags & ES_CELL_COLLAPSABLE) {
					position = barAfter < maximumPosition2 ? barAfter : maximumPosition2;
				} else {
					position = maximumPosition;
				}
			} else {
				position = newPosition;
			}

			if (oldPosition != position) {
				EsElementRelayout(splitter);
			}
		}
	}
};

int ProcessSplitBarMessage(EsElement *element, EsMessage *message) {
	SplitBar *bar = (SplitBar *) element;
	EsSplitter *splitter = (EsSplitter *) bar->parent;

	if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
		bar->dragStartPosition = bar->position;

		if (!bar->window->focused || bar->window->focused->classCallback != ProcessSplitBarMessage) {
			// Don't take focus.
			return ES_REJECTED;
		}
	} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
		if (splitter->horizontal) {
			bar->Move(message->mouseDragged.newPositionX - message->mouseDragged.originalPositionX + bar->dragStartPosition, false);
		} else {
			bar->Move(message->mouseDragged.newPositionY - message->mouseDragged.originalPositionY + bar->dragStartPosition, false);
		}
	} else if (message->type == ES_MSG_KEY_TYPED) {
		if (message->keyboard.scancode == (splitter->horizontal ? ES_SCANCODE_LEFT_ARROW : ES_SCANCODE_UP_ARROW)) {
			bar->Move(bar->position - GetConstantNumber("splitBarKeyboardMovementAmount"), true);
		} else if (message->keyboard.scancode == (splitter->horizontal ? ES_SCANCODE_RIGHT_ARROW : ES_SCANCODE_DOWN_ARROW)) {
			bar->Move(bar->position + GetConstantNumber("splitBarKeyboardMovementAmount"), true);
		}
	}

	return 0;
}

int ProcessSplitterMessage(EsElement *element, EsMessage *message) {
	EsSplitter *splitter = (EsSplitter *) element;

	if (message->type == ES_MSG_LAYOUT && splitter->GetChildCount()) {
		EsRectangle client = splitter->GetBounds();
		EsRectangle bounds = AddBorder(client, splitter->currentStyle->insets);

		size_t childCount = splitter->GetChildCount();
		EsAssert(childCount & 1); // Expected split bars between each EsSplitter child.
		uint64_t pushFlag = splitter->horizontal ? ES_CELL_H_PUSH : ES_CELL_V_PUSH;

		if (!splitter->calculatedInitialSize) {
			for (uintptr_t i = 0; i < childCount; i += 2) {
				EsElement *child = splitter->GetChild(i);

				if (~child->flags & pushFlag) {
					int width = child->GetWidth(bounds.b - bounds.t);
					int height = child->GetHeight(width);
					arrput(splitter->resizeStartSizes, splitter->horizontal ? width : height);
				} else {
					arrput(splitter->resizeStartSizes, 0);
				}
			}
		}

		int64_t newSize = splitter->horizontal ? (bounds.r - bounds.l) : (bounds.b - bounds.t);

		if (newSize != splitter->previousSize && childCount > 1) {
			// Step 1: Make a list of current sizes.

			int64_t barSize = splitter->horizontal ? splitter->GetChild(1)->currentStyle->preferredWidth : splitter->GetChild(1)->currentStyle->preferredHeight;
			int64_t previousPosition = 0;

			if (!splitter->resizeStartSizes) {
				for (uintptr_t i = 1; i < childCount; i += 2) {
					int64_t position = ((SplitBar *) splitter->GetChild(i))->position;
					arrput(splitter->resizeStartSizes, position - previousPosition);
					previousPosition = position + barSize;
				}

				arrput(splitter->resizeStartSizes, splitter->previousSize - previousPosition);
			}

			DS_ARRAY(int64_t) currentSizes = nullptr;

			for (uintptr_t i = 0; i < arrlenu(splitter->resizeStartSizes); i++) {
				arrput(currentSizes, splitter->resizeStartSizes[i]);
			}

			// Step 2: Calculate the fixed size, and total weight.

			int64_t fixedSize = 0, totalWeight = 0;

			for (uintptr_t i = 0; i < childCount; i += 2) {
				EsElement *child = splitter->GetChild(i);

				if (~child->flags & pushFlag) {
					fixedSize += currentSizes[i >> 1];
				} else {
					if (currentSizes[i >> 1] < 1) currentSizes[i >> 1] = 1;
					totalWeight += currentSizes[i >> 1];
				}
			}

			EsAssert(totalWeight); // Splitter must have at least one child with a PUSH flag for its orientation.

			// Step 3: Calculate the new weighted sizes.

			int64_t availableSpace = newSize - fixedSize - barSize * (childCount >> 1);

			if (availableSpace >= 0) {
				for (uintptr_t i = 0; i < childCount; i += 2) {
					EsElement *child = splitter->GetChild(i);

					if (child->flags & pushFlag) {
						currentSizes[i >> 1] = availableSpace * currentSizes[i >> 1] / totalWeight;
					}
				}
			} else {
				availableSpace += fixedSize;
				if (availableSpace < 0) availableSpace = 0;

				for (uintptr_t i = 0; i < childCount; i += 2) {
					EsElement *child = splitter->GetChild(i);

					if (child->flags & pushFlag) {
						currentSizes[i >> 1] = 0;
					} else {
						currentSizes[i >> 1] = availableSpace * currentSizes[i >> 1] / fixedSize;
					}
				}
			}

			// Step 4: Update the positions.

			previousPosition = 0;

			for (uintptr_t i = 1; i < childCount; i += 2) {
				SplitBar *bar = (SplitBar *) splitter->GetChild(i);
				bar->position = previousPosition + currentSizes[i >> 1];
				previousPosition = bar->position + barSize - (splitter->horizontal ? bar->currentStyle->borders.l : bar->currentStyle->borders.t);

				if (bar->position == 0) {
					bar->position -= splitter->horizontal ? bar->currentStyle->borders.l  : bar->currentStyle->borders.t;
				} else if (bar->position == newSize - barSize) {
					bar->position += splitter->horizontal ? bar->currentStyle->borders.r : bar->currentStyle->borders.b;
				}
			}

			arrfree(currentSizes);
		}

		splitter->calculatedInitialSize = true;
		splitter->previousSize = newSize;

		int position = splitter->horizontal ? bounds.l : bounds.t;

		for (uintptr_t i = 0; i < childCount; i++) {
			EsElement *child = splitter->GetChild(i);

			if (i & 1) {
				if (splitter->horizontal) {
					int size = child->currentStyle->preferredWidth;
					child->InternalMove(size, client.b - client.t, position, client.t);
					position += size;
				} else {
					int size = child->currentStyle->preferredHeight;
					child->InternalMove(client.r - client.l, size, client.l, position);
					position += size;
				}
			} else if (i == childCount - 1) {
				if (splitter->horizontal) {
					child->InternalMove(bounds.r - position, bounds.b - bounds.t, position, bounds.t);
				} else {
					child->InternalMove(bounds.r - bounds.l, bounds.b - position, bounds.l, position);
				}
			} else {
				SplitBar *bar = (SplitBar *) splitter->GetChild(i + 1);
				int size = bar->position - position;

				if (splitter->horizontal) {
					child->InternalMove(size, bounds.b - bounds.t, position, bounds.t);
				} else {
					child->InternalMove(bounds.r - bounds.l, size, bounds.l, position);
				}

				position += size;
			}
		}
	} else if ((message->type == ES_MSG_GET_WIDTH && splitter->horizontal)
			|| (message->type == ES_MSG_GET_HEIGHT && !splitter->horizontal)) {
		int size = 0;

		for (uintptr_t i = 0; i < splitter->GetChildCount(); i++) {
			EsElement *child = splitter->GetChild(i);
			size += splitter->horizontal ? child->GetWidth(message->measure.height) : child->GetHeight(message->measure.width);
		}

		if (splitter->horizontal) {
			message->measure.width = size;
		} else {
			message->measure.height = size;
		}
	} else if (message->type == ES_MSG_PRE_ADD_CHILD && !splitter->addingSplitBar && splitter->GetChildCount()) {
		splitter->addingSplitBar = true;
		SplitBar *bar = (SplitBar *) EsHeapAllocate(sizeof(SplitBar), true);

		bar->Initialise(splitter, ES_ELEMENT_FOCUSABLE | ES_ELEMENT_NOT_TAB_TRAVERSABLE | ES_ELEMENT_CENTER_ACCESS_KEY_HINT | ES_CELL_EXPAND, 
				ProcessSplitBarMessage, splitter->horizontal ? ES_STYLE_SPLIT_BAR_VERTICAL : ES_STYLE_SPLIT_BAR_HORIZONTAL);

		bar->cName = "split bar";
		bar->accessKey = 'Q';

		splitter->addingSplitBar = false;
	} else if (message->type == ES_MSG_REMOVE_CHILD && ((EsElement *) message->child)->classCallback != ProcessSplitBarMessage) {
		for (uintptr_t i = 0; i < splitter->GetChildCount(); i++) {
			if (splitter->GetChild(i) == message->child) {
				// Remove the corresponding split bar.

				if (i) {
					splitter->GetChild(i - 1)->Destroy();
				} else {
					splitter->GetChild(i + 1)->Destroy();
				}

				break;
			}
		}
	} else if (message->type == ES_MSG_DESTROY) {
		arrfree(splitter->resizeStartSizes);
	}

	return 0;
}

EsSplitter *EsSplitterCreate(EsElement *parent, uint64_t flags, const EsStyle *style) {
	EsSplitter *splitter = (EsSplitter *) EsHeapAllocate(sizeof(EsSplitter), true);
	splitter->horizontal = flags & ES_SPLITTER_HORIZONTAL;
	splitter->Initialise(parent, flags | ES_ELEMENT_NO_CLIP, ProcessSplitterMessage, 
			style ?: ES_STYLE_PANEL_WINDOW_BACKGROUND);
	splitter->cName = "splitter";
	return splitter;
}

// --------------------------------- Image displays.

// TODO
// 	asynchronous/synchronous load/decode from file/memory
// 	unloading image when not visible
// 	aspect ratio; sizing
// 	upscale/downscale quality
// 	subregion, transformations
// 	transparency, IsRegionCompletelyOpaque, proper blending mode with fragmentShader in DrawStyledBox
// 	image sets, DPI; SVG scaling
// 	embedding in TextDisplay
// 	merge with IconDisplay
// 	decode in separate process for security?

struct EsImageDisplay : EsElement {
};

int ProcessImageDisplayMessage(EsElement *, EsMessage *) {
	return 0;
}

EsImageDisplay *EsImageDisplayCreate(EsElement *parent, uint64_t flags, const EsStyle *style) {
	EsImageDisplay *display = (EsImageDisplay *) EsHeapAllocate(sizeof(EsImageDisplay), true);
	display->Initialise(parent, flags, ProcessImageDisplayMessage, style);
	display->cName = "image";
	return display;
}

struct EsIconDisplay : EsElement {
	uint32_t iconID;
};

int ProcessIconDisplayMessage(EsElement *element, EsMessage *message) {
	EsIconDisplay *display = (EsIconDisplay *) element;

	if (message->type == ES_MSG_PAINT) {
		EsPainterDrawStandardContent(message->painter, "", 0, display->iconID);
	}

	return 0;
}

EsIconDisplay *EsIconDisplayCreate(EsElement *parent, uint64_t flags, const EsStyle *style, uint32_t iconID) {
	EsIconDisplay *display = (EsIconDisplay *) EsHeapAllocate(sizeof(EsIconDisplay), true);
	display->Initialise(parent, flags, ProcessIconDisplayMessage, style ?: ES_STYLE_ICON_DISPLAY);
	display->cName = "icon";
	display->iconID = iconID;
	return display;
}

// --------------------------------- Message loop and core UI infrastructure.

void EsElement::PrintTree(int depth) {
	const char *tabs = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	EsPrint("%s%z c:%d, %z\n", depth, tabs, cName, arrlenu(children), state & UI_STATE_DESTROYING ? "DESTROYING" : "");

	for (uintptr_t i = 0; i < arrlenu(children); i++) {
		children[i]->PrintTree(depth + 1);
	}
}

void EsElement::Destroy(bool manual) {
	if (state & UI_STATE_DESTROYING) {
		return;
	}

	if (manual) {
#ifndef DISABLE_ALL_ANIMATIONS
		if (currentStyle->metrics->exitDuration) {
			EsAssert(!exitTransitionFrame); // Element is already exiting.
			exitTransitionFrame = (EsPaintTarget *) EsHeapAllocate(sizeof(EsPaintTarget), true);

			if (EsPaintTargetTake(exitTransitionFrame, width, height)) {
				// TODO Doesn't support shadows.
				EsPainter painter = { .clip = ES_MAKE_RECTANGLE(0, width, 0, height), .width = width, .height = height, .target = exitTransitionFrame };
				InternalPaint(&painter, PAINT_NO_TRANSITION | PAINT_NO_OFFSET);
				state |= UI_STATE_EXITING;
				RefreshStyle();
			}
		}
#endif

		EsElement *ancestor = parent;

		while (ancestor && (~ancestor->state & UI_STATE_DESTROYING_CHILD)) {
			ancestor->state |= UI_STATE_DESTROYING_CHILD;
			ancestor = ancestor->parent;
		}
	}

	state |= UI_STATE_DESTROYING | UI_STATE_DESTROYING_CHILD | UI_STATE_BLOCK_INTERACTION;

	if (parent) {
		EsMessage m = { ES_MSG_REMOVE_CHILD };
		m.child = this;
		EsMessageSend(parent, &m);
	}

	if (window->hovered == this) {
		window->hovered = window;
		window->state |= UI_STATE_HOVERED;
		EsMessage m = {};
		m.type = ES_MSG_HOVERED_START;
		EsMessageSend(window, &m);
	}

	EsMessage m = { ES_MSG_DESTROY };
	if (userCallback) userCallback(this, &m);
	userCallback = nullptr;
	if (classCallback) classCallback(this, &m);
	classCallback = nullptr;

	if (window->inactiveFocus	== this) window->inactiveFocus = nullptr;
	if (window->pressed 		== this) window->pressed = nullptr;
	if (window->focused 		== this) window->focused = nullptr;
	if (window->defaultEnterButton 	== this) window->defaultEnterButton = nullptr;		
	if (window->enterButton 	== this) window->enterButton = window->defaultEnterButton;
	if (window->escapeButton 	== this) window->escapeButton = nullptr;

	if (parent) EsElementUpdateContentSize(parent);

	EnsureWindowWillUpdate(window);
}

bool EsElement::InternalDestroy() {
	if (~state & UI_STATE_DESTROYING_CHILD) {
		return false;
	}

	for (uintptr_t i = 0; i < arrlenu(children); i++) {
		if (state & UI_STATE_DESTROYING) {
			children[i]->Destroy(false);
		}

		if (children[i]->InternalDestroy() && (~state & UI_STATE_DESTROYING)) {
			arrdel(children, i);
			i--;
		}
	}

	state &= ~UI_STATE_DESTROYING_CHILD;

	if (~state & UI_STATE_DESTROYING) {
		return false;
	}

	arrfree(children);

	if (state & UI_STATE_EXITING) {
		return false;
	}

	InspectorNotifyElementDestroyed(this);

	if (state & UI_STATE_ANIMATING) {
		for (uintptr_t i = 0; i < arrlenu(gui.animatingElements); i++) {
			if (gui.animatingElements[i] == this) {
				arrdelswap(gui.animatingElements, i);
				break;
			}
		}

		state &= ~UI_STATE_ANIMATING;
	}

	if (currentStyle) currentStyle->CloseReference(currentStyleKey);
	if (exitTransitionFrame) EsPaintTargetReturn(exitTransitionFrame);
	EsHeapFree(exitTransitionFrame);
	ThemeAnimationDestroy(&animation);
	if (window == this) InternalDestroyWindow(window); // Windows are deallocated after receiving ES_MSG_WINDOW_DESTROYED.
	else EsHeapFree(this);

	return true;
}

EsElement *EsWindowGetToolbar(EsWindow *window) {
	return window->GetChild(1);
}

EsHandle EsWindowGetHandle(EsWindow *window) {
	return window->handle;
}

EsRectangle EsWindowGetBounds(EsWindow *window) {
	EsRectangle bounds;
	EsSyscall(ES_SYSCALL_WINDOW_GET_BOUNDS, window->handle, (uintptr_t) &bounds, 0, 0);
	return bounds;
}

EsRectangle EsElementGetInsetSize(EsElement *element) {
	EsMessageMutexCheck();

	EsRectangle insets = element->currentStyle->insets;
	return ES_MAKE_RECTANGLE(0, element->width - insets.l - insets.r, 
			0, element->height - insets.t - insets.b);
}

void EsWindowSetIcon(EsWindow *window, uint32_t iconID) {
	EsMessageMutexCheck();

	char buffer[5];
	buffer[0] = WINDOW_METADATA_ICON;
	EsMemoryCopy(buffer + 1, &iconID, sizeof(uint32_t));
	EsSyscall(ES_SYSCALL_WINDOW_SET_METADATA, (uintptr_t) buffer, sizeof(buffer), window->handle, 0);
}

void EsWindowSetTitle(EsWindow *window, const char *title, ptrdiff_t titleBytes) {
	EsMessageMutexCheck();

	ApplicationInstance *instance = window->instance ? ((ApplicationInstance *) window->instance->_private) : nullptr;
	const char *applicationName = instance ? instance->applicationName : nullptr;
	size_t applicationNameBytes = instance ? instance->applicationNameBytes : 0;

	if (!applicationNameBytes && !titleBytes) {
		return;
	}

	if (titleBytes == -1) {
		titleBytes = EsCStringLength(title);
	}

	if (titleBytes) {
		applicationNameBytes = 0;
	}

	char buffer[4096];
	size_t bytes = EsStringFormat(buffer, 4096, "%c%s%s", WINDOW_METADATA_TITLE, titleBytes, title, applicationNameBytes, applicationName);
	EsSyscall(ES_SYSCALL_WINDOW_SET_METADATA, (uintptr_t) buffer, bytes, window->handle, 0);
}

void EsMouseSetPosition(EsWindow *relativeWindow, int x, int y) {
	if (relativeWindow) {
		EsRectangle bounds = EsWindowGetBounds(relativeWindow);
		x += bounds.l;
		y += bounds.t;
	}

	EsSyscall(ES_SYSCALL_SET_CURSOR_POSITION, x, y, 0, 0);
}

EsPoint EsMouseGetPosition(EsElement *relativeElement) {
	EsPoint position;
	EsSyscall(ES_SYSCALL_GET_CURSOR_POSITION, (uintptr_t) &position, 0, 0, 0);

	if (relativeElement) {
		EsRectangle bounds = relativeElement->GetScreenBounds();
		position.x -= bounds.l;
		position.y -= bounds.t;
	}

	return position;
}

EsElement *FindHoverElementRecursively(EsElement *element, int offsetX, int offsetY, EsPoint position);

EsElement *_FindHoverElementRecursively(EsElement *element, int offsetX, int offsetY, EsPoint position, uintptr_t i) {
	EsElement *child = element->GetChildByZ(i - 1);

	if (!child) return nullptr;
	if (child->flags & ES_ELEMENT_HIDDEN) return nullptr;
	if (child->state & UI_STATE_DESTROYING) return nullptr;
	if (child->state & UI_STATE_BLOCK_INTERACTION) return nullptr;

	if (!IsPointInRectangle(ES_MAKE_RECTANGLE(offsetX + child->offsetX, offsetX + child->offsetX + child->width, 
					offsetY + child->offsetY, offsetY + child->offsetY + child->height), 
				position.x, position.y)) {
		return nullptr;
	}

	EsMessage m = { ES_MSG_HIT_TEST };
	m.hitTest.x = position.x - offsetX - child->offsetX - child->internalOffsetLeft;
	m.hitTest.y = position.y - offsetY - child->offsetY - child->internalOffsetTop;
	m.hitTest.inside = true;
	int response = EsMessageSend(child, &m);

	if ((response != ES_HANDLED || m.hitTest.inside) && response != ES_REJECTED) {
		return FindHoverElementRecursively(child, offsetX, offsetY, position);
	}

	return nullptr;
}

EsElement *FindHoverElementRecursively(EsElement *element, int offsetX, int offsetY, EsPoint position) {
	offsetX += element->offsetX;
	offsetY += element->offsetY;

	EsMessage zOrder = { ES_MSG_BEFORE_Z_ORDER };
	zOrder.beforeZOrder.nonClient = zOrder.beforeZOrder.end = arrlenu(element->children);
	zOrder.beforeZOrder.clip = Translate(ES_MAKE_RECTANGLE(0, element->width, 0, element->height), offsetX, offsetY);
	EsMessageSend(element, &zOrder);

	EsElement *result = nullptr;

	for (uintptr_t i = arrlenu(element->children); !result && i > zOrder.beforeZOrder.nonClient; i--) {
		result = _FindHoverElementRecursively(element, offsetX, offsetY, position, i);
	}

	for (uintptr_t i = zOrder.beforeZOrder.end; !result && i > zOrder.beforeZOrder.start; i--) {
		result = _FindHoverElementRecursively(element, offsetX, offsetY, position, i);
	}

	zOrder.type = ES_MSG_AFTER_Z_ORDER;
	EsMessageSend(element, &zOrder);

	return result ? result : (element->flags & ES_ELEMENT_NO_HOVER) ? nullptr : element;
}

void FindHoverElement(EsWindow *window) {
	// TS("Finding element under cursor\n");

	EsPoint position = EsMouseGetPosition(window);

	EsElement *element;

	if (position.x < 0 || position.y < 0 || position.x >= window->width || position.y >= window->height || !window->hovering) {
		element = window;
	} else {
		element = FindHoverElementRecursively(window, 0, 0, position);
	}

	if (element->state & UI_STATE_HOVERED) {
		EsAssert(window->hovered == element); // Window's hovered element mismatched element state flags.
	} else {
		EsMessage m = {};
		m.type = ES_MSG_HOVERED_END;
		window->hovered->state &= ~UI_STATE_HOVERED;
		EsMessageSend(window->hovered, &m);
		element->state |= UI_STATE_HOVERED;
		m.type = ES_MSG_HOVERED_START;
		EsMessageSend((window->hovered = element), &m);
	}
}

void EsElementFocus(EsElement *element, bool ensureVisible) {
	EsMessageMutexCheck();

	EsWindow *window = element->window;
	EsMessage m;

	// If this element is not focusable or if the window doesn't allow focused elements, ignore the request.

	if (!element->IsFocusable() || (window->windowStyle == ES_WINDOW_CONTAINER)) return;

	// If the element is already focused, then don't resend it any messages.
	
	if (window->focused == element) return;

	// Tell the previously focused element it's no longer focused.

	EsElement *oldFocus = window->focused;
	window->focused = element;
	RemoveFocusFromElement(oldFocus);

	// Tell any parents of the previously focused element that aren't parents of the newly focused element that they no longer has focus-within,
	// and the parents of the newly focused element that they have focus-within.
	
	EsElement *parent = element->parent;

	while (parent) {
		parent->state |= UI_STATE_TEMP;
		parent = parent->parent;
	}

	if (oldFocus) {
		parent = oldFocus->parent;

		while (parent) {
			if (~parent->state & UI_STATE_TEMP) {
				parent->state &= ~UI_STATE_FOCUS_WITHIN;
				m.type = ES_MSG_FOCUS_WITHIN_END;
				EsMessageSend(parent, &m);
			}

			parent = parent->parent;
		}
	}

	parent = element->parent;
	window->focused = element;

	while (parent) {
		if (~parent->state & UI_STATE_FOCUS_WITHIN) {
			parent->state |= UI_STATE_FOCUS_WITHIN;
			m.type = ES_MSG_FOCUS_WITHIN_START;
			EsMessageSend(parent, &m);

			EsAssert(window->focused == element); // Cannot change window focus from FOCUS_WITHIN_START message.
		}

		parent->state &= ~UI_STATE_TEMP;
		parent = parent->parent;
	}

	// Tell the newly focused element it's focused.

	m.type = ES_MSG_FOCUSED_START;
	window->focused->state |= UI_STATE_FOCUSED;
	EsMessageSend(element, &m);
	EsAssert(window->focused == element); // Cannot change window focus from FOCUSED_START message.

	// Ensure the element is visible.

	if (ensureVisible && element) {
		EsElement *child = element, *e = element;

		while (e->parent) {
			EsMessage m = { ES_MSG_ENSURE_VISIBLE };
			m.ensureVisible.child = child;
			e = e->parent;

			if (ES_HANDLED == EsMessageSend(e, &m)) {
				child = e;
			}
		}
	}
}

EsRectangle EsElement::GetWindowBounds(bool client) {
	EsElement *element = this;
	int x = 0, y = 0;

	while (element) {
		x += element->offsetX, y += element->offsetY;
		element = element->parent;
	}

	int cw = width  - (client ? (internalOffsetLeft + internalOffsetRight) : 0);
	int ch = height - (client ? (internalOffsetTop + internalOffsetBottom) : 0);

	return ES_MAKE_RECTANGLE(x, x + cw, y, y + ch);
}

EsRectangle EsElement::GetScreenBounds(bool client) {
	EsRectangle elementBoundsInWindow = GetWindowBounds(client);
	EsRectangle windowBoundsInScreen = EsWindowGetBounds(window);
	return Translate(elementBoundsInWindow, windowBoundsInScreen.l, windowBoundsInScreen.t);
}

void EsElementInsertAfter(EsElement *element) {
	EsAssert(!gui.insertAfter);
	gui.insertAfter = element;
}

void EsElement::Initialise(EsElement *_parent, uint64_t _flags, EsUICallbackFunction _classCallback, const EsStyle *style) {
	EsMessageMutexCheck();

	// EsPrint("New element '%z' %x with parent %x.\n", _debugName, this, _parent);

	classCallback = _classCallback;
	flags = _flags;

	if (gui.insertAfter) {
		if (_parent) {
			EsAssert(_parent == gui.insertAfter->parent);
		} else {
			_parent = gui.insertAfter->parent;
		}
	}

	if (_parent) {
		if (!_parent->parent && (~_flags & ES_ELEMENT_NON_CLIENT)) {
			_parent = WindowGetMainPanel((EsWindow *) _parent);
		}

		parent = _parent;
		window = parent->window;
		instance = window->instance;

		if (parent->flags & ES_ELEMENT_DISABLED) flags |= ES_ELEMENT_DISABLED;

		if (~flags & ES_ELEMENT_NON_CLIENT) {
			EsMessage m = {};
			m.type = ES_MSG_PRE_ADD_CHILD;
			m.child = this;
			EsMessageSend(parent, &m);
		}

		if (gui.insertAfter) {
			bool found = false;

			for (uintptr_t i = 0; i < arrlenu(parent->children); i++) {
				if (parent->children[i] == gui.insertAfter) {
					found = true;
					arrins(parent->children, i + 1, this);
					break;
				}
			}

			EsAssert(found);
			gui.insertAfter = nullptr;
		} else if (flags & ES_ELEMENT_NON_CLIENT) {
			arrput(parent->children, this);
		} else {
			for (uintptr_t i = arrlen(parent->children);; i--) {
				if (i == 0 || (~parent->children[i - 1]->flags & ES_ELEMENT_NON_CLIENT)) {
					arrins(parent->children, i, this);
					break;
				}
			}
		}

		if (~flags & ES_ELEMENT_NON_CLIENT) {
			EsMessage m = {};
			m.type = ES_MSG_ADD_CHILD;
			m.child = this;
			EsMessageSend(parent, &m);
		}

		EsElementUpdateContentSize(parent);
	}

	SetStylePart(style, false);
	RefreshStyle();
	InspectorNotifyElementCreated(this);
}

EsRectangle EsElementGetInsets(EsElement *element) {
	EsMessageMutexCheck();
	return element->currentStyle->insets;
}

EsThemeMetrics EsElementGetMetrics(EsElement *element) {
	EsMessageMutexCheck();
	EsThemeMetrics m = {};
	ThemeMetrics *metrics = element->currentStyle->metrics;
#define RECTANGLE_8_TO_ES_RECTANGLE(x) { (int32_t) (x).l, (int32_t) (x).r, (int32_t) (x).t, (int32_t) (x).b }
	m.insets = RECTANGLE_8_TO_ES_RECTANGLE(metrics->insets);
	m.clipInsets = RECTANGLE_8_TO_ES_RECTANGLE(metrics->clipInsets);
	m.globalOffset = RECTANGLE_8_TO_ES_RECTANGLE(metrics->globalOffset);
	m.clipEnabled = metrics->clipEnabled;
	m.cursor = metrics->cursor;
	m.entranceTransition = metrics->entranceTransition;
	m.exitTransition = metrics->exitTransition;
	m.entranceDuration = metrics->entranceDuration;
	m.exitDuration = metrics->exitDuration;
	m.preferredWidth = metrics->preferredWidth;
	m.preferredHeight = metrics->preferredHeight;
	m.minimumWidth = metrics->minimumWidth;
	m.minimumHeight = metrics->minimumHeight;
	m.maximumWidth = metrics->maximumWidth;
	m.maximumHeight = metrics->maximumHeight;
	m.gapMajor = metrics->gapMajor;
	m.gapMinor = metrics->gapMinor;
	m.gapWrap = metrics->gapWrap;
	m.textColor = metrics->textColor;
	m.selectedBackground = metrics->selectedBackground;
	m.selectedText = metrics->selectedText;
	m.iconColor = metrics->iconColor;
	m.textAlign = metrics->textAlign;
	m.textSize = metrics->textSize;
	m.fontFamily = metrics->fontFamily;
	m.fontWeight = metrics->fontWeight;
	m.iconSize = metrics->iconSize;
	m.isItalic = metrics->isItalic;
	m.ellipsis = metrics->ellipsis;
	m.wrapText = metrics->wrapText;
	return m;
}

void EsElementSetCallback(EsElement *element, EsUICallbackFunction callback) {
	EsMessageMutexCheck();
	element->userCallback = callback;
}

void EsElementGroupSetCallback(EsElementGroup *group, EsUICallbackFunction callback) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < arrlenu(group->elements); i++) {
		EsElementSetCallback(group->elements[i], callback);
	}
}

void EsElementSetHidden(EsElement *element, bool hidden) {
	EsMessageMutexCheck();

	bool old = element->flags & ES_ELEMENT_HIDDEN;
	if (old == hidden) return;

	if (hidden) {
		element->flags |= ES_ELEMENT_HIDDEN;
	} else {
		element->flags &= ~ES_ELEMENT_HIDDEN;
	}

	EsElementUpdateContentSize(element->parent);
	MaybeRemoveFocusedElement(element->window);
}

void EsElementGroupSetHidden(EsElementGroup *group, bool hidden) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < arrlenu(group->elements); i++) {
		EsElementSetHidden(group->elements[i], hidden);
	}
}

void EsElementSetDisabled(EsElement *element, bool disabled) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < element->GetChildCount(); i++) {
		if (element->GetChild(i)->flags & ES_ELEMENT_NON_CLIENT) continue;
		EsElementSetDisabled(element->GetChild(i), disabled);
	}

	if ((element->flags & ES_ELEMENT_DISABLED) && disabled) return;
	if ((~element->flags & ES_ELEMENT_DISABLED) && !disabled) return;

	if (disabled) element->flags |= ES_ELEMENT_DISABLED;
	else element->flags &= ~ES_ELEMENT_DISABLED;

	element->MaybeRefreshStyle();

	if (element->window->focused == element) {
		element->window->focused = nullptr;
	}
}

void EsElementGroupSetDisabled(EsElementGroup *group, bool disabled) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < arrlenu(group->elements); i++) {
		EsElementSetDisabled(group->elements[i], disabled);
	}
}

void EsElementDestroy(EsElement *element) {
	EsMessageMutexCheck();

	element->Destroy();
}

void EsElementDestroyContents(EsElement *element) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < element->GetChildCount(); i++) {
		if (element->GetChild(i)->flags & ES_ELEMENT_NON_CLIENT) continue;
		element->GetChild(i)->Destroy();
	}

	EsMessage m = {};
	m.type = ES_MSG_DESTROY_CONTENTS;
	EsMessageSend(element, &m);
}

EsElement *TabTraversalGetChild(EsElement *element, uintptr_t index) {
	if (element->flags & ES_ELEMENT_LAYOUT_HINT_REVERSE) {
		return element->children[arrlenu(element->children) - index - 1];
	} else {
		return element->children[index];
	}
}

EsElement *TabTraversalDo(EsElement *element, bool shift) {
	if (shift) {
		if (element->parent && TabTraversalGetChild(element->parent, 0) != element) {
			for (uintptr_t i = 1; i < arrlenu(element->parent->children); i++) {
				if (TabTraversalGetChild(element->parent, i) == element) {
					element = TabTraversalGetChild(element->parent, i - 1);

					while (arrlen(element->children)) {
						element = TabTraversalGetChild(element, arrlen(element->children) - 1);
					}

					return element;
				}
			}
		} else if (element->parent) {
			return element->parent;
		} else {
			while (arrlen(element->children)) {
				element = TabTraversalGetChild(element, arrlen(element->children) - 1);
			}

			return element;
		}
	} else {
		if (arrlen(element->children)) {
			return TabTraversalGetChild(element, 0);
		} else while (element->parent) {
			EsElement *child = element;
			element = element->parent;

			for (uintptr_t i = 0; i < arrlenu(element->children) - 1u; i++) {
				if (TabTraversalGetChild(element, i) == child) {
					element = TabTraversalGetChild(element, i + 1);
					return element;
				}
			}
		}
	}

	return element;
}

bool EsKeyboardIsAltHeld()   { return gui.holdingAlt;   }
bool EsKeyboardIsCtrlHeld()  { return gui.holdingCtrl;  }
bool EsKeyboardIsShiftHeld() { return gui.holdingShift; }

void EsStyleRefreshAll(EsElement *element) {
	EsMessageMutexCheck();

	element->RefreshStyle(nullptr, false, true);

	for (uintptr_t i = 0; i < arrlenu(element->children); i++) {
		if (element->children[i]) {
			EsStyleRefreshAll(element->children[i]);
		}
	}
}

void EsUISetDPI(int dpiScale) {
	EsMessageMutexCheck();

	if (dpiScale < 50) {
		dpiScale = 50;
	}

	themeScale = dpiScale / 100.0f;

	for (uintptr_t i = 0; i < arrlenu(gui.allWindows); i++) {
		EsStyleRefreshAll(gui.allWindows[i]);
	}
}

void EsElementGetSize(EsElement *element, int *width, int *height) {
	EsMessageMutexCheck();

	EsRectangle bounds = element->GetBounds();
	*width = bounds.r;
	*height = bounds.b;
}

void EsElementRepaint(EsElement *element, bool all, EsRectangle region) {
	EsMessageMutexCheck();

	element->Repaint(all, region);
}

bool EsElementStartAnimating(EsElement *element) {
	EsMessageMutexCheck();

	return element->StartAnimating();
}

EsElement *EsElementGetLayoutParent(EsElement *element) {
	EsMessageMutexCheck();

	return element->parent;
}

void EsElementDraw(EsElement *element, EsPainter *painter) {
	element->InternalPaint(painter, PAINT_SHADOW);
	element->InternalPaint(painter, ES_FLAGS_DEFAULT);
}

void MousePressReleased(EsWindow *window, EsMessage *message, bool sendClick) {
	if (window->pressed) {
		EsElement *pressed = window->pressed;
		window->pressed = nullptr;

		if (message) {
			EsRectangle bounds = pressed->GetWindowBounds();
			message->mouseDown.positionX -= bounds.l;
			message->mouseDown.positionY -= bounds.t;
			EsMessageSend(pressed, message);
		} else {
			EsMessage m = {};
			m.type = (EsMessageType) (gui.lastClickButton + 1);
			EsMessageSend(pressed, &m);
		}

		pressed->state &= ~UI_STATE_PRESSED;
		EsMessage m = { ES_MSG_PRESSED_END };
		EsMessageSend(pressed, &m);

		if (message && (pressed->state & UI_STATE_HOVERED) && message->type == ES_MSG_MOUSE_LEFT_UP && !gui.draggingStarted && sendClick
				/* && gui.lastClickChainCount == 1 */) { // TODO I think the click chain timer is too long for this to feel right..?
			m.type = ES_MSG_CLICKED;
			EsMessageSend(pressed, &m);
		}

		if (window->hovered) window->hovered->MaybeRefreshStyle();
	}

	gui.draggingStarted = false;
}

void InitialiseKeyboardShortcutNamesTable() {
	uint32_t scancode; const char *name;
	scancode = ES_SCANCODE_A; name = "A"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_B; name = "B"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_C; name = "C"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_D; name = "D"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_E; name = "E"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F; name = "F"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_G; name = "G"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_H; name = "H"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_I; name = "I"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_J; name = "J"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_K; name = "K"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_L; name = "L"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_M; name = "M"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_N; name = "N"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_O; name = "O"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_P; name = "P"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_Q; name = "Q"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_R; name = "R"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_S; name = "S"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_T; name = "T"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_U; name = "U"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_V; name = "V"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_W; name = "W"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_X; name = "X"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_Y; name = "Y"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_Z; name = "Z"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_0; name = "0"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_1; name = "1"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_2; name = "2"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_3; name = "3"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_4; name = "4"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_5; name = "5"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_6; name = "6"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_7; name = "7"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_8; name = "8"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_9; name = "9"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_BACKSPACE; name = "Backspace"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_ESCAPE; name = "Esc"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_INSERT; name = "Ins"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_HOME; name = "Home"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PAGE_UP; name = "PgUp"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_DELETE; name = "Del"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_END; name = "End"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PAGE_DOWN; name = "PgDn"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_UP_ARROW; name = "Up"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_LEFT_ARROW; name = "Left"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_DOWN_ARROW; name = "Down"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_RIGHT_ARROW; name = "Right"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_SPACE; name = "Space"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_TAB; name = "Tab"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_ENTER; name = "Enter"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F1; name = "F1"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F2; name = "F2"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F3; name = "F3"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F4; name = "F4"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F5; name = "F5"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F6; name = "F6"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F7; name = "F7"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F8; name = "F8"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F9; name = "F9"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F10; name = "F10"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F11; name = "F11"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_F12; name = "F12"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_NEXT; name = "Next Track"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_PREVIOUS; name = "Previous Track"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_STOP; name = "Stop Media"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_PAUSE; name = "Play/Pause"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_MUTE; name = "Mute"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_QUIETER; name = "Quieter"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_LOUDER; name = "Louder"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_MM_SELECT; name = "Open Media"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_SEARCH; name = "Search"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_HOME; name = "Homepage"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_BACK; name = "Back"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_FORWARD; name = "Forward"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_STOP; name = "Stop"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_REFRESH; name = "Refresh"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_WWW_STARRED; name = "Bookmark"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_SLASH; name = "/"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PUNCTUATION_1; name = "\\"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_LEFT_BRACE; name = "("; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_RIGHT_BRACE; name = ")"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_EQUALS; name = "="; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PUNCTUATION_5; name = "`"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_HYPHEN; name = "-"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PUNCTUATION_3; name = ";"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PUNCTUATION_4; name = "'"; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_COMMA; name = ","; hmput(gui.keyboardShortcutNames, scancode, name);
	scancode = ES_SCANCODE_PERIOD; name = "."; hmput(gui.keyboardShortcutNames, scancode, name);
}

void AccessKeysGather(EsElement *element) {
	if (element->flags & ES_ELEMENT_BLOCK_FOCUS) return;
	if (element->state & UI_STATE_BLOCK_INTERACTION) return;
	if (element->flags & ES_ELEMENT_HIDDEN) return;

	for (uintptr_t i = 0; i < arrlenu(element->children); i++) {
		AccessKeysGather(element->children[i]);
	}

	if (!element->accessKey) return;
	if (element->state & UI_STATE_DESTROYING) return;
	if (element->flags & ES_ELEMENT_DISABLED) return;

	EsRectangle bounds = element->GetWindowBounds();

	AccessKeyEntry entry = {};
	entry.character = element->accessKey;
	entry.number = gui.accessKeys.numbers[entry.character - 'A'];
	entry.element = element;

	if (element->flags & ES_ELEMENT_CENTER_ACCESS_KEY_HINT) {
		entry.x = (bounds.l + bounds.r) / 2;
		entry.y = (bounds.t + bounds.b) / 2;
	} else {
		entry.x = (bounds.l + bounds.r) / 2;
		entry.y = bounds.b;
	}

	if (entry.number >= 10) return;

	arrput(gui.accessKeys.entries, entry);
	gui.accessKeys.numbers[entry.character - 'A']++;
}

void AccessKeyHintsShow(EsPainter *painter) {
	for (uintptr_t i = 0; i < arrlenu(gui.accessKeys.entries); i++) {
		AccessKeyEntry *entry = gui.accessKeys.entries + i;
		UIStyle *style = gui.accessKeys.hintStyle;

		if (gui.accessKeys.typedCharacter && entry->character != gui.accessKeys.typedCharacter) {
			continue;
		}

		EsRectangle bounds = ES_MAKE_RECTANGLE(
					entry->x - style->preferredWidth / 2, 
					entry->x + style->preferredWidth / 2, 
					entry->y - style->preferredHeight / 4, 
					entry->y + 3 * style->preferredHeight / 4);

		if (entry->element->flags & ES_ELEMENT_CENTER_ACCESS_KEY_HINT) {
			bounds.t -= style->preferredHeight / 4;
			bounds.b -= style->preferredHeight / 4;
		}

		style->PaintLayers(painter, Width(bounds), Height(bounds), bounds.l, bounds.t, 0, false);

		char c = gui.accessKeys.typedCharacter ? entry->number + '0' : entry->character;
		style->PaintText(painter, bounds, &c, 1);
	}
}

void AccessKeyModeEnter(EsWindow *window) {
	if (window->hasDialog || gui.menuMode || gui.accessKeyMode || window->windowStyle != ES_WINDOW_NORMAL) {
		return;
	}

	gui.accessKeyMode = true;
	AccessKeysGather(window);

	if (!gui.accessKeys.hintStyle) {
		gui.accessKeys.hintStyle = GetStyle(MakeStyleKey(ES_STYLE_ACCESS_KEY_HINT, 0), false);
	}

	for (uintptr_t i = 0; i < arrlenu(gui.accessKeys.entries); i++) {
		if (gui.accessKeys.numbers[gui.accessKeys.entries[i].character - 'A'] == 1) {
			gui.accessKeys.entries[i].number = -1;
		}
	}

	gui.accessKeys.window = window; 
	window->Repaint(true);
}

void AccessKeyModeExit() {
	if (!gui.accessKeyMode) {
		return;
	}

	arrfree(gui.accessKeys.entries);
	gui.accessKeys.window->Repaint(true);
	EsMemoryZero(gui.accessKeys.numbers, sizeof(gui.accessKeys.numbers));
	gui.accessKeys.typedCharacter = 0;
	gui.accessKeys.window = nullptr;
	gui.accessKeyMode = false;
}

void AccessKeyModeHandleKeyPress(EsMessage *message) {
	int ic, isc;
	ConvertScancodeToCharacter(message->keyboard.scancode, ic, isc, false, false);
	ic = EsCRTtoupper(ic);

	bool keepAccessKeyModeActive = false;

	if (ic >= 'A' && ic <= 'Z' && !gui.accessKeys.typedCharacter) {
		if (gui.accessKeys.numbers[ic - 'A'] > 1) {
			keepAccessKeyModeActive = true;
			gui.accessKeys.typedCharacter = ic;
		} else if (gui.accessKeys.numbers[ic - 'A'] == 1) {
			for (uintptr_t i = 0; i < arrlenu(gui.accessKeys.entries); i++) {
				AccessKeyEntry *entry = gui.accessKeys.entries + i;

				if (entry->character == ic) {
					EsMessage m = { ES_MSG_CLICKED };
					EsMessageSend(entry->element, &m);
					EsElementFocus(entry->element, true);

					keepAccessKeyModeActive = entry->element->flags & ES_ELEMENT_STICKY_ACCESS_KEY;
				}
			}
		}
	} else if (ic >= '0' && ic <= '9' && gui.accessKeys.typedCharacter) {
		for (uintptr_t i = 0; i < arrlenu(gui.accessKeys.entries); i++) {
			AccessKeyEntry *entry = gui.accessKeys.entries + i;

			if (entry->character == gui.accessKeys.typedCharacter && entry->number == ic - '0') {
				EsMessage m = { ES_MSG_CLICKED };
				EsMessageSend(entry->element, &m);
				EsElementFocus(entry->element, true);

				keepAccessKeyModeActive = entry->element->flags & ES_ELEMENT_STICKY_ACCESS_KEY;
			}
		}
	}

	if (!keepAccessKeyModeActive) {
		AccessKeyModeExit();
	} else {
		gui.accessKeys.window->Repaint(true);
	}
}

void RefreshPrimaryClipboard(EsWindow *window) {
	if (window->focused) {
		EsMessage m = {};
		m.type = ES_MSG_PRIMARY_CLIPBOARD_UPDATED;
		EsMessageSend(window->focused, &m);
	}
}

void HandleKeyPress(EsWindow *window, EsMessage *message) {
	if (window->targetMenu) {
		window = window->targetMenu;
	}

	bool ctrl = message->keyboard.ctrl, alt = message->keyboard.alt, shift = message->keyboard.shift;

	gui.unhandledAltPress = false;

	if (message->keyboard.scancode == ES_SCANCODE_F2 && alt) {
		EnterDebugger();
		EsPrint("[Alt-F2]\n");
	}

	if (message->keyboard.scancode == ES_SCANCODE_LEFT_CTRL  ) gui.holdingCtrl  = true;
	if (message->keyboard.scancode == ES_SCANCODE_LEFT_ALT   ) gui.holdingAlt   = true;
	if (message->keyboard.scancode == ES_SCANCODE_LEFT_SHIFT ) gui.holdingShift = true;
	if (message->keyboard.scancode == ES_SCANCODE_RIGHT_CTRL ) gui.holdingCtrl  = true;
	if (message->keyboard.scancode == ES_SCANCODE_RIGHT_ALT  ) gui.holdingAlt   = true;
	if (message->keyboard.scancode == ES_SCANCODE_RIGHT_SHIFT) gui.holdingShift = true;

	if (window->windowStyle == ES_WINDOW_MENU && message->keyboard.scancode == ES_SCANCODE_ESCAPE) {
		window->Destroy();
		return;
	}

	if (gui.menuMode) {
		// TODO Check the window is the one that enabled menu mode.
		// TODO Escape to close/exit menu mode.
		// TODO Left/right to navigate columns/cycle menubar and open/close submenus.
		// TODO Up/down to traverse menu.
		// TODO Enter to open submenu/invoke item.
		return;
	} else if (gui.accessKeyMode) {
		if (gui.accessKeys.window != window) {
			AccessKeyModeExit();
		} else {
			AccessKeyModeHandleKeyPress(message);
			return;
		}
	}

	if (window->pressed) {
		if (message->keyboard.scancode == ES_SCANCODE_ESCAPE) {
			MousePressReleased(window, nullptr, false);
			return;
		}
	}

	if (window->focused) {
		message->type = ES_MSG_KEY_TYPED;
		if (EsMessageSend(window->focused, message) != 0) return;

		EsElement *element = window->focused;
		message->type = ES_MSG_KEY_DOWN;

		while (element) {
			if (EsMessageSend(element, message) != 0) return;
			element = element->parent;
		}

		if (message->keyboard.scancode == ES_SCANCODE_TAB && !alt && !ctrl) {
			element = window->focused;

			do element = TabTraversalDo(element, shift);
			while (!element->IsFocusable() || (element->flags & ES_ELEMENT_NOT_TAB_TRAVERSABLE));

			EsElementFocus(element, true);
			return;
		}

		if (message->keyboard.scancode == ES_SCANCODE_SPACE && !alt && !ctrl && !shift) {
			EsMessage m = { ES_MSG_CLICKED };
			EsMessageSend(window->focused, &m);
			return;
		}

		// TODO Radio group navigation.
	} else {
		if (message->keyboard.scancode == ES_SCANCODE_TAB && !alt && !ctrl) {
			EsElement *element = window;

			do element = TabTraversalDo(element, shift);
			while (!element->IsFocusable() || (element->flags & ES_ELEMENT_NOT_TAB_TRAVERSABLE));

			EsElementFocus(element, true);
			return;
		}

		EsMessageSend(window, message);
	}

	if (window->enterButton && message->keyboard.scancode == ES_SCANCODE_ENTER && !shift && !ctrl && !alt 
			&& window->enterButton->onCommand && (~window->enterButton->flags & ES_ELEMENT_DISABLED)) {
		window->enterButton->onCommand(window->instance, window->enterButton, window->enterButton->command);
	} else if (window->escapeButton && message->keyboard.scancode == ES_SCANCODE_ESCAPE && !shift && !ctrl && !alt
			&& window->escapeButton->onCommand && (~window->escapeButton->flags & ES_ELEMENT_DISABLED)) {
		window->escapeButton->onCommand(window->instance, window->escapeButton, window->escapeButton->command);
	}

	if (!window->hasDialog) {
		// TODO Sort out what commands can be used from within dialogs and menus.

		if (message->keyboard.scancode == ES_SCANCODE_LEFT_ALT) {
			gui.unhandledAltPress = true;
			return;
		}

		if (!gui.keyboardShortcutNames) InitialiseKeyboardShortcutNamesTable();
		const char *shortcutName = hmget(gui.keyboardShortcutNames, message->keyboard.scancode);

		if (shortcutName && window->instance && window->instance->_private) {
			ApplicationInstance *instance = (ApplicationInstance *) window->instance->_private;

			char keyboardShortcutString[128];
			size_t bytes = EsStringFormat(keyboardShortcutString, 128, "%z%z%z%z%c",
					ctrl ? "Ctrl+" : "", shift ? "Shift+" : "", alt ? "Alt+" : "", shortcutName, 0) - 1;

			for (uintptr_t i = 0; i < hmlenu(instance->commands); i++) {
				EsCommand *command = instance->commands[i].value;
				if (!command->cKeyboardShortcut || command->disabled) continue;
				const char *position = EsCRTstrstr(command->cKeyboardShortcut, keyboardShortcutString);
				if (!position) continue;

				if ((position[bytes] == 0 || position[bytes] == '|') && (position == command->cKeyboardShortcut || position[-1] == '|')) {
					if (command->callback) {
						command->callback(window->instance, nullptr, command);
					}

					return;
				}
			}
		}
	}
}

void UIProcessWindowManagerMessage(EsWindow *window, EsMessage *message, ProcessMessageTiming *timing) {
	if (window->isSpecial) {
		return;
	}

	// Check if the window has been destroyed.

	if (message->type == ES_MSG_WINDOW_DESTROYED) {
		EsAssert(window->handle == ES_INVALID_HANDLE); // Window destroyed, but handle not closed.
		EsHeapFree(window);

		for (uintptr_t i = 0; i < arrlenu(gui.allWindows); i++) {
			if (gui.allWindows[i] == window) {
				arrdelswap(gui.allWindows, i);
			}
		}

		return;
	} else if (window->handle == ES_INVALID_HANDLE) {
		return;
	}

	// Begin update.

	window->willUpdate = true;

	ProcessAnimations();
	FindHoverElement(window);

	// Process input message.

	if (timing) timing->startLogic = EsTimeStampMs();

	if (message->type == ES_MSG_MOUSE_MOVED) {
		if (window->pressed) {
			if (gui.draggingStarted || DistanceSquared(message->mouseMoved.newPositionX - gui.lastClickX, 
						message->mouseMoved.newPositionY - gui.lastClickY) >= GetConstantNumber("dragThreshold")) {
				message->mouseDragged.originalPositionX = gui.lastClickX;
				message->mouseDragged.originalPositionY = gui.lastClickY;

				message->type = ES_MSG_MOUSE_DRAGGED;

				{
					EsRectangle bounds = window->pressed->GetWindowBounds();
					message->mouseDragged.oldPositionX -= bounds.l;
					message->mouseDragged.oldPositionY -= bounds.t;
					message->mouseDragged.newPositionX -= bounds.l;
					message->mouseDragged.newPositionY -= bounds.t;
					message->mouseDragged.originalPositionX -= bounds.l;
					message->mouseDragged.originalPositionY -= bounds.t;
				}

				if (ES_HANDLED == EsMessageSend(window->pressed, message)) {
					gui.draggingStarted = true;
				}
			}
		} else {
			{
				EsRectangle bounds = window->hovered->GetWindowBounds();
				message->mouseMoved.oldPositionX -= bounds.l;
				message->mouseMoved.oldPositionY -= bounds.t;
				message->mouseMoved.newPositionX -= bounds.l;
				message->mouseMoved.newPositionY -= bounds.t;
			}

			EsMessageSend(window->hovered, message);
		}

		window->hovering = true;
	} else if (message->type == ES_MSG_MOUSE_EXIT) {
		window->hovering = false;
	} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN || message->type == ES_MSG_MOUSE_RIGHT_DOWN || message->type == ES_MSG_MOUSE_MIDDLE_DOWN) {
		AccessKeyModeExit();

		if (!gui.mouseButtonDown) {
			gui.lastClickX = message->mouseDown.positionX;
			gui.lastClickY = message->mouseDown.positionY;
			gui.lastClickChainCount = message->mouseDown.clickChainCount;
			gui.lastClickWasActivation = message->mouseDown.activationClick;
			gui.lastClickButton = message->type;
			gui.mouseButtonDown = true;

			if ((~window->hovered->flags & ES_ELEMENT_DISABLED) && (~window->hovered->state & UI_STATE_BLOCK_INTERACTION)) {
				// If the hovered element is destroyed in response to one of these messages, 
				// window->hovered will be set to nullptr, so save the element here.
				EsElement *element = window->hovered;

				element->state |= UI_STATE_PRESSED;
				window->pressed = element;
				EsMessage m = { ES_MSG_PRESSED_START };
				EsMessageSend(element, &m);

				EsRectangle bounds = element->GetWindowBounds();
				message->mouseDown.positionX -= bounds.l;
				message->mouseDown.positionY -= bounds.t;

				if (ES_REJECTED != EsMessageSend(element, message)) {
					EsElementFocus(element, false);
				}
			}

			if (window->hovered != window->focused && window->focused && (~window->focused->state & UI_STATE_LOST_STRONG_FOCUS)) {
				EsMessage m = { ES_MSG_STRONG_FOCUS_END };
				window->focused->state |= UI_STATE_LOST_STRONG_FOCUS;
				EsMessageSend(window->focused, &m);
			}
		}
	} else if (message->type == ES_MSG_MOUSE_LEFT_UP || message->type == ES_MSG_MOUSE_RIGHT_UP || message->type == ES_MSG_MOUSE_MIDDLE_UP) {
		AccessKeyModeExit();

		if (gui.mouseButtonDown && gui.lastClickButton == message->type - 1) {
			gui.mouseButtonDown = false;
			MousePressReleased(window, message, true);
		}
	} else if (message->type == ES_MSG_KEY_UP) {
		if (message->keyboard.scancode == ES_SCANCODE_LEFT_CTRL  ) gui.holdingCtrl  = false;
		if (message->keyboard.scancode == ES_SCANCODE_LEFT_ALT   ) gui.holdingAlt   = false;
		if (message->keyboard.scancode == ES_SCANCODE_LEFT_SHIFT ) gui.holdingShift = false;
		if (message->keyboard.scancode == ES_SCANCODE_RIGHT_CTRL ) gui.holdingCtrl  = false;
		if (message->keyboard.scancode == ES_SCANCODE_RIGHT_ALT  ) gui.holdingAlt   = false;
		if (message->keyboard.scancode == ES_SCANCODE_RIGHT_SHIFT) gui.holdingShift = false;

		if (message->keyboard.scancode == ES_SCANCODE_LEFT_ALT && gui.unhandledAltPress) {
			AccessKeyModeEnter(window);
		}

		if (window->focused) EsMessageSend(window->focused, message);
	} else if (message->type == ES_MSG_KEY_DOWN) {
		HandleKeyPress(window, message);
	} else if (message->type == ES_MSG_WINDOW_RESIZED) {
		AccessKeyModeExit();

		window->receivedFirstResize = true;

		window->width = window->windowWidth = message->windowResized.content.r;
		window->height = window->windowHeight = message->windowResized.content.b;

		if (window->windowStyle == ES_WINDOW_CONTAINER) {
			EsRectangle opaqueBounds = ES_MAKE_RECTANGLE(WINDOW_INSET, window->windowWidth - WINDOW_INSET, 
					WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT, window->windowHeight - WINDOW_INSET);
			EsSyscall(ES_SYSCALL_WINDOW_SET_OPAQUE_BOUNDS, window->handle, (uintptr_t) &opaqueBounds, 0, 0);
		} else if (window->windowStyle == ES_WINDOW_INSPECTOR) {
			EsRectangle opaqueBounds = ES_MAKE_RECTANGLE(0, window->windowWidth, 0, window->windowHeight);
			EsSyscall(ES_SYSCALL_WINDOW_SET_OPAQUE_BOUNDS, window->handle, (uintptr_t) &opaqueBounds, 0, 0);
		}

		if (!message->windowResized.hidden) {
			EsElementRelayout(window);
			window->Repaint(true);
		}

		EsMessageSend(window, message);
	} else if (message->type == ES_MSG_WINDOW_DEACTIVATED) {
		AccessKeyModeExit();

		if (window->windowStyle == ES_WINDOW_MENU) {
			window->Destroy();
		} else if (window->windowStyle == ES_WINDOW_CONTAINER) {
			// Redraw window borders.
			EsSyscall(ES_SYSCALL_WINDOW_REDRAW, window->handle, 0, 0, 0);
		}

		window->activated = false;

		if (window->focused) {
			window->inactiveFocus = window->focused;
			window->inactiveFocus->Repaint(true);
			RemoveFocusFromElement(window->focused);
			window->focused = nullptr;
		}

		EsMessageSend(window, message);
	} else if (message->type == ES_MSG_WINDOW_ACTIVATED) {
		gui.holdingShift = gui.holdingCtrl = gui.holdingAlt = false;
		window->activated = true;
		EsMessage m = { ES_MSG_WINDOW_ACTIVATED };
		EsMessageSend(window, &m);

		if (!window->focused && window->inactiveFocus) {
			EsElementFocus(window->inactiveFocus, false);
			window->inactiveFocus->Repaint(true);
			window->inactiveFocus = nullptr;
		}

		RefreshPrimaryClipboard(window);
	} else if (message->type == ES_MSG_PRIMARY_CLIPBOARD_UPDATED) {
		RefreshPrimaryClipboard(window);
	}

	if (timing) timing->endLogic = EsTimeStampMs();

	// Destroy, relayout, and repaint elements as necessary.

	if (window->InternalDestroy()) {
		// The window has been destroyed.
		return;
	}

	FindHoverElement(window);

	EsCursorStyle cursorStyle = ES_CURSOR_NORMAL;
	if (window->pressed) cursorStyle = window->pressed->GetCursor();
	else if (window->hovered) cursorStyle = window->hovered->GetCursor();
	bool changedCursor;

	{
		// TODO Make these configurable in the theme file!

		int x, y, ox, oy, w, h;

#define CURSOR(_constant, _x, _y, _ox, _oy, _w, _h) _constant: { x = _x; y = _y; ox = -_ox; oy = -_oy; w = _w; h = _h; } break

		switch (cursorStyle) {
			CURSOR(case ES_CURSOR_TEXT, 84, 60, 4, 10, 11, 22);
			CURSOR(case ES_CURSOR_RESIZE_VERTICAL, 44, 12, 4, 11, 11, 24);
			CURSOR(case ES_CURSOR_RESIZE_HORIZONTAL, 68, 15, 11, 4, 24, 11);
			CURSOR(case ES_CURSOR_RESIZE_DIAGONAL_1, 55, 35, 8, 8, 19, 19);
			CURSOR(case ES_CURSOR_RESIZE_DIAGONAL_2, 82, 35, 8, 8, 19, 19);
			CURSOR(case ES_CURSOR_SPLIT_VERTICAL, 37, 55, 4, 10, 12, 24);
			CURSOR(case ES_CURSOR_SPLIT_HORIZONTAL, 56, 60, 10, 4, 24, 12);
			CURSOR(case ES_CURSOR_HAND_HOVER, 131, 14, 8, 1, 21, 26);
			CURSOR(case ES_CURSOR_HAND_DRAG, 107, 18, 7, 1, 20, 22);
			CURSOR(case ES_CURSOR_HAND_POINT, 159, 14, 5, 1, 21, 26);
			CURSOR(case ES_CURSOR_SCROLL_UP_LEFT, 217, 89, 9, 9, 16, 16);
			CURSOR(case ES_CURSOR_SCROLL_UP, 193, 87, 5, 11, 13, 18);
			CURSOR(case ES_CURSOR_SCROLL_UP_RIGHT, 234, 89, 4, 9, 16, 16);
			CURSOR(case ES_CURSOR_SCROLL_LEFT, 175, 93, 11, 5, 18, 13);
			CURSOR(case ES_CURSOR_SCROLL_CENTER, 165, 51, 12, 12, 28, 28);
			CURSOR(case ES_CURSOR_SCROLL_RIGHT, 194, 106, 4, 4, 18, 13);
			CURSOR(case ES_CURSOR_SCROLL_DOWN_LEFT, 216, 106, 10, 4, 16, 16);
			CURSOR(case ES_CURSOR_SCROLL_DOWN, 182, 107, 4, 3, 12, 16);
			CURSOR(case ES_CURSOR_SCROLL_DOWN_RIGHT, 234, 106, 4, 4, 16, 16);
			CURSOR(case ES_CURSOR_SELECT_LINES, 7, 19, 10, 0, 16, 23);
			CURSOR(case ES_CURSOR_DROP_TEXT, 11, 48, 1, 1, 21, 28);
			CURSOR(case ES_CURSOR_CROSS_HAIR_PICK, 104, 56, 11, 11, 26, 26);
			CURSOR(case ES_CURSOR_CROSS_HAIR_RESIZE, 134, 53, 11, 11, 26, 26);
			CURSOR(case ES_CURSOR_MOVE_HOVER, 226, 13, 10, 10, 24, 32);
			CURSOR(case ES_CURSOR_MOVE_DRAG, 197, 13, 10, 10, 24, 24);
			CURSOR(case ES_CURSOR_ROTATE_HOVER, 228, 49, 9, 10, 24, 32);
			CURSOR(case ES_CURSOR_ROTATE_DRAG, 198, 49, 9, 10, 22, 22);
			CURSOR(case ES_CURSOR_BLANK, 5, 13, 0, 0, 1, 1);
			CURSOR(default, 23, 18, 1, 1, 15, 24);
		}

		changedCursor = EsSyscall(ES_SYSCALL_WINDOW_SET_CURSOR, window->handle, (uintptr_t) themeCursors.bits + x * 4 + y * themeCursors.stride, 
				((0xFF & ox) << 0) | ((0xFF & oy) << 8) | ((0xFF & w) << 16) | ((0xFF & h) << 24), 
				themeCursors.stride | (cursorStyle << 24));
	}
	
	if (window->receivedFirstResize || window->windowStyle != ES_WINDOW_NORMAL) {
		if (timing) timing->startLayout = EsTimeStampMs();
		window->InternalMove(window->width, window->height, 0, 0);
		if (timing) timing->endLayout = EsTimeStampMs();
	}

	if (THEME_RECT_VALID(window->updateRegion) && window->width == (int) window->windowWidth && window->height == (int) window->windowHeight) {
		// Calculate the regions to repaint, and then perform painting.

		if (timing) timing->startPaint = EsTimeStampMs();

		EsRectangle updateRegion = window->updateRegion;

		if (window->windowStyle == ES_WINDOW_CONTAINER) {
			EsRectangle bounds = ES_MAKE_RECTANGLE(WINDOW_INSET, window->windowWidth - WINDOW_INSET, 
					WINDOW_INSET, WINDOW_INSET + CONTAINER_TAB_BAND_HEIGHT);
			EsRectangleClip(updateRegion, bounds, &updateRegion);
		} else {
			EsRectangle bounds = ES_MAKE_RECTANGLE(0, window->windowWidth, 0, window->windowHeight);
			EsRectangleClip(updateRegion, bounds, &updateRegion);
		}

		if (THEME_RECT_VALID(updateRegion)) {
			EsPainter painter = {};
			EsPaintTarget target = {};

			target.fullAlpha = window->windowStyle != ES_WINDOW_NORMAL;
			target.width = Width(updateRegion);
			target.height = Height(updateRegion);
			target.stride = target.width * 4;
			target.bits = EsHeapAllocate(target.stride * target.height, false);
			painter.offsetX -= updateRegion.l;
			painter.offsetY -= updateRegion.t;
			painter.clip = ES_MAKE_RECTANGLE(0, target.width, 0, target.height);
			painter.target = &target;

			if (window->windowStyle == ES_WINDOW_CONTAINER) {
				updateRegion = Translate(updateRegion, -WINDOW_INSET, -WINDOW_INSET);
			}

			window->InternalPaint(&painter, ES_FLAGS_DEFAULT);
			if (timing) timing->endPaint = EsTimeStampMs();

#if 0
			EsDrawRectangle(&painter, painter.clip, 0, EsRandomU64(), ES_MAKE_RECTANGLE_ALL(3));
#endif

			// Update the screen.
			if (timing) timing->startUpdate = EsTimeStampMs();
			EsSyscall(ES_SYSCALL_WINDOW_SET_BITS, window->handle, (uintptr_t) &updateRegion, (uintptr_t) target.bits, 0);
			if (timing) timing->endUpdate = EsTimeStampMs();

			EsHeapFree(target.bits);
		}

		window->updateRegion = ES_MAKE_RECTANGLE(window->windowWidth, 0, window->windowHeight, 0);
	} else if (changedCursor) {
		EsSyscall(ES_SYSCALL_FORCE_SCREEN_UPDATE, 0, 0, 0, 0);
	}

	if (window->instance) {
		ApplicationInstance *instance = (ApplicationInstance *) window->instance->_private;
		EsUndoEndGroup(instance->activeUndoManager);
	}

	// Free any unused styles.

	FreeUnusedStyles();

	// Finish update.

	window->willUpdate = false;
}

// --------------------------------- List view.

#include "list_view.cpp"

// --------------------------------- Inspector.

struct InspectorElementEntry {
	EsElement *element;
	EsRectangle takenBounds, givenBounds;
	int depth;
};

struct InspectorWindow : EsInstance {
	EsInstance *instance;

	EsWindow *window;

	EsListView *elementList;
	InspectorElementEntry *elements;
	InspectorElementEntry hoveredElement;

	intptr_t selectedElement;
	EsButton *alignH[6];
	EsButton *alignV[6];
	EsButton *direction[4];
	EsTextbox *styleTextbox;
	EsTextbox *contentTextbox;
	EsButton *addChildButton;
	EsButton *addSiblingButton;
};

EsListViewColumn inspectorElementListColumns[] = {
	{ "Name", -1, 0, 300 },
	{ "Bounds", -1, 0, 200 },
	{ "Information", -1, 0, 200 },
};

int InspectorElementItemCallback(EsElement *element, EsMessage *message) {
	InspectorWindow *inspector = (InspectorWindow *) element->instance;

	if (message->type == ES_MSG_HOVERED_START) {
		InspectorElementEntry *entry = inspector->elements + EsListViewGetIndexFromItem(element).u;
		if (entry->element->parent) entry->element->parent->Repaint(true);
		else entry->element->Repaint(true);
		inspector->hoveredElement = *entry;
	} else if (message->type == ES_MSG_HOVERED_END || message->type == ES_MSG_DESTROY) {
		InspectorElementEntry *entry = inspector->elements + EsListViewGetIndexFromItem(element).u;
		if (entry->element->parent) entry->element->parent->Repaint(true);
		else entry->element->Repaint(true);
		inspector->hoveredElement = {};
	}

	return 0;
}

void InspectorUpdateEditor(InspectorWindow *inspector) {
	EsElement *e = inspector->selectedElement == -1 ? nullptr : inspector->elements[inspector->selectedElement].element;

	bool isStack = e && e->classCallback == ProcessPanelMessage && !(e->flags & (ES_PANEL_Z_STACK | ES_PANEL_TABLE | ES_PANEL_SWITCHER));
	bool alignHLeft = e ? (e->flags & ES_CELL_H_LEFT) : false, alignHRight = e ? (e->flags & ES_CELL_H_RIGHT) : false;
	bool alignHExpand = e ? (e->flags & ES_CELL_H_EXPAND) : false, alignHShrink = e ? (e->flags & ES_CELL_H_SHRINK) : false; 
	bool alignHPush = e ? (e->flags & ES_CELL_H_PUSH) : false;
	bool alignVTop = e ? (e->flags & ES_CELL_V_TOP) : false, alignVBottom = e ? (e->flags & ES_CELL_V_BOTTOM) : false;
	bool alignVExpand = e ? (e->flags & ES_CELL_V_EXPAND) : false, alignVShrink = e ? (e->flags & ES_CELL_V_SHRINK) : false; 
	bool alignVPush = e ? (e->flags & ES_CELL_V_PUSH) : false;
	bool stackHorizontal = isStack && (e->flags & ES_PANEL_HORIZONTAL);
	bool stackReverse = isStack && (e->flags & ES_PANEL_REVERSE);

	EsButtonSetCheck(inspector->alignH[0], (EsCheckState) (e && alignHLeft && !alignHRight), false);
	EsButtonSetCheck(inspector->alignH[1], (EsCheckState) (e &&  alignHLeft == alignHRight), false);
	EsButtonSetCheck(inspector->alignH[2], (EsCheckState) (e && !alignHLeft && alignHRight), false);
	EsButtonSetCheck(inspector->alignH[3], (EsCheckState) (e && alignHExpand), false);
	EsButtonSetCheck(inspector->alignH[4], (EsCheckState) (e && alignHShrink), false);
	EsButtonSetCheck(inspector->alignH[5], (EsCheckState) (e && alignHPush), false);

	EsButtonSetCheck(inspector->alignV[0], (EsCheckState) (e && alignVTop && !alignVBottom), false);
	EsButtonSetCheck(inspector->alignV[1], (EsCheckState) (e &&  alignVTop == alignVBottom), false);
	EsButtonSetCheck(inspector->alignV[2], (EsCheckState) (e && !alignVTop && alignVBottom), false);
	EsButtonSetCheck(inspector->alignV[3], (EsCheckState) (e && alignVExpand), false);
	EsButtonSetCheck(inspector->alignV[4], (EsCheckState) (e && alignVShrink), false);
	EsButtonSetCheck(inspector->alignV[5], (EsCheckState) (e && alignVPush), false);

	EsButtonSetCheck(inspector->direction[0], (EsCheckState) (isStack &&  stackHorizontal &&  stackReverse), false);
	EsButtonSetCheck(inspector->direction[1], (EsCheckState) (isStack &&  stackHorizontal && !stackReverse), false);
	EsButtonSetCheck(inspector->direction[2], (EsCheckState) (isStack && !stackHorizontal &&  stackReverse), false);
	EsButtonSetCheck(inspector->direction[3], (EsCheckState) (isStack && !stackHorizontal && !stackReverse), false);

	EsElementSetDisabled(inspector->alignH[0], !e);
	EsElementSetDisabled(inspector->alignH[1], !e);
	EsElementSetDisabled(inspector->alignH[2], !e);
	EsElementSetDisabled(inspector->alignH[3], !e);
	EsElementSetDisabled(inspector->alignH[4], !e);
	EsElementSetDisabled(inspector->alignH[5], !e);
	EsElementSetDisabled(inspector->alignV[0], !e);
	EsElementSetDisabled(inspector->alignV[1], !e);
	EsElementSetDisabled(inspector->alignV[2], !e);
	EsElementSetDisabled(inspector->alignV[3], !e);
	EsElementSetDisabled(inspector->alignV[4], !e);
	EsElementSetDisabled(inspector->alignV[5], !e);
	EsElementSetDisabled(inspector->direction[0], !isStack);
	EsElementSetDisabled(inspector->direction[1], !isStack);
	EsElementSetDisabled(inspector->direction[2], !isStack);
	EsElementSetDisabled(inspector->direction[3], !isStack);
	EsElementSetDisabled(inspector->styleTextbox, !e);
	EsElementSetDisabled(inspector->addChildButton, !isStack);
	EsElementSetDisabled(inspector->addSiblingButton, !e || !e->parent);

	EsTextboxSelectAll(inspector->styleTextbox);
	EsTextboxInsert(inspector->styleTextbox, "", 0, false);
	EsTextboxSelectAll(inspector->contentTextbox);
	EsTextboxInsert(inspector->contentTextbox, "", 0, false);

	if (e) {
#if 0
		for (uintptr_t i = 0; i < sizeof(builtinStyles) / sizeof(builtinStyles[0]); i++) {
			if (e->currentStyleKey.partHash == CalculateCRC64(EsLiteral(builtinStyles[i]))) {
				EsTextboxInsert(inspector->styleTextbox, builtinStyles[i], -1, false);
				break;
			}
		}
#endif

		if (e->classCallback == ProcessButtonMessage) {
			EsButton *button = (EsButton *) e;
			EsElementSetDisabled(inspector->contentTextbox, false);
			EsTextboxInsert(inspector->contentTextbox, button->label, button->labelBytes, false);
		} else if (e->classCallback == ProcessTextDisplayMessage) {
			EsTextDisplay *display = (EsTextDisplay *) e;
			EsElementSetDisabled(inspector->contentTextbox, false);
			EsTextboxInsert(inspector->contentTextbox, display->contents, display->textRuns[display->textRunCount].offset, false);
		} else {
			EsElementSetDisabled(inspector->contentTextbox, true);
		}
	} else {
		EsElementSetDisabled(inspector->contentTextbox, true);
	}
}

int InspectorElementListCallback(EsElement *element, EsMessage *message) {
	InspectorWindow *inspector = (InspectorWindow *) element->instance;

	if (message->type == ES_MSG_LIST_VIEW_GET_CONTENT) {
		int column = message->getContent.column, index = message->getContent.index.i;
		EsAssert(index >= 0 && index < arrlen(inspector->elements));
		InspectorElementEntry *entry = inspector->elements + index;

		if (column == 0) {
			message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, "%z", entry->element->cName);
		} else if (column == 1) {
			message->getContent.bytes = EsStringFormat(message->getContent.buffer, message->getContent.bufferSpace, "%R", entry->element->GetWindowBounds(false));
		} else if (column == 2) {
			EsMessage m = *message;
			m.type = ES_MSG_GET_INSPECTOR_INFORMATION;
			EsMessageSend(entry->element, &m);
			message->getContent.bytes = m.getContent.bytes;
		}

		return ES_HANDLED;
	} else if (message->type == ES_MSG_LIST_VIEW_GET_INDENT) {
		message->getIndent.indent = inspector->elements[message->getIndent.index.i].depth;
		return ES_HANDLED;
	} else if (message->type == ES_MSG_LIST_VIEW_CREATE_ITEM) {
		message->createItem.parent->userCallback = InspectorElementItemCallback;
		return ES_HANDLED;
	} else if (message->type == ES_MSG_LIST_VIEW_SELECT) {
		inspector->selectedElement = message->selectItem.isSelected ? message->selectItem.index.i : -1;
		InspectorUpdateEditor(inspector);
		return ES_HANDLED;
	} else if (message->type == ES_MSG_LIST_VIEW_IS_SELECTED) {
		message->selectItem.isSelected = message->selectItem.index.i == inspector->selectedElement;
		return ES_HANDLED;
	}

	return 0;
}

int InspectorStyleTextboxCallback(EsElement *element, EsMessage *message) {
	InspectorWindow *inspector = (InspectorWindow *) element->instance;

	if (message->type == ES_MSG_TEXTBOX_EDIT_END) {
		(void) inspector;
#if 0
		char *newStyle = EsTextboxGetContents(inspector->styleTextbox, nullptr);
		EsElement *e = inspector->elements[inspector->selectedElement].element;
		e->SetStylePart(newStyle);
		EsElementUpdateContentSize(e);
		if (e->parent) EsElementUpdateContentSize(e->parent);
		EsHeapFree(newStyle);
#endif
		return ES_HANDLED;
	}

	return 0;
}

int InspectorContentTextboxCallback(EsElement *element, EsMessage *message) {
	InspectorWindow *inspector = (InspectorWindow *) element->instance;

	if (message->type == ES_MSG_TEXTBOX_EDIT_END) {
		size_t newContentBytes;
		char *newContent = EsTextboxGetContents(inspector->contentTextbox, &newContentBytes);
		EsElement *e = inspector->elements[inspector->selectedElement].element;

		if (e->classCallback == ProcessButtonMessage) {
			EsButton *button = (EsButton *) e;
			HeapDuplicate((void **) &button->label, newContent, newContentBytes);
			button->labelBytes = newContentBytes;
		} else if (e->classCallback == ProcessTextDisplayMessage) {
			EsTextDisplay *display = (EsTextDisplay *) e;
			EsTextDisplaySetContents(display, newContent, newContentBytes);
		} else {
			EsAssert(false);
		}

		EsElementUpdateContentSize(e);
		if (e->parent) EsElementUpdateContentSize(e->parent);
		EsHeapFree(newContent);
		return ES_HANDLED;
	}

	return 0;
}

InspectorWindow *InspectorGet(EsElement *element) {
	if (!element->window || !element->instance) return nullptr;
	ApplicationInstance *instance = (ApplicationInstance *) element->instance->_private;
	InspectorWindow *inspector = instance->attachedInspector;
	if (!inspector || inspector->instance->window != element->window) return nullptr;
	return inspector;
}

void InspectorNotifyElementContentChanged(EsElement *element) {
	InspectorWindow *inspector = InspectorGet(element);
	if (!inspector) return;

	for (uintptr_t i = 0; i < arrlenu(inspector->elements); i++) {
		if (inspector->elements[i].element == element) {
			EsListViewInvalidateContent(inspector->elementList, 0, i);
			return;
		}
	}

	EsAssert(false);
}

void InspectorNotifyElementMoved(EsElement *element, EsRectangle takenBounds, EsRectangle givenBounds) {
	InspectorWindow *inspector = InspectorGet(element);
	if (!inspector) return;

	for (uintptr_t i = 0; i < arrlenu(inspector->elements); i++) {
		if (inspector->elements[i].element == element) {
			inspector->elements[i].takenBounds = takenBounds;
			inspector->elements[i].givenBounds = givenBounds;
			EsListViewInvalidateContent(inspector->elementList, 0, i);
			return;
		}
	}

	EsAssert(false);
}

void InspectorNotifyElementDestroyed(EsElement *element) {
	InspectorWindow *inspector = InspectorGet(element);
	if (!inspector) return;

	for (uintptr_t i = 0; i < arrlenu(inspector->elements); i++) {
		if (inspector->elements[i].element == element) {
			if (inspector->selectedElement == (intptr_t) i) {
				inspector->selectedElement = -1;
				InspectorUpdateEditor(inspector);
			} else if (inspector->selectedElement > (intptr_t) i) {
				inspector->selectedElement--;
			}

			EsListViewRemove(inspector->elementList, 0, i, i);
			arrdel(inspector->elements, i);
			return;
		}
	}

	EsAssert(false);
}

void InspectorNotifyElementCreated(EsElement *element) {
	InspectorWindow *inspector = InspectorGet(element);
	if (!inspector) return;

	ptrdiff_t indexInParent = -1;

	for (uintptr_t i = 0; i < arrlenu(element->parent->children); i++) {
		if (element->parent->children[i] == element) {
			indexInParent = i;
			break;
		}
	}

	EsAssert(indexInParent != -1);

	ptrdiff_t insertAfterIndex = -1;

	for (uintptr_t i = 0; i < arrlenu(inspector->elements); i++) {
		if (indexInParent == 0) {
			if (inspector->elements[i].element == element->parent) {
				insertAfterIndex = i;
				break;
			}
		} else {
			if (inspector->elements[i].element == element->parent->children[indexInParent - 1]) {
				insertAfterIndex = i;
				int baseDepth = inspector->elements[i++].depth;

				for (; i < arrlenu(inspector->elements); i++) {
					if (inspector->elements[i].depth > baseDepth) {
						insertAfterIndex++;
					} else {
						break;
					}
				}

				break;
			}
		}
	}

	EsAssert(insertAfterIndex != -1);

	int depth = 0;
	EsElement *ancestor = element->parent;

	while (ancestor) {
		depth++;
		ancestor = ancestor->parent;
	}

	if (inspector->selectedElement > insertAfterIndex) {
		inspector->selectedElement++;
	}

	InspectorElementEntry entry;
	entry.element = element;
	entry.depth = depth;
	arrins(inspector->elements, insertAfterIndex + 1, entry);
	EsListViewInsert(inspector->elementList, 0, insertAfterIndex + 1, insertAfterIndex + 1);
}

void InspectorFindElementsRecursively(InspectorWindow *inspector, EsElement *element, int depth) {
	InspectorElementEntry entry = {};
	entry.element = element;
	entry.depth = depth;
	arrput(inspector->elements, entry);

	for (uintptr_t i = 0; i < arrlenu(element->children); i++) {
		InspectorFindElementsRecursively(inspector, element->children[i], depth + 1);
	}
}

void InspectorRefreshElementList(InspectorWindow *inspector) {
	EsListViewRemoveAll(inspector->elementList, 0);
	arrfree(inspector->elements);
	InspectorFindElementsRecursively(inspector, inspector->instance->window, 0);
	EsListViewInsert(inspector->elementList, 0, 0, arrlenu(inspector->elements) - 1);
}

void InspectorNotifyElementPainted(EsElement *element, EsPainter *painter) {
	InspectorWindow *inspector = InspectorGet(element);
	if (!inspector) return;

	InspectorElementEntry *entry = inspector->hoveredElement.element ? &inspector->hoveredElement : nullptr;
	if (!entry) return;

	EsRectangle bounds = ES_MAKE_RECTANGLE(painter->offsetX, painter->offsetX + painter->width, 
			painter->offsetY, painter->offsetY + painter->height);

	if (entry->element == element) {
		EsDrawRectangle(painter, bounds, 0x607F7FFF, 0x60FFFF7F, element->currentStyle->insets);
	} else if (entry->element->parent == element) {
		if ((element->flags & ES_CELL_FILL) != ES_CELL_FILL) {
			EsRectangle rectangle = entry->givenBounds;
			rectangle.l += bounds.l, rectangle.r += bounds.l;
			rectangle.t += bounds.t, rectangle.b += bounds.t;
			// EsDrawBlock(painter, rectangle, 0x20FF7FFF);
		}
	}
}

#define INSPECTOR_ALIGN_COMMAND(name, clear, set, toggle) \
void name (EsInstance *instance, EsElement *, EsCommand *) { \
	InspectorWindow *inspector = (InspectorWindow *) instance; \
	EsElement *e = inspector->elements[inspector->selectedElement].element; \
	if (toggle) e->flags ^= set; \
	else { e->flags &= ~(clear); e->flags |= set; } \
	EsElementUpdateContentSize(e); \
	if (e->parent) EsElementUpdateContentSize(e->parent); \
	inspector->elementList->Repaint(true); \
	InspectorUpdateEditor(inspector); \
}

INSPECTOR_ALIGN_COMMAND(InspectorHAlignLeft, ES_CELL_H_LEFT | ES_CELL_H_RIGHT, ES_CELL_H_LEFT, false);
INSPECTOR_ALIGN_COMMAND(InspectorHAlignCenter, ES_CELL_H_LEFT | ES_CELL_H_RIGHT, ES_CELL_H_LEFT | ES_CELL_H_RIGHT, false);
INSPECTOR_ALIGN_COMMAND(InspectorHAlignRight, ES_CELL_H_LEFT | ES_CELL_H_RIGHT, ES_CELL_H_RIGHT, false);
INSPECTOR_ALIGN_COMMAND(InspectorHAlignExpand, 0, ES_CELL_H_EXPAND, true);
INSPECTOR_ALIGN_COMMAND(InspectorHAlignShrink, 0, ES_CELL_H_SHRINK, true);
INSPECTOR_ALIGN_COMMAND(InspectorHAlignPush, 0, ES_CELL_H_PUSH, true);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignTop, ES_CELL_V_TOP | ES_CELL_V_BOTTOM, ES_CELL_V_TOP, false);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignCenter, ES_CELL_V_TOP | ES_CELL_V_BOTTOM, ES_CELL_V_TOP | ES_CELL_V_BOTTOM, false);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignBottom, ES_CELL_V_TOP | ES_CELL_V_BOTTOM, ES_CELL_V_BOTTOM, false);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignExpand, 0, ES_CELL_V_EXPAND, true);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignShrink, 0, ES_CELL_V_SHRINK, true);
INSPECTOR_ALIGN_COMMAND(InspectorVAlignPush, 0, ES_CELL_V_PUSH, true);
INSPECTOR_ALIGN_COMMAND(InspectorDirectionLeft, ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_REVERSE, 
		ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_REVERSE, false);
INSPECTOR_ALIGN_COMMAND(InspectorDirectionRight, ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_REVERSE, 
		ES_PANEL_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL, false);
INSPECTOR_ALIGN_COMMAND(InspectorDirectionUp, ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_REVERSE, 
		ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_REVERSE, false);
INSPECTOR_ALIGN_COMMAND(InspectorDirectionDown, ES_PANEL_HORIZONTAL | ES_PANEL_REVERSE | ES_ELEMENT_LAYOUT_HINT_HORIZONTAL | ES_ELEMENT_LAYOUT_HINT_REVERSE, 0, false);

void InspectorAddElement2(EsMenu *menu, EsGeneric context) {
	InspectorWindow *inspector = (InspectorWindow *) menu->instance; 
	if (inspector->selectedElement == -1) return;
	EsElement *e = inspector->elements[inspector->selectedElement].element; 
	int asSibling = context.u & 0x80;
	context.u &= ~0x80;

	if (asSibling) {
		EsElementInsertAfter(e); 
		e = e->parent;
	}

	if (context.u == 1) {
		EsButtonCreate(e);
	} else if (context.u == 2) {
		EsPanelCreate(e);
	} else if (context.u == 3) {
		EsSpacerCreate(e);
	} else if (context.u == 4) {
		EsTextboxCreate(e);
	} else if (context.u == 5) {
		EsTextDisplayCreate(e);
	}
}

void InspectorAddElement(EsInstance *, EsElement *element, EsCommand *) {
	EsMenu *menu = EsMenuCreate(element, ES_FLAGS_DEFAULT);
	EsMenuAddItem(menu, 0, "Add button", -1, InspectorAddElement2, element->userData.u | 1);
	EsMenuAddItem(menu, 0, "Add panel", -1, InspectorAddElement2, element->userData.u | 2);
	EsMenuAddItem(menu, 0, "Add spacer", -1, InspectorAddElement2, element->userData.u | 3);
	EsMenuAddItem(menu, 0, "Add textbox", -1, InspectorAddElement2, element->userData.u | 4);
	EsMenuAddItem(menu, 0, "Add text display", -1, InspectorAddElement2, element->userData.u | 5);
	EsMenuShow(menu);
}

void InspectorSetup(EsWindow *window) {
	InspectorWindow *inspector = (InspectorWindow *) EsHeapAllocate(sizeof(InspectorWindow), true); // TODO Freeing this.
	inspector->window = window;
	InstanceSetup(inspector);
	inspector->instance = window->instance;
	window->instance = inspector;

	inspector->selectedElement = -1;

	EsPanel *panel = EsPanelCreate(window, ES_CELL_FILL);

	inspector->elementList = EsListViewCreate(panel, ES_CELL_FILL | ES_LIST_VIEW_COLUMNS | ES_LIST_VIEW_SINGLE_SELECT);
	inspector->elementList->userCallback = InspectorElementListCallback;
	EsListViewSetColumns(inspector->elementList, inspectorElementListColumns, sizeof(inspectorElementListColumns) / sizeof(EsListViewColumn));
	EsListViewInsertGroup(inspector->elementList, 0);

	{
		EsPanel *toolbar = EsPanelCreate(panel, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_PANEL_TOOLBAR_ROOT);
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		EsTextDisplayCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, "Horizontal:");
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		inspector->alignH[0] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignH[0], ES_ICON_ALIGN_HORIZONTAL_LEFT);
		EsButtonOnCommand(inspector->alignH[0], InspectorHAlignLeft);
		inspector->alignH[1] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignH[1], ES_ICON_ALIGN_HORIZONTAL_CENTER);
		EsButtonOnCommand(inspector->alignH[1], InspectorHAlignCenter);
		inspector->alignH[2] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignH[2], ES_ICON_ALIGN_HORIZONTAL_RIGHT);
		EsButtonOnCommand(inspector->alignH[2], InspectorHAlignRight);
		inspector->alignH[3] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Expand");
		EsButtonOnCommand(inspector->alignH[3], InspectorHAlignExpand);
		inspector->alignH[4] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Shrink");
		EsButtonOnCommand(inspector->alignH[4], InspectorHAlignShrink);
		inspector->alignH[5] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Push");
		EsButtonOnCommand(inspector->alignH[5], InspectorHAlignPush);
	}

	{
		EsPanel *toolbar = EsPanelCreate(panel, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_PANEL_TOOLBAR_ROOT);
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		EsTextDisplayCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, "Vertical:");
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		inspector->alignV[0] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignV[0], ES_ICON_ALIGN_VERTICAL_TOP);
		EsButtonOnCommand(inspector->alignV[0], InspectorVAlignTop);
		inspector->alignV[1] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignV[1], ES_ICON_ALIGN_VERTICAL_CENTER);
		EsButtonOnCommand(inspector->alignV[1], InspectorVAlignCenter);
		inspector->alignV[2] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->alignV[2], ES_ICON_ALIGN_VERTICAL_BOTTOM);
		EsButtonOnCommand(inspector->alignV[2], InspectorVAlignBottom);
		inspector->alignV[3] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Expand");
		EsButtonOnCommand(inspector->alignV[3], InspectorVAlignExpand);
		inspector->alignV[4] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Shrink");
		EsButtonOnCommand(inspector->alignV[4], InspectorVAlignShrink);
		inspector->alignV[5] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED, 0, "Push");
		EsButtonOnCommand(inspector->alignV[5], InspectorVAlignPush);
	}

	{
		EsPanel *toolbar = EsPanelCreate(panel, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_PANEL_TOOLBAR_ROOT);
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		EsTextDisplayCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, "Stack:");
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		inspector->direction[0] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->direction[0], ES_ICON_GO_PREVIOUS);
		EsButtonOnCommand(inspector->direction[0], InspectorDirectionLeft);
		inspector->direction[1] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->direction[1], ES_ICON_GO_NEXT);
		EsButtonOnCommand(inspector->direction[1], InspectorDirectionRight);
		inspector->direction[2] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->direction[2], ES_ICON_GO_UP);
		EsButtonOnCommand(inspector->direction[2], InspectorDirectionUp);
		inspector->direction[3] = EsButtonCreate(toolbar, ES_BUTTON_TOOLBAR | ES_ELEMENT_DISABLED);
		EsButtonSetIcon(inspector->direction[3], ES_ICON_GO_DOWN);
		EsButtonOnCommand(inspector->direction[3], InspectorDirectionDown);
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 25, 0);
		inspector->addChildButton = EsButtonCreate(toolbar, ES_BUTTON_DROPDOWN | ES_ELEMENT_DISABLED | ES_BUTTON_COMPACT, nullptr, "Add child... ");
		EsButtonOnCommand(inspector->addChildButton, InspectorAddElement);
		inspector->addSiblingButton = EsButtonCreate(toolbar, ES_BUTTON_DROPDOWN | ES_ELEMENT_DISABLED | ES_BUTTON_COMPACT, nullptr, "Add sibling... ");
		inspector->addSiblingButton->userData.i = 0x80;
		EsButtonOnCommand(inspector->addSiblingButton, InspectorAddElement);
	}

	{
		EsPanel *toolbar = EsPanelCreate(panel, ES_CELL_H_FILL | ES_PANEL_HORIZONTAL, ES_STYLE_PANEL_TOOLBAR_ROOT);
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		EsTextDisplayCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, "Style:");
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		inspector->styleTextbox = EsTextboxCreate(toolbar, ES_ELEMENT_DISABLED | ES_TEXTBOX_EDIT_BASED);
		inspector->styleTextbox->userCallback = InspectorStyleTextboxCallback;
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 25, 0);
		EsTextDisplayCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, "Content:");
		EsSpacerCreate(toolbar, ES_FLAGS_DEFAULT, nullptr, 5, 0);
		inspector->contentTextbox = EsTextboxCreate(toolbar, ES_ELEMENT_DISABLED | ES_TEXTBOX_EDIT_BASED);
		inspector->contentTextbox->userCallback = InspectorContentTextboxCallback;
	}

	InspectorRefreshElementList(inspector);

	ApplicationInstance *instance = (ApplicationInstance *) inspector->instance->_private;
	instance->attachedInspector = inspector;
}
