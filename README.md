# **Essence** — An Operating System

![Screenshot of the OS running in an emulator, showing File Manager, and the new tab screen.](https://essence.handmade.network/static/media/image/p-essence-s1.png)
![Screenshot of the OS running in an emulator, showing GCC running under the POSIX subsystem.](https://essence.handmade.network/static/media/image/p-essence-s3.png)
![Screenshot of the OS running in an emulator, showing the shutdown dialog.](https://essence.handmade.network/static/media/image/p-essence-s5.png)

## Features

Kernel
* Audio mixer.
* Filesystem independent cache manager.
* Memory manager with shared memory, memory-mapped files and multithreaded paging zeroing and working set balancing.
* Networking stack for TCP/IP.
* POSIX subsystem.
* Scheduler with multiple priority levels and priority inversion.
* On-demand module loading.
* Virtual filesystem.
* Window manager.

Applications
* File Manager
* Text Editor
* IRC Client
* System Monitor

Ports
* Bochs
* GCC and Binutils
* FFmpeg
* Mesa (for software-rendered OpenGL)
* Musl

Drivers
* Power management: ACPI with ACPICA.
* Secondary storage: IDE, AHCI and NVMe.
* Graphics: BGA and SVGA.
* Read-write filesystems: EssenceFS.
* Read-only filesystems: Ext2, FAT, NTFS, ISO9660.
* Audio: HD Audio.
* NICs: 8254x.
* USB: XHCI, bulk storage devices, human interface devices.

Desktop
* Custom user interface library.
* Software vector renderer with complex animation support.
* Tabbed windows.
* Multi-lingual text rendering and layout with FreeType and Harfbuzz.

## Discussion

Visit https://essence.handmade.network/forums.

## Building

**Warning: This software is still in development. Expect bugs!**

### Linux or macOS

Download this project's source. 

    git clone --depth=1 https://gitlab.com/nakst/essence.git/

Start the build system.

    ./start.sh

Follow the on-screen instructions to build a cross compiler.

You can then run the build system again to build the operating system, and test it in an emulator. If you have Qemu installed, try the command `t2`. If you have VirtualBox installed, make a 128MB drive called `vbox.vdi` in the `essence` folder, attach it as a to a virtual machine called "Essence" (choose Windows 7 64-bit as the OS), and try the command `v` for a much smoother experience than in Qemu.

## Generating the API header

If you want your project to target Essence, you need to generate the API header for your programming language of choice.

    g++ -o bin/header_generator util/header_generator.cpp
    bin/header_generator <language> <path-to-output-file>

Currently supported languages are 'c' (also works for C++) and 'odin'.
