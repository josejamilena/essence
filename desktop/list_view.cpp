// TODO RMB click/drag.
// TODO Consistent int64_t/intptr_t.
// TODO Rare visible item at unexpected position crash? Still happening occasionally.
// TODO Drag and drop.
// TODO GetFirstIndex/GetLastIndex assume that every group is non-empty.

struct ListViewItem {
	EsElement *element;
	int32_t group;
	int32_t size;
	EsGeneric index;
	uint8_t indent;
	bool startAtSecondColumn;
	bool isHeader, isFooter;
	bool showSearchHighlight;
};

struct ListViewGroup {
	// TODO Empty groups.
	uint64_t itemCount;
	int64_t totalSize;
	uint32_t flags;
	bool initialised;
};

struct EsListView : EsElement {
	ScrollPane scroll;

	uint64_t totalItemCount;
	uint64_t totalSize;
	DS_ARRAY(ListViewGroup) groups;
	DS_ARRAY(ListViewItem) visibleItems;

	const EsStyle *itemStyle, *headerItemStyle, *footerItemStyle;
	int64_t fixedWidth, fixedHeight;
	int64_t fixedHeaderSize, fixedFooterSize;

	// TODO Updating these when the style changes.
	UIStyle *primaryCellStyle;
	UIStyleKey primaryCellStyleKey;
	UIStyle *secondaryCellStyle;
	UIStyleKey secondaryCellStyleKey;

	bool hasFocusedItem;
	int32_t focusedItemGroup;
	EsGeneric focusedItemIndex;

	bool hasAnchorItem;
	int32_t anchorItemGroup;
	EsGeneric anchorItemIndex;

	// Valid only during Z-order messages.
	EsElement **zOrderItems;

	EsElement *selectionBox;
	bool hasSelectionBoxAnchor;
	int64_t selectionBoxAnchorX, selectionBoxAnchorY,
		selectionBoxPositionX, selectionBoxPositionY;

	bool firstLayout;

	char searchBuffer[64];
	size_t searchBufferBytes;
	uint64_t searchBufferLastKeyTime;

	char *emptyMessage;
	size_t emptyMessageBytes;

	EsElement *columnHeader;
	EsListViewColumn *columns;
	size_t columnCount;
	int columnResizingOriginalWidth;
	// TODO Updating this when the style changes.
	int64_t totalColumnWidth;

	inline EsRectangle GetListBounds() {
		EsRectangle bounds = GetBounds();

		if (columnHeader) {
			bounds.t += columnHeader->currentStyle->preferredHeight;
		}

		return bounds;
	}

	int CompareIndices(int32_t groupIndex, EsGeneric left, EsGeneric right) {
		if (left.u == right.u) {
			return 0;
		}

		if (~flags & ES_LIST_VIEW_NON_LINEAR) {
			if (left.i > right.i) return  1;
			if (left.i < right.i) return -1;
		}

		EsMessage m = { ES_MSG_LIST_VIEW_COMPARE_INDICES };
		m.compareIndices.group = groupIndex;
		m.compareIndices.left = left;
		m.compareIndices.right = right;
		EsAssert(EsMessageSend(this, &m) == ES_HANDLED); // Could not compare indices.
		return m.compareIndices.result;
	}

	inline void GetFirstIndex(EsMessage *message) {
		EsAssert(message->iterateIndex.group < arrlen(groups)); // Invalid group index.
		EsAssert(groups[message->iterateIndex.group].itemCount); // No items in the group.

		if (flags & ES_LIST_VIEW_NON_LINEAR) {
			message->type = ES_MSG_LIST_VIEW_FIRST_INDEX;
			EsAssert(ES_HANDLED == EsMessageSend(this, message)); // First index message not handled.
		} else {
			message->iterateIndex.index.i = 0;
		}
	}

	inline EsGeneric GetFirstIndex(int32_t group) {
		EsMessage m = {};
		m.iterateIndex.group = group;
		GetFirstIndex(&m);
		return m.iterateIndex.index;
	}

	inline void GetLastIndex(EsMessage *message) {
		EsAssert(message->iterateIndex.group < arrlen(groups)); // Invalid group index.
		EsAssert(groups[message->iterateIndex.group].itemCount); // No items in the group.

		if (flags & ES_LIST_VIEW_NON_LINEAR) {
			message->type = ES_MSG_LIST_VIEW_LAST_INDEX;
			EsAssert(ES_HANDLED == EsMessageSend(this, message)); // First index message not handled.
		} else {
			message->iterateIndex.index.i = groups[message->iterateIndex.group].itemCount - 1;
		}
	}

	inline EsGeneric GetLastIndex(int32_t group) {
		EsMessage m = {};
		m.iterateIndex.group = group;
		GetLastIndex(&m);
		return m.iterateIndex.index;
	}

	inline bool IterateForwards(EsMessage *message) {
		if (flags & ES_LIST_VIEW_NON_LINEAR) {
			message->type = ES_MSG_LIST_VIEW_NEXT_INDEX;
			int response = EsMessageSend(this, message);
			EsAssert(0 != response); // Next index message not handled.
			return response == ES_HANDLED;
		} else {
			if (message->iterateIndex.index.u == groups[message->iterateIndex.group].itemCount - 1) {
				if (message->iterateIndex.group == arrlen(groups) - 1) {
					return false;
				}

				message->iterateIndex.group++;
				message->iterateIndex.index.i = 0;
			} else {
				message->iterateIndex.index.i++;
			}

			return true;
		}
	}

	inline bool IterateBackwards(EsMessage *message) {
		if (flags & ES_LIST_VIEW_NON_LINEAR) {
			message->type = ES_MSG_LIST_VIEW_PREVIOUS_INDEX;
			int response = EsMessageSend(this, message);
			EsAssert(0 != response); // Previous index message not handled.
			return response == ES_HANDLED;
		} else {
			if (message->iterateIndex.index.u == 0) {
				if (message->iterateIndex.group == 0) {
					return false;
				}

				message->iterateIndex.group--;
				message->iterateIndex.index.i = groups[message->iterateIndex.group].itemCount - 1;
			} else {
				message->iterateIndex.index.i--;
			}

			return true;
		}
	}

	int64_t CountItems(int32_t groupIndex, EsGeneric firstIndex, EsGeneric lastIndex) {
		if (firstIndex.u == lastIndex.u) {
			return 1;
		}

		if (~flags & ES_LIST_VIEW_NON_LINEAR) {
			return lastIndex.i - firstIndex.i + 1;
		}

		{
			EsMessage m = { ES_MSG_LIST_VIEW_COUNT_ITEMS };
			m.itemRange.group = groupIndex;
			m.itemRange.firstIndex = firstIndex;
			m.itemRange.lastIndex = lastIndex;

			if (ES_HANDLED == EsMessageSend(this, &m)) {
				return m.itemRange.result;
			}
		}

		EsMessage m = {};
		m.iterateIndex.group = groupIndex;
		m.iterateIndex.index = firstIndex;

		int64_t count = 1;

		while (true) {
			IterateForwards(&m);
			EsAssert(groupIndex == m.iterateIndex.group); // Index range did not exist in group.
			count++;

			if (m.iterateIndex.index.u == lastIndex.u) {
				return count;
			}
		}
	}

	int64_t MeasureItems(int32_t groupIndex, EsGeneric firstIndex, EsGeneric lastIndex, int64_t *count = nullptr) {
		int64_t _tempCount = -1;
		if (!count) count = &_tempCount;

		if (~flags & ES_LIST_VIEW_NON_LINEAR) {
			if (*count == -1) {
				*count = lastIndex.i - firstIndex.i + 1;
			} else {
				EsAssert(*count == lastIndex.i - firstIndex.i + 1); // Invalid item count.
			}
		}

		bool haveCount = *count != -1;
		bool variableSize = flags & ES_LIST_VIEW_VARIABLE_SIZE;

		if (!variableSize) {
			if (!haveCount) {
				*count = CountItems(groupIndex, firstIndex, lastIndex);
			} 

			ListViewGroup *group = groups + groupIndex;
			int64_t normalCount = *count;
			int64_t additionalSize = 0;

			if ((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && firstIndex.u == 0) {
				normalCount--;
				additionalSize += fixedHeaderSize;
			}

			if ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && lastIndex.u == group->itemCount - 1) {
				normalCount--;
				additionalSize += fixedFooterSize;
			}

			return additionalSize + normalCount * (flags & ES_LIST_VIEW_HORIZONTAL ? fixedWidth : fixedHeight);
		}

		if (firstIndex.u != lastIndex.u) {
			EsMessage m = { ES_MSG_LIST_VIEW_MEASURE_RANGE };
			m.itemRange.group = groupIndex;
			m.itemRange.firstIndex = firstIndex;
			m.itemRange.lastIndex = lastIndex;

			if (ES_HANDLED == EsMessageSend(this, &m)) {
				return m.itemRange.result;
			}
		}

		EsMessage m = {};
		m.iterateIndex.group = groupIndex;
		m.iterateIndex.index = firstIndex;
		int64_t total = 0;
		int64_t _count = 0;

		while (true) {
			EsMessage m2 = { ES_MSG_LIST_VIEW_MEASURE_ITEM };
			m2.measureItem.group = groupIndex;
			m2.measureItem.index = m.iterateIndex.index;
			EsAssert(ES_HANDLED == EsMessageSend(this, &m2)); // Variable height list view must be able to measure items.
			total += m2.measureItem.result;
			_count++;

			if (m.iterateIndex.index.u == lastIndex.u) {
				if (*count != -1) EsAssert(*count == _count); // Invalid item count.
				else *count = _count;
				return total;
			}

			IterateForwards(&m);
			EsAssert(groupIndex == m.iterateIndex.group); // Index range did not exist in group.
		}
	}

	void GetItemPosition(int32_t groupIndex, EsGeneric index, int64_t *_position, int64_t *_itemSize) {
		int64_t gapBetweenGroup = currentStyle->gapMajor,
			gapBetweenItems = (flags & ES_LIST_VIEW_TILED) ? currentStyle->gapWrap : currentStyle->gapMinor,
			fixedSize       = (flags & ES_LIST_VIEW_VARIABLE_SIZE) ? 0 : (flags & ES_LIST_VIEW_HORIZONTAL ? fixedWidth : fixedHeight),
			startInset 	= flags & ES_LIST_VIEW_HORIZONTAL ? currentStyle->insets.l : currentStyle->insets.t;

		int64_t position = (flags & ES_LIST_VIEW_HORIZONTAL ? -scroll.x : -scroll.y) + startInset, 
			itemSize = 0;

		EsGeneric targetIndex = index;

		for (int32_t i = 0; i < groupIndex; i++) {
			position += groups[i].totalSize + gapBetweenGroup;
		}

		ListViewGroup *group = groups + groupIndex;

		if ((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && index.u == 0) {
			itemSize = fixedHeaderSize;
		} else if ((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && index.u == group->itemCount - 1) {
			position += group->totalSize - fixedFooterSize;
			itemSize = fixedFooterSize;
		} else if ((~flags & ES_LIST_VIEW_NON_LINEAR) && (~flags & ES_LIST_VIEW_VARIABLE_SIZE)) {
			// TODO MAP_TO_LINEAR if non-linear but fixed size.

			intptr_t linearIndex = index.i;

			if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
				linearIndex--;
				position += fixedHeaderSize + gapBetweenItems;
			}

			linearIndex /= GetItemsPerBand();
			position += (fixedSize + gapBetweenItems) * linearIndex;
			itemSize = fixedSize;
		} else {
			EsAssert(~flags & ES_LIST_VIEW_TILED); // Tiled list views must be linear and fixed-size.;

			EsMessage index = {};
			index.type = ES_MSG_LIST_VIEW_FIND_POSITION;
			index.iterateIndex.group = groupIndex;
			index.iterateIndex.index = targetIndex;

			if (ES_HANDLED == EsMessageSend(this, &index)) {
				position += index.iterateIndex.position;
			} else {
				if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
					position += fixedHeaderSize + gapBetweenItems;
				}

				bool forwards;
				ListViewItem *reference = arrlenu(visibleItems) ? visibleItems : nullptr;

				bool closerToStartThanReference = reference && targetIndex.i < reference->index.i / 2;
				bool closerToEndThanReference   = reference && targetIndex.i > reference->index.i / 2 + (intptr_t) group->itemCount / 2;
				if (flags & ES_LIST_VIEW_NON_LINEAR) closerToStartThanReference = closerToEndThanReference = false;

				if (reference && reference->group == groupIndex && !closerToStartThanReference && !closerToEndThanReference) {
					index.iterateIndex.index = reference->index;
					position = (flags & ES_LIST_VIEW_HORIZONTAL) ? reference->element->offsetX : reference->element->offsetY;
					forwards = CompareIndices(groupIndex, reference->index, targetIndex) < 0;

					EsMessage firstIndex = {};
					firstIndex.iterateIndex.group = groupIndex;
					GetFirstIndex(&firstIndex);

					if (index.iterateIndex.index == firstIndex.iterateIndex.index) {
						forwards = true;
					}
				} else if ((~flags & ES_LIST_VIEW_NON_LINEAR) && targetIndex.u > group->itemCount / 2) {
					GetLastIndex(&index); 
					position += group->totalSize;
					position -= MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);
					forwards = false;
				} else {
					GetFirstIndex(&index); 
					forwards = true;
				}

				while (index.iterateIndex.index.u != targetIndex.u) {
					int64_t size = MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);
					position += forwards ? (size + gapBetweenItems) : -(size + gapBetweenItems);
					EsAssert((forwards ? IterateForwards(&index) : IterateBackwards(&index)) && index.iterateIndex.group == groupIndex);
							// Could not find the item in the group.
				}

				itemSize = MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);
			}
		}

		*_position = position;
		*_itemSize = itemSize;
	}

	void EnsureItemVisible(int32_t groupIndex, EsGeneric index, bool alignTop) {
		EsRectangle contentBounds = GetListBounds();

		int64_t startInset = flags & ES_LIST_VIEW_HORIZONTAL ? currentStyle->insets.l : currentStyle->insets.t,
			endInset = flags & ES_LIST_VIEW_HORIZONTAL ? currentStyle->insets.r : currentStyle->insets.b,
			contentSize = flags & ES_LIST_VIEW_HORIZONTAL ? Width(contentBounds) : Height(contentBounds);

		int64_t position, itemSize;
		GetItemPosition(groupIndex, index, &position, &itemSize);

		if (position >= 0 && position + itemSize <= contentSize - endInset) {
			return;
		}

		if (alignTop) {
			if (flags & ES_LIST_VIEW_HORIZONTAL) {
				scroll.SetX(scroll.x + position - startInset);
			} else {
				scroll.SetY(scroll.y + position - startInset);
			}
		} else {
			if (flags & ES_LIST_VIEW_HORIZONTAL) {
				scroll.SetX(scroll.x + position + itemSize - contentSize + endInset);
			} else {
				scroll.SetY(scroll.y + position + itemSize - contentSize + endInset);
			}
		}
	}

	EsMessage FindFirstVisibleItem(int64_t *_position, int64_t position, ListViewItem *reference, bool *noItems) {
		int64_t gapBetweenGroup = currentStyle->gapMajor,
			gapBetweenItems = (flags & ES_LIST_VIEW_TILED) ? currentStyle->gapWrap : currentStyle->gapMinor,
			fixedSize       = (flags & ES_LIST_VIEW_VARIABLE_SIZE) ? 0 : (flags & ES_LIST_VIEW_HORIZONTAL ? fixedWidth : fixedHeight);
		
		// Find the group.
		// TODO Faster searching when there are many groups.

		int32_t groupIndex = 0;
		bool foundGroup = false;

		for (; groupIndex < arrlen(groups); groupIndex++) {
			ListViewGroup *group = groups + groupIndex;
			int64_t totalSize = group->totalSize;

			if (position + totalSize > 0) {
				foundGroup = true;
				break;
			}

			position += totalSize + gapBetweenGroup;
		}

		if (!foundGroup) {
			if (noItems) {
				*noItems = true;
				return {};
			} else {
				EsAssert(false); // Could not find the first visible item with the given scroll.
			}
		}

		EsMessage index = {};
		index.iterateIndex.group = groupIndex;

		// Can we go directly to the item?

		if ((~flags & ES_LIST_VIEW_NON_LINEAR) && (~flags & ES_LIST_VIEW_VARIABLE_SIZE)) {
			// TODO MAP_FROM_LINEAR message if non-linear but fixed size.

			index.iterateIndex.index.i = 0;
			intptr_t addHeader = 0;

			ListViewGroup *group = groups + groupIndex;

			if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
				if (position + fixedHeaderSize > 0) {
					*_position = position;
					return index;
				}

				position += fixedHeaderSize + gapBetweenItems;
				addHeader = 1;
			}

			intptr_t band = -position / (fixedSize + gapBetweenItems);
			if (band < 0) band = 0;
			position += band * (fixedSize + gapBetweenItems);

			if (flags & ES_LIST_VIEW_TILED) {
				intptr_t itemsPerBand = GetWrapLimit() / (((flags & ES_LIST_VIEW_HORIZONTAL) ? fixedHeight : fixedWidth) + currentStyle->gapMinor);
				band *= itemsPerBand ?: 1;
			}

			index.iterateIndex.index.i = band + addHeader;

			if (index.iterateIndex.index.i >= (intptr_t) group->itemCount) {
				index.iterateIndex.index.i = group->itemCount - 1;
			}

			*_position = position;
			return index;
		}

		EsAssert(~flags & ES_LIST_VIEW_TILED); // Trying to use TILED mode with NON_LINEAR or VARIABLE_SIZE mode.

		// Try asking the application to find the item.

		index.type = ES_MSG_LIST_VIEW_FIND_INDEX;
		index.iterateIndex.position = -position;

		if (ES_HANDLED == EsMessageSend(this, &index)) {
			*_position = -index.iterateIndex.position;
			return index;
		}

		// Find the item within the group, manually.

		bool forwards;

		if (reference && reference->group == groupIndex) {
			int64_t referencePosition = (flags & ES_LIST_VIEW_HORIZONTAL) ? reference->element->offsetX : reference->element->offsetY;

			if (AbsoluteInteger64(referencePosition) < AbsoluteInteger64(position)
					&& AbsoluteInteger64(referencePosition) < AbsoluteInteger64(position + groups[groupIndex].totalSize)) {
				index.iterateIndex.index = reference->index;
				position = referencePosition; // Use previous first visible item as reference.
				forwards = position < 0;

				EsMessage firstIndex = {};
				firstIndex.iterateIndex.group = groupIndex;
				GetFirstIndex(&firstIndex);

				if (index.iterateIndex.index == firstIndex.iterateIndex.index) {
					forwards = true;
				}

				goto gotReference;
			}
		} 
		
		if (position + groups[groupIndex].totalSize / 2 >= 0) {
			GetFirstIndex(&index); // Use start of group as reference.
			forwards = true;
		} else {
			GetLastIndex(&index); // Use end of group as reference
			position += groups[groupIndex].totalSize;
			position -= MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);
			forwards = false;
		}

		gotReference:;

		if (forwards) {
			// Iterate forwards from reference point.

			while (true) {
				int64_t size = fixedSize ?: MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);

				if (position + size > 0) {
					*_position = position;
					return index;
				}

				EsAssert(IterateForwards(&index) && index.iterateIndex.group == groupIndex);
						// No items in the group are visible. Maybe invalid scroll position?
				position += size + gapBetweenItems;
			}
		} else {
			// Iterate backwards from reference point.

			while (true) {
				if (position <= 0 || !IterateBackwards(&index)) {
					*_position = position;
					return index;
				}

				int64_t size = fixedSize ?: MeasureItems(index.iterateIndex.group, index.iterateIndex.index, index.iterateIndex.index);
				EsAssert(index.iterateIndex.group == groupIndex);
						// No items in the group are visible. Maybe invalid scroll position?
				position -= size + gapBetweenItems;
			}
		}
	}

	void Populate() {
		// TODO Keep one item before and after the viewport, so tab traversal on custom elements works.
		// TODO Always keep an item if it has FOCUS_WITHIN.
		// 	- But maybe we shouldn't allow focusable elements in a list view.

		if (!totalItemCount) {
			return;
		}

		EsRectangle contentBounds = GetListBounds();
		int64_t contentSize = flags & ES_LIST_VIEW_HORIZONTAL ? Width(contentBounds) : Height(contentBounds);
		int64_t scroll = EsCRTfloor(flags & ES_LIST_VIEW_HORIZONTAL ? (this->scroll.x - currentStyle->insets.l) : (this->scroll.y - currentStyle->insets.t));

		int64_t position = 0;
		bool noItems = false;
		EsMessage currentItem = FindFirstVisibleItem(&position, -scroll, arrlenu(visibleItems) ? visibleItems : nullptr, &noItems);
		uintptr_t visibleIndex = 0;

		int64_t wrapLimit = GetWrapLimit();
		int64_t fixedMinorSize = (flags & ES_LIST_VIEW_HORIZONTAL) ? fixedHeight : fixedWidth;
		intptr_t itemsPerBand = fixedMinorSize && ((fixedMinorSize + currentStyle->gapMinor) < wrapLimit) ? (wrapLimit / (fixedMinorSize + currentStyle->gapMinor)) : 1;
		intptr_t itemInBand = 0;
		int64_t computedMinorGap = (wrapLimit - itemsPerBand * fixedMinorSize) / (itemsPerBand + 1);
		int64_t minorPosition = computedMinorGap;

		while (visibleIndex < arrlenu(visibleItems)) {
			// Remove visible items no longer visible, before the viewport.

			ListViewItem *visibleItem = visibleItems + visibleIndex;
			int64_t visibleItemPosition = flags & ES_LIST_VIEW_HORIZONTAL ? visibleItem->element->offsetX : visibleItem->element->offsetY;
			if (visibleItemPosition >= position) break;
			visibleItem->element->userData = visibleIndex;
			visibleItem->element->Destroy();
			arrdel(visibleItems, visibleIndex);
		}

		while (position < contentSize && !noItems) {
			ListViewItem *visibleItem = visibleIndex == arrlenu(visibleItems) ? nullptr : visibleItems + visibleIndex;

			if (visibleItem && visibleItem->index == currentItem.iterateIndex.index
					&& visibleItem->group == currentItem.iterateIndex.group) {
				// This is already a visible item.

				if (~flags & ES_LIST_VIEW_TILED) {
					int64_t expectedPosition = (flags & ES_LIST_VIEW_HORIZONTAL)
							? visibleItem->element->offsetX - contentBounds.l 
							: visibleItem->element->offsetY - contentBounds.t;

					if (position < expectedPosition - 1 || position > expectedPosition + 1) {
						EsPrint("Item in unexpected position: expected %d, got %d; index %d, scroll %d.\n", 
								expectedPosition, position, visibleItem->index.i, scroll);
						EsAssert(false);
					}
				}
			} else {
				// Add a new visible item.

				ListViewItem empty = {};
				arrins(visibleItems, visibleIndex, empty);
				visibleItem = visibleItems + visibleIndex;

				visibleItem->group = currentItem.iterateIndex.group;
				visibleItem->index = currentItem.iterateIndex.index;
				visibleItem->size = MeasureItems(visibleItem->group, visibleItem->index, visibleItem->index);

				ListViewGroup *group = visibleItem->group + groups;
				const EsStyle *style = nullptr;

				if ((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && visibleItem->index.i == 0 ) {
					style = headerItemStyle;
					visibleItem->isHeader = true;
				} else if ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && visibleItem->index.i == (intptr_t) group->itemCount - 1) {
					style = footerItemStyle;
					visibleItem->isFooter = true;
				} else {
					if (group->flags & ES_LIST_VIEW_GROUP_INDENT) {
						visibleItem->indent++;
					}

					style = itemStyle;
				}

				visibleItem->element = EsCustomElementCreate(this, ES_CELL_FILL, style);
				visibleItem->element->userData = visibleIndex;
				visibleItem->element->cName = "list view item";

				visibleItem->element->classCallback = [] (EsElement *element, EsMessage *message) {
					return ((EsListView *) element->parent)->ProcessItemMessage(element->userData.u, message);
				};

				if (hasFocusedItem && visibleItem->group == focusedItemGroup && visibleItem->index.u == focusedItemIndex.u) {
					visibleItem->element->customStyleState |= THEME_STATE_FOCUSED_ITEM;
				}

				if (state & UI_STATE_FOCUSED) {
					visibleItem->element->customStyleState |= THEME_STATE_LIST_FOCUSED;
				}

				EsMessage m = {};

				m.type = ES_MSG_LIST_VIEW_IS_SELECTED;
				m.selectItem.group = visibleItem->group;
				m.selectItem.index = visibleItem->index;
				EsMessageSend(this, &m);
				if (m.selectItem.isSelected) visibleItem->element->customStyleState |= THEME_STATE_SELECTED;

				m.type = ES_MSG_LIST_VIEW_CREATE_ITEM;
				m.createItem.group = visibleItem->group;
				m.createItem.index = visibleItem->index;
				m.createItem.parent = visibleItem->element;
				EsMessageSend(this, &m);

				m.type = ES_MSG_LIST_VIEW_GET_INDENT;
				m.getIndent.group = visibleItem->group;
				m.getIndent.index = visibleItem->index;
				m.getIndent.indent = 0;
				EsMessageSend(this, &m);
				visibleItem->indent += m.getIndent.indent;

				SelectPreview(arrlen(visibleItems) - 1);
				
				visibleItem->element->MaybeRefreshStyle();
			}

			visibleItem->element->userData = visibleIndex;

			// Update the item's position.

			ListViewGroup *group = groups + visibleItem->group;

			if ((flags & ES_LIST_VIEW_TILED) && !visibleItem->isHeader && !visibleItem->isFooter) {
				if (flags & ES_LIST_VIEW_HORIZONTAL) {
					visibleItem->element->InternalMove(fixedWidth, fixedHeight, 
							position + contentBounds.l, minorPosition + currentStyle->insets.t + contentBounds.t);
				} else {
					visibleItem->element->InternalMove(fixedWidth, fixedHeight, 
							minorPosition + currentStyle->insets.l + contentBounds.l, position + contentBounds.t);
				}

				minorPosition += computedMinorGap + fixedMinorSize;
				itemInBand++;

				bool endOfGroup = ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && currentItem.iterateIndex.index.u == group->itemCount - 2)
					|| (currentItem.iterateIndex.index.u == group->itemCount - 1);

				if (itemInBand == itemsPerBand || endOfGroup) {
					minorPosition = computedMinorGap;
					itemInBand = 0;
					position += (flags & ES_LIST_VIEW_HORIZONTAL) ? visibleItem->element->width : visibleItem->element->height;
					if (!endOfGroup || (group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER)) position += currentStyle->gapWrap;
				}
			} else {
				if (flags & ES_LIST_VIEW_HORIZONTAL) {
					visibleItem->element->InternalMove(
						visibleItem->size, 
						Height(contentBounds) - currentStyle->insets.t - currentStyle->insets.b - visibleItem->indent * currentStyle->gapWrap,
						position + contentBounds.l, 
						currentStyle->insets.t - this->scroll.y + visibleItem->indent * currentStyle->gapWrap + contentBounds.t);
					position += visibleItem->element->width;
				} else if (flags & ES_LIST_VIEW_COLUMNS) {
					int indent = visibleItem->indent * currentStyle->gapWrap;
					int firstColumn = columns[0].width + secondaryCellStyle->gapMajor;
					visibleItem->startAtSecondColumn = indent > firstColumn;
					if (indent > firstColumn) indent = firstColumn;
					visibleItem->element->InternalMove(
						totalColumnWidth - indent,
						visibleItem->size, 
						indent - this->scroll.x + contentBounds.l + currentStyle->insets.l, 
						position + contentBounds.t);
					position += visibleItem->element->height;
				} else {
					int indent = visibleItem->indent * currentStyle->gapWrap + currentStyle->insets.l;
					visibleItem->element->InternalMove(Width(contentBounds) - indent - currentStyle->insets.r, visibleItem->size, 
						indent + contentBounds.l - this->scroll.x, position + contentBounds.t);
					position += visibleItem->element->height;
				}

				if ((flags & ES_LIST_VIEW_TILED) && (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && currentItem.iterateIndex.index.u == 0) {
					position += currentStyle->gapWrap;
				}
			}

			// Go to the next item.

			visibleIndex++;
			int32_t previousGroup = currentItem.iterateIndex.group;
			if (!IterateForwards(&currentItem)) break;
			position += previousGroup == currentItem.iterateIndex.group ? (flags & ES_LIST_VIEW_TILED ? 0 : currentStyle->gapMinor) : currentStyle->gapMajor;
		}

		while (visibleIndex < arrlenu(visibleItems)) {
			// Remove visible items no longer visible, after the viewport.

			ListViewItem *visibleItem = visibleItems + visibleIndex;
			visibleItem->element->userData = visibleIndex;
			visibleItem->element->Destroy();
			arrdel(visibleItems, visibleIndex);
		}
	}

	void Wrap(bool autoScroll) {
		if (~flags & ES_LIST_VIEW_TILED) return;

		totalSize = 0;

		int64_t limit = GetWrapLimit();
		intptr_t itemsPerBand = limit / (((flags & ES_LIST_VIEW_HORIZONTAL) ? fixedHeight : fixedWidth) + currentStyle->gapMinor);
		if (!itemsPerBand) itemsPerBand = 1;

		for (uintptr_t i = 0; i < arrlenu(groups); i++) {
			ListViewGroup *group = groups + i;
			int64_t groupSize = 0;

			intptr_t itemCount = group->itemCount;

			if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
				groupSize += fixedHeaderSize + currentStyle->gapWrap;
				itemCount--;
			}

			if (group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) {
				groupSize += fixedFooterSize + currentStyle->gapWrap;
				itemCount--;
			}

			intptr_t bandsInGroup = (itemCount + itemsPerBand - 1) / itemsPerBand;
			groupSize += (((flags & ES_LIST_VIEW_HORIZONTAL) ? fixedWidth : fixedHeight) + currentStyle->gapWrap) * bandsInGroup;
			groupSize -= currentStyle->gapWrap;
			group->totalSize = groupSize;

			totalSize += groupSize + (group == &arrlast(groups) ? 0 : currentStyle->gapMajor);
		}

		scroll.Refresh();

		if (arrlen(visibleItems) && autoScroll) {
			EnsureItemVisible(visibleItems[0].group, visibleItems[0].index, true);
		}
	}

	void InsertSpace(int64_t space, uintptr_t beforeItem) {
		if (!space) return;

		if (flags & ES_LIST_VIEW_TILED) {
			EsElementUpdateContentSize(this);
			return;
		}

		int64_t currentScroll = (flags & ES_LIST_VIEW_HORIZONTAL) ? scroll.x : scroll.y;
		int64_t scrollLimit = (flags & ES_LIST_VIEW_HORIZONTAL) ? scroll.xLimit : scroll.yLimit;

		totalSize += space;

		if (((beforeItem == 0 && currentScroll) || currentScroll == scrollLimit) && firstLayout && space > 0 && scrollLimit) {
			scroll.Refresh();

			if (flags & ES_LIST_VIEW_HORIZONTAL) {
				scroll.SetX(scroll.x + space, false);
			} else {
				scroll.SetY(scroll.y + space, false);
			}
		} else {
			for (uintptr_t i = beforeItem; i < arrlenu(visibleItems); i++) {
				ListViewItem *item = visibleItems + i;

				if (flags & ES_LIST_VIEW_HORIZONTAL) {
					item->element->offsetX += space;
				} else {
					item->element->offsetY += space;
				}
			}

			scroll.Refresh();
		}

		EsElementUpdateContentSize(this);
	}

	void SetSelected(int32_t fromGroup, EsGeneric fromIndex, int32_t toGroup, EsGeneric toIndex, 
			bool select, bool toggle,
			intptr_t period = 0, intptr_t periodBegin = 0, intptr_t periodEnd = 0) {
		if (!select && (flags & ES_LIST_VIEW_SINGLE_SELECT)) {
			EsMessage m = {};
			m.type = ES_MSG_LIST_VIEW_SELECT;
			m.selectItem.isSelected = false;
			EsMessageSend(this, &m);
			return;
		}

		if (fromGroup == toGroup && CompareIndices(fromGroup, fromIndex, toIndex) > 0) {
			EsGeneric temp = fromIndex;
			fromIndex = toIndex;
			toIndex = temp;
		} else if (fromGroup > toGroup) {
			int32_t temp1 = fromGroup;
			fromGroup = toGroup;
			toGroup = temp1;
			EsGeneric temp2 = fromIndex;
			fromIndex = toIndex;
			toIndex = temp2;
		}

		EsMessage start = {}, end = {};
		start.iterateIndex.group = fromGroup;
		end.iterateIndex.group = fromGroup;

		for (; start.iterateIndex.group <= toGroup; start.iterateIndex.group++, end.iterateIndex.group++) {
			if (start.iterateIndex.group == fromGroup) {
				start.iterateIndex.index = fromIndex;
			} else {
				GetFirstIndex(&start);
			}

			if (end.iterateIndex.group == toGroup) {
				end.iterateIndex.index = toIndex;
			} else {
				GetLastIndex(&end);
			}

			EsMessage m = { ES_MSG_LIST_VIEW_SELECT_RANGE };
			m.selectRange.group = start.iterateIndex.group;
			m.selectRange.fromIndex = start.iterateIndex.index;
			m.selectRange.toIndex = end.iterateIndex.index;
			m.selectRange.select = select;
			m.selectRange.toggle = toggle;

			if (!period && 0 != EsMessageSend(this, &m)) {
				continue;
			}

			intptr_t linearIndex = 0;

			while (true) {
				m.selectItem.group = start.iterateIndex.group;
				m.selectItem.index = start.iterateIndex.index;
				m.selectItem.isSelected = select;

				if (period) {
					ListViewGroup *group = groups + m.selectItem.group;
					intptr_t i = linearIndex;

					if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
						if (linearIndex == 0) {
							goto process;
						} else {
							i--;
						}
					}

					if (group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) {
						if (linearIndex == (intptr_t) group->itemCount - 1) {
							goto process;
						}
					}

					i %= period;

					if (i < periodBegin || i > periodEnd) {
						goto ignore;
					}
				}

				process:;

				if (toggle) {
					m.type = ES_MSG_LIST_VIEW_IS_SELECTED;
					EsMessageSend(this, &m);
					m.selectItem.isSelected = !m.selectItem.isSelected;
				}

				m.type = ES_MSG_LIST_VIEW_SELECT;
				EsMessageSend(this, &m);

				ignore:;

				if (start.iterateIndex.index == end.iterateIndex.index) {
					break;
				}

				IterateForwards(&start);
				linearIndex++;
				EsAssert(start.iterateIndex.group == end.iterateIndex.group); // The from and to selection indices in the group were incorrectly ordered.
			}
		}
	}

	void SelectPreview(intptr_t singleItem = -1) {
		if (!hasSelectionBoxAnchor) {
			return;
		}
		
		int64_t x1 = selectionBoxPositionX, x2 = selectionBoxAnchorX,
			y1 = selectionBoxPositionY, y2 = selectionBoxAnchorY;

		if (x1 > x2) { int64_t temp = x1; x1 = x2; x2 = temp; }
		if (y1 > y2) { int64_t temp = y1; y1 = y2; y2 = temp; }

		x1 -= scroll.x, x2 -= scroll.x;
		y1 -= scroll.y, y2 -= scroll.y;

		EsRectangle bounds = GetListBounds();

		if (x1 < -1000) x1 = -1000; 
		if (x2 < -1000) x2 = -1000; 
		if (y1 < -1000) y1 = -1000; 
		if (y2 < -1000) y2 = -1000; 

		if (x1 > bounds.r + 1000) x1 = bounds.r + 1000;
		if (x2 > bounds.r + 1000) x2 = bounds.r + 1000;
		if (y1 > bounds.b + 1000) y1 = bounds.b + 1000;
		if (y2 > bounds.b + 1000) y2 = bounds.b + 1000;

		selectionBox->InternalMove(x2 - x1, y2 - y1, x1, y1);

		for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
			if (singleItem != -1) {
				i = singleItem;
			}

			EsMessage m = { ES_MSG_LIST_VIEW_IS_SELECTED };
			m.selectItem.index = visibleItems[i].index;
			m.selectItem.group = visibleItems[i].group;
			EsMessageSend(this, &m);

			EsElement *item = visibleItems[i].element;

			if (x1 < item->offsetX + item->width && x2 >= item->offsetX && y1 < item->offsetY + item->height && y2 >= item->offsetY) {
				if (gui.holdingCtrl) {
					m.selectItem.isSelected = !m.selectItem.isSelected;
				} else {
					m.selectItem.isSelected = true;
				}
			}

			if (m.selectItem.isSelected) {
				item->customStyleState |= THEME_STATE_SELECTED;
			} else {
				item->customStyleState &= ~THEME_STATE_SELECTED;
			}

			item->MaybeRefreshStyle();

			if (singleItem != -1) {
				break;
			}
		}
	}

	intptr_t GetItemsPerBand() {
		if (~flags & ES_LIST_VIEW_TILED) {
			return 1;
		} else {
			int64_t wrapLimit = GetWrapLimit();
			int64_t fixedMinorSize = (flags & ES_LIST_VIEW_HORIZONTAL) ? fixedHeight : fixedWidth;
			return fixedMinorSize && ((fixedMinorSize + currentStyle->gapMinor) < wrapLimit) 
				? (wrapLimit / (fixedMinorSize + currentStyle->gapMinor)) : 1;
		}
	}

	void SelectBox(int64_t x1, int64_t x2, int64_t y1, int64_t y2, bool toggle) {
		EsRectangle contentBounds = GetListBounds();
		int64_t offset = 0;
		EsMessage start, end;
		bool noItems = false;

		if (flags & ES_LIST_VIEW_HORIZONTAL) {
			if (y1 >= contentBounds.b - currentStyle->insets.b || y2 < contentBounds.t + currentStyle->insets.t) {
				return;
			}
		} else if (flags & ES_LIST_VIEW_COLUMNS) {
			if (x1 >= contentBounds.l + currentStyle->insets.l + totalColumnWidth || x2 < contentBounds.l + currentStyle->insets.l) {
				return;
			}
		} else {
			if (x1 >= contentBounds.r - currentStyle->insets.r || x2 < contentBounds.l + currentStyle->insets.l) {
				return;
			}
		}

		// TODO Use reference for FindFirstVisibleItem.

		bool adjustStart = false, adjustEnd = false;
		int r1 = (flags & ES_LIST_VIEW_HORIZONTAL) ? currentStyle->insets.l - x1 : currentStyle->insets.t - y1 + scroll.fixedViewportHeight;
		int r2 = (flags & ES_LIST_VIEW_HORIZONTAL) ? currentStyle->insets.l - x2 : currentStyle->insets.t - y2 + scroll.fixedViewportHeight;
		start = FindFirstVisibleItem(&offset, r1, nullptr, &noItems);
		if (noItems) return;
		adjustStart = -offset >= MeasureItems(start.iterateIndex.group, start.iterateIndex.index, start.iterateIndex.index);
		end = FindFirstVisibleItem(&offset, r2, nullptr, &noItems);
		adjustEnd = !noItems;
		if (noItems) { end.iterateIndex.group = arrlen(groups) - 1; GetLastIndex(&end); }

		if (flags & ES_LIST_VIEW_TILED) {
			int64_t wrapLimit = GetWrapLimit();
			int64_t fixedMinorSize = (flags & ES_LIST_VIEW_HORIZONTAL) ? fixedHeight : fixedWidth;
			intptr_t itemsPerBand = fixedMinorSize && ((fixedMinorSize + currentStyle->gapMinor) < wrapLimit) ? (wrapLimit / (fixedMinorSize + currentStyle->gapMinor)) : 1;
			int64_t computedMinorGap = (wrapLimit - itemsPerBand * fixedMinorSize) / (itemsPerBand + 1);
			int64_t minorStartOffset = computedMinorGap + ((flags & ES_LIST_VIEW_HORIZONTAL) ? currentStyle->insets.t : currentStyle->insets.l);
			intptr_t startInBand = (((flags & ES_LIST_VIEW_HORIZONTAL) ? y1 : x1) - minorStartOffset) / (fixedMinorSize + computedMinorGap);
			intptr_t endInBand = (((flags & ES_LIST_VIEW_HORIZONTAL) ? y2 : x2) - minorStartOffset) / (fixedMinorSize + computedMinorGap);

			if (adjustStart) {
				ListViewGroup *group = groups + start.iterateIndex.group;

				if (((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && start.iterateIndex.index.u == 0)
						|| ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && start.iterateIndex.index.u == group->itemCount - 1)) {
					IterateForwards(&start);
				} else {
					for (intptr_t i = 0; i < itemsPerBand; i++) {
						if ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && start.iterateIndex.index.u == group->itemCount - 1) {
							break;
						}

						IterateForwards(&start);
					}
				}
			}

			if (adjustEnd) {
				ListViewGroup *group = groups + end.iterateIndex.group;

				if (((group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) && end.iterateIndex.index.u == 0)
						|| ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && end.iterateIndex.index.u == group->itemCount - 1)) {
				} else {
					for (intptr_t i = 0; i < itemsPerBand - 1; i++) {
						if ((group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) && end.iterateIndex.index.u == group->itemCount - 1) {
							IterateBackwards(&end);
							break;
						}

						IterateForwards(&end);
					}
				}
			}

			SetSelected(start.iterateIndex.group, start.iterateIndex.index, end.iterateIndex.group, end.iterateIndex.index, true, toggle,
					itemsPerBand, startInBand, endInBand);
		} else {
			if (adjustStart) {
				IterateForwards(&start);
			}

			SetSelected(start.iterateIndex.group, start.iterateIndex.index, end.iterateIndex.group, end.iterateIndex.index, true, toggle);
		}
	}

	void Select(int32_t group, EsGeneric index, bool range, bool toggle, bool moveAnchorOnly) {
		if ((~flags & ES_LIST_VIEW_SINGLE_SELECT) && (~flags & ES_LIST_VIEW_MULTI_SELECT)) {
			return;
		}

		if (!totalItemCount) {
			return;
		}

		if (!hasAnchorItem || (~flags & ES_LIST_VIEW_MULTI_SELECT)) {
			range = false;
		}

		bool emptySpace = false;

		if (group == -1) {
			// Clicked on empty space.
			if (range || toggle) return;
			emptySpace = true;
		}

		if (!range && !emptySpace) {
			hasAnchorItem = true;
			anchorItemGroup = group;
			anchorItemIndex = index;
		}

		if (moveAnchorOnly) {
			return;
		}

		if (!toggle) {
			// Clear existing selection.
			SetSelected(0, GetFirstIndex(0), arrlen(groups) - 1, GetLastIndex(arrlen(groups) - 1), false, false);
		}

		if (range) {
			// Select range.
			SetSelected(anchorItemGroup, anchorItemIndex, group, index, true, false);
		} else if (toggle) {
			// Toggle single item.
			SetSelected(group, index, group, index, false, true);
		} else if (!emptySpace) {
			// Select single item.
			SetSelected(group, index, group, index, true, false);
		}

		for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
			EsMessage m = {};
			m.type = ES_MSG_LIST_VIEW_IS_SELECTED;
			m.selectItem.group = visibleItems[i].group;
			m.selectItem.index = visibleItems[i].index;
			EsMessageSend(this, &m);

			if (m.selectItem.isSelected) {
				visibleItems[i].element->customStyleState |=  THEME_STATE_SELECTED;
			} else {
				visibleItems[i].element->customStyleState &= ~THEME_STATE_SELECTED;
			}

			visibleItems[i].element->MaybeRefreshStyle();
		}
	}

	int ProcessItemMessage(uintptr_t visibleIndex, EsMessage *message) {
		ListViewItem *item = visibleItems + visibleIndex;
		EsElement *element = item->element;

		if (message->type == ES_MSG_PAINT) {
			EsMessage m = { ES_MSG_LIST_VIEW_GET_CONTENT };
			char buffer[512];
			m.getContent.buffer = buffer;
			m.getContent.bufferSpace = sizeof(buffer);
			m.getContent.index = item->index;
			m.getContent.group = item->group;

			EsRectangle bounds = AddBorder(item->element->GetBounds(), item->element->currentStyle->insets);

			if (flags & ES_LIST_VIEW_COLUMNS) {
				for (uintptr_t i = item->startAtSecondColumn ? 1 : 0; i < columnCount; i++) {
					m.getContent.column = i;
					m.getContent.bytes = 0;
					m.getContent.icon = 0;

					bounds.r = bounds.l + columns[i].width 
						- item->element->currentStyle->insets.r - item->element->currentStyle->insets.l;

					if (i == 0) {
						bounds.r -= item->indent * currentStyle->gapWrap;
					}

					EsRectangle drawBounds = { bounds.l + message->painter->offsetX, bounds.r + message->painter->offsetX,
						bounds.t + message->painter->offsetY, bounds.b + message->painter->offsetY };

					if (EsRectangleClip(drawBounds, message->painter->clip, nullptr) 
							&& ES_HANDLED == EsMessageSend(this, &m)) {
						UIStyle *style = i ? secondaryCellStyle : primaryCellStyle;

						uint8_t previousTextAlign = style->textAlign;

						if (columns[i].flags & ES_LIST_VIEW_COLUMN_RIGHT_ALIGNED) {
							style->textAlign ^= ES_TEXT_H_RIGHT | ES_TEXT_H_LEFT;
						}

						EsTextSelection selection = {};
						selection.hideCaret = true;
						
						if (searchBufferBytes && item->showSearchHighlight && !i) {
							selection.caret1 = searchBufferBytes;
							selection.hideCaret = false;
						}

						style->PaintText(message->painter, bounds, m.getContent.buffer, m.getContent.bytes, m.getContent.icon,
								0, &selection);
						style->textAlign = previousTextAlign;
					}

					bounds.l += columns[i].width + secondaryCellStyle->gapMajor;

					if (i == 0) {
						bounds.l -= item->indent * currentStyle->gapWrap;
					}
				}
			} else {
				if (ES_HANDLED == EsMessageSend(this, &m)) {
					primaryCellStyle->PaintText(message->painter, bounds, m.getContent.buffer, m.getContent.bytes, m.getContent.icon);
				}
			}
		} else if (message->type == ES_MSG_LAYOUT) {
			if (element->GetChildCount()) {
				EsElement *child = element->GetChild(0);
				EsRectangle bounds = element->GetBounds();
				child->InternalMove(bounds.r - bounds.l, bounds.b - bounds.t, bounds.l, bounds.t);
			}
		} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
			EsElementFocus(this, false);

			ListViewItem *oldFocus = FindVisibleItem(focusedItemGroup, focusedItemIndex);

			if (oldFocus) {
				oldFocus->element->customStyleState &= ~THEME_STATE_FOCUSED_ITEM;
				oldFocus->element->MaybeRefreshStyle();
			}

			hasFocusedItem = true;
			focusedItemGroup = item->group;
			focusedItemIndex = item->index;
			element->customStyleState |= THEME_STATE_FOCUSED_ITEM;

			Select(item->group, item->index, gui.holdingShift, gui.holdingCtrl, false);

			if (message->mouseDown.clickChainCount == 2 && !gui.holdingShift && !gui.holdingCtrl) {
				EsMessage m = { ES_MSG_LIST_VIEW_CHOOSE_ITEM };
				m.chooseItem.group = item->group;
				m.chooseItem.index = item->index;
				EsMessageSend(this, &m);
			}
		} else if (message->type == ES_MSG_GET_INSPECTOR_INFORMATION) {
			EsMessage m = { ES_MSG_LIST_VIEW_GET_CONTENT };
			char buffer2[256];
			m.getContent.buffer = buffer2;
			m.getContent.bufferSpace = sizeof(buffer2);
			m.getContent.index = item->index;
			m.getContent.group = item->group;
			EsMessageSend(this, &m);

			char *buffer = message->getContent.buffer;
			size_t bufferSpace = message->getContent.bufferSpace;
			message->getContent.bytes = EsStringFormat(buffer, bufferSpace, "index %d '%s'", item->index.u, m.getContent.bytes, m.getContent.buffer);
		} else {
			return 0;
		}

		return ES_HANDLED;
	}

	inline int GetWrapLimit() {
		EsRectangle bounds = GetListBounds();
		return (flags & ES_LIST_VIEW_HORIZONTAL) 
			? bounds.b - bounds.t - currentStyle->insets.b - currentStyle->insets.t 
			: bounds.r - bounds.l - currentStyle->insets.r - currentStyle->insets.l;
	}

	ListViewItem *FindVisibleItem(int32_t group, EsGeneric index) {
		for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
			ListViewItem *item = visibleItems + i;

			if (item->group == group && item->index.u == index.u) {
				return item;
			}
		}

		return nullptr;
	}

	bool Search() {
		char buffer[64];

		if (!hasFocusedItem) {
			// Select the first item in the list.
			KeyInput(ES_SCANCODE_DOWN_ARROW, false, false, false, true);
			if (!hasFocusedItem) return false;
		}

		EsMessage m = { ES_MSG_LIST_VIEW_SEARCH };
		m.searchItem.index = focusedItemIndex;
		m.searchItem.group = focusedItemGroup;
		m.searchItem.query = searchBuffer;
		m.searchItem.queryBytes = searchBufferBytes;
		int response = EsMessageSend(this, &m);

		if (response == ES_REJECTED) {
			return false;
		} 

		ListViewItem *oldFocus = FindVisibleItem(focusedItemGroup, focusedItemIndex);

		if (oldFocus) {
			oldFocus->element->customStyleState &= ~THEME_STATE_FOCUSED_ITEM;
			oldFocus->element->MaybeRefreshStyle();
		}
		
		bool found = false;

		if (response == ES_HANDLED) {
			focusedItemIndex = m.searchItem.index;
			focusedItemGroup = m.searchItem.group;
			found = true;
		} else {
			EsMessage m = { ES_MSG_LIST_VIEW_GET_CONTENT };
			m.getContent.buffer = buffer;
			m.getContent.bufferSpace = sizeof(buffer);
			m.getContent.index = focusedItemIndex;
			m.getContent.group = focusedItemGroup;
			EsMessage m2 = {};
			m2.iterateIndex.index = focusedItemIndex;
			m2.iterateIndex.group = focusedItemGroup;

			do {
				EsMessageSend(this, &m);

				if (EsStringStartsWith(m.getContent.buffer, m.getContent.bytes, searchBuffer, searchBufferBytes, true)) {
					found = true;
					break;
				}

				if (!IterateForwards(&m2)) {
					m2.getContent.group = 0;
					GetFirstIndex(&m2);
				}

				m.getContent.index = m2.iterateIndex.index;
				m.getContent.group = m2.iterateIndex.group;
			} while (m.getContent.index.u != focusedItemIndex.u || m.getContent.group != focusedItemGroup);

			focusedItemIndex = m.getContent.index;
			focusedItemGroup = m.getContent.group;
			EnsureItemVisible(focusedItemGroup, focusedItemIndex, false);
		}

		ListViewItem *newFocus = FindVisibleItem(focusedItemGroup, focusedItemIndex);

		if (newFocus) {
			newFocus->element->customStyleState |= THEME_STATE_FOCUSED_ITEM;
			newFocus->element->MaybeRefreshStyle();
		}

		{
			EsMessage m = { ES_MSG_LIST_VIEW_GET_CONTENT };
			m.getContent.buffer = buffer;
			m.getContent.bufferSpace = sizeof(buffer);

			for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
				ListViewItem *item = visibleItems + i;
				m.getContent.index = item->index;
				m.getContent.group = item->group;
				EsMessageSend(this, &m);
				bool shouldShowSearchHighlight = EsStringStartsWith(m.getContent.buffer, m.getContent.bytes, searchBuffer, searchBufferBytes, true);

				if (shouldShowSearchHighlight || (!shouldShowSearchHighlight && item->showSearchHighlight)) {
					item->showSearchHighlight = shouldShowSearchHighlight;
					item->element->Repaint(true);
				}
			}
		}

		Select(-1, 0, false, false, false);
		Select(focusedItemGroup, focusedItemIndex, false, false, false);
		return found;
	}

	bool KeyInput(int scancode, bool ctrl, bool alt, bool shift, bool keepSearchBuffer = false) {
		if (!totalItemCount || alt) {
			return false;
		}

		if (scancode == ES_SCANCODE_BACKSPACE && searchBufferBytes) {
			searchBufferBytes = 0;
			Search();
			return true;
		}

		bool isNext = false, 
		     isPrevious = false,
		     isNextBand = false,
		     isPreviousBand = false,
		     isHome = scancode == ES_SCANCODE_HOME,
		     isEnd = scancode == ES_SCANCODE_END,
		     isPageUp = scancode == ES_SCANCODE_PAGE_UP,
		     isPageDown = scancode == ES_SCANCODE_PAGE_DOWN,
		     isSpace = scancode == ES_SCANCODE_SPACE,
		     isEnter = scancode == ES_SCANCODE_ENTER;

		if (flags & ES_LIST_VIEW_HORIZONTAL) {
			isNext = scancode == ES_SCANCODE_DOWN_ARROW;
			isNextBand = scancode == ES_SCANCODE_RIGHT_ARROW;
			isPrevious = scancode == ES_SCANCODE_UP_ARROW;
			isPreviousBand = scancode == ES_SCANCODE_LEFT_ARROW;
		} else {
			isNext = scancode == ES_SCANCODE_RIGHT_ARROW;
			isNextBand = scancode == ES_SCANCODE_DOWN_ARROW;
			isPrevious = scancode == ES_SCANCODE_LEFT_ARROW;
			isPreviousBand = scancode == ES_SCANCODE_UP_ARROW;
		}

		if (hasSelectionBoxAnchor) {
			if (scancode == ES_SCANCODE_UP_ARROW)     scroll.SetY(scroll.y - GetConstantNumber("scrollKeyMovement"));
			if (scancode == ES_SCANCODE_DOWN_ARROW)   scroll.SetY(scroll.y + GetConstantNumber("scrollKeyMovement"));
			if (scancode == ES_SCANCODE_LEFT_ARROW)   scroll.SetX(scroll.x - GetConstantNumber("scrollKeyMovement"));
			if (scancode == ES_SCANCODE_RIGHT_ARROW)  scroll.SetX(scroll.x + GetConstantNumber("scrollKeyMovement"));
			if (scancode == ES_SCANCODE_PAGE_UP)      scroll.SetY(scroll.y - Height(GetBounds()));
			if (scancode == ES_SCANCODE_PAGE_DOWN)    scroll.SetY(scroll.y + Height(GetBounds()));
			
			if (flags & ES_LIST_VIEW_HORIZONTAL) {
				if (scancode == ES_SCANCODE_HOME) scroll.SetX(0);
				if (scancode == ES_SCANCODE_END)  scroll.SetX(scroll.xLimit);
			} else {
				if (scancode == ES_SCANCODE_HOME) scroll.SetY(0);
				if (scancode == ES_SCANCODE_END)  scroll.SetY(scroll.yLimit);
			}
		} else if (isPrevious || isNext || isHome || isEnd || isPageUp || isPageDown || isNextBand || isPreviousBand) {
			ListViewItem *oldFocus = FindVisibleItem(focusedItemGroup, focusedItemIndex);

			if (oldFocus) {
				oldFocus->element->customStyleState &= ~THEME_STATE_FOCUSED_ITEM;
				oldFocus->element->MaybeRefreshStyle();
			}

			EsMessage m = {};

			if (hasFocusedItem && (isPrevious || isNext || isPageUp || isPageDown || isNextBand || isPreviousBand)) {
				m.iterateIndex.group = focusedItemGroup;
				m.iterateIndex.index = focusedItemIndex;

				uintptr_t itemsPerBand = GetItemsPerBand();

				for (uintptr_t i = 0; i < ((isPageUp || isPageDown) ? (10 * itemsPerBand) : (isNextBand || isPreviousBand) ? itemsPerBand : 1); i++) {
					if (isNext || isPageDown || isNextBand) IterateForwards(&m);
					else IterateBackwards(&m);
				}
			} else {
				if (isNext || isNextBand || isHome) {
					m.iterateIndex.group = 0;
					GetFirstIndex(&m);
				} else {
					m.iterateIndex.group = arrlen(groups) - 1;
					GetLastIndex(&m);
				}
			}

			hasFocusedItem = true;
			focusedItemGroup = m.iterateIndex.group;
			focusedItemIndex = m.iterateIndex.index;

			ListViewItem *newFocus = FindVisibleItem(focusedItemGroup, focusedItemIndex);

			if (newFocus) {
				newFocus->element->customStyleState |= THEME_STATE_FOCUSED_ITEM;
				newFocus->element->MaybeRefreshStyle();
			}

			if (!keepSearchBuffer) ClearSearchBuffer();
			EnsureItemVisible(focusedItemGroup, focusedItemIndex, isPrevious || isHome || isPageUp || isPreviousBand);
			Select(focusedItemGroup, focusedItemIndex, shift, ctrl, ctrl && !shift);
			return true;
		} else if (isSpace && ctrl && !shift && hasFocusedItem) {
			Select(focusedItemGroup, focusedItemIndex, false, true, false);
			return true;
		} else if (isEnter && hasFocusedItem && !shift && !ctrl && !alt) {
			EsMessage m = { ES_MSG_LIST_VIEW_CHOOSE_ITEM };
			m.chooseItem.group = focusedItemGroup;
			m.chooseItem.index = focusedItemIndex;
			EsMessageSend(this, &m);
			return true;
		} else if (!ctrl && !alt) {
			uint64_t currentTime = EsTimeStamp() / esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];

			if (searchBufferLastKeyTime + GetConstantNumber("listViewSearchBufferTimeout") * 1000 < currentTime) {
				searchBufferBytes = 0;
			}

			StartAnimating();
			searchBufferLastKeyTime = currentTime;
			int ic = 0, isc = 0;
			ConvertScancodeToCharacter(scancode, ic, isc, false, false);
			int character = shift ? isc : ic;

			if (character && searchBufferBytes + 4 < sizeof(searchBuffer)) {
				utf8_encode(character, searchBuffer + searchBufferBytes);
				size_t previousSearchBufferBytes = searchBufferBytes;
				searchBufferBytes += utf8_length_char(searchBuffer + searchBufferBytes);
				if (!Search()) searchBufferBytes = previousSearchBufferBytes;
				return true;
			}
		}

		return false;
	}

	void ClearSearchBuffer() {
		searchBufferBytes = 0;

		for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
			if (visibleItems[i].showSearchHighlight) {
				visibleItems[i].showSearchHighlight = false;
				visibleItems[i].element->Repaint(true);
			}
		}
	}

	int ProcessMessage(EsMessage *message) {
		scroll.ReceivedMessage(message);

		if (message->type == ES_MSG_GET_WIDTH || message->type == ES_MSG_GET_HEIGHT) {
			if (flags & ES_LIST_VIEW_HORIZONTAL) {
				message->measure.width  = totalSize + currentStyle->insets.l + currentStyle->insets.r;
			} else {
				message->measure.height = totalSize + currentStyle->insets.t + currentStyle->insets.b;

				if (flags & ES_LIST_VIEW_COLUMNS) {
					message->measure.width = totalColumnWidth + currentStyle->insets.l + currentStyle->insets.r;
				}
			}
		} else if (message->type == ES_MSG_LAYOUT) {
			firstLayout = true;
			Wrap(message->layout.sizeChanged);
			Populate();

			if (columnHeader) {
				EsRectangle bounds = GetBounds();
				columnHeader->InternalMove(Width(bounds), columnHeader->currentStyle->preferredHeight, 0, 0);
			}
		} else if (message->type == ES_MSG_SCROLL_X || message->type == ES_MSG_SCROLL_Y) {
			int64_t delta = message->scrollbarMoved.scroll - message->scrollbarMoved.previous;

			if ((message->type == ES_MSG_SCROLL_X) == ((flags & ES_LIST_VIEW_HORIZONTAL) ? true : false)) {
				for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
					if (flags & ES_LIST_VIEW_HORIZONTAL) visibleItems[i].element->offsetX -= delta;
					else				     visibleItems[i].element->offsetY -= delta;
				}
			}

			Populate();
			Repaint(true);

			if (columnHeader) {
				EsElementRelayout(columnHeader);
			}

			if (selectionBox) {
				EsPoint position = EsMouseGetPosition(this);
				selectionBoxPositionX = position.x + scroll.x;
				selectionBoxPositionY = position.y + scroll.y;
				SelectPreview();
			}
		} else if (message->type == ES_MSG_DESTROY) {
			primaryCellStyle->CloseReference(primaryCellStyleKey);
			secondaryCellStyle->CloseReference(secondaryCellStyleKey);
			arrfree(groups);
			arrfree(visibleItems);
		} else if (message->type == ES_MSG_KEY_UP) {
			if (message->keyboard.scancode == ES_SCANCODE_LEFT_CTRL || message->keyboard.scancode == ES_SCANCODE_RIGHT_CTRL) {
				SelectPreview();
			}
		} else if (message->type == ES_MSG_KEY_DOWN) {
			if (message->keyboard.scancode == ES_SCANCODE_LEFT_CTRL || message->keyboard.scancode == ES_SCANCODE_RIGHT_CTRL) {
				SelectPreview();
			}

			return KeyInput(message->keyboard.scancode, message->keyboard.ctrl, message->keyboard.alt, message->keyboard.shift)
				? ES_HANDLED : 0;
		} else if (message->type == ES_MSG_FOCUSED_START) {
			for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
				ListViewItem *item = visibleItems + i;
				item->element->customStyleState |= THEME_STATE_LIST_FOCUSED;

				if (hasFocusedItem && focusedItemGroup == item->group && focusedItemIndex.u == item->index.u) {
					item->element->customStyleState |= THEME_STATE_FOCUSED_ITEM;
				}

				item->element->MaybeRefreshStyle();
			}
		} else if (message->type == ES_MSG_FOCUSED_END) {
			for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
				ListViewItem *item = visibleItems + i;
				item->element->customStyleState &= ~(THEME_STATE_LIST_FOCUSED | THEME_STATE_FOCUSED_ITEM);
				item->element->MaybeRefreshStyle();
			}
		} else if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
			Select(-1, 0, gui.holdingShift, gui.holdingCtrl, false);
		} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
			if (selectionBox) {
				Repaint(false, ES_MAKE_RECTANGLE(selectionBox->offsetX, selectionBox->offsetX + selectionBox->width,
							selectionBox->offsetY, selectionBox->offsetY + selectionBox->height));
				
				if (!hasSelectionBoxAnchor) {
					hasSelectionBoxAnchor = true;
					selectionBoxAnchorX = message->mouseDragged.originalPositionX + scroll.x;
					selectionBoxAnchorY = message->mouseDragged.originalPositionY + scroll.y;

					if (gui.lastClickButton == ES_MSG_MOUSE_LEFT_DOWN) {
						Select(-1, 0, gui.holdingShift, gui.holdingCtrl, false);
					}
				}

				EsElementSetDisabled(selectionBox, false);

				selectionBoxPositionX = message->mouseDragged.newPositionX + scroll.x;
				selectionBoxPositionY = message->mouseDragged.newPositionY + scroll.y;

				SelectPreview();
			}
		} else if (message->type == ES_MSG_MOUSE_LEFT_UP || message->type == ES_MSG_MOUSE_RIGHT_UP) {
			if (selectionBox) {
				EsElementSetDisabled(selectionBox, true);

				if (hasSelectionBoxAnchor) {
					hasSelectionBoxAnchor = false;

					int64_t x1 = selectionBoxPositionX, x2 = selectionBoxAnchorX,
						y1 = selectionBoxPositionY, y2 = selectionBoxAnchorY;
					if (x1 > x2) { int64_t temp = x1; x1 = x2; x2 = temp; }
					if (y1 > y2) { int64_t temp = y1; y1 = y2; y2 = temp; }

					SelectBox(x1, x2, y1, y2, gui.holdingCtrl);
				}

				for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
					EsMessage m = { ES_MSG_LIST_VIEW_IS_SELECTED };
					m.selectItem.index = visibleItems[i].index;
					m.selectItem.group = visibleItems[i].group;
					EsMessageSend(this, &m);

					EsElement *item = visibleItems[i].element;

					if (m.selectItem.isSelected) {
						item->customStyleState |= THEME_STATE_SELECTED;
					} else {
						item->customStyleState &= ~THEME_STATE_SELECTED;
					}

					item->MaybeRefreshStyle();
				}
			}
		} else if (message->type == ES_MSG_Z_ORDER) {
			uintptr_t index = message->zOrder.index;

			if (index < arrlenu(visibleItems)) {
				EsAssert(arrlenu(zOrderItems) == arrlenu(visibleItems));
				message->zOrder.child = zOrderItems[index];
				return ES_HANDLED;
			} else {
				index -= arrlenu(visibleItems);
			}

			if (selectionBox) {
				if (index == 0) {
					message->zOrder.child = selectionBox;
					return ES_HANDLED;
				} else {
					index--;
				}
			}

			if (columnHeader) {
				if (index == 0) {
					message->zOrder.child = columnHeader;
					return ES_HANDLED;
				} else {
					index--;
				}
			}

			message->zOrder.child = nullptr;
		} else if (message->type == ES_MSG_PAINT && !totalItemCount && emptyMessageBytes) {
			UIStyle *style = GetStyle(MakeStyleKey(ES_STYLE_TEXT_LABEL_SECONDARY, 0), true);
			EsTextPlanProperties properties = {};
			properties.flags = ES_TEXT_H_CENTER | ES_TEXT_V_CENTER | ES_TEXT_WRAP | ES_TEXT_PLAN_SINGLE_USE;
			EsTextRun textRun[2] = {};
			style->GetTextStyle(&textRun[0].style);
			textRun[1].offset = emptyMessageBytes;
			EsRectangle bounds = EsPainterBoundsInset(message->painter); 
			EsTextPlan *plan = EsTextPlanCreate(&properties, bounds, emptyMessage, textRun, 1);
			EsDrawText(message->painter, plan, bounds); 
		} else if (message->type == ES_MSG_ANIMATE) {
			uint64_t currentTime = EsTimeStamp() / esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
			int64_t remainingTime = searchBufferLastKeyTime + GetConstantNumber("listViewSearchBufferTimeout") * 1000 - currentTime;

			if (remainingTime < 0) {
				ClearSearchBuffer();
			} else {
				message->animate.waitUs = remainingTime;
				message->animate.complete = false;
			}
		} else if (message->type == ES_MSG_BEFORE_Z_ORDER) {
			EsAssert(!zOrderItems);
			intptr_t focused = -1, hovered = -1;

			for (uintptr_t i = 0; i < arrlenu(visibleItems); i++) {
				if (hasFocusedItem && visibleItems[i].index.u == focusedItemIndex.u && visibleItems[i].group == focusedItemGroup) {
					focused = i;
				} else if (visibleItems[i].element->state & UI_STATE_HOVERED) {
					hovered = i;
				} else {
					arrput(zOrderItems, visibleItems[i].element);
				}
			}

			if (hovered != -1) {
				arrput(zOrderItems, visibleItems[hovered].element);
			}
			
			if (focused != -1) {
				arrput(zOrderItems, visibleItems[focused].element);
			}
		} else if (message->type == ES_MSG_AFTER_Z_ORDER) {
			arrfree(zOrderItems);
		} else {
			return 0;
		}

		return ES_HANDLED;
	}
};

EsListView *EsListViewCreate(EsElement *parent, uint64_t flags, const EsStyle *style, 
		const EsStyle *itemStyle, const EsStyle *headerItemStyle, const EsStyle *footerItemStyle) {
	EsListView *view = (EsListView *) EsHeapAllocate(sizeof(EsListView), true);

	flags |= ES_ELEMENT_FOCUSABLE;

	view->itemStyle = itemStyle ?: ES_STYLE_LIST_ITEM;
	view->headerItemStyle = headerItemStyle ?: ES_STYLE_LIST_ITEM_GROUP_HEADER;
	view->footerItemStyle = footerItemStyle ?: ES_STYLE_LIST_ITEM_GROUP_FOOTER;
	GetPreferredSizeFromStylePart(view->itemStyle, &view->fixedWidth, &view->fixedHeight);
	GetPreferredSizeFromStylePart(view->headerItemStyle, (flags & ES_LIST_VIEW_HORIZONTAL) ? &view->fixedHeaderSize : nullptr,
			(flags & ES_LIST_VIEW_HORIZONTAL) ? nullptr : &view->fixedHeaderSize);
	GetPreferredSizeFromStylePart(view->footerItemStyle, (flags & ES_LIST_VIEW_HORIZONTAL) ? &view->fixedFooterSize : nullptr,
			(flags & ES_LIST_VIEW_HORIZONTAL) ? nullptr : &view->fixedFooterSize);

	view->primaryCellStyleKey = MakeStyleKey(ES_STYLE_LIST_PRIMARY_CELL, 0);
	view->primaryCellStyle = GetStyle(view->primaryCellStyleKey, false);
	view->secondaryCellStyleKey = MakeStyleKey(ES_STYLE_LIST_SECONDARY_CELL, 0);
	view->secondaryCellStyle = GetStyle(view->secondaryCellStyleKey, false);

	view->Initialise(parent, flags, [] (EsElement *element, EsMessage *message) { return ((EsListView *) element)->ProcessMessage(message); }, style ?: ES_STYLE_LIST_VIEW);
	view->cName = "list view";

	if (flags & ES_LIST_VIEW_MULTI_SELECT) {
		view->selectionBox = EsCustomElementCreate(view, ES_CELL_FILL | ES_ELEMENT_DISABLED | ES_ELEMENT_NO_HOVER, ES_STYLE_LIST_SELECTION_BOX);
		view->selectionBox->cName = "selection box";
	}

	if (flags & ES_LIST_VIEW_COLUMNS) {
		view->columnHeader = EsCustomElementCreate(view, ES_CELL_FILL, ES_STYLE_LIST_COLUMN_HEADER);
		view->columnHeader->cName = "column header";
		view->columnHeader->userData = view;

		view->columnHeader->userCallback = [] (EsElement *element, EsMessage *message) {
			EsListView *view = (EsListView *) element->userData.p;

			if (message->type == ES_MSG_LAYOUT) {
				int x = view->currentStyle->insets.l - view->scroll.x;

				for (uintptr_t i = 0; i < arrlenu(element->children); i += 2) {
					EsElement *item = element->children[i], *splitter = element->children[i + 1];
					EsListViewColumn *column = (EsListViewColumn *) item->userData.p;
					int splitterLeft = splitter->currentStyle->preferredWidth - view->secondaryCellStyle->gapMajor;
					item->InternalMove(column->width - splitterLeft, element->height, x, 0);
					splitter->InternalMove(splitter->currentStyle->preferredWidth, element->height, x + column->width - splitterLeft, 0);
					x += column->width + view->secondaryCellStyle->gapMajor;
				}
			}

			return 0;
		};

		view->scroll.fixedViewportHeight = view->columnHeader->currentStyle->preferredHeight;
	}

	// It's safe to use SCROLL_MODE_AUTO even in tiled mode,
	// because decreasing the secondary axis can only increase the primary axis.

	uint8_t scrollXMode = 0, scrollYMode = 0;

	if (flags & ES_LIST_VIEW_COLUMNS) {
		scrollXMode = SCROLL_MODE_AUTO;
		scrollYMode = SCROLL_MODE_AUTO;
	} else if (flags & ES_LIST_VIEW_HORIZONTAL) {
		scrollXMode = SCROLL_MODE_AUTO;
	} else {
		scrollYMode = SCROLL_MODE_AUTO;
	}

	view->scroll.Setup(view, scrollXMode, scrollYMode, SCROLL_X_DRAG | SCROLL_Y_DRAG);

	return view;
}

void EsListViewInsertGroup(EsListView *view, int32_t group, uint32_t flags) {
	EsMessageMutexCheck();

	// Add the group.

	ListViewGroup empty = { .flags = flags };
	EsAssert(group <= arrlen(view->groups)); // Invalid group index.
	arrins(view->groups, group, empty);

	// Update the group index on visible items.

	uintptr_t firstVisibleItemToMove = arrlenu(view->visibleItems);

	for (uintptr_t i = 0; i < arrlenu(view->visibleItems); i++) {
		ListViewItem *item = view->visibleItems + i;

		if (item->group >= group) {
			item->group++;

			if (i < firstVisibleItemToMove) {
				firstVisibleItemToMove = i;
			}
		}
	}

	// Insert gap between groups.

	view->InsertSpace(arrlenu(view->groups) > 1 ? view->currentStyle->gapMajor : 0, firstVisibleItemToMove);

	// Create header and footer items.

	int64_t additionalItems = ((flags & ES_LIST_VIEW_GROUP_HAS_HEADER) ? 1 : 0) + ((flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) ? 1 : 0);
	if (additionalItems) EsListViewInsert(view, group, 0, additionalItems - 1, additionalItems);
	view->groups[group].initialised = true;
}

void EsListViewInsert(EsListView *view, int32_t groupIndex, EsGeneric firstIndex, EsGeneric lastIndex, int64_t count) {
	EsMessageMutexCheck();

	bool pushIndices = false;

	if (~view->flags & ES_LIST_VIEW_NON_LINEAR) {
		// Skip check for non-linear views, because it'd be expensive.
		EsAssert(firstIndex.i <= lastIndex.i); // Invalid item index range.

		pushIndices = true;
	}

	// Get the group.

	EsAssert(groupIndex < arrlen(view->groups)); // Invalid group index.
	ListViewGroup *group = view->groups + groupIndex;

	if (group->initialised) {
		if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
			EsAssert(firstIndex.i > 0); // Cannot insert before group header.
		}

		if (group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) {
			EsAssert(firstIndex.i < (intptr_t) group->itemCount); // Cannot insert after group footer.
		}
	}

	// Add the items to the group.

	bool alreadySetItemCount = false;
	bool addedFirstItemInGroup = !group->itemCount;

	if (count != -1) {
		// Setting the item count early is necessary for MeasureItems when adding the group header/footer items.
		alreadySetItemCount = true;
		group->itemCount += count;
	}

	int64_t totalSizeOfItems = view->MeasureItems(groupIndex, firstIndex, lastIndex, &count);
	int64_t sizeToAdd = (count - (addedFirstItemInGroup ? 1 : 0)) * view->currentStyle->gapMinor + totalSizeOfItems;

	if (!alreadySetItemCount) group->itemCount += count;
	group->totalSize += sizeToAdd;
	view->totalItemCount += count;

	// Update indices of visible items.

	uintptr_t firstVisibleItemToMove = arrlenu(view->visibleItems);

	if (view->hasFocusedItem && view->focusedItemGroup == groupIndex && pushIndices) {
		if (view->CompareIndices(groupIndex, view->focusedItemIndex, firstIndex) >= 0) {
			view->focusedItemIndex.i += count;
		}
	}

	if (view->hasAnchorItem && view->anchorItemGroup == groupIndex && pushIndices) {
		if (view->CompareIndices(groupIndex, view->anchorItemIndex, firstIndex) >= 0) {
			view->anchorItemIndex.i += count;
		}
	}

	for (uintptr_t i = 0; i < arrlenu(view->visibleItems); i++) {
		ListViewItem *item = view->visibleItems + i;
		if (item->group != groupIndex) continue;

		if (view->flags & ES_LIST_VIEW_NON_LINEAR) {
			int result = 1;

			if (firstVisibleItemToMove >= i) {
				result = view->CompareIndices(groupIndex, item->index, lastIndex);
			}

			EsAssert(result != 0); // Adding item already in the list.

			if (result > 0) {
				if (i < firstVisibleItemToMove) {
					firstVisibleItemToMove = i;
				}
			}
		} else {
			if (item->index.i >= firstIndex.i) {
				item->index.i += count;

				if (i < firstVisibleItemToMove) {
					firstVisibleItemToMove = i;
				}
			}
		}
	}

	// Insert the space for the items.

	view->InsertSpace(sizeToAdd, firstVisibleItemToMove);
}

void EsListViewRemove(EsListView *view, int32_t groupIndex, EsGeneric firstIndex, EsGeneric lastIndex, int64_t count) {
	EsMessageMutexCheck();

	bool pushIndices = false;

	if (~view->flags & ES_LIST_VIEW_NON_LINEAR) {
		// Skip check for non-linear views, because it'd be expensive.
		EsAssert(firstIndex.i <= lastIndex.i); // Invalid item index range.

		pushIndices = true;
	}

	// Get the group.

	EsAssert(groupIndex < arrlen(view->groups)); // Invalid group index.
	ListViewGroup *group = view->groups + groupIndex;

	if (group->initialised) {
		if (group->flags & ES_LIST_VIEW_GROUP_HAS_HEADER) {
			EsAssert(firstIndex.i > 0); // Cannot remove the group header.
		}

		if (group->flags & ES_LIST_VIEW_GROUP_HAS_FOOTER) {
			EsAssert(lastIndex.i < (intptr_t) group->itemCount - 1); // Cannot remove the group footer.
		}
	}

	// Remove the items from the group.

	int64_t totalSizeOfItems = view->MeasureItems(groupIndex, firstIndex, lastIndex, &count);
	int64_t sizeToRemove = (int64_t) group->itemCount == count ? group->totalSize 
		: (count * view->currentStyle->gapMinor + totalSizeOfItems);

	group->itemCount -= count;
	group->totalSize -= sizeToRemove;
	view->totalItemCount -= count;

	// Update indices of visible items,
	// and remove deleted items.

	uintptr_t firstVisibleItemToMove = arrlenu(view->visibleItems);

	if (view->hasFocusedItem && view->focusedItemGroup == groupIndex) {
		if (view->CompareIndices(groupIndex, view->focusedItemIndex, firstIndex) >= 0 
				&& view->CompareIndices(groupIndex, view->focusedItemIndex, lastIndex) <= 0) {
			view->hasFocusedItem = false;
		} else if (pushIndices) {
			view->focusedItemIndex.i -= count;
		}
	}

	if (view->hasAnchorItem && view->anchorItemGroup == groupIndex) {
		if (view->CompareIndices(groupIndex, view->focusedItemIndex, firstIndex) >= 0 
				&& view->CompareIndices(groupIndex, view->anchorItemIndex, lastIndex) <= 0) {
			view->hasAnchorItem = false;
		} else if (pushIndices) {
			view->anchorItemIndex.i -= count;
		}
	}

	for (uintptr_t i = 0; i < arrlenu(view->visibleItems); i++) {
		ListViewItem *item = view->visibleItems + i;
		if (item->group != groupIndex) continue;

		if (view->flags & ES_LIST_VIEW_NON_LINEAR) {
			int r1 = view->CompareIndices(groupIndex, item->index, firstIndex);
			int r2 = view->CompareIndices(groupIndex, item->index, lastIndex);

			if (r1 < 0) EsAssert(r2 < 0); // Invalid index order.
			if (r2 > 0) EsAssert(r1 > 0); // Invalid index order.

			if (r2 > 0) {
				if (i < firstVisibleItemToMove) {
					firstVisibleItemToMove = i;
				}
			} else if (r1 >= 0 && r2 <= 0) {
				item->element->userData = i;
				item->element->Destroy();
				arrdel(view->visibleItems, i);
				i--;
			}
		} else {
			if (item->index.i > lastIndex.i) {
				item->index.i -= count;

				if (i < firstVisibleItemToMove) {
					firstVisibleItemToMove = i;
				}
			} else if (item->index.i >= firstIndex.i && item->index.i <= lastIndex.i) {
				item->element->userData = i;
				item->element->Destroy();
				arrdel(view->visibleItems, i);
				i--;
			}
		}
	}

	// Remove the space of the items.

	view->InsertSpace(-sizeToRemove, firstVisibleItemToMove);
}

EsGeneric EsListViewGetIndex(EsListView *view, EsElement *item) {
	EsMessageMutexCheck();

	for (uintptr_t i = 0; i < arrlenu(view->visibleItems); i++) {
		if (arrlenu(view->visibleItems[i].element->children) && view->visibleItems[i].element->children[0] == item) {
			return view->visibleItems[i].index;
		}
	}

	EsAssert(false); // Element was not a visible item in the list.
	return 0;
}

void EsListViewRemoveAll(EsListView *view, int32_t group) {
	EsMessageMutexCheck();

	if (view->groups[group].itemCount) {
		EsListViewRemove(view, group, view->GetFirstIndex(group), view->GetLastIndex(group), view->groups[group].itemCount);
	}
}

void EsListViewSetColumns(EsListView *view, EsListViewColumn *columns, size_t columnCount) {
	EsMessageMutexCheck();

	EsAssert(view->flags & ES_LIST_VIEW_COLUMNS); // List view does not have columns flag set.

	for (uintptr_t i = 0; i < arrlenu(view->columnHeader->children); i++) {
		view->columnHeader->children[i]->Destroy();
	}

	view->columns = columns;
	view->columnCount = columnCount;

	view->totalColumnWidth = -view->secondaryCellStyle->gapMajor;

	for (uintptr_t i = 0; i < columnCount; i++) {
		EsElement *columnHeaderItem = EsCustomElementCreate(view->columnHeader, ES_CELL_FILL, 
				(columns[i].flags & ES_LIST_VIEW_COLUMN_HAS_MENU) ? ES_STYLE_LIST_COLUMN_HEADER_ITEM_HAS_MENU : ES_STYLE_LIST_COLUMN_HEADER_ITEM);

		columnHeaderItem->userCallback = [] (EsElement *element, EsMessage *message) {
			EsListViewColumn *column = (EsListViewColumn *) element->userData.p;

			if (message->type == ES_MSG_PAINT) {
				element->currentStyle->PaintText(message->painter, element->GetBounds(), 
						column->title, column->titleBytes, 0, 
						(column->flags & ES_LIST_VIEW_COLUMN_DESCENDING) ? MARKER_SORT_DESCENDING
						: (column->flags & ES_LIST_VIEW_COLUMN_ASCENDING) ? MARKER_SORT_ASCENDING : 0);
			}

			return 0;
		};

		columnHeaderItem->cName = "column header item";
		columnHeaderItem->userData = columns + i;

		if (!columns[i].width) {
			columns[i].width = (i ? view->secondaryCellStyle : view->primaryCellStyle)->preferredWidth;
		}

		EsElement *splitter = EsCustomElementCreate(view->columnHeader, ES_CELL_FILL, ES_STYLE_LIST_COLUMN_HEADER_SPLITTER);

		splitter->userCallback = [] (EsElement *element, EsMessage *message) {
			EsListViewColumn *column = (EsListViewColumn *) element->userData.p;
			EsListView *view = (EsListView *) element->parent->parent;

			if (message->type == ES_MSG_MOUSE_LEFT_DOWN) {
				view->columnResizingOriginalWidth = column->width;
			} else if (message->type == ES_MSG_MOUSE_DRAGGED) {
				int width = message->mouseDragged.newPositionX - message->mouseDragged.originalPositionX + view->columnResizingOriginalWidth;
				int minimumWidth = element->currentStyle->metrics->minimumWidth;
				if (width < minimumWidth) width = minimumWidth;

				view->totalColumnWidth += width - column->width;
				column->width = width;
				EsElementRelayout(element->parent);
				EsElementRelayout(view);

				return ES_HANDLED;
			}

			return 0;
		},

		splitter->cName = "column header splitter";
		splitter->userData = columns + i;

		view->totalColumnWidth += columns[i].width + view->secondaryCellStyle->gapMajor;
	}

	view->scroll.Refresh();
}

void EsListViewContentChanged(EsListView *view) {
	EsMessageMutexCheck();

	view->searchBufferLastKeyTime = 0;
	view->searchBufferBytes = 0;

	view->scroll.SetX(0);
	view->scroll.SetY(0);
}

void EsListViewSelect(EsListView *view, int32_t group, EsGeneric index) {
	view->Select(group, index, false, false, false);
}

void EsListViewSetEmptyMessage(EsListView *view, const char *message, ptrdiff_t messageBytes) {
	if (messageBytes == -1) messageBytes = EsCStringLength(message);
	HeapDuplicate((void **) &view->emptyMessage, message, messageBytes);
	view->emptyMessageBytes = messageBytes;

	if (!view->totalItemCount) {
		view->Repaint(true);
	}
}

EsGeneric EsListViewGetIndexFromItem(EsElement *element, int32_t *group) {
	EsListView *view = (EsListView *) element->parent;
	EsAssert(element->userData.u < arrlenu(view->visibleItems));
	if (group) *group = view->visibleItems[element->userData.u].group;
	return view->visibleItems[element->userData.u].index;
}

void EsListViewInvalidateContent(EsListView *view, int32_t group, EsGeneric index) {
	for (uintptr_t i = 0; i < arrlenu(view->visibleItems); i++) {
		if (view->visibleItems[i].group == group && view->visibleItems[i].index.u == index.u) {
			view->visibleItems[i].element->Repaint(true);
			break;
		}
	}
}
