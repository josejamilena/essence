#ifndef IMPLEMENTATION

#define K_IN_CORE_KERNEL
#define K_PRIVATE
#include "module.h"

#include <kernel_config.h>
#include <shared/ini.h>

#define EsAssertionFailure(file, line) KernelPanic("%z:%d - EsAssertionFailure called.\n", file, line)

#ifdef ARCH_X86_64
#include "x86_64.cpp"
#endif

struct AsyncTask {
	KAsyncTaskCallback callback;
	void *argument;
	struct MMSpace *addressSpace;
};

struct CPULocalStorage {
	// Must be the first fields; used in __cyg_profile_func_enter.
	uintptr_t kernelFunctionCallCount;

	struct Thread *currentThread, 
		      *idleThread, 
		      *asyncTaskThread;

	struct InterruptContext *panicContext;

	bool irqSwitchThread, schedulerReady, inIRQ;

	unsigned processorID;
	size_t spinlockCount;

	ArchCPU *archCPU;

	// TODO Have separate interrupt task threads and system worker threads (with no task limit).
#define MAX_ASYNC_TASKS (256)
	volatile AsyncTask asyncTasks[MAX_ASYNC_TASKS];
	volatile uint8_t asyncTasksRead, asyncTasksWrite;

#ifdef DEBUG_BUILD
	uintptr_t lastSchedulerLogPosition;
#endif
};

void KernelInitialise();
void KernelShutdown();

void ArchInitialise();
void ArchShutdown();

extern "C" void ArchResetCPU();
extern "C" void ArchSpeakerBeep();
extern "C" void ArchNextTimer(size_t ms); // Receive a TIMER_INTERRUPT in ms ms.
InterruptContext *ArchInitialiseThread(uintptr_t kernelStack, uintptr_t kernelStackSize, struct Thread *thread, 
		uintptr_t startAddress, uintptr_t argument1, uintptr_t argument2,
		 bool userland, uintptr_t stack, uintptr_t userStackSize);

uint64_t timeStampTicksPerMs;

EsUniqueIdentifier installationID; // The identifier of this OS installation, given to us by the bootloader.
struct Process *desktopProcess;

KSpinlock ipiLock;

KDriver **loadedDrivers;
KMutex loadedDriversMutex;

int kernelReportedProblems;

#endif

#include <shared/avl_tree.cpp>
#include <shared/bitset.cpp>
#include <shared/range_set.cpp>

#include "memory.cpp"

#ifndef IMPLEMENTATION
#include <shared/heap.cpp>
#include <shared/arena.cpp>
#endif

#include "objects.cpp"
#include "syscall.cpp"
#include "scheduler.cpp"
#include "synchronisation.cpp"
#include "drivers.cpp"
#include "elf.cpp"
#include "graphics.cpp"
#include "cache.cpp"
#include "files.cpp"
#include "windows.cpp"
#include "audio.cpp"
#include "networking.cpp"

#ifdef ENABLE_POSIX_SUBSYSTEM
#include "posix.cpp"
#endif

#include "terminal.cpp"

#ifdef IMPLEMENTATION

extern "C" uint64_t KGetTimeInMs() {
	if (!timeStampTicksPerMs) return 0;
	return scheduler.timeMs;
}

uint64_t KGetTimeStampTicksPerMs() {
	return timeStampTicksPerMs;
}

uint64_t KGetTimeStampTicksPerUs() {
	return timeStampTicksPerMs / 1000;
}

bool KInIRQ() {
	return GetLocalStorage()->inIRQ;
}

bool KBootedFromEFI() {
	extern uint32_t bootloaderID;
	return bootloaderID == 2;
}

void KSwitchThreadAfterIRQ() {
	GetLocalStorage()->irqSwitchThread = true; 
}

EsUniqueIdentifier KGetBootIdentifier() {
	return installationID;
}

#ifdef ARCH_X86_64
#include "x86_64.h"
#include <drivers/acpi.cpp>
#include "x86_64.cpp"
#endif

#endif
