// TODO Single global handle table?
// TODO Reject opening handles if the handle table has been destroyed!!

#ifndef IMPLEMENTATION

inline KernelObjectType operator|(KernelObjectType a, KernelObjectType b) {
	return (KernelObjectType) ((int) a | (int) b);
}

struct Handle {
	void *object;	
	uint64_t flags; // Make this uint32_t?
	KernelObjectType type;
};

struct Mailslot {
	Process *target;
	void *context;
	KMutex mutex;
	volatile size_t handles;

	bool SendData(void *data, size_t bytes);
	bool Close();
	bool SendMessage(EsMessage *message);
};

struct ConstantBuffer {
	volatile size_t handles;
	size_t bytes : 48,
	       isPaged : 1;
	// Data follows.
};

struct Pipe {
#define PIPE_READER (1)
#define PIPE_WRITER (2)
#define PIPE_BUFFER_SIZE (K_PAGE_SIZE)
#define PIPE_CLOSED (0)

	volatile char buffer[PIPE_BUFFER_SIZE];
	volatile size_t writers, readers;
	volatile uintptr_t writePosition, readPosition, unreadData;
	KEvent canWrite, canRead;
	KMutex mutex;

	size_t Access(void *buffer, size_t bytes, bool write, bool userBlockRequest);
};

struct EventSink {
	KEvent available;
	KSpinlock spinlock; // Take after the scheduler's spinlock.
	volatile size_t handles;
	uintptr_t queuePosition;
	size_t queueCount;
	bool overflow, ignoreDuplicates;
	EsGeneric queue[ES_MAX_EVENT_SINK_BUFFER_SIZE];

	EsError Push(EsGeneric data);
};

struct EventSinkTable {
	EventSink *sink;
	EsGeneric data;
};

size_t totalHandleCount;

struct HandleTableL2 {
#define HANDLE_TABLE_L2_ENTRIES 2048
	Handle t[HANDLE_TABLE_L2_ENTRIES];
};

struct HandleTableL1 {
#define HANDLE_TABLE_L1_ENTRIES 2048
	HandleTableL2 *t[HANDLE_TABLE_L1_ENTRIES];
	uint16_t u[HANDLE_TABLE_L1_ENTRIES];
};

struct HandleTable {
	HandleTableL1 l1r;
	KMutex lock;
	Process *process;
	bool destroyed;

	EsHandle OpenHandle(void *_object, uint64_t _flags, KernelObjectType _type, EsHandle at = ES_INVALID_HANDLE);
	bool CloseHandle(EsHandle handle);

	// Resolve the handle if it is valid and return the type in type.
	// The initial value of type is used as a mask of expected object types for the handle.
	void ResolveHandle(struct KObject *object, EsHandle handle); 

	void Destroy(); 
};

struct KObject {
	void Initialise(Process *process, EsHandle _handle, KernelObjectType _type);

	KObject() {
		EsMemoryZero(this, sizeof(*this));
	}

	KObject(Process *process, EsHandle _handle, KernelObjectType _type) {
		EsMemoryZero(this, sizeof(*this));
		Initialise(process, _handle, _type);
	}

	~KObject() {
		if (!checked && valid) {
			KernelPanic("KObject - Object not checked!\n");
		}

		if (parentObject && close) {
			CloseHandleToObject(parentObject, parentType, parentFlags);
		}
	}

	void *object, *parentObject;
	uint64_t flags, parentFlags;
	KernelObjectType type, parentType;
	bool valid, checked, close, softFailure;
};

void InitialiseObjectManager();

#endif

#ifdef IMPLEMENTATION

void KObject::Initialise(Process *process, EsHandle _handle, KernelObjectType _type) {
	type = _type;

	process->handleTable.ResolveHandle(this, _handle);
	parentObject = object, parentType = type, parentFlags = flags;

	if (!valid) {
		KernelLog(LOG_ERROR, "Object Manager", "invalid handle", "KObject::Initialise - Invalid handle %d for type mask %x.\n", _handle, _type);
	}
}

// A lock used to change the handle count on several objects.
// TODO Make changing handle count lockless wherever possible?
KMutex objectHandleCountChange;

// TODO Use uint64_t for handle counts, or restrict OpenHandleToObject to some maximum (...but most callers don't check if OpenHandleToObject succeeds).

bool OpenHandleToObject(void *object, KernelObjectType type, uint64_t flags, bool maybeHasNoHandles) {
	bool hadNoHandles = false, failed = false;

	switch (type) {
		case KERNEL_OBJECT_EVENT: {
			objectHandleCountChange.Acquire();
			KEvent *event = (KEvent *) object;
			if (!event->handles) hadNoHandles = true;
			else event->handles++;
			objectHandleCountChange.Release();
		} break;

		case KERNEL_OBJECT_PROCESS: {
			scheduler.lock.Acquire();
			Process *process = (Process *) object;
			if (!process->handles) hadNoHandles = true;
			else process->handles++; // NOTE Scheduler::OpenProcess and MMBalanceThread also adjust process handles.
			KernelLog(LOG_VERBOSE, "Scheduler", "open process handle", "Opened handle to process %d; %d handles.\n", process->id, process->handles);
			scheduler.lock.Release();
		} break;

		case KERNEL_OBJECT_THREAD: {
			scheduler.lock.Acquire();
			Thread *thread = (Thread *) object;
			if (!thread->handles) hadNoHandles = true;
			else thread->handles++;
			scheduler.lock.Release();
		} break;

		case KERNEL_OBJECT_SHMEM: {
			MMSharedRegion *region = (MMSharedRegion *) object;
			region->mutex.Acquire();
			if (!region->handles) hadNoHandles = true;
			else region->handles++;
			region->mutex.Release();
		} break;

		case KERNEL_OBJECT_WINDOW: {
			// NOTE The handle count of Window object is modified elsewhere.
			Window *window = (Window *) object;
			hadNoHandles = 0 == __sync_fetch_and_add(&window->handles, 1);
		} break;

		case KERNEL_OBJECT_EMBEDDED_WINDOW: {
			EmbeddedWindow *window = (EmbeddedWindow *) object;
			hadNoHandles = 0 == __sync_fetch_and_add(&window->handles, 1);
		} break;

		case KERNEL_OBJECT_CONSTANT_BUFFER: {
			ConstantBuffer *buffer = (ConstantBuffer *) object;
			objectHandleCountChange.Acquire();
			if (!buffer->handles) hadNoHandles = true;
			else buffer->handles++;
			objectHandleCountChange.Release();
		} break;

		case KERNEL_OBJECT_MAILSLOT: {
			Mailslot *mailslot = (Mailslot *) object;
			mailslot->mutex.Acquire();
			if (!mailslot->handles) hadNoHandles = true;
			else mailslot->handles++;
			mailslot->mutex.Release();
		} break;

		case KERNEL_OBJECT_TIMER: {
			objectHandleCountChange.Acquire();
			KTimer *timer = (KTimer *) object;
			if (!timer->handles) hadNoHandles = true;
			else timer->handles++;
			objectHandleCountChange.Release();
		} break;

#if 0
		case KERNEL_OBJECT_DIRECTORY_MONITOR: {
			objectHandleCountChange.Acquire();
			DirectoryMonitor *monitor = (DirectoryMonitor *) object;
			if (!monitor->handles) hadNoHandles = true;
			else monitor->handles++;
			objectHandleCountChange.Release();
		} break;
#endif

#ifdef ENABLE_POSIX_SUBSYSTEM
		case KERNEL_OBJECT_POSIX_FD: {
			POSIXFile *file = (POSIXFile *) object;
			file->mutex.Acquire();
			if (!file->handles) hadNoHandles = true;
			else file->handles++;
			file->mutex.Release();
		} break;
#endif

		case KERNEL_OBJECT_NODE: {
			failed = ES_SUCCESS != FSNodeOpenHandle((KNode *) object, flags);
		} break;

		case KERNEL_OBJECT_PIPE: {
			Pipe *pipe = (Pipe *) object;
			pipe->mutex.Acquire();

			if (((flags & PIPE_READER) && !pipe->readers)
					|| ((flags & PIPE_WRITER) && !pipe->writers)) {
				hadNoHandles = true;
			} else {
				if (flags & PIPE_READER) {
					pipe->readers++;
				} 

				if (flags & PIPE_WRITER) {
					pipe->writers++;
				} 
			}

			pipe->mutex.Release();
		} break;

		case KERNEL_OBJECT_AUDIO_STREAM: {
			objectHandleCountChange.Acquire();
			KAudioStream *stream = (KAudioStream *) object;
			if (!stream->handles) hadNoHandles = true;
			else stream->handles++;
			objectHandleCountChange.Release();
		} break;

		case KERNEL_OBJECT_EVENT_SINK: {
			EventSink *sink = (EventSink *) object;
			sink->spinlock.Acquire();
			if (!sink->handles) hadNoHandles = true;
			else sink->handles++;
			sink->spinlock.Release();
		} break;

		case KERNEL_OBJECT_CONNECTION: {
			NetConnection *connection = (NetConnection *) object;
			hadNoHandles = 0 == __sync_fetch_and_add(&connection->handles, 1);
		} break;

		default: {
			KernelPanic("OpenHandleToObject - Cannot open object of type %x.\n", type);
		} break;
	}

	if (hadNoHandles) {
		if (maybeHasNoHandles) {
			return false;
		} else {
			KernelPanic("OpenHandleToObject - Object %x of type %x had no handles.\n", object, type);
		}
	}

	return !failed;
}

void CloseHandleToObject(void *object, KernelObjectType type, uint64_t flags) {
	switch (type) {
		case KERNEL_OBJECT_PROCESS: {
			CloseHandleToProcess(object);
		} break;

		case KERNEL_OBJECT_THREAD: {
			CloseHandleToThread(object);
		} break;

		case KERNEL_OBJECT_NODE: {
			FSNodeCloseHandle((KNode *) object, flags);
		} break;

		case KERNEL_OBJECT_EVENT: {
			KEvent *event = (KEvent *) object;
			objectHandleCountChange.Acquire();
			bool destroy = event->handles == 1;
			event->handles--;
			objectHandleCountChange.Release();

			if (destroy) {
				if (event->sinkTable) {
					for (uintptr_t i = 0; i < ES_MAX_EVENT_FORWARD_COUNT; i++) {
						if (event->sinkTable[i].sink) {
							CloseHandleToObject(event->sinkTable[i].sink, KERNEL_OBJECT_EVENT_SINK, 0);
						}
					}

					EsHeapFree(event->sinkTable, sizeof(EventSinkTable) * ES_MAX_EVENT_FORWARD_COUNT, K_FIXED);
				}

				EsHeapFree(event, sizeof(KEvent), K_FIXED);
			}
		} break;

		case KERNEL_OBJECT_CONSTANT_BUFFER: {
			ConstantBuffer *buffer = (ConstantBuffer *) object;
			objectHandleCountChange.Acquire();
			bool destroy = buffer->handles == 1;
			buffer->handles--;
			objectHandleCountChange.Release();

			if (destroy) {
				EsHeapFree(object, sizeof(ConstantBuffer) + buffer->bytes, buffer->isPaged ? K_PAGED : K_FIXED);
			}
		} break;

		case KERNEL_OBJECT_SHMEM: {
			MMSharedRegion *region = (MMSharedRegion *) object;
			region->mutex.Acquire();
			bool destroy = region->handles == 1;
			region->handles--;
			region->mutex.Release();

			if (destroy) {
				MMSharedDestroyRegion(region);
			}
		} break;

		case KERNEL_OBJECT_TIMER: {
			KTimer *timer = (KTimer *) object;
			objectHandleCountChange.Acquire();
			bool destroy = timer->handles == 1;
			timer->handles--;
			objectHandleCountChange.Release();

			if (destroy) {
				timer->Remove();
				EsHeapFree(timer, sizeof(KTimer), K_FIXED);
			}
		} break;

		case KERNEL_OBJECT_WINDOW: {
			Window *window = (Window *) object;
			unsigned previous = __sync_fetch_and_sub(&window->handles, 1);
			if (!previous) KernelPanic("CloseHandleToObject - Window %x has no handles.\n", window);

			if (previous == 2) {
				windowManager.windowsToCloseEvent.Set();
			} else if (previous == 1) {
				window->Destroy();
			}
		} break;

		case KERNEL_OBJECT_EMBEDDED_WINDOW: {
			EmbeddedWindow *window = (EmbeddedWindow *) object;
			unsigned previous = __sync_fetch_and_sub(&window->handles, 1);
			if (!previous) KernelPanic("CloseHandleToObject - EmbeddedWindow %x has no handles.\n", window);

			if (previous == 2) {
				windowManager.windowsToCloseEvent.Set();
			} else if (previous == 1) {
				window->Destroy();
			}
		} break;

		case KERNEL_OBJECT_MAILSLOT: {
			Mailslot *mailslot = (Mailslot *) object;
			mailslot->mutex.Acquire();
			mailslot->handles--;
			bool destroy = !mailslot->handles;
			mailslot->mutex.Release();

			if (destroy) {
				mailslot->Close();
				CloseHandleToObject(mailslot->target, KERNEL_OBJECT_PROCESS, 0);
				EsHeapFree(mailslot, sizeof(Mailslot), K_FIXED);
			}
		} break;

#ifdef ENABLE_POSIX_SUBSYSTEM
		case KERNEL_OBJECT_POSIX_FD: {
			POSIXFile *file = (POSIXFile *) object;
			file->mutex.Acquire();
			file->handles--;
			bool destroy = !file->handles;
			file->mutex.Release();

			if (destroy) {
				if (file->type == POSIX_FILE_NORMAL || file->type == POSIX_FILE_DIRECTORY) CloseHandleToObject(file->node, KERNEL_OBJECT_NODE, file->openFlags);
				if (file->type == POSIX_FILE_PIPE) CloseHandleToObject(file->pipe, KERNEL_OBJECT_PIPE, file->openFlags);
				EsHeapFree(file->path, 0, K_FIXED);
				EsHeapFree(file->directoryBuffer, file->directoryBufferLength, K_FIXED);
				EsHeapFree(file, sizeof(POSIXFile), K_FIXED);
			}
		} break;
#endif

		case KERNEL_OBJECT_PIPE: {
			Pipe *pipe = (Pipe *) object;
			pipe->mutex.Acquire();

			if (flags & PIPE_READER) {
				pipe->readers--;

				if (!pipe->readers) {
					// If there are no more readers, wake up any blocking writers.
					pipe->canWrite.Set(false, true);
				}
			} 
			
			if (flags & PIPE_WRITER) {
				pipe->writers--;

				if (!pipe->writers) {
					// If there are no more writers, wake up any blocking readers.
					pipe->canRead.Set(false, true);
				}
			} 

			bool destroy = pipe->readers == 0 && pipe->writers == 0;

			pipe->mutex.Release();

			if (destroy) {
				EsHeapFree(pipe, sizeof(Pipe), K_PAGED);
			}
		} break;

#if 0
		case KERNEL_OBJECT_DIRECTORY_MONITOR: {
			objectHandleCountChange.Acquire();

			DirectoryMonitor *monitor = (DirectoryMonitor *) object;
			monitor->handles--;

			bool deallocate = !monitor->handles;

			objectHandleCountChange.Release();

			if (deallocate) {
				monitor->directory->directory.monitorLock.Take(K_LOCK_EXCLUSIVE);

				bool found = false;

				for (uintptr_t i = 0; i < ArrayLength(monitor->directory->directory.monitors); i++) {
					if (monitor->directory->directory.monitors[i] == monitor) {
						found = true;
						ArrayDelete(monitor->directory->directory.monitors, i);
					}
				}

				if (!found) {
					KernelPanic("CloseHandleToObject - Monitor could not be found in directory's list of monitors.\n");
				}

				monitor->directory->directory.monitorLock.Return(K_LOCK_EXCLUSIVE);

				_EsMessageWithObject m = { monitor->context.p, ES_MSG_FS_MONITOR_DESTROY };
				monitor->process->messageQueue.SendMessage(m);

				CloseHandleToObject(monitor->directory, KERNEL_OBJECT_NODE);
				CloseHandleToObject(monitor->process, KERNEL_OBJECT_PROCESS);
				EsHeapFree(monitor, sizeof(DirectoryMonitor), K_FIXED);
			}
		} break;
#endif

		case KERNEL_OBJECT_AUDIO_STREAM: {
			KAudioStream *stream = (KAudioStream *) object;
			objectHandleCountChange.Acquire();
			bool destroy = stream->handles == 1;
			stream->handles--;
			objectHandleCountChange.Release();
			if (destroy) AudioStreamDestroy(stream);
		} break;

		case KERNEL_OBJECT_EVENT_SINK: {
			EventSink *sink = (EventSink *) object;
			sink->spinlock.Acquire();
			bool destroy = sink->handles == 1;
			sink->handles--;
			sink->spinlock.Release();

			if (destroy) {
				EsHeapFree(sink, sizeof(EventSink), K_FIXED);
			}
		} break;

		case KERNEL_OBJECT_CONNECTION: {
			NetConnection *connection = (NetConnection *) object;
			unsigned previous = __sync_fetch_and_sub(&connection->handles, 1);
			if (!previous) KernelPanic("CloseHandleToObject - NetConnection %x has no handles.\n", connection);
			if (previous == 1) NetConnectionClose(connection);
		} break;

		default: {
			KernelPanic("CloseHandleToObject - Cannot close object of type %x.\n", type);
		} break;
	}
}

bool HandleTable::CloseHandle(EsHandle handle) {
	if (handle > HANDLE_TABLE_L1_ENTRIES * HANDLE_TABLE_L2_ENTRIES) {
		return false;
	}

	lock.Acquire();

	HandleTableL1 *l1 = &l1r;
	HandleTableL2 *l2 = l1->t[handle / HANDLE_TABLE_L2_ENTRIES];
	if (!l2) { lock.Release(); return false; }
	Handle *_handle = l2->t + (handle % HANDLE_TABLE_L2_ENTRIES);
	KernelObjectType type = _handle->type;
	uint64_t flags = _handle->flags;
	void *object = _handle->object;
	if (!object) { lock.Release(); return false; }
	EsMemoryZero(_handle, sizeof(Handle));
	l1->u[handle / HANDLE_TABLE_L2_ENTRIES]--;

	lock.Release();

	__sync_fetch_and_sub(&totalHandleCount, 1);
	CloseHandleToObject(object, type, flags);
	return true;
}

void HandleTable::ResolveHandle(struct KObject *object, EsHandle handle) {
	// Special handles.
	{ 
		if (handle == ES_CURRENT_THREAD && (object->type & KERNEL_OBJECT_THREAD)) {
			object->type = KERNEL_OBJECT_THREAD, object->valid = true, object->object = GetCurrentThread();
			return;
		} else if (handle == ES_INVALID_HANDLE && (object->type & KERNEL_OBJECT_NONE)) {
			object->type = KERNEL_OBJECT_NONE, object->valid = true;
			return;
		} else if (handle == ES_CURRENT_PROCESS && (object->type & KERNEL_OBJECT_PROCESS)) {
			object->type = KERNEL_OBJECT_PROCESS, object->valid = true, object->object = GetCurrentThread()->process;
			return;
		}

#ifdef ENABLE_POSIX_SUBSYSTEM
		if (object->type & KERNEL_OBJECT_POSIX_FD) {
			Process *currentProcess = GetCurrentThread()->process;

			if (handle == 1 && (!l1r.t[0] || !l1r.t[0]->t[1].object)) {
				object->type = KERNEL_OBJECT_POSIX_FD, object->valid = true, object->object = &currentProcess->posixData->standardOutput;
				return;
			} else if (handle == 3 && (!l1r.t[0] || !l1r.t[0]->t[3].object)) {
				object->type = KERNEL_OBJECT_POSIX_FD, object->valid = true, object->object = &currentProcess->posixData->standardInput;
				return;
			} else if (handle == 2 && (!l1r.t[0] || !l1r.t[0]->t[2].object)) {
				object->type = KERNEL_OBJECT_POSIX_FD, object->valid = true, object->object = &currentProcess->posixData->standardError;
				return;
			}
		}
#endif
	}

	// Check that the handle is within the correct bounds.
	if ((!handle) || handle >= HANDLE_TABLE_L1_ENTRIES * HANDLE_TABLE_L2_ENTRIES) {
		object->type = COULD_NOT_RESOLVE_HANDLE;
		return;
	}

	lock.Acquire();
	EsDefer(lock.Release());

	uintptr_t l1Index = handle / HANDLE_TABLE_L2_ENTRIES;
	uintptr_t l2Index = handle % HANDLE_TABLE_L2_ENTRIES;

	HandleTableL1 *l1 = &l1r;
	HandleTableL2 *l2 = l1->t[l1Index];

	if (!l2) {
		object->type = COULD_NOT_RESOLVE_HANDLE;
		return;
	}

	Handle *_handle = l2->t + l2Index;

	if ((_handle->type & object->type) && (_handle->object)) {
		// Open a handle to the object so that it can't be destroyed while the system call is still using it.
		// The handle is closed in the KObject's destructor.
		object->type = _handle->type, object->valid = true, object->object = _handle->object, object->flags = _handle->flags, object->close = true;
		OpenHandleToObject(object->object, object->type, object->flags);
	} else {
		object->type = COULD_NOT_RESOLVE_HANDLE;
	}
}

// TODO Switch the order of flags and type, so that the default value of flags can be 0.
EsHandle HandleTable::OpenHandle(void *object, uint64_t flags, KernelObjectType type, EsHandle at) {
	lock.Acquire();
	EsDefer(lock.Release());

	Handle handle = {};
	handle.object = object;
	handle.flags = flags;
	handle.type = type;

	{
		if (destroyed) goto error;

		if (!handle.object) {
			KernelPanic("HandleTable::OpenHandle - Invalid object.\n");
		}

		HandleTableL1 *l1 = &l1r;
		uintptr_t l1Index = HANDLE_TABLE_L1_ENTRIES;

		for (uintptr_t i = 1 /* The first set of handles are reserved. */; i < HANDLE_TABLE_L1_ENTRIES; i++) {
			if (at) i = at / HANDLE_TABLE_L2_ENTRIES;

			if (l1->u[i] != HANDLE_TABLE_L2_ENTRIES) {
				l1->u[i]++;
				l1Index = i;
				break;
			}

			if (at) goto error;
		}

		if (l1Index == HANDLE_TABLE_L1_ENTRIES) goto error;

		if (!l1->t[l1Index]) l1->t[l1Index] = (HandleTableL2 *) EsHeapAllocate(sizeof(HandleTableL2), true, K_FIXED);
		HandleTableL2 *l2 = l1->t[l1Index];
		uintptr_t l2Index = HANDLE_TABLE_L2_ENTRIES;

		for (uintptr_t i = 0; i < HANDLE_TABLE_L2_ENTRIES; i++) {
			if (at) i = at % HANDLE_TABLE_L2_ENTRIES;

			if (!l2->t[i].object) {
				l2Index = i;
				break;
			}

			if (at) goto error;
		}

		if (l2Index == HANDLE_TABLE_L2_ENTRIES)	KernelPanic("HandleTable::OpenHandle - Unexpected lack of free handles.\n");
		Handle *_handle = l2->t + l2Index;
		*_handle = handle;

		__sync_fetch_and_add(&totalHandleCount, 1);

		EsHandle index = l2Index + (l1Index * HANDLE_TABLE_L2_ENTRIES);
		return index;
	}

	error:;
	// TODO Close the handle to the object with CloseHandleToObject?
	return ES_INVALID_HANDLE;
}

void HandleTable::Destroy() {
	lock.Acquire();
	EsDefer(lock.Release());

	destroyed = true;
	HandleTableL1 *l1 = &l1r;

	for (uintptr_t i = 0; i < HANDLE_TABLE_L1_ENTRIES; i++) {
		if (l1->u[i]) {
			HandleTableL2 *l2 = l1->t[i];

			for (uintptr_t k = 0; k < HANDLE_TABLE_L2_ENTRIES; k++) {
				Handle *handle = l2->t + k;

				if (handle->object) {
					CloseHandleToObject(handle->object, handle->type);
				}
			}

			EsHeapFree(l2, 0, K_FIXED);
		}
	}
}

ConstantBuffer *MakeConstantBuffer(K_USER_BUFFER const void *data, size_t bytes) {
	ConstantBuffer *buffer = (ConstantBuffer *) EsHeapAllocate(sizeof(ConstantBuffer) + bytes, false, K_FIXED);
	if (!buffer) return nullptr;
	EsMemoryZero(buffer, sizeof(ConstantBuffer));
	buffer->handles = 1;
	buffer->bytes = bytes;
	EsMemoryCopy(buffer + 1, data, buffer->bytes);
	return buffer;
}

EsHandle MakeConstantBuffer(K_USER_BUFFER const void *data, size_t bytes, Process *process) {
	void *object = MakeConstantBuffer(data, bytes);

	if (!object) {
		return ES_INVALID_HANDLE;
	}

	EsHandle h = process->handleTable.OpenHandle(object, 0, KERNEL_OBJECT_CONSTANT_BUFFER); 

	if (h == ES_INVALID_HANDLE) {
		CloseHandleToObject(object, KERNEL_OBJECT_CONSTANT_BUFFER, 0);
		return ES_INVALID_HANDLE;
	}

	return h;
}

EsHandle MakeConstantBufferForDesktop(K_USER_BUFFER const void *data, size_t bytes) {
	return MakeConstantBuffer(data, bytes, desktopProcess);
}

bool Mailslot::Close() {
	mutex.Acquire();
	EsDefer(mutex.Release());
	_EsMessageWithObject message = { context, ES_MSG_MAILSLOT_CLOSED };
	return target->messageQueue.SendMessage(message);
}

bool Mailslot::SendData(K_USER_BUFFER void *data, size_t bytes) {
	mutex.Acquire();
	EsDefer(mutex.Release());

	if (!handles) KernelPanic("Mailslot::Send - No handles.\n");

	_EsMessageWithObject m = { context, ES_MSG_RECEIVE_DATA };
	m.message.receive.buffer = bytes ? MakeConstantBuffer(data, bytes, target) : ES_INVALID_HANDLE;
	m.message.receive.bytes = bytes;

	if (bytes && m.message.receive.buffer == ES_INVALID_HANDLE) return false;
	return target->messageQueue.SendMessage(m);
}

bool Mailslot::SendMessage(EsMessage *message) {
	if (message->type == ES_MSG_MAILSLOT_CLOSED || message->type == ES_MSG_RECEIVE_DATA) return false;
	_EsMessageWithObject m = { context, *message };
	return target->messageQueue.SendMessage(m);
}

size_t Pipe::Access(void *_buffer, size_t bytes, bool write, bool user) {
	size_t amount = 0;
	Thread *currentThread = GetCurrentThread();
	// EsPrint("--> %z %d\n", write ? "Write" : "Read", bytes);

	{
		if (user) currentThread->terminatableState = THREAD_USER_BLOCK_REQUEST;

		if (write) {
			// Wait until we can write to  the pipe.
			canWrite.Wait(ES_WAIT_NO_TIMEOUT);
		} else {
			// Wait until we can read from the pipe.
			canRead.Wait(ES_WAIT_NO_TIMEOUT);
		}

		if (user) {
			currentThread->terminatableState = THREAD_IN_SYSCALL;
			if (currentThread->terminating) goto done;
		}

		mutex.Acquire();
		EsDefer(mutex.Release());

		if (write) {
			// EsPrint("Write:\n");

			size_t spaceAvailable = PIPE_BUFFER_SIZE - unreadData;
			size_t toWrite = bytes > spaceAvailable ? spaceAvailable : bytes;

			size_t spaceAvailableRight = PIPE_BUFFER_SIZE - writePosition;
			size_t toWriteRight = toWrite > spaceAvailableRight ? spaceAvailableRight : toWrite;
			size_t toWriteLeft = toWrite - toWriteRight;

			// EsPrint("\tunread: %d; wp: %d\n", unreadData, writePosition);
			// EsPrint("\t%d, %d, %d, %d, %d\n", spaceAvailable, spaceAvailableRight, toWrite, toWriteRight, toWriteLeft);

			EsMemoryCopy((uint8_t *) buffer + writePosition, _buffer, toWriteRight);
			EsMemoryCopy((uint8_t *) buffer, (uint8_t *) _buffer + toWriteRight, toWriteLeft);

			writePosition += toWrite;
			writePosition %= PIPE_BUFFER_SIZE;
			unreadData += toWrite;
			bytes -= toWrite;
			_buffer = (uint8_t *) _buffer + toWrite;
			amount += toWrite;

			canRead.Set(false, true);

			if (!readers) {
				// EsPrint("\tPipe closed\n");
				// Nobody is reading from the pipe, so there's no point writing to it.
				goto done;
			} else	if (PIPE_BUFFER_SIZE == unreadData) {
				canWrite.Reset();
				// EsPrint("\treset canWrite\n");
			}
		} else {
			// EsPrint("Read:\n");

			size_t dataAvailable = unreadData;
			size_t toRead = bytes > dataAvailable ? dataAvailable : bytes;

			size_t spaceAvailableRight = PIPE_BUFFER_SIZE - readPosition;
			size_t toReadRight = toRead > spaceAvailableRight ? spaceAvailableRight : toRead;
			size_t toReadLeft = toRead - toReadRight;

			// EsPrint("\tunread: %d; rp: %d\n", unreadData, readPosition);
			// EsPrint("\t%d, %d, %d, %d, %d\n", dataAvailable, spaceAvailableRight, toRead, toReadRight, toReadLeft);

			EsMemoryCopy(_buffer, (uint8_t *) buffer + readPosition, toReadRight);
			EsMemoryCopy((uint8_t *) _buffer + toReadRight, (uint8_t *) buffer, toReadLeft);

			readPosition += toRead;
			readPosition %= PIPE_BUFFER_SIZE;
			unreadData -= toRead;
			bytes -= toRead;
			_buffer = (uint8_t *) _buffer + toRead;
			amount += toRead;

			canWrite.Set(false, true);

			if (!writers) {
				// Nobody is writing to the pipe, so there's no point reading from it.
				// EsPrint("\tPipe closed\n");
				goto done;
			} else if (!unreadData) {
				canRead.Reset();
				// EsPrint("\treset canRead\n");
			}
		}
	}

	done:;
	// EsPrint("<-- %d (%d remaining, %z)\n", amount, bytes, write ? "Write" : "Read");
	return amount;
}

#endif
