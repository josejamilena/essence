// #define UPDATE_SCREEN_AFTER_EVERY_PAINT_STEP

#ifdef UPDATE_SCREEN_AFTER_EVERY_PAINT_STEP
#define MAYBE_UPDATE_SCREEN() EsSyscall(ES_SYSCALL_REDRAW_ALL, 0, 0, 0, 0)
#else
#define MAYBE_UPDATE_SCREEN()
#endif

void EsElement::SetStylePart(const EsStyle *part, bool refreshIfChanged) {
	UIStyleKey oldStyleKey = currentStyleKey;
	currentStyleKey.part = (uintptr_t) part;

	if (currentStyleKey.part != oldStyleKey.part && refreshIfChanged) {
		RefreshStyle(&oldStyleKey);
	}
}

EsCursorStyle EsElement::GetCursor() {
	EsMessage m = { ES_MSG_GET_CURSOR };

	if (ES_HANDLED == EsMessageSend(this, &m)) {
		return m.cursorStyle;
	}

	return (EsCursorStyle) currentStyle->metrics->cursor;
}

bool UIStyle::IsRegionCompletelyOpaque(EsRectangle region, int width, int height) {
	return region.l >= opaqueInsets.l && region.r < width - opaqueInsets.r
		&& region.t >= opaqueInsets.t && region.b < height - opaqueInsets.b;
}

bool EsElement::StartAnimating() {
	if ((state & UI_STATE_ANIMATING) || (state & UI_STATE_DESTROYING)) return false;
	arrput(gui.animatingElements, this);
	gui.animationSleep = false;
	state |= UI_STATE_ANIMATING;
	lastTimeStamp = 0;
	return true;
}

void EnsureWindowWillUpdate(EsWindow *window);

void ProcessAnimations() {
	uint64_t timeStamp = EsTimeStamp(); // TODO Use global time instead.
	int64_t waitUs = -1;

	for (uintptr_t i = 0; i < arrlenu(gui.animatingElements); i++) {
		EsElement *element = gui.animatingElements[i];
		// EsPrint("Animating %x...\n", element);
		EsAssert(element->state & UI_STATE_ANIMATING); // Element was not animating but was in the animating elements list.

		if (element->lastTimeStamp == 0) {
			element->lastTimeStamp = timeStamp;
		}

		EsMessage m = {};
		m.type = ES_MSG_ANIMATE;
		m.animate.deltaUs = (timeStamp - element->lastTimeStamp) / esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
		m.animate.deltaUs -= m.animate.deltaUs % 1000;
		m.animate.complete = true;

		if (!m.animate.deltaUs) {
			waitUs = 0;
			continue;
		}

		if (ThemeAnimationStep(&element->animation, m.animate.deltaUs / 1000)) {
			element->Repaint(true, ES_MAKE_RECTANGLE_ALL(0));
		}

		bool backgroundAnimationComplete = ThemeAnimationComplete(&element->animation);

		EsMessageSend(element, &m);

		if (m.animate.complete && backgroundAnimationComplete) {
			arrdelswap(gui.animatingElements, i);
			element->state &= ~UI_STATE_ANIMATING;
			i--;

			if (element->state & UI_STATE_EXITING) {
				EsElement *ancestor = element;

				while (ancestor) {
					ancestor->state |= UI_STATE_DESTROYING_CHILD;
					ancestor = ancestor->parent;
				}

				element->state &= ~UI_STATE_EXITING;
			}
		} else if (m.animate.waitUs < waitUs || waitUs == -1) {
			waitUs = m.animate.waitUs;
		}

		element->lastTimeStamp += m.animate.deltaUs * esSystemConstants[ES_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
		EnsureWindowWillUpdate(element->window);
	}

	if (waitUs != -1) {
		int64_t waitMs = waitUs / 1000;

		if (waitMs) {
			gui.animationSleep = true;

			EsTimerSet(gui.animationSleepTimer, waitMs, [] (EsGeneric) { 
				gui.animationSleep = false; 
			}, nullptr);
		}
	}
}

bool EsElement::RefreshStyleState() {
	uint16_t styleStateFlags = customStyleState;

	if (flags & ES_ELEMENT_DISABLED) {
		styleStateFlags |= THEME_PRIMARY_STATE_DISABLED;
	} else {
		if (((state & UI_STATE_PRESSED) && ((state & UI_STATE_HOVERED) || gui.draggingStarted || (state & UI_STATE_STRONG_PRESSED))) || (state & UI_STATE_MENU_SOURCE)) {
			styleStateFlags |= THEME_PRIMARY_STATE_PRESSED;
		} else if (((state & UI_STATE_HOVERED) && !window->pressed) || (state & UI_STATE_PRESSED)) {
			styleStateFlags |= THEME_PRIMARY_STATE_HOVERED;
		} else {
			styleStateFlags |= THEME_PRIMARY_STATE_IDLE;
		}

		if (state & UI_STATE_FOCUSED) {
			styleStateFlags |= THEME_STATE_FOCUSED;
		}
	}

	if (state & UI_STATE_EXITING) {
		styleStateFlags |= THEME_STATE_AFTER_EXIT;
	}

	bool observedBitsChanged = false;

	if (!currentStyle || currentStyle->IsStateChangeObserved(styleStateFlags, previousStyleState)) {
		observedBitsChanged = true;
	}

	previousStyleState = styleStateFlags;

	return observedBitsChanged;
}

void EsElement::RefreshStyle(UIStyleKey *_oldStyleKey, bool alreadyRefreshStyleState, bool force) {
	// Compute state flags.

	if (!alreadyRefreshStyleState) {
		RefreshStyleState();
	}

	uint16_t styleStateFlags = previousStyleState;

	// Initialise the style.

	UIStyleKey oldStyleKey = _oldStyleKey ? *_oldStyleKey : currentStyleKey;
	currentStyleKey.stateFlags = styleStateFlags;

	if (!force && 0 == EsMemoryCompare(&currentStyleKey, &oldStyleKey, sizeof(UIStyleKey)) && currentStyle) {
		return;
	}

	if (~state & UI_STATE_ENTERED) {
		if (currentStyle) currentStyle->CloseReference(oldStyleKey);
		oldStyleKey = currentStyleKey;
		oldStyleKey.stateFlags |= THEME_STATE_BEFORE_ENTER;
		currentStyle = GetStyle(oldStyleKey, false);
	}

	UIStyle *oldStyle = currentStyle;
	currentStyle = GetStyle(currentStyleKey, false); // TODO Forcing new styles if force flag set.

	// Respond to modifications.

	bool repaint = false, animate = false;

	if (force) {
		repaint = true;
	}

	if (oldStyle) {
		if (oldStyle->style == currentStyle->style) {
			ThemeAnimationBuild(&animation, oldStyle, oldStyleKey.stateFlags, currentStyleKey.stateFlags);
			animate = !ThemeAnimationComplete(&animation);
		} else {
			ThemeAnimationDestroy(&animation);
		}

		repaint = true;
	} else {
		repaint = animate = true;
	}

	if (repaint) {
		if (animate) StartAnimating();
		Repaint(true, ES_MAKE_RECTANGLE_ALL(0));
	}

	// Delete the old style if necessary.

	if (oldStyle) {
		oldStyle->CloseReference(oldStyleKey);
	}
}

void EnsureWindowWillUpdate(EsWindow *window) {
	if (!window->willUpdate) {
		EsMessage m = { ES_MSG_UPDATE_WINDOW };
		EsMessagePost(window, &m);
		window->willUpdate = true;
	}
}

EsRectangle GetTransitionEffectRectangle(EsRectangle bounds, EsTransitionType type, double progress, bool to) {
	int width = Width(bounds), height = Height(bounds);
	double ratio = (double) height / (double) width;

	if (!to) {
		if (type == ES_TRANSITION_SLIDE_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - progress * height / 2, bounds.b - progress * height / 2);
		} else if (type == ES_TRANSITION_SLIDE_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + progress * height / 2, bounds.b + progress * height / 2); 
		} else if (type == ES_TRANSITION_COVER_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t, bounds.b);
		} else if (type == ES_TRANSITION_COVER_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t, bounds.b);
		} else if (type == ES_TRANSITION_SQUISH_UP || type == ES_TRANSITION_REVEAL_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t, bounds.t + height * (1 - progress));
		} else if (type == ES_TRANSITION_SQUISH_DOWN || type == ES_TRANSITION_REVEAL_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + height * progress, bounds.b);
		} else if (type == ES_TRANSITION_ZOOM_OUT) {
			return ES_MAKE_RECTANGLE(bounds.l + 20 * progress, bounds.r - 20 * progress, 
					bounds.t + 20 * progress * ratio, bounds.b - 20 * progress * ratio);
		} else if (type == ES_TRANSITION_ZOOM_IN) {
			return ES_MAKE_RECTANGLE(bounds.l - 20 * progress, bounds.r + 20 * progress, 
					bounds.t - 20 * progress * ratio, bounds.b + 20 * progress * ratio);
		} else if (type == ES_TRANSITION_ZOOM_OUT_LIGHT) {
			return ES_MAKE_RECTANGLE(bounds.l + 5 * progress, bounds.r - 5 * progress, 
					bounds.t + 5 * progress * ratio, bounds.b - 5 * progress * ratio);
		} else if (type == ES_TRANSITION_ZOOM_IN_LIGHT) {
			return ES_MAKE_RECTANGLE(bounds.l - 5 * progress, bounds.r + 5 * progress, 
					bounds.t - 5 * progress * ratio, bounds.b + 5 * progress * ratio);
		} else if (type == ES_TRANSITION_FADE_IN || type == ES_TRANSITION_FADE_OUT) {
			return bounds;
		} else if (type == ES_TRANSITION_SLIDE_UP_OVER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - progress * height / 4, bounds.b - progress * height / 4);
		} else if (type == ES_TRANSITION_SLIDE_DOWN_OVER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + progress * height / 4, bounds.b + progress * height / 4); 
		} else if (type == ES_TRANSITION_SLIDE_UP_UNDER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - progress * height / 2, bounds.b - progress * height / 2);
		} else if (type == ES_TRANSITION_SLIDE_DOWN_UNDER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + progress * height / 2, bounds.b + progress * height / 2); 
		}
	} else {
		if (type == ES_TRANSITION_SLIDE_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + (1 - progress) * height / 2, bounds.b + (1 - progress) * height / 2); 
		} else if (type == ES_TRANSITION_SLIDE_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - (1 - progress) * height / 2, bounds.b - (1 - progress) * height / 2); 
		} else if (type == ES_TRANSITION_COVER_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + (1 - progress) * height, bounds.b + (1 - progress) * height); 
		} else if (type == ES_TRANSITION_COVER_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - (1 - progress) * height, bounds.b - (1 - progress) * height); 
		} else if (type == ES_TRANSITION_SQUISH_UP || type == ES_TRANSITION_REVEAL_UP) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + (1 - progress) * height, bounds.b); 
		} else if (type == ES_TRANSITION_SQUISH_DOWN || type == ES_TRANSITION_REVEAL_DOWN) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t, bounds.t + progress * height); 
		} else if (type == ES_TRANSITION_ZOOM_OUT) {
			return ES_MAKE_RECTANGLE(bounds.l - 20 * (1 - progress), bounds.r + 20 * (1 - progress), 
					bounds.t - 20 * (1 - progress) * ratio, bounds.b + 20 * (1 - progress) * ratio);
		} else if (type == ES_TRANSITION_ZOOM_IN) {
			return ES_MAKE_RECTANGLE(bounds.l + 20 * (1 - progress), bounds.r - 20 * (1 - progress) + 0.5, 
					bounds.t + 20 * (1 - progress) * ratio, bounds.b - 20 * (1 - progress) * ratio + 0.5);
		} else if (type == ES_TRANSITION_ZOOM_OUT_LIGHT) {
			return ES_MAKE_RECTANGLE(bounds.l - 5 * (1 - progress), bounds.r + 5 * (1 - progress), 
					bounds.t - 5 * (1 - progress) * ratio, bounds.b + 5 * (1 - progress) * ratio);
		} else if (type == ES_TRANSITION_ZOOM_IN_LIGHT) {
			return ES_MAKE_RECTANGLE(bounds.l + 5 * (1 - progress), bounds.r - 5 * (1 - progress) + 0.5, 
					bounds.t + 5 * (1 - progress) * ratio, bounds.b - 5 * (1 - progress) * ratio + 0.5);
		} else if (type == ES_TRANSITION_FADE_IN || type == ES_TRANSITION_FADE_OUT) {
			return bounds;
		} else if (type == ES_TRANSITION_SLIDE_UP_OVER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + (1 - progress) * height / 2, bounds.b + (1 - progress) * height / 2); 
		} else if (type == ES_TRANSITION_SLIDE_DOWN_OVER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - (1 - progress) * height / 2, bounds.b - (1 - progress) * height / 2); 
		} else if (type == ES_TRANSITION_SLIDE_UP_UNDER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t + (1 - progress) * height / 4, bounds.b + (1 - progress) * height / 4); 
		} else if (type == ES_TRANSITION_SLIDE_DOWN_UNDER) {
			return ES_MAKE_RECTANGLE(bounds.l, bounds.r, 
					bounds.t - (1 - progress) * height / 4, bounds.b - (1 - progress) * height / 4); 
		}
	}

	EsAssert(false); // Unknown transition type.
	return {};
}

void EsDrawPaintTarget(EsPainter *painter, EsPaintTarget *source, EsRectangle destinationRegion, EsRectangle sourceRegion, uint8_t alpha) {
	bool scale = !(Width(destinationRegion) == Width(sourceRegion) && Height(destinationRegion) == Height(sourceRegion));

	if (scale) {
		ImageDraw((uint32_t *) painter->target->bits, painter->target->width, painter->target->height, painter->target->stride,
				(uint32_t *) source->bits, source->width, source->height, source->stride,
				destinationRegion, sourceRegion, alpha, painter->target->fullAlpha);
	} else {
		EsDrawBitmap(painter, destinationRegion, 
				(uint32_t *) source->bits + sourceRegion.l + sourceRegion.t * source->stride / 4, 
				source->stride, source->fullAlpha ? alpha : 0xFFFF);
	}
}

void DrawTransitionEffect(EsPainter *painter, EsPaintTarget *sourceSurface, EsRectangle bounds, EsTransitionType type, double progress, bool to) {
	EsRectangle destinationRegion = GetTransitionEffectRectangle(bounds, type, progress, to);
	EsRectangle sourceRegion = ES_MAKE_RECTANGLE(0, bounds.r - bounds.l, 0, bounds.b - bounds.t);
	uint16_t alpha = (to ? progress : (1 - progress)) * 255;
	EsDrawPaintTarget(painter, sourceSurface, destinationRegion, sourceRegion, alpha);
}

EsRectangle EsPainterBoundsClient(EsPainter *painter) {
	return ES_MAKE_RECTANGLE(painter->offsetX, painter->offsetX + painter->width, painter->offsetY, painter->offsetY + painter->height);
}

EsRectangle EsPainterBoundsInset(EsPainter *painter) {
	UIStyle *style = (UIStyle *) painter->style;
	return ES_MAKE_RECTANGLE(painter->offsetX + style->insets.l, painter->offsetX + painter->width - style->insets.r, 
			painter->offsetY + style->insets.t, painter->offsetY + painter->height - style->insets.b);
}

uint32_t EsColorConvertToRGB(float h, float s, float v) {
	float r = 0, g = 0, b = 0;
	ConvertHSVToRGB(h, s, v, r, g, b);
	return (((uint32_t) (r * 255)) << 16) | (((uint32_t) (g * 255)) << 8) | (((uint32_t) (b * 255)) << 0);
}

bool EsColorConvertToHSV(uint32_t color, float *h, float *s, float *v) {
	return ConvertRGBToHSV((float) ((color >> 16) & 0xFF) / 255.0f, (float) ((color >> 8) & 0xFF) / 255.0f, (float) ((color >> 0) & 0xFF) / 255.0f, *h, *s, *v);
}

uint32_t EsColorBlend(uint32_t under, uint32_t over, bool fullAlpha) {
	BlendPixel(&under, over, fullAlpha);
	return under;
}

bool EsPaintTargetTake(EsPaintTarget *target, size_t width, size_t height) {
	EsMemoryZero(target, sizeof(EsPaintTarget));
	target->fullAlpha = true;
	target->width = width;
	target->height = height;
	target->stride = width * 4;
	target->bits = EsHeapAllocate(target->stride * target->height, true);
	return target->bits != nullptr;
}

void EsPaintTargetClear(EsPaintTarget *t) {
	EsPainter painter = {};
	painter.clip.r = t->width;
	painter.clip.b = t->height;
	painter.target = t;
	EsDrawClear(&painter, painter.clip);
}

void EsPaintTargetReturn(EsPaintTarget *target) {
	EsHeapFree(target->bits);
}

void EsPaintTargetEndDirectAccess(EsPaintTarget *target) {
	// Don't need to do anything, currently.
	(void) target;
}

void EsPaintTargetStartDirectAccess(EsPaintTarget *target, uint32_t **bits, size_t *width, size_t *height, size_t *stride) {
	if (bits) *bits = (uint32_t *) target->bits;
	if (width) *width = target->width;
	if (height) *height = target->height;
	if (stride) *stride = target->stride;
}

void EsElement::Repaint(bool all, EsRectangle region) {
	// TODO Optimisation: don't paint if overlapped by an opaque child or sibling.

	if (all) {
		region.l = -currentStyle->paintOutsets.l, region.r =  width + currentStyle->paintOutsets.r;
		region.t = -currentStyle->paintOutsets.t, region.b = height + currentStyle->paintOutsets.b;
	} else {
		region = Translate(region, -internalOffsetLeft, -internalOffsetTop);
	}

	if (parent) {
		EsRectangle parentBounds = parent->GetWindowBounds(false);

		region = Translate(region, offsetX + parentBounds.l, offsetY + parentBounds.t);

		if (parent->currentStyle->metrics->clipEnabled) {
			Rectangle16 clipInsets = parent->currentStyle->metrics->clipInsets;
			region = EsRectangleIntersection(region, AddBorder(parentBounds, clipInsets));
		}

		EnsureWindowWillUpdate(window);
	}

	if (THEME_RECT_VALID(region)) {
		window->updateRegion = EsRectangleBounding(window->updateRegion, region);
	}
}

void EsElement::InternalPaint(EsPainter *painter, int paintFlags) {
	if (width <= 0 || height <= 0 || (flags & ES_ELEMENT_HIDDEN)) {
		return;
	}

	state |= UI_STATE_ENTERED;

	int pOffsetX = painter->offsetX;
	int pOffsetY = painter->offsetY;

	if (~paintFlags & PAINT_NO_OFFSET) {
		pOffsetX += offsetX;
		pOffsetY += offsetY;
	}

	// Is it possible for this element to paint within the clip?

	{
		EsRectangle area; 
		area.l = pOffsetX - currentStyle->paintOutsets.l;
		area.r = pOffsetX + width + currentStyle->paintOutsets.r;
		area.t = pOffsetY - currentStyle->paintOutsets.t;
		area.b = pOffsetY + height + currentStyle->paintOutsets.b;

		if (!THEME_RECT_VALID(EsRectangleIntersection(area, painter->clip))) {
			return;
		}
	}

	// Get the interpolated style.

	EsPainter oldPainter = *painter;

	UIStyle *style = ThemeAnimationComplete(&animation) ? currentStyle : ThemeStyleInterpolate(currentStyle, &animation);
	EsDefer(if (style != currentStyle) EsHeapFree(style));
	painter->style = style;

	painter->offsetX = pOffsetX, painter->offsetY = pOffsetY;
	painter->width = width, painter->height = height;

	// Get the child type of the element.

	int childType = 0;

	if (parent && parent->GetChildCount()) {
		if (parent->GetChildCount() == 1 && parent->GetChild(0) == this) {
			childType = THEME_CHILD_TYPE_ONLY;
		} else if (parent->GetChild(0) == this) {
			childType = (parent->flags & ES_ELEMENT_LAYOUT_HINT_REVERSE) ? THEME_CHILD_TYPE_LAST : THEME_CHILD_TYPE_FIRST;
		} else if (parent->GetChild(parent->GetChildCount() - 1) == this) {
			childType = (parent->flags & ES_ELEMENT_LAYOUT_HINT_REVERSE) ? THEME_CHILD_TYPE_FIRST : THEME_CHILD_TYPE_LAST;
		} else {
			childType = THEME_CHILD_TYPE_NONE;
		}

		if (parent->flags & ES_ELEMENT_LAYOUT_HINT_HORIZONTAL) {
			childType |= THEME_CHILD_TYPE_HORIZONTAL;
		}
	}

	if (paintFlags & PAINT_SHADOW) {
		// Paint the shadow.

		style->PaintLayers(painter, painter->width, painter->height, 0, 0, childType, true);
	} else {
		// Paint the background.

		EsMessage m;
		m.type = ES_MSG_PAINT_BACKGROUND;
		m.painter = painter;

		if (!EsMessageSend(this, &m)) {
			// TODO Optimisation: don't paint if overlapped by an opaque child.
			style->PaintLayers(painter, painter->width, painter->height, 0, 0, childType, false);
		}
		
		// Apply the clipping insets.

		EsRectangle oldClip = painter->clip;

		if (currentStyle->metrics->clipEnabled && (~flags & ES_ELEMENT_NO_CLIP)) {
			Rectangle16 insets = currentStyle->metrics->clipInsets;
			EsRectangle content = ES_MAKE_RECTANGLE(painter->offsetX + insets.l, painter->offsetX + width - insets.r, 
					painter->offsetY + insets.t, painter->offsetY + height - insets.b);
			painter->clip = EsRectangleIntersection(content, painter->clip);
		}

		if (THEME_RECT_VALID(painter->clip)) {
			// Paint the content.

			painter->width -= internalOffsetLeft + internalOffsetRight;
			painter->height -= internalOffsetTop + internalOffsetBottom;
			painter->offsetX += internalOffsetLeft, painter->offsetY += internalOffsetTop;

			m.type = ES_MSG_PAINT;
			m.painter = painter;
			EsMessageSend(this, &m);

			painter->width += internalOffsetLeft + internalOffsetRight;
			painter->height += internalOffsetTop + internalOffsetBottom;
			painter->offsetX -= internalOffsetLeft, painter->offsetY -= internalOffsetTop;

			// Paint the children.
			// TODO Optimisation: don't paint children overlapped by an opaque sibling.

			m.type = ES_MSG_PAINT_CHILDREN;
			m.painter = painter;

			if (!EsMessageSend(this, &m)) {
				bool isZStack = state & UI_STATE_Z_STACK;

				EsMessage zOrder;
				zOrder.type = ES_MSG_BEFORE_Z_ORDER;
				zOrder.beforeZOrder.start = 0;
				zOrder.beforeZOrder.nonClient = zOrder.beforeZOrder.end = arrlenu(children);
				zOrder.beforeZOrder.clip = Translate(painter->clip, -painter->offsetX, -painter->offsetY);
				EsMessageSend(this, &zOrder);

				if (isZStack) {
					// Elements cast shadows on each other.

					for (uintptr_t i = zOrder.beforeZOrder.start; i < zOrder.beforeZOrder.end; i++) {
						EsElement *child = GetChildByZ(i);
						if (!child) continue;
						child->InternalPaint(painter, PAINT_SHADOW);
						child->InternalPaint(painter, ES_FLAGS_DEFAULT);
					}

					for (uintptr_t i = zOrder.beforeZOrder.nonClient; i < arrlenu(children); i++) {
						EsElement *child = GetChildByZ(i);
						if (!child) continue;
						child->InternalPaint(painter, PAINT_SHADOW);
						child->InternalPaint(painter, ES_FLAGS_DEFAULT);
					}
				} else {
					// Elements cast shadows on the container.

					for (uintptr_t i = zOrder.beforeZOrder.start; i < zOrder.beforeZOrder.end; i++) {
						EsElement *child = GetChildByZ(i);
						if (child) child->InternalPaint(painter, PAINT_SHADOW);
					}

					for (uintptr_t i = zOrder.beforeZOrder.nonClient; i < arrlenu(children); i++) {
						EsElement *child = GetChildByZ(i);
						if (child) child->InternalPaint(painter, PAINT_SHADOW);
					}

					for (uintptr_t i = zOrder.beforeZOrder.start; i < zOrder.beforeZOrder.end; i++) {
						EsElement *child = GetChildByZ(i);
						if (child) child->InternalPaint(painter, ES_FLAGS_DEFAULT);
					}

					for (uintptr_t i = zOrder.beforeZOrder.nonClient; i < arrlenu(children); i++) {
						EsElement *child = GetChildByZ(i);
						if (child) child->InternalPaint(painter, ES_FLAGS_DEFAULT);
					}
				}

				zOrder.type = ES_MSG_AFTER_Z_ORDER;
				EsMessageSend(this, &zOrder);
			}
		}

		// Let the inspector draw some decorations over the element.

		painter->clip = oldClip;
		InspectorNotifyElementPainted(this, painter);
	}

	*painter = oldPainter;
}
