#include <essence.h>

extern "C" int main(int argc, char **argv, char **envp);
extern "C" int __libc_start_main(int (*main)(int, char **, char **), int argc, char **argv);

extern "C" void _start() {
	int argc; 
	char **argv;
	EsInitialiseCStandardLibrary(&argc, &argv);
	__libc_start_main(main, argc, argv);
}

extern "C" long OSMakeLinuxSystemCall(long n, long a1, long a2, long a3, long a4, long a5, long a6) {
	return EsMakeLinuxSystemCall2(n, a1, a2, a3, a4, a5, a6);
}
